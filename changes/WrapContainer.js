import React, { Component } from "react";
import { connect } from "react-redux";
import { getInitialData, setErrorMessage } from "./../actions/commonAction";
import GenericErrorMessage from "./../components/GenericErrorMessage/GenericErrorMessage";
import Loader from "./../components/Loader/Loader";
import NavItem from "./../components/NavItem/NavItem";

import "./../assets/css/bootstrap.css";
import "./../assets/css/uob-form-loan.css";
import "./../assets/css/eBiz-style.css";

class WrapContainer extends Component {
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(getInitialData());
  }

  handleOnClearMessage() {
    const { dispatch } = this.props;
    dispatch(setErrorMessage(""));
  }

  render() {
    const { commonReducer, retrievingReducer } = this.props;
    let imgUrlWhite = "./images/logos/uob-eBiz-Logo.svg"; //white
    let imgUrl = "./images/logos/uobLogo.svg"; //blue
    let pathname = window.location.pathname;
    const currentPath = pathname.substr(pathname.lastIndexOf('/') + 1);
    const currentSection = commonReducer.currentSection;
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "taxSelfDeclarations", "confirmDetailsPage"];
    const indexOfSteps = steps.indexOf(currentSection);
    const whiteScreenLogo = currentPath === "error";
    const productCode = retrievingReducer.myInfoData !== null && retrievingReducer.myInfoData.product && retrievingReducer.myInfoData.product.code ? retrievingReducer.myInfoData.product.code : "BB-EBIZ";
    const productTitle = commonReducer.appData !== null && commonReducer.appData.productTitle[productCode];

    return (
      <div className="uob-form-loan">
        {commonReducer.isLoading && <Loader />}
        {whiteScreenLogo &&
          <div className='uob-logo' style={{ zIndex: "9999", padding: "26px 40px 40px" }}>
            <img alt="logo" src={imgUrl} />
          </div>
        }
        <div>
          <nav
            className={`navbar navbar-expand-lg navbar-dark bg-primary-left fixed-top `}
            id="sideNav"
          >
            <div className="leftTopLogo">
              <img src={imgUrlWhite} alt="Logo" className="uob-logo" width="160" />
            </div>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav">
                <NavItem
                  defaultStep={true}
                  navTitle={"MyInfo Business"}
                  defaultLink={true}
                />
                <NavItem
                  stepFinished={indexOfSteps > 0}
                  currentActiveStep={indexOfSteps === 0}
                  linkSelected={
                    (indexOfSteps === 0 && this.props.location.hash === "") ||
                    (indexOfSteps >= 0 &&
                      this.props.location.hash === "#contactDetails-section")
                  }
                  linkIdName={"#contactDetails-section"}
                  navTitle={"Basic Details"}
                />
                <NavItem
                  stepFinished={indexOfSteps > 1}
                  currentActiveStep={indexOfSteps === 1}
                  linkSelected={
                    (indexOfSteps === 1 && this.props.location.hash === "") ||
                    (indexOfSteps >= 1 &&
                      this.props.location.hash ===
                      "#personalIncomeDetails-section")
                  }
                  linkIdName={"#personalIncomeDetails-section"}
                  navTitle={"Applicant's Personal Details"}
                />
                <NavItem
                  stepFinished={indexOfSteps > 2}
                  currentActiveStep={indexOfSteps === 2}
                  linkSelected={
                    (indexOfSteps === 2 && this.props.location.hash === "") ||
                    (indexOfSteps >= 2 &&
                      this.props.location.hash === "#companyDetails-section")
                  }
                  linkIdName={"#companyDetails-section"}
                  navTitle={"Company Details"}
                />
                <NavItem
                  stepFinished={indexOfSteps > 3}
                  currentActiveStep={indexOfSteps === 3}
                  linkSelected={
                    (indexOfSteps === 3 && this.props.location.hash === "") ||
                    (indexOfSteps >= 3 && this.props.location.hash === "#accountSetup-section")
                  }
                  linkIdName={"#accountSetup-section"}
                  navTitle={"Account Setup"}
                />
                <NavItem
                  stepFinished={indexOfSteps > 4}
                  currentActiveStep={indexOfSteps === 4}
                  linkSelected={
                    (indexOfSteps === 4 && this.props.location.hash === "") ||
                    (indexOfSteps >= 4 && this.props.location.hash === "#operatingMandate-section")
                  }
                  linkIdName={"#operatingMandate-section"}
                  navTitle={"Operating Mandate"}
                />
                <NavItem
                  stepFinished={indexOfSteps > 5}
                  currentActiveStep={indexOfSteps === 5}
                  linkSelected={
                    (indexOfSteps === 5 && this.props.location.hash === "") ||
                    (indexOfSteps >= 5 && this.props.location.hash === "#taxSelfDeclarations-section")
                  }
                  linkIdName={"#taxSelfDeclarations-section"}
                  navTitle={"Tax Self-declaration"}
                />
                <NavItem
                  stepFinished={indexOfSteps > 6}
                  currentActiveStep={indexOfSteps === 6}
                  linkSelected={
                    (indexOfSteps === 6 && this.props.location.hash === "") ||
                    (indexOfSteps >= 6 &&
                      this.props.location.hash ===
                      "#confirmDetailsPage-section")
                  }
                  linkIdName={"#confirmDetailsPage-section"}
                  navTitle={"Review"}
                  hideBottomVerticalLine={true}
                />
              </ul>
            </div>
          </nav>
          {commonReducer.appData !== null && retrievingReducer.myInfoData !== null &&
            <div className="container-fluid p-0 content_1">
              <section
                className="resume-section d-flex d-column"
                id="home"
              >
                <div className="my-auto">
                  <h1 className="mt-0 title1">
                    <span className="logoRight">
                      <img
                        src={imgUrlWhite}
                        alt="Logo"
                        className="uob-logo"
                        width="160"
                      />
                    </span>
                    <span className="title2">
                      {productTitle}
                    </span>
                  </h1>
                </div>
              </section>
            </div>
          }
        </div>
        <div className="uob-form-loan-container">
          <div className="uob-body">
            {commonReducer.appData !== null && this.props.children}
          </div>
        </div>
        {commonReducer.messageContent !== "" && (
          <GenericErrorMessage
            {...this.props}
            interval={60}
            messageContent={commonReducer.messageContent}
            onClearMessage={this.handleOnClearMessage.bind(this)}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { commonReducer, retrievingReducer } = state;
  return { commonReducer, retrievingReducer };
};

export default connect(mapStateToProps)(WrapContainer);
