import React, { Component } from "react";
import { connect } from "react-redux";
import Dropdown from "./../components/Dropdown/Dropdown";
import TextInput from "./../components/TextInput/TextInput";
import { mapValueToDescription, capitalize } from "./../common/utils";
import moment from 'moment';
import Checkbox from "./../components/Checkbox/Checkbox";
import RadioButton from "./../components/RadioButton/RadioButton";
import { handleIsChecked, setDropdownFocusStatus, selectDropdownItem, handleTextInputChange, changeSearchInputValue, selectRadioItem } from "./../actions/companyBasicDetailsAction";
import LocalAddressInput from "./../components/Uob/LocalAddressInput/LocalAddressInput";
class CompanyBasicDetails extends Component {


    handleOnChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChange(data, field));
    }

    handleOnCheckbox(field, isToggled) {
        const { dispatch } = this.props;
        dispatch(handleIsChecked(field, isToggled));
    }

    handleDropdownBlur(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(false, field));
    }

    handleDropdownFocus(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(true, field));
    }

    handleDropdownClick(data, field) {
        const { dispatch, companyBasicDetailsReducer } = this.props;

        if (companyBasicDetailsReducer[field].value === data.value) {
            return;
        }

        dispatch(selectDropdownItem(data.value, field, data.description));
    }

    handleOnSearchChange(e, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValue(value, field));
    }

    handleRadioSelection(data, field) {
        const { dispatch } = this.props;
        dispatch(selectRadioItem(data, field));
    }

    render() {
        const { commonReducer, companyBasicDetailsReducer, localAddressInputReducer } = this.props;
        const { standard, unformattedCompanyCountry, companyAddressLine1, companyAddressLine2, companyRegistrationNumber, registeredCompanyName, typeOfCompany, companyStatus, countryOfIncorporation, ownership, registrationDate, companyExpiryDate, mailingCountry, mailingAddressCheckbox, overseasAddress1, overseasAddress2, overseasAddress3, overseasAddress4, mailingCity, annualTurnover } = companyBasicDetailsReducer;
        const { companyCountry } = localAddressInputReducer;

        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";

        const companyDetailsValues = commonReducer.appData.companyDetails;
        const companyBasicDetailsValues = companyDetailsValues.companyBasicDetails;
        const labels = companyBasicDetailsValues.labels;

        const errorMsgList = commonReducer.appData.errorMsgs;
        const countriesNamesMap = inputValues.countriesNamesMap;
        const companyTypeList = inputValues.companyType;
        const annualTurnoverList = inputValues.annualTurnoverType;
        const ownershipList = inputValues.ownership;

        const countryOfIncorporationValue = countryOfIncorporation.value !== "" ? countryOfIncorporation.value : "SG";
        const ownershipValue = ownership.value !== "" ? ownership.value : "NA";

        const registrationDateFormat = moment(registrationDate.value, "YYYY-MM-DD").format("DD/MM/YYYY");
        const companyExpiryDateFormat = moment(companyExpiryDate.value, "YYYY-MM-DD").format("DD/MM/YYYY");
        return (
            <div className="uob-content">
                <h1>{companyDetailsValues.title}</h1>
                <div className="uob-form-separator" />
                <p className="uob-headline">{companyDetailsValues.headline}</p>
                <h1 className="sub-title">{companyBasicDetailsValues.subtitle}</h1>

                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyRegistrationNumber}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {companyRegistrationNumber.value}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.registeredCompanyName}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {registeredCompanyName.value}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.typeOfCompany}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {companyTypeList[typeOfCompany.value]}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyStatus}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {capitalize(companyStatus.value)}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.countryOfIncorporation}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {countriesNamesMap[countryOfIncorporationValue]}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.ownership}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {ownershipList[ownershipValue]}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-border bottom-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.registrationDate}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {registrationDateFormat}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyExpiryDate}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {companyExpiryDateFormat}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="sub-title"> {labels.annualTurnover} </div>
                <div className="sub-sub-title"> {labels.annualTurnoverSubtitle}</div>
                <RadioButton
                    inputID="annualTurnover"
                    value={annualTurnover.value}
                    radioObj={mapValueToDescription(
                        annualTurnoverList
                    )}
                    isDisabled={false}
                    errorMsg={annualTurnover.errorMsg}
                    isValid={annualTurnover.isValid}
                    onClick={(data) => this.handleRadioSelection(data, "annualTurnover")}
                />
                <h1 className="sub-title">{companyBasicDetailsValues.subtitle2}</h1>
                <div className="sub-div-common">
                    <div className="p-l-10 fullWidth positionRelative">
                        <Dropdown
                            inputID={"companyCountry"}
                            label={labels.companyCountry}
                            value={standard.value === "D" ? companyCountry.value : unformattedCompanyCountry.value}
                            isFocus={companyCountry.isFocus}
                            errorMsg={companyCountry.errorMsg}
                            dropdownItems={mapValueToDescription(
                                countriesNamesMap
                            )}
                            searchValue={companyCountry.searchValue}
                            onBlur={this.handleDropdownBlur.bind(this, "companyCountry")}
                            onFocus={this.handleDropdownFocus.bind(this, "companyCountry")}
                            onClick={data => this.handleDropdownClick(data, "companyCountry")}
                            onSearchChange={event =>
                                this.handleOnSearchChange(event, "companyCountry")
                            }
                            isReadOnly
                            flagImg />
                    </div>
                </div>
                {
                    companyAddressLine1.value === "" && companyAddressLine2.value === "" ?
                        <LocalAddressInput
                            labels={labels}
                            errorMsgList={errorMsgList}
                            addressType="company"
                        /> :
                        <div>
                            <div className="sub-div-common  flexCenterFullWidth positionRelative">
                                <div className="p-l-10 fullWidth">
                                    <TextInput
                                        inputID={`companyAddressLine1`}
                                        isValid={companyAddressLine1.isValid}
                                        label={labels.companyAddressLine1}
                                        value={companyAddressLine1.value}
                                        isReadOnly
                                        hasIcon={false}
                                    />
                                </div>
                            </div>
                            <div className="sub-div-common  flexCenterFullWidth positionRelative bottom-border">
                                <div className="p-l-10 fullWidth">
                                    <TextInput
                                        inputID={`companyAddressLine2`}
                                        isValid={companyAddressLine2.isValid}
                                        label={labels.companyAddressLine2}
                                        value={companyAddressLine2.value}
                                        isReadOnly
                                        hasIcon={false}
                                    />
                                </div>
                            </div>
                        </div>
                }

                <Checkbox
                    inputID="mailingAddressCheckbox"
                    description={companyBasicDetailsValues.mailingSubtitle}
                    isChecked={mailingAddressCheckbox.isToggled}
                    onClick={() => this.handleOnCheckbox('mailingAddressCheckbox', mailingAddressCheckbox.isToggled)}
                />
                {
                    mailingAddressCheckbox.isToggled ?
                        <div className="mailingDetails-section">
                            <div className="top-border">
                                <div className="fullTable confirmDetails-flexContainer">
                                    <div className="right-border halfRow positionRelative">
                                        <div className="confirmDetails-label-container ">
                                            <Dropdown
                                                inputID={"mailingCountry"}
                                                label={labels.companyCountry}
                                                value={mailingCountry.value}
                                                isFocus={mailingCountry.isFocus}
                                                errorMsg={mailingCountry.errorMsg}
                                                dropdownItems={mapValueToDescription(
                                                    countriesNamesMap
                                                )}
                                                searchValue={mailingCountry.searchValue}
                                                onBlur={this.handleDropdownBlur.bind(this, "mailingCountry")}
                                                onFocus={this.handleDropdownFocus.bind(this, "mailingCountry")}
                                                onClick={data => this.handleDropdownClick(data, "mailingCountry")}
                                                onSearchChange={event =>
                                                    this.handleOnSearchChange(event, "mailingCountry")
                                                }
                                                flagImg />
                                        </div>
                                    </div>
                                    <div className="halfRow">
                                    </div>
                                </div>
                            </div>
                            {mailingCountry.value !== "SG" ? (
                                <div>
                                    <div className="sub-div-common positionRelative">
                                        <div className="p-l-10 fullWidth">
                                            <TextInput
                                                inputID="overseasAddress1"
                                                label={labels.overseasAddress1}
                                                value={overseasAddress1.value}
                                                errorMsg={overseasAddress1.errorMsg}
                                                onChange={data => this.handleOnChange(data, "overseasAddress1")}
                                                isValid={overseasAddress1.isValid}
                                                validator={["required", "maxSize|30"]}
                                                errorMsgList={errorMsgList}
                                                hasIcon={false}
                                            />
                                        </div>
                                    </div>
                                    <div className="sub-div-common positionRelative">
                                        <div className="p-l-10 fullWidth">
                                            <TextInput
                                                inputID="overseasAddress2"
                                                label={labels.overseasAddress2}
                                                value={overseasAddress2.value}
                                                errorMsg={overseasAddress2.errorMsg}
                                                onChange={data => this.handleOnChange(data, "overseasAddress2")}
                                                isValid={overseasAddress2.isValid}
                                                validator={["maxSize|30"]}
                                                errorMsgList={errorMsgList}
                                                hasIcon={false}
                                            />
                                        </div>
                                    </div>
                                    <div className="sub-div-common positionRelative">
                                        <div className="p-l-10 fullWidth">
                                            <TextInput
                                                inputID="overseasAddress3"
                                                label={labels.overseasAddress3}
                                                value={overseasAddress3.value}
                                                errorMsg={overseasAddress3.errorMsg}
                                                onChange={data => this.handleOnChange(data, "overseasAddress3")}
                                                isValid={overseasAddress3.isValid}
                                                validator={["maxSize|30"]}
                                                errorMsgList={errorMsgList}
                                                hasIcon={false}
                                            />
                                        </div>
                                    </div>
                                    <div className="sub-div-common positionRelative">
                                        <div className="p-l-10 fullWidth">
                                            <TextInput
                                                inputID="overseasAddress4"
                                                label={labels.overseasAddress4}
                                                value={overseasAddress4.value}
                                                errorMsg={overseasAddress4.errorMsg}
                                                onChange={data => this.handleOnChange(data, "overseasAddress4")}
                                                isValid={overseasAddress4.isValid}
                                                validator={["maxSize|30"]}
                                                errorMsgList={errorMsgList}
                                                hasIcon={false}
                                            />
                                        </div>
                                    </div>
                                    <div className="sub-div-common positionRelative bottom-border">
                                        <div className="p-l-10 fullWidth">
                                            <TextInput
                                                inputID="mailingCity"
                                                label={labels.mailingCity}
                                                value={mailingCity.value}
                                                errorMsg={mailingCity.errorMsg}
                                                onChange={data => this.handleOnChange(data, "mailingCity")}
                                                isValid={mailingCity.isValid}
                                                validator={["required", "maxSize|30"]}
                                                errorMsgList={errorMsgList}
                                                hasIcon={false}
                                            />
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                    <LocalAddressInput
                                        labels={labels}
                                        errorMsgList={errorMsgList}
                                        addressType="mailing"
                                    />
                                )}
                        </div>
                        : null
                }
            </div >
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, companyBasicDetailsReducer, localAddressInputReducer } = state;
    return { commonReducer, companyBasicDetailsReducer, localAddressInputReducer };
};

export default connect(mapStateToProps)(CompanyBasicDetails);
