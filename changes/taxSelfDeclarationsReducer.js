import * as types from './../actions/actionTypes';

const initialState = {
    checkboxSG: {
        isToggled: true,
        isValid: true,
    },
    checkboxUS: {
        isToggled: false,
        isValid: true,
    },
    checkboxOthers: {
        isToggled: false,
        isValid: true,
    },
    checkboxNone: {
        isToggled: false,
        isValid: true,
    },
    tinNoSG: {
        name: 'tinNoSG',
        value: '',
        isValid: true,
        errorMsg: '',
        isInitial: true
    },
    tinNoUS: {
        name: 'tinNoUS',
        value: '',
        isValid: true,
        errorMsg: '',
        isInitial: true
    },
    isUSResident: {
        name: 'isUSResident',
        value: '',
        isValid: false,
        errorMsg: '',
        isInitial: true
    },
    tinNoOthersId: [0],
    tinNoOthers0: {
        haveTINNo: {
            name: 'haveTINNo',
            value: '',
            isValid: true,
            errorMsg: '',
            isInitial: true
        },
        othersCountry: {
            name: "othersCountry",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
        },
        othersTINNo: {
            name: "othersTINNo",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
        },
        reasons: {
            name: "reasons",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true
        }
    },
    countryNone: {
        name: 'countryNone',
        isValid: true,
        isFocus: false,
        value: '',
        errorMsg: '',
        isInitial: true,
        isMyInfo: false,
        description: ''
    }
};

const taxSelfDeclarationsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CRS_DROPDOWN_ITEM_SELECT:
            return { ...state, [action.field]: { ...state[action.field], value: action.value, description: action.description, isValid: true, errorMsg: '', isInitial: false } };


        case types.CRS_DROPDOWN_ITEM_SELECT_CHANGE:
            return {
                ...state, [action.key]:
                {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value,
                        description: action.description,
                        isValid: true,
                        errorMsg: '',
                        isInitial: false
                    }
                }
            };

        case types.CRS_DROPDOWN_FOCUS_CHANGE:
            return {
                ...state, [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        isFocus: action.isFocus
                    }
                }
            };

        case types.CRS_DROPDOWN_SEARCH_INPUT_CHANGE:
            return {
                ...state, [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        searchValue: action.searchValue
                    }
                }
            };

        case types.CRS_DROPDOWN_SEARCH_INPUT_CHANGE:
            return { ...state, [action.field]: { ...state[action.field], searchValue: action.searchValue } };


        case types.CRS_HANDLE_TEXT_INPUT_CHANGE_OTHERS:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value,
                        isValid: action.isValid,
                        errorMsg: action.errorMsg,
                        isInitial: false
                    }
                }
            };

        case types.CRS_IS_CHECKED:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    isToggled: action.isToggled,
                    isValid: true
                }
            };

        case types.CRS_HANDLE_TEXT_INPUT_CHANGE:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    value: action.value,
                    isValid: action.isValid,
                    errorMsg: action.errorMsg,
                    isInitial: false
                }
            };
        case types.CRS_HANDLE_BUTTON_CHANGE:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    value: action.value,
                    isValid: true
                }
            };

        case types.CRS_HANDLE_BUTTON_CHANGE_OTHERS:
            return {
                ...state, [action.key]:
                {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value
                    }
                }
            };

        case types.CRS_SELECT_RADIO_BUTTON:
            return {
                ...state, [action.key]:
                {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value,
                        errorMsg: "",
                        isValid: true
                    }
                }
            };

        case types.MYINFO_SET_COMPANY_UEN_DATA:
            return {
                ...state,
                tinNoSG: {
                    ...state.tinNoSG,
                    value: action.tinNoSG
                }
            };

        case types.CRS_ADD_COUNTRY:
            return {
                ...state,
                [action.tinNoOthers]: {
                    haveTINNo: {
                        name: 'haveTINNo',
                        value: '',
                        isValid: true,
                        errorMsg: '',
                        isInitial: true
                    },
                    othersCountry: {
                        name: "othersCountry",
                        isValid: true,
                        value: "",
                        errorMsg: "",
                        isInitial: true,
                    },
                    othersTINNo: {
                        name: "othersTINNo",
                        isValid: true,
                        value: "",
                        errorMsg: "",
                        isInitial: true,
                    },
                    reasons: {
                        name: "reasons",
                        isValid: true,
                        value: "",
                        errorMsg: "",
                        isInitial: true
                    }
                }
            };

        case types.CRS_INCREMENT_COUNTRY_COUNT:
            return {
                ...state,
                tinNoOthersId: action.tinNoOthersId
            };

        case types.CRS_DROPDOWN_FOCUS:
            return { ...state, [action.field]: { ...state[action.field], isFocus: action.isFocus } };

        case types.CRS_UNCHECK_REST:
            return {
                ...state,
                checkboxSG: {
                    ...state,
                    isToggled: false,
                },
                checkboxUS: {
                    ...state,
                    isToggled: false
                },
                checkboxOthers: {
                    ...state,
                    isToggled: false
                }
            };

        case types.CRS_UNCHECK_NONE:
            return {
                ...state,
                checkboxNone: {
                    ...state,
                    isToggled: false
                }
            };
        default:
            return state;
    }
}

export default taxSelfDeclarationsReducer;