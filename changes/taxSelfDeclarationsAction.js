import * as types from "./actionTypes";

export const handleIsChecked = (field, isToggled) => {
    return {
        type: types.CRS_IS_CHECKED,
        field,
        isToggled: !isToggled
    }
}

export const uncheckRest = () => {
    return {
        type: types.CRS_UNCHECK_REST
    }
}

export const uncheckNone = () => {
    return {
        type: types.CRS_UNCHECK_NONE
    }
}

export const handleButtonChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_BUTTON_CHANGE,
            value: data,
            field
        });
    };
};


export const handleButtonChangeOthers = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_BUTTON_CHANGE_OTHERS,
            value: data,
            key,
            field
        });
    };
};

export const handleTextInputChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_CHANGE,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
};

export const setCompanyUEN = (tinNoSG) => {
    return {
        type: types.MYINFO_SET_COMPANY_UEN_DATA,
        tinNoSG: tinNoSG ? tinNoSG : ""
    };
};

export const handleTextInputChangeOthers = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_CHANGE_OTHERS,
            key,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
}

//handle dropdown focus
export const setDropdownFocusStatusChange = (isFocus, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_FOCUS_CHANGE,
            isFocus,
            key,
            field
        });
    };
};

//choose item from dropdown
export const selectDropdownItemChange = (value, description, key, field) => {
    return (dispatch) => {
        if (field === "othersCountry") {
            const element = document.getElementById("editableDropdown-flagImg");
            element.style.display = "block";
        }

        dispatch({
            type: types.CRS_DROPDOWN_ITEM_SELECT_CHANGE,
            value,
            key,
            field,
            description
        })
    };
};

export const changeSearchInputValueOther = (searchValue, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_SEARCH_INPUT_CHANGE,
            key,
            searchValue,
            field
        });
    };
};


//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_FOCUS,
            isFocus,
            field
        });
    };
};

//choose item from dropdown
export const selectDropdownItem = (value, description, field) => {
    return (dispatch) => {
        if (field === "countryNone") {
            const element = document.getElementById("editableDropdown-flagImg");
            element.style.display = "block";
        }

        dispatch({
            type: types.CRS_DROPDOWN_ITEM_SELECT,
            value,
            field,
            description
        })
    };
};

export const changeSearchInputValue = (searchValue, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_SEARCH_INPUT,
            searchValue,
            field
        });
    };
};

export const selectRadioItem = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_SELECT_RADIO_BUTTON,
            value: data.value,
            key,
            field
        })
    }
}

//increment count
export const incrementCountry = (key) => {
    return (dispatch, getState) => {
        let tinNoOthersId = getState().taxSelfDeclarationsReducer.tinNoOthersId;
        tinNoOthersId.push(key + 1);
        dispatch({
            type: types.CRS_INCREMENT_COUNTRY_COUNT,
            tinNoOthersId
        });
    };
}

export const addNewCountry = (key) => {
    return {
        type: types.CRS_ADD_COUNTRY,
        tinNoOthers: `tinNoOthers${key + 1}`

    }
}