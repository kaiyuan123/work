import React, { Component } from "react";
import TextInput from "./../components/TextInput/TextInput";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import Checkbox from "./../components/Checkbox/Checkbox";
import { connect } from "react-redux";
import { handleIsChecked, handleBibPlusUserToggle, handleIsSignatoryChecked, handleTextInputChange } from "./../actions/operatingMandateAction";

class OperatingMandatePage extends Component {

	// componentWillMount() {
	//    const { dispatch, accountSetupReducer } = this.props;
	//   dispatch(handleBibPlusUserToggle("setupBIBUser", accountSetupReducer.setupBIBPlus.isToggled));
	// }

	handleToTaxSelfDeclarationsPage() {
		this.props.onContinue();
	}

	handleOnChange(data, key, field) {
		const { dispatch } = this.props;
		dispatch(handleTextInputChange(data, key, field));
	}

	handleOnRadioButton(value) {
		const { dispatch } = this.props;
		dispatch(handleIsSignatoryChecked(value));
	}

	handleOnCheckbox(key, field, isToggled, isValid) {
		const { dispatch } = this.props;
		dispatch(handleIsChecked(key, field, isToggled, isValid));
	}

	approvedSignatoriesValid() {
		const { operatingMandateReducer, accountSetupReducer } = this.props;
		const isBibPlusUserToggled = accountSetupReducer.setupBIBPlus.isToggled;
		const { signatoriesID, mainApplicantLegalId } = operatingMandateReducer;
		let errorCount = 0;
		if (signatoriesID.length > 0) {

			operatingMandateReducer.signatoriesID.map((key) => {
				const signatoriesEmail = operatingMandateReducer[`signatories${key}`].signatoriesEmail;
				const signatoriesNRIC = operatingMandateReducer[`signatories${key}`].signatoriesNRIC;
				const signatoriesName = operatingMandateReducer[`signatories${key}`].signatoriesName;
				const signatoriesMobileNo = operatingMandateReducer[`signatories${key}`].signatoriesMobileNo;
				const signatoriesBibUserId = operatingMandateReducer[`signatories${key}`].signatoriesBibUserId;

				const isApprovedSignatoryValid = signatoriesEmail.value !== "" && signatoriesEmail.isValid && signatoriesNRIC.value !== "" && signatoriesNRIC.isValid && signatoriesName.value !== "" && signatoriesName.isValid && signatoriesMobileNo.value !== "" && signatoriesMobileNo.isValid;

				let isBibUserIdValid = true;

				if (mainApplicantLegalId === operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value && isBibPlusUserToggled) {
					isBibUserIdValid = signatoriesBibUserId.value !== "";
				}


				if (!isApprovedSignatoryValid || !isBibUserIdValid) {
					errorCount++
				}
				return errorCount;
			})

			return errorCount;
		}
	}

	renderApprovedSignatoryList() {
		const { operatingMandateReducer, commonReducer, accountSetupReducer } = this.props;
		const isBibPlusUserToggled = accountSetupReducer.setupBIBPlus.isToggled;

		//const isBibUserToggled = operatingMandateReducer.setupBIBUser.isToggled;
		const { signatoriesID, setupBIBUser, bibUserId, mainApplicantLegalId } = operatingMandateReducer;

		const length = signatoriesID.length;
		const operatingMandateValue = commonReducer.appData.operatingMandate;
		const subHeading = operatingMandateValue.subHeading;
		const sublabels = subHeading.labels;
		const tmpArray = [];
		const errorMsgList = commonReducer.appData.errorMsgs;

		signatoriesID.map((key, i) => {
			console.log("testing---", operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled)
			const applicantNumber = i + 1;
			tmpArray.push(
				<div key={key} >
					<div className="partner-subtitle-container">
						<div className="sub-title partner-subtite big-text">{applicantNumber}</div>
					</div>
					<div className="top-border">
						<div className="fullTable confirmDetails-flexContainer">
							<div className="halfRow right-border positionRelative">
								<TextInput
									inputID={`signatoriesName${key}`}
									label={sublabels.name}
									value={operatingMandateReducer[`signatories${key}`].signatoriesName.value}
									errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesName.errorMsg}
									isValid={operatingMandateReducer[`signatories${key}`].signatoriesName.isValid}
									onChange={(data) => this.handleOnChange(data, `signatories${key}`, 'signatoriesName')}
									validator={["isFullName", "maxSize|70"]}
									errorMsgList={errorMsgList}
									hasIcon={false}
									isReadOnly={operatingMandateReducer[`signatories${key}`].signatoriesName.isReadOnly}
								/>
							</div>
							<div className="halfRow positionRelative">
								<TextInput
									isUpperCase
									inputID={`signatoriesNRIC${key}`}
									label={sublabels.nric}
									value={operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value}
									errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesNRIC.errorMsg}
									isValid={operatingMandateReducer[`signatories${key}`].signatoriesNRIC.isValid}
									onChange={(data) => this.handleOnChange(data, `signatories${key}`, 'signatoriesNRIC')}
									errorMsgList={errorMsgList}
									validator={["isNRIC"]}
									hasIcon={false}
									isReadOnly={operatingMandateReducer[`signatories${key}`].signatoriesNRIC.isReadOnly}
								/>
							</div>
						</div>
					</div>
					<div className="top-border bottom-border">
						<div className="fullTable confirmDetails-flexContainer">
							<div className="halfRow right-border positionRelative">
								<TextInput
									type="Phone"
									inputID={`signatoriesMobileNo${key}`}
									label={sublabels.mobile}
									value={operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.value}
									errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.errorMsg}
									isValid={operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.isValid}
									onChange={(data) => this.handleOnChange(data, `signatories${key}`, 'signatoriesMobileNo')}
									errorMsgList={errorMsgList}
									validator={["isMobileNumber"]}
									hasIcon={false}
									isReadOnly={operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.isReadOnly}
								/>
							</div>
							<div className="halfRow positionRelative">
								<TextInput
									inputID={`signatoriesEmail${key}`}
									label={sublabels.email}
									value={operatingMandateReducer[`signatories${key}`].signatoriesEmail.value}
									errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesEmail.errorMsg}
									isValid={operatingMandateReducer[`signatories${key}`].signatoriesEmail.isValid}
									onChange={(data) => this.handleOnChange(data, `signatories${key}`, 'signatoriesEmail')}
									errorMsgList={errorMsgList}
									validator={["isEmail", "maxSize|30"]}
									hasIcon={false}
									isReadOnly={operatingMandateReducer[`signatories${key}`].signatoriesEmail.isReadOnly}
								/>
							</div>
						</div>
					</div>
					{mainApplicantLegalId === operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value && isBibPlusUserToggled ? <div className="top-border bottom-border bibuser">
						<Checkbox
							description={sublabels.setupBIBUser}
							isChecked={operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled}
							onClick={() => this.handleOnCheckbox(`signatories${key}`, 'signatoriesToggleBIBUser', operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled, `signatoriesToggleBIBUser.isValid`)}
						/>

						{operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled && <div className="bibuser-container">
							<div className="sub-div-common flexCenterFullWidth positionRelative bottom-border">
								<div className="p-l-10 fullWidth">
									<TextInput
										inputID={'bibUserId'}
										label={sublabels.bibUserId}
										isReadOnly={false}
										value={operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.value}
										errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.errorMsg}
										onChange={data => this.handleOnChange(data, `signatories${key}`, "signatoriesBibUserId")}
										isValid={operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.isValid}
										validator={["isBIBUserId", "maxSize|20"]}
										errorMsgList={errorMsgList}
										hasIcon={false}
									/>
								</div>
							</div>
							<div className="companyGroupIDInfo">{sublabels.helpText}</div>
						</div>}
					</div> : <div className="top-border bottom-border bibuser">
							<Checkbox
								description={sublabels.setupBIBUser}
								isChecked={operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled}
								onClick={() => this.handleOnCheckbox(`signatories${key}`, 'signatoriesToggleBIBUser', operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled, `signatoriesToggleBIBUser.isValid`)}
							/></div>}
				</div>
			);
			return tmpArray;
		})
		return tmpArray;
	}

	render() {
		const { commonReducer, operatingMandateReducer } = this.props;
		const { approvedSignatories, bibUserId } = operatingMandateReducer;

		const lengthOfsignatories = approvedSignatories.value.length;
		const signatoriesArr = approvedSignatories.value;


		const operatingMandateValue = commonReducer.appData.operatingMandate;
		const subHeading = operatingMandateValue.subHeading;
		const sublabels = subHeading.labels;
		const errorMsgList = commonReducer.appData.errorMsgs;
		const labels = operatingMandateValue.labels;


		return (
			<div className="uob-content" id="operatingMandate-section">
				<h1>{operatingMandateValue.title}</h1>
				<p className="uob-headline">
					{operatingMandateValue.headline}
				</p>
				<div className="sub-title">{subHeading.title1}</div>
				<div className="medium-txt">{subHeading.subtitle1}</div>
				<div className="radiobutton-container">
					<div>
						<input
							type="radio"
							id={"signatory1"}
							name={"opertingMandate"}
							value={"signatory1"}
							onChange={(e) => this.handleOnRadioButton(e.target.value)}
						/>
						<label className="lbl-second" htmlFor={"signatory1"}>{sublabels.signatory1}</label>
					</div>
					{
						lengthOfsignatories > 1 && <div>
							<input
								type="radio"
								id={"signatory2"}
								name={"opertingMandate"}
								value={"signatory2"}
								onChange={(e) => this.handleOnRadioButton(e.target.value)}

							/>
							<label className="lbl-second" htmlFor={"signatory2"}>{sublabels.signatory2}</label>
						</div>
					}
					<div>
						<input
							type="radio"
							id={"signatory3"}
							name={"opertingMandate"}
							value={"signatory3"}
							onChange={(e) => this.handleOnRadioButton(e.target.value)}

						/>
						<label className="lbl-second" htmlFor={"signatory3"}>{sublabels.signatory3}</label>
					</div>
				</div>

				<div className="signatory-info">
					<div className="big-text">{subHeading.title2}</div>
					<div className="medium-txt">{subHeading.subtitle2}</div>
					<div>{this.renderApprovedSignatoryList()}</div>
				</div>


				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToTaxSelfDeclarationsPage()} isLoading={commonReducer.isProcessing} />
				</div>

				{
					this.approvedSignatoriesValid() > 0 &&
					<div>
						<div className="uob-input-separator fixedContinue fixedContinueGray">
							<PrimaryButton label={labels.continueButton} isLoading={commonReducer.isProcessing} />
						</div>
					</div>
				}
			</div>

		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, operatingMandateReducer } = state;
	return { commonReducer, operatingMandateReducer };
};

export default connect(mapStateToProps)(OperatingMandatePage);