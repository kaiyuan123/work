import React, { Component } from "react";
import { connect } from "react-redux";
import WrapContainer from "./WrapContainer";
// import LoadingPage from "./../pages/LoadingPage";
import ThankYouPage from "./../pages/ThankYouPage";
import ContactDetailsPage from "./../pages/ContactDetailsPage";
import PersonalIncomeDetailsPage from "./../pages/PersonalIncomeDetailsPage";
import CompanyDetailsPage from "./../pages/CompanyDetailsPage";
import AccountSetupPage from "./../pages/AccountSetupPage";
import OperatingMandatePage from "./../pages/OperatingMandatePage";
import ConfirmDetailsPage from "./../pages/ConfirmDetailsPage";
import TaxSelfDeclarationsPage from "./../pages/TaxSelfDeclarationsPage";
import { scrollToSection, setRetrievingMyInfo, handleErrorMessage, scrollToElement } from "./../actions/commonAction";
import {
  retrieveMyInfoDetails,
  submitInitiateApplication,
  submitPartialApplication,
  submitApplication
} from "./../actions/applicationAction";
import { groupByCode } from "../common/utils";
import { LocalEnvironment, root_path, Force_URL_redirect } from "./../api/httpApi"
import moment from 'moment';
import localStore from './../common/localStore';

class ApplicationContainer extends Component {
  componentWillMount() {
    const { dispatch, applicationReducer } = this.props;
    const params = applicationReducer.startingParameter;
    if (LocalEnvironment && Force_URL_redirect) {
      return true;
    }
    else {
      this.props.history.push(LocalEnvironment ? `/application${params}` : `${root_path}/application${params}`);
      dispatch(setRetrievingMyInfo(true));
      dispatch(retrieveMyInfoDetails(() => this.redirectToErrorPage()));
    }
  }

  checkisValidFields(fields) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (!fields[i].isValid) {
        const fieldError = fields.find(x => x.isValid === false);
        dispatch(scrollToElement(fieldError.name))
        errorCount++;
      }
    }
    if (errorCount > 0) {
      return false;
    }
    return true;
  }

  checkEmptyFields(fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (fields[i].value === "") {
        dispatch(action(fields[i].name, errorMsg));
        const fieldError = fields.find(x => x.value === '');
        dispatch(scrollToElement(fieldError.name))
        errorCount++;
      }
    }
    if (errorCount > 0) {
      return false;
    }
    return true;
  }


  checkAdditionalEmptyFields(key, fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (fields[i].value === "") {
        dispatch(action(key, fields[i].name, errorMsg));
        errorCount++;
      }
    }
    if (errorCount > 0) {
      return false;
    }
    return true;
  }

  isContactDetailsPassChecking() {
    const { contactDetailsReducer, commonReducer } = this.props;
    const { entityType, entitySubType, emailAddress, primaryNatureOfBusiness, secondaryNatureOfBusiness, primaryCountryOfOperation } = contactDetailsReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    if (!this.checkEmptyFields([primaryCountryOfOperation, primaryNatureOfBusiness, secondaryNatureOfBusiness, emailAddress], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (!this.checkisValidFields([primaryCountryOfOperation, primaryNatureOfBusiness, secondaryNatureOfBusiness, emailAddress], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (entityType.value === "P" || entityType.value === "O") {
      if (!this.checkEmptyFields([entitySubType], requiredMsg, handleErrorMessage)) {
        errorCount++
      }

      if (!this.checkisValidFields([entitySubType], requiredMsg, handleErrorMessage)) {
        errorCount++
      }
    }

    if (errorCount > 0) {
      return false;
    }

    return true;
  }

  isPersonalIncomeDetailsPassChecking() {
    const { personalIncomeDetailsReducer, commonReducer } = this.props;
    const { passportNumber, passportExpiryDate, residentialStatus } = personalIncomeDetailsReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    const isSgOrPr = residentialStatus.value === 'C' || residentialStatus.value === 'P';

    if (!isSgOrPr) {
      if (!this.checkEmptyFields([passportNumber, passportExpiryDate], requiredMsg, handleErrorMessage)) {
        errorCount++
      }

      if (!this.checkisValidFields([passportNumber, passportExpiryDate], requiredMsg, handleErrorMessage)) {
        errorCount++
      }
    }

    if (errorCount > 0) {
      return false;
    }

    return true;
  }

  isAccountSetupPassChecking() {
    const { accountSetupReducer, commonReducer } = this.props;
    const {
      accountName, purposeOfAccount, registerOfPayNow,
      setupBIBPlus, BIBGroupId, BIBMobileNumber, BIBContactPerson, BIBEmailAddress, payNowID
    } = accountSetupReducer;
    let errorCount = 0;

    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;

    if (!this.checkEmptyFields([accountName, purposeOfAccount], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (!this.checkisValidFields([accountName, purposeOfAccount], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (registerOfPayNow.isToggled) {
      if (!this.checkEmptyFields([payNowID], requiredMsg, handleErrorMessage)) {
        errorCount++
      }

      if (!this.checkisValidFields([payNowID], requiredMsg, handleErrorMessage)) {
        errorCount++
      }
    }

    if (setupBIBPlus.isToggled) {
      if (!this.checkEmptyFields([BIBMobileNumber, BIBContactPerson, BIBMobileNumber, BIBEmailAddress], requiredMsg, handleErrorMessage)) {
        errorCount++
      }

      if (!this.checkisValidFields([BIBGroupId, BIBContactPerson, BIBMobileNumber, BIBEmailAddress], requiredMsg, handleErrorMessage)) {
        errorCount++
      }
    }

    if (errorCount > 0) {
      return false
    }
    return true;
  }

  handleToPersonalIncomeDetails() {
    const { dispatch } = this.props;
    if (!this.isContactDetailsPassChecking()) {
      return;
    }
    // const dataObj = this.getDataForSubmission();
    // dispatch(submitPartialApplication(dataObj));
    dispatch(scrollToSection("personalIncomeDetails"));
  }

  handleToCompanyDetails() {
    const { dispatch } = this.props;
    // const dataObj = this.getDataForSubmission();
    // dispatch(submitPartialApplication(dataObj));
    dispatch(scrollToSection("companyDetails"));
  }

  handleToAccountSetupPage() {
    const { dispatch } = this.props;
    // const dataObj = this.getDataForSubmission();
    // dispatch(submitPartialApplication(dataObj));
    dispatch(scrollToSection("accountSetup"));
  }


  handleToOperatingMandatePage() {
    const { dispatch } = this.props;
    // const dataObj = this.getDataForSubmission();
    // dispatch(submitPartialApplication(dataObj));
    dispatch(scrollToSection("operatingMandate"));
  }

  handleToReviewPage() {
    const { dispatch } = this.props;
    // const dataObj = this.getDataForSubmission();
    // dispatch(submitPartialApplication(dataObj));
    dispatch(scrollToSection("confirmDetailsPage"));
  }

  handleToTaxSelfDeclarationsPage() {
    const { dispatch } = this.props;
    // const dataObj = this.getDataForSubmission();
    // dispatch(submitPartialApplication(dataObj));
    dispatch(scrollToSection("taxSelfDeclarations"));
  }


  handleInitiateApplication() {
    const { dispatch } = this.props;
    const dataObj = this.getDataForInitiation();
    dispatch(submitInitiateApplication(dataObj, () => this.handleToPersonalIncomeDetails()));
  }

  getDataForInitiation() {
    const {
      contactDetailsReducer,
      personalIncomeDetailsReducer
    } = this.props;

    const {
      emailAddress,
      entityType,
      primaryNatureOfBusiness,
      primaryCountryOfOperation
    } = contactDetailsReducer;

    const {
      legalId
    } = personalIncomeDetailsReducer;

    const dataObjBasicDetails = {
      person: [{
        basicInfo: {
          emailAddress: emailAddress.value
        },
      }],
      entity: {
        basicProfile: {
          entitySubType: entityType.value,
          natureOfBusiness: primaryNatureOfBusiness.value,
          primaryCountryOfOperation: primaryCountryOfOperation.value
        }
      }
    }

    return dataObjBasicDetails;
  }

  getDataForSubmission() {
    const { applicationReducer, contactDetailsReducer, personalIncomeDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer } = this.props;
    const { myInfoData } = applicationReducer;
    const { emailAddress, isCommerical, requestLoan, annualTurnover } = contactDetailsReducer;
    const { maritalStatus, passportExpiryDate, passportNumber } = personalIncomeDetailsReducer;
    const { mailingAddressCheckbox, mailingCountry, overseasAddress1, overseasAddress2, overseasAddress3, overseasAddress4, mailingCity } = companyBasicDetailsReducer;
    const { mailingPostalCode, mailingBlock, mailingStreet, mailingBuilding, mailingLevel, mailingUnit } = localAddressInputReducer

    const shareholders = [groupByCode(myInfoData.person, "type")];
    const mainShareholders = shareholders[0]["MAIN"][0];

    const mainBasicDetailsObj = {
      legalId: mainShareholders.basicInfo.legalId,
      legalIdType: mainShareholders.basicInfo.legalIdType,
      legalIdIssueDate: mainShareholders.basicInfo.legalIdIssueDate,
      legalIdExpiryDate: mainShareholders.basicInfo.legalIdExpiryDate,
      legalIdIssueCountry: mainShareholders.basicInfo.legalIdIssueCountry,
      passportNumber: passportNumber.value !== "" ? passportNumber.value : null,
      passportExpiryDate: passportExpiryDate.value !== "" ? moment(passportExpiryDate.value, "DD/MM/YYYY").format("YYYY-MM-DD") : null,
      workPassStatus: mainShareholders.basicInfo.workPassStatus,
      workPassExpiryDate: mainShareholders.basicInfo.workPassExpiryDate,
      alternateNames: mainShareholders.basicInfo.alternateNames,
      residentialStatus: mainShareholders.basicInfo.residentialStatus,
      principalName: mainShareholders.basicInfo.principalName,
      emailAddress: emailAddress.value,
      mobileNumber: mainShareholders.basicInfo.mobileNumber
    }

    const mainPersonDetailsObj = {
      dateOfBirth: mainShareholders.personalInfo.dateOfBirth,
      gender: mainShareholders.personalInfo.gender,
      maritalStatus: maritalStatus.value,
      educationLevel: mainShareholders.personalInfo.educationLevel,
      nationality: mainShareholders.personalInfo.nationality,
      countryOfBirth: mainShareholders.personalInfo.countryOfBirth,
      countryOfResidence: mainShareholders.personalInfo.countryOfResidence,
      race: mainShareholders.personalInfo.race
    }

    const mainObj = {
      type: "MAIN",
      basicInfo: mainBasicDetailsObj,
      personalInfo: mainPersonDetailsObj,
      addresses: mainShareholders.addresses,
      employmentInfo: mainShareholders.employmentInfo,
      declarations: mainShareholders.declarations,
      consents: mainShareholders.consents,
      incomes: mainShareholders.incomes
    }

    let personObj = [mainObj];

    const entityMyinfoObj = myInfoData.entity;
    const countryNotSG = mailingCountry.value !== "SG" ? true : false;
    let withMailingAddress = null;

    if (mailingAddressCheckbox.isToggled) {
      withMailingAddress = {
        type: "M",
        unitNo: !countryNotSG && mailingUnit.value !== "" ? mailingUnit.value : null,
        street: !countryNotSG && mailingStreet.value !== "" ? mailingStreet.value : null,
        block: !countryNotSG && mailingBlock.value !== "" ? mailingBlock.value : null,
        postalCode: !countryNotSG && mailingPostalCode.value !== "" ? mailingPostalCode.value : null,
        floor: !countryNotSG && mailingLevel.value !== "" ? mailingLevel.value : null,
        building: !countryNotSG && mailingBuilding.value !== "" ? mailingBuilding.value : null,
        city: countryNotSG && mailingCity.value !== "" ? mailingCity.value : null,
        country: mailingCountry.value,
        line1: countryNotSG && overseasAddress1.value !== "" ? overseasAddress1.value : null,
        line2: countryNotSG && overseasAddress2.value !== "" ? overseasAddress2.value : null,
        line3: countryNotSG && overseasAddress3.value !== "" ? overseasAddress3.value : null,
        line4: countryNotSG && overseasAddress4.value !== "" ? overseasAddress4.value : null
      }
    }

    const mailingAddressObj = {
      mailingAddress: withMailingAddress
    };

    const withCommerical = {
      requestLoan: isCommerical ? requestLoan.value : null,
      annualTurnover: isCommerical ? annualTurnover.value : null,
    };

    const entityObj = Object.assign({}, entityMyinfoObj, mailingAddressObj, withCommerical);

    const dataObj = {
      person: personObj,
      entity: entityObj
    }
    return dataObj;
  }

  handleSubmitApplication() {
    const { dispatch } = this.props;

    // const dataObj = this.getDataForSubmission();
    // dispatch(submitApplication(dataObj));
  }

  redirectToErrorPage() {
    const { commonReducer, applicationReducer } = this.props;
    localStore.clear();
    this.props.history.push({
      pathname: "error",
      search: "code=" + applicationReducer.errorCode
    })
  }

  isCompanyDetailsPassing() {
    const { companyBasicDetailsReducer, localAddressInputReducer, commonReducer } = this.props;
    const { mailingAddressCheckbox, overseasAddress1, mailingCity, mailingCountry } = companyBasicDetailsReducer;
    const { mailingBlock, mailingLevel, mailingPostalCode, mailingStreet, mailingUnit, annualTurnover } = localAddressInputReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;

    if (!this.checkEmptyFields([annualTurnover], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (!this.checkisValidFields([annualTurnover], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (mailingAddressCheckbox.isToggled) {
      if (mailingCountry.value !== "SG") {
        if (!this.checkEmptyFields([overseasAddress1, mailingCity], requiredMsg, handleErrorMessage)) {
          errorCount++
        }

        if (!this.checkisValidFields([overseasAddress1, mailingCity], requiredMsg, handleErrorMessage)) {
          errorCount++
        }
      } else {
        if (!this.checkEmptyFields([mailingPostalCode, mailingBlock, mailingLevel, mailingStreet, mailingUnit], requiredMsg, handleErrorMessage)) {
          errorCount++
        }

        if (!this.checkisValidFields([mailingPostalCode, mailingBlock, mailingLevel, mailingStreet, mailingUnit], requiredMsg, handleErrorMessage)) {
          errorCount++
        }
      }
    }
    if (errorCount > 0) {
      return false
    }
    return true;
  }



  render() {
    const { commonReducer, applicationReducer } = this.props;
    // const { dataLoaded } = applicationReducer;
    const currentSection = commonReducer.currentSection;
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "confirmDetailsPage", "taxSelfDeclarations"];
    const indexOfSteps = steps.indexOf(currentSection);

    return (
      <WrapContainer {...this.props}>
        {currentSection === "thankyou" ? <ThankYouPage /> :
          <div>
            <TaxSelfDeclarationsPage {...this.props} onContinue={() => this.handleInitiateApplication()}
              onCheck={() => this.isContactDetailsPassChecking()} />
            {indexOfSteps >= 1 &&
              <PersonalIncomeDetailsPage {...this.props} onContinue={() => this.handleToCompanyDetails()} onCheck={() => this.isPersonalIncomeDetailsPassChecking()} />}
            {indexOfSteps >= 2 &&
              <CompanyDetailsPage {...this.props} onContinue={() => this.handleToAccountSetupPage()} onCheck={() => this.isCompanyDetailsPassing()} />}
            {indexOfSteps >= 3 && <AccountSetupPage {...this.props} onContinue={() => this.handleToOperatingMandatePage()}
              onCheck={() => this.isAccountSetupPassChecking()} />}
            {indexOfSteps >= 4 && <OperatingMandatePage {...this.props} onContinue={() => this.handleToTaxSelfDeclarationsPage()} />}
            {indexOfSteps >= 5 && <TaxSelfDeclarationsPage {...this.props} onContinue={() => this.handleToReviewPage()} />}
            {indexOfSteps >= 6 && <ConfirmDetailsPage {...this.props} onContinue={() => null} />}
          </div>
        }
      </WrapContainer>
    );
  }
}

const mapStateToProps = state => {

  const { commonReducer, applicationReducer, contactDetailsReducer, personalIncomeDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer, accountSetupReducer, confirmTandCReducer } = state;
  return {
    commonReducer,
    applicationReducer,
    contactDetailsReducer,
    personalIncomeDetailsReducer,
    companyBasicDetailsReducer,
    localAddressInputReducer,
    accountSetupReducer,
    confirmTandCReducer
  };
};

export default connect(mapStateToProps)(ApplicationContainer);
