import React, { Component } from "react";
import { connect } from "react-redux";
import TextInput from "./../components/TextInput/TextInput";
import Dropdown from "./../components/Dropdown/Dropdown";
import Checkbox from "./../components/Checkbox/Checkbox";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import RadioButton from "./../components/RadioButton/RadioButton";
import { handleIsChecked, handleTextInputChange, handleButtonChange, setDropdownFocusStatus, setDropdownFocusStatusChange, selectDropdownItemChange, selectDropdownItem, handleTextInputChangeOthers, changeSearchInputValueOther, changeSearchInputValue, handleButtonChangeOthers, selectRadioItem, incrementCountry, addNewCountry, uncheckRest, uncheckNone } from "./../actions/taxSelfDeclarationsAction";

import SecondaryButton from "./../components/SecondaryButton/SecondaryButton";
import deleteIcon from "./../assets/images/delete_icon.svg"
import addIcon from "./../assets/images/add_icon.svg"
import { capitalize, mapValueToDescription, mapValueToDescriptionArrayObj, mapValueToDescriptionRiskArrayObj, mapValueToDescriptionRisk } from "../common/utils";

class TaxSelfDeclarationsPage extends Component {
    handleToReviewPage() {
        this.props.onContinue();
    }

    handleRadioSelection(data, key, field) {
        const { dispatch } = this.props;
        dispatch(selectRadioItem(data, key, field));
    }

    handleOnCheckbox(field, isToggled) {
        const { dispatch } = this.props;
        if (field === "checkboxNone") {
            dispatch(uncheckRest());
        } else {
            dispatch(uncheckNone());
        }
        dispatch(handleIsChecked(field, isToggled));
    }

    handleOnSearchChange(e, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValue(value, field));
    }

    handleOnSearchChangeOthers(e, key, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValueOther(value, key, field));
    }

    handleOnChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChange(data, field));
    }

    handleOnTextChangeOthers(data, key, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChangeOthers(data, key, field));
    }

    handleOnButtonChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleButtonChange(data, field));
    }

    handleOnButtonChangeOthers(data, key, field) {
        const { dispatch } = this.props;
        dispatch(handleButtonChangeOthers(data, key, field));
    }

    handleDropdownFocus(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(true, field));
    }

    handleDropdownFocusChange(key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChange(true, key, field));
    }

    handleDropdownBlur(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(false, field));
    }

    handleDropdownBlurChange(key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChange(false, key, field));
    }
    handleDropdownClick(data, field) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        if (taxSelfDeclarationsReducer[field].value === data.value) {
            return;
        }
        dispatch(selectDropdownItem(data.value, data.description, field));
    }

    handleDropdownClickChange(data, key, field) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        if (taxSelfDeclarationsReducer[key][field].value === data.value) {
            return;
        }

        dispatch(selectDropdownItemChange(data.value, data.description, key, field));
    }

    addhaveTINNo(key) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        const tinNoOthersId = taxSelfDeclarationsReducer.tinNoOthersId;
        const maxKey = Math.max.apply(null, tinNoOthersId);
        dispatch(incrementCountry(maxKey));
        dispatch(addNewCountry(maxKey));

    }

    renderCountries() {
        const { taxSelfDeclarationsReducer, commonReducer } = this.props;
        const { tinNoOthersId } = taxSelfDeclarationsReducer;

        //haveTINNo mapping
        const maxKey = Math.max.apply(null, tinNoOthersId);
        const length = tinNoOthersId.length;
        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        const countriesWithRiskMap = inputValues.countriesWithRiskMap;
        const reasonsList = inputValues.reasons;

        const tmpArray = [];
        const errorMsgList = commonReducer.appData.errorMsgs;
        const maxCountries = parseInt(taxSelfDeclarationsValue.maxCountries, 10);

        tinNoOthersId.map((key, i) => {
            tmpArray.push(
                <div key={key} >
                    <div className="country-separator" />
                    <div className="crs-buttons p-t-5">
                        <span className="crs-txt">{labels.haveTINNo}</span>
                        <div>
                            <input type="radio" id={`othersYes${key}`} checked={taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "Y" ? true : false} name={`radioButtonOthers${key}`} value="Y" onChange={(e) => this.handleOnButtonChangeOthers(e.target.value, `tinNoOthers${key}`, "haveTINNo")} />
                            <label className="lbl-first" htmlFor={`othersYes${key}`}>{labels.yesButton}</label>

                            <input type="radio" id={`othersNo${key}`} name={`radioButtonOthers${key}`} value="N" checked={taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "N" ? true : false} onChange={(e) => this.handleOnButtonChangeOthers(e.target.value, `tinNoOthers${key}`, "haveTINNo")} />
                            <label className="lbl-second" htmlFor={`othersNo${key}`}>{labels.noButton}</label>
                        </div>
                    </div>
                    {taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value !== '' &&
                        <div className="top-border bottom-border">
                            <div className="fullTable confirmDetails-flexContainer">
                                <div className="halfRow right-border positionRelative">
                                    <Dropdown
                                        flagImg
                                        inputID={`othersCountry${key}`}
                                        label={labels.othersCountry}
                                        value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value}
                                        errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.errorMsg}
                                        isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.isValid}
                                        isFocus={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.isFocus}
                                        dropdownItems={mapValueToDescriptionRiskArrayObj(
                                            countriesWithRiskMap
                                        )}
                                        searchValue={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.searchValue}
                                        onBlur={this.handleDropdownBlurChange.bind(this, `tinNoOthers${key}`, 'othersCountry')}
                                        onFocus={this.handleDropdownFocusChange.bind(this, `tinNoOthers${key}`, 'othersCountry')}
                                        onClick={data => this.handleDropdownClickChange(data, `tinNoOthers${key}`, 'othersCountry')}
                                        onSearchChange={event =>
                                            this.handleOnSearchChangeOthers(event, `tinNoOthers${key}`, 'othersCountry')
                                        }
                                        validator={["required"]}
                                        isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.isValid} />
                                </div>
                                <div className="halfRow positionRelative">
                                    {taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "Y" &&
                                        <TextInput
                                            inputID={`othersTINNo${key}`}
                                            label={labels.othersTINNo}
                                            value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.value}
                                            errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.errorMsg}
                                            isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.isValid}
                                            onChange={(data) => this.handleOnTextChangeOthers(data, `tinNoOthers${key}`, 'othersTINNo')}
                                            errorMsgList={errorMsgList}
                                            validator={["isAlphanumeric", "maxSize|19"]}
                                            hasIcon={false}
                                        />
                                    }
                                </div>
                            </div>
                        </div>
                    }
                    {taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "N" &&
                        <div>
                            <span className="crs-txt crs-questions">{labels.reason}</span>
                            <RadioButton
                                inputID="reasons"
                                value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.value}
                                radioObj={mapValueToDescription(
                                    reasonsList
                                )}
                                isDisabled={false}
                                errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.errorMsg}
                                isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.isValid}
                                onClick={(data) => this.handleRadioSelection(data, `tinNoOthers${key}`, "reasons")}
                            />
                        </div>
                    }
                    {maxKey === key && tinNoOthersId.length < maxCountries &&
                        <SecondaryButton
                            icon={addIcon}
                            label={"Another Country"}
                            onClick={() => this.addhaveTINNo(key)}
                        />
                    }
                </div>
            );
            return tmpArray;
        })
        return tmpArray;
    }

    checkOthersValid() {
        const { taxSelfDeclarationsReducer } = this.props;
        const { tinNoOthersId } = taxSelfDeclarationsReducer;
        let errorCount = 0;
        if (tinNoOthersId.length > 0) {
            taxSelfDeclarationsReducer.tinNoOthersId.map((key) => {
                const haveTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo;
                const othersCountry = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry;
                const othersTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo;
                const reasons = taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons;

                const checkTINNOYes = othersCountry.value !== "" && othersTINNo.value !== "" ? true : false;
                const checkTINNONo = othersCountry.value !== "" && reasons.value !== "" ? true : false;

                if (haveTINNo.value !== "") {
                    if (haveTINNo.value === "Y") {
                        if (!checkTINNOYes) {
                            errorCount++;
                        }
                    } else {
                        if (!checkTINNONo) {
                            errorCount++;
                        }
                    }
                } else {
                    errorCount++
                }
                return errorCount;
            })

            return errorCount;
        }
    }

    render() {
        const { commonReducer, taxSelfDeclarationsReducer } = this.props;
        const { checkboxSG, checkboxUS, checkboxOthers, checkboxNone, tinNoSG, tinNoUS, countryNone, isUSResident } = taxSelfDeclarationsReducer;
        //Lists
        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";
        const errorMsgList = commonReducer.appData.errorMsgs;
        const countriesNamesMap = inputValues.countriesNamesMap;


        //Values
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;

        //Validation

        const if1Checked = checkboxNone.isToggled || checkboxOthers.isToggled || checkboxSG.isToggled || checkboxUS.isToggled;

        let errorCount = 0;
        if (if1Checked) {
            if (checkboxSG.isToggled) {
                if (!(tinNoSG.value !== "" && tinNoSG.isValid)) {
                    errorCount++;
                }
            }
            if (checkboxUS.isToggled) {
                if (!(tinNoUS.value !== "" && tinNoUS.isValid && isUSResident.value !== "" && isUSResident.isValid)) {
                    errorCount++;
                }
            }
            if (checkboxOthers.isToggled) {
                if (!(this.checkOthersValid() === 0)) {
                    errorCount++;
                }
            }
            if (checkboxNone.isToggled) {
                if (!(countryNone.value !== "" && countryNone.isValid)) {
                    errorCount++;
                }
            }
        } else {
            errorCount++;
        }

        const fullValidation = errorCount > 0 ? false : true;

        return (
            <div className="uob-content" id="taxSelfDeclaration-section">
                <h1>{taxSelfDeclarationsValue.title}</h1>
                <p className="uob-headline">
                    {taxSelfDeclarationsValue.headline}
                </p>
                <h1 className="sub-title">{taxSelfDeclarationsValue.subtitle}</h1>
                <p className="sub-headline">{taxSelfDeclarationsValue.subHeadline}</p>
                <Checkbox
                    inputID="checkboxSG"
                    description={labels.checkboxSG}
                    isChecked={checkboxSG.isToggled}
                    onClick={() => this.handleOnCheckbox('checkboxSG', checkboxSG.isToggled)}
                />
                {checkboxSG.isToggled &&
                    <div className="crs-container">
                        <div className="sub-div-common bottom-border">
                            <div className="p-l-10 positionRelative">
                                <TextInput
                                    inputID="tinNoSG"
                                    label={labels.tinNoSG}
                                    value={tinNoSG.value}
                                    errorMsg={tinNoSG.errorMsg}
                                    onChange={data => this.handleOnChange(data, "tinNoSG")}
                                    isValid={tinNoSG.isValid}
                                    validator={["isAlphanumeric", "maxSize|19"]}
                                    errorMsgList={errorMsgList}
                                    hasIcon={false}
                                />
                            </div>
                        </div>
                    </div>}
                <Checkbox
                    inputID="checkboxUS"
                    description={labels.checkboxUS}
                    isChecked={checkboxUS.isToggled}
                    onClick={() => this.handleOnCheckbox('checkboxUS', checkboxUS.isToggled)}
                />
                {checkboxUS.isToggled &&
                    <div className="crs-container">
                        <div className="sub-div-common bottom-border">
                            <div className="p-l-10 positionRelative">
                                <TextInput
                                    inputID="tinNoUS"
                                    label={labels.tinNoUS}
                                    value={tinNoUS.value}
                                    errorMsg={tinNoUS.errorMsg}
                                    onChange={data => this.handleOnChange(data, "tinNoUS")}
                                    isValid={tinNoUS.isValid}
                                    validator={["isAlphanumericSymbol", "maxSize|19"]}
                                    errorMsgList={errorMsgList}
                                    hasIcon={false}
                                />
                            </div>
                        </div>
                        <div className="crs-buttons">
                            <span className="crs-txt">{labels.isUSResident}</span>
                            <div>
                                <input type="radio" id="usYes" name="radioButtonUS" value="Y" checked={isUSResident.value === "Y" ? true : false} onChange={(e) => this.handleOnButtonChange(e.target.value, "isUSResident")} />
                                <label className="lbl-first" htmlFor="usYes">{labels.yesButton}</label>

                                <input type="radio" id="usNo" name="radioButtonUS" value="N" checked={isUSResident.value === "N" ? true : false} onChange={(e) => this.handleOnButtonChange(e.target.value, "isUSResident")} />
                                <label className="lbl-second" htmlFor="usNo">{labels.noButton}</label>
                            </div>
                        </div>
                    </div>}
                <Checkbox
                    inputID="checkboxOthers"
                    description={labels.checkboxOthers}
                    isChecked={checkboxOthers.isToggled}
                    onClick={() => this.handleOnCheckbox('checkboxOthers', checkboxOthers.isToggled)}
                />
                {
                    checkboxOthers.isToggled &&
                    <div className="crs-container">
                        {this.renderCountries()}
                    </div>

                }
                <Checkbox
                    inputID="checkboxNone"
                    description={labels.checkboxNone}
                    isChecked={checkboxNone.isToggled}
                    onClick={() => this.handleOnCheckbox('checkboxNone', checkboxNone.isToggled)}
                />

                {checkboxNone.isToggled &&
                    <div className="crs-container">
                        <span className="crs-txt float-none">{labels.none}</span>
                        <div className="p-t-20">
                            <div className="top-border bottom-border">
                                <div className="fullTable confirmDetails-flexContainer">
                                    <div className="halfRow right-border positionRelative">
                                        <Dropdown
                                            inputID="countryNone"
                                            label={labels.countryNone}
                                            dropdownItems={mapValueToDescription(
                                                countriesNamesMap
                                            )}
                                            isFocus={countryNone.isFocus}
                                            value={countryNone.value}
                                            isValid={countryNone.isValid}
                                            errorMsg={countryNone.errorMsg}
                                            focusOutItem={true}
                                            searchValue={countryNone.searchValue}
                                            onBlur={this.handleDropdownBlur.bind(this, "countryNone")}
                                            onFocus={this.handleDropdownFocus.bind(this, "countryNone")}
                                            onClick={data => this.handleDropdownClick(data, "countryNone")}
                                            onSearchChange={event =>
                                                this.handleOnSearchChange(event, "countryNone")
                                            }
                                            flagImg
                                        />
                                    </div>
                                    <div className="halfRow positionRelative">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }

                <div className="uob-input-separator fixedContinue">
                    <PrimaryButton label={labels.continueButton} onClick={() => this.handleToReviewPage()} isLoading={commonReducer.isProcessing} />
                </div>

                {
                    !fullValidation &&
                    <div>
                        <div className="uob-input-separator fixedContinue fixedContinueGray">
                            <PrimaryButton label={labels.continueButton} onClick={() => this.isContactDetailsPassChecking()} isLoading={commonReducer.isProcessing} />
                        </div>
                    </div>
                }

            </div >
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer } = state;
    return { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer };
};

export default connect(mapStateToProps)(TaxSelfDeclarationsPage);
