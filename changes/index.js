import { combineReducers } from 'redux';
import commonReducer from './commonReducer';
import localAddressInputReducer from './localAddressInputReducer';
import contactDetailsReducer from './contactDetailsReducer';
import accountSetupReducer from './accountSetupReducer';
import personalIncomeDetailsReducer from './personalIncomeDetailsReducer';
import companyBasicDetailsReducer from './companyBasicDetailsReducer';
import companyOverallDetailsReducer from './companyOverallDetailsReducer';
import askQuestionsReducer from './askQuestionsReducer';
import operatingMandateReducer from './operatingMandateReducer';
import retrievingReducer from './retrievingReducer';
import applicationReducer from './applicationReducer';
import confirmTandCReducer from './confirmTandCReducer';
import taxSelfDeclarationsReducer from './taxSelfDeclarationsReducer';

export default combineReducers({
    commonReducer,
    localAddressInputReducer,
    contactDetailsReducer,
    personalIncomeDetailsReducer,
    companyBasicDetailsReducer,
    companyOverallDetailsReducer,
    accountSetupReducer,
    askQuestionsReducer,
    operatingMandateReducer,
    retrievingReducer,
    applicationReducer,
    confirmTandCReducer,
    taxSelfDeclarationsReducer
})
