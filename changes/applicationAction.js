import * as types from "./actionTypes";
import { Application, LocalEnvironment, Config } from "./../api/httpApi";
import { groupByCode } from "../common/utils";
import { setMyInfoFlowStatus, setProcessingStatus, setErrorMessage, setRetrievingMyInfo, setApplicationReferenceNo, setCurrentSection, setLoadingStatus } from "./commonAction";
import { setMyInfoContactDetailsData } from "./contactDetailsAction";
import { setMyInfoPersonalIncomeDetailsData, setIncomeDetailData } from "./personalIncomeDetailsAction";
import { setResidentialAddress, setCompanyAddressData } from "./localAddressInputAction";
import { setCompanyBasicData, setCompanyUnformattedAddress, setCompanyAddressType } from "./companyBasicDetailsAction";
import { setCompanyPreviousNamesData, setCompanyPreviousUENData, setCompanyCapitalsData, setCompanyAppointmentsData, setCompanyShareholdersData, setCompanyFinancialsData, setCompanyGrantsData } from "./companyOverallDetailsAction";
import { setOperatingMandateData } from "./operatingMandateAction";
import { setCompanyUEN } from "./taxSelfDeclarationsAction";
import { setAccountSetupData } from "./accountSetupAction";
import localStore from "./../common/localStore";

//Retrieve MY Info
export const retrieveMyInfoDetails = (redirectToErrorPage) => {
    return (dispatch, getState) => {
        const response = getState().retrievingReducer.myInfoData;
        dispatch(setRetrievingMyInfo(true));
        if (response !== null) {
            Promise.resolve().then(() => {
                // const shareholders = response.person ? [groupByCode(response.person, "type")] : null;
                const companyDetails = response.entity;
                const residentialAddress = response.person[0].addresses[0];
                // console.log(shareholders);
                dispatch(setMyInfoContactDetailsData(response.person, companyDetails));
                dispatch(setMyInfoPersonalIncomeDetailsData(response.person, residentialAddress));
                dispatch(setIncomeDetailData(response.person));
                dispatch(setResidentialAddress(residentialAddress))
                dispatch(setCompanyBasicData(response.entity.basicProfile));
                dispatch(setCompanyUEN(response.taxDeclarations.crs[0].tin))
                dispatch(setCompanyPreviousNamesData(response.entity.previousNames));
                dispatch(setCompanyPreviousUENData(response.entity.previousUens));
                dispatch(setCompanyCapitalsData(response.entity.capitals));
                dispatch(setCompanyAppointmentsData(response.entity.appointments));
                dispatch(setCompanyShareholdersData(response.entity.shareholders));
                dispatch(setCompanyFinancialsData(response.entity.financials));
                dispatch(setCompanyGrantsData(response.entity.grants));
                dispatch(setCompanyAddressType(response.entity.addresses[0].standard));
                dispatch(setCompanyAddressData(response.entity.addresses[0]));
                if (response.entity.addresses[0].line1 && response.entity.addresses[0].line2) {
                    dispatch(setCompanyAddressData(response.entity.addresses[0]));
                } else {
                    dispatch(setCompanyUnformattedAddress(response.entity.addresses[0]));
                }
                // dispatch(setOperatingMandateData(response.operatingMandate.approvedSignatories, shareholders[0]["MAIN"]));

            }).then(() => {
                dispatch(setRetrievingMyInfo(false));
                dispatch(setApplicationDataLoaded());
                dispatch(setMyInfoFlowStatus(true));
                localStore.clear();
            });
        } else {
            Promise.resolve().then(() => {
                dispatch(setErrorWithValue("NOREF"));
            }).then(() => {
                redirectToErrorPage && redirectToErrorPage();
            });
        }
    };
};

export const setApplicationDataLoaded = () => {
    return {
        type: types.SET_APPLICATION_DATA_LOADED
    };
};

//Initiate Application
export const submitInitiateApplication = (dataObj, handleToPersonalIncomeDetails) => {
    return (dispatch, getState) => {
        const errorMsg = getState().commonReducer.appData.globalErrors.apiException;
        const parameter = getState().applicationReducer.startingParameter;
        dispatch(setProcessingStatus(true));
        if (LocalEnvironment) {
            Config.getInitiateMyinfoData().then(response => {
                Promise.resolve()
                    .then(() => {
                        const operatingMandate = response.operatingMandate;
                        const mainApplicant = response.person[0];
                        dispatch(setProcessingStatus(false))
                        dispatch(setInitateMyInfoResponse(response));
                        // dispatch(setOperatingMandateData(response.entity.basicProfile.entityType, operatingMandate.approvedSignatories, mainApplicant));
                        dispatch(setAccountSetupData(response.accountSetup, response.entity))
                    }).then(() => {
                        handleToPersonalIncomeDetails && handleToPersonalIncomeDetails();
                    })
            }).catch(e => {
                dispatch(setProcessingStatus(false))
                dispatch(setErrorMessage(errorMsg));
            })
        } else {
            Application.submitInitiateApplication(dataObj, parameter).then(response => {
                const data = response.data;
                Promise.resolve()
                    .then(() => {
                        const operatingMandate = data.operatingMandate;
                        dispatch(setProcessingStatus(false))
                        dispatch(setInitateMyInfoResponse(data));
                        // dispatch(setOperatingMandateData(response.entity.basicProfile.entityType, operatingMandate.approvedSignatories));
                        dispatch(setAccountSetupData(data.accountSetup, data.entity))
                    }).then(() => {
                        handleToPersonalIncomeDetails && handleToPersonalIncomeDetails();
                    })
            }).catch(e => {
                dispatch(setProcessingStatus(false))
                dispatch(setErrorMessage(errorMsg));
            })
        }
    };
};

//Save Application
export const submitPartialApplication = (dataObj) => {
    return dispatch => {
        Application.submitPartialApplication(dataObj);
    };
};

export const setMyInfoResponse = (response) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_MYINFO_RESPONSES,
            response
        })
    }
}

export const setInitateMyInfoResponse = (response) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_INITIATE_MYINFO_RESPONSES,
            response
        })
    }
}

export const submitApplication = (dataObj, applicationId, redirectToErrorPage) => {
    return (dispatch, getState) => {
        dispatch(setLoadingStatus(true));
        Application.submitApplication(dataObj).then(response => {
            const referenceNo = response.data.referenceNumber;
            dispatch(setApplicationReferenceNo(referenceNo));
            dispatch(setLoadingStatus(false));
            if (referenceNo !== null && referenceNo !== undefined && referenceNo !== '') {
                dispatch(setCurrentSection('thankyou'));
            } else {
                dispatch(setErrorWithValue("NOREF"));
                redirectToErrorPage && redirectToErrorPage();
            }
        }).catch(e => {
            dispatch(setLoadingStatus(false));
        })
    }
}

export const setErrorWithValue = (errorCode) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ERROR_MESSAGE_WITH_ERRORCODE,
            errorCode
        })
    }
}

export const storeParameter = (parameter) => {
    return (dispatch) => {
        dispatch({
            type: types.STORE_PARAMETER,
            parameter
        })
    }
}
