// company basic details (basic details, registered address, mailing address)
// company overall details (table view and review grants)
import React, { Component } from "react";
import { connect } from "react-redux";
import CompanyBasicDetails from "./CompanyBasicDetails";
import CompanyOverallDetails from "./CompanyOverallDetails";
import AskQuestionPopup from "./AskQuestionPopup";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import { handleAskQuestionPopup } from "./../actions/askQuestionsAction";

class CompanyDetailsPage extends Component {

	handlePopup(popuptoggle, questionObj) {
		const { dispatch } = this.props;
		dispatch(handleAskQuestionPopup(popuptoggle, questionObj));
	}

	handleToAdditionalPartnerDetailsPage() {
		this.props.onContinue();
	}

	isCompanyDetailsPassing() {
		this.props.onCheck()
	}

	checkCompanyBasicDetailValid() {
		const { companyBasicDetailsReducer, localAddressInputReducer } = this.props;
		const { mailingAddressCheckbox, overseasAddress1, mailingCity, mailingCountry, annualTurnover } = companyBasicDetailsReducer;
		const { mailingBlock, mailingLevel, mailingPostalCode, mailingStreet, mailingUnit } = localAddressInputReducer;
		let errorCount = 0;

		if (annualTurnover.value === "" || !annualTurnover.isValid) {
			errorCount++;
		}

		if (mailingAddressCheckbox.isToggled) {
			if (mailingCountry.value !== "SG") {
				if (mailingCity.value === "" || !mailingCity.isValid || overseasAddress1.value === "" || !overseasAddress1.isValid) {
					errorCount++;
				}
			} else {
				if (mailingBlock.value === "" ||
					mailingLevel.value === "" ||
					mailingPostalCode.value === "" ||
					mailingStreet.value === "" ||
					mailingUnit.value === "" ||
					!mailingBlock.isValid ||
					!mailingLevel.isValid ||
					!mailingPostalCode.isValid ||
					!mailingStreet.isValid ||
					!mailingUnit.isValid) {
					errorCount++
				}
			}
		}
		return errorCount;
	}

	render() {
		const { commonReducer, askQuestionsReducer } = this.props;
		const { popuptoggle } = askQuestionsReducer;
		const labels = commonReducer.appData.companyDetails.labels;
		const askQuestionsValue = commonReducer.appData.askQuestion;
		const questionnaires = askQuestionsValue.questions;
		return (
			<div id="companyDetails-section">
				<CompanyBasicDetails {...this.props} />
				<CompanyOverallDetails {...this.props} />
				<div className="uob-input-separator" />
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => { this.handlePopup(true, questionnaires) }} />
				</div>

				{popuptoggle &&
					<AskQuestionPopup {...this.props} />
				}

				{
					this.checkCompanyBasicDetailValid() > 0 &&
					<div className="uob-input-separator fixedContinue fixedContinueGray">
						<PrimaryButton label={labels.continueButton} onClick={() => this.isCompanyDetailsPassing()} />
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, companyBasicDetailsReducer, localAddressInputReducer, askQuestionsReducer } = state;
	return { commonReducer, companyBasicDetailsReducer, localAddressInputReducer, askQuestionsReducer };
};

export default connect(mapStateToProps)(CompanyDetailsPage);
