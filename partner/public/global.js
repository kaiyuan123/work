// eslint-disable-next-line no-unused-vars
const GLOBAL_CONFIG = {
	API_PATH: "/bb/api/v1/liability",
  COMMON_API_PATH: "/api/sg/v1",
	CALLBACK_API_PATH: "/bb/api/v1/liability/myinfo/person",
  SPEC_API_PATH: "/bb/api/v1/liability/partner/specifications",
	SING_PASS_URL:  "./dummysingpass.html?redirect_uri={redirect_uri}&state=S4610928T&code=ffddfdf&scope=attributes&client_id=test&client_id=STG-193500026Z-UOB-SMELOAN&purpose=opening%20an%20account%20online%20with%20UOB&attributes=basic-profile,previous-names,previous-uens,addresses,financials,capitals,appointments,shareholders,grants,aliasname,assessableincome,assessyear,birthcountry,cpfcontributions,dob,edulevel,email,hanyupinyinaliasname,hanyupinyinname,hdbtype,housingtype,marriedname,marital,mobileno,name,nationality,race,regadd,residentialstatus,sex",
	formConfig: "./liability.json",
	myinfoDataJson: "./myinfoData.json",
	initiateMyinfoDataJson: "./initiateJsonFile.json",
	timeout: "45000",
	Local_Environment: true,
	MY_INFO_BUSINESS_URI: "/businessbanking"
};
