import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ApplicationContainer from './../containers/ApplicationContainer';
import ErrorContainer from './../containers/ErrorContainer';
import ApplyContainer from './../containers/ApplyContainer';
import RetrievingContainer from './../containers/RetrievingContainer';
import VerifyContainer from './../containers/VerifyContainer';

export const detectPathName = () => {
	const router = ["verification", "apply", "application", "error"];
	let pn = window.location.pathname;
	return pn.replace(router.filter(a => pn.indexOf(a) !== -1)[0], "");
};
const configureRoutes = () => (
	<main>
		<Switch>
			<Route exact path={`${detectPathName()}`}
				component={RetrievingContainer} />
			<Route exact path={`${detectPathName()}verification`} component={VerifyContainer} />
			<Route exact path={`${detectPathName()}apply`} component={ApplyContainer} />
			<Route path={`${detectPathName()}error`} component={ErrorContainer} />
			<Route exact path={`${detectPathName()}application`} component={ApplicationContainer} />
		</Switch>
	</main>
);

export default configureRoutes;
