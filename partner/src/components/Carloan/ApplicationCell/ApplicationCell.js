import React from 'react';
import PropTypes from 'prop-types';

import './ApplicationCell.css';

import uploadImg from './../../../assets/images/carloan/upload.svg';

const getDescriptionByCode = (code, list) => {
  const item = list.find(x => x.value === code);
  return item ? item.description : '';
}

const ApplicationCell = props => {
  const { onClick, style, containerStyle, item, statusList } = props;
  const appRefText = item.customerName ? `${item.appRefNum} - ${item.customerName}` : item.appRefNum;
  return (
    <div className='application-cell-contain' style={containerStyle ? containerStyle : null} onClick={ onClick && onClick }>
      <div className='application-cell-left'>
        <div className='application-cell-label'>{getDescriptionByCode(item.appStatus, statusList)}</div>
        <div className='application-cell-text'>{appRefText}</div>
      </div>
      <img src={uploadImg} alt='Upload' className='application-cell-right' />
    </div>
  );
};

ApplicationCell.propTypes = {
    onClick: PropTypes.func,
    style: PropTypes.object,
    containerStyle: PropTypes.object
};

export default ApplicationCell;
