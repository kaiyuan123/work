import React from 'react';
import PropTypes from 'prop-types';

import './ReadOnlyDropdown.css';

// import nonEditableImg from './../../../assets/images/non-editable-icon4.svg';

const ReadOnlyDropdown = props => {
	const { label, value, dropdownItems, flagImg, currency, flagImgName, currencyDescription } = props;
	const mapValueToLabel = (arrayOfItems, value) => {
		const item = arrayOfItems.find(x => x.value === value);
		return item ? item.description : value;
	};
	let countryFlagPath = `./images/countriesFlags/${currency ? flagImgName : value.toLowerCase()}.svg`;
	const withFlag = flagImg ? "--withFlag" : "";
	return (
		<div className='ready-only-dropdown'>
			<div className='dropdown--vertical-container' tabIndex='1'>
				<div className={"ready-only-dropdown-container--focused"}>
					<div className='ready-only-dropdown--content'>
						<div className='ready-only-dropdown--content-left'>
							<div className={"ready-only-dropdown--inputLabel--focused"}>
								{`${label}`}
							</div>
							<div className={`read-only-input-field`}>
								{flagImg ? <img className={"dropdown-flagImg "} onError={(e) => e.target.style.display = 'none'} src={countryFlagPath} alt='icon' /> : null}
								<div className={`ready-only-dropdown--value-text${withFlag}`}>
									{currency ? currencyDescription : mapValueToLabel(dropdownItems, value)}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

ReadOnlyDropdown.propTypes = {
	label: PropTypes.string.isRequired,
	value: PropTypes.string.isRequired,
	dropdownItems: PropTypes.array.isRequired
};

ReadOnlyDropdown.defaultProps = {
	dropdownItems: [],
	label: 'Label',
	value: ''
};

export default ReadOnlyDropdown;
