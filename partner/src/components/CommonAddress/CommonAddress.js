import React, { Component } from "react";
import { connect } from "react-redux";

let flagImg = `./images/countriesFlags/sg.svg`;

export class CommonAddress extends Component {
  render() {
    return (
      <div>
        <div className="top-border">
          <div className="fullTable confirmDetails-flexContainer">
            <div className="right-border halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS">Block</div>
                  <div className="confirmDetails-ValueCSS">345</div>
                </div>
              </div>
            </div>
            <div className="halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS">Building Name</div>
                  <div className="confirmDetails-ValueCSS">Alexandra Road</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="top-border">
          <div className="fullTable confirmDetails-flexContainer">
            <div className="right-border halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS">Floor</div>
                  <div className="confirmDetails-ValueCSS">06</div>
                </div>
              </div>
            </div>
            <div className="halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS">Unit Number</div>
                  <div className="confirmDetails-ValueCSS">543</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="top-border">
          <div className="fullTable confirmDetails-flexContainer">
            <div className="right-border halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS">Street Name</div>
                  <div className="confirmDetails-ValueCSS">Raffles Street</div>
                </div>
              </div>
            </div>
            <div className="halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS">Postal Code</div>
                  <div className="confirmDetails-ValueCSS">765 432</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="sub-div-common display-flex bottom-border">
          <div className="p-l-10">
            <div className="confirmDetails-label-container ">
              <div className="confirmDetails-labelValue">
                <div className="confirmDetails-LabelCSS">Country</div>
                <div className="confirmDetails-ValueCSS">
                  <div>
                    <table>
                      <tbody>
                        <tr>
                          <td>
                            <img
                              className={"phoneInputFlag"}
                              src={flagImg}
                              alt="flag"
                            />
                          </td>
                          <td>&nbsp;Singapore</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { commonReducer } = state;
  return { commonReducer };
};

export default connect(mapStateToProps)(CommonAddress);
