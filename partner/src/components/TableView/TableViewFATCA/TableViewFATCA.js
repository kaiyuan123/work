import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import "./TableViewFATCA.css";
import { selectStatus } from "./../../../actions/taxSelfDeclarationsAction";


class TableViewFATCA extends Component {

  handleOnClick(value, field) {
    const { dispatch } = this.props;
    dispatch(selectStatus(value, field));
  }

  render() {
    const { taxSelfDeclarationsReducer, labels, isChecked, isDisabled, colTitles, inputID } = this.props;
    const { fatcaCRSStatus } = taxSelfDeclarationsReducer;
    const checkAndDisabled = isChecked && isDisabled ? "disabledCheckRadio" : '';
    const notCheckedDisabled = !isChecked && isDisabled ? "disabledText" : '';
    const bgChecked = fatcaCRSStatus.value === "AB" ? "bg-checked" : "";
    const bgCheckedPassive = fatcaCRSStatus.value === "PB" ? "bg-checked" : "";

    return (
      <div className="income-breakdown-table--main-container-flex-fatca new-table" id={inputID}>
        <div className="income-breakdown-table--titles-container-flex-fatca bg-none">
          <div className="income-breakdown-table--title-flex-fatca-70">
            {colTitles[0]}
          </div>
          <div className="income-breakdown-table--title-flex-fatca-15" dangerouslySetInnerHTML={{ __html: colTitles[1] }} />
          <div className="income-breakdown-table--title-flex-fatca-15">
            {colTitles[2]}
          </div>
        </div>
        <div className="income-breakdown-table--rows-container-flex-fatca">
          <div className={`income-breakdown-table--row-flex-fatca ${bgChecked}`}>
            <div className="income-breakdown-table--row-item-fatca">
              <div className="fatca-radio-input">
                <label className="radio-container" style={{ marginBottom: "0" }}>
                  <input type="radio" name={`${inputID}-radio`} className="radioButton"
                    onClick={(e) => this.handleOnClick(e.target.value, inputID)}
                    checked={fatcaCRSStatus.value === "AB" ? true : false}
                    value={"AB"}
                    disabled={true}
                  />
                  <span className={`radio-description ${notCheckedDisabled}`}>
                    <p className="fatca-header">{labels.activeBusiness}</p>
                    <p className="fatca-desc" dangerouslySetInnerHTML={{ __html: labels.activeBusinessDesc }} />
                  </span>
                  <span style={{ top: "2px" }} className={`radio-checkmark ${checkAndDisabled}`}></span>
                  <div className="mobile-content">
                    <div>
                      <span className="fat-status">{colTitles[1].split("<br/>")[0]}</span>
                      <span className="fatca-desc">({colTitles[1].split("<br/>")[1]})</span>
                      <p className="fatca-desc">{labels.activeNonFinancialFE}</p>
                    </div>
                    <div>
                      <span className="fat-status">{colTitles[2]}</span>
                      <p className="fatca-desc">{labels.activeNonFinancialEntityANFE}</p>
                    </div>
                  </div>
                </label>
              </div>
            </div>
            <div className={`income-breakdown-table--row-item-fatca ${bgChecked} mob-hide`}>{labels.activeNonFinancialFE}</div>
            <div className={`income-breakdown-table--row-item-fatca ${bgChecked} mob-hide`} dangerouslySetInnerHTML={{ __html: labels.activeNonFinancialEntityANFE }} />
          </div>
          <div className={`income-breakdown-table--row-flex-fatca ${bgCheckedPassive}`}>
            <div className="income-breakdown-table--row-item-fatca">
              <div className="fatca-radio-input">
                <label className="radio-container" style={{ marginBottom: "0" }}>
                  <input type="radio" name={`${inputID}-radio`} className="radioButton"
                    onClick={(e) => this.handleOnClick(e.target.value, inputID)}
                    checked={fatcaCRSStatus.value === "PB" ? true : false}
                    value={"PB"}
                    disabled={true}
                  />
                  <span className={`radio-description ${notCheckedDisabled}`}>
                    <p className="fatca-header">{labels.passiveBusiness}</p>
                    <p className="fatca-desc" dangerouslySetInnerHTML={{ __html: labels.passiveBusinessDesc }} />
                  </span>
                  <span style={{ top: "2px" }} className={`radio-checkmark ${checkAndDisabled}`}></span>
                  <div className="mobile-content">
                    <div>
                      <span className="fat-status">{colTitles[1].split("<br/>")[0]}</span>
                      <span className="fatca-desc">({colTitles[1].split("<br/>")[1]})</span>
                      <p className="fatca-desc">{labels.passiveNonFinancialFE}</p>
                    </div>
                    <div>
                      <span className="fat-status">{colTitles[2]}</span>
                      <p className="fatca-desc">{labels.passiveNonFinancialEntity}</p>
                    </div>
                  </div>
                </label>
              </div>
            </div>
            <div className={`income-breakdown-table--row-item-fatca ${bgCheckedPassive} mob-hide`}>{labels.passiveNonFinancialFE}</div>
            <div className={`income-breakdown-table--row-item-fatca ${bgCheckedPassive} mob-hide`}>{labels.passiveNonFinancialEntity}</div>
          </div>
        </div>
      </div>
    );
  }
};

TableViewFATCA.propTypes = {
  labels: PropTypes.object.isRequired,
  colTitles: PropTypes.array.isRequired
}

TableViewFATCA.defaultProps = {
};

const mapStateToProps = (state) => {
  const { taxSelfDeclarationsReducer } = state;
  return { taxSelfDeclarationsReducer };
}

export default connect(mapStateToProps)(TableViewFATCA);
