import React from "react";
import moment from "moment";
import "./TableViewCompany.css";
import NumberFormat from 'react-number-format';

const generateTableRows = (tableContent, type, mapItems) => {

  return tableContent.map((row, i) => {
    switch (type) {

      case 'particularsOfPersons':
        let positionList = mapItems.position;

        //check if position code exists, else show position desc
        const rowPosDisplay = row.positionCode !== null ? positionList[row.positionCode] : row.positionDesc !== null ? row.positionDesc : "-";

        return (
          <div key={i} className="income-breakdown-table--row-flex">
            <div className="income-breakdown-table--row-item">{row.name}</div>
            <div className="income-breakdown-table--row-item">{row.legalId}</div>
            <div className="income-breakdown-table--row-item">{rowPosDisplay}</div>
          </div>
        );

      case 'previousCompanyData':
        return (
          <div key={i} className="income-breakdown-table--row-flex">
            <div className="income-breakdown-table--row-item">{mapItems[i] && mapItems[i].previousUen !== "" && mapItems[i].previousUen !== null ? mapItems[i].previousUen : "-"}</div>
            <div className="income-breakdown-table--row-item">{row.historyName &&
              row.historyName !== "" && row.historyName !== null ? row.historyName : "-"}</div>
            <div className="income-breakdown-table--row-item">
              {row.historyNameEffectiveDate && row.historyNameEffectiveDate !== null && row.historyNameEffectiveDate !== "" && moment(row.historyNameEffectiveDate, "YYYY-MM-DD").isValid() ? moment(row.historyNameEffectiveDate, "YYYY-MM-DD").format("DD/MM/YYYY") : "-"}
            </div>
          </div>
        );

      case 'companyCapitalsData':
        const capitalTypeList = mapItems;
        return (
          <div key={i} className="income-breakdown-table--row-flex">
            <div className="income-breakdown-table--row-item">{row.capitalType && capitalTypeList[row.capitalType] !== undefined ? capitalTypeList[row.capitalType] : "-"}</div>
            <div className="income-breakdown-table--row-item">
              <table className="currencyContent">
                <tbody>
                  <tr>
                    <td className="currencyAmount">
                      {row.shareAllottedAmount ?
                        <NumberFormat value={row.shareAllottedAmount}
                          displayType={'text'}
                          thousandSeparator={true}
                        /> : "-"}
                    </td>
                    <td className="currencyType">
                      {row.currency && row.currency !== "" && row.currency !== null ? row.currency : ""}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="income-breakdown-table--row-item">
              <table className="currencyContent">
                <tbody>
                  <tr>
                    <td className="currencyAmount">
                      {row.issuedCapitalAmount ?
                        <NumberFormat value={row.issuedCapitalAmount}
                          displayType={'text'}
                          thousandSeparator={true}
                        />
                        : "-"}
                    </td>
                    <td className="currencyType">
                      {row.currency && row.currency !== "" && row.currency !== null ? row.currency : ""}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="income-breakdown-table--row-item">
              <table className="currencyContent">
                <tbody>
                  <tr>
                    <td className="currencyAmount">
                      {row.paidUpCapitalAmount ?
                        <NumberFormat value={row.paidUpCapitalAmount}
                          displayType={'text'}
                          thousandSeparator={true}
                        /> : "-"}
                    </td>
                    <td className="currencyType">
                      {row.currency && row.currency !== "" && row.currency !== null ? row.currency : ""}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        );

      case 'appointmentsData':
        const appointmentCategoryList = mapItems.appointmentCategory;
        positionList = mapItems.position;
        const nationality = mapItems.nationality;

        //Check if either or both exist, return "-" on null
        const personReferenceRow = row.personReference ? row.personReference : false;
        const entityReferenceRow = row.entityReference ? row.entityReference : false;

        //check name
        const rowName = personReferenceRow ? personReferenceRow.personName : entityReferenceRow ? entityReferenceRow.entityName : null;
        //check id
        const rowIdNo = personReferenceRow ? personReferenceRow.idno : entityReferenceRow ? entityReferenceRow.uen : null;
        //check nationality for person
        const rowNationality = personReferenceRow && nationality[personReferenceRow.nationality] !== undefined ? nationality[personReferenceRow.nationality] : "-";

        //check if empty
        const name = rowName !== null && rowName !== "" ? rowName : "-";
        const idNo = rowIdNo !== null && rowIdNo !== "" ? rowIdNo : "-";

        //check if position code exists, else show position desc
        const rowAppPosDisplay = row.positionCode !== null && row.positionCode !== "" ? positionList[row.positionCode] : row.positionDesc !== null ? row.positionDesc : "-";

        return (
          <div key={i} className="income-breakdown-table--row-flex">
            <div className="income-breakdown-table--row-item custom-tooltip-tb">
              <span className="font-14 tooltip-tb">
                <div className="row-txt">{name}</div>
                {name !== "-" && <div className="tooltiptext-tb">{name}</div>}
              </span>
            </div>
            <div className="income-breakdown-table--row-item">{idNo}</div>
            <div className="income-breakdown-table--row-item custom-tooltip-tb">
              <span className="font-14 tooltip-tb">
                <div className="row-txt">{rowNationality}</div>
                {rowNationality !== "-" && <div className="tooltiptext-tb">{rowNationality}</div>}
              </span>
            </div>
            <div className="income-breakdown-table--row-item">{appointmentCategoryList[row.category]}
            </div>
            <div className="income-breakdown-table--row-item custom-tooltip-tb">
              <span className="font-14 tooltip-tb">
                <div className="row-txt">{rowAppPosDisplay}</div>
                {rowAppPosDisplay !== "-" && <div className="tooltiptext-tb">{rowAppPosDisplay}</div>}
              </span>
            </div>
            <div className="income-breakdown-table--row-item">{row.appointmentDate && row.appointmentDate !== null && row.appointmentDate !== "" ? moment(row.appointmentDate, "YYYY-MM-DD").format("DD/MM/YYYY") : "-"}</div>
          </div>
        );

      case 'shareholdersData':
        const shareHoldersNationality = mapItems.nationality;
        const shareHolderCategoryList = mapItems.shareholderCategory;
        const shareTypeList = mapItems.shareType;

        //Extra "S" indicating for shareholders use

        //Check if either or both exist, return "-" on null
        const personReferenceRowS = row.personReference ? row.personReference : false;
        const entityReferenceRowS = row.entityReference ? row.entityReference : false;

        //check name
        const rowNameS = personReferenceRowS ? personReferenceRowS.personName : entityReferenceRowS ? entityReferenceRowS.entityName : null;
        //check id
        const rowIdNoS = personReferenceRowS ? personReferenceRowS.idno : entityReferenceRowS ? entityReferenceRowS.uen : null;
        //check nationality for person
        const rowNationalityS = personReferenceRowS && shareHoldersNationality[personReferenceRowS.nationality] !== undefined ? shareHoldersNationality[personReferenceRowS.nationality] : "-";

        //check if empty
        const nameS = rowNameS !== null && rowNameS !== "" ? rowNameS : "-";
        const idNoS = rowIdNoS !== null && rowIdNoS !== "" ? rowIdNoS : "-";

        return (
          <div key={i} className="income-breakdown-table--row-flex">
            <div className="income-breakdown-table--row-item custom-tooltip-tb">
              <span className="font-14 tooltip-tb">
                <div className="row-txt">{nameS}</div>
                {nameS !== "-" && <div className="tooltiptext-tb">{nameS}</div>}
              </span>
            </div>
            <div className="income-breakdown-table--row-item">{idNoS}</div>
            <div className="income-breakdown-table--row-item custom-tooltip-tb">
              <span className="font-14 tooltip-tb">
                <div className="row-txt">{rowNationalityS}</div>
                {rowNationalityS !== "-" && <div className="tooltiptext-tb">{rowNationalityS}</div>}
              </span>
            </div>
            <div className="income-breakdown-table--row-item">{shareHolderCategoryList[row.category] !== undefined ? shareHolderCategoryList[row.category] : "-"}</div>
            <div className="income-breakdown-table--row-item custom-tooltip-tb">
              <span className="font-14 tooltip-tb">
                <div className="row-txt">{shareTypeList[row.shareType] !== undefined ? shareTypeList[row.shareType] : "-"}</div>
                {shareTypeList[row.shareType] !== undefined && <div className="tooltiptext-tb">{shareTypeList[row.shareType]}</div>}
              </span>
            </div>
            <div className="income-breakdown-table--row-item">
              <table className="currencyContent">
                <tbody>
                  <tr>
                    <td className="currencyAmount">
                      {row.allocation ?
                        <NumberFormat value={row.allocation}
                          displayType={'text'}
                          thousandSeparator={true}
                        /> : "-"}
                    </td>
                    <td className="currencyType">
                      {row.currency !== "" && row.currency !== null ? row.currency : ""}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        );

      case "grantsData":
        const grantAreaList = mapItems.grantArea;
        const grantStatusList = mapItems.grantStatus;
        const grantCategoryList = mapItems.grantCategory;
        const grantTypeList = mapItems.grantType;
        const grantType = row.grantType ? grantTypeList[row.grantType] : "-";
        const functionalArea = row.functionalArea ? grantAreaList[row.functionalArea] : "-";
        const developmentCategory = row.developmentCategory ? grantCategoryList[row.developmentCategory] : "-";
        return (
          <div key={i} className="income-breakdown-table--row-flex">
            <div className="income-breakdown-table--row-item custom-tooltip-tb">
              <span className="font-14 tooltip-tb">
                <div className="row-txt">{grantType}</div>
                {<div className="tooltiptext-tb">{grantType}</div>}
              </span>
            </div>
            <div className="income-breakdown-table--row-item">
              {row.grantStatus ? grantStatusList[row.grantStatus] : "-"}
            </div>
            <div className="income-breakdown-table--row-item custom-tooltip-tb">
              <span className="font-14 tooltip-tb">
                <div className="row-txt">{functionalArea}</div>
                {<div className="tooltiptext-tb">{functionalArea}</div>}
              </span>
            </div>
            <div className="income-breakdown-table--row-item custom-tooltip-tb">
              <span className="font-14 tooltip-tb">
                <div className="row-txt">{developmentCategory}</div>
                {<div className="tooltiptext-tb">{developmentCategory}</div>}
              </span>
            </div>
            <div className="income-breakdown-table--row-item">
              <table className="currencyContent">
                <tbody>
                  <tr>
                    <td className="currencyAmount">
                      {row.approvedAmount ?
                        <NumberFormat value={row.approvedAmount}
                          displayType={'text'}
                          thousandSeparator={true}
                        /> : "-"}
                    </td>
                    <td className="currencyType">
                      {"SGD"}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="income-breakdown-table--row-item">
              {row.submittedDate && row.submittedDate !== null && row.submittedDate !== "" ? moment(row.submittedDate, "YYYY-MM-DD").format("DD/MM/YYYY") : "-"}
            </div>
            <div className="income-breakdown-table--row-item">
              {row.lastUpdatedDate && row.lastUpdatedDate !== null && row.lastUpdatedDate !== "" ? moment(row.lastUpdatedDate, "YYYY-MM-DD").format("DD/MM/YYYY") : "-"}
            </div>
          </div>
        );

      default:
        return (
          <div />
        );
    }

  });
};

const TableViewCompany = props => {
  return (
    <div className="income-breakdown-table--main-container-flex new-table" style={{ minWidth: props.flexTableWidth ? props.flexTableWidth + 'px' : '100%' }}>
      <div className="income-breakdown-table--titles-container-flex">
        {props.colTitles.map((title, i) => (
          <div key={i} className="income-breakdown-table--title-flex">
            {title}
          </div>
        ))}
      </div>
      <div className="income-breakdown-table--rows-container-flex">
        {generateTableRows(props.tableContent, props.tableType, props.mapItems)}
      </div>
    </div>
  );
};

export default TableViewCompany;