import React from "react";
import PropTypes from "prop-types";
import { validators } from "./../../../common/validations";
import "./EditableTextInput.css";

// import tickImg from "./../../../assets/images/Tick.svg";
// import crossImg from "./../../../assets/images/cross.svg";
import pencilIcon from "./../../../assets/images/pencil-icon.svg";

const Icon = props => {
	const visibility = props.isValid ? "--visible" : "--invisible";
	const tick = (
		<img className={`textInputTick${visibility}`} src={pencilIcon} alt="pencil" />
	);

	return props.isValid ? tick : "";
};

const EditableTextInput = props => {
	const {
		type,
		inputID,
		isFocus,
		isValid,
		errorMsg,
		label,
		value,
		onFocus,
		onBlur,
		onChange,
		validator,
		maxCharacters,
		isUpperCase,
		isDateFormat,
		isDisabled,
		hasIcon,
		errorMsgList,
		isNumber,
		alignCenter,
		isMinOne,
		// isInitial,
		isMyInfo,
		hasEdit
	} = props;
	const focus = isFocus || value.length > 0 || !isValid ? "--focused" : "";
	const focusText = isFocus ? "textInputContainer--onFocus" : "";
	const center = alignCenter ? "alignCenter" : "";
	const typeCenter = alignCenter ? "alignCenter" : "";
	const showErrorLabel = isValid ? "" : "textInputLabel--error";
	const errorContainerStyle = isValid ? "" : "textInputLabel-container--error";
	let keyCode;
	const handleOnChange = e => {
		const regex = /(^\d{2}$)|(^\d{2}\/\d{2}$)/;
		const dateFormatRegex = /(^\d{1,2}$)|(^\d{2}\/$)|(^\d{2}\/\d{1,2}$)|(^\d{2}\/\d{2}\/$)|(^\d{2}\/\d{2}\/\d{1,4}$)/;
		const regexNumber = /^([0-9])+$/;
		const checkForZero = /^(?:0)/;
		let val =
			isDateFormat && regex.test(e.target.value) && keyCode !== 8
				? `${e.target.value}${"/"}`
				: e.target.value;

		if (isMinOne && e.target.value && checkForZero.test(e.target.value)) {
			val = value
		}

		if (
			isDateFormat &&
			!dateFormatRegex.test(e.target.value) &&
			keyCode !== 8
		) {
			val = value;
		}

		if (isNumber && !regexNumber.test(e.target.value) && keyCode !== 8 && keyCode !== 229 && keyCode !== 46) {
			val = value;
		}

		const inputValue = isUpperCase ? val.toUpperCase() : val;
		let isValid = true;
		let errorMsg = "";
		if (validator && validator.length > 0) {
			let result = validators(validator, inputValue);
			isValid = result.status;
			errorMsg = result.errorMsg !== '' ? errorMsgList[result.errorMsg].replace("{number}", result.number).replace("{secondnumber}", result.secondnumber) : '';
		}

		const data = {
			value: inputValue,
			isValid,
			errorMsg,
			isInitial: false
		};
		onChange(data);
	};

	const handleOnBlur = e => {
		let val = e.target.value;
		const inputValue = isUpperCase ? val.toUpperCase() : val;
		const data = {
			value: inputValue,
			isInitial: false
		};
		onBlur(data);
	};

	return (
		<div>
			<div
				id={inputID}
				className={`textInputContainer ${errorContainerStyle} ${focusText}`}
				onFocus={onFocus ? onFocus : null}
				onBlur={onBlur ? handleOnBlur : null}
				onKeyDown={e => {
					keyCode = e.keyCode;
				}}
			>
				<div className="textInputContent">
					<label className={`textInputLabel${focus} ${showErrorLabel} ${center}`}>
						{`${label}${isValid ? "" : ` ${errorMsg}`}`}
					</label>
					<input
						disabled={isDisabled}
						onWheel={e => e.preventDefault()}
						value={value}
						onKeyDown={e => {
							keyCode = e.keyCode;
						}}
						onChange={handleOnChange}
						type={type ? type : "text"}
						className={`textInput${focus} ${typeCenter}`}
						maxLength={maxCharacters}
					/>
					{hasIcon && value && !isDisabled && isMyInfo && !hasEdit && <Icon isValid={isValid} />}
				</div>
			</div>
			<div className="bottomLine" />
			<div className="fullErrorContent" />
		</div>
	);
};

EditableTextInput.propTypes = {
	isFocus: PropTypes.bool,
	isValid: PropTypes.bool,
	isDisabled: PropTypes.bool,
	errorMsg: PropTypes.string,
	label: PropTypes.string.isRequired,
	value: PropTypes.string,
	maxCharacters: PropTypes.number,
	onFocus: PropTypes.func,
	onBlur: PropTypes.func,
	onChange: PropTypes.func.isRequired
};

EditableTextInput.defaultProps = {
	isFocus: false,
	isValid: true,
	errorMsg: "",
	label: "Label",
	value: "",
	isUpperCase: false,
	isDateFormat: false,
	isDisabled: false,
	hasIcon: true
};

export default EditableTextInput;
