import React, {Component} from 'react';
import PropTypes from 'prop-types';

import "./TenorTable.css";

import TenorTableMobile from "./TenorTableMobile/TenorTableMobile";
import TenorTableDesktop from "./TenorTableDesktop/TenorTableDesktop";

class TenorTable extends Component {
    componentWillReceiveProps(nextProps) {
        // const isPromoCodeSelected = nextProps.state.getIn(dataPath(["products", this.props.pid, "tenor"]), "");
        // const isTableOpenBefore =
        //     nextProps.state.getIn(allTableStatePath(`${UNSECRUED_LOAN_TABLE}-${nextProps.pid}`), Map()).count() > 0;
        // if (nextProps.tableContent.length && !isPromoCodeSelected) {
        //     const firstRow = nextProps.tableContent[0];
        //     this.props.dispatch(selectTableRow(UNSECRUED_LOAN_TABLE, this.props.pid, firstRow.promoCode));
        // }
        // if (!isTableOpenBefore) {
        //     this.props.dispatch(expandTableRow(`${UNSECRUED_LOAN_TABLE}-${nextProps.pid}`, 0));
        // }
    }
    render() {
        const props = this.props;
        return (
            <div>
                <div className='tenorTable-desktop'><TenorTableDesktop {...props} /></div>
                <div className='tenorTable-mobile'><TenorTableMobile {...props} /></div>
            </div>
        );
    }
}

TenorTable.propTypes = {
    dispatch: PropTypes.func.isRequired,
    pid: PropTypes.string.isRequired,
    tableContent: PropTypes.array.isRequired
};

export default TenorTable;
