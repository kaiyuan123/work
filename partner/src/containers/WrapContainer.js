import React, { Component } from "react";
import { connect } from "react-redux";
import { getInitialData, setErrorMessage, toggleNav } from "./../actions/commonAction";
import { handleHideAskQuestionPopup } from "./../actions/askQuestionsAction";
import GenericErrorMessage from "./../components/GenericErrorMessage/GenericErrorMessage";
import Loader from "./../components/Loader/Loader";
import NavItem from "./../components/NavItem/NavItem";
import closeIconWhite from "./../assets/images/x-white.svg";
import hamburger from "./../assets/images/menu-hamburger.svg";
import "./../assets/css/bootstrap.css";
import "./../assets/css/uob-form-loan.css";
import "./../assets/css/eBiz-style.css";

class WrapContainer extends Component {
  // constructor() {
  //   super();
  //   this.handleOutsideClick = this.handleOutsideClick.bind(this);
  // }

  componentWillMount() {
    const { dispatch } = this.props;
    // document.addEventListener('click', this.handleOutsideClick, false);
    dispatch(getInitialData());
  }

  // componentWillUnmount() {
  //   document.removeEventListener('click', this.handleOutsideClick, false);
  // }

  // handleOutsideClick(event) {
  //   const { dispatch, commonReducer } = this.props;
  //   event.stopPropagation();
  //   if (commonReducer.isSideBarShow && !this.refs.mainLeftMenu.contains(event.target)) {
  //     dispatch(toggleLeftSideBar(false));
  //   }
  // }

  closeNav() {
    const { commonReducer, dispatch } = this.props;
    if (commonReducer.toggleNav) {
      dispatch(toggleNav(true));
    }
  }


  toggleNav(showNav) {
    const { dispatch } = this.props;
    dispatch(toggleNav(showNav));
  }

  handleOnClearMessage() {
    const { dispatch } = this.props;
    dispatch(setErrorMessage(""));
  }

  // handleToggleLeftSideBar(status) {
  //   const { dispatch } = this.props;
  //   dispatch(toggleLeftSideBar(status));
  // }

  handlePopup(popuptoggle) {
    const { dispatch } = this.props;
    dispatch(handleHideAskQuestionPopup(popuptoggle));
  }

  renderMenu(currentSection) {
    const { contactDetailsReducer, shareHoldersReducer } = this.props;
    const { shareHoldersList } = shareHoldersReducer;
    const { businessConstitution } = contactDetailsReducer;
    const { personType } = contactDetailsReducer;

    let isShareHoldersList = "";
    if (shareHoldersList.value.length > 0) {
      isShareHoldersList = true;
    } else {
      isShareHoldersList = false;
    }
    if (personType.value === "AS") {
      const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "asrDetails", "confirmDetailsPage"];
      const indexOfSteps = steps.indexOf(currentSection);
      return <ul className="navbar-nav">
        <NavItem
          defaultStep={true}
          navTitle={"MyInfo Business"}
          defaultLink={true}
          stepFinished={true}
        />
        <NavItem
          stepFinished={indexOfSteps > 0}
          currentActiveStep={indexOfSteps === 0}
          linkSelected={
            (indexOfSteps === 0 && this.props.location.hash === "") ||
            (indexOfSteps >= 0 &&
              this.props.location.hash === "#contactDetails-section")
          }
          linkIdName={"#contactDetails-section"}
          navTitle={"Basic Details"}
        />
        <NavItem
          stepFinished={indexOfSteps > 1}
          currentActiveStep={indexOfSteps === 1}
          linkSelected={
            (indexOfSteps === 1 && this.props.location.hash === "") ||
            (indexOfSteps >= 1 &&
              this.props.location.hash ===
              "#personalIncomeDetails-section")
          }
          linkIdName={"#personalIncomeDetails-section"}
          navTitle={"Applicant's Personal Details"}
        />
        <NavItem
          stepFinished={indexOfSteps > 2}
          currentActiveStep={indexOfSteps === 2}
          linkSelected={
            (indexOfSteps === 2 && this.props.location.hash === "") ||
            (indexOfSteps >= 2 &&
              this.props.location.hash === "#companyDetails-section")
          }
          linkIdName={"#companyDetails-section"}
          navTitle={"Company Details"}
        />
        <NavItem
          stepFinished={indexOfSteps > 3}
          currentActiveStep={indexOfSteps === 3}
          linkSelected={
            (indexOfSteps === 3 && this.props.location.hash === "") ||
            (indexOfSteps >= 3 && this.props.location.hash === "#accountSetup-section")
          }
          linkIdName={"#accountSetup-section"}
          navTitle={"Account Setup"}
        />
        <NavItem
          stepFinished={indexOfSteps > 4}
          currentActiveStep={indexOfSteps === 4}
          linkSelected={
            (indexOfSteps === 4 && this.props.location.hash === "") ||
            (indexOfSteps >= 4 && this.props.location.hash === "#operatingMandate-section")
          }
          linkIdName={"#operatingMandate-section"}
          navTitle={"Operating Mandate"}
        />
        {isShareHoldersList && <NavItem
          stepFinished={indexOfSteps > 5}
          currentActiveStep={indexOfSteps === 5}
          linkSelected={
            (indexOfSteps === 5 && this.props.location.hash === "") ||
            (indexOfSteps >= 5 && this.props.location.hash === "#shareHolders-section")
          }
          linkIdName={"#shareHolders-section"}
          navTitle={"Shareholder Details"}
        />
        }
        <NavItem
          stepFinished={indexOfSteps > 6}
          currentActiveStep={indexOfSteps === 6}
          linkSelected={
            (indexOfSteps === 6 && this.props.location.hash === "") ||
            (indexOfSteps >= 6 && this.props.location.hash === "#taxSelfDeclarations-section")
          }
          linkIdName={"#taxSelfDeclarations-section"}
          navTitle={"Tax Self-declaration"}
        />

        {businessConstitution.value !== 'S' &&
          <NavItem
            stepFinished={indexOfSteps > 7}
            currentActiveStep={indexOfSteps === 7}
            linkSelected={
              (indexOfSteps === 7 && this.props.location.hash === "") ||
              (indexOfSteps >= 7 && this.props.location.hash === "#asrDetails-section")
            }
            linkIdName={"#asrDetails-section"}
            navTitle={"Accounts And Services Resolution"}
          />
        }
        <NavItem
          stepFinished={indexOfSteps > 8}
          currentActiveStep={indexOfSteps === 8}
          linkSelected={
            (indexOfSteps === 8 && this.props.location.hash === "") ||
            (indexOfSteps >= 8 &&
              this.props.location.hash ===
              "#confirmDetailsPage-section")
          }
          linkIdName={"#confirmDetailsPage-section"}
          navTitle={"Review"}
        />
      </ul>
    }

    if (personType.value === "SH" || personType.value === "CP") {
      const steps = ["contactDetails", "personalIncomeDetails", "confirmDetailsPage"];
      const indexOfSteps = steps.indexOf(currentSection);
      return <ul className="navbar-nav">
        <NavItem
          defaultStep={true}
          navTitle={"MyInfo Business"}
          defaultLink={true}
          stepFinished={true}
        />
        <NavItem
          stepFinished={indexOfSteps > 0}
          currentActiveStep={indexOfSteps === 0}
          linkSelected={
            (indexOfSteps === 0 && this.props.location.hash === "") ||
            (indexOfSteps >= 0 &&
              this.props.location.hash === "#contactDetails-section")
          }
          linkIdName={"#contactDetails-section"}
          navTitle={"Basic Details"}
        />
        <NavItem
          stepFinished={indexOfSteps > 1}
          currentActiveStep={indexOfSteps === 1}
          linkSelected={
            (indexOfSteps === 1 && this.props.location.hash === "") ||
            (indexOfSteps >= 1 &&
              this.props.location.hash ===
              "#personalIncomeDetails-section")
          }
          linkIdName={"#personalIncomeDetails-section"}
          navTitle={"Applicant's Personal Details"}
        />
        <NavItem
          stepFinished={indexOfSteps > 2}
          currentActiveStep={indexOfSteps === 2}
          linkSelected={
            (indexOfSteps === 2 && this.props.location.hash === "") ||
            (indexOfSteps >= 2 &&
              this.props.location.hash ===
              "#confirmDetailsPage-section")
          }
          linkIdName={"#confirmDetailsPage-section"}
          navTitle={"Review"}
        />
      </ul>
    }

  }

  render() {
    const { commonReducer, retrievingReducer, applyReducer, contactDetailsReducer, askQuestionsReducer } = this.props;
    const { popuptoggle } = askQuestionsReducer;
    const { personType } = contactDetailsReducer;
    let imgUrlWhite = "./images/logos/uob-eBiz-Logo.svg"; //white
    let imgUrl = "./images/logos/uob-eBiz-Logo-blue.svg"; //blue
    let rightByYouLogo = "./images/logos/right-by-you-logo.jpg"; //rightByYouLogo
    let pathname = window.location.pathname;
    const currentPath = pathname.substr(pathname.lastIndexOf('/') + 1);
    const currentSection = commonReducer.currentSection;
    const whiteScreenLogo = currentPath === "error" || currentSection === "thankyou" || currentPath === "verification";
    const productCode = applyReducer.productCode !== '' && applyReducer.productCode !== null ? applyReducer.productCode : "";
    const productTitle = applyReducer.productCode !== '' && applyReducer.productCode !== null && commonReducer.appData !== null && commonReducer.appData.productTitle[productCode];
    const productTitleError = applyReducer.productCode !== '' && applyReducer.productCode !== null && commonReducer.appData !== null && commonReducer.appData.productTitleThankYou[productCode];

    const isfullwhite = whiteScreenLogo ? "whitePage-header" : "";
    const navBarHide = whiteScreenLogo ? "whitepage-navBar" : "";

    const toggleNavCSS = commonReducer.toggleNav ? "showNav" : "hideNav";
    // const isfullWhiteError = whiteScreenLogo ? "whitePage-error" : "";

    let progressPercentage = 0;

    if (personType.value === "AS") {
      const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "asrDetails", "confirmDetailsPage"];
      const indexOfSteps = steps.indexOf(currentSection);

      progressPercentage = parseFloat(((parseInt(indexOfSteps) + 1) / parseInt(steps.length)) * 100);
    }

    if (personType.value === "SH" || personType.value === "CP") {
      const steps = ["contactDetails", "personalIncomeDetails", "confirmDetailsPage"];
      const indexOfSteps = steps.indexOf(currentSection);

      progressPercentage = parseFloat(((parseInt(indexOfSteps) + 1) / parseInt(steps.length)) * 100);
    }
    // const productTitleM = productTitle.toString().split("|");
    let isPopup = popuptoggle ? 'popup-on' : 'popup-off';

    return (
      <div>
        <div className={`uob-form-loan ${isPopup} ${isfullwhite}`}>
          {commonReducer.isLoading && <Loader />}
          {whiteScreenLogo &&
            <div className={`apply-page-header-container ${isfullwhite}`}>
              <div className="apply-page-header-title">
                <img className="img-size" alt="logo" src={imgUrl} />
                {productCode !== "" && < div className="title-vertical-line" />}
                {productCode !== "" && <div className="title-text"> {productTitleError} </div>}
              </div>
              <div className="right-by-you-container">
                <img alt="right-by-you" src={rightByYouLogo} />
              </div>
            </div>
          }
          <div>
            <nav
              className={`navbar navbar-expand-lg navbar-dark bg-primary-left fixed-top ${toggleNavCSS}`}
              id="sideNav"
            >
              <span className="sideBarCloseImage" onClick={() => this.toggleNav(true)}>
                <img className="close-icon" src={closeIconWhite} alt="Close-Icon" width={20} height={20} />
              </span>
              <div className="leftTopLogo">
                <img src={imgUrlWhite} alt="Logo" className="uob-logo" width="160" />
              </div>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                {this.renderMenu(currentSection)}
              </div>
            </nav>
            {commonReducer.appData !== null && retrievingReducer.myInfoData !== null &&
              <div className={`container-fluid content_1 productTitle-header ${navBarHide} ${toggleNavCSS}`}>

                <div className="displayHeader">
                  <button className={`menuButton  ${toggleNavCSS}`} type="button" onClick={() => this.toggleNav(commonReducer.toggleNav)}><img alt="hamburger" src={hamburger} /></button>
                  <img className="img-size" alt="logo" src={imgUrl} />
                  <div className="title-vertical-line" />
                  <div className="mob-prodtitle">
                    {productTitle.split(" | ")[0]}
                    <br />
                    <span className="font-16">{productTitle.split(" | ")[1]}</span>
                  </div>
                </div>
                <section
                  className="resume-section d-flex d-column"
                  id="home"
                >
                  <div className="my-auto">
                    <h1 className="mt-0 title1">
                      <span className="title2">
                        {productTitle}
                      </span>
                    </h1>
                  </div>
                </section>
                {
                  <div className="progressTopMain">
                    <div className="mobileProgressTop" style={{ width: progressPercentage + '%' }} />
                  </div>
                }

                {!whiteScreenLogo &&
                  <img className="rightByYouLogo" src={rightByYouLogo} alt="Right By You" />
                }
              </div>
            }
          </div>
          <div className={`uob-form-loan-container ${toggleNavCSS}`}>
            <div className="uob-body" onClick={() => this.closeNav()}>
              {commonReducer.appData !== null && this.props.children}
            </div>
          </div>
          {commonReducer.messageContent !== "" && (
            <GenericErrorMessage
              {...this.props}
              interval={60}
              messageContent={commonReducer.messageContent}
              onClearMessage={this.handleOnClearMessage.bind(this)}
            />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { commonReducer, retrievingReducer, contactDetailsReducer, shareHoldersReducer, applyReducer, askQuestionsReducer } = state;
  return { commonReducer, retrievingReducer, contactDetailsReducer, shareHoldersReducer, applyReducer, askQuestionsReducer };
};

export default connect(mapStateToProps)(WrapContainer);
