import React, { Component } from "react";
import { connect } from "react-redux";
import WrapContainer from "./WrapContainer";
// import LoadingPage from "./../pages/LoadingPage";
import ThankYouPage from "./../pages/ThankYouPage";
import ContactDetailsPage from "./../pages/ContactDetailsPage";
import PersonalIncomeDetailsPage from "./../pages/PersonalIncomeDetailsPage";
import CompanyDetailsPage from "./../pages/CompanyDetailsPage";
import AccountSetupPage from "./../pages/AccountSetupPage";
import OperatingMandatePage from "./../pages/OperatingMandatePage";
import ConfirmDetailsPage from "./../pages/ConfirmDetailsPage";
import ASRDetailsPage from "./../pages/ASRDetailsPage";
import ShareHoldersPage from "./../pages/ShareHoldersPage";
import TaxSelfDeclarationsPage from "./../pages/TaxSelfDeclarationsPage";
import {
	handleSignatoriesAdditionalErrorMessage,
	setSignatoriesPartnersInputError
} from "./../actions/operatingMandateAction";
import { handleShareHolderErrorMessage } from "./../actions/shareHoldersAction";
import { eliminateDuplicates } from "../common/utils";
import { scrollToSection, setRetrievingMyInfo, handleErrorMessage, scrollToElement, scrollBackToSection } from "./../actions/commonAction";
import {
	retrieveMyInfoDetails,
  submitPartialApplication,
	submitApplication,
  setErrorWithValue
} from "./../actions/applicationAction";
import { checkPurposeOfAccountIsValid } from "./../actions/accountSetupAction";
import { handleAdditionalErrorMessage, setTinNoOthersInputError } from './../actions/taxSelfDeclarationsAction';
import localStore from './../common/localStore';
import { APPLICANT_TYPE } from './../common/constant';
import { showErrorMessage } from "./../actions/signatureAction";
import SessionExpirePopup from "./../pages/SessionExpirePopup";
import {  checkPrimaryClienteleIsValid } from "./../actions/companyBasicDetailsAction";

class ApplicationContainer extends Component {
	componentWillMount() {
		const { dispatch } = this.props;
		dispatch(setRetrievingMyInfo(true));
		dispatch(retrieveMyInfoDetails(() => this.redirectToErrorPage()));
	}

	checkisValidFields(fields) {
		const { dispatch } = this.props;
		let errorCount = 0;
		for (let i = 0; i < fields.length; i++) {
			if (!fields[i].isValid) {
				const fieldError = fields.find(x => x.isValid === false);
				dispatch(scrollToElement(fieldError.name))
				errorCount++;
			}
		}
		if (errorCount > 0) {
			return false;
		}
		return true;
	}

	checkEmptyFieldsStatic(fields, errorMsg, action) {
		const { dispatch } = this.props;
		let errorCount = 0;
		for (let i = 0; i < fields.length; i++) {
			if (fields[i].value === "") {
				dispatch(action(fields[i].name, errorMsg));
				errorCount++;
			}
		}
		if (errorCount > 0) {
			return false;
		}
		return true;
	}

	checkisValidFieldsStatic(fields, errorMsg, action) {
		const { dispatch } = this.props;
		let errorCount = 0;
		for (let i = 0; i < fields.length; i++) {
			if (!fields[i].isValid) {
				dispatch(action(fields[i].name, errorMsg));
				errorCount++;
			}
		}
		if (errorCount > 0) {
			return false;
		}
		return true;
	}

	checkEmptyFields(fields, errorMsg, action) {
		const { dispatch } = this.props;
		let errorCount = 0;
		for (let i = 0; i < fields.length; i++) {
			if (fields[i].value === "") {
				dispatch(action(fields[i].name, errorMsg));
				const fieldError = fields.find(x => x.value === '');
				dispatch(scrollToElement(fieldError.name))
				errorCount++;
			}
		}
		if (errorCount > 0) {
			return false;
		}
		return true;
	}


	checkAdditionalEmptyFields(key, fields, errorMsg, action) {
		const { dispatch } = this.props;
		let errorCount = 0;
		for (let i = 0; i < fields.length; i++) {
			if (fields[i].value === "") {
				dispatch(action(key, fields[i].name, errorMsg));
				const fieldError = fields.find(x => x.value === '');
				dispatch(scrollToElement(fieldError.name + key));
				errorCount++;
			}
		}
		if (errorCount > 0) {
			return false;
		}
		return true;
	}

	checkAdditionalValidFields(key, fields, errorMsg, action) {
		const { dispatch } = this.props;
		let errorCount = 0;
		for (let i = 0; i < fields.length; i++) {
			if (!fields[i].isValid) {
				dispatch(action(key, fields[i].name, errorMsg));
				const fieldError = fields.find(x => x.isValid === false);
				dispatch(scrollToElement(fieldError.name + key));
				errorCount++;
			}
		}
		if (errorCount > 0) {
			return false;
		}
		return true;
	}

	isCRSCheckingPass() {
		const { taxSelfDeclarationsReducer, commonReducer, dispatch, contactDetailsReducer } = this.props;
		const { tinNoOthersId, checkboxOthers, checkboxUS, checkboxSG, checkboxNone, countryNone, tinNoSG, tinNoUS, isUSResident, fatcaCRSStatus } = taxSelfDeclarationsReducer;
		const { entityType } = contactDetailsReducer;
		const isPOrO = entityType.value === "P" || entityType.value === "O";
		let errorCount = 0;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
		let duplicateNricMsg = commonReducer.appData.errorMsgs.duplicateNricMsg;

		let duplicateKeys = [];
		for (let j = 0; j < tinNoOthersId.length; j++) {
			for (let k = j + 1; k < tinNoOthersId.length; k++) {
				const key = tinNoOthersId[k];
				const compareKey = tinNoOthersId[j];
				if (k !== j && taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value === taxSelfDeclarationsReducer[`tinNoOthers${compareKey}`].othersCountry.value) {
					duplicateKeys.push(tinNoOthersId[k]);
					duplicateKeys.push(tinNoOthersId[j]);
				}
			}
		}
		duplicateKeys = eliminateDuplicates(duplicateKeys);
		if (duplicateKeys.length > 0) {
			for (let i = 0; i < duplicateKeys.length; i++) {
				const key = duplicateKeys[i];
				dispatch(setTinNoOthersInputError(key, 'othersCountry', duplicateNricMsg));
				errorCount++;
			}
		}

		if (checkboxUS.isToggled) {
			if (!this.checkEmptyFields([tinNoUS, isUSResident], requiredMsg, handleErrorMessage)) {
				errorCount++
			}

			if (!this.checkisValidFields([tinNoUS, isUSResident], requiredMsg, handleErrorMessage)) {
				errorCount++
			}
		}

		if (checkboxSG.isToggled) {
			if (!this.checkEmptyFields([tinNoSG], requiredMsg, handleErrorMessage)) {
				errorCount++
			}

			if (!this.checkisValidFields([tinNoSG], requiredMsg, handleErrorMessage)) {
				errorCount++
			}
		}

		if (tinNoOthersId && tinNoOthersId.length > 0) {
			taxSelfDeclarationsReducer.tinNoOthersId.map((key) => {
				const haveTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo;
				const othersCountry = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry;
				const othersTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo;
				const reasons = taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons;

				if (checkboxOthers.isToggled) {
					if (haveTINNo.value !== "") {
						if (haveTINNo.value === "Y") {
							if (!this.checkAdditionalEmptyFields(key, [othersCountry, othersTINNo], requiredMsg, handleAdditionalErrorMessage)) {
								errorCount++;
							}
						} else {
							if (!this.checkAdditionalEmptyFields(key, [othersCountry, reasons], requiredMsg, handleAdditionalErrorMessage)) {
								errorCount++;
							}
						}
					} else {
						if (!this.checkAdditionalEmptyFields(key, [haveTINNo], requiredMsg, handleAdditionalErrorMessage)) {
							errorCount++;
						}
						errorCount++
					}
				}

				if (checkboxNone.isToggled) {
					if (!this.checkEmptyFields([countryNone], requiredMsg, handleErrorMessage)) {
						errorCount++
					}

					if (!this.checkisValidFields([countryNone], requiredMsg, handleErrorMessage)) {
						errorCount++
					}
				}

				return errorCount;
			})
		}

		if (isPOrO) {
			if (!this.checkEmptyFields([fatcaCRSStatus], requiredMsg, handleErrorMessage)) {
				errorCount++
			}

			if (!this.checkisValidFields([fatcaCRSStatus], requiredMsg, handleErrorMessage)) {
				errorCount++
			}
		}

		if (errorCount > 0) {
			return false
		}

		return true
	}

	isContactDetailsPassChecking() {
		const { contactDetailsReducer: { emailAddress } } = this.props;
		let flag = true;
		if (!this.checkEmptyFields([emailAddress], this.props.commonReducer.appData.errorMsgs.requiredMsg, handleErrorMessage)) {
			flag = false;
		}
		if (!this.checkisValidFields([emailAddress], this.props.commonReducer.appData.errorMsgs.requiredMsg, handleErrorMessage)) {
			flag = false;
		}
		return flag;
	}

	isPersonalIncomeDetailsPassChecking() {
		const { personalIncomeDetailsReducer, commonReducer } = this.props;
		const { maritalStatus } = personalIncomeDetailsReducer;
		let errorCount = 0;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
		let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;

		if (!this.checkEmptyFields([maritalStatus], requiredMsg, handleErrorMessage)) {
			errorCount++;
		}

		if (!this.checkisValidFields([maritalStatus], invalidMsg, handleErrorMessage)) {
			errorCount++;
		}

		if (errorCount > 0) {
			return false;
		}

		return true;
	}

	isAccountSetupPassChecking() {
		const { accountSetupReducer, commonReducer, dispatch } = this.props;
		const {
			accountName, purposeOfAccount, registerOfPayNow,
			setupBIBPlus, BIBGroupId, BIBMobileNumber, BIBContactPerson, BIBEmailAddress, payNowID, others, otherPurpose
		} = accountSetupReducer;
		let errorCount = 0;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;

		if (purposeOfAccount.length < 1) {
			dispatch(checkPurposeOfAccountIsValid(false));
			dispatch(scrollToElement('purposeOfAccountMain'));
			errorCount++
		}

		if (purposeOfAccount.length > 0) {
			dispatch(checkPurposeOfAccountIsValid(true));
		}

		if (!this.checkEmptyFields([accountName, purposeOfAccount], requiredMsg, handleErrorMessage)) {
			errorCount++
		}


		if (registerOfPayNow.isToggled) {
			if (!this.checkisValidFields([payNowID], requiredMsg, handleErrorMessage)) {
				errorCount++
			}
		}

		if (others.isToggled) {
			if (!this.checkEmptyFields([otherPurpose], requiredMsg, handleErrorMessage)) {
				errorCount++
			}

			if (!this.checkisValidFields([otherPurpose], requiredMsg, handleErrorMessage)) {
				errorCount++
			}
		}

		if (!this.checkEmptyFields([BIBMobileNumber, BIBContactPerson, BIBEmailAddress], requiredMsg, handleErrorMessage)) {
			errorCount++
		}

		if (!this.checkisValidFields([BIBContactPerson, BIBMobileNumber, BIBEmailAddress], requiredMsg, handleErrorMessage)) {
			errorCount++
		}

		if (setupBIBPlus.isToggled) {
			if (!this.checkisValidFields([BIBGroupId], requiredMsg, handleErrorMessage)) {
				errorCount++
			}
		}

		if (errorCount > 0) {
			return false
		}
		return true;
	}

	handleToPersonalIncomeDetails() {
		const { dispatch } = this.props;
		if (!this.isContactDetailsPassChecking()) {
			return;
		}
		dispatch(scrollToSection("personalIncomeDetails"));
	}

	handleToCompanyDetails() {
		const { dispatch } = this.props;
		this.handleSubmitPartialApplication();
		dispatch(scrollToSection("companyDetails"));
	}

	handleToAccountSetupPage() {
		const { dispatch } = this.props;
		this.handleSubmitPartialApplication();
		dispatch(scrollToSection("accountSetup"));
	}


	handleToOperatingMandatePage() {
		const { dispatch } = this.props;
		this.handleSubmitPartialApplication();
		dispatch(scrollToSection("operatingMandate"));
	}

	handleToShareHoldersPage() {
		const { dispatch } = this.props;
		this.handleSubmitPartialApplication();
		dispatch(scrollToSection("shareHolders"));
	}

	handleToASRDetailsPage() {
		const { dispatch, contactDetailsReducer } = this.props;
		const { businessConstitution } = contactDetailsReducer;
		if (businessConstitution.value !== "S") {
			this.handleSubmitPartialApplication();
			dispatch(scrollToSection("asrDetails"));
		} else {
			this.handleSubmitPartialApplication();
			dispatch(scrollToSection("confirmDetailsPage"));
		}
	}

	handleToReviewPage() {
		const { dispatch } = this.props;
		this.handleSubmitPartialApplication();
		dispatch(scrollToSection("confirmDetailsPage"));
	}

	handleToTaxSelfDeclarationsPage() {
		const { dispatch } = this.props;
		this.handleSubmitPartialApplication();
		dispatch(scrollToSection("taxSelfDeclarations"));
	}


	handleInitiateApplication() {
		//const { dispatch } = this.props;
		// const dataObj = this.getDataForInitiation();
		// dispatch(submitInitiateApplication(dataObj, () => this.handleToPersonalIncomeDetails()));
		this.handleSubmitPartialApplication();
		this.handleToPersonalIncomeDetails();
	}

	handleToUOBSite() {
		window.location.href = "https://www.uobgroup.com";
	}

	getDataForInitiation() {
		const {
			contactDetailsReducer
			// personalIncomeDetailsReducer
		} = this.props;

		const {
			emailAddress,
			entityType,
			secondaryNatureOfBusiness,
			primaryCountryOfOperation,
			primaryNatureOfBusiness
		} = contactDetailsReducer;

		// const {
		//   legalId
		// } = personalIncomeDetailsReducer;

		const dataObjBasicDetails = {
			person: [{
				basicInfo: {
					emailAddress: emailAddress.value
				},
			}],
			entity: {
				basicProfile: {
					entitySubType: entityType.value,
					natureOfBusiness: `${primaryNatureOfBusiness.value}|${secondaryNatureOfBusiness.value}`,
					primaryCountryOfOperation: primaryCountryOfOperation.value
				}
			}
		}

		return dataObjBasicDetails;
	}


	isAllChecking() {
		const { dispatch, commonReducer, confirmTandCReducer, contactDetailsReducer, drawSignatureReducer, signatureReducer, shareHoldersReducer } = this.props;
		const { acknowledgementCheckbox } = confirmTandCReducer;
		const { entityType, personType } = contactDetailsReducer;
		const { shareHoldersList } = shareHoldersReducer;
		let isShareHoldersList = "";
		if (shareHoldersList.value.length > 0) {
			isShareHoldersList = true;
		} else {
			isShareHoldersList = false;
		}
		if (!this.isContactDetailsPassChecking()) {
			dispatch(scrollBackToSection('contactDetails'));
			return false;
		}

		if (personType === "AS" && !this.isCompanyDetailsPassing()) {
			dispatch(scrollBackToSection('companyDetails'));
			return false;
		}

		if (personType === "AS" && !this.isAccountSetupPassChecking()) {
			dispatch(scrollBackToSection('accountSetup'));
			return false;
		}
		if (personType === "AS" && !this.isOperatingMandatePassing()) {
			dispatch(scrollBackToSection('operatingMandate'));
			return false;
		}

		if (personType === "AS" && entityType.value === 'LC' && isShareHoldersList && !this.isShareHoldersListPassing()) {
			dispatch(scrollBackToSection('shareHolders'));
			return false;
		}

		if (personType === "AS" && !this.isCRSCheckingPass()) {
			dispatch(scrollBackToSection('taxSelfDeclarations'));
			return false;
		}

		if (!acknowledgementCheckbox.isToggled) {
			dispatch(scrollToElement("confirmTandC"));
			return false;
		}

    if (personType === "AS" && signatureReducer.signaturePad.trimmedDataURL === null) {
			dispatch(showErrorMessage('Please provide your Signature'));
			dispatch(scrollBackToSection('signatureUpload'));
			return false;
		}
		return true;
	}

	getDataForSubmission() {
		const { contactDetailsReducer, applicationReducer, operatingMandateReducer, personalIncomeDetailsReducer } = this.props;
		const { emailAddress } = contactDetailsReducer;
		const { initiateMyinfoData } = applicationReducer;
		const { maritalStatus } = personalIncomeDetailsReducer;

		const dataObj = {
			person: [
				{
					basicInfo: { emailAddress: emailAddress.value },
					approvedSignatorie: { userId: "" },
					personalInfo: { maritalStatus: maritalStatus.value }
				}
			]
		};

		(operatingMandateReducer.signatoriesID || []).map((key) => {
			const approvedSignatoriesObj = operatingMandateReducer[`signatories${key}`];
			if (!approvedSignatoriesObj.signatoriesReadOnlyBibUserId && approvedSignatoriesObj.signatoriesBibUserId.value) {
				dataObj.person[0].approvedSignatorie.userId = approvedSignatoriesObj.signatoriesBibUserId.value;
			}
			return dataObj;
		});

		if (initiateMyinfoData.person[0] && !initiateMyinfoData.person[0].type === APPLICANT_TYPE.AS_AP) {
			delete dataObj.person[0].approvedSignatorie;
		}
		return dataObj;
	}

	handleSubmitPartialApplication() {
		const { dispatch, applicationReducer } = this.props;
		const { initiateMyinfoData } = applicationReducer;
		const applicationID = initiateMyinfoData.id;
		const dataObj = this.getDataForSubmission();
		dispatch(submitPartialApplication(dataObj, applicationID))
	}

	handleSubmitApplication() {
		const { dispatch, applicationReducer } = this.props;
		const { initiateMyinfoData } = applicationReducer;
		if (!this.isAllChecking()) {
			return;
		}
		const applicationID = initiateMyinfoData.id;
		const dataObj = this.getDataForSubmission();
		dispatch(submitApplication(dataObj, applicationID));
	}

	redirectToErrorPage() {
		// const { applicationReducer } = this.props;
		// localStore.clear();
		this.props.history.push({
			pathname: "error"
		})
	}

	isCompanyDetailsPassing() {
		const { dispatch, companyBasicDetailsReducer, localAddressInputReducer, commonReducer } = this.props;
		const { mailingAddressCheckbox, annualTurnover, primaryClientele } = companyBasicDetailsReducer;
		const {
			mailingUnit,
			mailingStreet,
			mailingBlock,
			mailingPostalCode,
			mailingLevel,
			mailingCity,
			mailingLine1,
			mailingCountry
		} = localAddressInputReducer;

		let errorCount = 0;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;

		if (mailingAddressCheckbox.isToggled) {
			if (mailingCountry.value !== "SG") {
				if (!this.checkEmptyFields([mailingCity, mailingLine1,], requiredMsg, handleErrorMessage)) {
					errorCount++
				}

				if (!this.checkisValidFields([mailingCity, mailingLine1,], requiredMsg, handleErrorMessage)) {
					errorCount++
				}
			} else {
				if (!this.checkEmptyFields([mailingPostalCode, mailingBlock, mailingLevel, mailingStreet, mailingUnit], requiredMsg, handleErrorMessage)) {
					errorCount++
				}

				if (!this.checkisValidFields([mailingPostalCode, mailingBlock, mailingLevel, mailingStreet, mailingUnit], requiredMsg, handleErrorMessage)) {
					errorCount++
				}
			}
		}

		 if (primaryClientele.length < 1) {
	      dispatch(checkPrimaryClienteleIsValid(false));
	      dispatch(scrollToElement('primaryClientele'));
	      errorCount++
	    }

	    if (primaryClientele.length > 0) {
	      dispatch(checkPrimaryClienteleIsValid(true));
	    }

		if (!this.checkEmptyFields([annualTurnover, primaryClientele], requiredMsg, handleErrorMessage)) {
			errorCount++
		}

		if (!this.checkisValidFields([annualTurnover], requiredMsg, handleErrorMessage)) {
			errorCount++
		}

		if (errorCount > 0) {
			return false
		}
		return true;
	}

	isOperatingMandatePassing() {
		const { operatingMandateReducer, commonReducer, accountSetupReducer } = this.props;
		const { signatoriesID, mainApplicantLegalId } = operatingMandateReducer;
		const isBibPlusUserToggled = accountSetupReducer.setupBIBPlus.isToggled;

		let errorCount = 0;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
		let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;

		if (signatoriesID.length > 0) {
			operatingMandateReducer.signatoriesID.map((key) => {
				const signatoriesBibUserId = operatingMandateReducer[`signatories${key}`].signatoriesBibUserId;
				const signatoriesToggleBIBUser = operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser;

				if (mainApplicantLegalId === operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value && isBibPlusUserToggled && signatoriesToggleBIBUser.isToggled) {
					if (!this.checkAdditionalEmptyFields(key, [signatoriesBibUserId], requiredMsg, handleSignatoriesAdditionalErrorMessage)) {
						errorCount++;
					}

					if (!this.checkAdditionalValidFields(key, [signatoriesBibUserId], invalidMsg, handleSignatoriesAdditionalErrorMessage)) {
						errorCount++;
					}
				}
				return errorCount;
			})
		}

		if (errorCount > 0) {
			return false
		}
		return true;

	}

	isASRDetailsPassing() {
		return true;
	}

	isShareHoldersListPassing() {
		const { dispatch, shareHoldersReducer, commonReducer } = this.props;
		const { shareholderID } = shareHoldersReducer;
		let errorCount = 0;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
		let duplicateNricMsg = commonReducer.appData.errorMsgs.duplicateNricMsg;

		if (shareholderID.length > 0) {
			let duplicateKeys = [];
			for (let j = 0; j < shareholderID.length; j++) {
				for (let k = j + 1; k < shareholderID.length; k++) {
					const key = shareholderID[k];
					const compareKey = shareholderID[j];
					if (k !== j && shareHoldersReducer[`shareholder${key}`].shareholderNRIC.value === shareHoldersReducer[`shareholder${compareKey}`].shareholderNRIC.value) {
						duplicateKeys.push(shareholderID[k]);
						duplicateKeys.push(shareholderID[j]);
					}
				}
			}
			duplicateKeys = eliminateDuplicates(duplicateKeys);
			if (duplicateKeys.length > 0) {
				for (let i = 0; i < duplicateKeys.length; i++) {
					const key = duplicateKeys[i];
					dispatch(setSignatoriesPartnersInputError(`shareholder${key}`, 'shareholderNRIC', duplicateNricMsg));
					errorCount++;
				}
			}
			shareHoldersReducer.shareholderID.map((key) => {
				const shareholderEmail = shareHoldersReducer[`shareholder${key}`].shareholderEmail;
				const shareholderNRIC = shareHoldersReducer[`shareholder${key}`].shareholderNRIC;
				const shareholderName = shareHoldersReducer[`shareholder${key}`].shareholderName;
				const shareholderMobileNo = shareHoldersReducer[`shareholder${key}`].shareholderMobileNo;

				if (!this.checkAdditionalEmptyFields(key, [shareholderEmail, shareholderNRIC, shareholderName, shareholderMobileNo], requiredMsg, handleShareHolderErrorMessage)) {
					errorCount++;
				}
				return errorCount;
			})
		}

		if (errorCount > 0) {
			return false
		}
		return true;

	}

	isAllCheckingNew() {
		const { dispatch, commonReducer, contactDetailsReducer, confirmTandCReducer, drawSignatureReducer, signatureReducer } = this.props;
		const { personType } = contactDetailsReducer;
		const { acknowledgementCheckbox } = confirmTandCReducer;
		// check steps
		const currentSection = commonReducer.currentSection;
		const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "asrDetails", "confirmDetailsPage"];
		const indexOfSteps = steps.indexOf(currentSection);

		if (indexOfSteps >= 0) {
			if (indexOfSteps > 0 && !this.isContactDetailsPassChecking()) {
				dispatch(scrollBackToSection('contactDetails'));
			}
			if (indexOfSteps === 0) {
				this.isContactDetailsPassChecking()
			}
		}

		if (indexOfSteps >= 1) {
			if (indexOfSteps > 1 && !this.isPersonalIncomeDetailsPassChecking()) {
				dispatch(scrollBackToSection('personalIncomeDetails'))
			}
			if (indexOfSteps === 1) {
				this.isPersonalIncomeDetailsPassChecking()
			}
		}

		if (personType.value === "AS" && indexOfSteps >= 4) {
			if (indexOfSteps > 4 && !this.isOperatingMandatePassing()) {
				dispatch(scrollBackToSection('operatingMandate'))
			}
			if (indexOfSteps === 4) {
				this.isOperatingMandatePassing()
			}
		}

		if (personType.value === "AS" && indexOfSteps >= 8 && !acknowledgementCheckbox.isToggled) {
			const checkboxError = commonReducer.appData.confirmDetails.confirmTandC.errorMsgCheckbox
			dispatch(handleErrorMessage("acknowledgementCheckbox", checkboxError))
			if((window.innerWidth < 992)){
				dispatch(scrollToElement("confirmationMain"));
			}else {
				dispatch(scrollToElement("confirmTandC"));
			}
		}

    if (personType.value === "AS" && indexOfSteps >= 8 && signatureReducer.signaturePad.trimmedDataURL === null) {
			const signatureUploadError = commonReducer.appData.signatureUpload.labels.error4
			dispatch(showErrorMessage(signatureUploadError));
			dispatch(scrollBackToSection('signatureUpload'));
		}

		if (personType.value === "SH" || personType.value === "CP" && indexOfSteps >= 8 && !acknowledgementCheckbox.isToggled) {
			const checkboxError = commonReducer.appData.confirmDetails.confirmTandC.errorMsgCheckbox
			dispatch(handleErrorMessage("acknowledgementCheckbox", checkboxError))
			if((window.innerWidth < 992)){
				dispatch(scrollToElement("confirmationMain"));
			}else {
				dispatch(scrollToElement("confirmTandC"));
			}
		}
	}

	handleGreyButton() {
		const { commonReducer, contactDetailsReducer, operatingMandateReducer, accountSetupReducer, confirmTandCReducer, signatureReducer, drawSignatureReducer, personalIncomeDetailsReducer } = this.props;
		const { personType, emailAddress } = contactDetailsReducer;
		const { maritalStatus } = personalIncomeDetailsReducer;
		const { acknowledgementCheckbox } = confirmTandCReducer;
		const currentSection = commonReducer.currentSection;
		const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "asrDetails", "confirmDetailsPage"];
		const indexOfSteps = steps.indexOf(currentSection);
		const { signatoriesID, mainApplicantLegalId } = operatingMandateReducer;
		const { setupBIBPlus } = accountSetupReducer;
		let errorCount = 0;

		if (indexOfSteps === 0 && !(emailAddress.isValid && emailAddress.value !== "")) {
			errorCount++;
		}

		if (indexOfSteps >= 1) {
			const maritalStatusValid = maritalStatus.value !== "" && maritalStatus.isValid;
			if (!maritalStatusValid) {
				errorCount++;
			}
		}

		if (personType.value === "AS" && indexOfSteps >= 4) {
			const isBibPlusUserToggled = setupBIBPlus.isToggled;
			if (signatoriesID.length > 0 && setupBIBPlus.isToggled) {
				operatingMandateReducer.signatoriesID.map((key) => {
					let isBibUserIdValid = true;
					const signatoriesBibUserId = operatingMandateReducer[`signatories${key}`].signatoriesBibUserId;
					const signatoriesToggleBIBUser = operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser;

					if (mainApplicantLegalId === operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value && isBibPlusUserToggled && signatoriesToggleBIBUser.isToggled) {
						isBibUserIdValid = signatoriesBibUserId.value !== "" && signatoriesBibUserId.isValid;
					}
					if (!isBibUserIdValid) {
						errorCount++;
					}
					return errorCount;
				})
			}
		}

		if (indexOfSteps >= 8) {
			if (!acknowledgementCheckbox.isToggled) {
				errorCount++;
			}

			if (personType.value === "AS") {
/*
				const signatureValidation = ((commonReducer.isMobile && drawSignatureReducer.trimmedDataURL !== null) || (!commonReducer.isMobile && signatureReducer.signaturePad.trimmedDataURL !== null));
*/
        const signatureValidation = signatureReducer.signaturePad.trimmedDataURL !== null;
				if (!signatureValidation) {
					errorCount++;
				}
			}
		}

		if (errorCount > 0) {
			return false;
		}
		return true;

	}

	renderContainer(currentSection) {
		const { contactDetailsReducer, shareHoldersReducer, applicationReducer } = this.props;
		const { shareHoldersList } = shareHoldersReducer;
		const { businessConstitution } = contactDetailsReducer;
		const { personType } = contactDetailsReducer;
		const { sessionExpirePopup } = applicationReducer;
		let isShareHoldersList = "";
		if (shareHoldersList.value.length > 0) {
			isShareHoldersList = true;
		} else {
			isShareHoldersList = false;
		}
		if (personType.value === "AS") {
			const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "asrDetails", "confirmDetailsPage"];
			const indexOfSteps = steps.indexOf(currentSection);
			return <div>
				{sessionExpirePopup && <SessionExpirePopup {...this.props} onContinue={() => this.handleToUOBSite()}/>}
				<ContactDetailsPage {...this.props} onContinue={() => this.handleInitiateApplication()}
														onCheck={() => this.isContactDetailsPassChecking()}/>
				{indexOfSteps >= 1 &&
				<PersonalIncomeDetailsPage {...this.props} onContinue={() => this.handleToCompanyDetails()}
																	 onCheck={() => this.isAllCheckingNew()}
																	 onFixedButton={() => this.handleGreyButton()}/>}
				{indexOfSteps >= 2 && <CompanyDetailsPage {...this.props} onContinue={() => this.handleToAccountSetupPage()}
																									onCheck={() => this.isAllCheckingNew()}
																									onFixedButton={() => this.handleGreyButton()}/>}
				{indexOfSteps >= 3 && <AccountSetupPage {...this.props} onContinue={() => this.handleToOperatingMandatePage()}
																								onCheck={() => this.isAllCheckingNew()}
																								onFixedButton={() => this.handleGreyButton()}/>}
				{indexOfSteps >= 4 &&
				<OperatingMandatePage {...this.props}
															onContinue={() => {
																isShareHoldersList ? this.handleToShareHoldersPage() : this.handleToTaxSelfDeclarationsPage()
															}
															} onCheck={() => this.isAllCheckingNew()}
															onHandleToReview={() => this.handleToReviewPage()}
															onFixedButton={() => this.handleGreyButton()}/>}

				{isShareHoldersList && indexOfSteps >= 5 &&
				<ShareHoldersPage {...this.props} onContinue={() => {
					this.handleToTaxSelfDeclarationsPage()
				}} onCheck={() => this.isAllCheckingNew()} onFixedButton={() => this.handleGreyButton()}/>
				}
				{indexOfSteps >= 6 && <TaxSelfDeclarationsPage {...this.props} onContinue={() => this.handleToASRDetailsPage()}
																											 onCheck={() => this.isAllCheckingNew()}
																											 onFixedButton={() => this.handleGreyButton()}/>}
				{businessConstitution.value !== 'S' && indexOfSteps >= 7 &&
				<ASRDetailsPage {...this.props} onContinue={() => this.handleToReviewPage()}
												onCheck={() => this.isAllCheckingNew()} onFixedButton={() => this.handleGreyButton()}/>}
				{indexOfSteps >= 8 && <ConfirmDetailsPage {...this.props} onContinue={() => this.handleSubmitApplication()}
																									onCheck={() => this.isAllCheckingNew()}
																									onFixedButton={() => this.handleGreyButton()}/>}
			</div>
		}

		if (personType.value === "SH" || personType.value === "CP") {
			const steps = ["contactDetails", "personalIncomeDetails", "confirmDetailsPage"];
			const indexOfSteps = steps.indexOf(currentSection);
			return <div>
				<ContactDetailsPage {...this.props} onContinue={() => this.handleInitiateApplication()}
														onCheck={() => this.isAllCheckingNew()} onFixedButton={() => this.handleGreyButton()}/>
				{indexOfSteps >= 1 && <PersonalIncomeDetailsPage {...this.props} onContinue={() => this.handleToReviewPage()}
																												 onCheck={() => this.isAllCheckingNew()}
																												 onFixedButton={() => this.handleGreyButton()}/>}
				{indexOfSteps >= 2 && <ConfirmDetailsPage {...this.props} onContinue={() => this.handleSubmitApplication()}
																									onCheck={() => this.isAllCheckingNew()}
																									onFixedButton={() => this.handleGreyButton()}/>}
			</div>
		}

	}

	handleToErrorLocal(e) {
		const { commonReducer, dispatch } = this.props;
		const duplicateTabId = commonReducer.applyDuplicateTab;
		const localStoreId = localStore.getStore("applyDuplicateTab");
		let count = 0;
		const supportOS = ["Chrome", "Firefox"];
		supportOS.map((i) => {
			const str = window.navigator.userAgent.toString();
			if (str.indexOf(i) >= 0){
				count ++;
			}
			return count
		});

		if (count > 0) {
			if(duplicateTabId !== localStoreId) {
				dispatch(setErrorWithValue("DuplicatedTabMsg"));
				this.props.history.push({
					pathname: `error`,
				})
			}
		}
	}

	componentDidMount() {
		window.addEventListener('storage', (e) => this.handleToErrorLocal(e));
	}

	componentWillUnmount() {
		window.removeEventListener('storage', (e) => this.handleToErrorLocal(e));
	}

	render() {
		const { commonReducer } = this.props;
		const currentSection = commonReducer.currentSection;

		return (
			<WrapContainer {...this.props}>
				{currentSection === "thankyou" ? <ThankYouPage/> :
					this.renderContainer(commonReducer.currentSection)
				}
			</WrapContainer>
		);
	}
}

const mapStateToProps = state => {

	const { commonReducer, applicationReducer, contactDetailsReducer, personalIncomeDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer, accountSetupReducer, confirmTandCReducer, drawSignatureReducer, signatureReducer, operatingMandateReducer, asrDetailsReducer, shareHoldersReducer, taxSelfDeclarationsReducer, uploadDocumentsReducer } = state;
	return {
		commonReducer,
		applicationReducer,
		contactDetailsReducer,
		personalIncomeDetailsReducer,
		companyBasicDetailsReducer,
		localAddressInputReducer,
		accountSetupReducer,
		confirmTandCReducer,
		signatureReducer,
		drawSignatureReducer,
		operatingMandateReducer,
		asrDetailsReducer,
		shareHoldersReducer,
		taxSelfDeclarationsReducer,
		uploadDocumentsReducer
	};
};

export default connect(mapStateToProps)(ApplicationContainer);
