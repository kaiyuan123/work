import React, { Component } from 'react';
import { connect } from 'react-redux';
import ApplyPage from './../pages/ApplyPage';
import { getInitialData } from "./../actions/commonAction";

import "./../assets/css/landing-style.css";

class ApplyContainer extends Component {
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(getInitialData());
  }

  render() {
    const { commonReducer } = this.props;
    return (
      <div className="applyPage-container">
        {commonReducer.appData !== null && <ApplyPage {...this.props}/>}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { applicationReducer, commonReducer } = state;
  return { applicationReducer, commonReducer };
}

export default connect(mapStateToProps)(ApplyContainer);
