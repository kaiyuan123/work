import React, { Component } from "react";
import TableViewCompany from "../components/TableView/TableViewCompany/TableViewCompany";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import RadioButton from "./../components/RadioButton/RadioButton";
import { mapValueToDescription } from "./../common/utils";
import { selectRadioItem, prefilledapplicantResolution, prefilledSignatorySelected } from "./../actions/asrDetailsAction";
import { connect } from "react-redux";

class ASRDetailsPage extends Component {

	componentWillMount() {
		const { dispatch, contactDetailsReducer, operatingMandateReducer } = this.props;
		const { entityType, businessConstitution } = contactDetailsReducer;
		const { signatorySelected } = operatingMandateReducer;
		dispatch(prefilledapplicantResolution(entityType, businessConstitution));
		dispatch(prefilledSignatorySelected(signatorySelected));
	}

	handleOnRadioButton(data, field) {
		const { dispatch } = this.props;
		dispatch(selectRadioItem(data, field));
	}

	selectRadioItem(data, field) {
		const { dispatch } = this.props;
		dispatch(selectRadioItem(data, field));
	}

	render() {
		const { commonReducer, asrDetailsReducer, operatingMandateReducer } = this.props;
		const { applicantResolution,
			particularsOfApprovedPersons,
			// resolutionDate,
			applicantName
		} = asrDetailsReducer;
		const { signatorySelected } = operatingMandateReducer;
		let { signingApprovedPerson } = asrDetailsReducer;
		signingApprovedPerson = signatorySelected;
		const asrDetailsValue = commonReducer.appData.asrDetails;
		const labels = asrDetailsValue.labels;
		const inputValues = commonReducer.appData.inputValues ? commonReducer.appData.inputValues : "";
		const applicantResolutionList = inputValues.applicantResolutionList;
		const signingApprovedPersonList = inputValues.signingApprovedPersonList;
		return (
			<div className="uob-content asr-details" id="asrDetails-section">
				<h1 className="sectionTitle-grey">{asrDetailsValue.title}</h1>
				<p className="uob-headline">
					{asrDetailsValue.headline}
				</p>
				{/* <div className="prefillSubtitle-container">
					<div className="prefillSubtitle-text">
						<span className="prefillSubtitle-icon">&#9432;</span>
						<div dangerouslySetInnerHTML={{ __html: asrDetailsValue.note }} ></div>
					</div>
				</div> */}
				<div className="medium-txt">
					{asrDetailsValue.certifyP1}
					{asrDetailsValue.certifyP2}
				</div>
				<div className="radiobutton-container radio-type2">
					<RadioButton
						inputID="applicantResolution"
						value={applicantResolution.value}
						radioObj={mapValueToDescription(
							applicantResolutionList
						)}
						isDisabled={true}
						errorMsg={applicantResolution.errorMsg}
						isValid={applicantResolution.isValid}
						onClick={(data) => this.handleOnRadioButton(data, "applicantResolution")}
					/>
				</div>
				<p className="labelFontWeight">{asrDetailsValue.resolution}</p>
				<h1 className="asr-heading">{asrDetailsValue.resolved}</h1>
				{/* <div className="medium-txt">{asrDetailsValue.bank}&nbsp;<span className="dash">{applicantName}</span>&nbsp;{asrDetailsValue.applicant}<br /><br /></div> */}
				<div className="agreementContent">
					<u>{asrDetailsValue.appointment}</u>
					<ol>
						<li>
							{asrDetailsValue.bank}
							&nbsp;<span className="dash">{applicantName.value}</span>&nbsp;
							{asrDetailsValue.applicant}
						</li>
					</ol>
					<div dangerouslySetInnerHTML={{ __html: asrDetailsValue.acknowledgementText }} />
				</div>

				<div className="legend-box">
					<u>{labels.legend}</u><br />
					<div>
						<div>
							<span className="legent-char">{labels.hash}</span><span className="legent-txt">{labels.hastTxt}</span>
						</div>
						<div>
							<span className="legent-char">{labels.plus}</span><span className="legent-txt">{labels.plusTxt}</span>
						</div>
						<div>
							<span className="legent-char">{labels.star}</span><span className="legent-txt">{labels.starTxt}</span>
						</div>
					</div>
				</div>
				<h1 className="sub-title-grey m-t-0 asr-heading">{asrDetailsValue.schedule}</h1>
				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow   positionRelative">
							<div className="read-only-text-input--container">
								<div>
									<label className="read-only-text-input--label--focused">{labels.appName}</label>
									<div className="read-only-text-input--input--focused">{applicantName.value || ""}</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer radio-head">
						<div className="halfRow   positionRelative">
							<div className="read-only-text-input--container">
								<div>
									<label className="read-only-text-input--label--focused">{labels.signCond}</label>
									<div className="radiobutton-container" id="signingApprovedPerson">
										<RadioButton
											inputID="signingApprovedPersonList"
											value={signingApprovedPerson.value}
											radioObj={mapValueToDescription(
												signingApprovedPersonList
											)}
											isDisabled={true}
											// errorMsg={signingApprovedPerson.errorMsg}
											// isValid={signingApprovedPerson.isValid}
											onClick={(data) => this.handleOnRadioButton(data, "signingApprovedPerson")}
										/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<h1 className="sub-title-grey">{asrDetailsValue.particularsApp}</h1>
				<div className="uob-input-separator">
					<TableViewCompany
						tableType={'particularsOfPersons'}
						colTitles={labels.particularsOfPersonsColTitle}
						tableContent={particularsOfApprovedPersons.value}
						mapItems={inputValues}
					/>
				</div>
				<h1 className="sub-title-grey ">{asrDetailsValue.particularsCert}</h1>
				<div className="uob-input-separator">
					<TableViewCompany
						tableType={'particularsOfPersons'}
						colTitles={labels.particularsOfPersonsColTitle}
						tableContent={particularsOfApprovedPersons.value}
						mapItems={inputValues}
					/>
				</div>
				<div className="legend-box m-bot0">
					{asrDetailsValue.particularsSub}
				</div>
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.props.onContinue()} isLoading={commonReducer.isProcessing} />
				</div>

			</div>
		)
	}
}

const mapStateToProps = state => {
	const { commonReducer, asrDetailsReducer, contactDetailsReducer, operatingMandateReducer } = state;
	return { commonReducer, asrDetailsReducer, contactDetailsReducer, operatingMandateReducer };
};

export default connect(mapStateToProps)(ASRDetailsPage);
