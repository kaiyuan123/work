import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCallback } from "./../actions/retrievingAction";
import { setApplyLoanData } from "./../actions/applyAction";
import localStore from './../common/localStore';
import { LocalEnvironment } from './../api/httpApi';
import queryString from 'query-string';


export class RetrievePage extends Component {
  componentWillMount() {
    const { dispatch, commonReducer } = this.props;
    const parameter = this.props.location.search;
    const dataCompanyDetailsObj = JSON.parse(localStore.getStore("dataObj"));
    const applicationId = dataCompanyDetailsObj ? dataCompanyDetailsObj.applicationId : "";
    const isCommerical = dataCompanyDetailsObj && dataCompanyDetailsObj.productCode ? dataCompanyDetailsObj.productCode.includes("cmb", 0) : false;
    dataCompanyDetailsObj && dispatch(setApplyLoanData(dataCompanyDetailsObj));
    if (parameter !== "") {
      const myInfoParams = queryString.parse(parameter);
      const error = myInfoParams.error ? myInfoParams.error : "";
      const errorDesc = myInfoParams.error_description ? myInfoParams.error_description : "";
      if (error !== "" && errorDesc !== "") {
        const localStorParms = localStore.getStore("params");
        const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
        const businessPathUrl = commonReducer.appData.pathname_uri_business;
        const applyPath = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : "/"
        this.props.history.push({
          pathname: `${applyPath}apply`,
          search: localStorParms
        })
      } else {
        const code = myInfoParams.code;
        const state = myInfoParams.state ? myInfoParams.state : "";
        const scope = commonReducer.appData.applyPage.attributes;
        const client_id = commonReducer.appData.applyPage.client_id;
        const parameterForBE = commonReducer.appData.applyPage.parameterSendBackToBE.replace("{code}", code).replace("{scope}", scope).replace("{client_id}", client_id).replace("{application_id}", applicationId).replace("{state}", state)
        dispatch(getCallback(parameterForBE, () => this.redirectToErrorPage(), (parameter) => this.redirectToApplication(parameter)))
      }
    } else {
      const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
      const businessPathUrl = commonReducer.appData.pathname_uri_business;
      const applyPath = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : "/"
      this.props.history.push({
        pathname: `${applyPath}apply`,
      })
    }
  }

  redirectToErrorPage() {
    const { commonReducer } = this.props;
    const dataCompanyDetailsObj = JSON.parse(localStore.getStore("dataObj"));
    const isCommerical = dataCompanyDetailsObj && dataCompanyDetailsObj.productCode.includes("cmb", 0);
    const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
    const businessPathUrl = commonReducer.appData.pathname_uri_business;
    const applyPath = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : "/"
    // localStore.clear();
    this.props.history.push({
      pathname: `${applyPath}error`
    })
  }

  redirectToApplication(parameter) {
    const { commonReducer } = this.props;
    const dataCompanyDetailsObj = JSON.parse(localStore.getStore("dataObj"));
    const isCommerical = dataCompanyDetailsObj.productCode.includes("cmb", 0);
    const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
    const businessPathUrl = commonReducer.appData.pathname_uri_business;
    const applyPath = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : "/"
    this.props.history.push({
      pathname: `${applyPath}application`,
      search: parameter
    })
  }

  render() {
    const { commonReducer } = this.props;
    const loadingText = commonReducer.appData.loadingPage.loadingText;
    const dataCompanyDetailsObj = JSON.parse(localStore.getStore("dataObj"));
    const isCommerical = dataCompanyDetailsObj && dataCompanyDetailsObj.productCode.includes("cmb", 0);
    const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
    const businessPathUrl = commonReducer.appData.pathname_uri_business;
    const root_path = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : ".";
    let imgUrl = `${root_path}/images/logos/uob-eBiz-Logo-blue.png`;
    return (
      <div id="loadingPage">
        <img src={imgUrl} alt="Logo" className="uob-logo" width="160" />
        <div className="loadingText" > {loadingText} </div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  const { applicationReducer, commonReducer } = state;
  return { applicationReducer, commonReducer };
}

export default connect(mapStateToProps)(RetrievePage);
