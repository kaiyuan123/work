import React, { Component } from "react";
import { connect } from "react-redux";
import closeIcon from "./../assets/images/cross-grey.svg";
import { handleHideAskQuestionPopup, handleIsChecked } from "./../actions/askQuestionsAction";
// import { askQuestionsSubmission } from "./../actions/applicationAction";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import { scrollToPopupErrorElement, scrollToElement, scrollToSection } from "./../actions/commonAction";
import { handleErrorMessage } from "./../actions/askQuestionsAction";
// import { findTrueInList } from "./../common/utils";
// import { setWhichKnockOutScenarioFalse, isKnockOutScenario } from "./../actions/applicationAction";
// import { sendDataToSparkline } from "./../common/utils";

class AskQuestionPopup extends Component {

	componentWillMount() {
		const { dispatch } = this.props;
		dispatch(scrollToElement("popupDetails"));
	}

	checkEmptyFields(fields, errorMsg, action) {
		const { dispatch } = this.props;
		let errorCount = 0;
		fields[0].map((question, i) => {
			return question.subType.length > 0 ?
				question.subType.map((quesSub, j) => {
					return quesSub.value === "" && dispatch(action(quesSub, i, j, errorMsg));
				})
				: question.value === "" ? dispatch(action(question, i, errorMsg)) : '';
		});

		dispatch(scrollToPopupErrorElement());
		if (errorCount > 0) {
			return false;
		}
		return true;
	}

	isAskQuestionPopupPassing() {
		const { commonReducer, askQuestionsReducer } = this.props;
		const { questionnaires } = askQuestionsReducer;
		let errorCount = 0;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;

		if (!this.checkEmptyFields([questionnaires[0]], requiredMsg, handleErrorMessage)) {
			errorCount++
		}

		if (errorCount > 0) {
			return false
		}
		return true;
	}

	handlePopup(popuptoggle) {
		const { dispatch } = this.props;
		dispatch(handleHideAskQuestionPopup(popuptoggle));
		dispatch(scrollToSection("companyDetails"));
	}

	handleToAccountSetupPage() {
		const { dispatch } = this.props;
		this.props.onContinue();
		dispatch(handleHideAskQuestionPopup(false));
	}

	handleOnRadioButton(value, lengthofButtonChecked, index, subindex) {
		const { dispatch } = this.props;
		dispatch(handleIsChecked(value, lengthofButtonChecked, index, subindex));
	}


	render() {
		const { commonReducer, askQuestionsReducer } = this.props;
		const askQuestionsValue = commonReducer.appData.askQuestion;
		const questionArr = askQuestionsValue.questions;
		const lengthofButtonChecked = askQuestionsReducer.noOfButtonChecked;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
		const generateQuestions = (question, key, subkey) => {
			return (
				<div className={key % 2 === 0 ? 'question-container odd-class' : 'question-container even-class'}>
					<div className="qnt-txt custom-tooltip" dangerouslySetInnerHTML={{ __html: question.desc }}></div>
					<div className="buttons-container" id={`question${question.id}`}>
						{
							!question.isValid && <span className="popup-errorMsg">{requiredMsg}</span>
						}
						<div>
							<input
								type="radio"
								id={`yes${question.id}`}
								name={`question${question.id}`}
								value={'Y'}
								onChange={(e) => this.handleOnRadioButton(e.target.value, lengthofButtonChecked, key, subkey)}
								disabled
							/>
							<label className="lbl-first" htmlFor={`yes${question.id}`}>{askQuestionsValue.yes}</label>
						</div>
						<div>
							<input
								type="radio"
								id={`no${question.id}`}
								name={`question${question.id}`}
								value={'N'}
								onChange={(e) => this.handleOnRadioButton(e.target.value, lengthofButtonChecked, key, subkey)}
								checked
								disabled
							/>
							<label className="lbl-second" htmlFor={`no${question.id}`}>{askQuestionsValue.no}</label>
						</div>
					</div>
				</div>
			)
		}

		return (
			<div>
				<div className='popup' id='popupDetails'>
					{askQuestionsReducer.popuptoggle && <style dangerouslySetInnerHTML={{ __html: `.uob-form-loan-container {overflow: hidden}` }} />}
					<div className='popup_inner'>
						<div className="inner-content">
							<span>
								<img className="close-icon" src={closeIcon} alt="Close-Icon" onClick={() => this.handlePopup(false)} />
							</span>
							<h1 className="sectionTitle">{askQuestionsValue.title}</h1>
							<div className="scrollable">
								{questionArr.map((question, i) => (
									<div key={i}>
										{question.title !== '' && <div className='question-header'>{question.title}</div>}
										{question.subType.length === 0 ? generateQuestions(question, i) :
											<div>
												<div className="sub-question-header" dangerouslySetInnerHTML={{ __html: question.desc }} />
												<div className={i % 2 === 0 ? 'odd-class child' : 'even-class'}>
													<span className="list-number" id="question3">{i + 1 + '.'}</span>
													<ol className="alpha sub-list">
														{question.subType.map((quest, j) => (
															<li key={j}>
																{generateQuestions(quest, i, j)}
															</li>
														))}
													</ol>
												</div>
											</div>
										}

									</div>
								))}


							</div>
						</div>
						<div className="uob-input-separator fixedContinue">
							<PrimaryButton label={askQuestionsValue.confirmButton} onClick={() => this.handleToAccountSetupPage(false)} isLoading={commonReducer.isProcessing} />
						</div>


						{
							lengthofButtonChecked !== askQuestionsReducer.noOfQuestions &&
							<div className="uob-input-separator fixedContinue fixedContinueGray">
								<PrimaryButton label={askQuestionsValue.nextButton} onClick={() => this.isAskQuestionPopupPassing()} isLoading={commonReducer.isProcessing} />
							</div>
						}

					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer } = state;
	return { commonReducer };
};


export default connect(mapStateToProps)(AskQuestionPopup);
