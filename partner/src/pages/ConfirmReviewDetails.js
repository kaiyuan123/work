import React, { Component } from "react";
import { connect } from "react-redux";
import Checkbox from "./../components/Checkbox/Checkbox";
import pencilIcon from "./../assets/images/pencil-icon.svg";
import { scrollBackToSection, scrollToElement } from "./../actions/commonAction";

export class ConfirmReviewDetails extends Component {
    handleScrollBackTosection(section) {
        const { dispatch } = this.props;
        dispatch(scrollBackToSection(section));
    }

    handleScrollBackToField(field) {
        const { dispatch } = this.props;
        dispatch(scrollToElement(field));
    }

    renderApprovedSignatoryList() {
        const { operatingMandateReducer, commonReducer, accountSetupReducer } = this.props;
        const isBibPlusUserToggled = accountSetupReducer.setupBIBPlus.isToggled;
        const { signatoriesID, mainApplicantLegalId } = operatingMandateReducer;

        // const length = signatoriesID.length;
        const operatingMandateValue = commonReducer.appData.operatingMandate;
        const confirmDetailsValue = commonReducer.appData.confirmDetails.ConfirmReviewDetails;
        const labels = confirmDetailsValue.subtitle;

        const tmpArray = [];
        const borderClass = !isBibPlusUserToggled ? 'bottom-border' : '';

        signatoriesID.map((key, i) => {
            let checkifMainApplicant = mainApplicantLegalId === operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value;
            const className = checkifMainApplicant ? 'main-applicant' : '';
            const applicantNumber = i + 1;
            tmpArray.push(
                <div key={key}>
                    <div className="partner-subtitle-container-confirm">
                        <div className="review-page-header-container">
                            <div className="sub-title-confirm-grey" id="">{labels.user.replace("{id}", applicantNumber)}</div>
                            <img className="review-page-pencil" src={pencilIcon} alt="pencil" onClick={() => this.handleScrollBackToField(`${key}`)} />
                        </div>
                    </div>
                    <div className="top-border">
                        <div className="fullTable confirmDetails-flexContainer">
                            <div className="right-border halfRow">
                                <div className="confirmDetails-label-container ">
                                    <div className="confirmDetails-labelValue">
                                        <div className="confirmDetails-LabelCSS"> {labels.name} </div>
                                        <div className="confirmDetails-ValueCSS"> {operatingMandateReducer[`signatories${key}`].signatoriesName.value} </div>
                                    </div>
                                </div>
                            </div>
                            <div className="halfRow">
                                <div className="confirmDetails-label-container ">
                                    <div className="confirmDetails-labelValue">
                                        <div className="confirmDetails-LabelCSS"> {labels.nric}</div>
                                        <div className="confirmDetails-ValueCSS"> {operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={`top-border ${className} ${borderClass}`}>
                        <div className="fullTable confirmDetails-flexContainer">
                            <div className="right-border halfRow">
                                <div className="confirmDetails-label-container ">
                                    <div className="confirmDetails-labelValue">
                                        <div className="confirmDetails-LabelCSS"> {labels.mobile} </div>
                                        <div className="confirmDetails-ValueCSS"> {operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.value} </div>
                                    </div>
                                </div>
                            </div>
                            <div className="halfRow">
                                <div className="confirmDetails-label-container ">
                                    <div className="confirmDetails-labelValue">
                                        <div className="confirmDetails-LabelCSS"> {labels.email}</div>
                                        <div className="confirmDetails-ValueCSS"> {operatingMandateReducer[`signatories${key}`].signatoriesEmail.value} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="withBottomBorderBIB">
                        {checkifMainApplicant && isBibPlusUserToggled ?
                            <div className={`fullTable confirmDetails-flexContainer ${className}`}>
                                <div className="halfRow">
                                    <div className="confirmDetails-label-container">
                                        <div className="confirmDetails-labelValue  ">
                                            <Checkbox
                                                description={operatingMandateValue.subHeading.labels.setupBIBUser}
                                                isChecked={operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled}
                                                isDisabled={true}
                                            />
                                        </div>
                                    </div>
                                </div>
                                {operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled &&
                                    <div className="halfRow">
                                        <div className="confirmDetails-label-container ">
                                            <div className="confirmDetails-labelValue">
                                                <div className="confirmDetails-LabelCSS">{operatingMandateValue.subHeading.labels.bibUserId}</div>
                                                <div className="confirmDetails-ValueCSS">{operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.value}</div>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                            : <div>
                                {
                                    isBibPlusUserToggled && <div className="sub-div-common display-flex">
                                        <div className="p-l-10">
                                            <div className="confirmDetails-label-container">
                                                <Checkbox
                                                    description={operatingMandateValue.subHeading.labels.setupBIBUser}
                                                    isChecked={operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled}
                                                    isDisabled={true}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        }
                    </div>
                </div>
            )
            return tmpArray;
        });
        return tmpArray;
    }

    render() {
        const { commonReducer, contactDetailsReducer, personalIncomeDetailsReducer, operatingMandateReducer, accountSetupReducer } = this.props;
        const { emailAddress } = contactDetailsReducer;
        const { setupBIBPlus } = accountSetupReducer;
        const { signatoriesID, mainApplicantLegalId } = operatingMandateReducer;
        const { maritalStatus } = personalIncomeDetailsReducer;
        const operatingMandateValue = commonReducer.appData.operatingMandate;
        const confirmDetailsValue = commonReducer.appData.confirmDetails.ConfirmReviewDetails;
        const contactDetailsValue = commonReducer.appData.contactDetails;
        const personalIncomeDetailsValue = commonReducer.appData.personalIncomeDetails;
        const labels = confirmDetailsValue.subtitle;
        const inputValues = commonReducer.appData.inputValues ? commonReducer.appData.inputValues : {};
        const maritalStatusList = inputValues.maritalStatus;
        const bLabels = contactDetailsValue.labels;
        const pLabels = personalIncomeDetailsValue.labels;
        let partnerID = "";
        let partnerLabel = "";
        const isBibPlusUserToggled = setupBIBPlus.isToggled;
        let showBibUser = false;
        signatoriesID.map((key) => {
            const signatoriesToggleBIBUser = operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser;
            if (mainApplicantLegalId === operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value && isBibPlusUserToggled && signatoriesToggleBIBUser.isToggled) {
                partnerID = key
                partnerLabel = (parseInt(key, 10) + 1).toString();
                showBibUser = true;
            }
            return (partnerID, partnerLabel, showBibUser);
        });

        return (
            <div className="reviewContent">
                <h1 className="sectionTitle-blue">{confirmDetailsValue.title}</h1>
                <h1 className="desc-grey desc-confirm">{confirmDetailsValue.subtitle.basicDetails}</h1>
                <div className="sub-div-common display-flex bottom-border p-10-0">
                    <div className="padding-0">
                        <div className="confirmDetails-label-container ">
                            <div className="confirmDetails-LabelCSS">
                                {bLabels.emailAddress}
                            </div>
                            <div className="confirmDetails-ValueCSS">
                                <div>{emailAddress.value}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="review-page-header-container">
                    <h1 className="sub-title-confirm-grey">{personalIncomeDetailsValue.subtitle}</h1>
                    <img className="review-page-pencil" src={pencilIcon} alt="pencil" onClick={() => this.handleScrollBackTosection("personalIncomeDetails")} />
                </div>
                <div className="sub-div-common display-flex bottom-border p-10-0">
                    <div className="padding-0">
                        <div className="confirmDetails-label-container">
                            <div className="confirmDetails-LabelCSS">
                                {pLabels.maritalStatus}
                            </div>
                            <div className="confirmDetails-ValueCSS">
                                <div>{maritalStatusList[maritalStatus.value]}</div>
                            </div>
                        </div>
                    </div>
                </div>
                {showBibUser &&
                    <div>
                        <h1 className="desc-grey">{confirmDetailsValue.subtitle.approvedSign}</h1>
                        <div className="review-page-header-container">
                            <h1 className="sub-title-confirm-grey">{labels.user.replace("{id}", partnerLabel)}</h1>
                            <img className="review-page-pencil" src={pencilIcon} alt="pencil" onClick={() => this.handleScrollBackTosection("operatingMandate")} />
                        </div>
                        <div className="sub-div-common display-flex bottom-border p-10-0">
                            <div className="padding-0">
                                <div className="confirmDetails-label-container">
                                    <div className="confirmDetails-LabelCSS">
                                        {operatingMandateValue.subHeading.labels.bibUserId}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        <div>{operatingMandateReducer[`signatories${partnerID}`].signatoriesBibUserId.value}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>}
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, contactDetailsReducer, personalIncomeDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer, applicationReducer } = state;
    return { commonReducer, contactDetailsReducer, personalIncomeDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer, applicationReducer };
};

export default connect(mapStateToProps)(ConfirmReviewDetails);
