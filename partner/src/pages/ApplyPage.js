import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { setApplyLoanData } from "./../actions/applyAction";
import { setErrorMessage, setCompatibilityErrorMessage, setDuplicateTabId } from "./../actions/commonAction";
import CompatibilityErrorMessage from "./../components/CompatibilityErrorMessage/CompatibilityErrorMessage";
import Loader from "./../components/Loader/Loader";
// import queryString from 'query-string';
import GenericErrorMessage from "./../components/GenericErrorMessage/GenericErrorMessage";
// import countDownImg from './../assets/images/myInfo/2-3-min.png';
// import myInfoImg from './../assets/images/myInfo/myinfo.png';
import { LocalEnvironment } from './../api/httpApi';
import { setErrorWithValue } from "./../actions/applicationAction";
// import queryString from 'query-string';
import moment from 'moment';
import localStore from './../common/localStore';

export class ApplyPage extends Component {
	componentWillMount() {
		const { dispatch, applyReducer, commonReducer } = this.props;

		  if(applyReducer.refNo === "" ) {
			  dispatch(setErrorWithValue("AppNotFound"))
			  this.props.history.push({
				  pathname: "error"
			  })
		  } else {
				const expiryDate = applyReducer.expiryDate;
				const status = applyReducer.status;
				const expiryDateFormat = moment(expiryDate.substring(0, expiryDate.indexOf(' '))).subtract(1,'d');
				const isNotExpired = moment(expiryDateFormat).isSameOrAfter(moment());
				if (status !== "PENDING") {
					dispatch(setErrorWithValue("linkExpired"))
					this.props.history.push({
						pathname: "error"
					})
				} else {
					if (!isNotExpired) {
						dispatch(setErrorWithValue("linkExpired"))
						this.props.history.push({
							pathname: "error"
						})
					} else {
						const duplicateTabs = btoa(`${moment()}&${Math.random() + 100}`);
						localStore.setStore('applyDuplicateTab',duplicateTabs);
						dispatch(setDuplicateTabId( "applyDuplicateTab" , duplicateTabs));

						let count = 0;
			      let countMobile = 0;
			      commonReducer.appData.supportOSMobile.map((i) => {
			        const str = window.navigator.userAgent.toString();
			        if (str.indexOf(i) >= 0) {
			          countMobile++;
			        }
			        return countMobile
			      });

						commonReducer.appData.supportOS.map((i) => {
							const str = window.navigator.userAgent.toString();
							if (str.indexOf(i) < 0){
								count ++;
							}
							return count
						});

			      if (countMobile > 0) {
			        dispatch(setCompatibilityErrorMessage(commonReducer.appData.supportOSTextMobile))
			      } else if (countMobile === 0 && count > 0) {
							dispatch(setCompatibilityErrorMessage(commonReducer.appData.supportOSText))
						}
					}
				}
		  }
	  }


	handleToMyInfo() {
		const { commonReducer } = this.props;
		const jsonUrl = commonReducer.appData.applyPage.mockServiceURL;
		const realUrl = commonReducer.appData.applyPage.url;
		let pathname = window.location.pathname;
		const splitPathName = pathname.split("/");
		const isCommercial = splitPathName.indexOf("corporate") === 1;
		const redirectURL = isCommercial ? encodeURIComponent(commonReducer.appData.applyPage.redirect_uri_commercial) : encodeURIComponent(commonReducer.appData.applyPage.redirect_uri_business)
		const pathnameURL = isCommercial ? commonReducer.appData.pathname_uri_commercial : commonReducer.appData.pathname_uri_business;
		const redirect_uri = LocalEnvironment ? encodeURIComponent(commonReducer.appData.applyPage.redirect_uri) : redirectURL;
		const pathname_uri = LocalEnvironment ? commonReducer.appData.applyPage.redirect_uri : pathnameURL

		const url = commonReducer.appData.isMockService ? jsonUrl.replace("{redirect_uri}", redirect_uri).replace("{pathname}", pathname_uri) : realUrl;
		window.open(url, '_parent');
	}

	handleOnClearMessage() {
		const { dispatch } = this.props;
		dispatch(setErrorMessage(""));
	}

	handleOnClearCompatibilityMessage() {
		const { dispatch } = this.props;
		dispatch(setCompatibilityErrorMessage(""));
	}

	handleToErrorLocal(e) {
		const { commonReducer, dispatch } = this.props;
		const duplicateTabId = commonReducer.applyDuplicateTab;
		const localStoreId = localStore.getStore("applyDuplicateTab");
		let count = 0;
		const supportOS = ["Chrome", "Firefox"];
		supportOS.map((i) => {
			const str = window.navigator.userAgent.toString();
			if (str.indexOf(i) >= 0){
				count ++;
			}
			return count
		});

		if (count > 0) {
		if(duplicateTabId !== localStoreId) {
			dispatch(setErrorWithValue("DuplicatedTabMsg"));
			this.props.history.push({
				pathname: `error`,
			})
		}
	}
	}

	componentDidMount() {
		window.addEventListener('storage', (e) => this.handleToErrorLocal(e));
	}

	componentWillUnmount() {
		window.removeEventListener('storage', (e) => this.handleToErrorLocal(e));
	}

	render() {
		const { commonReducer, applyReducer } = this.props;
		const code = applyReducer.productCode !== "" ? applyReducer.productCode : 'BB-BPL';
		const productTitle = commonReducer.appData.productTitleThankYou[code];
		const withMyinfoLabels = commonReducer.appData.applyPage.withMyinfo;
		const applyPageValue = commonReducer.appData.applyPage;
		const expiryDate = applyReducer.expiryDate;
		const expiryDateFormat = moment(expiryDate.substring(0, expiryDate.indexOf(' '))).subtract(1,'d').format('DD/MM/YYYY');
		const withoutErrorMsg = commonReducer.compatibilityMessage === "" ? "apply-page-body-container-padding" : "";
		return (
			<div>
				{commonReducer.isLoading && <Loader />}
				<style dangerouslySetInnerHTML={{
					__html: `
				body { padding: 0;  background: url(./images/applyPageImages/background.png); background-size: cover;
				background-repeat: no-repeat;
				background-position: 100%;
				height: 100%; width: 100%;}
				.uob-form-loan-container { background: transparent;}
				.uob-content { padding: 0; }
				#sideNav { display: none }
				.logoRight { display: block!important;  }
				#home .title2 { display: none }
				section.resume-section { padding: 32px 35px 0!important;}
				.blue-uob-logo {padding: 26px 40px}
				.apply-page-tick {
					list-style-image: url('./images/applyPageImages/tick_list.svg');
					margin: 0px
				}
				.uob-body {padding:10px;}
				`
				}} />
				{commonReducer.appData !== null &&
					<div className="apply-for-mobile">
						<div className='apply-page-header-container'>
							<div className="apply-page-header-title">
								<img alt="logo" className="img-size" src='./images/logos/uob-eBiz-Logo-blue.svg' />
								<div className="title-vertical-line" />
								<div className="title-text"> {productTitle} </div>
							</div>
							<div className="right-by-you-container">
								<img alt="right-by-you" src='./images/logos/right-by-you-logo.png' />
							</div>
						</div>
						{commonReducer.compatibilityMessage !== "" && (
							<CompatibilityErrorMessage
								{...this.props}
								interval={120}
								messageContent={commonReducer.compatibilityMessage}
								onClearMessage={this.handleOnClearCompatibilityMessage.bind(this)}
							/>
						)}
						<div className="apply-container-for-mobile">
							<div className={`apply-page-body-container ${withoutErrorMsg}`}>
								<div className="apply-page-body-left">
									<div className="body-left-content">
										<div className="body-left-title" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.title.replace("{productTitle}", productTitle) }} />
										<div className="body-left-refNo-container">
											<div className="body-left-refNo-title"> {applyPageValue.refNo} {applyReducer.refNo}</div>
										</div>
										<div className="body-left-subtite">
											<div className="body-left-left">
												<div className="body-left-left-title"> {applyPageValue.businessName} </div>
												<div className="body-left-left-desp"> {applyReducer.entityName} </div>
											</div>
											<div className="body-left-right">
												<div className="body-left-right-title"> {applyPageValue.applicationExpiry} </div>
												<div className="body-left-right-desp"> {expiryDateFormat} </div>
											</div>
										</div>
										<div className="body-left-description" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.description }} />
										<div className='body-left-button-container'>
											<div className="body-left-button" onClick={() => this.handleToMyInfo()}>{withMyinfoLabels.buttonText}</div>
										</div>
									</div>
									<div className="body-left-footer">
										<div className="body-left-footer-image" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.footerImage }} />
										<div className="body-left-footer-text" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.footerDesc }}>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				}

				{commonReducer.messageContent !== "" && (
					<GenericErrorMessage
						{...this.props}
						interval={120}
						messageContent={commonReducer.messageContent}
						onClearMessage={this.handleOnClearMessage.bind(this)}
					/>
				)}
			</div>

		);

	}

}

const mapStateToProps = (state) => {
	const { applyReducer, commonReducer, verifyReducer } = state;
	return { applyReducer, commonReducer, verifyReducer };
}

export default connect(mapStateToProps)(ApplyPage);
