import React, { Component } from "react";
import { connect } from "react-redux";
import TextInput from "./../components/TextInput/TextInput";
import Dropdown from "./../components/Dropdown/Dropdown";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import moment from 'moment';
import {
	handleTextInputChange,
	setDropdownFocusStatus,
	selectDropdownItem,
	changeSearchInputValue
} from "./../actions/contactDetailsAction";
import {
	capitalize,
	mapValueToDescription,
	mapValueToDescriptionArrayObj,
	getURLParameter,
	stringInject,
	mapValueToDescriptionRiskArrayObj
} from "../common/utils";

class ContactDetailsPage extends Component {

	constructor(props) {
		super(props);
		const code = getURLParameter(window.location.search, "code");
		const params = atob(code);
		this.ref = getURLParameter(params, "ref");
		this.id = getURLParameter(params, "id");
		this.name = getURLParameter(params, "name");
		this.time = moment().format("h:mm a");
		this.date = moment().format("DD/MM/YYYY");
		this.title = "";
	}

	componentWillMount() {
		this.title = stringInject(this.props.commonReducer.appData.contactDetails.prefillSubtitle, [this.name, this.date, this.time]);
	}

	handleOnChange(data, field) {
		const { dispatch } = this.props;
		dispatch(handleTextInputChange(data, field));
	}

	handleDropdownBlur(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(false, field));
	}

	handleDropdownFocus(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(true, field));
	}

	handleDropdownClick(data, field) {
		const { dispatch, contactDetailsReducer } = this.props;
		if (contactDetailsReducer[field].value === data.value) {
			return;
		}
		dispatch(selectDropdownItem(data.value, data.description, field));
	}

	handleOnSearchChange(e, field) {
		const { dispatch } = this.props;
		const value = e.target.value;
		dispatch(changeSearchInputValue(value, field));
	}

	handleToPersonalIncomeDetails() {
		this.props.onContinue();
	}

	isContactDetailsPassChecking() {
		this.props.onCheck();
	}

	render() {
		const { commonReducer, contactDetailsReducer, applyReducer } = this.props;
		const {
			emailAddress,
			entityName,
			uen,
			principalName,
			mobileNumber,
			alternateNames,
			entityType,
			primaryCountryOfOperation,
			secondaryNatureOfBusiness,
			primaryNatureOfBusiness,
			companyType,
			businessConstitution,
			primaryActivityDesc,
			secondaryActivityDesc,
			countryOfIncorporation,
			mainApplicantName,
			created,
			createdTime
		} = contactDetailsReducer;
		//Lists
		const errorMsgList = commonReducer.appData.errorMsgs;
		const inputValues = commonReducer.appData.inputValues
			? commonReducer.appData.inputValues
			: "";
		const countriesWithRiskMap = inputValues.countriesWithRiskMap;
		const countriesNamesMap = inputValues.countriesNamesMap;
		const entityTypeList = inputValues.entityType;
		const businessConstitutionList = inputValues.businessConstitution;
		const companyTypeList = inputValues.companyType;
		const primaryNatureOfBusinessList = inputValues.primaryNatureOfBusiness;
		const secondaryNatureOfBusinessList = primaryNatureOfBusiness.value !== "" ? inputValues[primaryNatureOfBusiness.value] : [];
		//Values
		const contactDetailsValue = commonReducer.appData.contactDetails;
		const companyDetailsValues = commonReducer.appData.companyDetails;
		const companyBasicDetailsValues = companyDetailsValues.companyBasicDetails;

		//Labels
		const cLabels = companyBasicDetailsValues.labels;
		// const createdDate = applyReducer.createdDate;
		const labels = contactDetailsValue.labels;

		//Hide, Show etc. logic
		const isDisabled = (commonReducer.currentSection !== 'contactDetails' && commonReducer.currentSection !== 'thankyou') ? true : false;
		const positionRelative = isDisabled ? "" : "positionRelative";
		const countryOfIncorporationValue = countryOfIncorporation.value !== "" && countryOfIncorporation.value !== null ? countryOfIncorporation.value : "SG";
		const isDisabledSecondary = primaryNatureOfBusiness.isInitial && primaryNatureOfBusiness.value === "" ? true : isDisabled;
		const checkBNorC = entityType.value === "LC" || entityType.value === "BN";
		//Flag img
		let countryFlagPath = countryOfIncorporationValue !== "" ? `./images/countriesFlags/${countryOfIncorporationValue.toLowerCase()}.svg` : null;

		//Validations
		const isValidEmail = emailAddress.isValid && emailAddress.value !== '';

		//Alias
		const hanYuPinYinName = alternateNames.hanYuPinYinName ? alternateNames.hanYuPinYinName : '';
		const hanYuPinYinAliasName = alternateNames.hanYuPinYinAliasName ? alternateNames.hanYuPinYinAliasName : '';
		const alias = alternateNames.alias ? alternateNames.alias : '';
		const marriedName = alternateNames.marriedName ? alternateNames.marriedName : '';
		const haveAlternateName = hanYuPinYinName === "" && hanYuPinYinAliasName === "" && alias === "" && marriedName === "";
		const otherNames = () => {
			const tmpArray = [];
			if (hanYuPinYinName !== "")
				tmpArray.push(
					<div key="alias1">
						{capitalize(hanYuPinYinName)}
					</div>
				);
			if (hanYuPinYinAliasName !== "")
				tmpArray.push(
					<div key="alias2">
						{capitalize(hanYuPinYinAliasName)}
					</div>
				);
			if (alias !== "")
				tmpArray.push(
					<div key="alias3">
						{capitalize(alias)}
					</div>
				);
			if (marriedName !== "")
				tmpArray.push(
					<div key="alias4">
						{capitalize(marriedName)}
					</div>
				);
			return tmpArray;
		};
		return (
			<div className="uob-content" id="contactDetails-section">
				<div className="prefillSubtitle-container">
					<div className="prefillSubtitle-text">
						<span className="prefillSubtitle-icon">&#9432;</span>
						<span
							dangerouslySetInnerHTML={{ __html: contactDetailsValue.prefillSubtitle.replace("{1}", mainApplicantName).replace("{2}", applyReducer.entityName).replace("{3}", created).replace("{4}", createdTime) }} />
					</div>
				</div>
				<h1 style={{ margin: '20px 0 10px' }} className="sectionTitle-blue">{contactDetailsValue.title}</h1>
				<p className="uob-headline">
					{contactDetailsValue.headline}
				</p>

				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow right-border">
							<TextInput
								inputID="entityName"
								isReadOnly={true}
								label={labels.entityName}
								value={entityName.value}
								errorMsg={entityName.errorMsg}
								onChange={data => this.handleOnChange(data, "entityName")}
								isValid={entityName.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
							/>
						</div>
						<div className="halfRow positionRelative">
							<TextInput
								inputID="uen"
								isReadOnly={true}
								label={labels.uen}
								value={uen.value}
								errorMsg={uen.errorMsg}
								onChange={data => this.handleOnChange(data, "uen")}
								isValid={uen.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
							/>
						</div>
					</div>
				</div>

				{checkBNorC ? (
					<div className="sub-div-common">
						<div className="fullTable confirmDetails-flexContainer">
							<div className={`halfRow right-border dropdown-nopadding ${positionRelative}`}>
								<Dropdown
									inputID="entityType"
									label={labels.entityType}
									isFocus={entityType.isFocus}
									value={entityType.value}
									isReadOnly={true}
									focusOutItem={true}
									dropdownItems={mapValueToDescription(
										entityTypeList
									)}
								/>
							</div>
							<div className={`halfRow dropdown-nopadding ${positionRelative}`}>
								{checkBNorC && companyType.value !== "" ?
									(
										<Dropdown
											inputID={"companyType"}
											label={labels.companyType}
											value={companyType.value}
											isFocus={companyType.isFocus}
											errorMsg={companyType.errorMsg}
											dropdownItems={mapValueToDescription(
												companyTypeList
											)}
											focusOutItem={true}
											isDisabled={isDisabled}
											isReadOnly={true}
										/>
									) : (
										<Dropdown
											inputID={"businessConstitution"}
											label={labels.businessConstitution}
											value={businessConstitution.value}
											isFocus={businessConstitution.isFocus}
											errorMsg={businessConstitution.errorMsg}
											dropdownItems={mapValueToDescription(
												businessConstitutionList
											)}
											focusOutItem={true}
											isDisabled={isDisabled}
											isReadOnly={true}
										/>
									)
								}
							</div>
						</div>
					</div>
				) : (
						<div className="sub-div-common flex-alignCenter">
							<div className="p-l-10 dropdown-nopadding">
								<Dropdown
									inputID="entityType"
									label={labels.entityType}
									isFocus={entityType.isFocus}
									value={entityType.value}
									isReadOnly={true}
									focusOutItem={true}
									dropdownItems={mapValueToDescription(
										entityTypeList
									)}
								/>
							</div>
						</div>
					)
				}
				<div className="sub-div-common">
					<div className="p-l-10">
						<div className="read-only-text-input--container">
							<label className="read-only-text-input--label--focused">
								{cLabels.primaryActivityDesc}
							</label>
							<div className="read-only-text-input--input--focused">
								{primaryActivityDesc.value !== "" && primaryActivityDesc.value !== null ? capitalize(primaryActivityDesc.value) : "-"}
							</div>
						</div>
					</div>
				</div>
				<div className="sub-div-common">
					<div className="p-l-10">
						<div className="read-only-text-input--container">
							<label className="read-only-text-input--label--focused">
								{cLabels.secondaryActivityDesc}
							</label>
							<div className="read-only-text-input--input--focused">
								{secondaryActivityDesc.value !== "" && secondaryActivityDesc.value !== null ? capitalize(secondaryActivityDesc.value) : "-"}
							</div>
						</div>
					</div>
				</div>
				<div className="sub-div-common">
					<div className="fullTable confirmDetails-flexContainer">
						<div className={`halfRow right-border ${positionRelative} dropdown-nopadding`}>
							<Dropdown
								flagImg
								inputID={"primaryCountryOfOperation"}
								label={labels.primaryCountryOfOperation}
								value={primaryCountryOfOperation.value}
								isFocus={primaryCountryOfOperation.isFocus}
								errorMsg={primaryCountryOfOperation.errorMsg}
								dropdownItems={mapValueToDescriptionRiskArrayObj(
									countriesWithRiskMap
								)}
								searchValue={primaryCountryOfOperation.searchValue}
								onBlur={this.handleDropdownBlur.bind(this, "primaryCountryOfOperation")}
								onFocus={this.handleDropdownFocus.bind(this, "primaryCountryOfOperation")}
								onClick={data => this.handleDropdownClick(data, "primaryCountryOfOperation")}
								onSearchChange={event =>
									this.handleOnSearchChange(event, "primaryCountryOfOperation")
								}
								isDisabled={isDisabled}
								isReadOnly={true}
								validator={["required"]}
								isValid={primaryCountryOfOperation.isValid}
							/>
						</div>
						<div className={`halfRow dropdown-nopaddingF`}>
							<div className="confirmDetails-label-container ">
								<div className="confirmDetails-labelValue">
									<div className="confirmDetails-LabelCSS">
										{cLabels.countryOfIncorporation}
									</div>
									<div className="confirmDetails-ValueCSS">
										<img className="dropdown-flagImg" onError={(e) => e.target.style.display = 'none'}
											src={countryFlagPath} alt='icon' />
										{countriesNamesMap[countryOfIncorporationValue]}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="sub-div-common">
					<div className="fullTable confirmDetails-flexContainer">
						<div className={`halfRow right-border dropdown-nopadding ${positionRelative}`}>
							<Dropdown
								inputID="primaryNatureOfBusiness"
								label={labels.natureOfBusiness}
								isFocus={primaryNatureOfBusiness.isFocus}
								value={primaryNatureOfBusiness.value}
								errorMsg={primaryNatureOfBusiness.errorMsg}
								focusOutItem={true}
								dropdownItems={mapValueToDescriptionArrayObj(
									primaryNatureOfBusinessList
								)}
								searchValue={primaryNatureOfBusiness.searchValue}
								onBlur={this.handleDropdownBlur.bind(this, "primaryNatureOfBusiness")}
								onFocus={this.handleDropdownFocus.bind(this, "primaryNatureOfBusiness")}
								onClick={data => this.handleDropdownClick(data, "primaryNatureOfBusiness")}
								onSearchChange={event =>
									this.handleOnSearchChange(event, "primaryNatureOfBusiness")
								}
								isDisabled={isDisabled}
								isReadOnly={true}
								validator={["required"]}
								isValid={primaryNatureOfBusiness.isValid}
							/>
						</div>
						<div className={`halfRow dropdown-nopadding ${positionRelative}`}>
							<Dropdown
								inputID="secondaryNatureOfBusiness"
								label={labels.secondaryNatureOfBusiness}
								isFocus={secondaryNatureOfBusiness.isFocus}
								value={secondaryNatureOfBusiness.value}
								errorMsg={secondaryNatureOfBusiness.errorMsg}
								focusOutItem={true}
								dropdownItems={mapValueToDescriptionArrayObj(
									secondaryNatureOfBusinessList
								)}
								searchValue={secondaryNatureOfBusiness.searchValue}
								onBlur={this.handleDropdownBlur.bind(this, "secondaryNatureOfBusiness")}
								onFocus={this.handleDropdownFocus.bind(this, "secondaryNatureOfBusiness")}
								onClick={data => this.handleDropdownClick(data, "secondaryNatureOfBusiness")}
								onSearchChange={event =>
									this.handleOnSearchChange(event, "secondaryNatureOfBusiness")
								}
								isDisabled={isDisabledSecondary}
								isReadOnly={true}
								validator={["required"]}
								isValid={secondaryNatureOfBusiness.isValid}
								errorClick={() => this.handleErrorSeconday("secondaryNatureOfBusiness")}
								notAbleToSelect={primaryNatureOfBusiness.value === ""}
							/>
						</div>
					</div>
				</div>
				<div className="sub-div-common">
					<div className="p-l-10">
						<TextInput
							inputID="principalName"
							label={labels.principalName}
							value={capitalize(principalName.value)}
							errorMsg={principalName.errorMsg}
							onChange={data => this.handleOnChange(data, "principalName")}
							isValid={principalName.isValid}
							validator={["required"]}
							errorMsgList={errorMsgList}
							isReadOnly={true}
							hasIcon={false}
						/>
					</div>
				</div>
				<div className="sub-div-common alignCenter" style={{ minHeight: '70px', height: 'initial' }}>
					<div className="fullTable confirmDetails-flexContainer">
						<div className="p-l-10">
							<div className="confirmDetails-label-container ">
								<div className="confirmDetails-labelValue">
									<div className="confirmDetails-LabelCSS">
										{labels.alias}
									</div>
									<div className="confirmDetails-ValueCSS">
										{!haveAlternateName ?
											(
												<div>
													{otherNames().length > 0 && (
														<div>{otherNames()}</div>
													)}
												</div>
											) : (
												<div>-</div>
											)
										}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="top-border bottom-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow right-border">
							<TextInput
								type="Phone"
								inputID="mobileNumber"
								isReadOnlyMobile={true}
								label={labels.mobileNumber}
								value={mobileNumber.value}
								errorMsg={mobileNumber.errorMsg}
								onChange={data => this.handleOnChange(data, "mobileNumber")}
								isValid={mobileNumber.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
								onlySg={false}
							/>
						</div>
						<div className={`halfRow ${positionRelative}`}>
							<TextInput
								inputID="email"
								isReadOnly={false}
								label={labels.emailAddress}
								value={emailAddress.value}
								errorMsg={emailAddress.errorMsg}
								onChange={data => this.handleOnChange(data, "emailAddress")}
								isValid={emailAddress.isValid}
								validator={["required", "isEmail", "maxSize|30"]}
								errorMsgList={errorMsgList}
								hasIcon={true}
								isInitial={emailAddress.isInitial}
								isDisabled={isDisabled}
								isMyInfo={emailAddress.isMyInfo}
								isFocus={emailAddress.isFocus}
								hasEdit={emailAddress.hasEdit}
							/>
						</div>
					</div>
				</div>

				<div className="uob-terms" dangerouslySetInnerHTML={{ __html: contactDetailsValue.terms }} />
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToPersonalIncomeDetails()}
						isLoading={commonReducer.isProcessing} />
				</div>
				{
					!isValidEmail &&
					<div>
						<div className="uob-input-separator fixedContinue fixedContinueGray">
							<PrimaryButton label={labels.continueButton} onClick={() => this.isContactDetailsPassChecking()}
								isLoading={commonReducer.isProcessing} />
						</div>
					</div>
				}
			</div >
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, contactDetailsReducer, companyBasicDetailsReducer, applyReducer } = state;
	return { commonReducer, contactDetailsReducer, companyBasicDetailsReducer, applyReducer };
};

export default connect(mapStateToProps)(ContactDetailsPage);
