//This is for combining AccountDetails.js and AccountBiBPlus.js
import React, { Component } from "react";
import { connect } from "react-redux";
import AccountDetails from "./AccountDetails";
import AccountBiBPlus from "./AccountBiBPlus";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";

class AccountSetupPage extends Component {
	handleToOperatingMandatePage() {
		this.props.onContinue();
	}

	isAccountSetupPassing() {
		this.props.onCheck()
	}

	render() {
		const { commonReducer } = this.props;
		const accountSetupValue = commonReducer.appData.accountSetup;
		const labels = accountSetupValue.labels;
		return (
			<div id="accountSetup-section">
				<AccountDetails {...this.props} />
				<AccountBiBPlus {...this.props} />
				<div className="uob-input-separator"/>
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToOperatingMandatePage()}/>
				</div>
				{!this.props.onFixedButton() &&
					<div className="uob-input-separator fixedContinue fixedContinueGray">
						<PrimaryButton label={labels.continueButton} onClick={() => this.isAccountSetupPassing()}/>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, accountSetupReducer, contactDetailsReducer } = state;
	return { commonReducer, accountSetupReducer, contactDetailsReducer };
};

export default connect(mapStateToProps)(AccountSetupPage);
