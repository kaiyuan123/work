import React, { Component } from "react";
import { connect } from "react-redux";

class LoadingPage extends Component {
	render() {
		const { commonReducer } = this.props;
		const loadingText = commonReducer.appData.loadingPage.loadingText;
		let imgUrl = "./images/logos/uob-eBiz-Logo-blue.png";
		return (
			<div id="loadingPage">
				<img src={imgUrl} alt="Logo" className="uob-logo" width="160" />
				<div className="loadingText" > {loadingText} </div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer } = state;
	return { commonReducer };
};

export default connect(mapStateToProps)(LoadingPage);
