import React, { Component } from "react";
import { connect } from "react-redux";
// import Dropdown from "./../components/Dropdown/Dropdown";
// import TextInput from "./../components/TextInput/TextInput";
import { capitalize } from "./../common/utils";
import moment from 'moment';
// import Checkbox from "./../components/Checkbox/Checkbox";
import { handleIsChecked, setDropdownFocusStatus, selectDropdownItem, handleTextInputChange, changeSearchInputValue, selectRadioItem } from "./../actions/companyBasicDetailsAction";
import LocalAddressInput from "./../components/Uob/LocalAddressInput/LocalAddressInput";
import Checkbox from "./../components/Checkbox/Checkbox";

class CompanyBasicDetails extends Component {


    handleOnChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChange(data, field));
    }

    handleOnCheckbox(field, isToggled) {
        const { dispatch } = this.props;
        dispatch(handleIsChecked(field, isToggled));
    }

    handleDropdownBlur(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(false, field));
    }

    handleDropdownFocus(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(true, field));
    }

    handleDropdownClick(data, field) {
        const { dispatch, companyBasicDetailsReducer } = this.props;

        if (companyBasicDetailsReducer[field].value === data.value) {
            return;
        }

        dispatch(selectDropdownItem(data.value, field, data.description));
    }

    handleOnSearchChange(e, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValue(value, field));
    }

    handleRadioSelection(data, field) {
        const { dispatch } = this.props;
        dispatch(selectRadioItem(data, field));
    }

    render() {
        const { commonReducer, companyBasicDetailsReducer } = this.props;
        const { companyRegistrationNumber, registeredCompanyName, companyStatus, ownership, registrationDate, companyExpiryDate, annualTurnover, mailingAddressCheckbox, readOnly } = companyBasicDetailsReducer;

        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";
        const companyDetailsValues = commonReducer.appData.companyDetails;
        const companyBasicDetailsValues = companyDetailsValues.companyBasicDetails;
        const labels = companyBasicDetailsValues.labels;

        const errorMsgList = commonReducer.appData.errorMsgs;
        const ownershipList = inputValues.ownership;
        const ownershipValue = ownership.value !== "" ? ownership.value : "NA";
        const registrationDateFormat = registrationDate.value !== "" && moment(registrationDate.value, "YYYY-MM-DD").isValid() ? moment(registrationDate.value, "YYYY-MM-DD").format("DD/MM/YYYY") : "-";
        const companyExpiryDateFormat = companyExpiryDate.value !== "" && moment(companyExpiryDate.value, "YYYY-MM-DD").isValid() ? moment(companyExpiryDate.value, "YYYY-MM-DD").format("DD/MM/YYYY") : "-";

        return (
            <div className="uob-content p-b-0">
                <h1 className="sectionTitle">{companyDetailsValues.title}</h1>
                <div className="uob-form-separator" />
                <p className="uob-headline">{companyDetailsValues.headline}</p>
                <h1 className="sub-title-grey">{companyBasicDetailsValues.subtitle}</h1>

                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="halfRow right-border   ">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyRegistrationNumber}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {companyRegistrationNumber.value}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.registeredCompanyName}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {registeredCompanyName.value}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="halfRow right-border   ">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.ownership}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {ownershipList[ownershipValue]}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow  ">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyStatus}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {capitalize(companyStatus.value)}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-border bottom-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="halfRow right-border   ">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.registrationDate}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {registrationDateFormat}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow  ">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyExpiryDate}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {companyExpiryDateFormat}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="sub-title-grey" id="annualTurnover"> {labels.annualTurnover} </div>
                <div className="sub-div-common flex-alignCenter bottom-border">
                    <div className="p-l-10 fullWidth">
                        <div className="confirmDetails-label-container ">
                            <div className="confirmDetails-labelValue">
                                <div className="confirmDetails-LabelCSS">
                                    {labels.annualTurnoverDesc}
                                </div>
                                <div className="confirmDetails-ValueCSS">
                                    {annualTurnover.value}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`multicheckbox-container ${readOnly ? "multicheckbox-container--read-only" : ""}`} id="primaryClientele">
                    <span className="checkBoxBottomSub checkbox-headlabel">{labels.primaryClientele}</span>                    
                    <Checkbox
                        description={labels.corporateClients}
                        isChecked={companyBasicDetailsReducer.corporateClients.isToggled}
                        isDisabled={readOnly}
                    />
                    <Checkbox
                        description={labels.individualClients}
                        isChecked={companyBasicDetailsReducer.individualClients.isToggled}
                        isDisabled={readOnly}
                    />
                </div>
                <h1 className="sub-title-grey">{companyBasicDetailsValues.subtitle2}</h1>

                <LocalAddressInput
                    labels={labels}
                    errorMsgList={errorMsgList}
                    addressType="company"
                    inputValues={inputValues}
                />
                {mailingAddressCheckbox.isToggled &&
                    <div>
                        <h1 className="sub-title-grey">{companyBasicDetailsValues.mailingSubtitle}</h1>
                        <div className="mailingDetails-section">
                            <LocalAddressInput
                                labels={labels}
                                errorMsgList={errorMsgList}
                                addressType="mailing"
                                inputValues={inputValues}
                            />
                        </div>
                    </div>
                }
            </div >
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, companyBasicDetailsReducer, localAddressInputReducer } = state;
    return { commonReducer, companyBasicDetailsReducer, localAddressInputReducer };
};

export default connect(mapStateToProps)(CompanyBasicDetails);
