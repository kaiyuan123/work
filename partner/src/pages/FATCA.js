import React, { Component } from "react";
import { connect } from "react-redux";
import TableViewFATCA from "./../components/TableView/TableViewFATCA/TableViewFATCA";
import Dropdown from "./../components/Dropdown/Dropdown";
import TextInput from "./../components/TextInput/TextInput";
import { mapValueToDescription } from "../common/utils";
import Checkbox from "./../components/Checkbox/Checkbox";
import RadioButton from "./../components/RadioButton/RadioButton";
import { capitalize } from "./../common/utils";

class FATCA extends Component {

    renderOthers(cpKey) {
        const { taxSelfDeclarationsReducer, commonReducer } = this.props;

        const tinNoOthersId = taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthersId`];

        const excludes = ["SG", "US"];
        //haveTINNo mapping
        const length = tinNoOthersId.length;
        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        const countriesNamesMap = inputValues.countriesNamesMap;
        const reasonsList = inputValues.reasons;

        const tmpArray = [];

        tinNoOthersId.map((key, i) => {
            const bottomBorder = taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "Y" ? "" : "bottom-border";
            tmpArray.push(
                <div key={key} >
                    {length > 1 &&
                        <div className="crs-others-container">
                            <h1 className="sub-title-grey partner-subtitle">{i + 1}</h1>
                        </div>
                    }
                    <div className="crs-container">
                        <div className={`top-border ${bottomBorder}`}>
                            <div className="fullTable confirmDetails-flexContainer">
                                <div className="halfRow right-border positionRelative dropdown-nopadding">
                                    <Dropdown
                                        isReadOnly={true}
                                        flagImg
                                        inputID={`othersCountry${cpKey}${key}`}
                                        label={labels.othersCountry}
                                        value={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.value}
                                        errorMsg={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.errorMsg}
                                        isValid={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.isValid}
                                        isFocus={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.isFocus}
                                        dropdownItems={mapValueToDescription(
                                            countriesNamesMap
                                        )}
                                        searchValue={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.searchValue}
                                        excludes={excludes}
                                    />
                                </div>
                                <div className="halfRow positionRelative">
                                    <div className="crs-buttons p-t-5">
                                        <div className="txt-container">
                                            <span className="crs-txt">{labels.haveTINNo}</span>
                                            {!taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.isValid &&
                                                <span className="errorMsg">{labels.errorMsgRequired}</span>}
                                        </div>
                                        <div className="flex-1">
                                            <input type="radio" id={`othersYes${cpKey}${key}`} checked={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "Y" ? true : false} name={`radioButtonOthers${cpKey}${key}`} value="Y" disabled={true} />
                                            <label className="lbl-first" htmlFor={`othersYes${cpKey}${key}`}>{labels.yesButton}</label>

                                            <input type="radio" id={`othersNo${cpKey}${key}`} name={`radioButtonOthers${cpKey}${key}`} value="N" checked={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "N" ? true : false} disabled={true} />
                                            <label className="lbl-second" htmlFor={`othersNo${cpKey}${key}`}>{labels.noButton}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "Y" &&
                            <div className="sub-div-common bottom-border">
                                <div className="p-l-10 positionRelative">
                                    <TextInput
                                        onChange={null}
                                        isReadOnly={true}
                                        inputID={`othersTINNo${cpKey}${key}`}
                                        label={labels.othersTINNo}
                                        value={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersTINNo.value}
                                        hasIcon={false}
                                    />
                                </div>
                            </div>
                        }

                        {taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "N" &&
                            <div>
                                <div className="crs-txt-container">
                                    <span className="crs-txt crs-questions">{labels.reason}</span>
                                </div>
                                <RadioButton
                                    inputID={`reasons${cpKey}${key}`}
                                    value={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].reasons.value}
                                    radioObj={mapValueToDescription(
                                        reasonsList
                                    )}
                                    isDisabled={true}
                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].reasons.errorMsg}
                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].reasons.isValid}
                                    onClick={(data) => this.handleRadioSelection(data, `controllingPerson${cpKey}`, `tinNoOthers${key}`, "reasons")}
                                />
                                {taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].reasons.value === "3" &&
                                    <div className="p-l-65">
                                        <div className="others-container">
                                            <div className="others-label p-r-0">{labels.othersNone}</div>
                                            <div className="bottom-border positionRelative width-others">
                                                <TextInput
                                                    inputID={`othersOthers${cpKey}${key}`}
                                                    maxCharacters={70}
                                                    label={labels.othersNone}
                                                    value={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersOthers.value}
                                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersOthers.errorMsg}
                                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersOthers.isValid}
                                                    onChange={(data) => this.handleOnTextChangeOthers(data, `controllingPerson${cpKey}`, `tinNoOthers${key}`, 'othersOthers')}
                                                    validator={["required", "maxSize|70"]}
                                                    hasIcon={false}
                                                    isReadOnly={true}
                                                    type="Others"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        }
                    </div>
                </div>
            );
            return tmpArray;
        })
        return tmpArray;

    }

    renderControllingPerson() {
        const { taxSelfDeclarationsReducer, commonReducer } = this.props;
        const { controllingPersonId } = taxSelfDeclarationsReducer;

        //controllingPerson mapping
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        const tmpArray = [];
        const errorMsgList = commonReducer.appData.errorMsgs;
        controllingPersonId.map((key, i) => {
            const applicantNumber = i + 1;
            tmpArray.push(
                <div key={key} style={{ paddingTop: "30px" }}>
                    <div className="partner-subtitle-container" id={`cp${key}`}>
                        <div>
                            <h1 className="sub-title-grey partner-subtitle">{applicantNumber}</h1>
                            <h1 className="sub-title-grey cp-title">{labels.cpBasicDetails}</h1>
                        </div>
                    </div>

                    <div className="top-border">
                        <div className="fullTable confirmDetails-flexContainer">
                            <div className="halfRow right-border positionRelative">
                                <TextInput onChange={null}
                                    isReadOnly={true}
                                    inputID={`cpName${key}`}
                                    label={labels.cpName}
                                    value={capitalize(taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.value)}
                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.errorMsg}
                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.isValid}
                                    hasIcon={false}
                                />

                            </div>
                            <div className="halfRow positionRelative">
                                <TextInput onChange={null}
                                    isReadOnly={true}
                                    inputID={`cpIdNo${key}`}
                                    label={labels.cpIdNo}
                                    isUpperCase
                                    value={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.value}
                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.errorMsg}
                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.isValid}
                                    hasIcon={false}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="top-border">
                        <div className="fullTable confirmDetails-flexContainer">
                            <div className="halfRow right-border">
                                <TextInput onChange={null}
                                    isReadOnlyMobile={true}
                                    type="Phone"
                                    inputID={`cpMobileNo${key}`}
                                    label={labels.cpMobileNo}
                                    value={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo.value}
                                    validator={["required", "isPhoneNumber"]}
                                    errorMsgList={errorMsgList}
                                    hasIcon={false}
                                    onlySg={false}
                                />
                            </div>
                            <div className="halfRow positionRelative">
                                <TextInput onChange={null}
                                    isReadOnly={true}
                                    inputID={`cpEmail${key}`}
                                    label={labels.cpEmail}
                                    value={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpEmail.value}
                                    hasIcon={false} />
                            </div>
                        </div>
                    </div>
                    <div className="sub-div-common bottom-border positionRelative">
                        <div className="p-l-10">
                            <TextInput onChange={null}
                                isReadOnly={true}
                                isNumber
                                inputID={`cpPercentageOwnership${key}`}
                                label={labels.cpPercentageOwnership}
                                value={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership.value}
                                hasIcon={false}
                            />
                        </div>
                    </div>
                    <div>
                    </div>
                    <div className="partner-subtitle-container">
                        <div>
                            <h1 className="sub-title-grey">{labels.cpTaxResidency}</h1>
                            <h1 className="sub-headline">{labels.cpTaxResidencyDesc}</h1>
                        </div>
                    </div>
                    <div id={`taxResidency${key}`}>
                        <Checkbox
                            inputID={`checkboxSG${key}`}
                            description={labels.checkboxSG}
                            isChecked={taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxSG.isToggled}
                            isDisabled={true}
                        />
                        {taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxSG.isToggled &&
                            <div className="crs-container">
                                <div className="sub-div-common bottom-border">
                                    <div className="p-l-10 positionRelative">
                                        <TextInput onChange={null}
                                            isReadOnly={true}
                                            inputID={`tinNoSG${key}`}
                                            label={labels.tinNoSG}
                                            value={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoSG.value}
                                            hasIcon={false}
                                        />
                                    </div>
                                </div>
                            </div>
                        }
                        <Checkbox
                            inputID={`checkboxUS${key}`}
                            description={labels.checkboxUS}
                            isChecked={taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxUS.isToggled}
                            isDisabled={true}
                        />
                        {taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxUS.isToggled &&
                            <div className="crs-container">
                                <div className="sub-div-common bottom-border">
                                    <div className="p-l-10 positionRelative">
                                        <TextInput onChange={null}
                                            isReadOnly={true}
                                            inputID="tinNoUS"
                                            label={labels.tinNoUS}
                                            value={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoUS.value}
                                            errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoUS.errorMsg}
                                            hasIcon={false}
                                        />
                                    </div>
                                </div>
                            </div>
                        }
                        <Checkbox
                            inputID={`checkboxOthers${key}`}
                            description={labels.checkboxOthers}
                            isChecked={taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxOthers.isToggled}
                            isDisabled={true}
                        />
                        {taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxOthers.isToggled &&
                            <div>
                                {this.renderOthers(key)}
                            </div>


                        }
                    </div>
                </div>
            );
            return tmpArray;
        })
        return tmpArray;
    }

    render() {
        const { commonReducer, taxSelfDeclarationsReducer } = this.props;
        const { fatcaCRSStatus } = taxSelfDeclarationsReducer;
        //Lists
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        const checkFATCACRSStatus = fatcaCRSStatus.isValid && fatcaCRSStatus.value !== "";
        const paddingTop = !checkFATCACRSStatus ? "0px" : "60px";
        return (
            <div style={{ paddingTop: paddingTop }} id="fatca-section">
                {!checkFATCACRSStatus &&
                    <div style={{ paddingTop: "60px" }}
                        className="radio-errorMsg">
                        {labels.errorMsgFatca}
                    </div>
                }
                <h1 className="desc-grey" style={{ marginTop: "0px" }}>{taxSelfDeclarationsValue.fatcaTitle}</h1>
                <p className="sub-headline">
                    {taxSelfDeclarationsValue.fatcaHeadline}
                </p>
                <TableViewFATCA
                    colTitles={labels.fatcaColTitle}
                    labels={labels}
                    inputID={fatcaCRSStatus.name}
                    flexTableWidth={900}
                />

                {fatcaCRSStatus.value === "PB" &&
                    <div>
                        <h1 className="desc-grey">
                            {taxSelfDeclarationsValue.cpTitle}
                        </h1>
                        <p className="sub-headline" dangerouslySetInnerHTML={{ __html: taxSelfDeclarationsValue.cpHeadline }} />
                        <p className="sub-headline-bold">{taxSelfDeclarationsValue.cpSubHeadline}</p>
                        {this.renderControllingPerson()}
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, taxSelfDeclarationsReducer } = state;
    return { commonReducer, taxSelfDeclarationsReducer };
};

export default connect(mapStateToProps)(FATCA);
