import React, { Component } from 'react';
import { connect } from 'react-redux';
import greenTick from './../assets/images/green-tick-successful.svg';
import MessageBox from './../components/MessageBox/MessageBox';
import moment from "moment";

class ThankYouPage extends Component {

  render() {
    const { commonReducer, applyReducer } = this.props;
    const { productCode } = applyReducer;
    const thankyouValues = commonReducer.appData.thankYouPage;
    const referenceNo = commonReducer.referenceNo;    
    const applicationExpiryDate = commonReducer.applicationExpiryDate;
    const date = moment().format("DD/MM/YYYY");
    const code = productCode.toUpperCase();
    const productTitle = commonReducer.appData.productTitleThankYou;
    const codeDesc = productTitle[code]
    const subTitle = thankyouValues.subtitle.replace("{referenceNo}", referenceNo).replace("{date}", date).replace("{productTitle}", codeDesc).replace("{applicationExpiryDate}", applicationExpiryDate);;
    return (
      <div id="loadingPage" className = 'uob-content'>
        <div className="error-pg-container">
          <MessageBox
            isThankyou
            img={greenTick}
            title={thankyouValues.title}
            subtitle={subTitle}
            description={thankyouValues.description}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { commonReducer, applyReducer } = state;
  return { commonReducer, applyReducer };
}

export default connect(mapStateToProps)(ThankYouPage);
