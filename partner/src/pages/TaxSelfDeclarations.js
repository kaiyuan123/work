import React, { Component } from "react";
import { connect } from "react-redux";
import TextInput from "./../components/TextInput/TextInput";
import Dropdown from "./../components/Dropdown/Dropdown";
import Checkbox from "./../components/Checkbox/Checkbox";
import RadioButton from "./../components/RadioButton/RadioButton";
import { handleIsChecked, handleTextInputChange, handleButtonChange, setDropdownFocusStatus, setDropdownFocusStatusChange, selectDropdownItemChange, selectDropdownItem, handleTextInputChangeOthers, changeSearchInputValueOther, changeSearchInputValue, handleButtonChangeOthers, selectRadioItem, incrementCountry, addNewCountry, uncheckRest, uncheckNone, decreaseCountry, removeCountry } from "./../actions/taxSelfDeclarationsAction";
import { mapValueToDescription } from "../common/utils";

class TaxSelfDeclarations extends Component {

    handleRadioSelection(data, key, field) {
        const { dispatch } = this.props;
        dispatch(selectRadioItem(data, key, field));
    }

    //Delete
    deleteCountry(key) {
        const { dispatch } = this.props;
        dispatch(decreaseCountry(key));
        dispatch(removeCountry(key));

    }

    handleOnCheckbox(field, isToggled) {
        const { dispatch } = this.props;
        if (field === "checkboxNone") {
            dispatch(uncheckRest());
        } else {
            dispatch(uncheckNone());
        }
        dispatch(handleIsChecked(field, isToggled));
    }

    handleOnSearchChange(e, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValue(value, field));
    }

    handleOnSearchChangeOthers(e, key, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValueOther(value, key, field));
    }

    handleOnChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChange(data, field));
    }

    handleOnTextChangeOthers(data, key, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChangeOthers(data, key, field));
    }

    handleOnButtonChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleButtonChange(data, field));
    }

    handleOnButtonChangeOthers(data, key, field) {
        const { dispatch } = this.props;
        dispatch(handleButtonChangeOthers(data, key, field));
    }

    handleDropdownFocus(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(true, field));
    }

    handleDropdownFocusChange(key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChange(true, key, field));
    }

    handleDropdownBlur(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(false, field));
    }

    handleDropdownBlurChange(key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChange(false, key, field));
    }
    handleDropdownClick(data, field) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        if (taxSelfDeclarationsReducer[field].value === data.value) {
            return;
        }
        dispatch(selectDropdownItem(data.value, data.description, field));
    }

    handleDropdownClickChange(data, key, field) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        if (taxSelfDeclarationsReducer[key][field].value === data.value) {
            return;
        }

        dispatch(selectDropdownItemChange(data.value, data.description, key, field));
    }

    addhaveTINNo() {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        const tinNoOthersId = taxSelfDeclarationsReducer.tinNoOthersId;
        const maxKey = Math.max.apply(null, tinNoOthersId);
        dispatch(incrementCountry(maxKey));
        dispatch(addNewCountry(maxKey));

    }


    renderCountries() {
        const { taxSelfDeclarationsReducer, commonReducer } = this.props;
        const { tinNoOthersId } = taxSelfDeclarationsReducer;
        const excludes = ["SG", "US"];
        //haveTINNo mapping
        const length = tinNoOthersId.length;
        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        const countriesNamesMap = inputValues.countriesNamesMap;
        const reasonsList = inputValues.reasons;

        const tmpArray = [];
        const errorMsgList = commonReducer.appData.errorMsgs;

        tinNoOthersId.map((key, i) => {
            const bottomBorder = taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "Y" ? "" : "bottom-border";
            tmpArray.push(
                <div key={key} >
                    {length > 1 &&
                        <div className="crs-others-container">
                            <h1 className="sub-title partner-subtitle">{i + 1}</h1>
                        </div>
                    }
                    <div className="crs-container">
                        <div className={`top-border ${bottomBorder}`}>
                            <div className="fullTable confirmDetails-flexContainer">
                                <div className="halfRow right-border positionRelative dropdown-nopadding">
                                    <Dropdown
                                        isReadOnly={true}
                                        flagImg
                                        inputID={`othersCountry${key}`}
                                        label={labels.othersCountry}
                                        value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value}
                                        errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.errorMsg}
                                        isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.isValid}
                                        isFocus={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.isFocus}
                                        dropdownItems={mapValueToDescription(
                                            countriesNamesMap
                                        )}
                                        searchValue={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.searchValue}
                                        onBlur={this.handleDropdownBlurChange.bind(this, `tinNoOthers${key}`, 'othersCountry')}
                                        onFocus={this.handleDropdownFocusChange.bind(this, `tinNoOthers${key}`, 'othersCountry')}
                                        onClick={data => this.handleDropdownClickChange(data, `tinNoOthers${key}`, 'othersCountry')}
                                        onSearchChange={event =>
                                            this.handleOnSearchChangeOthers(event, `tinNoOthers${key}`, 'othersCountry')
                                        }
                                        validator={["required"]}
                                        excludes={excludes}
                                    />
                                </div>
                                <div className="halfRow   positionRelative">
                                    <div className="crs-buttons p-t-5">
                                        <div className="txt-container">
                                            <span className="crs-txt">{labels.haveTINNo}</span>{!taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.isValid &&
                                                <span className="errorMsg">{labels.errorMsgRequired}</span>}
                                        </div>
                                        <div className="flex-1">
                                            <input type="radio" id={`othersYes${key}`} checked={taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "Y" ? true : false} name={`radioButtonOthers${key}`} value="Y" disabled={true} />
                                            <label className="lbl-first" htmlFor={`othersYes${key}`}>{labels.yesButton}</label>

                                            <input type="radio" id={`othersNo${key}`} name={`radioButtonOthers${key}`} value="N" checked={taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "N" ? true : false} disabled={true} />
                                            <label className="lbl-second" htmlFor={`othersNo${key}`}>{labels.noButton}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "Y" &&
                            <div className="sub-div-common bottom-border">
                                <div className="p-l-10 positionRelative">
                                    <TextInput
                                        isReadOnly={true}
                                        inputID={`othersTINNo${key}`}
                                        label={labels.othersTINNo}
                                        value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.value}
                                        errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.errorMsg}
                                        isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.isValid}
                                        onChange={(data) => this.handleOnTextChangeOthers(data, `tinNoOthers${key}`, 'othersTINNo')}
                                        errorMsgList={errorMsgList}
                                        validator={["required", "isAlphanumeric", "maxSize|19"]}
                                        hasIcon={false}
                                    />
                                </div>
                            </div>
                        }

                        {taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "N" &&
                            <div>
                                <div className="crs-txt-container">
                                    <span className="crs-txt crs-questions">{labels.reason}</span>
                                </div>
                                <RadioButton
                                    inputID={`reasons${key}`}
                                    value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.value}
                                    radioObj={mapValueToDescription(
                                        reasonsList
                                    )}
                                    isDisabled={true}
                                    errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.errorMsg}
                                    isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.isValid}
                                    onClick={(data) => this.handleRadioSelection(data, `tinNoOthers${key}`, "reasons")}
                                />
                                {taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.value === "3" &&
                                    <div className="p-l-65">
                                        <div className="others-container">
                                            <div className="others-label p-r-0">{labels.othersNone}</div>
                                            <div className="bottom-border positionRelative width-others">
                                                <TextInput
                                                    inputID={`othersOthers${key}`}
                                                    label={labels.othersNone}
                                                    value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers.value}
                                                    errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers.errorMsg}
                                                    isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers.isValid}
                                                    onChange={(data) => this.handleOnTextChangeOthers(data, `tinNoOthers${key}`, 'othersOthers')}
                                                    validator={["required", "maxSize|70"]}
                                                    type="Others"
                                                    maxCharacters={70}
                                                    hasIcon={false}
                                                    isReadOnly={true}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        }
                    </div>
                </div>
            );
            return tmpArray;
        })
        return tmpArray;
    }

    render() {
        const { commonReducer, taxSelfDeclarationsReducer } = this.props;
        const { checkboxSG, checkboxUS, checkboxOthers, checkboxNone, tinNoSG, tinNoUS, countryNone, isUSResident, haveTINNoNone, tinNoNone, reasonsNone, othersNone } = taxSelfDeclarationsReducer;
        //Lists
        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";
        const countriesNamesMap = inputValues.countriesNamesMap;
        const reasonsList = inputValues.reasons;

        //Values
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;

        //Validation

        const bottomBorder = haveTINNoNone.value === "Y" ? "" : "bottom-border";


        return (
            <div>
                <h1 className="desc-grey">{taxSelfDeclarationsValue.subtitle}</h1>
                <p className="sub-headline">{taxSelfDeclarationsValue.subHeadline}</p>
                <Checkbox
                    inputID="checkboxSG"
                    description={labels.checkboxSG}
                    isChecked={checkboxSG.isToggled}
                    isDisabled={true}
                    onClick={() => this.handleOnCheckbox('checkboxSG', checkboxSG.isToggled)}
                />
                {
                    checkboxSG.isToggled &&
                    <div className="crs-container">
                        <div className="sub-div-common bottom-border">
                            <div className="p-l-10 positionRelative">
                                <TextInput
                                    isReadOnly={true}
                                    inputID="tinNoSG"
                                    label={labels.tinNoSG}
                                    value={tinNoSG.value}
                                    errorMsg={tinNoSG.errorMsg}
                                    isDisabled={true}
                                    hasIcon={false}
                                />
                            </div>
                        </div>
                    </div>
                }
                <Checkbox
                    inputID="checkboxUS"
                    description={labels.checkboxUS}
                    isChecked={checkboxUS.isToggled}
                    isDisabled={true}
                    onClick={() => this.handleOnCheckbox('checkboxUS', checkboxUS.isToggled)}
                />
                {
                    checkboxUS.isToggled &&
                    <div className="crs-container">
                        <div className="top-border bottom-border">
                            <div className="fullTable confirmDetails-flexContainer">
                                <div className="halfRow right-border   positionRelative">
                                    <TextInput
                                        isReadOnly={true}
                                        inputID="tinNoUS"
                                        label={labels.tinNoUS}
                                        value={tinNoUS.value}
                                        errorMsg={tinNoUS.errorMsg}
                                        isDisabled={true}
                                        hasIcon={false}
                                    />
                                </div>
                                <div className="halfRow   positionRelative">
                                    <div className="crs-buttons" >
                                        <div className="txt-container">
                                            <span className="crs-txt">{labels.isUSResident}</span>
                                        </div>
                                        <div className="flex-1">
                                            <input type="radio" id="usYes" name="radioButtonUS" value="Y" checked={isUSResident.value === "Y" ? true : false} disabled={true} />
                                            <label className="lbl-first" htmlFor="usYes">{labels.yesButton}</label>

                                            <input type="radio" id="usNo" name="radioButtonUS" value="N" checked={isUSResident.value === "N" ? true : false} disabled={true} />
                                            <label className="lbl-second" htmlFor="usNo">{labels.noButton}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                <Checkbox
                    inputID="checkboxOthers"
                    description={labels.checkboxOthers}
                    isChecked={checkboxOthers.isToggled}
                    isDisabled={true}
                    onClick={() => this.handleOnCheckbox('checkboxOthers', checkboxOthers.isToggled)}
                />
                {
                    checkboxOthers.isToggled &&
                    <div>
                        {this.renderCountries()}
                    </div>


                }
                <Checkbox
                    inputID="checkboxNone"
                    description={labels.checkboxNone}
                    isChecked={checkboxNone.isToggled}
                    isDisabled={true}
                    onClick={() => this.handleOnCheckbox('checkboxNone', checkboxNone.isToggled)}
                />

                {
                    checkboxNone.isToggled &&
                    <div className="crs-container">
                        <span className="crs-txt float-none">{labels.none}</span>
                        <div className="p-t-20">
                            <div className={`top-border ${bottomBorder}`}>
                                <div className="fullTable confirmDetails-flexContainer">
                                    <div className="halfRow right-border positionRelative dropdown-nopadding">
                                        <Dropdown
                                            isReadOnly={true}
                                            inputID="countryNone"
                                            label={labels.countryNone}
                                            dropdownItems={mapValueToDescription(
                                                countriesNamesMap
                                            )}
                                            isFocus={countryNone.isFocus}
                                            value={countryNone.value}
                                            isValid={countryNone.isValid}
                                            errorMsg={countryNone.errorMsg}
                                            focusOutItem={true}
                                            searchValue={countryNone.searchValue}
                                            onBlur={this.handleDropdownBlur.bind(this, "countryNone")}
                                            onFocus={this.handleDropdownFocus.bind(this, "countryNone")}
                                            onClick={data => this.handleDropdownClick(data, "countryNone")}
                                            onSearchChange={event =>
                                                this.handleOnSearchChange(event, "countryNone")
                                            }
                                            flagImg
                                        />
                                    </div>
                                    <div className="halfRow positionRelative">
                                        <div className="crs-buttons">
                                            <div className="txt-container">
                                                <span className="crs-txt">{labels.haveTINNo}</span>
                                                {!haveTINNoNone.isValid && <span className="errorMsg">{labels.errorMsgRequired}</span>}
                                            </div>
                                            <div className="flex-1">
                                                <input type="radio" id="noneYes" name="radioButtonNone" value="Y" checked={haveTINNoNone.value === "Y" ? true : false} disabled={true} />
                                                <label className="lbl-first" htmlFor="noneYes">{labels.yesButton}</label>

                                                <input type="radio" id="noneNo" name="radioButtonNone" value="N" checked={haveTINNoNone.value === "N" ? true : false} disabled={true} />
                                                <label className="lbl-second" htmlFor="noneNo">{labels.noButton}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {haveTINNoNone.value === "Y" &&
                                <div className="sub-div-common bottom-border">
                                    <div className="p-l-10 positionRelative">
                                        <TextInput
                                            inputID="tinNoNone"
                                            label={labels.othersTINNo}
                                            value={tinNoNone.value}
                                            errorMsg={tinNoNone.errorMsg}
                                            isValid={tinNoNone.isValid}
                                            onChange={data => this.handleOnChange(data, "tinNoNone")}
                                            validator={["required", "isAlphanumeric", "maxSize|19"]}
                                            hasIcon={false}
                                            isReadOnly={true}
                                        />
                                    </div>
                                </div>
                            }

                            {haveTINNoNone.value === "N" &&
                                <div>
                                    <div className="crs-txt-container">
                                        <span className="crs-txt crs-questions">{labels.reason}</span>
                                    </div>
                                    <RadioButton
                                        inputID="reasonsNone"
                                        value={reasonsNone.value}
                                        radioObj={mapValueToDescription(
                                            reasonsList
                                        )}
                                        errorMsg={reasonsNone.errorMsg}
                                        isValid={reasonsNone.isValid}
                                        onClick={(data) => this.handleRadioSelectionNone(data, "reasonsNone")}
                                        isDisabled={true}
                                    />
                                    {reasonsNone.value === "3" &&
                                        <div className="p-l-65">
                                            <div className="others-container">
                                                <div className="others-label p-r-0">{labels.othersNone}</div>
                                                <div className="bottom-border positionRelative width-others">
                                                    <TextInput
                                                        inputID="othersNone"
                                                        label={labels.othersNone}
                                                        value={othersNone.value}
                                                        errorMsg={othersNone.errorMsg}
                                                        onChange={data => this.handleOnChange(data, "othersNone")}
                                                        isValid={othersNone.isValid}
                                                        validator={["required", "maxSize|70"]}
                                                        hasIcon={false}
                                                        isReadOnly={true}
                                                        type="Others"
                                                    />
                                                </div>
                                            </div>
                                        </div>}
                                </div>
                            }
                        </div>
                    </div>
                }
            </div>
        );
    }

}

const mapStateToProps = state => {
    const { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer } = state;
    return { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer };
};

export default connect(mapStateToProps)(TaxSelfDeclarations);
