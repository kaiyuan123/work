import React, { Component } from "react";
import { connect } from "react-redux";
import ConfirmReviewDetails from "./ConfirmReviewDetails";
import ConfirmTandC from "./ConfirmTandC";
import Signature from "./Signature";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";

class ConfirmDetailsPage extends Component {
	handleToThankYouPage() {
		this.props.onContinue();
	}

	isAllChecking() {
		this.props.onCheck()
	}

	render() {
		const { commonReducer, contactDetailsReducer } = this.props;
		const { personType } = contactDetailsReducer;
		const labels = commonReducer.appData.confirmDetails.confirmTandC.labels;

		return (
			<div className="uob-content" id="confirmDetailsPage-section">
				{
					personType.value === "AS" &&
					<ConfirmReviewDetails {...this.props} />
				}
				{
					<ConfirmTandC {...this.props} />
				}
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.submitButton} onClick={() => this.handleToThankYouPage()}/>
				</div>
				{
					personType.value === "AS" &&
					<Signature  {...this.props} />
				}
				{!this.props.onFixedButton() &&
				<div className="uob-input-separator fixedContinue fixedContinueGray">
					<PrimaryButton label={labels.submitButton} onClick={() => this.isAllChecking()}/>
				</div>
				}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, applicationReducer, additionalPartnersDetailsReducer, localAddressInputReducer, companyBasicDetailsReducer, confirmTandCReducer, contactDetailsReducer, signatureReducer, drawSignatureReducer, uploadDocumentsReducer } = state;
	return {
		commonReducer,
		applicationReducer,
		additionalPartnersDetailsReducer,
		localAddressInputReducer,
		companyBasicDetailsReducer,
		confirmTandCReducer,
		contactDetailsReducer,
		signatureReducer,
		drawSignatureReducer,
		uploadDocumentsReducer
	};
};

export default connect(mapStateToProps)(ConfirmDetailsPage);
