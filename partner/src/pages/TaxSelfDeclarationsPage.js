import React, { Component } from "react";
import { connect } from "react-redux";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import TaxSelfDeclarations from "./TaxSelfDeclarations";
import FATCA from './FATCA';
class TaxSelfDeclarationsPage extends Component {

    render() {
        const { commonReducer, contactDetailsReducer } = this.props;
        const { businessConstitution } = contactDetailsReducer;

        //Values
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;

        //Validation
        const isNotSoleProp = businessConstitution.value !== "S";
        console.log(taxSelfDeclarationsValue.glossary);
        return (
            <div className="uob-content" id="taxSelfDeclarations-section">
                <h1 className="sectionTitle-grey">{taxSelfDeclarationsValue.title}</h1>
                <p className="uob-headline" dangerouslySetInnerHTML={{ __html: taxSelfDeclarationsValue.headline }} />
                <TaxSelfDeclarations {...this.props} />
                {isNotSoleProp &&
                    <FATCA {...this.props} />
                }
                <div className="uob-input-separator fixedContinue">
                    <PrimaryButton label={labels.continueButton} onClick={() => this.props.onContinue()} isLoading={commonReducer.isProcessing} />
                </div>
                <div className="tax-glossary" dangerouslySetInnerHTML={{ __html: taxSelfDeclarationsValue.glossary }} />
            </div >
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer, contactDetailsReducer } = state;
    return { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer, contactDetailsReducer };
};

export default connect(mapStateToProps)(TaxSelfDeclarationsPage);
