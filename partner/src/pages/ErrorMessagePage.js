import React, { Component } from 'react';
import { connect } from 'react-redux';
import MessageBox from './../components/MessageBox/MessageBox';

class ErrorMessagePage extends Component {

  render() {
    const { commonReducer, errorCode } = this.props;
    const errorValue = commonReducer.appData.errorMessageBox;
    const code = errorValue[errorCode.value] ? errorCode.value : 'serviceDown';

    return (
      <div id="loadingPage" style={{ top: "60px" }} className='uob-content'>
        <div className="error-pg-container">
          <MessageBox
            title={errorValue[code].title}
            subtitle={errorValue[code].subtitle}
          />

          {code === "AppExistSameApplicant" &&
            <div className='error-button-container'>
              <div className="error-button" onClick={() => null}>{errorValue.continueButton}</div>
            </div>}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { commonReducer } = state;
  return { commonReducer };
}

export default connect(mapStateToProps)(ErrorMessagePage);
