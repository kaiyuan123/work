import React, { Component } from "react";
import { connect } from "react-redux";
import { handleIsChecked } from "./../actions/confirmTandCAction";
import Checkbox from "./../components/Checkbox/Checkbox";
import closeIcon from "./../assets/images/cross-grey.svg";
import checkMark from "./../assets/images/checkMark.svg";

export class ConfirmTandC extends Component {
	state = {
		popupOpen: false,
		showReviewMobile: false
	};

	handlePopupEvent = () => {
		this.setState({ popupOpen: !this.state.popupOpen });
	};

  handleOnCheckbox(field, isToggled, isValid) {
    const { dispatch } = this.props;
    dispatch(handleIsChecked(field, isToggled, isValid));
    if(!isToggled){
			this.setState({ popupOpen: false });
		}
  }

	handleResize = () => {
		this.setState({ showReviewMobile: (window.innerWidth < 992) });
	}

	componentDidMount() {
		this.handleResize();
		window.addEventListener('resize', this.handleResize)
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.handleResize)
	}

  render() {
    const { commonReducer, confirmTandCReducer } = this.props;
    const { acknowledgementCheckbox } = confirmTandCReducer;
    const confirmDetailsValue = commonReducer.appData.confirmDetails.confirmTandC;
    const showRedBorder = !acknowledgementCheckbox.isValid;
    const agreementButtonClass = acknowledgementCheckbox.isToggled ?'agreementButton agreementButton--selected':showRedBorder?'agreementButton agreementButton--error':'agreementButton';

    if(this.state.showReviewMobile){
			return (
				<div>
					<div className="uob-input-separator"  id="confirmationMain">
						<h1 className="sectionTitle-blue">{confirmDetailsValue.title}</h1>
						<div className="uob-form-separator" />
						<p className="uob-headline">
							{'Please read and accept the:'}
						</p>

						<table width={'100%'}>
							<tbody>
							<tr>
								<td style={{ width: '60px', textAlign: 'center', cursor: 'pointer' }}
										onClick={() => this.handlePopupEvent()}>
									<div className={agreementButtonClass}>
										{acknowledgementCheckbox.isToggled && <img style={{ marginRight: '15px'}} src={checkMark} alt="check-Icon" width={43} height={35}/>}
										{'Confirmation and Agreement'}
									</div>
								</td>
							</tr>
							</tbody>
						</table>
						{showRedBorder && <div className="errorMsg" style={{ textAlign: 'left',marginTop: '5px'}}>{confirmDetailsValue.errorMsgCheckbox}</div>}
					</div>
					<div>
						<div className="remodal-overlay remodal-is-opened"
								 style={{ display: this.state.popupOpen ? "block" : "none" }}/>
						<div className="remodal-wrapper remodal-is-opened" style={{ display: this.state.popupOpen ? "flex" : "none" }}>
							{this.state.popupOpen && <style dangerouslySetInnerHTML={{ __html: `.uob-form-loan-container {overflow: hidden}` }} />}
							<div className="agreementMainPopup">
								<div className="agreementCloseMark" onClick={() => this.handlePopupEvent()}>
									<img className="close-icon" src={closeIcon} alt="Close-Icon"/>
								</div>
								<div>
									<h1 style={{ padding: '10px', width: '80%' }} className="sectionTitle-blue">{confirmDetailsValue.title}</h1>
								</div>
								<p className="uob-headline" style={{ padding: '10px'}} >
									{confirmDetailsValue.headline}
								</p>
								<div className="agreementContent" dangerouslySetInnerHTML={{ __html: confirmDetailsValue.acknowledgementText }} />
								<div id="confirmTandC">
									<Checkbox
										description={confirmDetailsValue.acknowledgementSubtitle}
										isChecked={acknowledgementCheckbox.isToggled}
										onClick={() => this.handleOnCheckbox('acknowledgementCheckbox', acknowledgementCheckbox.isToggled, acknowledgementCheckbox.isToggled)
										}
										errorMsg={confirmDetailsValue.errorMsgCheckbox}
										isValid={acknowledgementCheckbox.isValid}
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
			);
		}
    return (
			<div className="uob-input-separator"  id="confirmationMain">
				<div>
					<h1 className="sectionTitle-blue">{confirmDetailsValue.title}</h1>
				</div>
				<p className="uob-headline">
					{confirmDetailsValue.headline}
				</p>
				<div className="agreementContent" dangerouslySetInnerHTML={{ __html: confirmDetailsValue.acknowledgementText }} />
				<div id="confirmTandC">
					<Checkbox
						description={confirmDetailsValue.acknowledgementSubtitle}
						isChecked={acknowledgementCheckbox.isToggled}
						onClick={() => this.handleOnCheckbox('acknowledgementCheckbox', acknowledgementCheckbox.isToggled, acknowledgementCheckbox.isToggled)
						}
						errorMsg={confirmDetailsValue.errorMsgCheckbox}
						isValid={acknowledgementCheckbox.isValid}
					/>
				</div>
			</div>
    );
  }
}

const mapStateToProps = state => {
  const { commonReducer, confirmTandCReducer } = state;
  return { commonReducer, confirmTandCReducer };
};

export default connect(mapStateToProps)(ConfirmTandC);
