import React, { Component } from "react";
import TextInput from "./../components/TextInput/TextInput";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import { handleTextInputChange } from "./../actions/shareHoldersAction";
import { connect } from "react-redux";

class ShareHoldersPage extends Component {

	isShareHoldersListPassing() {
		this.props.onCheck()
	}

	handleToASRDetailsPage() {
		this.props.onContinue();
	}

	handleOnChange(data, key, field) {
		const { dispatch } = this.props;
		dispatch(handleTextInputChange(data, key, field));
	}

	shareHoldersListValid() {
		const { shareHoldersReducer } = this.props;
		const { shareholderID } = shareHoldersReducer;
		let errorCount = 0;

		if (shareholderID.length > 0) {
			shareHoldersReducer.shareholderID.map((key) => {
				const shareholderEmail = shareHoldersReducer[`shareholder${key}`].shareholderEmail;
				const shareholderNRIC = shareHoldersReducer[`shareholder${key}`].shareholderNRIC;
				const shareholderName = shareHoldersReducer[`shareholder${key}`].shareholderName;
				const shareholderMobileNo = shareHoldersReducer[`shareholder${key}`].shareholderMobileNo;

				const isApprovedshareHoldersValid = shareholderName.value !== "" && shareholderName.isValid && shareholderEmail.value !== "" && shareholderEmail.isValid && shareholderNRIC.value !== "" && shareholderNRIC.isValid && shareholderEmail.value !== "" && shareholderEmail.isValid && shareholderMobileNo.value !== "" && shareholderMobileNo.isValid;

				if (!isApprovedshareHoldersValid) {
					errorCount++
				}
				return errorCount;
			})
		}
		return errorCount;
	}

	renderShareHoldersList() {
		const { shareHoldersReducer, commonReducer } = this.props;
		const { shareholderID } = shareHoldersReducer;
		const shareHoldersValue = commonReducer.appData.shareHolders;
		const labels = shareHoldersValue.labels;
		const errorMsgList = commonReducer.appData.errorMsgs;
		const tmpArray = [];

		shareholderID.map((key, i) => {
			const applicantNumber = i + 1;
			tmpArray.push(
				<div key={key} >
					<div className="partner-subtitle-container">
						<div className="sub-title-confirm-grey" id={`shareHolder${key}`}>{labels.user.replace("{id}", applicantNumber)}</div>
					</div>
					<div className="top-border">
						<div className="fullTable confirmDetails-flexContainer">
							<div className="halfRow right-border positionRelative">
								<TextInput
									inputID={`shareholderName${key}`}
									label={labels.name}
									value={shareHoldersReducer[`shareholder${key}`].shareholderName.value}
									errorMsg={shareHoldersReducer[`shareholder${key}`].shareholderName.errorMsg}
									isValid={shareHoldersReducer[`shareholder${key}`].shareholderName.isValid}
									onChange={(data) => this.handleOnChange(data, `shareholder${key}`, 'shareholderName')}
									validator={["isFullName", "maxSize|70"]}
									errorMsgList={errorMsgList}
									isMyInfo={shareHoldersReducer[`shareholder${key}`].shareholderName.isMyInfo}
									hasIcon={false}
									isReadOnly={shareHoldersReducer[`shareholder${key}`].shareholderName.isReadOnly}
								/>
							</div>
							<div className="halfRow positionRelative">
								<TextInput
									isUpperCase
									inputID={`signatoriesNRIC${key}`}
									label={labels.nric}
									value={shareHoldersReducer[`shareholder${key}`].shareholderNRIC.value}
									errorMsg={shareHoldersReducer[`shareholder${key}`].shareholderNRIC.errorMsg}
									isValid={shareHoldersReducer[`shareholder${key}`].shareholderNRIC.isValid}
									onChange={(data) => this.handleOnChange(data, `shareholder${key}`, 'shareholderNRIC')}
									errorMsgList={errorMsgList}
									validator={["isNRIC"]}
									isMyInfo={shareHoldersReducer[`shareholder${key}`].shareholderNRIC.isMyInfo}
									hasIcon={false}
									isReadOnly={shareHoldersReducer[`shareholder${key}`].shareholderNRIC.isReadOnly}
								/>
							</div>
						</div>
					</div>
					<div className="top-border bottom-border">
						<div className="fullTable confirmDetails-flexContainer">
							<div className={`halfRow right-border positionRelative`}>
								<TextInput
									type="Phone"
									inputID={`shareholderMobileNo${key}`}
									label={labels.mobile}
									value={shareHoldersReducer[`shareholder${key}`].shareholderMobileNo.value}
									errorMsg={shareHoldersReducer[`shareholder${key}`].shareholderMobileNo.errorMsg}
									isValid={shareHoldersReducer[`shareholder${key}`].shareholderMobileNo.isValid}
									onChange={(data) => this.handleOnChange(data, `shareholder${key}`, 'shareholderMobileNo')}
									errorMsgList={errorMsgList}
									validator={["isPhoneNumber"]}
									isMyInfo={shareHoldersReducer[`shareholder${key}`].shareholderMobileNo.isMyInfo}
									hasIcon={false}
									isReadOnly={shareHoldersReducer[`shareholder${key}`].shareholderMobileNo.isReadOnly}
									onlySg={false}
								/>
							</div>
							<div className={`halfRow positionRelative`}>
								<TextInput
									inputID={`shareholderEmail${key}`}
									label={labels.email}
									value={shareHoldersReducer[`shareholder${key}`].shareholderEmail.value}
									errorMsg={shareHoldersReducer[`shareholder${key}`].shareholderEmail.errorMsg}
									isValid={shareHoldersReducer[`shareholder${key}`].shareholderEmail.isValid}
									onChange={(data) => this.handleOnChange(data, `shareholder${key}`, 'shareholderEmail')}
									errorMsgList={errorMsgList}
									validator={["isEmail", "maxSize|30"]}
									isMyInfo={shareHoldersReducer[`shareholder${key}`].shareholderEmail.isMyInfo}
									hasIcon={false}
									isReadOnly={shareHoldersReducer[`shareholder${key}`].shareholderEmail.isReadOnly}
								/>
							</div>
						</div>
					</div>
				</div>
			);
			return tmpArray;
		})
		return tmpArray;
	}

	render() {
		const { commonReducer } = this.props;
		const shareHoldersValue = commonReducer.appData.shareHolders;
		const labels = shareHoldersValue.labels;

		return (
			<div className="uob-content" id="shareHolders-section">
				<h1 className="sectionTitle-grey">{shareHoldersValue.title}</h1>

				<p className="uob-headline" dangerouslySetInnerHTML={{ __html: shareHoldersValue.subtitle }} />

				<div>{this.renderShareHoldersList()}</div>
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToASRDetailsPage()} isLoading={commonReducer.isProcessing} />
				</div>

				{
					!this.props.onFixedButton() &&
					<div className="uob-input-separator fixedContinue fixedContinueGray">
						<PrimaryButton label={labels.continueButton} onClick={() => this.isShareHoldersListPassing()} />
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = state => {
	const { commonReducer, shareHoldersReducer } = state;
	return { commonReducer, shareHoldersReducer };
};

export default connect(mapStateToProps)(ShareHoldersPage);
