import React, { Component } from 'react';
import { connect } from 'react-redux';
import queryString from 'query-string';
import { handleEmailValidation, handleSetDefaultError } from './../actions/verifyAction';
import MessageBox from './../components/MessageBox/MessageBox';
import loadingImg from './../assets/images/loading_icon.svg';
import errorImg from './../assets/images/errorImg.svg';
import PrimaryButton from './../components/PrimaryButton/PrimaryButton';


class VerifyPage extends Component {
  componentWillMount() {
    const { dispatch } = this.props;
    const params = queryString.parse(this.props.location.search);
    const code = params.code ? params.code : '';
    if (code === '') {
      dispatch(handleSetDefaultError('badRequest'));
    }
  }

  redirectOnSuccess() {
    this.props.history.push({
      pathname: "apply",
      search: ''
    })
  }

  redirectToErrorPage() {
    // const { applicationReducer } = this.props;
    this.props.history.push({
      pathname: "error"
    })
  }

  renderErrorMessage(message) {
    return (
      <div className='uob-content'>
        <MessageBox
          img={errorImg}
          title={message.title}
          subtitle={message.subtitle}
          description={message.description}
        />
      </div>
    );
  }

  handleToVerify() {
    const { dispatch } = this.props;
    const params = queryString.parse(this.props.location.search);
    const code = params.code ? params.code : '';
    const mode = params.mode ? params.mode : '';
    dispatch(handleEmailValidation(code, mode, () => this.redirectOnSuccess(), () => this.redirectToErrorPage()));
  }

  render() {
    const { verifyReducer, commonReducer } = this.props;
    const { isVerifying, verifyingBox } = verifyReducer;
    const verifyValues = commonReducer.appData.verify;
    // const errorMessageBox = commonReducer.appData.errorMessageBox;
    // let verifyImg = './images/verifyImg.jpg';
    return (
      <div>
        <style dangerouslySetInnerHTML={{
          __html: `
          body { padding: 0;
          background-repeat: no-repeat;
          background-position: 100%;
          height: 100%; width: 100%;}
          .uob-form-loan-container { background: transparent;}
          .uob-content { padding: 0; }
          #sideNav { display: none }
          .logoRight { display: block!important;  }
          #home .title2 { display: none }
          section.resume-section { padding: 32px 35px 0!important;}
          .blue-uob-logo {padding: 26px 40px}
          .uob-form-loan-container {width: 100%; padding: 0px;}
          .uob-body {padding:10px;}
          `
        }} />
        {
          isVerifying &&
          <div className='verfiying-container'>
            <img
              className='icon-verifying--spinner'
              src={loadingImg}
              alt='icon'
            />
            <div>{verifyValues.verifying}</div>
          </div>
        }
        {verifyingBox &&
          <div id='verify-container'>
            <div className="verify-content">
              <div className="verify-title">
                {verifyValues.labels.header}
              </div>
              <div className="subtitle-box">
                <span className="subtitle-box-icon">&#9432;</span>
                <span className="subtitle-box-subtite" dangerouslySetInnerHTML={{ __html: verifyValues.labels.subtite }} />
              </div>
              <div className="subtitle-content">
                <span className="subtitle-box-subtite" dangerouslySetInnerHTML={{ __html: verifyValues.labels.content }} />
              </div>
              <div className='uob-space-separator' />
              <div className='verify-primary-button'>
                <PrimaryButton label={verifyValues.labels.continueButton} onClick={this.handleToVerify.bind(this)} />
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { commonReducer, verifyReducer, applicationReducer } = state;
  return { commonReducer, verifyReducer, applicationReducer };
}

export default connect(mapStateToProps)(VerifyPage);
