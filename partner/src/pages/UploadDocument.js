import React, { Component } from "react";
import { connect } from "react-redux";
import FileUpload from "./../components/FileUpload/FileUpload";
import {
	dispatchUpdateUploadProgress,
	dispatchUploadFile,
	dispatchErrorMsg,
	dispatchResetUploadStatus
} from './../actions/uploadDocumentsAction';
import { setErrorMessage } from './../actions/commonAction';

class UploadDocument extends Component {
	updateUploadProgress(id, progress) {
		const { dispatch } = this.props;
		dispatch(dispatchUpdateUploadProgress(id, progress));
	}

	handleUploadFile(id, name, size) {
		const { dispatch } = this.props;
		dispatch(dispatchUploadFile(id, name, size));
	}

	handleErrorMsg(id, error) {
		const { dispatch } = this.props;
		dispatch(dispatchErrorMsg(id, error));
	}

	handleUploadErrorMsg(id, error) {
		const { dispatch } = this.props;
		dispatch(setErrorMessage(error));
		dispatch(dispatchResetUploadStatus(id));
	}

	render() {
		const { commonReducer, uploadDocumentsReducer, applicationReducer } = this.props;
		const { MADocument } = uploadDocumentsReducer;
		const { initiateMyinfoData } = applicationReducer;
		const applicationID = initiateMyinfoData !== null ? initiateMyinfoData.id : null;
		const globalErrors = commonReducer.appData.globalErrors;
		const errorMsgs = commonReducer.appData.errorMsgs;

		const uploadDocuments = commonReducer.appData.uploadDocuments;
		const labels = uploadDocuments.labels;

		return (
			<div className="uob-content p-b-0">
				<div className="uob-input-separator">
					<h1>{uploadDocuments.title}</h1>
					<div className="font-16">
						{uploadDocuments.subtitle1}
					</div>
					<div className="font-12">
						{uploadDocuments.subtitle2}
					</div>
				</div>
				<div className="mainUploadSection">
					<FileUpload
						inputID={'MADocument'}
						isValid={MADocument.isValid}
						errorMsg={MADocument.errorMsg}
						title={labels.title}
						errorTitle={labels.errorTitle}
						buttonName={labels.buttonName}
						inputValue={MADocument.inputValue}
						fileSize={MADocument.fileSize}
						identifier={applicationID}
						docType={'ma'}
						description={labels.description}
						progressValue={MADocument.progress}
						updateUploadProgress={(id, progress) => this.updateUploadProgress(id, progress)}
						handleUploadFile={(id, name, size) => this.handleUploadFile(id, name, size)}
						handleShowErrorMsg={(id, error) => this.handleErrorMsg(id, error)}
						handleShowUploadErrorMsg={(id, error) => this.handleUploadErrorMsg(id, error)}
						uploadUnsuccessfulMsg={globalErrors.uploadUnsuccessfulMsg}
						uploadUnsuccessfulMsgSize={errorMsgs.uploadUnsuccessfulMsgSize}
						uploadUnsuccessfulMsgExtension={errorMsgs.uploadUnsuccessfulMsgExtension}
						uploadUnsuccessfulMsgFileName={errorMsgs.uploadUnsuccessfulMsgFileName}
					/>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, uploadDocumentsReducer } = state;
	return { commonReducer, uploadDocumentsReducer };
};

export default connect(mapStateToProps)(UploadDocument);
