import React, { Component } from "react";
import { connect } from "react-redux";
import TableViewCompany from "../components/TableView/TableViewCompany/TableViewCompany";

class CompanyOverallDetails extends Component {

	render() {
		const { commonReducer, companyOverallDetailsReducer } = this.props;
		const { previousNames, previousUens, capitals, appointments, shareholders } = companyOverallDetailsReducer;

		const inputValues = commonReducer.appData.inputValues
			? commonReducer.appData.inputValues
			: "";
		const companyCapitalTypeList = inputValues.capitalType;
		const companyDetailsValues = commonReducer.appData.companyDetails;
		const companyOverallDetailsValues = companyDetailsValues.companyOverallDetails;
		const labels = companyOverallDetailsValues.labels;

		return (
			<div>
				<div className="uob-content p-t-10">
					{previousNames.value.length > 0 && previousUens.value.length > 0 &&
						<div>
							<h1 className="sub-title-grey m-t-0">
								{companyOverallDetailsValues.subtitle}
							</h1>
							<div className="uob-input-separator enableOverflowX">
								<TableViewCompany
									tableContent={previousNames.value}
									colTitles={labels.previousCompanyColTitle}
									tableType={"previousCompanyData"}
									mapItems={previousUens.value}
								/>
							</div>
						</div>}
					{capitals.value.length > 0 &&
						<div>
							<h1 className="sub-title-grey">{companyOverallDetailsValues.subtitle2}</h1>
							<div className="uob-input-separator enableOverflowX mob-smwidth">
								<TableViewCompany
									tableType={'companyCapitalsData'}
									colTitles={labels.capitalDetailsColTitle}
									tableContent={capitals.value}
									mapItems={companyCapitalTypeList}
								/>
							</div>
						</div>}
					{appointments.value.length > 0 &&
						<div>
							<h1 className="sub-title-grey">
								{companyOverallDetailsValues.subtitle3}
							</h1>
							<div className="uob-input-separator enableOverflowX">
								<TableViewCompany
									colTitles={labels.appointmentDetailsColTitle}
									tableType={'appointmentsData'}
									tableContent={appointments.value}
									mapItems={inputValues}
									flexTableWidth={900}
								/>
							</div>
						</div>}
					{shareholders.value.length > 0 &&
						<div>
							<h1 className="sub-title-grey">
								{companyOverallDetailsValues.subtitle4}
							</h1>
							<div className="uob-input-separator enableOverflowX mob-lgwidth">
								<TableViewCompany
									colTitles={labels.shareholdersColTitle}
									tableType={'shareholdersData'}
									tableContent={shareholders.value}
									mapItems={inputValues}
								/>
							</div>
						</div>}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, companyOverallDetailsReducer } = state;
	return { commonReducer, companyOverallDetailsReducer };
};

export default connect(mapStateToProps)(CompanyOverallDetails);
