import React, { Component } from "react";
import { connect } from "react-redux";
import Checkbox from "./../components/Checkbox/Checkbox";
import TextInput from "./../components/TextInput/TextInput";
import { handleTextInputChange } from "./../actions/accountSetupAction";
import { handleIsChecked } from "./../actions/accountSetupAction";

class AccountBiBPlus extends Component {
	handleOnChange(data, field) {
		const { dispatch } = this.props;
		dispatch(handleTextInputChange(data, field));
	}

	handleOnCheckbox(field, isToggled, isValid) {
		const { dispatch } = this.props;
		dispatch(handleIsChecked(field, isToggled, isValid));
	}

	render() {
		const { commonReducer, accountSetupReducer } = this.props;
		const { registerOfPayNow, setupBIBPlus, BIBBulkService, BIBGroupId, BIBRemittanceMessage, BIBRoleType, BIBMobileNumber, BIBContactPerson, BIBEmailAddress, payNowUEN, payNowID, readOnly } = accountSetupReducer;
		const errorMsgList = commonReducer.appData.errorMsgs;
		const accountSetupValue = commonReducer.appData.accountSetup;
		const labels = accountSetupValue.labels;

		return (
			<div className="uob-content additional-service" id="additionalServices-section">
				<h1 className="sub-title-grey m-t-0">{accountSetupValue.additionalServices}</h1>
				<div className="top-border">
					<div className="additionalServicesMain">
						<br />
						<Checkbox
							description={labels.registerOfPayNow}
							isChecked={registerOfPayNow.isToggled}
							isDisabled={registerOfPayNow.isDisabled}
							onClick={() => this.handleOnCheckbox('registerOfPayNow', registerOfPayNow.isToggled)}
						/>
						<span className="checkBoxBottomSub">{labels.registerOfPayNowInfo}</span>
						<span className="checkBoxBottomSub">{labels.payNowIDTitleInfo}</span>
						<br />
						{registerOfPayNow.isToggled && <div className="BIBPlusMain">
							<label className="checkbox-container p-l-0 p-b-10">{labels.payNowIDTitle}</label>
							<div className="sub-div-common flexCenterFullWidth positionRelative bottom-border">
								<div className="p-l-10 fullWidth">
									<table style={{ width: '100%' }}>
										<tbody>
											<tr>
												<td style={{ width: '100px' }}>
													<TextInput
														inputID={'payNowUEN'}
														label={labels.payNowUEN}
														isReadOnly={true}
														value={payNowUEN.value}
														errorMsgList={errorMsgList}
														hasIcon={false}
													/>
												</td>
												<td style={{
													textAlign: 'center',
													width: '100px',
													fontWeight: 'bold'
												}}>&nbsp;+&nbsp;</td>
												{readOnly ?
													<td> {payNowID.value} </td> :
													<td className="positionRelative" style={{ padding: '0 12px' }}>
														<TextInput
															inputID={'payNowID'}
															label={labels.payNowID}
															isReadOnly={false}
															value={payNowID.value}
															errorMsg={payNowID.errorMsg}
															onChange={data => this.handleOnChange(data, "payNowID")}
															isValid={payNowID.isValid}
															validator={["isAlphanumeric", "minSizeAndNotRequired|3", "maxSize|3"]}
															errorMsgList={errorMsgList}
															hasIcon={false}
															maxCharacters={3}
														/>
														<div className="payNowIdBottomDash">
															<span>_</span>
															<span>_</span>
															<span>_</span>
														</div>
													</td>
												}
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<br />
							<br />
						</div>}

						<Checkbox
							description={labels.setupBIBPlus}
							isChecked={setupBIBPlus.isToggled}
							isDisabled={setupBIBPlus.isDisabled}
							onClick={() => this.handleOnCheckbox('setupBIBPlus', setupBIBPlus.isToggled)}
						/>
						<span className="checkBoxBottomSub">{labels.companyGroupIDInfo}</span>
						<br />						{setupBIBPlus.isToggled && <div className="BIBPlusMain">
							<div className="sub-div-common flexCenterFullWidth positionRelative bottom-border">
								<div className="p-l-10 fullWidth">
									<TextInput
										inputID={'BIBGroupId'}
										label={labels.BIBGroupId}
										isReadOnly={readOnly}
										value={BIBGroupId.value}
										errorMsg={BIBGroupId.errorMsg}
										onChange={data => this.handleOnChange(data, "BIBGroupId")}
										isValid={BIBGroupId.isValid}
										validator={["isAlphanumeric", "minSizeAndNotRequired|8", "maxSize|35"]}
										errorMsgList={errorMsgList}
										hasIcon={false}
									/>
								</div>
							</div>
							<br />
							<h1 className="sub-title-grey m-t-0 m-b-0">{accountSetupValue.subtitle1}</h1>
							<div className="bottom-border standardRoleMain">
								<Checkbox
									description={labels.BIBRoleType}
									isChecked={BIBRoleType.isToggled}
									isDisabled={BIBRoleType.isDisabled}
									onClick={() => this.handleOnCheckbox('BIBRoleType', BIBRoleType.isToggled)}
									isRadio={true}
								/>
								<div className="checkBoxBottomSub">
									<ul className="standardRoleBottom">
										<li>{labels.listTitle}</li>
										<li>{labels.listItem1}</li>
										<li>{labels.listItem2}</li>
										<li>{labels.listItem3}</li>
									</ul>
								</div>
							</div>
							<br />
							<h1 className="sub-title-grey m-t-0 m-b-0">{accountSetupValue.subtitle2}</h1>
							<Checkbox
								description={labels.BIBBulkService}
								isChecked={BIBBulkService.isToggled}
								isDisabled={readOnly}
								onClick={() => this.handleOnCheckbox('BIBBulkService', BIBBulkService.isToggled)}
							/>
							<span className="checkBoxBottomSub custom-tooltip" dangerouslySetInnerHTML={{ __html: labels.BIBBulkServiceInfo }} />

							<Checkbox
								description={labels.BIBRemittanceMessage}
								isChecked={BIBRemittanceMessage.isToggled}
								isDisabled={readOnly}
								onClick={() => this.handleOnCheckbox('BIBRemittanceMessage', BIBRemittanceMessage.isToggled)}
							/>
							<span className="checkBoxBottomSub custom-tooltip"
								style={{ marginBottom: '30px' }} dangerouslySetInnerHTML={{ __html: labels.BIBRemittanceMessageInfo }} />
						</div>}
					</div>
					<div>
						<h1 className="sub-title-grey" style={{ marginTop: '12px' }}>{accountSetupValue.subtitle3}</h1>
						<span className="BIBSubTitle"
							style={{ marginBottom: '12px', display: 'block' }}>{accountSetupValue.subtitle3Info}</span>
						<div className="sub-div-common flexCenterFullWidth positionRelative">
							<div className="p-l-10 fullWidth">
								<TextInput
									inputID={'BIBContactPerson'}
									label={labels.BIBContactPerson}
									value={BIBContactPerson.value}
									onChange={data => this.handleOnChange(data, "BIBContactPerson")}
									validator={["required", "maxSize|70"]}
									errorMsgList={errorMsgList}
									errorMsg={BIBContactPerson.errorMsg}
									isValid={BIBContactPerson.isValid}
									isReadOnly={readOnly}
									hasIcon={false}
									isMyInfo={BIBContactPerson.isMyInfo}
									isDisabled={readOnly}
								/>
							</div>
						</div>

						<div className="top-border bottom-border">
							<div className="fullTable confirmDetails-flexContainer">
								<div className="halfRow right-border positionRelative  ">
									<TextInput
										type="Phone"
										inputID={'BIBMobileNumber'}
										label={labels.BIBMobileNumber}
										value={BIBMobileNumber.value}
										errorMsg={BIBMobileNumber.errorMsg}
										onChange={data => this.handleOnChange(data, "BIBMobileNumber")}
										isValid={BIBMobileNumber.isValid}
										errorMsgList={errorMsgList}
										validator={["required", "isPhoneNumber"]}
										onlySg={false}
										hasIcon={false}
										isMyInfo={BIBMobileNumber.isMyInfo}
										isReadOnly={readOnly}
										isDisabled={readOnly}
									/>
								</div>
								<div className="halfRow positionRelative  ">
									<TextInput
										inputID={'BIBEmailAddress'}
										label={labels.BIBEmailAddress}
										value={BIBEmailAddress.value}
										errorMsg={BIBEmailAddress.errorMsg}
										onChange={data => this.handleOnChange(data, "BIBEmailAddress")}
										isValid={BIBEmailAddress.isValid}
										errorMsgList={errorMsgList}
										validator={["isEmail", "maxSize|30"]}
										hasIcon={false}
										isMyInfo={BIBEmailAddress.isMyInfo}
										isReadOnly={readOnly}
										isDisabled={readOnly}
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer } = state;
	return { commonReducer };
};

export default connect(mapStateToProps)(AccountBiBPlus);
