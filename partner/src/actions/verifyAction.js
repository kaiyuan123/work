import * as types from './actionTypes';
import { Config, LocalEnvironment } from './../api/httpApi';
import { setIdentifier } from './commonAction';
import { setErrorWithValue } from './applicationAction';
import { setApplyLoanData } from './applyAction';

export const handleEmailValidation = (code, mode, redirectOnSuccess, redirectToErrorPage) => {
  return (dispatch, getState) => {
    dispatch(setEmailVerifying(true));
    if (LocalEnvironment) {
      const response = {
        status: "PENDING",
        productCode: "BB-EBIZ",
        referenceNumber: "20181217129898976",
        entityName: "Banquet Tailor",
        createdDate: "2018-12-27 11:00:44",
        expiryDate: "2019-12-27 11:00:42",
        applicationId: "20181217129898976"
      }

      dispatch(setApplyLoanData(response));
      dispatch(setEmailVerifying(false));
      redirectOnSuccess && redirectOnSuccess();
    } else {
      Config.verifyEmail(code, mode).then(response => {
        dispatch(setIdentifier(response.data.identifier));
        dispatch(setApplyLoanData(response.data));
        dispatch(setEmailVerifying(false));
        redirectOnSuccess && redirectOnSuccess();
      }).catch(e => {
        dispatch(setEmailVerifying(false));
        if (e.response && e.response.data.errorCode && e.response.data.errorCode !== "") {
          let code = e.response.data.errorCode;
          if (code === "invalid.productCode") {
            code = "InvalidProductCode"
          }

          if (code === "myinfo.error") {
            code = "MyInfoError";
          }

          /*if (e.response.data.application) {
            dispatch(setErrorWithDescription(e.response.data.application));
          }*/

          dispatch(setErrorWithValue(code));
          redirectToErrorPage && redirectToErrorPage();
        } else if ( e.response && e.response.status ) {
          dispatch(setErrorWithValue(`${e.response.status}Error`));
          redirectToErrorPage && redirectToErrorPage();
        } else {
          dispatch(setErrorWithValue("serviceDown"));
          redirectToErrorPage && redirectToErrorPage();
        }
      });
    }
  }
}


export const handleSetDefaultError = (errorCode) => {
  return {
    type: types.SET_DEFAULT_VERIFY_ERROR_CODE,
    errorCode
  }
}

export const setEmailVerifyStatus = (status, errorCode) => {
  return {
    type: types.SET_EMAIL_VERIFY_STATUS,
    status,
    errorCode
  }
}

const setEmailVerifying = (status) => {
  return {
    type: types.SET_EMAIL_VERIFYING_STATUS,
    status
  }
}

export const appendParameter = (parameter) => {
  return (dispatch) => {
    dispatch({
      type: types.APPEND_PARAMETER_TO_URL,
      parameter
    })
  }
}
