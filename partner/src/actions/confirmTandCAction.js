import * as types from "./actionTypes";

export const handleIsChecked = (field, isToggled, isValid) => {
    return {
        type: types.TANDC_IS_CHECKED,
        field,
        isToggled: !isToggled,
        isValid: !isValid
    }
}
