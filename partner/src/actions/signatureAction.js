import * as types from "./actionTypes";


export const showErrorMessage = (errorMsg) => {
	return (dispatch) => {
		dispatch({
			type: types.SIGNATURE_ERROR_SHOW,
			errorMsg
		});
	};
};

export const changeProgressValue = (newValue) => {
	return (dispatch) => {
		dispatch({
			type: types.CHANGE_PROGRESS_VALUE,
			newValue
		});
	};
};
export const removeErrorMessage = () => {
	return (dispatch) => {
		dispatch({
			type: types.SIGNATURE_ERROR_REMOVE
		});
	};
};
export const saveSignatureData = (trimmedDataURL) => {
	return (dispatch) => {
		dispatch({
			type: types.SIGNATURE_SAVE_DATA,
			trimmedDataURL
		});
	};
};