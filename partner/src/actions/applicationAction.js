import * as types from "./actionTypes";
import { Application, LocalEnvironment, Config } from "./../api/httpApi";
import { getTypeNatureOfBussiness } from "../common/utils";
import { setMyInfoFlowStatus, setProcessingStatus, setErrorMessage, setRetrievingMyInfo, setApplicationReferenceNo, setCurrentSection, setLoadingStatus, setExpiryDate, setDuplicateTabId } from "./commonAction";
import { setMyInfoContactDetailsData, setRetrieveContactDetailsData } from "./contactDetailsAction";
import { setMyInfoPersonalIncomeDetailsData, setIncomeDetailData } from "./personalIncomeDetailsAction";
import { setResidentialAddress, setCompanyAddressData, setMailingAddressData, setCompanyUnformattedAddress } from "./localAddressInputAction";
import { setCompanyBasicData, setCompanyAddressType } from "./companyBasicDetailsAction";
import { setCompanyPreviousNamesData, setCompanyPreviousUENData, setCompanyCapitalsData, setCompanyAppointmentsData, setCompanyShareholdersData, setCompanyFinancialsData, setCompanyGrantsData } from "./companyOverallDetailsAction";
import { handlePopupHideOnOkButton } from "./askQuestionsAction.js"
import { setOperatingMandateData } from "./operatingMandateAction";
import { setasrApprovedPersonData } from "./asrDetailsAction";
import { setShareHoldersData } from "./shareHoldersAction";
import { setAccountSetupData } from "./accountSetupAction";
import { setTaxSelfDetails, setCPData } from "./taxSelfDeclarationsAction";
import localStore from "./../common/localStore";
import moment from "moment";
import { showErrorMessage,saveSignatureData, setSignatureUploadStatus, setPopupStatus, setSignatureUploadIsValidStatus } from './signatureAction';

//Retrieve MY Info
export const retrieveMyInfoDetails = (redirectToErrorPage) => {
    return (dispatch, getState) => {
        const response = getState().retrievingReducer.myInfoData;
        dispatch(setRetrievingMyInfo(true));
        if (response && response.entity && response.entity !== null && response.person && response.person !== null) {
            Promise.resolve().then(() => {
                const companyDetails = response.entity;
                const residentialAddress = response.person[0].addresses[0];
                const createdBy = response.createdBy;
                const createdDate = response.createdDate;
                dispatch(setMyInfoContactDetailsData(response.person, companyDetails, createdBy, createdDate));
                dispatch(setRetrieveContactDetailsData(companyDetails));
                dispatch(setMyInfoPersonalIncomeDetailsData(response.person, residentialAddress));
                dispatch(setIncomeDetailData(response.person));
                dispatch(setResidentialAddress(residentialAddress))
                if (response.entity && response.person[0].type === 'AS') {
                    dispatch(setCompanyBasicData(response));
                    dispatch(setCompanyPreviousNamesData(response.entity.previousNames));
                    dispatch(setCompanyPreviousUENData(response.entity.previousUens));
                    dispatch(setCompanyCapitalsData(response.entity.capitals));
                    dispatch(setCompanyAppointmentsData(response.entity.appointments));
                    dispatch(setCompanyShareholdersData(response.entity.shareholders));
                    dispatch(setCompanyFinancialsData(response.entity.financials));
                    dispatch(setCompanyGrantsData(response.entity.grants));
                    dispatch(setCompanyAddressType(response.entity.addresses[0].standard));
                    if (response.entity.addresses[1]) {
                        dispatch(setMailingAddressData(response.entity.addresses[1]));
                    }

                    if (response.entity.addresses[0]) {
                        if (response.entity.addresses[0].standard === "N" || response.entity.addresses[0].standard === "L") {
                            dispatch(setCompanyUnformattedAddress(response.entity.addresses[0]));
                        } else {
                            dispatch(setCompanyAddressData(response.entity.addresses[0]));
                        }
                    }

                    if (response.entity.basicProfile && response.entity.basicProfile.ownership !== "1") {
                        dispatch(setWhichKnockOutScenarioFalse("ownership"));
                        dispatch(isKnockOutScenario(true));
                    }
                }
                response.accountSetup && dispatch(setAccountSetupData(response.accountSetup, response.entity));
                dispatch(setInitateMyInfoResponse(response));
                (response.accountSetup && response.entity) && dispatch(setAccountSetupData(response.accountSetup, response.entity));

                if (response.kyc && response.kyc !== [] && response.kyc.length > 0) {
                    dispatch(setShareHoldersData(response.kyc));
                }
                if (response.asr) {
                    dispatch(setasrApprovedPersonData(response.asr));
                }

                const isKnockOut = getTypeNatureOfBussiness(response.type)[0] === "STP" ? false : true;
                dispatch(isWhichTypeSTPorNSTP(response.type));
                dispatch(isKnockOutScenario(isKnockOut));
                response.operatingMandate && dispatch(setOperatingMandateData(response.operatingMandate, response.person[0]));
                response.taxDeclarations && dispatch(setTaxSelfDetails(response.taxDeclarations));
                if (response.taxDeclarations && response.taxDeclarations.controllingPerson && response.taxDeclarations.controllingPerson.length > 0) {
                    dispatch(setCPData(response.taxDeclarations.controllingPerson));
                }

            }).then(() => {
                dispatch(setRetrievingMyInfo(false));
                dispatch(setApplicationDataLoaded());
                dispatch(setMyInfoFlowStatus(true));
                const duplicateTabs = btoa(`${moment()}&${Math.random() + 100}`);
                localStore.setStore('applyDuplicateTab', duplicateTabs);
                dispatch(setDuplicateTabId("applyDuplicateTab", duplicateTabs));
            });
        } else {
            Promise.resolve().then(() => {
                dispatch(setErrorWithValue("NOREF"));
            }).then(() => {
                redirectToErrorPage && redirectToErrorPage();
            });
        }
    };
};

export const setApplicationDataLoaded = () => {
    return {
        type: types.SET_APPLICATION_DATA_LOADED
    };
};

//Initiate Application
export const submitInitiateApplication = (dataObj, handleToPersonalIncomeDetails) => {
    return (dispatch, getState) => {
        const errorMsg = getState().commonReducer.appData.globalErrors.apiException;
        const parameter = getState().applicationReducer.startingParameter;
        dispatch(setProcessingStatus(true));
        if (LocalEnvironment) {
            Config.getInitiateMyinfoData().then(response => {
                Promise.resolve()
                    .then(() => {
                        const operatingMandate = response.operatingMandate;
                        const mainApplicant = response.person[0];
                        let accountHolderType;
                        const isKnockOut = response.type === "STP" ? false : true;
                        dispatch(isWhichTypeSTPorNSTP(response.type));
                        dispatch(isKnockOutScenario(isKnockOut))
                        if (isKnockOut) {
                            dispatch(handlePopupHideOnOkButton(false));
                        }
                        if (operatingMandate) {
                            accountHolderType = operatingMandate.approvedSignatories.filter(function (user) {
                                return user.legalId === mainApplicant.basicInfo.legalId
                            });
                            dispatch(setAccountHolderType(accountHolderType));
                        }
                        dispatch(setProcessingStatus(false))
                        dispatch(setInitateMyInfoResponse(response));
                        dispatch(setasrApprovedPersonData(response.asr));
                        dispatch(setShareHoldersData(response.kyc));
                        if (operatingMandate) {
                            dispatch(setOperatingMandateData(operatingMandate.approvedSignatories, mainApplicant));
                        }
                        dispatch(setAccountSetupData(response.accountSetup, response.entity));

                    }).then(() => {
                        handleToPersonalIncomeDetails && handleToPersonalIncomeDetails();
                    })
            }).catch(e => {
                dispatch(setProcessingStatus(false))
                dispatch(setErrorMessage(errorMsg));
            })
        } else {
            Application.submitInitiateApplication(dataObj, parameter).then(response => {
                const data = response.data;
                Promise.resolve()
                    .then(() => {
                        const operatingMandate = data.operatingMandate;
                        const mainApplicant = data.person[0];
                        let accountHolderType;
                        if (operatingMandate) {
                            accountHolderType = operatingMandate.approvedSignatories.filter(function (user) {
                                return user.legalId === mainApplicant.basicInfo.legalId
                            });
                            dispatch(setAccountHolderType(accountHolderType));
                        }
                        dispatch(setInitateMyInfoResponse(data));
                        if (operatingMandate) {
                            dispatch(setOperatingMandateData(operatingMandate.approvedSignatories, mainApplicant));
                        }
                        dispatch(setAccountSetupData(data.accountSetup, data.entity));
                        if (data.kyc) {
                            dispatch(setShareHoldersData(response.kyc));
                        }
                        if (data.asr) {
                            dispatch(setasrApprovedPersonData(data.asr));
                        }
                        dispatch(setExpiryDate(data.expiry));
                    }).then(() => {
                        dispatch(setProcessingStatus(false));
                        handleToPersonalIncomeDetails && handleToPersonalIncomeDetails();
                    })
            }).catch(e => {
                if (e.response && e.response.status === 403) {
                    dispatch(setProcessingStatus(false));
                    dispatch(setSessionExpirePopup(true));
                } else {
                    dispatch(setProcessingStatus(false))
                    dispatch(setErrorMessage(errorMsg));
                }

            })
        }

    };
};

//Save Application
export const submitPartialApplication = (dataObj, applicationID) => {
    return dispatch => {
        Application.submitPartialApplication(dataObj, applicationID).catch(e => {
            if (e.response && e.response.status === 403) {
                dispatch(setSessionExpirePopup(true));
            }
        });
    }
}

//Save Upload document
/*export const saveUploadDocument = (dataObj, applicationID) => {
    return (dispatch, getState) => {
        dispatch(setLoadingStatus(true));
        Application.saveUploadDocument(dataObj, applicationID).then(res => {
            dispatch(setLoadingStatus(false));
        }).catch(e => {
            dispatch(setLoadingStatus(false));
            dispatch(setErrorMessage(getState().commonReducer.appData.globalErrors.apiException));
        });
    };
};*/
export const saveUploadDocument = (dataObj, applicationID,img) => {
	return (dispatch, getState) => {
		const errorMsg = getState().commonReducer.appData.globalErrors.apiException;
		dispatch(setErrorMessage(""));
		dispatch(setProcessingStatus(true));
		Application.saveUploadDocument(dataObj, applicationID).then(response => {
			Promise.resolve()
				.then(() => {
					dispatch(setProcessingStatus(false));
					dispatch(setErrorMessage(""));
					dispatch(saveSignatureData(img));
          // currentSection === "confirmDetailsPage" && dispatch(setPopupStatus(false));
          // currentSection === "confirmDetailsPage" && dispatch(setSignatureUploadStatus());
          // currentSection === "confirmDetailsPage" && dispatch(setSignatureUploadIsValidStatus(true));
				})
		}).catch(e => {
			dispatch(setProcessingStatus(false));
			dispatch(setErrorMessage(errorMsg));
			dispatch(showErrorMessage(errorMsg));
		});
    };
};

//Questions Submission
export const askQuestionsSubmission = (dataObj, applicationID, _this) => {
    if (LocalEnvironment) {
        return dispatch => {
            Application.askQuestionsSubmission(dataObj, applicationID);
            _this.props.onContinue();
        };
    } else {
        return (dispatch, getState) => {
            const errorMsg = getState().commonReducer.appData.globalErrors.apiException;
            Application.askQuestionsSubmission(dataObj, applicationID).then(response => {
                Promise.resolve()
                    .then(() => {
                        _this.props.onContinue();
                    })
            }).catch(e => {
                dispatch(setProcessingStatus(false))
                dispatch(setErrorMessage(errorMsg));
            });

        };
    }

};

export const setSessionExpirePopup = (showPopup) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_SESSIONEXPIRE_POPUP,
            showPopup
        })
    }
}

export const setMyInfoResponse = (response) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_MYINFO_RESPONSES,
            response
        })
    }
}

export const setInitateMyInfoResponse = (response) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_INITIATE_MYINFO_RESPONSES,
            response
        })
    }
}

export const setAccountHolderType = (accountHolderType) => {
    let isKeyManApply = accountHolderType.length > 0;
    return (dispatch) => {
        dispatch({
            type: types.SET_ACCOUNT_HOLDER_TYPE,
            isKeyManApply
        })
    }
}

export const submitApplication = (dataObj, applicationId, redirectToErrorPage) => {
    return (dispatch, getState) => {
        dispatch(setLoadingStatus(true));
        // const referenceNo = getState().applicationReducer.initiateMyinfoData.referenceNumber;
        // dispatch(setApplicationReferenceNo(referenceNo));
        // dispatch(setCurrentSection('thankyou'));
        Application.submitApplication(dataObj, applicationId).then(response => {
            const referenceNo = response.data.referenceNumber;
            dispatch(setApplicationReferenceNo(referenceNo));
            dispatch(setLoadingStatus(false));
            if (referenceNo !== null && referenceNo !== undefined && referenceNo !== '') {
                dispatch(setCurrentSection('thankyou'));
            } else {
                dispatch(setErrorWithValue("NOREF"));
                redirectToErrorPage && redirectToErrorPage();
            }
        }).catch(e => {
          if (e.response && e.response.data.errorCode && e.response.data.errorCode !== '') {
            dispatch(setErrorWithValue(e.response.errorCode));
            redirectToErrorPage && redirectToErrorPage();
          } else if ( e.response && e.response.status ) {
            dispatch(setErrorWithValue(`${e.response.status}Error`));
            redirectToErrorPage && redirectToErrorPage();
          } else {
            dispatch(setErrorWithValue("serviceDown"));
            redirectToErrorPage && redirectToErrorPage();
          }
            dispatch(setLoadingStatus(false));
        })
    }
}

export const setErrorWithValue = (errorCode) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ERROR_MESSAGE_WITH_ERRORCODE,
            errorCode
        })
    }
}
export const setErrorWithDescription = (description) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ERROR_MESSAGE_WITH_DESCRIPTION,
            description
        })
    }
}
export const storeParameter = (parameter) => {
    return (dispatch) => {
        dispatch({
            type: types.STORE_PARAMETER,
            parameter
        })
    }
}

export const isWhichTypeSTPorNSTP = (status) => {
    return (dispatch) => {
        dispatch({
            type: types.WHICH_TYPE,
            status
        })
    }
}

export const isKnockOutScenario = (isKnockOut) => {
    return (dispatch) => {
        dispatch({
            type: types.IS_KNOCKOUT_SCENARIO,
            isKnockOut
        })
    }
}

export const setWhichKnockOutScenarioFalse = (field) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_WHICH_KNOCKOUT_SCENARIO_FAIL,
            field
        })
    }
}
