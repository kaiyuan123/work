import * as types from "./actionTypes";

export const setAccountSetupData = (acountSetupObj, companyDetailsObj) => {
	const entityNameEmpty = acountSetupObj && acountSetupObj.name ? acountSetupObj.name : "";
	const contactEmpty = acountSetupObj && acountSetupObj.additionalServices.bibPlus.authorizedPerson.name ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.name : "";
	const phoneEmpty = acountSetupObj && acountSetupObj.additionalServices.bibPlus.authorizedPerson.mobileNumber ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.mobileNumber : "";
	const emailEmpty = acountSetupObj && acountSetupObj.additionalServices.bibPlus.authorizedPerson.emailAddress ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.emailAddress : "";
	const payNowChecked = acountSetupObj && acountSetupObj.additionalServices.corporatePayNow.suffix === null ? false : true;
	const bibPlusChecked = acountSetupObj && (acountSetupObj.additionalServices.bibPlus.groupId === null && acountSetupObj.additionalServices.bibPlus.groupId === null) ? false : true;
	const bulkChecked = acountSetupObj && acountSetupObj.additionalServices.bibPlus.additionalServices.bulkServicesInd === "N" ? false : true;
	const swiftChecked = acountSetupObj && acountSetupObj.additionalServices.bibPlus.additionalServices.remittanceServiceInd === "N" ? false : true;
	let transactionalToggle, loanRepaymentToggle, investmentToggle, othersToggle, otherPurpose;
	const purposeOfAccount = acountSetupObj.purpose ? acountSetupObj.purpose : '';

	if (purposeOfAccount) {
		purposeOfAccount.map((purpose, i) => {
			if (purpose === 'Transactional') {
				transactionalToggle = true;
			} else if (purpose === 'Loan Repayment') {
				loanRepaymentToggle = true;
			} else if (purpose === 'Investment') {
				investmentToggle = true;
			}
			else {
				othersToggle = true;
				otherPurpose = purpose.substring(purpose.lastIndexOf('-') + 2);
			}
			return true;
		});
	}
	return {
		type: types.MYINFO_SET_ACCOUNT_SETUP_DATA,
		entityName: acountSetupObj && acountSetupObj.name ? acountSetupObj.name : "",
		uen: acountSetupObj && companyDetailsObj.basicProfile.uen,
		principalName: acountSetupObj && acountSetupObj.additionalServices.bibPlus.authorizedPerson.name ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.name : "",
		mobileNumber: (acountSetupObj && acountSetupObj.additionalServices.bibPlus.authorizedPerson.mobileNumber) || "",
		emailAddress: (acountSetupObj && acountSetupObj.additionalServices.bibPlus.authorizedPerson.emailAddress) || "",
		entityNameMyinfo: entityNameEmpty !== "" ? true : false,
		contactMyinfo: contactEmpty !== "" ? true : false,
		phoneMyinfo: phoneEmpty !== "" ? true : false,
		emailMyinfo: emailEmpty !== "" ? true : false,
		chequebooks: (acountSetupObj && acountSetupObj.numberOfChequebooks) || "",
		transactional: transactionalToggle,
		loanRepayment: loanRepaymentToggle,
		investment: investmentToggle,
		others: othersToggle,
		otherPurpose: otherPurpose,
		entityNoSuffix: (acountSetupObj && acountSetupObj.additionalServices && acountSetupObj.additionalServices.corporatePayNow && acountSetupObj.additionalServices.corporatePayNow.suffix) || "",
		BIBGroupId: (acountSetupObj && acountSetupObj.additionalServices && acountSetupObj.additionalServices.bibPlus && acountSetupObj.additionalServices.bibPlus.groupId) || "",
		registerOfPayNow: payNowChecked,
		setupBIBPlus: bibPlusChecked,
		BIBBulkService: bulkChecked,
		BIBRemittanceMessage: swiftChecked,
	};
};

export const handleTextInputChange = (data, field) => {
	return (dispatch) => {
		dispatch({
			type: types.ACCOUNT_NAME_HANDLE_TEXT_INPUT_CHANGE,
			value: data.value,
			field,
			isValid: data.isValid,
			errorMsg: data.errorMsg,
			isInitial: data.isInitial
		});
	};
};


export const handleIsChecked = (field, isToggled, text, isValid) => {
	return (dispatch, getState) => {
		const purposeOfAccount = getState().accountSetupReducer.purposeOfAccount;

		if (field === "transactional" || field === "loanRepayment" || field === "investment" || field === "others") {
			if (!isToggled) {
				purposeOfAccount.push(text);
				dispatch(checkPurposeOfAccountIsValid(true))
			} else {
				purposeOfAccount.splice(purposeOfAccount.indexOf(text), 1);
				if (purposeOfAccount.length < 1) {
					dispatch(checkPurposeOfAccountIsValid(false))
				}
				else {
					dispatch(checkPurposeOfAccountIsValid(true))
				}
			}
		}

		dispatch({
			type: types.ACCOUNT_SETUP_HANDLE_TOGGLE,
			field,
			isToggled: !isToggled,
			isValid: !isValid,
			purposeOfAccount
		})
	}

};


export const setDropdownFocusStatus = (isFocus, field) => {
	return (dispatch) => {
		dispatch({
			type: types.ACCOUNT_SETUP_DROPDOWN_FOCUS,
			isFocus,
			field
		});
	};
};

export const selectDropdownItem = (value, description, field) => {
	return (dispatch) => {
		dispatch({
			type: types.ACCOUNT_SETUP_DROPDOWN_ITEM_SELECT,
			value,
			field,
			description
		})
	};
};

export const changeSearchInputValue = (searchValue, field) => {
	return (dispatch) => {
		dispatch({
			type: types.ACCOUNT_SETUP_DROPDOWN_SEARCH_INPUT_CHANGE,
			searchValue,
			field
		});
	};
};

export const checkPurposeOfAccountIsValid = (status) => {
	return (dispatch) => {
		dispatch({
			type: types.ACCOUNT_SETUP_PURPOSE_OF_ACCOUNT_IS_VALID,
			status
		});
	};
};
