import * as types from './actionTypes';
import scrollIntoView from 'scroll-into-view';
import { Config, LocalEnvironment } from './../api/httpApi';
import moment from "moment";

//change current step
export const changeCurrentStep = (step) => {
  return (dispatch) => {
    dispatch({
      type: types.CHANGE_CURRENT_STEP,
      step
    });
  };
};

//get initial application data
export const getInitialData = () => {
  return (dispatch) => {
    dispatch(setLoadingStatus(true));
    Config.getInitialData().then((response) => {
      dispatch({
        type: types.INITIAL_APP_DATA,
        appData: LocalEnvironment ? response.partner_form : response
      });
      dispatch(setLoadingStatus(false));
    });
  };
}

//set global error message
export const setErrorMessage = (messageContent) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_ERROR_CONTENT_MESSAGE,
      messageContent
    })
  };
}

export const setInlineErrorMessage = (inlineMessage) => {
  return {
    type: types.SET_INLINE_ERROR_MESSAGE,
    inlineMessage
  }
}

//set loading status
export const setLoadingStatus = (isLoading) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_APP_LOADING_STATUS,
      isLoading
    });
  };
}

export const setProcessingStatus = (isProcessing) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_APP_PROCESSING_STATUS,
      isProcessing
    });
  };
}


export const setJWTToken = (accessToken) => {
  return {
    type: types.SET_JWT_TOKEN,
    accessToken
  }
}

export const scrollToSection = (nextStep) => {
  // similar to nextTick in node. will push function to the next queue stack.
  return (dispatch) => {
    Promise.resolve().then(() => {
      dispatch(setCurrentSection(nextStep));
    }).then(() => {
      const element = document.querySelector(`[id='${nextStep}-section']`);
      scrollIntoView(element, { time: 500, align: { top: 0 } });
    });
  }
}

export const scrollBackToSection = (nextStep) => {
  // similar to nextTick in node. will push function to the next queue stack.
  return (dispatch) => {
    Promise.resolve().then(() => {
      const element = document.querySelector(`[id='${nextStep}-section']`);
      scrollIntoView(element, { time: 500, align: { top: 0 } });
    });
  }
}


export const scrollToPopupErrorElement = (element) => {
  // similar to nextTick in node. will push function to the next queue stack.
  return (dispatch) => {
    Promise.resolve().then(() => {
      // var myElement = document.querySelector('.popup-errorMsg');
      // var topPos = myElement.parentElement.offsetTop;
      document.querySelector('.popup-errorMsg').scrollIntoView();
    });
  }
}

export const scrollToElement = (element) => {
  // similar to nextTick in node. will push function to the next queue stack.
  return (dispatch) => {
    Promise.resolve().then(() => {
      const toElement = document.querySelector(`[id='${element}']`);
      scrollIntoView(toElement, { time: 500, align: { top: 0.1 } });
    });
  }
}

export const setCurrentSection = (currentSection) => {
  return {
    type: types.SET_CURRENT_SECTION,
    currentSection
  }
}


export const setIdentifier = (identifier) => {
  return {
    type: types.SET_IDENTIFIER,
    identifier
  }
}

export const setShowDocumentStatus = (status) => {
  return {
    type: types.SET_SHOW_DOCUMENT_STATUS,
    status
  }
}

export const setMyInfoFlowStatus = (status) => {
  return {
    type: types.SET_MYINFO_FLOW_STATUS,
    status
  }
}

//handle HelpMessage click
export const setHelpMessageExpanded = (isExpand) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_HELP_MESSAGE_EXPANDED,
      isExpanded: !isExpand
    });
  };
}

export const setApplicationReferenceNo = (referenceNo) => {
  return {
    type: types.SET_APPLICATION_REFERENCE_NO,
    referenceNo
  }
}

export const setExpiryDate = (date) => {
  const expiryDate = date ? moment(date.substring(0, date.indexOf(' '))).subtract(1, 'd').format('DD/MM/YYYY') : '';
  return {
    type: types.SET_APPLICATION_EXPIRY_DATE,
    expiryDate
  }
}

export const setRetrievingMyInfo = (status) => {
  return {
    type: types.SET_RETRIEVING_MYINFO,
    status
  }
}

// handle set error msg
export const handleErrorMessage = (field, errorMsg) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_ERROR_MESSAGE_INPUT,
      field,
      errorMsg
    })
  }
}

export const isMobileDevice = (status) => {
  return {
    type: types.IS_MOBILE_DEVICE,
    status
  }
}

export const handleTextInputFocus = (field, status) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_TEXT_INPUT_FOCUS,
      field,
      status
    })
  }
}

export const handleTextInputWithKeyFocus = (key, field, status) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_TEXT_INPUT_FOCUS_WITH_KEY,
      field,
      status,
      key
    })
  }
}

export const setTextInputHasEdited = (field) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_TEXT_INPUT_HAS_EDITED,
      field
    })
  }
}

//set compatibility error message
export const setCompatibilityErrorMessage = (messageContent) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_ERROR_COMPATIBILITY_MESSAGE,
      messageContent
    })
  }
}

export const setDuplicateTabId = (field, id) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_DUPLICATE_TAB_ID,
      id,
      field
    })
  }
}

export const setMyInfoParams = (myInfoParams) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_MYINFO_PARAMS,
      myInfoParams
    })
  }
}

export const toggleNav = (status) => {
  if (status === true) {
    status = false;
  } else {
    status = true;
  }
  return (dispatch) => {
    dispatch({
      type: types.TOGGLE_NAV,
      status
    });
  };
}

export const toggleLeftSideBar = (status) => {
  return (dispatch) => {
    dispatch({
      type: types.TOGGLE_LEFT_SIDE_BAR,
      status
    })
  }
}