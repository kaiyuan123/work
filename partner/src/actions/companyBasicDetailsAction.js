import * as types from "./actionTypes";

export const setCompanyBasicData = (response) => {
    const obj = response.entity.basicProfile || {};
    const primaryClienteleBase = response.entity.primaryClienteleBase ? response.entity.primaryClienteleBase : '';
    let individualToggle, corporateToggle;
    if (primaryClienteleBase) {
        primaryClienteleBase.map((base, i) => {
            if (base === 'Individual') {
                individualToggle = true;
            } else if (base === 'Corporate') {
                corporateToggle = true;
            } 
            return true;
        });
    }
    return {
        type: types.MYINFO_SET_COMPANY_BASIC_DETAIL_DATA,
        companyRegistrationNumber: obj && obj["uen"] ? obj["uen"] : "",
        registeredCompanyName: obj && obj.entityName ? obj.entityName : "",
        typeOfCompany: obj && obj["companyType"] ? obj["companyType"] : "",
        companyStatus: obj && obj["entityStatus"] ? obj["entityStatus"] : "",
        countryOfIncorporation: obj && obj["countryOfIncorporation"] ? obj["countryOfIncorporation"] : "",
        ownership: obj && obj["ownership"] ? obj["ownership"] : "",
        registrationDate: obj && obj["registrationDate"] ? obj["registrationDate"] : "",
        companyExpiryDate: obj && obj["businessExpiryDate"] ? obj["businessExpiryDate"] : "",
        primaryActivityCode:
            obj && obj["primaryActivityCode"]
                ? obj["primaryActivityCode"]
                : "",
        primaryActivityDesc:
            obj && obj["primaryActivityDesc"]
                ? obj["primaryActivityDesc"]
                : "",
        secondaryActivityCode: obj && obj["secondaryActivityCode"] ? obj["secondaryActivityCode"] : "",
        secondaryActivityDesc: obj && obj["secondaryActivityDesc"] ? obj["secondaryActivityDesc"] : "",
        annualTurnover: response.entity && response.entity.annualTurnover ? response.entity.annualTurnover : "",
        questionnaires: response.questions || [],
        primaryClienteleBase: primaryClienteleBase ? primaryClienteleBase : [],
        individualClients: individualToggle ? individualToggle : false,
        corporateClients: corporateToggle ? corporateToggle : false
    };
};

export const setMailingAddressCheckbox = (isToggled) => {
    return {
        type: types.MYINFO_SET_MAILING_ADDRESS_SHOW,
        isToggled
    }
}

export const setCompanyUnformattedAddress = (obj) => {
    return {
        type: types.MYINFO_SET_COMPANY_UNFORMATTED_ADDRESS_DATA,
        unformattedCompanyCountry: obj && obj["country"] ? obj["country"] : "-",
        companyAddressLine1: obj && obj["line1"] ? obj["line1"] : "-",
        companyAddressLine2: obj && obj["line2"] ? obj["line2"] : "-"
    }
}

export const setCompanyAddressType = (standard) => {
    return {
        type: types.MYINFO_SET_COMPANY_ADDRESS_TYPE,
        standard: standard
    }
}

export const handleIsChecked = (field, isToggled) => {
    return {
        type: types.LENDING_IS_CHECKED,
        field,
        isToggled: !isToggled
    }
}

export const checkPrimaryClienteleIsValid = (status) => {
    return (dispatch) => {
        dispatch({
            type: types.PRIMARY_CLIENTELE_IS_VALID,
            status
        });
    };
};

//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
    return (dispatch) => {
        dispatch({
            type: types.MAILINGADDRESS_DROPDOWN_FOCUS,
            isFocus,
            field
        });
    };
}

//choose item from dropdown
export const selectDropdownItem = (value, field, description) => {
    if (field === "mailingCountry") {
        const element = document.getElementById("editableDropdown-flagImg");
        element.style.display = "block";
    }

    return (dispatch) => {
        dispatch({
            type: types.MAILINGADDRESS_DROPDOWN_ITEM_SELECT,
            value,
            field,
            description
        })
    };
}

export const handleTextInputChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.COMPANYBASIC_HANDLE_TEXT_INPUT_CHANGE,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
};

export const changeSearchInputValue = (searchValue, field) => {
    return (dispatch) => {
        dispatch({
            type: types.COMPANYBASICDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE,
            searchValue,
            field
        });
    };
}

export const selectRadioItem = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.COMPANY_SELECT_RADIO_BUTTON,
            value: data.value,
            field
        })
    }
}
