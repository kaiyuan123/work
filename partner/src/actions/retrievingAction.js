import * as types from "./actionTypes";
import localStore from './../common/localStore';
import { Application } from "./../api/httpApi";
import { setLoadingStatus } from "./commonAction";
import { setErrorWithValue, storeParameter, setErrorWithDescription } from "./applicationAction";

export const storeMyInfoParameter = (parameter) => {
  return (dispatch) => {
    dispatch({
      type: types.STORE_MY_INFO_PARAMETER,
      parameter
    })
  }
}

export const getCallback = (parameter, redirectToErrorPage, redirectToApplication) => {
  return (dispatch) => {
    Application.myInfoCallback(parameter).then(response => {
      Promise.resolve().then(() => {
        dispatch({
          type: types.STORE_MY_INFO_REPONSES,
          response: response.data
        })
      }).then(() => {
        const params = localStore.getStore('params');
        dispatch(storeParameter(params));
        dispatch(storeMyInfoParameter(parameter));
        const search = response.data.sourceType;
        redirectToApplication && redirectToApplication(search);
      });
    }).catch(e => {
      if (e.response && e.response.data.errorCode && e.response.data.errorCode !== "") {
        let code = e.response.data.errorCode;
        if (code === "invalid.productCode") {
          code = "InvalidProductCode"
        }

        if (code === "myinfo.error") {
          code = "MyInfoError";
        }

        if (code === "myinfo.required") {
          code = "MyInfoRequired";
        }

        if (e.response.data.application) {
          dispatch(setErrorWithDescription(e.response.data.application));
        }

        dispatch(setErrorWithValue(code));
        redirectToErrorPage && redirectToErrorPage();
      } else if ( e.response && e.response.status ) {
        dispatch(setErrorWithValue(`${e.response.status}Error`));
        redirectToErrorPage && redirectToErrorPage();
      } else {
        dispatch(setErrorWithValue("serviceDown"));
        redirectToErrorPage && redirectToErrorPage();
      }
      dispatch(setLoadingStatus(false));
    });
  }
}
