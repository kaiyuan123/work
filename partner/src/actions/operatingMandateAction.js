import * as types from "./actionTypes";
import { capitalize } from "../common/utils";

export const setOperatingMandateData = (operatingMandateObj, mainApplicantObj) => {
	//const approvedSignatories = operatingMandateObj.approvedSignatories || [];
	const approvedSignatories = ((operatingMandateObj.approvedSignatories || []).filter(e => e.userId)).concat(((operatingMandateObj.approvedSignatories || []).filter(e => !e.userId)));

	return (dispatch) => {
		dispatch(populateSignatoryList(approvedSignatories[0], 0));
		if (approvedSignatories.length > 1) {
			for (let i = 1; i < approvedSignatories.length; i++) {
				dispatch(incrementSignatoryList(i));
				dispatch(populateAdditionalSignatoryList(approvedSignatories[i], i));
			}
		}
		dispatch({
			type: types.APPROVED_SIGNATORY_LIST,
			signatories: approvedSignatories,
			mainApplicantLegalId: mainApplicantObj.basicInfo.legalId,
			signatorySelected: operatingMandateObj.signatorySelected || ""
		})
	}
}

export const populateSignatoryList = (obj, key) => {
	return (dispatch) => {
		// let setupBIBPlusUser = getState().accountSetupReducer.setupBIBPlus.isToggled;
		const name = capitalize(obj.name);
		const nric = obj.legalId;
		const email = obj.emailAddress ? obj.emailAddress : "";
		const phone = obj.mobileNumber ? obj.mobileNumber : "";
		const userId = obj.userId ? obj.userId : "";
		const bibUserToggle = obj.bibPlusUserInd === "Y" ? true : false;

		dispatch({
			type: types.POPULATE_SIGNATORY_LIST,
			key: `signatories${key}`,
			signatoriesName: name,
			signatoriesNRIC: nric,
			signatoriesEmail: email,
			signatoriesMobileNo: phone,
			signatoriesBibUserId: userId,
			signatoriesToggleBIBUser: bibUserToggle,
			signatoriesReadOnlyBibUserId: userId ? true : false,
			phoneMyinfo: phone !== "" ? true : false,
			emailMyinfo: email !== "" ? true : false,
			bibUserIdMyinfo: userId !== "" ? true : false
		})
	}
}

export const incrementSignatoryList = (key) => {
	return (dispatch, getState) => {
		let signatoriesID = getState().operatingMandateReducer.signatoriesID;
		signatoriesID.push(key);
		dispatch({
			type: types.INCREMENT_SIGNATORYLIST_COUNT,
			signatoriesID
		});
	};
}

//Populate other Signatory List items
export const populateAdditionalSignatoryList = (obj, key) => {
	return (dispatch) => {
		// let setupBIBPlusUser = getState().accountSetupReducer.setupBIBPlus.isToggled;
		const name = capitalize(obj.name);
		const nric = obj.legalId;
		const email = obj.emailAddress ? obj.emailAddress : "";
		const phone = obj.mobileNumber ? obj.mobileNumber : "";
		const userId = obj.userId ? obj.userId : "";
		const bibUserToggle = obj.bibPlusUserInd === "Y" ? true : false;

		dispatch({
			type: types.POPULATE_ADDITIONAL_SIGNATORY_LIST,
			signatories: `signatories${key}`,
			signatoriesName: name,
			signatoriesNRIC: nric,
			signatoriesEmail: email,
			signatoriesMobileNo: phone,
			signatoriesBibUserId: userId,
			signatoriesToggleBIBUser: bibUserToggle,
			signatoriesReadOnlyBibUserId: userId ? true : false,
			phoneMyinfo: phone !== "" ? true : false,
			emailMyinfo: email !== "" ? true : false,
			bibUserIdMyinfo: userId !== "" ? true : false
		})
	}
}


export const handleIsSignatoryChecked = (data, field) => {
	return (dispatch) => {
		dispatch({
			type: types.SIGNATORY_CHECKED_VALUE,
			value: data.value,
			field
		})
	}
}

export const handleTextInputChange = (data, key, field) => {
	return (dispatch) => {
		dispatch({
			type: types.OPERATING_MANDATE_NAME_HANDLE_TEXT_INPUT_CHANGE,
			key,
			value: data.value,
			field,
			isValid: data.isValid,
			errorMsg: data.errorMsg,
			isInitial: data.isInitial
		});
	};
};

export const updateBibUserToggle = (key, isToggled) => {
	const bibUserToggle = isToggled;
	return {
		type: types.UPDATE_BIB_USER_TOGGLE,
		field: `signatories${key}`,
		isToggled: bibUserToggle,
		isValid: true
	}
}

export const handleBibPlusUserToggle = (field, isToggled) => {
	return {
		type: types.BIBPLUSEUSER_HANDLE_TOGGLE,
		field,
		isToggled: isToggled
	}
}

export const handleIsChecked = (key, field, isToggled, isValid) => {
	return {
		type: types.BIBUSER_HANDLE_TOGGLE,
		key,
		field,
		isToggled: !isToggled,
		isValid: !isValid
	}
};

// handle set error msg
export const handleSignatoriesAdditionalErrorMessage = (key, field, errorMsg) => {
	return (dispatch) => {
		dispatch({
			type: types.SET_SIGNATORIES_ADDITIONAL_ERROR_MESSAGE_INPUT,
			field,
			key: `signatories${key}`,
			errorMsg
		})
	}
}

export const setSignatoriesPartnersInputError = (key, field, errorMsg) => {
	return {
		type: types.SET_SIGNATORIES_PARTNERS_INPUT_ERROR,
		key,
		field,
		errorMsg
	}
}
