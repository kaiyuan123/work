import * as types from "./actionTypes";

export const setasrApprovedPersonData = (obj) => {
    return {
        type: types.MYINFO_SET_ASR_APPROVED_PERSON_DATA,
        particularsOfApprovedPersons: obj.approvedPersons ? obj.approvedPersons : "",
        applicantName: obj.applicantName ? obj.applicantName : "",
        resolutionDate: obj.date ? obj.date : ""
    };
};


export const prefilledapplicantResolution = (entityType, businessConstitution) => {
    return (dispatch) => {
        let value = "";
        if (entityType.value === 'LC') {
            value = "1";
        }
        else if (entityType.value === 'BN') {
            if (businessConstitution.value === 'P') {
                value = "4";
            }
        } else if (entityType.value === 'LL') {
            value = "2";
        } else if (entityType.value === 'LP') {
            value = "3";
        }
        const data = { "value": value }
        dispatch(selectRadioItem(data, 'applicantResolution'));
        dispatch({
            type: types.SET_ENTITY_DATA,
            entityType: entityType,
            businessConstitution: businessConstitution
        })
    }
}

export const prefilledSignatorySelected = (signatorySelected) => {
    return (dispatch) => {
        dispatch(selectRadioItem(signatorySelected, 'signingApprovedPerson'));
        dispatch({
            type: types.SET_SIGNING_APPROVED_PERSON_DATA
        })
    }
}

export const selectRadioItem = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.SIGNING_CONDITIONS_RADIO_BUTTON,
            value: data.value,
            field
        })
    }
}
