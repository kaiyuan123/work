import * as types from "./actionTypes";
import { scrollToElement } from "./../actions/commonAction";

export const setCPData = (obj) => {
    return (dispatch) => {
        dispatch(populateCP(obj[0], 0));
        if (obj.length > 1) {
            for (let i = 1; i < obj.length; i++) {
                dispatch(incrementCP(i));
                dispatch(populateAdditionalCP(obj[i], i));
            }
        }
    }
}

export const populateAdditionalCP = (obj, key) => {
    return (dispatch) => {
        const crs = obj.crs;
        let checkboxSG = false;
        let checkboxUS = false;
        let checkboxOthers = false;
        let tinNoSG = "";
        let tinNoUS = "";

        const tinNoOthers = [];
        crs.map((obj) => {
            if (obj.country === "SG") {
                checkboxSG = true;
                tinNoSG = obj.tin && obj.tin !== null ? obj.tin : "";
            }
            else if (obj.country === "US") {
                checkboxUS = true;
                tinNoUS = obj.tin && obj.tin !== null ? obj.tin : "";

            } else {
                checkboxOthers = true;
                tinNoOthers.push(obj);
            }
            return {
                checkboxSG, checkboxUS, checkboxOthers, tinNoSG, tinNoUS, tinNoOthers
            };
        })

        dispatch({
            type: types.POPULATE_ADDITIONAL_CP,
            key: `controllingPerson${key}`,
            cpName: obj.name,
            cpIdNo: obj.legalId,
            cpMobileNo: obj.mobileNumber,
            cpEmail: obj.emailAddress,
            cpPercentageOwnership: obj.percentageOfOwnership,
            checkboxSG: checkboxSG,
            checkboxUS: checkboxUS,
            checkboxOthers: checkboxOthers,
            tinNoSG: tinNoSG,
            tinNoUS: tinNoUS
        })
        if (tinNoOthers.length > 0) {
            dispatch(populateTinNoOthersCP(`controllingPerson${key}`, tinNoOthers));
        }

    }
}

export const incrementCP = (key) => {
    return (dispatch, getState) => {
        let controllingPersonId = getState().taxSelfDeclarationsReducer.controllingPersonId;
        controllingPersonId.push(key);
        dispatch({
            type: types.CRS_INCREMENT_CP_COUNT,
            controllingPersonId
        });
    };
}

export const populateCP = (obj, key) => {
    return (dispatch) => {
        const crs = obj.crs;
        let checkboxSG = false;
        let checkboxUS = false;
        let checkboxOthers = false;
        let tinNoSG = "";
        let tinNoUS = "";

        const tinNoOthers = [];
        crs.map((obj) => {
            if (obj.country === "SG") {
                checkboxSG = true;
                tinNoSG = obj.tin && obj.tin !== null ? obj.tin : "";
            }
            else if (obj.country === "US") {
                checkboxUS = true;
                tinNoUS = obj.tin && obj.tin !== null ? obj.tin : "";
            } else {
                checkboxOthers = true;
                tinNoOthers.push(obj);
            }
            return {
                checkboxSG, checkboxUS, checkboxOthers, tinNoSG, tinNoUS, tinNoOthers
            };
        })

        if (tinNoOthers.length > 0) {
            dispatch(populateTinNoOthersCP(`controllingPerson${key}`, tinNoOthers));
        }

        dispatch({
            type: types.POPULATE_CP,
            key: `controllingPerson${key}`,
            cpName: obj.name,
            cpIdNo: obj.legalId,
            cpMobileNo: obj.mobileNumber,
            cpEmail: obj.emailAddress,
            cpPercentageOwnership: obj.percentageOfOwnership,
            checkboxSG: checkboxSG,
            checkboxUS: checkboxUS,
            checkboxOthers: checkboxOthers,
            tinNoSG: tinNoSG,
            tinNoUS: tinNoUS
        })
    }
}

export const populateTinNoOthersCP = (key, array) => {
    return (dispatch) => {
        dispatch(populateFirstTinCP(key, array[0], 0));
        if (array.length > 1) {
            for (let i = 1; i < array.length; i++) {
                dispatch(incrementCountryCP(key, i));
                dispatch(populateAdditionalTinNoOthersCP(key, array[i], i));
            }
        }
    }
}

export const populateAdditionalTinNoOthersCP = (cpKey, obj, key) => {
    let haveTINNo = "";
    const othersTINNo = obj.tin !== null && obj.tin !== "" ? obj.tin : "";
    const reasons = obj.reasonCode !== null && obj.reasonCode !== "" ? obj.reasonCode : "";
    const othersOthers = obj.reasonDetail !== null && obj.reasonDetail !== "" ? obj.reasonDetail : "";

    if (othersTINNo !== "") {
        haveTINNo = "Y";
    }

    if (reasons !== "") {
        haveTINNo = "N";
    }

    return {
        type: types.POPULATE_ADDITIONAL_TIN_CP,
        cpKey,
        key: `tinNoOthers${key}`,
        othersCountry: obj.country !== null && obj.country !== "" ? obj.country : "",
        haveTINNo: haveTINNo,
        othersTINNo: othersTINNo,
        reasons: reasons,
        othersOthers: othersOthers

    }
}

//increment count
export const incrementCountryCP = (cpKey, key) => {
    return (dispatch, getState) => {
        let tinNoOthersId = getState().taxSelfDeclarationsReducer[cpKey][`tinNoOthersId`];
        tinNoOthersId.push(key);
        dispatch({
            type: types.CRS_INCREMENT_COUNTRY_COUNT_CP,
            cpKey,
            tinNoOthersId
        });
    };
}

export const populateFirstTinCP = (cpKey, obj, key) => {
    return (dispatch) => {
        let haveTINNo = "";
        const othersTINNo = obj.tin !== null && obj.tin !== "" ? obj.tin : "";
        const reasons = obj.reasonCode !== null && obj.reasonCode !== "" ? obj.reasonCode : "";
        const othersOthers = obj.reasonDetail !== null && obj.reasonDetail !== "" ? obj.reasonDetail : "";

        if (othersTINNo !== "") {
            haveTINNo = "Y";
        }

        if (reasons !== "") {
            haveTINNo = "N";
        }

        dispatch({
            type: types.POPULATE_FIRST_TIN_CP,
            cpKey,
            key: `tinNoOthers${key}`,
            othersCountry: obj.country !== null && obj.country !== "" ? obj.country : "",
            haveTINNo: haveTINNo,
            othersTINNo: othersTINNo,
            reasons: reasons,
            othersOthers: othersOthers
        })
    }
}

export const handleIsChecked = (field, isToggled) => {
    return {
        type: types.CRS_IS_CHECKED,
        field,
        isToggled: !isToggled
    }
}

// handle set error msg
export const handleAdditionalErrorMessage = (key, field, errorMsg) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT,
            field,
            key: `tinNoOthers${key}`,
            errorMsg
        })
    }
}

export const uncheckRest = () => {
    return {
        type: types.CRS_UNCHECK_REST
    }
}

export const uncheckNone = () => {
    return {
        type: types.CRS_UNCHECK_NONE
    }
}

export const handleButtonChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_BUTTON_CHANGE,
            value: data,
            field
        });
    };
};


export const handleButtonChangeOthers = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_BUTTON_CHANGE_OTHERS,
            value: data,
            key,
            field
        });
    };
};

export const handleTextInputChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_CHANGE,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
};


export const handleTextInputChangeOthers = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_CHANGE_OTHERS,
            key,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
}

//handle dropdown focus
export const setDropdownFocusStatusChange = (isFocus, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_FOCUS_CHANGE,
            isFocus,
            key,
            field
        });
    };
};

//choose item from dropdown
export const selectDropdownItemChange = (value, description, key, field) => {
    return (dispatch, getState) => {
        const othersCountry = getState().taxSelfDeclarationsReducer[key] ? getState().taxSelfDeclarationsReducer[key].othersCountry.value : '';
        if (field === "othersCountry" && othersCountry !== "") {
            const element = document.getElementById("editableDropdown-flagImg");
            element.style.display = "block";
        }

        dispatch({
            type: types.CRS_DROPDOWN_ITEM_SELECT_CHANGE,
            value,
            key,
            field,
            description
        })
    };
};

export const changeSearchInputValueOther = (searchValue, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_SEARCH_INPUT_CHANGE,
            key,
            searchValue,
            field
        });
    };
};


//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_FOCUS,
            isFocus,
            field
        });
    };
};

export const selectStatus = (value, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_SELECT_STATUS,
            value: value,
            field
        });
    };
}

//choose item from dropdown
export const selectDropdownItem = (value, description, field) => {
    return (dispatch, getState) => {
        const countryNone = getState().taxSelfDeclarationsReducer ? getState().taxSelfDeclarationsReducer.countryNone.value : '';
        if (field === "countryNone" && countryNone !== "") {
            const element = document.getElementById("editableDropdown-flagImg");
            element.style.display = "block";
        }

        dispatch({
            type: types.CRS_DROPDOWN_ITEM_SELECT,
            value,
            field,
            description
        })
    };
};

export const changeSearchInputValue = (searchValue, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_SEARCH_INPUT,
            searchValue,
            field
        });
    };
};

export const selectRadioItem = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_SELECT_RADIO_BUTTON,
            value: data.value,
            key,
            field
        })
    }
}

//increment count
export const incrementCountry = (key) => {
    return (dispatch, getState) => {
        let tinNoOthersId = getState().taxSelfDeclarationsReducer.tinNoOthersId;
        tinNoOthersId.push(key);
        dispatch({
            type: types.CRS_INCREMENT_COUNTRY_COUNT,
            tinNoOthersId
        });
    };
}

export const addNewCountry = (key) => {
    return {
        type: types.CRS_ADD_COUNTRY,
        tinNoOthers: `tinNoOthers${key + 1}`

    }
}
//Remove partner
export const removeCountry = (key) => {
    return {
        type: types.CRS_REMOVE_COUNTRY,
        partners: `tinNoOthers${key}`
    }
}

//decrease count
export const decreaseCountry = (key) => {
    return (dispatch, getState) => {
        let tinNoOthersId = getState().taxSelfDeclarationsReducer.tinNoOthersId;
        let index = tinNoOthersId.indexOf(key);

        dispatch({
            type: types.CRS_DECREASE_COUNTRY,
            tinNoOthersId: [...tinNoOthersId.slice(0, index), ...tinNoOthersId.slice(index + 1)]
        });
    };
}

export const setTinNoOthersInputError = (key, field, errorMsg) => {
    return (dispatch) => {
        dispatch(scrollToElement(field + key));
        dispatch({
            type: types.SET_CRS_OTHERS_INPUT_ERROR,
            key: `tinNoOthers${key}`,
            field,
            errorMsg
        })
    }
}

export const setTaxSelfDetails = (obj) => {

    const crs = obj.crs !== [] && obj.crs !== null ? obj.crs : "";
    let checkboxSG = false;
    let checkboxUS = false;
    let checkboxOthers = false;
    let checkboxNone = false;
    let tinNoSG = "";
    let tinNoUS = "";
    let isUSResident = "";
    let countryNone = "";
    let haveTINNoNone = "";
    let tinNoNone = "";
    let reasonsNone = "";
    let othersNone = "";
    const tinNoOthers = [];
    crs.map((obj) => {
        if (obj.country === "SG" && obj.taxResidencyInd === "Y") {
            checkboxSG = true;
            tinNoSG = obj.tin && obj.tin !== null ? obj.tin : "";
        }
        else if (obj.country === "US" && obj.taxResidencyInd === "Y") {
            checkboxUS = true;
            tinNoUS = obj.tin && obj.tin !== null ? obj.tin : "";
            isUSResident = obj.usPersonInd;
        }
        else if (obj.taxResidencyInd !== "" && obj.taxResidencyInd !== null && obj.taxResidencyInd === "N") {
            checkboxNone = true;
            countryNone = obj.country;
            tinNoNone = obj.tin !== null && obj.tin !== "" ? obj.tin : "";
            reasonsNone = obj.reasonCode !== null && obj.reasonCode !== "" ? obj.reasonCode : "";

            if (tinNoNone !== "") {
                haveTINNoNone = "Y";
            }

            if (reasonsNone !== "") {
                haveTINNoNone = "N";
                if (reasonsNone === "3") {
                    othersNone = obj.reasonDetail !== null && obj.reasonDetail !== "" ? obj.reasonDetail : "";
                }
            }
        }
        else {
            checkboxOthers = true;
            tinNoOthers.push(obj);
        }
        return {
            checkboxSG, checkboxUS, checkboxOthers, tinNoSG, tinNoUS, isUSResident, tinNoOthers, checkboxNone, countryNone
        };
    })
    return (dispatch) => {
        dispatch({
            type: types.SET_TAX_SELF_DETAILS,
            fatcaCRSStatus: obj.fatcaStatus !== "" && obj.fatcaStatus !== null ? obj.fatcaStatus : "",
            checkboxSG: checkboxSG,
            checkboxUS: checkboxUS,
            checkboxOthers: checkboxOthers,
            checkboxNone: checkboxNone,
            tinNoSG: tinNoSG,
            tinNoUS: tinNoUS,
            isUSResident: isUSResident,
            countryNone: countryNone,
            haveTINNoNone: haveTINNoNone,
            tinNoNone: tinNoNone,
            reasonsNone: reasonsNone,
            othersNone: othersNone
        })
        if (tinNoOthers.length > 0) {
            dispatch(populateTinNoOthers(tinNoOthers));

        }
    }
}

export const populateTinNoOthers = (array) => {
    return (dispatch) => {
        dispatch(populateFirstTin(array[0], 0));
        if (array.length > 1) {
            for (let i = 1; i < array.length; i++) {
                if (array[i]) {
                    dispatch(incrementCountry(i))
                    dispatch(populateAdditionalTinNoOthers(array[i], i));
                }
            }
        }
    }
}

export const populateFirstTin = (obj, key) => {
    return (dispatch) => {
        let haveTINNo = "";
        const othersTINNo = obj.tin !== null && obj.tin !== "" ? obj.tin : "";
        const reasons = obj.reasonCode !== null && obj.reasonCode !== "" ? obj.reasonCode : "";
        const othersOthers = obj.reasonDetail !== null && obj.reasonDetail !== "" ? obj.reasonDetail : "";

        if (othersTINNo !== "") {
            haveTINNo = "Y";
        }

        if (reasons !== "") {
            haveTINNo = "N";
        }

        dispatch({
            type: types.POPULATE_FIRST_TIN,
            key: `tinNoOthers${key}`,
            othersCountry: obj.country !== null && obj.country !== "" ? obj.country : "",
            haveTINNo: haveTINNo,
            othersTINNo: othersTINNo,
            reasons: reasons,
            othersOthers: othersOthers
        })
    }
}


export const populateAdditionalTinNoOthers = (obj, key) => {
    let haveTINNo = "";
    const othersTINNo = obj.tin !== null && obj.tin !== "" ? obj.tin : "";
    const reasons = obj.reasonCode !== null && obj.reasonCode !== "" ? obj.reasonCode : "";
    const othersOthers = obj.reasonDetail !== null && obj.reasonDetail !== "" ? obj.reasonDetail : "";

    if (othersTINNo !== "") {
        haveTINNo = "Y";
    }

    if (reasons !== "") {
        haveTINNo = "N";
    }

    return {
        type: types.POPULATE_ADDITIONAL_TIN,
        key: `tinNoOthers${key}`,
        othersCountry: obj.country !== null && obj.country !== "" ? obj.country : "",
        haveTINNo: haveTINNo,
        othersTINNo: othersTINNo,
        reasons: reasons,
        othersOthers: othersOthers

    }
}
