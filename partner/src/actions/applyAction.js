import localStore from './../common/localStore';
import queryString from 'query-string';
import * as types from "./actionTypes";

export const setParamterToLocalStorage = (parameter) => {
  const params =  queryString.parse(parameter);
  localStore.setStore('uob_bizconnect_params', params);
}

export const setApplyLoanData = (companyDetailsDataObj) => {
  localStore.setStore('dataObj',companyDetailsDataObj);
  return {
    type: types.SET_APPLY_APPLICATION_DATA,
    entityName: companyDetailsDataObj.entityName,
    expiryDate: companyDetailsDataObj.expiryDate,
    applicationId: companyDetailsDataObj.applicationId,
    refNo: companyDetailsDataObj.referenceNumber,
    productCode: companyDetailsDataObj.productCode,
    status: companyDetailsDataObj.status
  }
}
