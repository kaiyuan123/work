import * as types from "./actionTypes";
import { setTextInputHasEdited } from "./commonAction";
import moment from "moment";

export const handleTextInputChange = (data, field) => {
  return (dispatch) => {
    dispatch({
      type: types.CONTACTDETAILS_HANDLE_TEXT_INPUT_CHANGE,
      value: data.value,
      field,
      isValid: data.isValid,
      errorMsg: data.errorMsg,
      isInitial: data.isInitial
    });
    dispatch(setTextInputHasEdited(field));
  };
};

export const setMyInfoContactDetailsData = (shareholdersDataObj, companyDetailsDataObj, createdBy, created) => {
  const contactDetailsDataObj = shareholdersDataObj && shareholdersDataObj[0].basicInfo;
  const emailEmpty = contactDetailsDataObj && contactDetailsDataObj.emailAddress ? contactDetailsDataObj.emailAddress : '';
  const createdDate = created ? moment(created, "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY')  : '';
  const createdTime = created ? moment(created, "YYYY-MM-DD HH:mm:ss").format("h:mm a") : '';
  return {
    type: types.SET_MYINFO_CONTACT_DETAILS,
    personType: shareholdersDataObj && shareholdersDataObj[0].type ? shareholdersDataObj[0].type : '',
    emailAddress: contactDetailsDataObj && contactDetailsDataObj.emailAddress ? contactDetailsDataObj.emailAddress : '',
    emailMyinfo: emailEmpty !== "" ? true : false,
    entityName: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.entityName ? companyDetailsDataObj.basicProfile.entityName : '',
    uen: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.uen ? companyDetailsDataObj.basicProfile.uen : '',
    entityType: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.entityType ? companyDetailsDataObj.basicProfile.entityType : '',
    companyType: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.companyType ? companyDetailsDataObj.basicProfile.companyType : '',
    businessConstitution: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.businessConstitution ? companyDetailsDataObj.basicProfile.businessConstitution : '',
    principalName: contactDetailsDataObj && contactDetailsDataObj.principalName ? contactDetailsDataObj.principalName : '',
    alternateNames: contactDetailsDataObj && contactDetailsDataObj.alternateNames ? contactDetailsDataObj.alternateNames : {},
    mobileNumber: contactDetailsDataObj && contactDetailsDataObj.mobileNumber ? contactDetailsDataObj.mobileNumber : '',
    annualTurnover: companyDetailsDataObj && companyDetailsDataObj.annualTurnover ? companyDetailsDataObj.annualTurnover : '',
    requestLoan: companyDetailsDataObj && companyDetailsDataObj.requestLoan ? companyDetailsDataObj.requestLoan : '',
    natureOfBusiness: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.natureOfBusiness ? companyDetailsDataObj.basicProfile.natureOfBusiness : '',
    primaryCountryOfOperation: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.primaryCountryOfOperation ? companyDetailsDataObj.basicProfile.primaryCountryOfOperation : '',
    mainApplicantName: createdBy ? createdBy: '',
    created: createdDate ? createdDate: '',
    createdTime: createdTime ? createdTime : ''
  }
}

export const clearSecondaryNatureOfBusiness = () => {
  return {
    type: types.SET_CLEAR_SECONDARY
  }
}

//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
  return (dispatch) => {
    dispatch({
      type: types.CONTACTDETAILS_DROPDOWN_FOCUS,
      isFocus,
      field
    });
  };
};

//choose item from dropdown
export const selectDropdownItem = (value, description, field) => {
  return (dispatch, getstate) => {
    const primaryCountryOfOperation = getstate().contactDetailsReducer.primaryCountryOfOperation.value;

    if (field === "primaryCountryOfOperation" && primaryCountryOfOperation !== "") {
      const element = document.getElementById("editableDropdown-flagImg");
      element.style.display = "block";
    }

    if (field === "primaryNatureOfBusiness") {
      dispatch(clearSecondaryNatureOfBusiness());
    }

    dispatch({
      type: types.CONTACTDETAILS_DROPDOWN_ITEM_SELECT,
      value,
      field,
      description
    })
  };
};

export const changeSearchInputValue = (searchValue, field) => {
  return (dispatch) => {
    dispatch({
      type: types.CONTACTDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE,
      searchValue,
      field
    });
  };
};
export const setRetrieveContactDetailsData = (companyDetailsDataObj) => {
  const natureOfBusiness = companyDetailsDataObj && companyDetailsDataObj.basicProfile.natureOfBusiness ? companyDetailsDataObj.basicProfile.natureOfBusiness : '';
  let primaryNatureOfBusiness = '';
  let secondaryNatureOfBusiness = '';
  if (natureOfBusiness) {
    primaryNatureOfBusiness = natureOfBusiness.split('|')[0];
    secondaryNatureOfBusiness = natureOfBusiness.split('|')[1];
  }

  return {
    type: types.SET_RETRIEVE_CONTACT_DETAILS,
    primaryCountryOfOperation: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.primaryCountryOfOperation ? companyDetailsDataObj.basicProfile.primaryCountryOfOperation : '',
    primaryNatureOfBusiness: primaryNatureOfBusiness,
    secondaryNatureOfBusiness: secondaryNatureOfBusiness,
    primaryActivityDesc: companyDetailsDataObj && companyDetailsDataObj.basicProfile.primaryActivityDesc ? companyDetailsDataObj.basicProfile.primaryActivityDesc : '',
    secondaryActivityDesc: companyDetailsDataObj && companyDetailsDataObj.basicProfile.primaryActivityDesc ? companyDetailsDataObj.basicProfile.secondaryActivityDesc : '',
    countryOfIncorporation: companyDetailsDataObj && companyDetailsDataObj.basicProfile.countryOfIncorporation ? companyDetailsDataObj.basicProfile.countryOfIncorporation : ''
  }
}
