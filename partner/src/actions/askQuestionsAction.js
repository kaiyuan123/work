import * as types from "./actionTypes";

export const handleAskQuestionPopup = (popuptoggle, questionnaires, anwsers) => {
    let noOfQuestions= 0;
    let noOfButtonChecked = 0;
    for (let i = 0; i < questionnaires.length; i++) {
        if (questionnaires[i].subType.length > 0) {
            for(let j =0; j<questionnaires[i].subType.length; j++) {
                noOfQuestions ++;
                noOfButtonChecked ++;
                const subItem = anwsers.filter(e => e.id === questionnaires[i].subType[j].id)
                questionnaires[i].subType[j].value = subItem[0] && subItem[0].value;
            }
        } else {
            noOfQuestions ++;
            noOfButtonChecked ++;
            const item = anwsers.filter(e => e.id === questionnaires[i].id)
            questionnaires[i].value = item[0] && item[0].value;
        }

    }
    return {
        type: types.ASK_POPUP_SHOW,
        popuptoggle: popuptoggle,
        noOfQuestions: noOfQuestions,
        noOfButtonChecked: noOfButtonChecked,
        questionnaires: questionnaires
    }
}

export const handleHideAskQuestionPopup = (popuptoggle) => {
    return (dispatch, getState) => {
        let questionnaires = getState().askQuestionsReducer.questionnaires;

        for (let i = 0; i < questionnaires[0].length; i++) {
            if(questionnaires[0][i].subType.length > 0){
                for(let j =0;j<questionnaires[0][i].subType.length;j++){
                    questionnaires[0][i].subType[j].value = "";
                    questionnaires[0][i].subType[j].isValid = true;
                }
            }else{
                questionnaires[0][i].value = "";
                questionnaires[0][i].isValid = true;
            }

        }
        // debugger;
        dispatch({
            type: types.ASK_POPUP_HIDE_ON_CLOSE_BUTTON,
            popuptoggle: popuptoggle,
            noOfQuestions: 0,
            noOfButtonChecked:0,
            questionnaires: questionnaires
        })

    }
}

export const handlePopupHideOnOkButton = (popuptoggle) => {
    return {
        type: types.ASK_POPUP_HIDE_ON_OK_BUTTON,
        popuptoggle: popuptoggle,

    }
}

export const handleIsChecked = (values, lengthofButtonChecked, index, subindex) => {
    // debugger;
    return (dispatch, getState) => {
      let questionnaires = getState().askQuestionsReducer.questionnaires;
        // let noOfQuestions = getState().askQuestionsReducer.noOfQuestions;
        if(questionnaires[0][index].subType.length > 0){
            if(questionnaires[0][index].subType[subindex].value === "") {
                lengthofButtonChecked++;
            }
            questionnaires[0][index].subType[subindex].value = values;
            questionnaires[0][index].subType[subindex].isValid = true;

        }else{
            if(questionnaires[0][index].value === "") {
                lengthofButtonChecked++;
            }
            questionnaires[0][index].value = values;
            questionnaires[0][index].isValid = true;
        }
      dispatch({
        type: types.ASK_POPUP_QUESTION_VALUES,
          questionnaires,
            noOfButtonChecked: lengthofButtonChecked
      })

    };
}

// handle set error msg
export const handleErrorMessage = (field, index, subindex, errorMsg) => {
  return (dispatch, getState) => {
    let questionnaires = getState().askQuestionsReducer.questionnaires;
    if(field.id.indexOf('_') === 1 ){
        questionnaires[0][index].subType[subindex].isValid = false;
    }else{
      questionnaires[0][index].isValid = false;
    }

    dispatch({
      type: types.SET_ERROR_MESSAGE_QUESTIONS,
      questionnaires,
      errorMsg
    })
  }
}
