import * as types from './../actions/actionTypes';

const initialState = {
    legalId: {
        name: "legalId",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true
    },
    dateOfBirth: {
        name: "dateOfBirth",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true
    },
    countryOfBirth: {
        name: "countryOfBirth",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true
    },
    gender: {
        name: "gender",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true
    },
    maritalStatus: {
        name: 'maritalStatus',
        isValid: true,
        isFocus: false,
        value: 'S',
        errorMsg: '',
        isInitial: true,
        isMyInfo: false,
        description: ''
    },
    nationality: {
        name: 'nationality',
        isValid: true,
        isFocus: false,
        value: 'SG',
        errorMsg: '',
        isInitial: true,
        isMyInfo: false,
        description: ''
    },
    propertyType: {
        name: 'propertyType',
        isValid: true,
        isFocus: false,
        value: '',
        errorMsg: '',
        isInitial: true,
        isMyInfo: false,
        description: ''
    },
    residentialStatus: {
        name: "residentialStatus",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true
    },
    yearlyIncome: [],
    incomeObj: null,
    residentialCountry: {
        name: "residentialCountry",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true
    },
    passportNumber: {
        name: "passportNumber",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true,
        isMyInfo: false
    },
    passportExpiryDate: {
        name: "passportExpiryDate",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true,
        isMyInfo: false
    }
};

const personalIncomeDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_MYINFO_PERSONAL_INCOME_DETAILS:
            return {
                ...state,
                legalId: { ...state.legalId, value: action.legalId, isInitial: false },
                dateOfBirth: { ...state.dateOfBirth, value: action.dateOfBirth, isInitial: false },
                gender: { ...state.gender, value: action.gender, isInitial: false },
                maritalStatus: { ...state.maritalStatus, value: action.maritalStatus, isInitial: false },
                nationality: { ...state.nationality, value: action.nationality, isInitial: false },
                residentialStatus: { ...state.residentialStatus, value: action.residentialStatus, isInitial: false },
                residentialCountry: { ...state.residentialCountry, value: action.residentialCountry, isInitial: false },
                passportNumber: { ...state.passportNumber, value: action.passportNumber, isInitial: true, isValid: true, isMyInfo: action.passportNumberMyInfo },
                passportExpiryDate: { ...state.passportExpiryDate, value: action.passportExpiryDate, isInitial: true, isValid: true, isMyInfo: action.passportExpiryDateMyinfo },
                workPassStatus: { ...state.workPassStatus, value: action.workPassStatus, isInitial: false },
                workPassExpiryDate: { ...state.workPassExpiryDate, value: action.workPassExpiryDate, isInitial: false },
                propertyType: { ...state.propertyType, value: action.propertyType, isInitial: false }, countryOfBirth: { ...state.countryOfBirth, value: action.countryOfBirth, isInitial: false }
            };

        case types.SET_INCOME_DETAIL_DATA:
            return {
                ...state,
                yearlyIncome: action.yearlyIncome,
                incomeObj: action.incomeObj
            };

        case types.PERSONALDETAILS_DROPDOWN_FOCUS:
            return { ...state, [action.field]: { ...state[action.field], isFocus: action.isFocus } };

        case types.PERSONALDETAILS_DROPDOWN_ITEM_SELECT:
            return { ...state, [action.field]: { ...state[action.field], value: action.value, description: action.description, isValid: true, errorMsg: '', isInitial: false } };

        case types.PERSONALDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE:
            return { ...state, [action.field]: { ...state[action.field], searchValue: action.searchValue } };

        case types.PERSONALDETAILS_HANDLE_TEXT_INPUT_CHANGE:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    value: action.value,
                    isValid: action.isValid,
                    errorMsg: action.errorMsg,
                    isInitial: false
                }
            };

        case types.SET_ERROR_MESSAGE_INPUT:
            return {
                ...state,
                [action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
            };

        default:
            return state;
    }
}

export default personalIncomeDetailsReducer;
