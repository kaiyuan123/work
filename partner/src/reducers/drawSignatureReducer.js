import * as types from './../actions/actionTypes';

const initialState = {
  signaturePad: {
    trimmedDataURL: null,
    isDisabled: false
  }
};

const drawSignatureReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SIGNATURE_DATA:
      return {
        ...state,
        signaturePad : {
          ...state.signaturePad,
          trimmedDataURL: action.trimmedDataURL,
          isDisabled: true
        }
      }

    case types.CLEAR_SIGNATURE:
      return {
        ...state,
        signaturePad: {
          ...state.signaturePad,
          trimmedDataURL: null,
          isDisabled: false
        }
      }
    default:
      return state;
  }
}

export default drawSignatureReducer;
