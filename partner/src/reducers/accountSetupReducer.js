import * as types from './../actions/actionTypes';

const initialState = {
	accountName: {
		name: 'accountName',
		isValid: true,
		value: '',
		searchValue: '',
		errorMsg: '',
		isInitial: true,
		isMyInfo: false
	},
	currencyOfAccount: {
		name: 'currencyOfAccount',
		isValid: true,
		isFocus: false,
		value: 'sgd',
		errorMsg: '',
		isInitial: true,
		isMyInfo: false,
		description: ''
	},
	purposeOfAccount: [],
	purposeOfAccountIsValid: true,
	transactional: {
		isToggled: false,
		isValid: false
	},
	loanRepayment: {
		isToggled: false,
		isValid: false
	},
	investment: {
		isToggled: false,
		isValid: false
	},
	others: {
		isToggled: false,
		isValid: false
	},
	otherPurpose: {
		name: "otherPurpose",
		isValid: true,
		value: "",
		errorMsg: "",
		isInitial: true
	},
	chequebooks: {
		name: 'chequebooks',
		isValid: true,
		isFocus: false,
		value: '1',
		errorMsg: '',
		isInitial: true,
		isMyInfo: false,
		description: ''
	},
	registerOfPayNow: {
		isToggled: true,
		isValid: true,
		isDisabled: true
	},
	payNowUEN: {
		name: 'payNowUEN',
		isValid: true,
		value: '',
		errorMsg: '',
		isInitial: true
	},
	payNowID: {
		name: 'payNowID',
		isValid: true,
		value: '',
		errorMsg: '',
		isInitial: true
	},
	setupBIBPlus: {
		isToggled: true,
		isValid: true,
		isDisabled: true
	},

	BIBGroupId: {
		name: 'BIBGroupId',
		isValid: true,
		value: '',
		errorMsg: '',
		isInitial: true
	},
	BIBRoleType: {
		isToggled: true,
		isValid: true,
		isDisabled: true
	},
	BIBBulkService: {
		isToggled: true,
		isValid: true,
		isDisabled: true
	},
	BIBRemittanceMessage: {
		isToggled: true,
		isValid: true,
		isDisabled: true
	},
	BIBContactPerson: {
		name: 'BIBContactPerson',
		isValid: true,
		value: 'Edmund Tan Kah Kee',
		errorMsg: '',
		isInitial: true,
		isMyInfo: true
	},
	BIBMobileNumber: {
		name: "BIBMobileNumber",
		isValid: true,
		value: "",
		errorMsg: "",
		isMyInfo: false,
		isInitial: true
	},
	BIBEmailAddress: {
		name: "BIBEmailAddress",
		isValid: true,
		value: "",
		errorMsg: "",
		isInitial: true,
		isMyInfo: false
	},
	BIBTempValues: {
		BIBGroupId: '',
		BIBContactPerson: '',
		BIBMobileNumber: '',
		BIBEmailAddress: ''
	},
	readOnly: true
};

const companyBasicDetailsReducer = (state = initialState, action) => {
	switch (action.type) {
		case types.ACCOUNT_NAME_HANDLE_TEXT_INPUT_CHANGE:
			return {
				...state,
				[action.field]: {
					...state[action.field],
					value: action.value,
					isValid: action.isValid,
					errorMsg: action.errorMsg,
					isInitial: false
				},
				BIBTempValues: {
					...state['BIBTempValues'],
					[action.field]: action.isValid ? action.value : ''
				}
			};

		case types.ACCOUNT_SETUP_HANDLE_TOGGLE:
			if (action.field === "setupBIBPlus" && !action.isToggled) {
				return {
					...state,
					[action.field]: {
						...state[action.field],
						isToggled: action.isToggled,
						isValid: action.isValid
					},
					BIBGroupId: {
						name: 'BIBGroupId',
						isValid: true,
						value: '',
						errorMsg: '',
						isInitial: true
					},
					BIBRoleType: {
						isToggled: false,
						isValid: true,
						isDisabled: true
					},
					BIBBulkService: {
						isToggled: false,
						isValid: true,
						isDisabled: true
					},
					BIBRemittanceMessage: {
						isToggled: false,
						isValid: true,
						isDisabled: true
					}
				}
			}
			else if (action.field === "setupBIBPlus" && action.isToggled) {
				return {
					...state,
					[action.field]: {
						...state[action.field],
						isToggled: action.isToggled,
						isValid: action.isValid
					},
					BIBGroupId: {
						name: 'BIBGroupId',
						isValid: true,
						value: state['BIBTempValues'].BIBGroupId !== '' ? state['BIBTempValues'].BIBGroupId : '',
						errorMsg: '',
						isInitial: true
					},
					BIBRoleType: {
						isToggled: true,
						isValid: true,
						isDisabled: true
					},
					BIBBulkService: {
						isToggled: true,
						isValid: true,
						isDisabled: false
					},
					BIBRemittanceMessage: {
						isToggled: true,
						isValid: true,
						isDisabled: false
					}
				}
			}
			else if (action.field === "registerOfPayNow") {
				return {
					...state,
					[action.field]: {
						...state[action.field],
						isToggled: action.isToggled,
						isValid: action.isValid
					},
					payNowID: {
						name: 'payNowID',
						isValid: true,
						value: '',
						errorMsg: '',
						isInitial: true
					}
				}
			}
			return {
				...state,
				[action.field]: {
					...state[action.field],
					isToggled: action.isToggled,
					isValid: action.isValid
				},
				purposeOfAccount: action.purposeOfAccount
			};

		case types.PURPOSE_OF_ACCOUNT_HANDLE_TOGGLE:
			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: {
						...state[action.key][action.field],
						isToggled: action.isToggled,
						isValid: action.isValid
					}
				}

			}

		case types.PURPOSE_OF_ACCOUNT_HANDLE_TEXT_INPUT_CHANGE:
			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: {
						...state[action.key][action.field],
						value: action.value,
						isValid: action.isValid,
						errorMsg: action.errorMsg,
						isInitial: false
					}
				}
			};

		case types.ACCOUNT_SETUP_DROPDOWN_FOCUS:
			return { ...state, [action.field]: { ...state[action.field], isFocus: action.isFocus } };

		case types.MYINFO_SET_ACCOUNT_SETUP_DATA:
			return {
				...state,
				accountName: {
					name: 'accountName',
					isValid: true,
					value: action.entityName,
					searchValue: '',
					errorMsg: '',
					isInitial: true,
					isMyInfo: action.entityNameMyinfo
				},
				BIBContactPerson: {
					name: 'BIBContactPerson',
					isValid: true,
					value: action.principalName,
					errorMsg: '',
					isInitial: true,
					isMyInfo: action.contactMyinfo
				},
				BIBMobileNumber: {
					name: "BIBMobileNumber",
					isValid: true,
					value: action.mobileNumber,
					errorMsg: "",
					isInitial: true,
					isMyInfo: action.phoneMyinfo
				},
				BIBEmailAddress: {
					name: "BIBEmailAddress",
					isValid: true,
					value: action.emailAddress,
					errorMsg: "",
					isInitial: true,
					isMyInfo: action.emailMyinfo
				},
				payNowUEN: {
					name: 'payNowUEN',
					isValid: true,
					value: action.uen,
					errorMsg: '',
					isInitial: true
				},
				BIBTempValues: {
					BIBGroupId: '',
					BIBContactPerson: action.principalName,
					BIBMobileNumber: action.mobileNumber,
					BIBEmailAddress: action.emailAddress
				},
				chequebooks: { ...state.chequebooks, value: action.chequebooks },
				purposeOfAccount: action.purposeOfAccount,
				payNowID: { ...state.payNowID, value: action.entityNoSuffix },
				BIBGroupId: { ...state.BIBGroupId, value: action.BIBGroupId },
				transactional: { ...state.transactional, isToggled: action.transactional},
				loanRepayment: { ...state.loanRepayment, isToggled: action.loanRepayment },
				investment: { ...state.investment, isToggled: action.investment},
				others: { ...state.others, isToggled: action.others },
				otherPurpose: { ...state.otherPurpose, value: action.otherPurpose},
				registerOfPayNow: { ...state.registerOfPayNow, isToggled: action.registerOfPayNow },
				setupBIBPlus: { ...state.setupBIBPlus, isToggled: action.setupBIBPlus },
				BIBBulkService: { ...state.BIBBulkService, isToggled: action.BIBBulkService },
				BIBRemittanceMessage: { ...state.BIBRemittanceMessage, isToggled: action.BIBRemittanceMessage },

			};

		case types.ACCOUNT_SETUP_DROPDOWN_SEARCH_INPUT_CHANGE:
			return { ...state, [action.field]: { ...state[action.field], searchValue: action.searchValue } };

		case types.ACCOUNT_SETUP_DROPDOWN_ITEM_SELECT:
			return {
				...state,
				[action.field]: {
					...state[action.field],
					value: action.value,
					description: action.description,
					isValid: true,
					errorMsg: '',
					isInitial: false
				}
			};

		case types.SET_ERROR_MESSAGE_INPUT:
			return {
				...state,
				[action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
			};

		case types.ACCOUNT_SETUP_PURPOSE_OF_ACCOUNT_IS_VALID:
			return {
				...state,
				purposeOfAccountIsValid: action.status,
			};

		default:
			return state;
	}
};

export default companyBasicDetailsReducer;
