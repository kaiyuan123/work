import * as types from './../actions/actionTypes';

const initialState = {
	signature: {
		image:'',
		isValid: true,
		errorMsg: ''
	},
	inputProgressValue: 0,
	signaturePad: {
		trimmedDataURL: null
	}
};

const signatureReducer = (state = initialState, action) => {
	switch (action.type) {
		case types.SIGNATURE_ERROR_SHOW:
			return {
				...state,
				inputProgressValue: 0,
				signature: {
					...state['signature'],
					isValid: false,
					errorMsg: action.errorMsg
				},
				signaturePad: {
					trimmedDataURL: null
				}
			};
		case types.SIGNATURE_ERROR_REMOVE:
			return {
				...state,
				inputProgressValue: 0,
				signature: {
					...state['signature'],
					isValid: true,
					errorMsg: ''
				},
				signaturePad: {
					trimmedDataURL: null
				}
			};
		case types.SIGNATURE_SAVE_DATA:
			return {
				...state,
				inputProgressValue: 100,
				signature: {
					...state['signature'],
					isValid: true,
					errorMsg: ''
				},
				signaturePad: {
					trimmedDataURL: action.trimmedDataURL
				}
			};
		case types.CHANGE_PROGRESS_VALUE:
			return {
				...state,
				inputProgressValue: 0,
				signaturePad: {
					trimmedDataURL: null
				}
			};

		default:
			return state;
	}
};

export default signatureReducer;
