import * as types from './../actions/actionTypes';

const initialState = {
  retrieveError: false,
  serviceDown: false,
  dataLoaded: false,
  isLoading: true,
  myInfoData: null,
  initiateMyinfoData: null,
  errorCode: "",
  startingParameter: "",
  isKeyManApply: null,
  isWhichType: "",
  isKnockOut: false,
  isKnockOutField: {
    entitySubType: false,
    countryOfIncorporation: false,
    natureOfBusiness: false,
    ownership: false
  },  
  sessionExpirePopup: false
};

const applicationReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_APPLICATION_DATA_LOADED:
      return {
        ...state,
        dataLoaded: true,
        isLoading: false
      };

    case types.SET_MYINFO_RESPONSES:
      return {
        ...state,
        myInfoData: action.response
      }

    case types.STORE_PARAMETER:
      return {
        ...state,
        startingParameter: action.parameter
      }

    case types.SET_INITIATE_MYINFO_RESPONSES:
      return {
        ...state,
        initiateMyinfoData: action.response
      }

    case types.SET_ACCOUNT_HOLDER_TYPE:
      return {
        ...state,
        isKeyManApply: action.isKeyManApply
      }

    case types.WHICH_TYPE:
      return {
        ...state,
        isWhichType: action.status
      }

    case types.IS_KNOCKOUT_SCENARIO:
      return {
        ...state,
        isKnockOut: action.isKnockOut
      }

    case types.SET_WHICH_KNOCKOUT_SCENARIO_FAIL:
      return {
        ...state,
        isKnockOutField: {
          ...state.isKnockOutField,
          [action.fleid]: true
        }
      }
    case types.SET_ERROR_MESSAGE_WITH_ERRORCODE:
      return {
        ...state,
        errorCode: {
          ...state.errorCode,
          value: action.errorCode
        },
        hasError: true,
        dataLoaded: false
      }

    case types.SET_ERROR_MESSAGE_WITH_DESCRIPTION:
      return {
        ...state,
        errorCode: {
          ...state.errorCode,
          description: action.description
        },
        hasError: true,
        dataLoaded: false
      }

    case types.SET_SESSIONEXPIRE_POPUP:
      return {
         ...state,
        sessionExpirePopup: action.showPopup
      }

    default:
      return state;
  }
}

export default applicationReducer;
