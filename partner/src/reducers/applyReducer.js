import * as types from './../actions/actionTypes';

const initialState = {
  retrieveError: false,
  serviceDown: false,
  dataLoaded: true,
  productCode: '',
  isCommerical: false,
  requestLoan: '',
  annualTurnover: '',
  entityName: '',
  uen: '',
  refNo: '',
  applicationId: '',
  expiryDate: '',
  status: ''
};

const applyReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_APPLY_APPLICATION_DATA:
      return {
        ...state,
        retrieveError: false,
        serviceDown: false,
        dataLoaded: true,
        applicationId: action.applicationId,
        refNo: action.refNo,
        entityName: action.entityName,
        productCode: action.productCode,
        expiryDate: action.expiryDate,
        status: action.status
      };

    default:
      return state;
  }
}

export default applyReducer;
