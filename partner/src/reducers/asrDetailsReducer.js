import * as types from './../actions/actionTypes';

const initialState = {
	applicantResolution: {
		name: "applicantResolution",
		isValid: true,
		isFocus: false,
		value: '',
		errorMsg: '',
		isInitial: true,
		description: ''
	},
	signingApprovedPerson: {
		name: "signingApprovedPerson",
		isValid: true,
		isFocus: false,
		value: '',
		errorMsg: '',
		isInitial: true,
		description: ''
	},
	particularsOfApprovedPersons: {
		name: 'particularsOfApprovedPersons',
		openRows: [],
		value: []
	},
    particularsOfCertifyingPersons: {
        name: 'particularsOfCertifyingPersons',
        openRows: [],
        value: []
    },
    applicantName : {
    	name: "applicantName",
	    isValid: true,
	    value: "",
	    errorMsg: "",
	    isInitial: true
    },
    resolutionDate : {
    	name: "resolutionDate",
	    isValid: true,
	    value: "",
	    errorMsg: "",
	    isInitial: true
    },
    entityType: {
	    name: "entityType",
	    isValid: true,
	    value: "",
	    errorMsg: "",
	    isInitial: true
		},
		businessConstitution: {
	    name: "businessConstitution",
	    isValid: true,
	    value: "",
	    searchValue: '',
	    errorMsg: '',
	    isInitial: true,
	    isFocus: false
	}
};

const asrDetailsReducer = (state = initialState, action) => {
	switch (action.type) {
		case types.MYINFO_SET_ASR_APPROVED_PERSON_DATA:
          return {
              ...state,
              particularsOfApprovedPersons: { ...state.particularsOfApprovedPersons, value: action.particularsOfApprovedPersons },
              applicantName: { ...state.applicantName, value: action.applicantName, isInitial: false },
							resolutionDate: { ...state.resolutionDate, value: action.resolutionDate, isInitial: false },
          };

    case types.SET_ENTITY_DATA:
    	return {
      	...state,
      	entityType: { ...state.entityType, value: action.entityType, isInitial: false },
				businessConstitution: { ...state.businessConstitution, value: action.businessConstitution, isInitial: false }
    	}

    case types.SIGNING_CONDITIONS_RADIO_BUTTON:
    	return {
      	...state,
      	[action.field]: {...state[action.field], value: action.value, errorMsg:"", isValid: true}
    	}

    case types.SET_ERROR_MESSAGE_INPUT:
        return {
          	...state,
        	[action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
      	};
		default:
			return state;
	}
}

export default asrDetailsReducer;
