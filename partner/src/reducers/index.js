import { combineReducers } from 'redux';
import commonReducer from './commonReducer';
import localAddressInputReducer from './localAddressInputReducer';
import contactDetailsReducer from './contactDetailsReducer';
import accountSetupReducer from './accountSetupReducer';
import personalIncomeDetailsReducer from './personalIncomeDetailsReducer';
import companyBasicDetailsReducer from './companyBasicDetailsReducer';
import companyOverallDetailsReducer from './companyOverallDetailsReducer';
import askQuestionsReducer from './askQuestionsReducer';
import operatingMandateReducer from './operatingMandateReducer';
import asrDetailsReducer from './asrDetailsReducer';
import shareHoldersReducer from './shareHoldersReducer';
import retrievingReducer from './retrievingReducer';
import applicationReducer from './applicationReducer';
import confirmTandCReducer from './confirmTandCReducer';
import signatureReducer from './signatureReducer';
import drawSignatureReducer from './drawSignatureReducer';
import taxSelfDeclarationsReducer from './taxSelfDeclarationsReducer';
import uploadDocumentsReducer from './uploadDocumentsReducer';
import applyReducer from './applyReducer';
import verifyReducer from './verifyReducer';

export default combineReducers({
    commonReducer,
    localAddressInputReducer,
    contactDetailsReducer,
    personalIncomeDetailsReducer,
    companyBasicDetailsReducer,
    companyOverallDetailsReducer,
    accountSetupReducer,
    askQuestionsReducer,
    operatingMandateReducer,
    asrDetailsReducer,
    shareHoldersReducer,
    retrievingReducer,
    applicationReducer,
    confirmTandCReducer,
    signatureReducer,
    drawSignatureReducer,
    taxSelfDeclarationsReducer,
		uploadDocumentsReducer,
    applyReducer,
    verifyReducer
})
