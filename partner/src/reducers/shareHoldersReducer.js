import * as types from './../actions/actionTypes';

const initialState = {
	shareHoldersList: {
        name: 'shareHoldersList',
        openRows: [],
        value: []
    },
	shareholderID: [0],
	shareholder0: {
	    shareholderName: {
	      name: "shareholderName",
	      isValid: true,
	      value: "",
	      errorMsg: "",
	      isInitial: true,
	      isReadOnly: false,
	      isMyInfo: false
	    },
	    shareholderNRIC: {
	      name: "shareholderNRIC",
	      isValid: true,
	      value: "",
	      errorMsg: "",
	      isInitial: true,
	      isReadOnly: false,
	      isMyInfo: false
	    },
	    shareholderEmail: {
	      name: "shareholderEmail",
	      isValid: true,
	      value: "",
	      errorMsg: "",
	      isInitial: true,
	      isReadOnly: false,
	      isMyInfo: false
	    },
	    shareholderMobileNo: {
	      name: "shareholderMobileNo",
	      isValid: true,
	      value: "",
	      errorMsg: "",
	      isInitial: true,
	      isReadOnly: false,
	      isMyInfo: false
	    }
	},
}

const shareHoldersReducer = (state = initialState, action) => {
	switch (action.type) {

		case types.APPROVED_SHAREHOLDERS_LIST:
	      return {
	        ...state,
	        shareHoldersList: {...state.shareHoldersList, value: action.shareHoldersList}
	    }

	    case types.POPULATE_SHAREHOLDERS_LIST:
	      return {
	        ...state,
	        [action.key]: {
	          ...state[action.key],
	          shareholderName: {
	            ...state[action.key]["shareholderName"],
	            value: action.shareholderName,
	            isInitial: true,
	            isValid: true,
	            isReadOnly: true
	          },
	          shareholderNRIC: {
	            ...state[action.key]["shareholderNRIC"],
	            value: action.shareholderNRIC,
	            isInitial: true,
	            isValid: true,
	            isReadOnly: true
	          },
	          shareholderEmail: {
	            ...state[action.key]["shareholderEmail"],
	            value: action.shareholderEmail,
	            isInitial: true,
	            isValid: true,
	            isReadOnly: true,
	            isMyInfo: action.emailMyinfo
	          },
	          shareholderMobileNo: {
	            ...state[action.key]["shareholderMobileNo"],
	            value: action.shareholderMobileNo,
	            isInitial: true,
	            isValid: true,
	            isReadOnly: true,
	            isMyInfo: action.phoneMyinfo
	          }
	          
	        }
	    };

	    case types.POPULATE_ADDITIONAL_SHAREHOLDERS_LIST:

	      return {
	        ...state,
	        [action.shareholder]: {
	          shareholderName: {
	            name: "shareholderName",
	            isValid: true,
	            value: action.shareholderName,
	            errorMsg: "",
	            isInitial: true,
	            isReadOnly: true
	          },

	          shareholderNRIC: {
	            name: "shareholderNRIC",
	            isValid: true,
	            value: action.shareholderNRIC,
	            errorMsg: "",
	            isInitial: true,
	            isReadOnly: true
	          },

	          shareholderEmail: {
	            name: "shareholderEmail",
	            isValid: true,
	            value: action.shareholderEmail,
	            errorMsg: "",
	            isInitial: true,
	            isReadOnly: true,
	            isMyInfo: action.emailMyinfo
	          },

	          shareholderMobileNo: {
	            name: "shareholderMobileNo",
	            isValid: true,
	            value: action.shareholderMobileNo,
	            errorMsg: "",
	            isInitial: true,
	            isReadOnly: true,
	            isMyInfo: action.phoneMyinfo
	          }
	        }
	    };

	    case types.INCREMENT_SHAREHOLDERS_COUNT:
	      return {
	        ...state,
	        shareholderID: action.shareholderID
	    };

	    case types.SHAREHOLDERS_HANDLE_TEXT_INPUT_CHANGE:
	      return {
		        ...state,
		        [action.key]: {
		          ...state[action.key],
		          [action.field]: {
		            ...state[action.key][action.field],
		            value: action.value,
		            isValid: action.isValid,
		            errorMsg: action.errorMsg,
		            isInitial: false
		          }
		        }
		    };

		case types.SET_SHAREHOLDER_ERROR_MESSAGE_INPUT:
	      return {
	        ...state,
	        [action.key]: {
	          ...state[action.key],
	          [action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
	        }
	    };


		default:
	      return state;
	}
}

export default shareHoldersReducer;