/* global GLOBAL_CONFIG */
export const CONFIG = GLOBAL_CONFIG;
export const PAGE = {INIT_PAGE: "apply"};
export const APPLICANT_TYPE = {AS_AP: "AS", SHARE_HOLDER: "SH", CONTROLLING_PERSON: "CP"};

export const BASIC_DETAIL = {
    IS_SHOW_WITH_TYPE: ["AS", "SH", "CP"]
};
export const PERSONAL_DETAIL = {
    IS_SHOW_WITH_TYPE: ["AS", "SH", "CP"]
};
export const COMPANY_DETAIL = {
    IS_SHOW_WITH_TYPE: ["AS"]
};
export const ACCOUNT_SET_UP = {
    IS_SHOW_WITH_TYPE: ["AS"]
};
export const OPERATING_MANDATE = {
    IS_SHOW_WITH_TYPE: ["AS"]
};
export const SHARE_HOLDER = {
    IS_SHOW_WITH_TYPE: ["AS"]
};
export const FATCA_CRS = {
    IS_SHOW_WITH_TYPE: ["AS"]
};
export const ASR = {
    IS_SHOW_WITH_TYPE: ["AS"]
};
export const TNC = {
    IS_SHOW_WITH_TYPE: ["AS", "SH", "CP"]
};
export const SIGNATURE_UPLOAD = {
    IS_SHOW_WITH_TYPE: ["AS"]
};

export const ENTITY_TYPE = {
    LIMITED_BY_SHARE: "LC",
    LIMITED_PARTNERSHIP: "LP",
    LTD_PARTNERSHIP: "LL",
    SOLE_AND_PARTNERSHIP: "BN"
}