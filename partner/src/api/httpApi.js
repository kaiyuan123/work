/* global GLOBAL_CONFIG */
import axios from 'axios';

const getFormConfigURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.formConfig : `${GLOBAL_CONFIG.SPEC_API_PATH}`;
const getInitiateMyinfoURL = GLOBAL_CONFIG.initiateMyinfoDataJson;
const retrieveLoanWithMyInfoURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.myinfoDataJson : `${GLOBAL_CONFIG.API_PATH}/applications/application`;
const getAddressByPostalCodeURL = `${GLOBAL_CONFIG.COMMON_API_PATH}/referencedata/address`;
const commonAPIURL = `${GLOBAL_CONFIG.API_PATH}/applications`;
const callbackURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.myinfoDataJson : `${GLOBAL_CONFIG.CALLBACK_API_PATH}`
const verifyEmailURL = `${GLOBAL_CONFIG.API_PATH}/applications/verify`;

const LocalEnvironment = GLOBAL_CONFIG.Local_Environment;
const Force_URL_redirect = GLOBAL_CONFIG.Force_URL_redirect;
const root_path = GLOBAL_CONFIG.root_path;
const timeout = GLOBAL_CONFIG.timeout;
const httpapi = axios.create();
httpapi.defaults.timeout = timeout;

const Application = {
	submitInitiateApplication: (dataObj, parameter) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/application${parameter}`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	submitPartialApplication: (dataObj,applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	submitApplication: (dataObj,applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}/submit`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	saveUploadDocument: (dataObj,applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}/upload`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},

	askQuestionsSubmission: (dataObj,applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}/question`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	myInfoCallback: (parameter) => {
		return new Promise((resolve, reject) => {
			httpapi
				.get(`${callbackURL}${parameter}`)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	}
};

const Config = {
	getInitialData: () => {
		return new Promise((resolve) => {
			axios.get(getFormConfigURL).then((response) => {
				resolve(response.data);
			});
		});
	},
	getMyInfoData: () => {
		return new Promise((resolve, reject) => {
			axios.get(retrieveLoanWithMyInfoURL).then((response) => {
				resolve(response.data);
			}).catch((e) => reject(e));
		});
	},
	getInitiateMyinfoData: () => {
		return new Promise((resolve, reject) => {
			axios.get(getInitiateMyinfoURL).then((response) => {
				resolve(response.data);
			}).catch((e) => reject(e));
		});
	},
	verifyEmail: (code, mode) => {
		return new Promise((resolve, reject) => {
		  httpapi.post(`${verifyEmailURL}?code=${code}&mode=${mode}`, {}, { headers: { 'Content-Type': 'application/json;charset=UTF-8' } }).then((response) => {
			resolve(response);
		  }).catch((e) => reject(e));
		});
	  }
}

const Address = {
	getAddressByCode: (postCode) => {
		return new Promise((resolve, reject) => {
			axios.get(`${getAddressByPostalCodeURL}?postalcode=${postCode}`).then((response) => {
				resolve(response);
			}).catch((e) => reject(e));
		})
	}
};

export {
	Application,
	Config,
	Address,
	LocalEnvironment,
	Force_URL_redirect,
	root_path
}
