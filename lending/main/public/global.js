// eslint-disable-next-line no-unused-vars
const GLOBAL_CONFIG = {
    API_PATH: "/businessbanking/api/v1",
    COMMON_API_PATH: "/api/sg/v1",
    MYINFO_URL: "/dummysingpass.html?redirect_uri=http://pibtsg95:6507/camelcar/callback&client_id=stg_uob_one_casacc&purpose=opening an account online with UOB&attributes=aliasname,assessableincome,birthcountry,dob,edulevel,email,employment,hanyupinyinaliasname,hanyupinyinname,hdbtype,housingtype,marriedname,marital,mobileno,name,nationality,regadd,sex&state=",
    formConfig: "./lending.json",
    myinfoDataJson: "./myinfoData.json",
    initateMyInfo: "./initateMyInfo.json",
    timeout: "45000",
    Local_Environment: true,
    root_path_business: "/businessbanking/loans/",
    root_path_commerical: "/commericalbanking/loans/"
};
