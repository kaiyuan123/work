import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';

import './PrimaryButton.css';

import loadingImg from './../../assets/images/loading_icon.svg';

const PrimaryButton = props => {
  const { commonReducer, isLoading, onClick, label, style, containerStyle, spinnerStyle, disabled } = props;
  const toggleNavCSS = commonReducer.toggleNav ? "showNav": "hideNav";
  const spinner = isLoading
    ? <img
          className='primary-button--spinner'
          style={spinnerStyle ? spinnerStyle : null}
          src={loadingImg}
          alt='primary-button'
      />
    : false;
  const isDisabled = disabled || isLoading ? true : false;
  return (
    <div style={containerStyle ? containerStyle : null}>
        <button className={`primary-button ${toggleNavCSS}`} onClick={onClick} disabled={isDisabled} style={style ? style : null}>
            {spinner} {isLoading ? '' : label}
        </button>
    </div>
  );
};

PrimaryButton.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    style: PropTypes.object,
    containerStyle: PropTypes.object
};

PrimaryButton.defaultProps = {
  isLoading: false
};

const mapStateToProps = state => {
	const { commonReducer } = state;
	return { commonReducer };
};

export default connect(mapStateToProps)(PrimaryButton);
