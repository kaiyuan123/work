import React from 'react';
import PropTypes from 'prop-types';
import { AsYouType } from 'libphonenumber-js';
import { validators } from './../../../common/validations';
import './PhoneInput.css';

// import tickImg from './../../../assets/images/Tick.svg';
// import crossImg from './../../../assets/images/cross.svg';
// import nonEditableImg from './../../../assets/images/non-editable-icon4.svg';
// const getVisibility = isValid => (isValid ? "" : "--invisible");
import pencilIcon from "./../../../assets/images/pencil-icon.svg";
const getReadOnlyClass = isReadOnlyMobile => (isReadOnlyMobile ? "PhoneInputContainer--readOnly" : "");
const getFocus = (isFocus, inputValue, isValid, onlySg) => isFocus || inputValue.length > 0 || !isValid || (onlySg && (isFocus || inputValue.length > 0 || !isValid)) ? "--focused" : "";

const Icon = props => {
  const visibility = props.isValid ? "--visible" : "--invisible";
  const tick = (
    <img className={`phoneInputTick${visibility}`} src={pencilIcon} alt="pencil" />
  );

  return props.isValid ? tick : "";
};


const PhoneInput = props => {
  const {
    inputID,
    isFocus,
    isValid,
    isReadOnlyMobile,
    errorMsg,
    label,
    value,
    onFocus,
    onBlur,
    onChange,
    style,
    validator,
    isUpperCase,
    isDisabled,
    onlySg,
    errorMsgList,
    hasIcon,
		isMyInfo,
    hasEdit
  } = props;

  const focus = getFocus(isFocus, value, isValid, onlySg);
	const focusText = isFocus ? "phoneInputContainer--onFocus" : "";
  // const visibility = getVisibility(isValid);
  const showErrorLabel = isValid ? "" : "phoneInputLabel--error";
  const readOnlyClass = getReadOnlyClass(isReadOnlyMobile);
  const errorContainerStyle = isValid ? "" : "PhoneInputContainer--error";
  const isReadOnly65 = isReadOnlyMobile || isDisabled ? "isReadOnly" : '';
  // const tick = (
  //   value.length > 3 ?
  //   <img className={`phoneInputTick${visibility}`} src={tickImg} alt='tick' />
  //   :
  //   ""
  // );
  // const cross = <img className={"phoneInputTick"} src={crossImg} alt='tick' />;
  // const validPic = isValid ? tick : cross;

  //const formatPhoneNo = no => new asYouType().input(no);

  const country = number => {
    const formatter = new AsYouType();
    formatter.input(number);
    return formatter.country;
  };
  const formatNumber = number => {
    const formatter = new AsYouType();
    return formatter.input(number);
  };

  const SGformatNumber = number => {
    const formatter = new AsYouType('SG');
    return formatter.input(number);
  };

  const contryName = onlySg ? "SG" : country(value);
  let countryFlagPath = contryName ? `./images/countriesFlags/${contryName.toLowerCase()}.svg` : null;

  const handleOnChange = e => {
    const inputValue = isUpperCase ? e.target.value.toUpperCase() : e.target.value;
    let isValid = true;
    let errorMsg = '';

    if (validator && validator.length > 0) {
      let result = validators(validator, inputValue);
      isValid = result.status;
      errorMsg = result.errorMsg !== '' ? errorMsgList[result.errorMsg].replace("{number}", result.number).replace("{secondnumber}", result.secondnumber) : '';
    }

    const data = {
      value: inputValue,
      isValid,
      errorMsg,
      isInitial: false
    };
    onChange(data);
  };

  return (
    <div className='PhoneInputWrap' id={inputID}>
      <div className={`PhoneInputContainer ${readOnlyClass} ${errorContainerStyle} ${focusText}`}>
        <label className={`phoneInputLabel${focus} ${showErrorLabel}`}>
          {`${label}${isValid ? "" : ` ${errorMsg}`}`}
        </label>
        <div className={`phoneInputField`}>
          {countryFlagPath && (isFocus || value.length > 0 || !isValid) && <img className={"phoneInputFlag"} src={countryFlagPath} alt='flag' />}
          {onlySg && (isFocus || value.length > 0 || !isValid) && <div className={`phoneInputFlag--number ${isReadOnly65}`} >+65</div>}
          <input
            onFocus={onFocus ? onFocus : null}
            onBlur={onBlur ? onBlur : null}
            disabled={isDisabled || isReadOnlyMobile}
            value={onlySg ? SGformatNumber(value) : formatNumber(value)}
            onChange={handleOnChange}
            type='tel'
            className={`phoneInput${focus}`}
            style={style ? style : null}
          />
          {hasIcon && value && !isDisabled && isMyInfo && !hasEdit && <Icon isValid={isValid} />}
        </div>
      </div>
      <div className="bottomLine" />
      <div className="fullErrorContent" />
    </div>
  );
};

PhoneInput.propTypes = {
  isFocus: PropTypes.bool,
  isValid: PropTypes.bool,
  isReadOnlyMobile: PropTypes.bool,
  isDisabled: PropTypes.bool,
  errorMsg: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.object
};

PhoneInput.defaultProps = {
  isFocus: false,
  isValid: true,
  isReadOnlyMobile: false,
  errorMsg: '',
  label: 'Label',
  value: '',
  isUpperCase: false,
  isDisabled: false,
  hasIcon: true,
  onlySg: true
};

export default PhoneInput;
