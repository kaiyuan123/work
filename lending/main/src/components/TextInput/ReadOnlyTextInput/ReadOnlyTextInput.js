import React from "react";
import PropTypes from "prop-types";

import "./ReadOnlyTextInput.css";
import nonEditableImg from "./../../../assets/images/non-editable-icon4.svg";

const ReadOnlyTextInput = props => {
  const { label, value, hasIcon } = props;
  return (
    <div className="read-only-text-input--container">
      <div>
        <label className={`read-only-text-input--label--focused`}>
          {label}
        </label>
        <div className={`read-only-text-input--input--focused`}>{value}</div>
      </div>
      {hasIcon && (
        <img
          className={"read-only-text-input--tick--visible"}
          src={nonEditableImg}
          alt="tick"
        />
      )}
    </div>
  );
};

ReadOnlyTextInput.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  hasIcon: PropTypes.bool
};

ReadOnlyTextInput.defaultProps = {
  label: "Label",
  value: "",
  hasIcon: true
};

export default ReadOnlyTextInput;
