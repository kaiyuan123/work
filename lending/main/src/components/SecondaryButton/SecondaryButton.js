import React from 'react';

import './SecondaryButton.css';


const SecondaryButton = props => {
  const { label, icon, onClick } = props;

  return (
    <div className='uob-secondary-button-container'>
      <span className="uob-secondary-button" onClick={() => onClick ? onClick() : null}>
        {icon && <img src={icon} alt='icon' className='uob-secondary-button-icon'/>}
        {label}
      </span>
    </div>
  );
}

export default SecondaryButton;
