import React from 'react';

import './CpfTableDesktop.css';

const generateTableRows = tableContent => {
    return tableContent.map((row, i) => {
        const rowData = Object.keys(row);
        return (
            <div key={i}>
                <div className='cpf-table--separator' />
                <div className={"cpf-table--row-flex"}>
                    {rowData.map((data, index) => <div key={index} className='cpf-table--row-item'>{row[data]}</div>)}
                </div>
            </div>
        );
    });
};

const CpfTableDesktop = props => {
    return (
        <div className='cpf-table--main-container-flex'>
            <div className='cpf-table-header'>{props.label}</div>
            <div className='cpf-table--titles-container-flex'>
                {props.colTitles.map((title, i) => <div key={i} className='cpf-table--title-flex'>{title}</div>)}
            </div>
            <div className='cpf-table--rows-container-flex'>
                {generateTableRows(props.tableContent, props)}
            </div>
        </div>
    );
};



export default CpfTableDesktop;
