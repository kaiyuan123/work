import React from 'react';
import PropTypes from 'prop-types';

import "./CpfTableMobile.css";

import arrowImg from './../../../assets/images/arrow.svg';

const Row = props => {
    const {
        handleClick,
        arrowClass,
        header,
        data,
        contentClass,
        bodyTitle,
        restOfDataKeysExceptHeader,
        isRowOpen
    } = props;
    return (
        <div className={"cpf-table-mobile-container-item"}>
            <div className='cpf-table-mobile-title-container' onClick={handleClick}>
                <img src={arrowImg} alt='tick' className={arrowClass} />
                <div className='cpf-table-mobile-title-checkbox-container'>
                    <div className='cpf-table-mobile-title'>{`${data[header]}`}</div>
                </div>
            </div>
            <div className={`cpf-table-mobile-content-container ${contentClass}`}>
                {isRowOpen &&
                    bodyTitle.map((itemTitle, i) => {
                        const currentTitleKey = restOfDataKeysExceptHeader[i];
                        return <RowItem key={i} title={itemTitle} data={data[currentTitleKey]} />;
                    })}
            </div>
        </div>
    );
};

const RowItem = props => {
    return (
        <div className='cpf-table-mobile-content-item-container'>
            <div className='cpf-table-mobile-content-item-title'>{props.title}</div>
            <div className='cpf-table-mobile-content-item-data'>
                {props.data}
            </div>
        </div>
    );
};

const CpfTableMobile = props => {
    const { colTitles, tableContent, openRows, expandTableRow, collapseTableRow} = props;
    const bodyTitle = colTitles.filter((title, i) => i !== 0);

    return (
        <div className='cpf-table-mobile-container'>
          <div className='cpf-table-mobile-header'>{props.label}</div>
            {tableContent.map((data, i) => {
                const dataKeys = Object.keys(data);
                const header = dataKeys[0];
                const isRowOpen = openRows.includes(i) ? true : false;
                const restOfDataKeysExceptHeader = dataKeys.filter(x => x !== header);
                const arrowClass = isRowOpen ? "cpf-table-mobile-arrow--selected" : "cpf-table-mobile-arrow";
                const contentClass = isRowOpen ? "cpf-table-mobile-content-container--selected" : "";
                const handleClick = () =>
                    isRowOpen
                        ? collapseTableRow && collapseTableRow(i)
                        : expandTableRow && expandTableRow(i);
                return (
                    <Row
                        key={i}
                        restOfDataKeysExceptHeader={restOfDataKeysExceptHeader}
                        arrowClass={arrowClass}
                        contentClass={contentClass}
                        handleClick={handleClick}
                        bodyTitle={bodyTitle}
                        data={data}
                        header={header}
                        index={i}
                        isRowOpen={isRowOpen}
                        pid={props.pid}
                    />
                );
            })}
        </div>
    );
};

Row.propTypes = {
    handleClick: PropTypes.func.isRequired,
    arrowClass: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired,
    data: PropTypes.object.isRequired,
    contentClass: PropTypes.string.isRequired,
    bodyTitle: PropTypes.array.isRequired,
    restOfDataKeysExceptHeader: PropTypes.array.isRequired,
    isRowOpen: PropTypes.bool.isRequired
};

RowItem.propTypes = {
    title: PropTypes.string.isRequired,
    data: PropTypes.string.isRequired
};

CpfTableMobile.propTypes = {
    colTitles: PropTypes.array.isRequired,
    tableContent: PropTypes.array.isRequired,
    pid: PropTypes.string
};

export default CpfTableMobile;
