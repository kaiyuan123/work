import React from 'react';
import PropTypes from 'prop-types';

import './RadioOption.css';

import normalCarImg from './../../assets/images/carloan/car-normal.svg';

const RadioOption = props => {
  const { isReadOnly, isSelected, option, onClick, label } = props;
  const selectedClass = isSelected ? 'radio-option-item--selected' : '';

  return (
    <div className='radio-option-container'>
      <div className='radio-option-row'>
          <div onClick={() => !isReadOnly && onClick && onClick(option.packageId, option.interestRate)} className={`radio-option-item ${selectedClass}`}>
            <div className='radio-option-middle'>
              <img src={normalCarImg} alt='Car' className='radio-button-car-icon' />
              <label className='radio-button-title'>{option.description}</label>
              <label className='radio-button-subtitle'>{`${option.interestRate}% ${label}`}</label>
            </div>
          </div>
      </div>
    </div>
  );

};

RadioOption.propTypes = {
  isReadOnly: PropTypes.bool,
  isSelected: PropTypes.bool,
  option: PropTypes.object.isRequired
};

RadioOption.defaultProps = {
  isReadOnly: false
};

export default RadioOption;
