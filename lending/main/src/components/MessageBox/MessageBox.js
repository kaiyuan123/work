import React from 'react';
import './MessageBox.css';

const MessageBox = props => {
  const { img, title, subtitle, description, isThankyou } = props;
  const fromThankYou = isThankyou ? "thankyou" : 'messageBox';
  return (
    <div className={`${fromThankYou}-container`}>
      <div className={`${fromThankYou}-content`}>
        <style dangerouslySetInnerHTML={{
          __html: `
      		body { padding-left: 0 }
      		.uob-content {padding: 0 35px; }
      		#sideNav { display: none }
      		.logoRight { display: block!important }
      		#home .title2 { display: none }
      		section.resume-section { padding: 32px 35px 0!important;}
      		`
        }} />
        {img &&
          <img
            className={`${fromThankYou}-imageAlignment`}
            src={img}
            alt='icon'
          />
        }
        <div className={`${fromThankYou}-title`}>{title}</div>
        <div className={`${fromThankYou}-subtitle`} dangerouslySetInnerHTML={{ __html: subtitle }} />
        <div className={`${fromThankYou}-information-container`}>
          <div className={`${fromThankYou}-description`} dangerouslySetInnerHTML={{ __html: description }} />
        </div>
      </div>
    </div>
  );
}

export default MessageBox;
