import React, { Component } from 'react';

import "./CompatibilityErrorMessage.css";

import crossImg from './../../assets/images/cross-grey.svg';

class CompatibilityErrorMessage extends Component {
  constructor(props) {
    super(props);
    this.timer = 0;
    this.fadeTimer = 0;
    this.state = {
      fadeSeconds: 0,
      seconds: 0,
      opacity: 1,
    };
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  componentDidMount() {
    const { interval } = this.props;
    this.setState({
      seconds: interval,
      fadeSeconds: interval
    }, () => {
      this.startTimer();
    });
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    clearInterval(this.fadeTimer);
  }

  startTimer() {
    if (this.timer === 0) {
      this.timer = setInterval(this.countDown, 1000);
    }

    if (this.fadeTimer === 0) {
      this.fadeTimer = setInterval(()=>{
        let fadeSeconds = this.state.fadeSeconds - 0.1;
        this.setState({
          fadeSeconds: fadeSeconds,
          opacity: fadeSeconds / this.props.interval
        });
        if (this.state.fadeSeconds === 0) {
          clearInterval(this.fadeTimer);
        }
      }, 100);
    }
  }

  countDown() {
    let seconds = this.state.seconds - 1;
    this.setState({
      seconds: seconds,
    });

    if (seconds === 0) {
      clearInterval(this.timer);
      this.props.onClearMessage();
    }
  }

  render() {
    const { messageContent, onClearMessage } = this.props;
    const visibility = messageContent === "" ? "--hidden" : "";

    return (
        <div className={`compatibility-error-message--wrapper-flex${visibility}`}>
            <div className='compatibility-error-message--message-text'>{messageContent}</div>
            <div
                className='compatibility-error-message--icon-wrapper'
                onClick={() => {
                    onClearMessage && onClearMessage();
                }}
            >
                <img
                    className='compatibility-error-message--icon-cross'
                    src={crossImg}
                    alt='removeIcon'
                />
            </div>
        </div>
    );
  }
}

export default CompatibilityErrorMessage;
