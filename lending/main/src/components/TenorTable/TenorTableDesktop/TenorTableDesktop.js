import React, {PropTypes} from "react";

import "./TenorTableDesktop.css";

import {UNSECRUED_LOAN_TABLE} from "../../../constants/inputsConstant";
import {dataPath} from "../../../constants/path";
import {selectTableRow} from "../../../actions/actionCreators";

const generateTableRows = (tableContent, props) => {
    const {state, dispatch} = props;
    const selectedRow = state.getIn(dataPath(["products", props.pid, "tenor"]), "");
    return tableContent.map((row, i) => {
        const rowData = Object.keys(row);
        const promoCode = row[rowData.pop()];
        const checkboxSelectedClass = selectedRow === promoCode
            ? "tenor-table--row-item-checkbox--selected"
            : "tenor-table--row-item-checkbox";
        const rowSelectedClass = selectedRow === promoCode ? "tenor-table--row-flex--selected" : "tenor-table--row-flex";
        return (
            <div key={i}>
                <div className='tenor-table--separator' />
                <div className={rowSelectedClass}>
                    <div
                        className={checkboxSelectedClass}
                        onClick={() => dispatch(selectTableRow(UNSECRUED_LOAN_TABLE, props.pid, promoCode))}
                    >
                        {selectedRow === promoCode && <div className='tenor-table--row-item-checkbox-inner-circle' />}
                    </div>
                    {rowData.map((data, index) => <div key={index} className='tenor-table--row-item'>{row[data]}</div>)}
                </div>
            </div>
        );
    });
};

const TenorTableDesktop = props => {
    return (
        <div className='tenor-table--main-container-flex'>
            <div className='tenor-table--titles-container-flex'>
                <div className='tenor-table--row-item-checkbox--title' />
                {props.colTitles.map((title, i) => <div key={i} className='tenor-table--title-flex'>{title}</div>)}
            </div>
            <div className='tenor-table--rows-container-flex'>
                {generateTableRows(props.tableContent, props)}
            </div>
        </div>
    );
};

TenorTableDesktop.propTypes = {
    tableContent: PropTypes.array.isRequired,
    colTitles: PropTypes.array.isRequired
};

export default TenorTableDesktop;
