/* global FORMS_CNFG_GLBL */
import React, {PropTypes} from "react";
import ImmutablePropTypes from "react-immutable-proptypes";

import "./TenorTableMobile.css";

import {UNSECRUED_LOAN_TABLE} from "../../../constants/inputsConstant";
import {dataPath, tableStatePath} from "../../../constants/path";
import {selectTableRow, expandTableRow, collapseTableRow} from "../../../actions/actionCreators";

const Row = props => {
    const {
        rowSelectedClass,
        handleClick,
        arrowClass,
        header,
        data,
        checkboxSelectedClass,
        contentClass,
        bodyTitle,
        dispatch,
        restOfDataKeysExceptHeader,
        isRowOpen,
        promoCode,
        selectedRow
    } = props;
    return (
        <div className={rowSelectedClass}>
            <div className='table-mobile-title-container' onClick={handleClick}>
                <img src={`${FORMS_CNFG_GLBL.imgPath}/arrow.svg`} alt='tick' className={arrowClass} />
                <div className='table-mobile-title-checkbox-container'>
                    <div className='table-mobile-title'>{`${data[header]} Months`}</div>
                    <div
                        className={checkboxSelectedClass}
                        onClick={e => {
                            e.stopPropagation();
                            dispatch(selectTableRow(UNSECRUED_LOAN_TABLE, props.pid, promoCode));
                        }}
                    >
                        {selectedRow === promoCode && <div className='table-mobile-checkbox-inner-circle' />}
                    </div>
                </div>
            </div>
            <div className={`table-mobile-content-container ${contentClass}`}>
                {isRowOpen &&
                    bodyTitle.map((itemTitle, i) => {
                        const currentTitleKey = restOfDataKeysExceptHeader[i];
                        return <RowItem key={i} title={itemTitle} data={data[currentTitleKey]} />;
                    })}
            </div>
        </div>
    );
};

const RowItem = props => {
    return (
        <div className='table-mobile-content-item-container'>
            <div className='table-mobile-content-item-title'>{props.title}</div>
            <div className='table-mobile-content-item-data'>
                {props.data}
            </div>
        </div>
    );
};

const TenorTableMobile = props => {
    const {dispatch, state, colTitles, tableContent} = props;
    const selectedRow = state.getIn(dataPath(["products", props.pid, "tenor"]), "");
    const bodyTitle = colTitles.filter((title, i) => i !== 0);
    return (
        <div className='table-mobile-container'>
            {tableContent.map((data, i) => {
                const dataKeys = Object.keys(data);
                const promoCode = data[dataKeys.pop()];
                const checkboxSelectedClass = selectedRow === promoCode
                    ? "table-mobile-checkbox--selected"
                    : "table-mobile-checkbox";
                const rowSelectedClass = selectedRow === promoCode
                    ? "table-mobile-container-item--selected"
                    : "table-mobile-container-item";

                const header = dataKeys[0];
                const isRowOpen = state.getIn(tableStatePath(`${UNSECRUED_LOAN_TABLE}-${props.pid}`, i));
                const restOfDataKeysExceptHeader = dataKeys.filter(x => x !== header);
                const arrowClass = isRowOpen ? "table-mobile-arrow--selected" : "table-mobile-arrow";
                const contentClass = isRowOpen ? "table-mobile-content-container--selected" : "";
                const handleClick = () =>
                    isRowOpen
                        ? dispatch(collapseTableRow(`${UNSECRUED_LOAN_TABLE}-${props.pid}`, i))
                        : dispatch(expandTableRow(`${UNSECRUED_LOAN_TABLE}-${props.pid}`, i));
                return (
                    <Row
                        key={i}
                        checkboxSelectedClass={checkboxSelectedClass}
                        rowSelectedClass={rowSelectedClass}
                        restOfDataKeysExceptHeader={restOfDataKeysExceptHeader}
                        arrowClass={arrowClass}
                        contentClass={contentClass}
                        handleClick={handleClick}
                        bodyTitle={bodyTitle}
                        data={data}
                        dispatch={dispatch}
                        header={header}
                        index={i}
                        isRowOpen={isRowOpen}
                        promoCode={promoCode}
                        pid={props.pid}
                        selectedRow={selectedRow}
                    />
                );
            })}
        </div>
    );
};

Row.propTypes = {
    handleClick: PropTypes.func.isRequired,
    arrowClass: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired,
    data: PropTypes.object.isRequired,
    contentClass: PropTypes.string.isRequired,
    bodyTitle: PropTypes.array.isRequired,
    restOfDataKeysExceptHeader: PropTypes.array.isRequired,
    isRowOpen: PropTypes.bool,
    rowSelectedClass: PropTypes.string.isRequired,
    checkboxSelectedClass: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired,
    promoCode: PropTypes.string.isRequired,
    selectedRow: PropTypes.string.isRequired
};

RowItem.propTypes = {
    title: PropTypes.string.isRequired,
    data: PropTypes.string.isRequired
};

TenorTableMobile.propTypes = {
    dispatch: PropTypes.func.isRequired,
    state: ImmutablePropTypes.map.isRequired,
    colTitles: PropTypes.array.isRequired,
    tableContent: PropTypes.array.isRequired,
    pid: PropTypes.string.isRequired
};

export default TenorTableMobile;
