import React from 'react';
import PropTypes from 'prop-types';
import './Checkbox.css';


const Checkbox = props => {
  const { description, onClick, isChecked,
    errorMsg, inputID } = props;
  const makeFontBlue = inputID === "mailingAddressCheckbox" ? "mailingFontColor" : "";
  return (
    <div className="toggle-input--container">
      { errorMsg !== "" &&
        <div className="errorMsg">
          {errorMsg}
        </div>}
      <label className={`checkbox-container ${makeFontBlue}`}>
        {description}
        <input type="checkbox"
          onClick={onClick ? onClick : null}
          checked={isChecked}
        />
        <span className="checkmark"></span>
      </label>
    </div>
  );
}

Checkbox.propTypes = {
  isChecked: PropTypes.bool.isRequired,
  onClick: PropTypes.func
};

Checkbox.defaultProps = {
  isValid: true,
  errorMsg: '',
};

export default Checkbox;
