import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './PassCode.css';

class PassCode extends Component {
  constructor(props) {
    super(props);
    this.hiddenInput = null;
    this.state = {
      focusing: false
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.removeFocusingState, true);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.removeFocusingState, true);
  }

  removeFocusingState = () => {
    this.setState({
      focusing: false
    });
  }

  handleOnChange(e) {
    const { onChangeCode } = this.props;
    onChangeCode(e.target.value);
  }

  focusInput(e) {
    e.preventDefault();
    e.stopPropagation();
    this.hiddenInput.focus();
    this.setState({
      focusing: true
    });
  }
  render() {
    const { passwordLength, prefix, passcodeTitle, value } = this.props;
    const tmpArray = [];
    for (let i = 0; i < passwordLength; i++) {
      tmpArray.push(i);
    }
    const focusClass = this.state.focusing ? 'isfocus' : '';

    return (
      <div onClick={this.focusInput.bind(this)} style={{position: 'relative'}}>

        {
          passcodeTitle ? <p>{passcodeTitle}</p> : null
        }

        <input
          className='passcode-input-hidden'
          type='text'
          value={value}
          onChange={this.handleOnChange.bind(this)}
          ref={(input) => this.hiddenInput = input}
          maxLength={passwordLength}
        />
        <div className={`passcode-container`}>
          {
            prefix ? <label className='passcode-prefix'>{prefix}</label> : null
          }
          <div className={`passcode-box-container ${focusClass}`}>
          <label className='passcode-small-box-wrap'>
            {
              tmpArray.map((item, index) => {
                const boxClass = (this.state.focusing && index === value.length) ? 'passcode-small-box passcode-small-box-active' : 'passcode-small-box';
                return (
                  <div
                    key={index}
                    className={boxClass}
                  >
                  {index < value.length ? '•' : ''}
                  </div>
                );
              })
            }
          </label>
          </div>
        </div>
      </div>
    );
  }
}

PassCode.propTypes = {
  password: PropTypes.bool,
  passwordLength: PropTypes.number,
  prefix: PropTypes.string,
  passcodeTitle: PropTypes.string,
  onChangeCode: PropTypes.func.isRequired
};

PassCode.defaultProps = {
  showPassword: false,
  passwordLength: 6
};

export default PassCode;
