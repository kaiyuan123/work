import React from 'react';

import './CustomerOverview.css';


const CustomerOverview = props => {
  const { monthlyPaymentlabels, monthlyPaymentvalue, loanAmountlabels, loanAmountvalue, purchasePricelabels, purchasePricevalue, downpaymentlabels, downpaymentvalue, loanTenurelabels, loanTenurevalue, interestRatelabels, interestRatevalue, effectiveinterestRatelabels,effectiveinterestRatevalue  } = props;
  return (
    <div>
      <table className="overviewTable">
        <tbody>
          <tr className="overviewTable_tr">
              <td className="overviewTable_td"> {monthlyPaymentlabels} </td>
              <td className="overviewTable_td_value"> {monthlyPaymentvalue} </td>
          </tr>
          <tr className="overviewTable_tr">
              <td className="overviewTable_td"> {loanAmountlabels}</td>
              <td className="overviewTable_td_value"> {loanAmountvalue}</td>
          </tr>
          <tr className="overviewTable_tr">
              <td className="overviewTable_td"> {purchasePricelabels}</td>
              <td className="overviewTable_td_value"> {purchasePricevalue}</td>
          </tr>
          <tr className="overviewTable_tr">
              <td className="overviewTable_td"> {downpaymentlabels} </td>
              <td className="overviewTable_td_value"> {downpaymentvalue} </td>
          </tr>
          <tr className="overviewTable_tr">
              <td className="overviewTable_td"> {loanTenurelabels} </td>
              <td className="overviewTable_td_value"> {loanTenurevalue} </td>
          </tr>
          <tr className="overviewTable_tr">
              <td className="overviewTable_td"> {interestRatelabels} </td>
              <td className="overviewTable_td_value"> {interestRatevalue} </td>
          </tr>
          <tr className="overviewTable_tr">
              <td className="overviewTable_td"> {effectiveinterestRatelabels} </td>
              <td className="overviewTable_td_value"> {effectiveinterestRatevalue} </td>
          </tr>
          </tbody>
      </table>
    </div>
  );
};


export default CustomerOverview;
