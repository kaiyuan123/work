import React from 'react';
import PropTypes from 'prop-types';

import './FileUpload.css';

import {bytesToSize, fileUploadHelper} from './../../common/utils';

const FileUpload = props => {
    const {
      isValid,
      errorMsg,
      fileSize,
      inputValue,
      description,
      inputStatus,
      progressValue,
      identifier,
      docType,
      updateUploadProgress,
      handleUploadFile,
      handleShowErrorMsg,
      handleShowUploadErrorMsg
    } = props;
    const showErrorLabel = isValid ? "" : "file-upload--error";
    const fileSizeString = fileSize ? " - " + fileSize : "";
    const descriptionContent = inputValue ? inputValue.replace(/^.*[\\]/, "") + fileSizeString : description;
    const checkInputStatus = inputStatus ? inputStatus : 'none';
    const isFileSelected = checkInputStatus === "uploading" ? "--selected" : "";
    const errorContainerStyle = isValid ? "" : "file-upload--container--error";
    const inputProgressValue = progressValue ? progressValue : 0;
    let fileInputRef = null;
    const checkFileExtension = (file, allowedExtensions) => allowedExtensions.includes(file.split(".").pop());

    const handleOnChange = () => {
        const file = fileInputRef.files[0];
        const extensionIsAllowed = checkFileExtension(file.name, ["jpg", "png", "pdf", "JPG", "PNG", "PDF"]);
        const actions = {
            successDispatch: () => ({}),
            errorDispatch: () => {
                handleShowUploadErrorMsg(props.inputID, 'File upload fail. Please try again later.');
            },
            progressDispatch: progress => updateUploadProgress(props.inputID, progress)
        };
        if (extensionIsAllowed) {
            if (file.size <= 3145728) {
                  fileUploadHelper(file, props.inputID, identifier, docType, actions);
                  handleUploadFile(props.inputID, file.name, bytesToSize(file.size), []);
            } else {
               handleShowErrorMsg(props.inputID, "file size cannot exceed 3MB");
            }
        } else {
            handleShowErrorMsg(props.inputID, "can only be either jpg, png or pdf");
        }
    };
    const fileUploadContainer = () => {
        return (
            <div className={"file-upload-button" + isFileSelected}>
                <progress className='file-upload-progress' max='100' value={inputProgressValue} />
                <div className='file-upload-button--text'>
                    {checkInputStatus === "none" ? "UPLOAD" : "EDIT"}
                </div>
                <input
                    accept='.jpg,.JPG,.png,.pdf,.PDF,.PNG'
                    ref={i => {
                        fileInputRef = i;
                    }}
                    type='file'
                    className='file-upload-input'
                    onClick={() => {
                        fileInputRef.value = null;
                        updateUploadProgress(props.inputID, 0);
                    }}
                    onChange={handleOnChange}
                />
            </div>
        );
    };
    return (
        <div id={props.inputID} className={`file-upload--container ${errorContainerStyle}`}>
            <div className='file-upload--text-container'>
                <div className={"file-upload--title " + showErrorLabel}>
                    {`${props.title}${isValid ? "" : ` ${errorMsg}`}`}
                </div>
                <div className='file-upload--description'>
                    {descriptionContent}
                </div>
            </div>
            <div className='file-upload--button-container'>
                {fileUploadContainer()}
            </div>
        </div>
    );
};

FileUpload.propTypes = {
    inputID: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    dispatchUploadFile: PropTypes.func,
    dispatchShowErrorMsg: PropTypes.func,
    dispatchupdateUploadProgress: PropTypes.func
};


export default FileUpload;
