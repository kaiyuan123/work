import React from 'react';
import PropTypes from 'prop-types';

import './BigButton.css';

import carImg from './../../../assets/images/carloan/dollarcar-blue.svg';

const BigButton = props => {
  const { onClick, label, style, containerStyle } = props;
  return (
    <div style={containerStyle ? containerStyle : null}>
      <div className='start-new-loan-button' onClick={onClick} style={style ? style : null}>
        <img src={carImg} alt='' />
        <label className='start-new-loan-label'>{label}</label>
      </div>
    </div>
  );
};

BigButton.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    style: PropTypes.object,
    containerStyle: PropTypes.object
};

BigButton.defaultProps = {
  isLoading: false
};

export default BigButton;
