import React from 'react';
import './SecondSchedule.css';

const SecondSchedule = props => {
  const{ label, value, indexNo, hasIndexNo } = props;
  return(
    <div>
      <div className='carLoanSummary-labelValue'>
        {hasIndexNo ?
          <div className='carLoanSummary-number'>{indexNo}</div>:null
        }
        <div>{label}</div>
        <div className='carLoanSummary-value'>{value}</div>
      </div>
    </div>
  )
}

export default SecondSchedule;
