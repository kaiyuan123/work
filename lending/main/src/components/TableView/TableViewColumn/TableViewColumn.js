import React from "react";
import "./TableViewColumn.css";
import NumberFormat from "react-number-format";

let currentKey;

const keyName = (title) => {
  switch (title) {
    case "Business Revenue":
      currentKey = "companyRevenue";
      break;
    case "Business Profit Loss Before Tax":
      currentKey = "companyProfitLossBeforeTax";
      break;
    case "Business Profit Loss After Tax":
      currentKey = "companyProfitLossAfterTax";
      break;
    case "Group Revenue":
      currentKey = "groupRevenue";
      break;
    case "Group Capital Paid Up Capital Amount":
      currentKey = "groupCapitalPaidUpCapitalAmount";
      break;
    case "Group Profit Loss Before Tax":
      currentKey = "groupProfitLossBeforeTax";
      break;
    case "Group Profit Loss After Tax":
      currentKey = "groupProfitLossAfterTax";
      break;
    case "Is Audited":
      currentKey = "isAudited";
      break;

    default:
      break;
  }
  return currentKey;
};

const TableViewColumn = props => {
  return (
    <div className="financial-data-container">
      <div className="income-breakdown-table--main-container-flex new-table" style={{ minWidth: props.flexTableWidth ? props.flexTableWidth + 'px' : '100%' }}>
        <div className="income-breakdown-table--titles-container-flex">
          {props.colTitles.map((title, i) => (
            <div key={i} className="income-breakdown-table--title-flex">
              {title}
            </div>
          ))}
        </div>
        <div className="income-breakdown-table--rows-container-flex">
          {props.financialRowTitle.map((title, i) => (
            <div key={i} className="income-breakdown-table--row-flex">
              <div className="income-breakdown-table--row-item">
                {title}
              </div>
              <div className="income-breakdown-table--row-item">
                <table className="currencyContent">
                  <tbody>
                    <tr>
                      <td className="currencyAmount">
                        {props.tableContent[0][keyName(title)] !== undefined && props.tableContent[0][keyName(title)] !== null && props.tableContent[0][keyName(title)] !== ""
                          ? keyName(title) === "isAudited"
                            ? props.tableContent[0][keyName(title)]
                              ? "Yes"
                              : "No"
                            : <NumberFormat value={props.tableContent[0][keyName(title)]}
                              displayType={'text'} thousandSeparator={true} />
                          : "-"}

                      </td>
                      <td className="currencyType">

                        {(props.tableContent[0][keyName(title)] !== undefined && props.tableContent[0][keyName(title)] !== null) &&
                          props.tableContent[0][keyName(title)] !== "" && typeof props.tableContent[0][keyName(title)] !== "boolean"
                          ? props.tableContent[0]["currency"]
                          : ""}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              {props.tableContent[1] !== undefined &&
                <div className="income-breakdown-table--row-item">
                  <table className="currencyContent">
                    <tbody>
                      <tr>
                        <td className="currencyAmount">
                          {props.tableContent[1][keyName(title)] !== undefined && props.tableContent[1][keyName(title)] !== null
                            ? keyName(title) === "isAudited"
                              ? props.tableContent[1][keyName(title)]
                                ? "Yes"
                                : "No"
                              : <NumberFormat value={props.tableContent[1][keyName(title)]}
                                displayType={'text'} thousandSeparator={true} />

                            : "-"}

                        </td>
                        <td className="currencyType">
                          {(props.tableContent[1][keyName(title)] !== undefined && props.tableContent[1][keyName(title)] !== null) && props.tableContent[0][keyName(title)] !== "" &&
                            typeof props.tableContent[1][keyName(title)] !== "boolean"
                            ? props.tableContent[1]["currency"]
                            : ""}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default TableViewColumn;
