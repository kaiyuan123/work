import React from "react";
// import moment from "moment";
import NumberFormat from 'react-number-format';
// import deleteIcon from "./../../../assets/images/delete_icon.svg";
// import "./TableDesktop.css";
// const getValueByCode = (code, list) => {
//   const item = list.find(x => x.incomeType === code);
//   return item ? item.value : null;
// }

const generateTableRows = (tableContent, type, mapItems, labels) => {
  return tableContent.map((row, i) => {
    const YearlyAssessableIncome = row.amount && row.amount !== "" && row.amount !== "NA" && row.amount !== "na" ? row.amount : '-';
    const YearlyEmploymentIncome = row.employment && row.employment !== "" && row.employment !== "NA" && row.employment !== "na" ? row.employment : '-';
    const YearlyTradeIncome = row.trade && row.trade !== "" && row.trade !== "NA" && row.trade !== "na" ? row.trade : '-';
    const YearlyRentalIncome = row.rent && row.rent !== "" && row.rent !== "NA" && row.rent !== "na" ? row.rent : '-';
    const YearlyTaxClearance = row.taxClearance && row.taxClearance !== "" && row.taxClearance !== "NA" && row.taxClearance !== "na" ? row.taxClearance : '-';
    const year = row.yearOfAssessment === null || row.yearOfAssessment === '' || row.yearOfAssessment === 'NA' || row.yearOfAssessment === 'null' ? '' : row.yearOfAssessment;
    const YearlyCategory = row.category ? row.category : '';
    const withCategory = YearlyCategory !== "" ? `${year} (${YearlyCategory})` : `${year}`;
    let textTaxClearance = '';
    if ( labels.taxClearance ) {
      textTaxClearance = labels.taxClearance[YearlyTaxClearance] ? labels.taxClearance[YearlyTaxClearance] : '-';
    } else {
      textTaxClearance = YearlyTaxClearance === 'N' ? 'No' : 'Yes';
    }

    return (
      <div key={i} className="income-breakdown-table--row-flex">
        <div className="income-breakdown-table--row-item">{withCategory}</div>
        <div className="income-breakdown-table--row-item">
          {YearlyAssessableIncome !== "-" ?
            <NumberFormat
              value={YearlyAssessableIncome}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'$'}
            />
            : YearlyAssessableIncome}
        </div>
        <div className="income-breakdown-table--row-item">
          {YearlyTradeIncome !== "-" ? <NumberFormat value={YearlyTradeIncome}
            displayType={'text'}
            thousandSeparator={true}
            prefix={'$'}
          /> : YearlyTradeIncome}
        </div>
        <div className="income-breakdown-table--row-item">
          {YearlyEmploymentIncome !== "-" ?
            <NumberFormat value={YearlyEmploymentIncome}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'$'}
            />
            : YearlyEmploymentIncome}
        </div>
        <div className="income-breakdown-table--row-item">
          {YearlyRentalIncome !== "-" ?
            <NumberFormat value={YearlyRentalIncome}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'$'}
            />
            : YearlyRentalIncome}
        </div>
        <div className="income-breakdown-table--row-item">
          {YearlyTaxClearance !== "-" ?
            textTaxClearance
            : YearlyTaxClearance}
        </div>
      </div>
    )
  });
}

const TableView = props => {
  return (
    <div className="income-breakdown-table--main-container-flex new-table" style={{ minWidth: props.flexTableWidth ? props.flexTableWidth + 'px' : '100%' }}>
      <div className="income-breakdown-table--titles-container-flex">
        {props.colTitles.map((title, i) => (
          <div key={i} className="income-breakdown-table--title-flex">
            {title}
          </div>
        ))}
      </div>
      <div className="income-breakdown-table--rows-container-flex">
        {generateTableRows(props.tableContent, props.tableType, props.mapItems, props.labels)}
      </div>
    </div>
  );
};

export default TableView;
