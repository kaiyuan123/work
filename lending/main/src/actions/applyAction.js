import { Application } from "./../api/httpApi";
import { setLoadingStatus, setErrorMessage } from "./commonAction";
import localStore from './../common/localStore';
import queryString from 'query-string';

export const retrieveMyInfoURL = (productCode) => {
  return ( dispatch , getState) => {
    dispatch(setLoadingStatus(true));
    const globalErrors = getState().commonReducer.appData.globalErrors;
    const isProductCode = getState().commonReducer.appData.productCode;
    Application.retrieveMyInfoRedirectURL(productCode, isProductCode).then(response => {
      const url = response.data.link;
      dispatch(setLoadingStatus(false));
      window.open(url, '_parent');
    }).catch(e => {
      dispatch(setErrorMessage(globalErrors.apiException));
      dispatch(setLoadingStatus(false));
    });
  }
}

export const setParamterToLocalStorage = (parameter) => {
    return (dispatch, getState) => {
      const productId = getState().commonReducer.appData.productCode;
      const params = queryString.parse(parameter);
      const code = params[productId] ? params[productId] : '';
      localStore.setStore('params',parameter);
      localStore.setStore('productCode', code);
    }
}
