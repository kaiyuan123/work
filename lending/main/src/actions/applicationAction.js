import * as types from "./actionTypes";
import { Config, Application, LocalEnvironment } from "./../api/httpApi";
// import { groupByCode } from "../common/utils";
import { setMyInfoFlowStatus, setProcessingStatus, setErrorMessage, setRetrievingMyInfo, setApplicationReferenceNo, setCurrentSection, setLoadingStatus, setExpiryDate } from "./commonAction";
import { setMyInfoContactDetailsData } from "./contactDetailsAction";
import { setMyInfoPersonalIncomeDetailsData, setIncomeDetailData } from "./personalIncomeDetailsAction";
import { setResidentialAddress, setCompanyAddressData, setCompanyUnformattedAddress } from "./localAddressInputAction";
import { setCompanyBasicData } from "./companyBasicDetailsAction";
import { setAdditionalPartnerData, hideAdditionalPartnerField } from './additionalPartnersDetailsAction';
import { setCompanyPreviousNamesData, setCompanyPreviousUENData, setCompanyCapitalsData, setCompanyAppointmentsData, setCompanyShareholdersData, setCompanyFinancialsData, setCompanyGrantsData } from "./companyOverallDetailsAction";
// import localStore from './../common/localStore';
import queryString from 'query-string';

//Retrieve MY Info
export const retrieveMyInfoDetails = (redirectToErrorPage, isCommerical) => {
    return (dispatch, getState) => {
        const response = getState().retrievingReducer.myInfoData;
        dispatch(setRetrievingMyInfo(true));
        if (response.entity && response.entity !== null && response.person && response.person !== null) {
            Promise.resolve().then(() => {
                const shareholders = response.person;
                const companyDetails = response.entity;
                const residentialAddress = shareholders[0].addresses[0];
                // const productObj = response.product ? response.product : {};
                dispatch(setMyInfoResponse(response));
                dispatch(setMyInfoContactDetailsData(shareholders, companyDetails, isCommerical));
                dispatch(setMyInfoPersonalIncomeDetailsData(shareholders, residentialAddress));
                dispatch(setIncomeDetailData(shareholders));
                dispatch(setResidentialAddress(residentialAddress))
                response.entity.basicProfile && dispatch(setCompanyBasicData(response.entity.basicProfile));
                response.entity.previousNames && dispatch(setCompanyPreviousNamesData(response.entity.previousNames));
                response.entity.previousUens && dispatch(setCompanyPreviousUENData(response.entity.previousUens));
                response.entity.capitals && dispatch(setCompanyCapitalsData(response.entity.capitals));
                response.entity.appointments && dispatch(setCompanyAppointmentsData(response.entity.appointments));
                response.entity.shareholders && dispatch(setCompanyShareholdersData(response.entity.shareholders));
                response.entity.financials && dispatch(setCompanyFinancialsData(response.entity.financials));
                response.entity.grants && dispatch(setCompanyGrantsData(response.entity.grants));
                if (response.entity.addresses[0].standard === "N" || response.entity.addresses[0].standard === "L") {
                    dispatch(setCompanyUnformattedAddress(response.entity.addresses[0]));
                } else {
                    dispatch(setCompanyAddressData(response.entity.addresses[0]));
                }
            }).then(() => {
                dispatch(setRetrievingMyInfo(false));
                dispatch(setApplicationDataLoaded());
                dispatch(setMyInfoFlowStatus(true));
            });
        } else {
            Promise.resolve().then(() => {
                dispatch(setErrorWithValue("serviceDown"));
            }).then(() => {
                redirectToErrorPage && redirectToErrorPage();
            });
        }
    };
};

export const setApplicationDataLoaded = () => {
    return {
        type: types.SET_APPLICATION_DATA_LOADED
    };
};

//Initiate Application
export const submitInitiateApplication = (dataObj, handleToPersonalIncomeDetails) => {
    return (dispatch, getState) => {
        const parameter = getState().applicationReducer.startingParameter;
        const errorMsg = getState().commonReducer.appData.globalErrors.apiException;
        const params = queryString.parse(parameter);
        const requiredParameterArray = getState().commonReducer.appData.dataElementObj.requiredParameter;
    		const parameterLength = (parseInt(getState().commonReducer.appData.maxLengthParams, 10) - 1);
        let requiredParameter= [];
        requiredParameterArray.map((i) => {
            const conCatString = params[i] ? `${i}=${params[i]}` : '';
            if(conCatString !== '') {
              requiredParameter.push(conCatString);
            }
            return requiredParameter;
        });
        const parameterNeed = `?${requiredParameter.join('&').substring(0,parameterLength)}`;
        dispatch(setProcessingStatus(true));
        if (LocalEnvironment) {
            Config.getInitialMyinfoData().then(response => {
                const data = response;
                Promise.resolve()
                    .then(() => {
                        dispatch(setInitateMyInfoResponse(data));
                        // console.log(data);
                        if (data.additionalAppointment && data.additionalAppointment.length > 0) {
                            dispatch(setAdditionalPartnerData(data.additionalAppointment));
                        } else {
                            dispatch(hideAdditionalPartnerField(true));
                        }
                        dispatch(setProcessingStatus(false))
                    }).then(() => {
                        handleToPersonalIncomeDetails && handleToPersonalIncomeDetails();
                    })
            }).catch(e => {
                dispatch(setProcessingStatus(false))
                dispatch(setErrorMessage(errorMsg));
            })
            handleToPersonalIncomeDetails && handleToPersonalIncomeDetails();
        }
        else {
            Application.submitInitiateApplication(dataObj, parameterNeed).then(response => {
                const data = response.data;
                Promise.resolve()
                    .then(() => {
                        dispatch(setInitateMyInfoResponse(data));
                        if (data.additionalAppointment && data.additionalAppointment.length > 0) {
                            dispatch(setAdditionalPartnerData(data.additionalAppointment));
                        } else {
                            dispatch(hideAdditionalPartnerField(true));
                        }
                        dispatch(setExpiryDate(data.expiry));
                    }).then(() => {
                        dispatch(setProcessingStatus(false))
                        // localStore.clear();
                        handleToPersonalIncomeDetails && handleToPersonalIncomeDetails();
                    })
            }).catch(e => {
                if (e.response && e.response.status === 403) {
                    dispatch(setProcessingStatus(false));
                    dispatch(setSessionExpirePopup(true));
                } else {
                    dispatch(setProcessingStatus(false))
                    dispatch(setErrorMessage(errorMsg));
                }
            })
        }

    };
};

//Save Application
export const submitPartialApplication = (dataObj) => {
    return (dispatch, getState) => {
        const applicationID = getState().applicationReducer.initiateMyinfoData.id;
        // console.log(getState().applicationReducer.initiateMyinfoData.id);
        Application.submitPartialApplication(dataObj, applicationID).catch(e => {
            if (e.response && e.response.status === 403) {
                dispatch(setSessionExpirePopup(true));
            }
        });
    }
}

export const setSessionExpirePopup = (showPopup) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_SESSIONEXPIRE_POPUP,
            showPopup
        })
    }
}

export const setMyInfoResponse = (response) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_MYINFO_RESPONSES,
            response
        })
    }
}

export const submitApplication = (dataObj, redirectToErrorPage) => {
    return (dispatch, getState) => {
        dispatch(setLoadingStatus(true));
        const applicationID = getState().applicationReducer.initiateMyinfoData.id;
        Application.submitApplication(dataObj, applicationID).then(response => {
            const referenceNo = response.data.referenceNumber;
            dispatch(setApplicationReferenceNo(referenceNo));
            dispatch(setLoadingStatus(false));
            if (referenceNo !== null && referenceNo !== undefined && referenceNo !== '') {
                dispatch(setCurrentSection('thankyou'));
            } else {
                dispatch(setErrorWithValue("NOREF"));
                redirectToErrorPage && redirectToErrorPage();
            }
        }).catch(e => {
            if (e.response && e.response.data.errorCode && e.response.data.errorCode !== '') {
              dispatch(setErrorWithValue(e.response.errorCode));
              redirectToErrorPage && redirectToErrorPage();
            } else if ( e.response && e.response.status ) {
              dispatch(setErrorWithValue(`${e.response.status}Error`));
              redirectToErrorPage && redirectToErrorPage();
            } else {
              dispatch(setErrorWithValue("serviceDown"));
              redirectToErrorPage && redirectToErrorPage();
            }
            dispatch(setLoadingStatus(false));
        })
    }
}

export const setErrorWithValue = (errorCode) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ERROR_MESSAGE_WITH_ERRORCODE,
            errorCode
        })
    }
}

export const storeParameter = (parameter, productCode) => {
    return (dispatch) => {
        dispatch({
            type: types.STORE_PARAMETER,
            parameter,
            productCode
        })
    }
}


export const setInitateMyInfoResponse = (response) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_INITIATE_MYINFO_RESPONSES,
            response
        })
    }
}

export const setErrorWithDescription = (description) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ERROR_MESSAGE_WITH_DESCRIPTION,
            description
        })
    }
}
