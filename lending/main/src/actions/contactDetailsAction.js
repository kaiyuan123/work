import * as types from "./actionTypes";
import { setTextInputHasEdited } from "./commonAction";

export const handleTextInputChange = (data, field) => {
  return (dispatch, getState) => {
    dispatch({
      type: types.CONTACTDETAILS_HANDLE_TEXT_INPUT_CHANGE,
      value: data.value,
      field,
      isValid: data.isValid,
      errorMsg: data.errorMsg,
      isInitial: data.isInitial
    });
    dispatch(setTextInputHasEdited(field));
  };
};

export const setMyInfoContactDetailsData = (shareholdersDataObj, companyDetailsDataObj, isCommerical) => {
  const contactDetailsDataObj = shareholdersDataObj && shareholdersDataObj[0].basicInfo;
  const emailEmpty = contactDetailsDataObj && contactDetailsDataObj.emailAddress ? contactDetailsDataObj.emailAddress : '';

  return {
    type: types.SET_MYINFO_CONTACT_DETAILS,
    emailAddress: contactDetailsDataObj && contactDetailsDataObj.emailAddress ? contactDetailsDataObj.emailAddress : '',
    emailMyinfo: emailEmpty !== "" ? true : false,
    entityName: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.entityName ? companyDetailsDataObj.basicProfile.entityName : '',
    uen: companyDetailsDataObj && companyDetailsDataObj.basicProfile && companyDetailsDataObj.basicProfile.uen ? companyDetailsDataObj.basicProfile.uen : '',
    principalName: contactDetailsDataObj && contactDetailsDataObj.principalName ? contactDetailsDataObj.principalName : '',
    alternateNames: contactDetailsDataObj && contactDetailsDataObj.alternateNames ? contactDetailsDataObj.alternateNames : {},
    mobileNumber: contactDetailsDataObj && contactDetailsDataObj.mobileNumber ? contactDetailsDataObj.mobileNumber : '',
    annualTurnover: companyDetailsDataObj && companyDetailsDataObj.annualTurnover ? companyDetailsDataObj.annualTurnover : '',
    requestLoan: companyDetailsDataObj && companyDetailsDataObj.requestLoan ? companyDetailsDataObj.requestLoan : '',
    isCommerical
  }
}

//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
  return (dispatch) => {
    dispatch({
      type: types.CONTACTDETAILS_DROPDOWN_FOCUS,
      isFocus,
      field
    });
  };
};

//choose item from dropdown
export const selectDropdownItem = (value, description, field) => {
  return (dispatch) => {
    dispatch({
      type: types.CONTACTDETAILS_DROPDOWN_ITEM_SELECT,
      value,
      field,
      description
    })
  };
};

export const changeSearchInputValue = (searchValue, field) => {
  return (dispatch) => {
    dispatch({
      type: types.CONTACTDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE,
      searchValue,
      field
    });
  };
};

export const selectRadioItem = (data, field) => {
  return (dispatch) => {
    dispatch({
      type: types.CONTACTDETAILS_SELECT_RADIO_BUTTON,
      value: data.value,
      field
    })
  }
}
