import * as types from "./actionTypes";
import { capitalize } from "../common/utils";
import { scrollToElement } from "./../actions/commonAction";

export const setAdditionalPartnerData = (obj) => {
  return (dispatch) => {
    dispatch(populatePartners(obj[0], 0));
    if (obj.length > 1) {
      for (let i = 1; i < obj.length; i++) {
        dispatch(incrementPrepopulatedPartner(i));
        dispatch(populateAdditionalPartners(obj[i], i));
      }
    }
    dispatch({
      type: types.SET_MYINFO_ADDITIONAL_PARTNER_DATA,
      shareholdersData: obj
    })
  }
}
//increment count
export const incrementPartner = (key) => {
  return (dispatch, getState) => {
    let partnersID = getState().additionalPartnersDetailsReducer.partnersID;
    partnersID.push(key + 1);
    dispatch({
      type: types.LENDING_INCREMENT_PARTNER_COUNT,
      partnersID
    });
  };
}

export const incrementPrepopulatedPartner = (key) => {
  return (dispatch, getState) => {
    let partnersID = getState().additionalPartnersDetailsReducer.partnersID;
    partnersID.push(key);
    dispatch({
      type: types.LENDING_INCREMENT_PARTNER_COUNT,
      partnersID
    });
  };
}

export const populatePartners = (obj, key) => {
  return (dispatch) => {
    const name = capitalize(obj.name);
    const nric = obj.legalId;
    const email = obj.emailAddress ? obj.emailAddress : "";
    const isMyInfoEmail = email !== "" ? true : false;
    const phone = obj.mobileNumber ? obj.mobileNumber : "+65";
    const isMyInfoPhone = phone === "+65" || phone === '' ? false : true;
    dispatch({
      type: types.POPULATE_PARTNERS,
      key: `partners${key}`,
      partnersName: name,
      partnersNRIC: nric,
      partnersEmail: email,
      emailMyInfo: isMyInfoEmail,
      partnersMobileNo: phone,
      phoneMyInfo: isMyInfoPhone
    })
  }
}

//clear first partner
export const resetFirstPartner = (key) => {
  return (dispatch) => {
    dispatch({
      type: types.RESET_FIRST_PARTNER,
      key: `partners${key}`
    })
  }
}
//Add partner
export const addNewPartner = (key) => {
  return {
    type: types.LENDING_ADD_PARTNER,
    partners: `partners${key + 1}`

  }
}

//Populate other partners
export const populateAdditionalPartners = (obj, key) => {

  const name = capitalize(obj.name);
  const nric = obj.legalId;
  const email = obj.emailAddress ? obj.emailAddress : "";
  const isMyInfoEmail = email !== "" ? true : false;
  const phone = obj.mobileNumber ? obj.mobileNumber: "+65";
  const isMyInfoPhone = phone === "+65" || phone === '' ? false : true;

  return {
    type: types.LENDING_POPULATE_PARTNER,
    partners: `partners${key}`,
    partnersName: name,
    partnersNRIC: nric,
    partnersEmail: email,
    emailMyInfo: isMyInfoEmail,
    partnersMobileNo: phone,
    phoneMyInfo: isMyInfoPhone
  }
}

export const removePartner = (key) => {
  return {
    type: types.LENDING_REMOVE_PARTNER,
    partners: `partners${key}`
  }
}
//Remove partner

//decrease count
export const decreasePartner = (key) => {
  return (dispatch, getState) => {
    let partnersID = getState().additionalPartnersDetailsReducer.partnersID;
    let index = partnersID.indexOf(key);

    dispatch({
      type: types.LENDING_DECREASE_PARTNER,
      partnersID: [...partnersID.slice(0, index), ...partnersID.slice(index + 1)]
    });
  };
}
//Handle text input for partners
export const handleTextInputChange = (data, key, field) => {
  return (dispatch) => {
    dispatch({
      type: types.PARTNERS_HANDLE_TEXT_INPUT_CHANGE,
      key,
      value: data.value,
      field,
      isValid: data.isValid,
      errorMsg: data.errorMsg,
      isInitial: data.isInitial
    });
  };
}

export const clearErrorMsgNric = () => {
  return (dispatch, getState) => {
    let partnersID = getState().additionalPartnersDetailsReducer.partnersID;
    let errorMsg = getState().commonReducer.appData.errorMsgs.duplicateNricMsg;
    for (let i = 0; i < partnersID.length; i++) {
      const key = partnersID[i];
      const partnersNricErrorMsg = getState().additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.errorMsg;
      if (partnersNricErrorMsg === errorMsg) {
        dispatch({
          type: types.CLEAR_ERROR_MSG_NRIC,
          key: `partners${key}`
        });
      }
    }
  }
}



export const removeInvestorID = (key) => {
  return {
    type: types.REMOVE_INVESTOR_ID,
    investors: `investors${key}`
  }
}

export const decreaseInvestorCount = (key) => {
  return (dispatch, getState) => {
    let investorsID = getState().additionalPartnersDetailsReducer.investorsID;
    let index = investorsID.indexOf(1);

    dispatch({
      type: types.DECREASE_INVESTOR_COUNT,
      investorsID: [...investorsID.slice(0, index), ...investorsID.slice(index + 1)]
    });
  };
}

export const addBackFirstPartner = () => {
  return (dispatch) => {
    dispatch({
      type: types.ADD_BACK_FIRST_PARTNER
    })
  }
}

export const hideAdditionalPartnerField = (status) => {
  return (dispatch) => {
    dispatch({
      type: types.HIDE_ADDITIONAL_PARTNER_FIELD,
      status
    })
  }
}

// handle set error msg
export const handleAdditionalErrorMessage = (key, field, errorMsg) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT,
      field,
      key: `partners${key}`,
      errorMsg
    })
  }
}

export const setAdditionalPartnersInputError = (key, field, errorMsg) => {
  return (dispatch) => {
    dispatch(scrollToElement(field + key));
    dispatch({
      type: types.SET_ADDITIONAL_PARTNERS_INPUT_ERROR,
      key: `partners${key}`,
      field,
      errorMsg
    })
  }
}

export const handleAdditionalErrorMessageMobile = (key, field, errorMsg, value) => {
	return (dispatch) => {
		dispatch({
			type: types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT_MOBILE,
			field,
			key: `partners${key}`,
			errorMsg,
			value
		})
	}
}
