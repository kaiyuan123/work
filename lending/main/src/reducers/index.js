import { combineReducers } from 'redux';
import commonReducer from './commonReducer';
import localAddressInputReducer from './localAddressInputReducer';
import contactDetailsReducer from './contactDetailsReducer';
import personalIncomeDetailsReducer from './personalIncomeDetailsReducer';
import companyBasicDetailsReducer from './companyBasicDetailsReducer';
import companyOverallDetailsReducer from './companyOverallDetailsReducer';
import applicationReducer from './applicationReducer';
import additionalPartnersDetailsReducer from './additionalPartnersDetailsReducer';
import confirmTandCReducer from './confirmTandCReducer';
import retrievingReducer from './retrievingReducer';

export default combineReducers({
    commonReducer,
    localAddressInputReducer,
    contactDetailsReducer,
    personalIncomeDetailsReducer,
    companyBasicDetailsReducer,
    applicationReducer,
    additionalPartnersDetailsReducer,
    companyOverallDetailsReducer,
    confirmTandCReducer,
     retrievingReducer
});
