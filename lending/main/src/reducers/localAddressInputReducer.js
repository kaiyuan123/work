import * as types from './../actions/actionTypes';

const initialState = {
  residentialStandard: {
    name: 'residentialStandard',
    isValid: true,
    value: 'D',
    searchValue: '',
    errorMsg: '',
    isInitial: true
  },
  residentialCountry: {
    name: 'residentialCountry',
    isValid: true,
    value: '',
    searchValue: '',
    errorMsg: '',
    isInitial: true
  },
  homeAddressFormat: '',
  residentialPostalCode: {
    name: 'residentialPostalCode',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isLoading: false,
    isReadOnly: false,
    count: 0,
    autoFilled: true
  },
  residentialBlock: {
    name: 'residentialBlock',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  residentialStreet: {
    name: 'residentialStreet',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  residentialBuilding: {
    name: 'residentialBuilding',
    value: 'test',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  residentialLevel: {
    name: 'residentialLevel',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  residentialUnit: {
    name: 'residentialUnit',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  residentialCity: {
    name: 'residentialCity',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  residentialLine1: {
    name: 'residentialLine1',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  residentialLine2: {
    name: 'residentialLine2',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  residentialLine3: {
    name: 'residentialLine3',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  residentialLine4: {
    name: 'residentialLine4',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  mailingStandard: {
    name: 'mailingStandard',
    isValid: true,
    value: 'D',
    searchValue: '',
    errorMsg: '',
    isInitial: true
  },
  mailingCountry: {
    name: 'mailingCountry',
    isValid: true,
    value: 'SG',
    searchValue: '',
    errorMsg: '',
    isInitial: true,
    isFocus: false
  },
  mailingAddressFormat: '',
  mailingPostalCode: {
    name: 'mailingPostalCode',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isLoading: false,
    isReadOnly: false,
    count: 0,
    isFocus: false
  },
  mailingBlock: {
    name: 'mailingBlock',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  mailingStreet: {
    name: 'mailingStreet',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  mailingBuilding: {
    name: 'mailingBuilding',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  mailingLevel: {
    name: 'mailingLevel',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  mailingUnit: {
    name: 'mailingUnit',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  mailingCity: {
    name: 'mailingCity',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  mailingLine1: {
    name: 'mailingLine1',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  mailingLine2: {
    name: 'mailingLine2',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  mailingLine3: {
    name: 'mailingLine3',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  mailingLine4: {
    name: 'mailingLine4',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false,
    isFocus: false
  },
  companyPostalCode: {
    name: 'companyPostalCode',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isLoading: false,
    isReadOnly: false,
    count: 0,
    autoFilled: true
  },
  companyStandard: {
    name: 'companyStandard',
    isValid: true,
    value: '',
    searchValue: '',
    errorMsg: '',
    isInitial: true
  },
  companyBlock: {
    name: 'companyBlock',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  companyStreet: {
    name: 'companyStreet',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  companyBuilding: {
    name: 'companyBuilding',
    value: 'test',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  companyLevel: {
    name: 'companyLevel',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  companyUnit: {
    name: 'companyUnit',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  companyCountry: {
    name: "companyCountry",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true,
    isReadOnly: false
  },
  companyCity: {
    name: 'companyCity',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  companyLine1: {
    name: 'companyLine1',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  companyLine2: {
    name: 'companyLine2',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  companyLine3: {
    name: 'companyLine3',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
  companyLine4: {
    name: 'companyLine4',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true,
    isReadOnly: false
  },
};

const localAddressInputReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOCAL_ADRESS_INPUT_CHANGE:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          value: action.value,
          isValid: action.isValid,
          errorMsg: action.errorMsg,
          isInitial: false
        }
      };

    case types.LOCAL_ADDRESS_INPUT_AUTO_FILL_IN:
      return {
        ...state,
        [`${action.addressType}AddressFormat`]: action.addressFormat,
        [`${action.addressType}PostalCode`]: {
          ...state[`${action.addressType}PostalCode`],
          isLoading: false,
          isReadOnly: false,
          autoFilled: true,
          count: action.count,
        },
        [`${action.addressType}Street`]: {
          ...state[`${action.addressType}Street`],
          value: action.street,
          isValid: true,
          isInitial: false,
          isReadOnly: true
        },
        [`${action.addressType}Block`]: {
          ...state[`${action.addressType}Block`],
          value: action.block,
          isValid: true,
          isInitial: false,
          isReadOnly: true
        },
        [`${action.addressType}Building`]: {
          ...state[`${action.addressType}Building`],
          value: action.building,
          isValid: true,
          isInitial: false,
          isReadOnly: true
        }
      };

    case types.LOCAL_ADDRESS_INPUT_AUTO_FILL_IN_FAIL:
      return {
        ...state,
        [`${action.addressType}PostalCode`]: {
          ...state[`${action.addressType}PostalCode`],
          isValid: false,
          errorMsg: 'is Invalid',
          isReadOnly: false,
          isLoading: false,
          autoFilled: false,
          count: action.count,
        },
        [`${action.addressType}Street`]: {
          ...state[`${action.addressType}Street`],
          isReadOnly: false,
          isInitial: true,
          value: '',
        },
        [`${action.addressType}Block`]: {
          ...state[`${action.addressType}Block`],
          isReadOnly: false,
          isInitial: true,
          value: ''
        },
        [`${action.addressType}Building`]: {
          ...state[`${action.addressType}Building`],
          isReadOnly: false,
          isInitial: true,
          value: ''
        }
      };

    case types.LOCAL_ADDRESS_INPUT_RESET_AUTO_FILL_IN_STATUS:
      return {
        ...state,
        [`${action.addressType}PostalCode`]: {
          ...state[`${action.addressType}PostalCode`],
          isReadOnly: false,
          isLoading: false,
          autoFilled: false,
          count: 10
        },
        [`${action.addressType}Street`]: {
          ...state[`${action.addressType}Street`],
          isReadOnly: false,
          isInitial: true,
          value: ''
        },
        [`${action.addressType}Block`]: {
          ...state[`${action.addressType}Block`],
          isReadOnly: false,
          isInitial: true,
          value: ''
        },
        [`${action.addressType}Building`]: {
          ...state[`${action.addressType}Building`],
          isReadOnly: false,
          isInitial: true,
          value: ''
        }
      };

    case types.LOCAL_ADDRESS_INPUT_SERVICE_DOWN:
      return {
        ...state,
        [`${action.addressType}PostalCode`]: {
          ...state[`${action.addressType}PostalCode`],
          isReadOnly: false,
          isLoading: false,
          autoFilled: false,
          count: 10
        }
      };

    case types.IS_GETTING_LOCAL_ADDRESS:
      return {
        ...state,
        [`${action.addressType}PostalCode`]: {
          ...state[`${action.addressType}PostalCode`],
          isLoading: action.isLoading
        }
      };

    case types.LOCAL_ADDRESS_SET_TEXT_INPUT_REQUIRED_ERROR:
      return {
        ...state,
        [action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
      };

    case types.LOCAL_ADDRESS_MYINFO_SET_DATA:
      return {
        ...state,
        homePostalCode: { ...state.homePostalCode, value: action.homePostalCode, isReadOnly: true, autoFilled: true, isInitial: false },
        homeBlock: { ...state.homeBlock, value: action.homeBlock, isReadOnly: true, isInitial: false },
        homeStreet: { ...state.homeStreet, value: action.homeStreet, isReadOnly: true, isInitial: false },
        homeLevel: { ...state.homeLevel, value: action.homeLevel, isReadOnly: true, isInitial: false },
        homeUnit: { ...state.homeUnit, value: action.homeUnit, isReadOnly: true, isInitial: false },
        homeBuilding: { ...state.homeBuilding, value: action.homeBuilding, isReadOnly: true, isInitial: false }
      };

    case types.SET_MYINFO_COMPANY_ADDRESS_DATA:
      return {
        ...state,
        companyPostalCode: { ...state.companyPostalCode, value: action.companyPostalCode, isReadOnly: true, autoFilled: true, isInitial: false },
        companyBlock: { ...state.companyBlock, value: action.companyBlock, isReadOnly: true, isInitial: false },
        companyStreet: { ...state.companyStreet, value: action.companyStreet, isReadOnly: true, isInitial: false },
        companyLevel: { ...state.companyLevel, value: action.companyLevel, isReadOnly: true, isInitial: false },
        companyUnit: { ...state.companyUnit, value: action.companyUnit, isReadOnly: true, isInitial: false },
        companyBuilding: { ...state.companyBuilding, value: action.companyBuilding, isReadOnly: true, isInitial: false },
        companyCountry: { ...state.companyCountry, value: action.companyCountry, isReadOnly: true, isInitial: false },
        companyLine1: { ...state.companyLine1, value: action.companyLine1, isReadOnly: true, isInitial: false },
        companyLine2: { ...state.companyLine2, value: action.companyLine2, isReadOnly: true, isInitial: false },
        companyStandard: { ...state.companyStandard, value: action.companyStandard, isReadOnly: true, isInitial: false }
      };

    case types.MYINFO_SET_COMPANY_UNFORMATTED_ADDRESS_DATA:
      return {
        ...state,
        companyCountry: {
          ...state.companyCountry,
          value: action.companyCountry,
          isReadOnly: true,
          isInitial: false
        },
        companyLine1: {
          ...state.companyLine1,
          value: action.companyLine1,
          isReadOnly: true,
          isInitial: false
        },
        companyLine2: {
          ...state.companyLine2,
          value: action.companyLine2,
          isReadOnly: true,
          isInitial: false
        },
        companyStandard: {
          ...state.companyStandard,
          value: action.companyStandard,
          isReadOnly: true,
          isInitial: false
        }
      }

    case types.LOCAL_ADDRESS_SET_NOT_REQUIRED:
      return {
        ...state,
        [action.field]: { ...state[action.field], isValid: true, errorMsg: '' }
      };

    case types.SET_MYINFO_RESIDENTIAL_ADDRESS:
      return {
        ...state,
        residentialPostalCode: { ...state.residentialPostalCode, value: action.residentialPostalCode, isReadOnly: true, autoFilled: true, isInitial: false },
        residentialBlock: { ...state.residentialBlock, value: action.residentialBlock, isReadOnly: true, isInitial: false },
        residentialStreet: { ...state.residentialStreet, value: action.residentialStreet, isReadOnly: true, isInitial: false },
        residentialLevel: { ...state.residentialLevel, value: action.residentialLevel, isReadOnly: true, isInitial: false },
        residentialUnit: { ...state.residentialUnit, value: action.residentialUnit, isReadOnly: true, isInitial: false },
        residentialBuilding: { ...state.residentialBuilding, value: action.residentialBuilding, isReadOnly: true, isInitial: false },
        residentialCountry: {...state.residentialCountry, value: action.residentialCountry, isReadOnly: true, isInitial: false  }
      }

    case types.SET_ERROR_MESSAGE_INPUT:
      return {
        ...state,
        [action.field]: {...state[action.field], errorMsg: action.errorMsg, isValid: false}
      };

    case types.LOCAL_ADDRESS_DROPDOWN_FOCUS:
        return { ...state, [action.field]: { ...state[action.field], isFocus: action.isFocus } };

    case types.LOCAL_ADDRESS_DROPDOWN_ITEM_SELECT:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          value: action.value,
          description: action.description,
          isValid: true,
          errorMsg: '',
          isInitial: false
        },
        mailingStandard: {
          ...state.mailingStandard,
          value: action.standard
        },
    };

    case types.LOCAL_ADDRESS_DROPDOWN_SEARCH_INPUT_CHANGE:
        return { ...state, [action.field]: { ...state[action.field], searchValue: action.searchValue } };

    case types.SET_TEXT_INPUT_FOCUS:
      return {
        ...state,
        [action.field]: {...state[action.field], isFocus: action.status}
      }

    default:
      return state;
  }
}

export default localAddressInputReducer;
