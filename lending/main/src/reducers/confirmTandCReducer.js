import * as types from './../actions/actionTypes';

const initialState = {
  acknowledgementCheckbox: {
    isToggled: false,
    isValid: false,
    errorMsg: ""
  }
};

const confirmTandCReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.TANDC_IS_CHECKED:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          isToggled: action.isToggled,
          isValid: action.isValid,
          errorMsg: ""
        }
      };

    case types.SET_ERROR_MESSAGE_INPUT:
      return {
        ...state,
        [action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
      };

    default:
      return state;
  }
}

export default confirmTandCReducer;
