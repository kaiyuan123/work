import * as types from "./../actions/actionTypes";

const initialState = {
  shareholdersData: {
    name: "shareholdersData",
    openRows: [],
    value: []
  },

  addClicked: false,
  partnersID: [0],
  partners0: {
    partnersName: {
      name: "partnersName",
      isValid: true,
      value: "",
      errorMsg: "",
      isInitial: true,
      isReadOnly: false,
      isFocus: false
    },
    partnersNRIC: {
      name: "partnersNRIC",
      isValid: true,
      value: "",
      errorMsg: "",
      isInitial: true,
      isReadOnly: false,
      isFocus: false
    },
    partnersEmail: {
      name: "partnersEmail",
      isValid: true,
      value: "",
      errorMsg: "",
      isInitial: true,
      isMyInfo: false,
      isFocus: false,
      hasEdit: false
    },
    partnersMobileNo: {
      name: "partnersMobileNo",
      isValid: true,
      value: "+65",
      errorMsg: "",
      isInitial: true,
      isMyInfo: false,
      isFocus: false,
      hasEdit: false
    }
  },
  hidePartnerField: false,
};

const additionalPartnersDetailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.PARTNERS_HANDLE_TEXT_INPUT_CHANGE:
      return {
        ...state,
        [action.key]: {
          ...state[action.key],
          [action.field]: {
            ...state[action.key][action.field],
            value: action.value,
            isValid: action.isValid,
            errorMsg: action.errorMsg,
            isInitial: false
          }
        }
      };

    case types.LENDING_INCREMENT_PARTNER_COUNT:
      return {
        ...state,
        partnersID: action.partnersID
      };

    case types.POPULATE_PARTNERS:
      return {
        ...state,
        [action.key]: {
          ...state[action.key],
          partnersName: {
            ...state[action.key]["partnersName"],
            value: action.partnersName,
            isInitial: true,
            isValid: true,
            isReadOnly: true,
            isFocus: false
          },
          partnersNRIC: {
            ...state[action.key]["partnersNRIC"],
            value: action.partnersNRIC,
            isInitial: true,
            isValid: true,
            isReadOnly: true,
            isFocus: false
          },
          partnersEmail: {
            ...state[action.key]["partnersEmail"],
            value: action.partnersEmail,
            isInitial: true,
            isValid: true,
            isMyInfo: action.emailMyInfo,
            hasEdit: false
          },
          partnersMobileNo: {
            ...state[action.key]["partnersMobileNo"],
            value: action.partnersMobileNo,
            isInitial: true,
            isValid: true,
            isMyInfo: action.phoneMyInfo,
            isFocus: false,
            hasEdit: false
          }
        }
      };

    case types.RESET_FIRST_PARTNER:
      return {
        ...state,
        [action.key]: {
          ...state[action.key],
          partnersName: {
            ...state[action.key]["partnersName"],
            value: "",
            isInitial: true,
            isValid: true,
            isReadOnly: false,
            isFocus: false
          },
          partnersNRIC: {
            ...state[action.key]["partnersNRIC"],
            value: "",
            isInitial: true,
            isValid: true,
            isReadOnly: false,
            isFocus: false
          },
          partnersEmail: {
            ...state[action.key]["partnersEmail"],
            value: "",
            isInitial: true,
            isValid: true,
            isFocus: false
          },
          partnersMobileNo: {
            ...state[action.key]["partnersMobileNo"],
            value: "+65",
            isInitial: true,
            isValid: true,
            isFocus: false
          }
        },
        hidePartnerField: true
      };

    case types.LENDING_POPULATE_PARTNER:
      return {
        ...state,
        [action.partners]: {
          partnersName: {
            name: "partnersName",
            isValid: true,
            value: action.partnersName,
            errorMsg: "",
            isInitial: true,
            isReadOnly: true,
            isFocus: false
          },

          partnersNRIC: {
            name: "partnersNRIC",
            isValid: true,
            value: action.partnersNRIC,
            errorMsg: "",
            isInitial: true,
            isReadOnly: true,
            isFocus: false
          },

          partnersEmail: {
            name: "partnersEmail",
            isValid: true,
            value: action.partnersEmail,
            errorMsg: "",
            isInitial: true,
            isMyInfo: action.emailMyInfo,
            isFocus: false,
            hasEdit: false
          },

          partnersMobileNo: {
            name: "partnersMobileNo",
            isValid: true,
            value: action.partnersMobileNo,
            errorMsg: "",
            isInitial: true,
            isMyInfo: action.phoneMyInfo,
            isFocus: false,
            hasEdit: false
          }
        }
      };

    case types.LENDING_ADD_PARTNER:
      return {
        ...state,
        [action.partners]: {
          partnersName: {
            name: "partnersName",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
            isReadOnly: false,
            isFocus: false
          },

          partnersNRIC: {
            name: "partnersNRIC",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
            isReadOnly: false,
            isFocus: false
          },

          partnersEmail: {
            name: "partnersEmail",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
            isFocus: false
          },

          partnersMobileNo: {
            name: "partnersMobileNo",
            isValid: true,
            value: "+65",
            errorMsg: "",
            isInitial: true,
            isFocus: false
          }
        }
      };

    case types.LENDING_REMOVE_PARTNER:
      let newState = state;
      delete newState[action.partners];

      return {
        ...newState
      };

    case types.LENDING_DECREASE_PARTNER:
      return {
        ...state,
        partnersID: action.partnersID
      };

    case types.SET_MYINFO_ADDITIONAL_PARTNER_DATA:
      return {
        ...state,
        shareholdersData: {
          ...state.shareholdersData,
          value: action.shareholdersData
        }
      };

    case types.ADD_BACK_FIRST_PARTNER:
      return {
        ...state,
        hidePartnerField: false
      };

    case types.HIDE_ADDITIONAL_PARTNER_FIELD:
      return {
        ...state,
        hidePartnerField: action.status
      };

    case types.CLEAR_ERROR_MSG_NRIC:
      return {
        ...state,
        [action.key]: {
          ...state[action.key],
          partnersNRIC: {
            ...state[action.key].partnersNRIC,
            isValid: true,
            errorMsg: ""
          }
        }
      }

    case types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT:
      return {
        ...state,
        [action.key]: {
          ...state[action.key],
          [action.field]: { ...state[action.key][action.field], errorMsg: action.errorMsg, isValid: false }
        }
      };

    case types.SET_ADDITIONAL_PARTNERS_INPUT_ERROR:
      return {
        ...state,
        [action.key]: {
          ...state[action.key],
          [action.field]: {
            ...state[action.key][action.field],
            isValid: false,
            errorMsg: action.errorMsg,
            isInitial: false
          }
        }
      };

    case types.SET_TEXT_INPUT_FOCUS_WITH_KEY:
    return {
      ...state,
      [action.key]: {
        ...state[action.key],
        [action.field]: {
          ...state[action.key][action.field],
          isFocus: action.status
        }
      }
    };

    case types.SET_TEXT_INPUT_HAS_EDITED_WITH_KEY:
      console.log(action.key);
      return {
        ...state,
        [action.key]: {
          ...state[action.key],
          [action.field]: {
            ...state[action.key][action.field],
            hasEdit: true
          }
        }
      };

    case types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT_MOBILE:
  		return {
  			...state,
  			[action.key]: {
  				...state[action.key],
  				[action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false,value: action.value }
  			}
  		};

    default:
      return state;
  }
};

export default additionalPartnersDetailsReducer;
