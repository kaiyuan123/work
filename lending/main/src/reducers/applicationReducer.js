import * as types from './../actions/actionTypes';

const initialState = {
  retrieveError: false,
  serviceDown: false,
  dataLoaded: false,
  isLoading: true,
  myInfoData: null,
  errorCode: "",
  initiateMyinfoData: null,
  productCode:"",
  isInitial: false,
  sessionExpirePopup: false
};

const applicationReducer = (state = initialState, action) => {
  switch (action.type) {

    case types.SET_ERROR_MESSAGE_WITH_DESCRIPTION:
      return {
        ...state,
        errorCode: {
          ...state.errorCode,
          description: action.description
        },
        hasError: true,
        dataLoaded: false
      }

    case types.SET_APPLICATION_DATA_LOADED:
      return {
        ...state,
        dataLoaded: true,
        isLoading: false
      };

    case types.SET_MYINFO_RESPONSES:
      return {
        ...state,
        myInfoData: action.response
      }

    case types.SET_ERROR_MESSAGE_WITH_ERRORCODE:
      return {
        ...state,
        errorCode: action.errorCode,
        hasError: true,
        dataLoaded: false,
      }

    case types.STORE_PARAMETER:
      return {
        ...state,
        startingParameter: action.parameter,
        productCode: action.productCode
      }

    case types.SET_INITIATE_MYINFO_RESPONSES:
      return {
        ...state,
        initiateMyinfoData: action.response,
        isInitial: true
      }

    case types.SET_SESSIONEXPIRE_POPUP:
      return {
         ...state,
        sessionExpirePopup: action.showPopup
      }

    default:
      return state;
  }
}

export default applicationReducer;
