import * as types from "./../actions/actionTypes";

const initialState = {
  emailAddress: {
    name: "emailAddress",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true,
    isMyInfo: false,
    isFocus: false,
    hasEdit: false
  },
  entityName: {
    name: "entityName",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true,
    hasEdit: false
  },
  uen: {
    name: "uen",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true,
    hasEdit: false
  },
  principalName: {
    name: "principalName",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true,
    hasEdit: false
  },
  alternateNames: {},
  mobileNumber: '',
  annualTurnover: {
    name: "annualTurnover",
    isValid: true,
    isFocus: false,
    value: '',
    errorMsg: '',
    isInitial: true,
    description: ''
  },
  requestLoan: {
    name: "requestLoan",
    isValid: true,
    isFocus: false,
    value: '',
    errorMsg: '',
    isInitial: true,
    description: ''
  },
  isCommerical: false
};

const contactDetailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_MYINFO_CONTACT_DETAILS:
      return {
        ...state,
        emailAddress: { ...state.emailAddress, value: action.emailAddress, isInitial: false, isMyInfo: action.emailMyinfo },
        entityName: { ...state.entityName, value: action.entityName, isInitial: false },
        uen: { ...state.uen, value: action.uen, isInitial: false },
        principalName: { ...state.principalName, value: action.principalName, isInitial: false },
        mobileNumber: { ...state.mobileNumber, value: action.mobileNumber, isInitial: false },
        alternateNames: action.alternateNames,
        requestLoan: { ...state.requestLoan, value: action.requestLoan, isInitial: false },
        annualTurnover: { ...state.annualTurnover, value: action.annualTurnover, isInitial: false },
        isCommerical: action.isCommerical
      };

    case types.CONTACTDETAILS_HANDLE_TEXT_INPUT_CHANGE:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          value: action.value,
          isValid: action.isValid,
          errorMsg: action.errorMsg,
          isInitial: false
        }
      };

    case types.CONTACTDETAILS_DROPDOWN_FOCUS:
      return { ...state, [action.field]: { ...state[action.field], isFocus: action.isFocus }, inputIDClick: action.field };

    case types.CONTACTDETAILS_DROPDOWN_ITEM_SELECT:
      return { ...state, [action.field]: { ...state[action.field], value: action.value, description: action.description, isValid: true, errorMsg: '', isInitial: false } };

    case types.CONTACTDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE:
      return { ...state, [action.field]: { ...state[action.field], searchValue: action.searchValue } };

    case types.SET_ERROR_MESSAGE_INPUT:
      return {
        ...state,
        [action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
      };

    case types.CONTACTDETAILS_SELECT_RADIO_BUTTON:
      return {
        ...state,
        [action.field]: { ...state[action.field], value: action.value, errorMsg: "", isValid: true }
      }

    case types.SET_TEXT_INPUT_FOCUS:
      return {
        ...state,
        [action.field]: {...state[action.field], isFocus: action.status}
      }

    case types.SET_TEXT_INPUT_HAS_EDITED:
      return {
        ...state,
        [action.field]: {...state[action.field], hasEdit: true}
      }
    default:
      return state;
  }
};

export default contactDetailsReducer;
