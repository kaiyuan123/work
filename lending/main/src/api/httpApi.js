/* global GLOBAL_CONFIG */
import axios from 'axios';

const getFormConfigURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.formConfig : `${GLOBAL_CONFIG.API_PATH}/specifications`;
const getInitialMyinfoDataURL = GLOBAL_CONFIG.initateMyInfo;
// const retrieveLoanWithMyInfoURL = `${GLOBAL_CONFIG.API_PATH}/applications`;
//const retrieveLoanWithMyInfoURL = GLOBAL_CONFIG.myinfoDataJson; //hardcoded data
const retrieveLoanWithMyInfoURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.myinfoDataJson : `${GLOBAL_CONFIG.API_PATH}/applications/application`;
const getAddressByPostalCodeURL = `${GLOBAL_CONFIG.COMMON_API_PATH}/referencedata/address`;
const commonAPIURL = `${GLOBAL_CONFIG.API_PATH}/applications`;
const retrieveMyInfoURL = `${GLOBAL_CONFIG.API_PATH}/myinfo/redirecturl`;
const callbackURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.myinfoDataJson : `${GLOBAL_CONFIG.CALLBACK_API_PATH}`

const LocalEnvironment = GLOBAL_CONFIG.Local_Environment;
const root_path_commerical = GLOBAL_CONFIG.root_path_commerical;
const root_path_business = GLOBAL_CONFIG.root_path_business;
const timeout = GLOBAL_CONFIG.timeout;
const httpapi = axios.create();
httpapi.defaults.timeout = timeout;

const Application = {
  submitInitiateApplication: (dataObj, parameter) => {
  		return new Promise((resolve, reject) => {
  			httpapi
  				.post(`${commonAPIURL}/application${parameter}`, dataObj)
  				.then(response => {
  					resolve(response);
  				})
  				.catch(e => reject(e));
  		});
    },
    submitPartialApplication: (dataObj,applicationID) => {
        return new Promise((resolve, reject) => {
            httpapi
                .post(`${commonAPIURL}/${applicationID}`, dataObj)
                .then(response => {
                    resolve(response);
                })
                .catch(e => reject(e));
        });
    },
    submitApplication: (dataObj, applicationID) => {
        return new Promise((resolve, reject) => {
            httpapi
                .post(`${commonAPIURL}/${applicationID}/submit`, dataObj)
                .then(response => {
                    resolve(response);
                })
                .catch(e => reject(e));
        });
    },
    retrieveMyInfoRedirectURL: (productCode, isProductCode) => {
      return new Promise((resolve, reject) => {
        httpapi.get(`${retrieveMyInfoURL}?${isProductCode}=${productCode}`).then((response)=>{
          resolve(response);
        }).catch((e) => reject(e));
      })
    },
    myInfoCallback: (parameter) => {
      return new Promise((resolve, reject) => {
        httpapi
          .get(`${callbackURL}${parameter}`)
          .then(response => {
            resolve(response);
          })
          .catch(e => reject(e));
      });
    }
};

const Config = {
    getInitialData: () => {
        return new Promise((resolve) => {
            httpapi.get(getFormConfigURL).then((response) => {
                resolve(response.data);
            });
        });
    },
    getMyInfoData: () => {
        return new Promise((resolve, reject) => {
              httpapi.get(retrieveLoanWithMyInfoURL).then((response) => {
                resolve(response.data);
            }).catch((e) => reject(e));
        });
    },
    getInitialMyinfoData: () => {
        return new Promise((resolve) => {
            httpapi.get(getInitialMyinfoDataURL).then((response) => {
                resolve(response.data);
            });
        });
    },
}

const Address = {
    getAddressByCode: (postCode) => {
        return new Promise((resolve, reject) => {
            axios.get(`${getAddressByPostalCodeURL}?postalcode=${postCode}`).then((response) => {
                resolve(response);
            }).catch((e) => reject(e));
        })
    }
};

export {
    Application,
    Config,
    Address,
    LocalEnvironment,
    root_path_business,
    root_path_commerical
}
