import React, { Component } from 'react';
import { connect } from 'react-redux';
import greenTick from './../assets/images/green-tick-successful.svg';
import MessageBox from './../components/MessageBox/MessageBox';
import { sendDataToSparkline } from "./../common/utils";
import moment from "moment";

class ThankYouPage extends Component {
  componentWillMount() {
    const { commonReducer, applicationReducer} = this.props;
    const { isInitial } = applicationReducer;
    const dataElement = commonReducer.appData.dataElementObj
    const productCode = applicationReducer.productCode ? applicationReducer.productCode.toLowerCase() : "";
    const referenceNo = commonReducer.referenceNo;
    sendDataToSparkline(dataElement, productCode, "" , false, false, isInitial, referenceNo, true);
  }

  render() {
    const { commonReducer, additionalPartnersDetailsReducer, applicationReducer } = this.props;
    const { hidePartnerField, partnersID } = additionalPartnersDetailsReducer;
    const { productCode } = applicationReducer;
    const code = productCode.toUpperCase();
    const productTitle = commonReducer.appData.productTitleThankYou;
    const codeDesc = productTitle[code]
    const thankyouValues = !hidePartnerField && partnersID.length > 0 ? commonReducer.appData.thankYouPagePartner : commonReducer.appData.thankYouPage;
    const referenceNo = commonReducer.referenceNo;
    const applicationExpiryDate = commonReducer.applicationExpiryDate;
    const date = moment().format("DD/MM/YYYY");
    const subTitle = thankyouValues.subtitle.replace("{productTitle}", codeDesc).replace("{referenceNo}", referenceNo).replace("{date}", date).replace("{applicationExpiryDate}", applicationExpiryDate);
    // const referenceNoText = thankyouValues.referenceNo.replace("{referenceNo}", referenceNo);
    
    return(
      <div id="loadingPage" className = 'uob-content'>
        <div className="error-pg-container">
          <MessageBox
            isThankyou
            img={greenTick}
            title={thankyouValues.title}
            subtitle={subTitle}
            description={thankyouValues.description}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { commonReducer, additionalPartnersDetailsReducer, applicationReducer } = state;
  return { commonReducer, additionalPartnersDetailsReducer, applicationReducer };
}

export default connect(mapStateToProps)(ThankYouPage);
