import React, { Component } from "react";
import { connect } from "react-redux";
import TextInput from "./../components/TextInput/TextInput";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import SecondaryButton from "./../components/SecondaryButton/SecondaryButton";
import { decreaseInvestorCount, handleTextInputChange, incrementPartner, addNewPartner, removePartner, decreasePartner, resetFirstPartner, addBackFirstPartner, clearErrorMsgNric } from "./../actions/additionalPartnersDetailsAction";
import { handleTextInputWithKeyFocus } from "./../actions/commonAction";
import deleteIcon from "./../assets/images/delete_icon.svg";
import addIcon from "./../assets/images/add_icon.svg";

class AdditionalPartnersDetails extends Component {
	handleToReviewPage() {
		this.props.onContinue();
	}

	isAdditionalPartnerCheckingPass() {
		this.props.onCheck();
	}
	deleteInvestor(key) {
		const { dispatch } = this.props;
		dispatch(decreaseInvestorCount(key));
		//dispatch(removeInvestor(key));
	}

	handleOnChange(data, key, field) {
		const { dispatch } = this.props;
		if (field === 'partnersNRIC') {
			dispatch(clearErrorMsgNric());
		}
		dispatch(handleTextInputChange(data, key, field));
	}
	//Add
	addPartners(key) {
		const { dispatch, additionalPartnersDetailsReducer } = this.props;
		const partnersID = additionalPartnersDetailsReducer.partnersID;
		const maxKey = Math.max.apply(null, partnersID);
		dispatch(incrementPartner(maxKey));
		dispatch(addNewPartner(maxKey));

	}
	//Delete
	deletePartner(key) {
		const { dispatch } = this.props;
		dispatch(decreasePartner(key));
		dispatch(removePartner(key));

	}
	//Delete first partner
	removeFirstPartner(key) {
		const { dispatch } = this.props;
		dispatch(resetFirstPartner(key));
	}

	addFirstPartner() {
		const { dispatch } = this.props;
		dispatch(addBackFirstPartner())
	}

	handleTextInputOnFocus(key, field, status){
		const {dispatch} = this.props;
		dispatch(handleTextInputWithKeyFocus(key, field, status));
	}

	handleTextInputOnBlur(key, field, status){
		const {dispatch} = this.props;
		dispatch(handleTextInputWithKeyFocus(key, field,status))
	}

	addtionalPartnersDetailsValid() {
		const { additionalPartnersDetailsReducer } = this.props;
		const { hidePartnerField, partnersID } = additionalPartnersDetailsReducer;
		let errorCount = 0;
		if (!hidePartnerField && partnersID.length > 0) {
			for (let j = 0; j < partnersID.length; j++) {
				for (let k = j + 1; k < partnersID.length; k++) {
					const key = partnersID[k];
					const compareKey = partnersID[j];
					if (k !== j && additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.value === additionalPartnersDetailsReducer[`partners${compareKey}`].partnersNRIC.value) {
						errorCount++;
					}
				}
			}
			additionalPartnersDetailsReducer.partnersID.map((key) => {
				const partnersEmail = additionalPartnersDetailsReducer[`partners${key}`].partnersEmail;
				const partnersNRIC = additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC;
				const partnersName = additionalPartnersDetailsReducer[`partners${key}`].partnersName;
				const partnersMobileNo = additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo;

				const isAdditionalPartnerValid = partnersEmail.value !== "" && partnersEmail.isValid && partnersNRIC.value !== "" && partnersNRIC.isValid && partnersName.value !== "" && partnersName.isValid && partnersMobileNo.value !== "" && partnersMobileNo.isValid;
				if (!isAdditionalPartnerValid) {
					errorCount++
				}
				return errorCount;
			})

			return errorCount;
		}
	}

	renderAdditionalPartners() {
		const { additionalPartnersDetailsReducer, commonReducer } = this.props;
		const { partnersID } = additionalPartnersDetailsReducer;

		//Partners mapping
		const maxKey = Math.max.apply(null, partnersID);
		const length = partnersID.length;
		const addtionalPartnersDetailsValue = commonReducer.appData.addtionalPartnersDetails;
		const labels = addtionalPartnersDetailsValue.labels;
		const tmpArray = [];
		const errorMsgList = commonReducer.appData.errorMsgs;
		const maxPartner = parseInt(addtionalPartnersDetailsValue.maxPartner, 10);

		partnersID.map((key, i) => {
			const applicantNumber = i + 1;
			tmpArray.push(
				<div key={key} id={`${key}`} >
					<div className="partner-subtitle-container">
						<h1 className="sub-title partner-subtite">{labels.subtitle.replace("{key}", applicantNumber)}</h1>
						{length > 1 &&
							<img src={deleteIcon} className="partner-delete-icon" alt="delete" onClick={() => this.deletePartner(key)} />
						}

						{
							length === 1 &&
							<img src={deleteIcon} className="partner-delete-icon" alt="delete" onClick={() => this.removeFirstPartner(key)} />
						}

					</div>
					<div className="top-border">
						<div className="fullTable confirmDetails-flexContainer">
							<div className="halfRow right-border positionRelative">
								<TextInput
									inputID={`partnersName${key}`}
									label={labels.partnersName}
									value={additionalPartnersDetailsReducer[`partners${key}`].partnersName.value}
									errorMsg={additionalPartnersDetailsReducer[`partners${key}`].partnersName.errorMsg}
									isValid={additionalPartnersDetailsReducer[`partners${key}`].partnersName.isValid}
									onChange={(data) => this.handleOnChange(data, `partners${key}`, 'partnersName')}
									validator={["isFullName", "maxSize|70"]}
									errorMsgList={errorMsgList}
									hasIcon={false}
									isReadOnly={additionalPartnersDetailsReducer[`partners${key}`].partnersName.isReadOnly}
									isFocus={additionalPartnersDetailsReducer[`partners${key}`].partnersName.isFocus}
									onFocus={() => this.handleTextInputOnFocus(`partners${key}`, "partnersName", true)}
									onBlur={() => this.handleTextInputOnFocus(`partners${key}`, "partnersName", false)}
								/>
							</div>
							<div className="halfRow positionRelative">
								<TextInput
									isUpperCase
									inputID={`partnersNRIC${key}`}
									label={labels.partnersNRIC}
									value={additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.value}
									errorMsg={additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.errorMsg}
									isValid={additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.isValid}
									onChange={(data) => this.handleOnChange(data, `partners${key}`, 'partnersNRIC')}
									errorMsgList={errorMsgList}
									validator={["isNRIC"]}
									hasIcon={false}
									isReadOnly={additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.isReadOnly}
									isFocus={additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.isFocus}
									onFocus={() => this.handleTextInputOnFocus(`partners${key}`, "partnersNRIC", true)}
									onBlur={() => this.handleTextInputOnFocus(`partners${key}`, "partnersNRIC", false)}
								/>
							</div>
						</div>
					</div>

					<div className="top-border bottom-border">
						<div className="fullTable confirmDetails-flexContainer">
							<div className="halfRow right-border positionRelative">
								<TextInput
									type="Phone"
									inputID={`partnersMobileNo${key}`}
									label={labels.partnersMobileNo}
									value={additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.value}
									errorMsg={additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.errorMsg}
									isValid={additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.isValid}
									onChange={(data) => this.handleOnChange(data, `partners${key}`, 'partnersMobileNo')}
									errorMsgList={errorMsgList}
									validator={["isPhoneNumber"]}
									hasIcon={true}
									isMyInfo={additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.isMyInfo}
									isFocus={additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.isFocus}
									onFocus={() => this.handleTextInputOnFocus(`partners${key}`, "partnersMobileNo", true)}
									onBlur={() => this.handleTextInputOnFocus(`partners${key}`, "partnersMobileNo", false)}
									hasEdit={additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.hasEdit}
									onlySg={false}
								/>
							</div>
							<div className="halfRow positionRelative">
								<TextInput
									inputID={`partnersEmail${key}`}
									label={labels.partnersEmail}
									value={additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.value}
									errorMsg={additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.errorMsg}
									isValid={additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.isValid}
									onChange={(data) => this.handleOnChange(data, `partners${key}`, 'partnersEmail')}
									errorMsgList={errorMsgList}
									validator={["isEmail", "maxSize|30"]}
									hasIcon={true}
									isMyInfo={additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.isMyInfo}
									isFocus={additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.isFocus}
									onFocus={() => this.handleTextInputOnFocus(`partners${key}`, "partnersEmail", true)}
									onBlur={() => this.handleTextInputOnFocus(`partners${key}`, "partnersEmail", false)}
									hasEdit={additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.hasEdit}
								/>
							</div>
						</div>
					</div>

					{maxKey === key && partnersID.length < maxPartner &&
						<SecondaryButton
							icon={addIcon}
							label={labels.addGuarantor}
							onClick={() => this.addPartners(key)}
						/>
					}
				</div>
			);
			return tmpArray;
		})
		return tmpArray;
	}

	render() {
		const { commonReducer, additionalPartnersDetailsReducer } = this.props;
		const { partnersID, hidePartnerField } = additionalPartnersDetailsReducer;
		const addtionalPartnersDetailsValue = commonReducer.appData.addtionalPartnersDetails;
		const labels = addtionalPartnersDetailsValue.labels;
		const length = partnersID.length;

		return (
			<div className="uob-content" id="additionalPartnersDetails-section">
				<h1>
					{addtionalPartnersDetailsValue.title}
				</h1>
				<p className="uob-headline" dangerouslySetInnerHTML={{__html:addtionalPartnersDetailsValue.headline}}/>

				{length > 0 && !hidePartnerField && this.renderAdditionalPartners()}
				{length === 1 && hidePartnerField &&
					<div className="noAdditionalPartnerContainer">
						<div className="noAdditionalPartnerText"> {labels.noAdditionalPartnerText} </div>
						<SecondaryButton
							icon={addIcon}
							label={labels.addGuarantor}
							onClick={() => this.addFirstPartner()}
						/>
					</div>
				}
				<div className="uob-button-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToReviewPage()} />
				</div>

				{
					!this.props.onFixedButton() &&
					<div className="uob-input-separator fixedContinue fixedContinueGray">
						<PrimaryButton label={labels.continueButton} onClick={() => this.isAdditionalPartnerCheckingPass()} isLoading={commonReducer.isProcessing} />
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, additionalPartnersDetailsReducer } = state;
	return { commonReducer, additionalPartnersDetailsReducer };
};

export default connect(mapStateToProps)(AdditionalPartnersDetails);
