import React, { Component } from "react";
import { connect } from "react-redux";

class LandingPage extends Component {
	render() {
		return (
			<div className="p-lr-2rem">
				Here is the example page, you can put any component here.
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer } = state;
	return { commonReducer };
};

export default connect(mapStateToProps)(LandingPage);
