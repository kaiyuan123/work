import React, { Component } from "react";
import { connect } from "react-redux";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";

class SessionExpirePopup extends Component {	

	handleToUOBSite() {
		this.props.onContinue();
	}

	render() {
		const { commonReducer } = this.props;
		const sessionExpiredValue = commonReducer.appData.sessionExpired;

		return (
			<div className='popup-container'>
				<div className='popup-inner'>					
					<div className="popup-content">
						<span>{sessionExpiredValue.title}</span>
						<p dangerouslySetInnerHTML={{ __html: sessionExpiredValue.email }}></p>
					</div>

					<PrimaryButton label={sessionExpiredValue.leave} onClick={() => this.handleToUOBSite()} isLoading={commonReducer.isProcessing} />
				</div>
			</div>
		)
	}

}

const mapStateToProps = state => {
    const { commonReducer } = state;
    return { commonReducer };
};

export default connect(mapStateToProps)(SessionExpirePopup);
