import React, { Component } from 'react';
import { connect } from 'react-redux';
// import errorImg from './../assets/images/errorImg.svg';
import MessageBox from './../components/MessageBox/MessageBox';
// import {detectPathName} from './../common/utils';

class ErrorMessagePage extends Component {
  componentWillMount() {
    // const { applicationReducer, commonReducer } = this.props;
    // const code = applicationReducer.errorCode;
    // const errorValue = commonReducer.appData.errorMessageBox;
    // const paramString = this.props.location.search;
    // const paraArray = paramString.split('&');
    // const parameter = paraArray.splice(1).join('&');
    // if(code === 'MYRJTD'){
    //   const url = detectPathName() + errorValue["MYRJTD"].url + "?" + parameter;
    //   window.open(url, '_parent');
    // }
  }


  render() {
    const { commonReducer, errorCode } = this.props;
    const errorValue = commonReducer.appData.errorMessageBox;
    const code = errorValue[errorCode] ? errorCode : 'serviceDown';

    return (
      <div id="loadingPage" className='uob-content'>
        <div className="error-pg-container">
          <MessageBox
            title={errorValue[code].title}
            subtitle={errorValue[code].subtitle}
          />

          {code === "AppExistSameApplicant" &&
            <div className='error-button-container'>
              <div className="error-button" onClick={() => null}>{errorValue.continueButton}</div>
            </div>}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { commonReducer } = state;
  return { commonReducer };
}

export default connect(mapStateToProps)(ErrorMessagePage);
