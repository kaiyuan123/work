import React, {Component} from 'react';
import {connect} from 'react-redux';
import { getCallback } from "./../actions/retrievingAction";
import { setMyInfoParams } from "./../actions/commonAction";
import localStore from './../common/localStore';
import { LocalEnvironment } from "./../api/httpApi";
import queryString from 'query-string';

export class RetrievePage extends Component {
  componentWillMount(){
    const { dispatch, commonReducer} = this.props;
    const parameter = this.props.location.search;
    // const pathname = localStore.getStore("productCode") ? localStore.getStore("productCode").toLowerCase() : '';
    // const isCommerical = pathname.includes("cmb", 0);
    if( commonReducer.appData !== null ) {
      const callbackURi = commonReducer.appData.callbackURi;
      dispatch(setMyInfoParams(parameter));
      this.props.history.push(LocalEnvironment ? `/` : `${callbackURi}`);
      if( parameter === "") {
        if ( commonReducer.myInfoParams === "" || commonReducer.myInfoParams === null || commonReducer.myInfoParams) {
          this.redirectToErrorPage()
        }
      }
    }
  }


  redirectToErrorPage(){
    const { commonReducer } = this.props;
    let pathname = localStore.getStore("productCode") && localStore.getStore("productCode").toLowerCase();
    const isCommerical = localStore.getStore("productCode") && pathname.includes("cmb", 0);
    const corporateUrl = commonReducer.appData.pathname_uri_commercial;
    const businessUrl = commonReducer.appData.pathname_uri_business;
    const root_path = !LocalEnvironment ? isCommerical ? corporateUrl : businessUrl : "/";
    // localStore.clear();
    this.props.history.push({
        pathname : `${root_path}error`
    })
  }

  redirectToApplication(){
    const { commonReducer, applicationReducer } = this.props;
    // let pathname = window.location.pathname;
    let pathname = localStore.getStore("productCode").toLowerCase();
    const isCommerical = pathname.includes("cmb", 0);
    // console.log(isCommerical);
		const parameterLength = (parseInt(commonReducer.appData.maxLengthParams, 10) - 1);
    const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
    const businessPathUrl = commonReducer.appData.pathname_uri_business;
    const root_path = isCommerical ? corporatePathUrl : businessPathUrl;
    const params = queryString.parse(applicationReducer.startingParameter)
    let requiredParameter= [];
    commonReducer.appData.dataElementObj.requiredParameter.map((i) => {
        const conCatString = params[i] ? `${i}=${params[i]}` : '';
        if(conCatString !== '') {
          requiredParameter.push(conCatString);
        }
        return requiredParameter;
    });
    const parameterNeed = `${requiredParameter.join('&').substring(0,parameterLength)}`;

    this.props.history.push({
        pathname : `${root_path}application`,
        search : parameterNeed
    })
  }

  componentDidUpdate(prevState) {
    const {dispatch, commonReducer} = this.props;
    if(prevState.commonReducer.myInfoParams !== commonReducer.myInfoParams){
      const myInfoParamsString = queryString.parse(commonReducer.myInfoParams);
      const error = myInfoParamsString.error ? myInfoParamsString.error : "" ;
      const errorDesc = myInfoParamsString.error_description ? myInfoParamsString.error_description : "" ;
      const pathname = localStore.getStore("productCode") ? localStore.getStore("productCode").toLowerCase() : '';
      const isCommerical = pathname.includes("cmb", 0);
      if( error !== "" && errorDesc !== "") {
        const localStorParms = localStore.getStore("params");
        const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
        const businessPathUrl = commonReducer.appData.pathname_uri_business;
        const applyPath = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : "/"
        this.props.history.push({
          pathname: `${applyPath}apply`,
          search: localStorParms
        })
      } else {
        const code = myInfoParamsString.code;
        const state = myInfoParamsString.state ? myInfoParamsString.state : "" ;
        const scope = commonReducer.appData.applyPage.attributes;
        const client_id = commonReducer.appData.applyPage.client_id;
        const productCode = localStore.getStore("productCode");
        const parameterForBE = commonReducer.appData.applyPage.parameterSendBackToBE.replace("{code}" , code).replace("{scope}", scope).replace("{client_id}", client_id).replace("{productCode}", productCode).replace("{state}", state)
        dispatch(getCallback(parameterForBE, () => this.redirectToErrorPage(), () => this.redirectToApplication()))
      }
    }
  }

  render() {
		const { commonReducer } = this.props;
		const loadingText = commonReducer.appData.loadingPage.loadingText;
    let pathname = localStore.getStore("productCode") ? localStore.getStore("productCode").toLowerCase() : "";
    const isCommerical = pathname.includes("cmb", 0);
    const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
    const businessPathUrl = commonReducer.appData.pathname_uri_business;
    const root_path = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : ".";
    let imgUrl = `${root_path}/images/logos/uob-eBiz-Logo-blue.png`;
		return (
			<div id="loadingPage">
				<img src={imgUrl} alt="Logo" className="uob-logo" width="160" />
				<div className="loadingText" > {loadingText} </div>
			</div>
		);
	}

}

const mapStateToProps = (state) => {
	const {applicationReducer, commonReducer} = state;
	return {applicationReducer, commonReducer};
}

export default connect(mapStateToProps)(RetrievePage);
