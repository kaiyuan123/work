import React, { Component } from "react";
import { connect } from "react-redux";
import TextInput from "./../components/TextInput/TextInput";
import TableView from "./../components/TableView/TableView";
import Dropdown from "./../components/Dropdown/Dropdown";
import LocalAddressInput from "./../components/Uob/LocalAddressInput/LocalAddressInput"
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import { mapValueToDescription } from "./../common/utils";
import {
	setDropdownFocusStatus,
	selectDropdownItem,
	changeSearchInputValue,
	handleTextInputChange
} from "./../actions/personalIncomeDetailsAction";
import { handleTextInputFocus } from "./../actions/commonAction";
// import moment from 'moment';

class PersonalIncomeDetailsPage extends Component {
	handleToCompanyDetails() {
		this.props.onContinue();
	}

	isPersonalDetailsPassChecking() {
		this.props.onCheck();
	}

	handleOnChange(data, field) {
		const { dispatch } = this.props;
		dispatch(handleTextInputChange(data, field));
	}

	handleDropdownBlur(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(false, field));
	}

	handleDropdownFocus(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(true, field));
	}

	handleDropdownClick(data, field) {
		const { dispatch, personalIncomeDetailsReducer } = this.props;
		if (personalIncomeDetailsReducer[field].value === data.value) {
			return;
		}
		dispatch(selectDropdownItem(data.value, data.description, field));
	}

	handleOnSearchChange(e, field) {
		const { dispatch } = this.props;
		const value = e.target.value;
		dispatch(changeSearchInputValue(value, field));
	}

	handleTextInputOnFocus(field, status) {
		const { dispatch } = this.props;
		dispatch(handleTextInputFocus(field, status));
	}

	handleTextInputOnBlur(field, status) {
		const { dispatch } = this.props;
		dispatch(handleTextInputFocus(field, status))
	}

	render() {
		const { commonReducer, personalIncomeDetailsReducer } = this.props;
		const inputValues = commonReducer.appData.inputValues
			? commonReducer.appData.inputValues
			: "";
		const { legalId, dateOfBirth, gender, maritalStatus, nationality, residentialStatus, yearlyIncome, propertyType, countryOfBirth } = personalIncomeDetailsReducer;
		const personalIncomeDetailsValue = commonReducer.appData.personalIncomeDetails;
		const labels = personalIncomeDetailsValue.labels;
		const maritalStatusList = inputValues.maritalStatus;
		const genderList = inputValues.gender;
		const residentialStatusList = inputValues.residentialStatus;
		const errorMsgList = commonReducer.appData.errorMsgs;
		const countriesNamesMap = inputValues.countriesNamesMap;
		const nationalityList = inputValues.nationality;
		const isSgOrPr = residentialStatus.value === "C" || residentialStatus.value === "P";

		return (
			<div className="uob-content" id="personalIncomeDetails-section">
				<h1>{personalIncomeDetailsValue.title}</h1>

				<div className="uob-form-separator" />

				<p className="uob-headline">
					{personalIncomeDetailsValue.headline}
				</p>

				<h1 className="sub-title">{personalIncomeDetailsValue.subtitle}</h1>
				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow right-border">
							<TextInput
								inputID="legalId"
								isReadOnly={true}
								label={labels.legalId}
								value={legalId.value}
								errorMsg={legalId.errorMsg}
								onChange={data => this.handleOnChange(data, "legalId")}
								isValid={legalId.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
							/>
						</div>
						<div className="halfRow positionRelative">
							<TextInput
								inputID="residentialStatus"
								isReadOnly={true}
								label={labels.residentialStatus}
								value={residentialStatusList[residentialStatus.value] !== undefined ? residentialStatusList[residentialStatus.value] : "-"}
								errorMsg={residentialStatus.errorMsg}
								onChange={data => this.handleOnChange(data, "residentialStatus")}
								isValid={residentialStatus.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
							/>
						</div>
					</div>
				</div>
				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow right-border">
							<TextInput
								inputID="dateOfBirth"
								isReadOnly={true}
								label={labels.dateOfBirth}
								value={dateOfBirth.value}
								errorMsg={dateOfBirth.errorMsg}
								onChange={data => this.handleOnChange(data, "dateOfBirth")}
								isValid={dateOfBirth.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
							/>
						</div>
						<div className="halfRow dropdown-nopadding">
							<Dropdown
								inputID="countryOfBirth"
								label={labels.countryOfBirth}
								focusOutItem={true}
								value={countryOfBirth.value !== "" ? countryOfBirth.value : "-"}
								dropdownItems={mapValueToDescription(
									countriesNamesMap
								)}
								isReadOnly
								flagImg
							/>
						</div>
					</div>
				</div>

				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow right-border">
							<TextInput
								inputID="gender"
								isReadOnly={true}
								label={labels.gender}
								value={genderList[gender.value]}
								errorMsg={gender.errorMsg}
								onChange={data => this.handleOnChange(data, "gender")}
								isValid={gender.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
							/>
						</div>
						<div className="halfRow positionRelative dropdown-nopadding">
							<Dropdown
								inputID="maritalStatus"
								label={labels.maritalStatus}
								dropdownItems={mapValueToDescription(
									maritalStatusList
								)}
								isFocus={maritalStatus.isFocus}
								value={maritalStatus.value}
								isValid={maritalStatus.isValid}
								errorMsg={maritalStatus.errorMsg}
								focusOutItem={true}
								searchValue={maritalStatus.searchValue}
								onBlur={this.handleDropdownBlur.bind(this, "maritalStatus")}
								onFocus={this.handleDropdownFocus.bind(this, "maritalStatus")}
								onClick={data => this.handleDropdownClick(data, "maritalStatus")}
								onSearchChange={event =>
									this.handleOnSearchChange(event, "maritalStatus")
								}
							/>
						</div>
					</div>
				</div>

				<div className="top-border bottom-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow dropdown-nopadding">
							<Dropdown
								inputID="nationality"
								label={labels.nationality}
								focusOutItem={true}
								value={nationality.value}
								dropdownItems={mapValueToDescription(
									nationalityList
								)}
								isReadOnly
								flagImg
							/>
						</div>
					</div>
				</div>
				{isSgOrPr &&
					<div>
						<h1 className="sub-title">{personalIncomeDetailsValue.residentialSubtitle}</h1>
						<LocalAddressInput
							labels={labels}
							errorMsgList={errorMsgList}
							addressType="residential"
							propertyType={propertyType.value}
							inputValues={inputValues}
						/>
					</div>
				}
				{yearlyIncome.length > 0 &&
					<div>
						<h1 className="sub-title">{labels.incomeHeader}</h1>
						<div className="uob-input-separator enableOverflowX">
							<TableView
								tableContent={yearlyIncome}
								colTitles={labels.yearlyIncomeColTitle}
								tableType={"incomeDetailsData"}
								flexTableWidth={600}
								labels={labels}
							/>
						</div>
					</div>
				}


				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToCompanyDetails()} />
				</div>

				{
					!this.props.onFixedButton() &&
					<div>
						<div className="uob-input-separator fixedContinue fixedContinueGray">
							<PrimaryButton label={labels.continueButton} onClick={() => this.isPersonalDetailsPassChecking()} />
						</div>
					</div>
				}

			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, personalIncomeDetailsReducer } = state;
	return { commonReducer, personalIncomeDetailsReducer };
};

export default connect(mapStateToProps)(PersonalIncomeDetailsPage);
