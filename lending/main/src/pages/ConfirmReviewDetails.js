import React, { Component } from "react";
import { connect } from "react-redux";
import { AsYouType } from 'libphonenumber-js';
import { capitalize } from "../common/utils";
import { scrollBackToSection, scrollToElement } from "./../actions/commonAction";
import pencilIcon from "./../assets/images/pencil-icon.svg";

export class ConfirmReviewDetails extends Component {

  handleScrollBackTosection(section) {
      const { dispatch } = this.props;
      dispatch(scrollBackToSection(section));
  }

  handleScrollBackToField(field) {
      const { dispatch } = this.props;
      dispatch(scrollToElement(field));

  }

  renderGuarantorDetails() {
    const { additionalPartnersDetailsReducer, commonReducer } = this.props;
    const addtionalPartnersDetailsValue = commonReducer.appData.addtionalPartnersDetails;
    const labels = addtionalPartnersDetailsValue.labels;
    const { partnersID } = additionalPartnersDetailsReducer;

    const formatNumber = number => {
      const formatter = new AsYouType();
      return formatter.input(number);
    };

    const country = number => {
      const formatter = new AsYouType();
      formatter.input(number);
      return formatter.country;
    };

    let tmpArray = []
    partnersID.map((key, i) => {
      const applicantNumber = i + 1;
      let countryName = country(`${additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.value}`);
      let countryFlagPath = countryName ? `./images/countriesFlags/${countryName.toLowerCase()}.svg` : null; 
      tmpArray.push(
        <div key={key}>
          <div className="review-page-header-container">
              <h1 className="sub-title-confirm">{labels.subtitle.replace("{key}", applicantNumber)}</h1>
              <img className="review-page-pencil" src={pencilIcon} alt="pencil" onClick={() => this.handleScrollBackToField( `${key}`)} />
          </div>
          <div className="top-border">
            <div className="fullTable confirmDetails-flexContainer">
              <div className="right-border halfRow">
                <div className="confirmDetails-label-container ">
                  <div className="confirmDetails-labelValue">
                    <div className="confirmDetails-LabelCSS"> {labels.partnersName} </div>
                    <div className="confirmDetails-ValueCSS"> {additionalPartnersDetailsReducer[`partners${key}`].partnersName.value !== "" &&  additionalPartnersDetailsReducer[`partners${key}`].partnersName.value !== undefined && additionalPartnersDetailsReducer[`partners${key}`].partnersName.value  ? capitalize(additionalPartnersDetailsReducer[`partners${key}`].partnersName.value) : "" }</div>
                  </div>
                </div>
              </div>
              <div className="halfRow">
                <div className="confirmDetails-label-container ">
                  <div className="confirmDetails-labelValue">
                    <div className="confirmDetails-LabelCSS"> {labels.partnersNRIC}</div>
                    <div className="confirmDetails-ValueCSS"> {additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.value} </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="top-border bottom-border">
            <div className="fullTable confirmDetails-flexContainer">
              <div className="right-border halfRow">
                <div className="confirmDetails-label-container ">
                  <div className="confirmDetails-labelValue">
                    <div className="confirmDetails-LabelCSS"> {labels.partnersMobileNo} </div>
                    <div className="confirmDetails-ValueCSS"> {countryFlagPath && <img className={"phoneInputFlag"} src={countryFlagPath} alt='flag' />}
                      {formatNumber(`${additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.value}`)} </div>
                  </div>
                </div>
              </div>
              <div className="halfRow">
                <div className="confirmDetails-label-container ">
                  <div className="confirmDetails-labelValue">
                    <div className="confirmDetails-LabelCSS"> {labels.partnersEmail}</div>
                    <div className="confirmDetails-ValueCSS"> {additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.value} </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
      return tmpArray;
    });
    return tmpArray;
  }

  renderMailingAddress() {
    const { localAddressInputReducer, commonReducer } = this.props;
    const cLabels = commonReducer.appData.companyDetails.companyBasicDetails.labels;
    const { mailingBlock, mailingLevel, mailingPostalCode, mailingStreet, mailingUnit, mailingBuilding, mailingCountry } = localAddressInputReducer;
    const inputValues = commonReducer.appData.inputValues
      ? commonReducer.appData.inputValues
      : "";
    const withBottomBorder = mailingBuilding && mailingBuilding.value !== "" ? "" : "bottom-border"
    const countriesNamesMap = inputValues.countriesNamesMap;
    let countryFlagPath = mailingCountry.value !== "" ? `./images/countriesFlags/${mailingCountry.value.toLowerCase()}.svg` : null;
    return (
      <div>
        <div className="top-border">
          <div className="fullTable confirmDetails-flexContainer">
            <div className="right-border halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS"> {cLabels.mailingCountry} </div>
                  <div className="confirmDetails-ValueCSS">
                    <img className="dropdown-flagImg" onError={(e) => e.target.style.display = 'none'} src={countryFlagPath} alt='icon' />
                    {countriesNamesMap[mailingCountry.value]}
                  </div>
                </div>
              </div>
            </div>
            <div className="halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS"> {cLabels.mailingPostalCode}</div>
                  <div className="confirmDetails-ValueCSS"> {mailingPostalCode.value} </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="top-border">
          <div className="fullTable confirmDetails-flexContainer">
            <div className="right-border halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS"> {cLabels.mailingBlock} </div>
                  <div className="confirmDetails-ValueCSS"> {mailingBlock.value} </div>
                </div>
              </div>
            </div>
            <div className="halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS"> {cLabels.mailingStreet}</div>
                  <div className="confirmDetails-ValueCSS"> {mailingStreet.value} </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className={`top-border ${withBottomBorder}`}>
          <div className="fullTable confirmDetails-flexContainer">
            <div className="right-border halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS"> {cLabels.mailingLevel} </div>
                  <div className="confirmDetails-ValueCSS"> {mailingLevel.value} </div>
                </div>
              </div>
            </div>
            <div className="halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS"> {cLabels.mailingUnit}</div>
                  <div className="confirmDetails-ValueCSS"> {mailingUnit.value} </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {mailingBuilding && mailingBuilding.value !== "" &&
          <div className="sub-div-common display-flex bottom-border">
            <div className="p-l-10">
              <div className="confirmDetails-label-container ">
                <div>
                  <div className="confirmDetails-LabelCSS">
                    {cLabels.mailingBuilding}
                  </div>
                  <div className="confirmDetails-ValueCSS">
                    <div>{mailingBuilding.value}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        }

      </div>
    )
  }

  renderOverseaMailingAddress() {
    const { localAddressInputReducer, commonReducer } = this.props;
    const cLabels = commonReducer.appData.companyDetails.companyBasicDetails.labels;
    const { mailingCity, mailingLine1, mailingLine3, mailingLine4, mailingLine2, mailingCountry } = localAddressInputReducer;
    const inputValues = commonReducer.appData.inputValues
      ? commonReducer.appData.inputValues
      : "";
    const countriesNamesMap = inputValues.countriesNamesMap;
    let countryFlagPath = mailingCountry.value !== "" ? `./images/countriesFlags/${mailingCountry.value.toLowerCase()}.svg` : null;

    return (
      <div>
        <div className="top-border bottom-border">
          <div className="fullTable confirmDetails-flexContainer">
            <div className="right-border halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS"> {cLabels.mailingCountry} </div>
                  <div className="confirmDetails-ValueCSS">
                    <img className="dropdown-flagImg" onError={(e) => e.target.style.display = 'none'} src={countryFlagPath} alt='icon' />
                    {countriesNamesMap[mailingCountry.value]}
                  </div>
                </div>
              </div>
            </div>
            <div className="halfRow">
              <div className="confirmDetails-label-container ">
                <div className="confirmDetails-labelValue">
                  <div className="confirmDetails-LabelCSS"> {cLabels.mailingCity}</div>
                  <div className="confirmDetails-ValueCSS"> {mailingCity.value} </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {mailingLine1 && mailingLine1.value !== "" &&
          <div className="sub-div-common-no-top display-flex bottom-border">
            <div className="p-l-10">
              <div className="confirmDetails-label-container ">
                <div>
                  <div className="confirmDetails-LabelCSS">
                    {cLabels.mailingLine1}
                  </div>
                  <div className="confirmDetails-ValueCSS">
                    <div>{mailingLine1.value}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        }
        {mailingLine2 && mailingLine2.value !== "" &&
          <div className="sub-div-common-no-top display-flex bottom-border">
            <div className="p-l-10">
              <div className="confirmDetails-label-container ">
                <div>
                  <div className="confirmDetails-LabelCSS">
                    {cLabels.mailingLine2}
                  </div>
                  <div className="confirmDetails-ValueCSS">
                    <div>{mailingLine2.value}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        }
        {mailingLine3 && mailingLine3.value !== "" &&
          <div className="sub-div-common-no-top display-flex bottom-border">
            <div className="p-l-10">
              <div className="confirmDetails-label-container ">
                <div>
                  <div className="confirmDetails-LabelCSS">
                    {cLabels.mailingLine3}
                  </div>
                  <div className="confirmDetails-ValueCSS">
                    <div>{mailingLine3.value}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        }
        {mailingLine4 && mailingLine4.value !== "" &&
          <div className="sub-div-common-no-top display-flex bottom-border">
            <div className="p-l-10">
              <div className="confirmDetails-label-container ">
                <div>
                  <div className="confirmDetails-LabelCSS">
                    {cLabels.mailingLine4}
                  </div>
                  <div className="confirmDetails-ValueCSS">
                    <div>{mailingLine4.value}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        }

      </div>
    )
  }

  render() {
    const { commonReducer, personalIncomeDetailsReducer, additionalPartnersDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer, contactDetailsReducer } = this.props;
    const { emailAddress, isCommerical, annualTurnover, requestLoan } = contactDetailsReducer;
    const { maritalStatus } = personalIncomeDetailsReducer;
    const { partnersID, hidePartnerField } = additionalPartnersDetailsReducer;
    const { mailingAddressCheckbox } = companyBasicDetailsReducer;
    const { mailingCountry } = localAddressInputReducer;
    const confirmDetailsValue = commonReducer.appData.confirmDetails.ConfirmReviewDetails;
    const personalIncomeDetailsValue = commonReducer.appData.personalIncomeDetails;
    const inputValues = commonReducer.appData.inputValues
      ? commonReducer.appData.inputValues
      : "";
    const maritalStatusList = inputValues.maritalStatus;
    const annualTurnoverList = inputValues.annualTurnoverType;
    const requestLoanList = inputValues.requestLoanType;
    const pLabels = personalIncomeDetailsValue.labels;
    const cLabels = commonReducer.appData.contactDetails.labels;
    // const isSgOrPr = residentialStatus.value === 'C' || residentialStatus.value === 'P';
    // const withBottomBorderApplicant = isSgOrPr ? "bottom-border" : "";
    return (
      <div className="reviewContent">
        <h1 className="review-title">{confirmDetailsValue.title}</h1>
        <div className="review-page-header-container">
            <h1 className="desc review-subtite">{confirmDetailsValue.subtitle.basicDetails}</h1>
        </div>
        {isCommerical &&
          <div>
            <div className="top-border">
              <div className="fullTable confirmDetails-flexContainer">
                <div className="right-border halfRow">
                  <div className="confirmDetails-label-container ">
                    <div className="confirmDetails-labelValue">
                      <div className="confirmDetails-LabelCSS"> {cLabels.requestLoan}  </div>
                      <div className="confirmDetails-ValueCSS"> {requestLoanList[requestLoan.value]} </div>
                    </div>
                  </div>
                </div>
                <div className="halfRow">
                  <div className="confirmDetails-label-container ">
                    <div className="confirmDetails-labelValue">
                      <div className="confirmDetails-LabelCSS"> {cLabels.annualTurnover} </div>
                      <div className="confirmDetails-ValueCSS"> {annualTurnoverList[annualTurnover.value]} </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        }
        <div className={`sub-div-common display-flex bottom-border`}>
          <div className="p-l-10">
            <div className="confirmDetails-label-container ">
              <div className="confirmDetails-LabelCSS">
                {cLabels.emailAddress}
              </div>
              <div className="confirmDetails-ValueCSS">
                <div>{emailAddress.value}</div>
              </div>
            </div>
          </div>
        </div>
        <div className="review-page-header-container">
            <h1 className="desc review-subtite">{confirmDetailsValue.subtitle.applicantDetails}</h1>
            <img className="review-page-pencil" src={pencilIcon} alt="pencil" onClick={() => this.handleScrollBackTosection("personalIncomeDetails")} />
        </div>
        <div className={`sub-div-common display-flex bottom-border`}>
            <div className="p-l-10">
                <div className="confirmDetails-label-container ">
                    <div className="confirmDetails-LabelCSS">
                        {pLabels.maritalStatus}
                    </div>
                    <div className="confirmDetails-ValueCSS">
                        <div>{maritalStatusList[maritalStatus.value]}</div>
                    </div>
                </div>
            </div>
        </div>
        {mailingAddressCheckbox.isToggled &&
          <div>
            <h1 className="desc desc-confirm">{confirmDetailsValue.subtitle.companyDetails}</h1>
              <div>
                <div className="review-page-header-container">
                    <h1 className="sub-title-confirm">{confirmDetailsValue.subtitle.companyMailingAddress}</h1>
                    <img className="review-page-pencil" src={pencilIcon} alt="pencil" onClick={() => this.handleScrollBackTosection("mailingDetails")} />
                </div>
                {mailingCountry.value !== "SG" ? this.renderOverseaMailingAddress() : this.renderMailingAddress()}
              </div>
          </div>
        }

        {partnersID.length > 0 && !hidePartnerField &&
          <div>
            <h1 className="desc desc-confirm">{confirmDetailsValue.subtitle.guarantorDetails}</h1>
            {this.renderGuarantorDetails()}
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { commonReducer, contactDetailsReducer, personalIncomeDetailsReducer, additionalPartnersDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer } = state;
  return { commonReducer, contactDetailsReducer, personalIncomeDetailsReducer, additionalPartnersDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer };
};

export default connect(mapStateToProps)(ConfirmReviewDetails);
