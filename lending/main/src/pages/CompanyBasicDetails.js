import React, { Component } from "react";
import { connect } from "react-redux";
import { capitalize, mapValueToDescription } from "./../common/utils";
import moment from 'moment';
import ToggleInputWithoutTitle from "./../components/ToggleInput/ToggleInputWithoutTitle/ToggleInputWithoutTitle";
import Dropdown from "./../components/Dropdown/Dropdown";
import { handleIsChecked, setDropdownFocusStatus, selectDropdownItem, handleTextInputChange, changeSearchInputValue } from "./../actions/companyBasicDetailsAction";
import LocalAddressInput from "./../components/Uob/LocalAddressInput/LocalAddressInput";
class CompanyBasicDetails extends Component {


    handleOnChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChange(data, field));
    }

    handleOnCheckbox(field, isToggled) {
        const { dispatch } = this.props;
        dispatch(handleIsChecked(field, isToggled));
    }

    handleDropdownBlur(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(false, field));
    }

    handleDropdownFocus(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(true, field));
    }

    handleDropdownClick(data, field) {
        const { dispatch, companyBasicDetailsReducer } = this.props;

        if (companyBasicDetailsReducer[field].value === data.value) {
            return;
        }

        dispatch(selectDropdownItem(data.value, field, data.description));
    }

    handleOnSearchChange(e, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValue(value, field));
    }

    render() {
        const { commonReducer, companyBasicDetailsReducer } = this.props;
        const { companyRegistrationNumber, registeredCompanyName, companyStatus, countryOfIncorporation, ownership, registrationDate, companyExpiryDate, primaryActivityDesc, secondaryActivityDesc, mailingAddressCheckbox, businessConstitution, entityType, companyType } = companyBasicDetailsReducer;

        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";

        const companyDetailsValues = commonReducer.appData.companyDetails;
        const companyBasicDetailsValues = companyDetailsValues.companyBasicDetails;
        const labels = companyBasicDetailsValues.labels;

        const errorMsgList = commonReducer.appData.errorMsgs;
        const countriesNamesMap = inputValues.countriesNamesMap;
        const ownershipList = inputValues.ownership;

        const countryOfIncorporationValue = countryOfIncorporation.value !== "" ? countryOfIncorporation.value : "SG";
        const ownershipValue = ownership.value !== "" ? ownership.value : "NA";
        const mailingChecked = mailingAddressCheckbox.isToggled ? "" : "darkGrey";
        const registrationDateFormat = registrationDate.value !== "" ? moment(registrationDate.value, "YYYY-MM-DD").format("DD/MM/YYYY") : "-";
        const companyExpiryDateFormat = companyExpiryDate.value !== "" && moment(companyExpiryDate.value, "YYYY-MM-DD").isValid() ? moment(companyExpiryDate.value, "YYYY-MM-DD").format("DD/MM/YYYY") : "-";

        const entityTypeList = inputValues.entityType;
        const businessConstitutionList = inputValues.businessConstitution;
        const companyTypeList = inputValues.companyType;
        const entityTypeListDisplay = entityType.value === "BN" ? businessConstitutionList : entityTypeList;

    		const checkBNorC = entityType.value === "BN" || companyType.value !== "";
        return (
            <div>
                <h1>{companyDetailsValues.title}</h1>
                <div className="uob-form-separator" />
                <p className="uob-headline">{companyDetailsValues.headline}</p>
                <h1 className="sub-title">{companyBasicDetailsValues.subtitle}</h1>

                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyRegistrationNumber}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {companyRegistrationNumber.value}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.registeredCompanyName}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {registeredCompanyName.value}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {checkBNorC ? (
                  <div>
                    <div className="top-border">
                      <div className="fullTable confirmDetails-flexContainer">
                        <div className={`halfRow right-border dropdown-nopadding`}>
                          <Dropdown
                            inputID="entityType"
                            label={labels.entityType}
                            isFocus={entityType.isFocus}
                            value={entityType.value}
                            isReadOnly={true}
                            focusOutItem={true}
                            dropdownItems={mapValueToDescription(
                              entityTypeList
                            )}
                          />
                        </div>
                        <div className={`halfRow dropdown-nopadding`}>
                          {checkBNorC && companyType.value !== "" ?
                            (
                              <Dropdown
                                inputID={"companyType"}
                                label={labels.companyType}
                                value={companyType.value}
                                isFocus={companyType.isFocus}
                                errorMsg={companyType.errorMsg}
                                dropdownItems={mapValueToDescription(
                                  companyTypeList
                                )}
                                focusOutItem={true}
                                isDisabled={true}
                                isReadOnly={true}
                              />
                            ) : (
                              <Dropdown
                                inputID={"businessConstitution"}
                                label={labels.businessConstitution}
                                value={businessConstitution.value}
                                isFocus={businessConstitution.isFocus}
                                errorMsg={businessConstitution.errorMsg}
                                dropdownItems={mapValueToDescription(
                                  businessConstitutionList
                                )}
                                focusOutItem={true}
                                isDisabled={true}
                                isReadOnly={true}
                              />
                            )
                          }
                        </div>
                      </div>
                    </div>
                    <div className="sub-div-common alignCenter top-border ">
                            <div className="p-l-10">
                                <div className="confirmDetails-label-container ">
                                    <div className="confirmDetails-labelValue">
                                        <div className="confirmDetails-LabelCSS">
                                            {labels.companyStatus}
                                        </div>
                                        <div className="confirmDetails-ValueCSS">
                                            {capitalize(companyStatus.value)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                  </div>
                ) : (
                  <div className="top-border">
                         <div className="fullTable confirmDetails-flexContainer">
                             <div className="right-border halfRow dropdown-nopadding">
                                 <Dropdown
                                     inputID="entityType"
                                     label={labels.entityType}
                                     isFocus={entityType.isFocus}
                                     value={entityType.value === "BN" ? businessConstitution.value : entityType.value}
                                     isReadOnly={true}
                                     focusOutItem={true}
                                     dropdownItems={mapValueToDescription(
                                         entityTypeListDisplay
                                     )}
                                 />
                             </div>
                             <div className="halfRow">
                                 <div className="confirmDetails-label-container ">
                                     <div className="confirmDetails-labelValue">
                                         <div className="confirmDetails-LabelCSS">
                                             {labels.companyStatus}
                                         </div>
                                         <div className="confirmDetails-ValueCSS">
                                             {capitalize(companyStatus.value)}
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                  )
                }
                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.countryOfIncorporation}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {countriesNamesMap[countryOfIncorporationValue]}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.ownership}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {ownershipList[ownershipValue]}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.registrationDate}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {registrationDateFormat}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyExpiryDate}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {companyExpiryDateFormat}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="sub-div-common alignCenter top-border ">
                    <div className="p-l-10">
                        <div className="confirmDetails-label-container ">
                            <div className="confirmDetails-labelValue">
                                <div className="confirmDetails-LabelCSS">
                                    {labels.primaryActivityDesc}
                                </div>
                                <div className="confirmDetails-ValueCSS">
                                    {primaryActivityDesc.value !== "" && primaryActivityDesc.value !== null ? primaryActivityDesc.value : "-"}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="sub-div-common alignCenter top-border bottom-border">
                    <div className="p-l-10">
                        <div className="confirmDetails-label-container ">
                            <div className="confirmDetails-labelValue">
                                <div className="confirmDetails-LabelCSS">
                                    {labels.secondaryActivityDesc}
                                </div>
                                <div className="confirmDetails-ValueCSS">
                                    {secondaryActivityDesc.value !== "" && secondaryActivityDesc.value !== null ? secondaryActivityDesc.value : "-"}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h1 className="sub-title" id="mailingDetails-section">{companyBasicDetailsValues.subtitle2}</h1>
                <LocalAddressInput
                    labels={labels}
                    errorMsgList={errorMsgList}
                    addressType="company"
                    inputValues={inputValues}
                />
                <div className="flexDisplay" id="mailingAddressCheckbox">
                    <h1 className={`sub-title ${mailingChecked}`}>{companyBasicDetailsValues.mailingSubtitle}</h1>
                    <ToggleInputWithoutTitle
                        inputID="mailingAddressCheckbox"
                        isToggled={mailingAddressCheckbox.isToggled}
                        onClick={() => this.handleOnCheckbox('mailingAddressCheckbox', mailingAddressCheckbox.isToggled)}
                    />
                </div>
                {mailingAddressCheckbox.isToggled &&
                    <div className="mailingDetails-section">
                        <LocalAddressInput
                            labels={labels}
                            errorMsgList={errorMsgList}
                            addressType="mailing"
                            inputValues={inputValues}
                        />
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, companyBasicDetailsReducer, localAddressInputReducer } = state;
    return { commonReducer, companyBasicDetailsReducer, localAddressInputReducer };
};

export default connect(mapStateToProps)(CompanyBasicDetails);
