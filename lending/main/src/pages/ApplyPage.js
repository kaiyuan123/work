import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setErrorWithValue } from "./../actions/applicationAction";
import { setParamterToLocalStorage } from "./../actions/applyAction";
import { setErrorMessage, setCompatibilityErrorMessage } from "./../actions/commonAction";
import Loader from "./../components/Loader/Loader";
import queryString from 'query-string';
import GenericErrorMessage from "./../components/GenericErrorMessage/GenericErrorMessage";
import CompatibilityErrorMessage from "./../components/CompatibilityErrorMessage/CompatibilityErrorMessage";
import { LocalEnvironment } from "./../api/httpApi";
import { sendDataToSparkline } from "./../common/utils";

export class ApplyPage extends Component {
	componentWillMount() {
		const { dispatch, commonReducer } = this.props;
		const parameter = window.location.search;
    let pathname = window.location.pathname;
		const params = queryString.parse(parameter);
		const productId = commonReducer.appData.productCode;
		// const parameterLength = parseInt(commonReducer.appData.maxLengthParams, 10);
		const code = params[productId] ? params[productId].toLowerCase() : '';
		const isCommerical = code.includes("cmb", 0);
		const isBusinessPathname = pathname.includes("business");
		const isCommercialPathname = pathname.includes("corporate");

		let validProductCode = false;
		if ( commonReducer.appData.validProductCode.indexOf(code) >= 0 ) {
			if ( !LocalEnvironment && isBusinessPathname ) {
				validProductCode = code.includes("bb", 0);
			} else if (isCommercialPathname) {
				validProductCode = code.includes("cmb", 0);
			} else if (LocalEnvironment) {
				validProductCode = true;
			}
		}

		if (code !== "" && validProductCode) {
			dispatch(setParamterToLocalStorage(parameter));
			const dataElement = commonReducer.appData.dataElementObj;
			sendDataToSparkline(dataElement, code, "", true);
			let count = 0;
      let countMobile = 0;
      commonReducer.appData.supportOSMobile.map((i) => {
        const str = window.navigator.userAgent.toString();
        if (str.indexOf(i) >= 0) {
          countMobile++;
        }
        return countMobile
      });

			commonReducer.appData.supportOS.map((i) => {
				const str = window.navigator.userAgent.toString();
				if (str.indexOf(i) < 0) {
					count++;
				}
				return count
			});

      if (countMobile > 0) {
        // dispatch(setCompatibilityErrorMessage(commonReducer.appData.supportOSTextMobile))
      } else if (countMobile === 0 && count > 0) {
				dispatch(setCompatibilityErrorMessage(commonReducer.appData.supportOSText))
			}

		} else {
			const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
			const businessPathUrl = commonReducer.appData.pathname_uri_business;
			const applyPath = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : "/";
			dispatch(setErrorWithValue("InvalidProductCode"))
			this.props.history.push({
				pathname: `${applyPath}error`,
			})
		}
	}

	handleToMyInfo() {
		const { commonReducer } = this.props;
		const jsonUrl = commonReducer.appData.applyPage.mockServiceURL;
		const realUrl = commonReducer.appData.applyPage.url;
		let pathname = window.location.pathname;
		const splitPathName = pathname.split("/");
		const isCommercial = splitPathName.indexOf("corporate") === 1;
		const redirectURL = isCommercial ? encodeURIComponent(commonReducer.appData.applyPage.redirect_uri_commercial) : encodeURIComponent(commonReducer.appData.applyPage.redirect_uri_business)
		const pathnameURL = isCommercial ? commonReducer.appData.pathname_uri_commercial : commonReducer.appData.pathname_uri_business;
		const redirect_uri = LocalEnvironment ? encodeURIComponent(commonReducer.appData.applyPage.redirect_uri) : redirectURL;
		const pathname_uri = LocalEnvironment ? commonReducer.appData.applyPage.redirect_uri : pathnameURL

		const url = commonReducer.appData.isMockService ? jsonUrl.replace("{redirect_uri}", redirect_uri).replace("{pathname}", pathname_uri) : realUrl;
		window.open(url, '_parent');
	}

	handleToWithoutMyinfo() {
		const { commonReducer } = this.props;
		const productId = commonReducer.appData.productCode;
		const params = queryString.parse(this.props.location.search);
		const productCode = params[productId] ? params[productId] : '';
		const parameterLength = (parseInt(commonReducer.appData.maxLengthParams, 10) - 1);
		let requiredParameter= [];
		commonReducer.appData.dataElementObj.requiredParameter.map((i) => {
				const conCatString = params[i] ? `${i}=${params[i]}` : '';
				if(conCatString !== '') {
					requiredParameter.push(conCatString);
				}
				return requiredParameter;
		});
		const parameterNeed = requiredParameter.join('&').substring(0,parameterLength);

		const url = `${commonReducer.appData.applyPage.withoutMyInfoURL[productCode.toUpperCase()]}?${parameterNeed}`;
		window.open(url, '_parent');
	}

	handleOnClearMessage() {
		const { dispatch } = this.props;
		dispatch(setErrorMessage(""));
	}

	handleOnClearCompatibilityMessage() {
		const { dispatch } = this.props;
		dispatch(setCompatibilityErrorMessage(""));
	}

	render() {
		const { commonReducer } = this.props;
		const parameter = window.location.search;
		const params = queryString.parse(parameter);
		const productId = commonReducer.appData.productCode;
		const code = params[productId] ? params[productId].toUpperCase() : 'BB-BPL';
		const productTitle = commonReducer.appData !== null && commonReducer.appData.productTitle[code];
		const withMyinfoLabels = commonReducer.appData.applyPage.withMyinfo;
		const withoutMyInfoLabels = commonReducer.appData.applyPage.withoutMyInfo;

		const withoutDesc = withoutMyInfoLabels.description === "" ? "body-right-button-withoutDesc" : "";
		const withoutErrorMsg = commonReducer.compatibilityMessage === "" ? "apply-page-body-container-padding" : "";
		return (
			<div className="apply-page-container">
				{commonReducer.isLoading && <Loader />}
				<style dangerouslySetInnerHTML={{
					__html: `
				body { padding: 0;  background: url(./images/applyPageImages/background.png); background-size: cover;
				background-repeat: no-repeat;
				background-position: 100%;
				height: 100%; width: 100%;}
				.uob-form-loan-container { background: transparent;}
				.uob-content { padding: 0; }
				#sideNav { display: none }
				.logoRight { display: block!important;  }
				#home .title2 { display: none }
				section.resume-section { padding: 32px 35px 0!important;}
				.blue-uob-logo {padding: 26px 40px}
				.apply-page-tick {
					list-style-image: url('./images/applyPageImages/tick_list.svg');
					margin: 0px
				}
				`
				}} />
				{commonReducer.appData !== null &&
					<div>
						<div className='apply-page-header-container'>
							<div className="apply-page-header-title">
								<img alt="logo" className="img-size" src='./images/logos/uob-eBiz-Logo-blue.svg' />
								<div className="title-text"> {productTitle} </div>
							</div>
							<div className="displayHeader">
								<img className="img-size" alt="logo" src='./images/logos/uob-eBiz-Logo-blue.svg' />
								<div className="title-vertical-line"/>
								<div className="mob-prodtitle">
									<span className="font-14">{productTitle.split(" | ")[0]}</span>
									<br/>
									{productTitle.split(" | ")[1]}
								</div>
							</div>
							<div className="right-by-you-container">
								<img alt="right-by-you" src='./images/logos/right-by-you-logo.png' />
							</div>
						</div>
						{commonReducer.compatibilityMessage !== "" && (
							<CompatibilityErrorMessage
								{...this.props}
								interval={120}
								messageContent={commonReducer.compatibilityMessage}
								onClearMessage={this.handleOnClearCompatibilityMessage.bind(this)}
							/>
						)}
						<div className={`apply-page-body-container ${withoutErrorMsg}`}>
							<div className="apply-page-body-left">
								<div className="body-left-content">
									<div className="body-left-title" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.title }} />
									<div className="body-left-subtite">
										<div className="body-left-subtite-img">
											<img alt="logo" src='./images/applyPageImages/clock.svg' />
										</div>
										<div className="body-left-subtite-text" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.subtite }} />
									</div>
									<div className="body-left-description" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.description }} />
									<div className='body-left-button-container'>
										<div className="body-left-button" onClick={() => this.handleToMyInfo()}>{withMyinfoLabels.buttonText}</div>
									</div>
								</div>
								<div className="body-left-footer">
									<div className="body-left-footer-image" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.footerImage }} />
									<div className="body-left-footer-text" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.footerDesc }}>
									</div>
								</div>
							</div>


							<div className="apply-page-body-separater" />
							<div className="apply-page-body-right">
								<div className="body-right-content">
									<div className="body-right-title" dangerouslySetInnerHTML={{ __html: withoutMyInfoLabels.title }} />
									<div className="body-right-subtite">
										<div className="body-right-subtite-img">
											<img alt="logo" src='./images/applyPageImages/bookingIcon.svg' />
										</div>
										<div className="body-right-subtite-text" dangerouslySetInnerHTML={{ __html: withoutMyInfoLabels.subtite }} />
									</div>
									{withoutMyInfoLabels.description !== "" &&
										<div className="body-right-description" dangerouslySetInnerHTML={{ __html: withoutMyInfoLabels.description }} />
									}
									<div className={`body-right-button-container ${withoutDesc}`}>
										<div className="body-right-button" onClick={() => this.handleToWithoutMyinfo()}>{withoutMyInfoLabels.buttonText}</div>
									</div>
								</div>
								<div className="body-right-footer">
									{withoutMyInfoLabels.footerImage !== "" &&
										<div className="body-right-footer-image" dangerouslySetInnerHTML={{ __html: withoutMyInfoLabels.footerImage }} />
									}
									{withoutMyInfoLabels.footerDesc !== "" &&
										<div className="body-right-footer-text" dangerouslySetInnerHTML={{ __html: withoutMyInfoLabels.footerDesc }} />
									}
								</div>
							</div>
						</div>
					</div>
				}

				{commonReducer.messageContent !== "" && (
					<GenericErrorMessage
						{...this.props}
						interval={120}
						messageContent={commonReducer.messageContent}
						onClearMessage={this.handleOnClearMessage.bind(this)}
					/>
				)}
			</div>

		);

	}

}

const mapStateToProps = (state) => {
	const { applyReducer, commonReducer, verifyReducer } = state;
	return { applyReducer, commonReducer, verifyReducer };
}

export default connect(mapStateToProps)(ApplyPage);
