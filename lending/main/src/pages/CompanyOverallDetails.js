import React, { Component } from "react";
import { connect } from "react-redux";
import TableViewCompany from "../components/TableView/TableViewCompany/TableViewCompany";
import TableViewColumn from "../components/TableView/TableViewColumn/TableViewColumn";
import moment from "moment";

class CompanyOverallDetails extends Component {

	render() {
		const { commonReducer, companyOverallDetailsReducer } = this.props;
		const { previousNames, previousUens, capitals, appointments, shareholders, financials, grants } = companyOverallDetailsReducer;

		const inputValues = commonReducer.appData.inputValues
			? commonReducer.appData.inputValues
			: "";
		const companyCapitalTypeList = inputValues.capitalType;
		const companyDetailsValues = commonReducer.appData.companyDetails;
		const companyOverallDetailsValues = companyDetailsValues.companyOverallDetails;
		const labels = companyOverallDetailsValues.labels;

		//Financials Table Title
		const financialsColTitle = ["Type"];

		//Check if the first year exists
		const firstStartDate = financials.value[0] && financials.value[0].currentPeriodStartDate ? financials.value[0].currentPeriodStartDate : ""
		const firstEndDate = financials.value[0] && financials.value[0].currentPeriodEndDate ? financials.value[0].currentPeriodEndDate : "";

		const financialFirstYearFormatted = moment(firstStartDate, "YYYY-MM-DD").format("DD/MM/YYYY") + " - " + moment(firstEndDate, "YYYY-MM-DD").format("DD/MM/YYYY");

		//Check if the second year exists
		const secondStartDate = financials.value[1] && financials.value[1].currentPeriodStartDate ? financials.value[1].currentPeriodStartDate : ""
		const secondEndDate = financials.value[1] && financials.value[1].currentPeriodEndDate ? financials.value[1].currentPeriodEndDate : "";

		const financialSecondYearFormatted = moment(secondStartDate, "YYYY-MM-DD").format("DD/MM/YYYY") + " - " + moment(secondEndDate, "YYYY-MM-DD").format("DD/MM/YYYY");

		if (firstStartDate !== "" && firstEndDate !== "") {
			financialsColTitle.push(financialFirstYearFormatted);
		}

		if (secondStartDate !== "" && secondEndDate !== "") {
			financialsColTitle.push(financialSecondYearFormatted);
		}

		return (
			<div>
				<div>
					{previousUens.value && previousNames.value && previousNames.value.length > 0 && previousUens.value.length > 0 &&
						<div>
							<h1 className="sub-title">
								{companyOverallDetailsValues.subtitle}
							</h1>
							<div className="uob-input-separator">
								<TableViewCompany
									tableContent={previousNames.value}
									colTitles={labels.previousCompanyColTitle}
									tableType={"previousCompanyData"}
									mapItems={previousUens.value}
								/>
							</div>
						</div>}
					{capitals.value.length > 0 &&
						<div>
							<h1 className="sub-title">{companyOverallDetailsValues.subtitle2}</h1>
							<div className="uob-input-separator enableOverflowX">
								<TableViewCompany
									tableType={'companyCapitalsData'}
									colTitles={labels.capitalDetailsColTitle}
									tableContent={capitals.value}
									mapItems={companyCapitalTypeList}
									flexTableWidth={700}
								/>
							</div>
						</div>}
					{appointments.value.length > 0 &&
						<div>
							<h1 className="sub-title">
								{companyOverallDetailsValues.subtitle3}
							</h1>
							<div className="uob-input-separator enableOverflowX">
								<TableViewCompany
									colTitles={labels.appointmentDetailsColTitle}
									tableType={'appointmentsData'}
									tableContent={appointments.value}
									mapItems={inputValues}
									flexTableWidth={900}
								/>
							</div>
						</div>}
					{shareholders.value.length > 0 &&
						<div>
							<h1 className="sub-title">
								{companyOverallDetailsValues.subtitle4}
							</h1>
							<div className="uob-input-separator enableOverflowX">
								<TableViewCompany
									colTitles={labels.shareholdersColTitle}
									tableType={'shareholdersData'}
									tableContent={shareholders.value}
									mapItems={inputValues}
									flexTableWidth={900}
								/>
							</div>
						</div>}
					{financials.value.length > 0 &&
						<div>
							<h1 className="sub-title">{companyOverallDetailsValues.subtitle5}</h1>
							<div className="uob-input-separator enableOverflowX">
								<TableViewColumn
									colTitles={financialsColTitle}
									tableType={"financialsData"}
									tableContent={financials.value}
									mapItems={labels}
									financialRowTitle={labels.financialRowTitle}
									flexTableWidth={700}
								/>
							</div>
						</div>}
					{grants.value.length > 0 &&
						<div>
							<h1 className="sub-title">{companyOverallDetailsValues.subtitle6}</h1>
							<div className="uob-input-separator enableOverflowX">
								<TableViewCompany
									colTitles={labels.grantsColTitle}
									tableType={"grantsData"}
									tableContent={grants.value}
									mapItems={inputValues}
									flexTableWidth={1100}
								/>
							</div>
						</div>}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, companyOverallDetailsReducer } = state;
	return { commonReducer, companyOverallDetailsReducer };
};

export default connect(mapStateToProps)(CompanyOverallDetails);
