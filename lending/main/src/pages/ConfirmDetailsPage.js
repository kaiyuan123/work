//  Confirm Review details (display all the details)
//  Confirm T and C (agreement)
import React, { Component } from "react";
import { connect } from "react-redux";
import ConfirmReviewDetails from "./ConfirmReviewDetails";
import ConfirmTandC from "./ConfirmTandC";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";

class ConfirmDetailsPage extends Component {
	handleToThankYouPage() {
		this.props.onContinue();
	}

	addtionalPartnersDetailsValid(){
		const { additionalPartnersDetailsReducer } = this.props;
		const { hidePartnerField , partnersID } = additionalPartnersDetailsReducer;

		let errorCount = 0;
		if(!hidePartnerField && partnersID.length > 0) {
			additionalPartnersDetailsReducer.partnersID.map((key) => {
				const partnersEmail = additionalPartnersDetailsReducer[`partners${key}`].partnersEmail;
				const partnersNRIC = additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC;
				const partnersName =  additionalPartnersDetailsReducer[`partners${key}`].partnersName;
				const partnersMobileNo = additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo;

				const isAdditionalPartnerValid = partnersEmail.value !== "" && partnersEmail.isValid  && partnersNRIC.value !== "" && partnersNRIC.isValid  && partnersName.value !== "" && partnersName.isValid  && partnersMobileNo.value !== "" && partnersMobileNo.isValid ;
				if(!isAdditionalPartnerValid) {
					errorCount++
				}
				return errorCount;
			})
			return errorCount;
		}
	}

	checkCompanyBasicDetailValid(){
		const { companyBasicDetailsReducer, localAddressInputReducer } = this.props;
		const { mailingAddressCheckbox, overseasAddress1, mailingCity, mailingCountry} = companyBasicDetailsReducer;
		const { mailingBlock, mailingLevel, mailingPostalCode, mailingStreet, mailingUnit } = localAddressInputReducer;
		let errorCount = 0;

		if(mailingAddressCheckbox.isToggled) {
			if(mailingCountry.value !== "SG"){
				if(mailingCity.value === "" || !mailingCity.isValid || overseasAddress1.value === "" || !overseasAddress1.isValid ){
					errorCount++;
				}
			} else {
				if(mailingBlock.value === "" ||
					 mailingLevel.value === ""  ||
					 mailingPostalCode.value === "" ||
					 mailingStreet.value === ""  ||
					 mailingUnit.value === "" ||
					 !mailingBlock.isValid ||
					 !mailingLevel.isValid ||
					 !mailingPostalCode.isValid ||
					 !mailingStreet.isValid  ||
					 !mailingUnit.isValid) {
						 errorCount++
					 }
			}
		}
		return errorCount;
	}


	checkAllValidation(){
		const { confirmTandCReducer, contactDetailsReducer } = this.props;
		const { acknowledgementCheckbox } = confirmTandCReducer;
		const { emailAddress, annualTurnover, requestLoan , isCommerical } = contactDetailsReducer;
		const contactFieldValid = emailAddress.isValid && emailAddress.value !== '';
		const loanVaild = annualTurnover.isValid && annualTurnover.value !== '' && requestLoan.isValid && requestLoan.value !== '';
		const fullVaildation = isCommerical ? contactFieldValid && loanVaild : contactFieldValid;
		if(!acknowledgementCheckbox.isToggled || this.addtionalPartnersDetailsValid() > 0 || this.checkCompanyBasicDetailValid() > 0 || !fullVaildation){
			return false;
		}
		return true;
	}

	render() {
		const {commonReducer} = this.props;
		const labels = commonReducer.appData.confirmDetails.confirmTandC.labels;

		return (
			<div className="uob-content" id="confirmDetailsPage-section">
				<ConfirmReviewDetails {...this.props} />
				<br />
				
				<ConfirmTandC {...this.props} />
				<div className="uob-input-separator" />

				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.submitButton}  onClick={() => this.handleToThankYouPage()} />
				</div>

				{!this.props.onFixedButton() &&
					<div className="uob-input-separator fixedContinue fixedContinueGray">
						<PrimaryButton label={labels.submitButton} onClick={() => this.props.onCheck()} />
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, additionalPartnersDetailsReducer, localAddressInputReducer, companyBasicDetailsReducer, confirmTandCReducer, contactDetailsReducer } = state;
	return { commonReducer, additionalPartnersDetailsReducer, localAddressInputReducer, companyBasicDetailsReducer, confirmTandCReducer, contactDetailsReducer };
};

export default connect(mapStateToProps)(ConfirmDetailsPage);
