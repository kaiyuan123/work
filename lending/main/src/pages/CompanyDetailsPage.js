// company basic details (basic details, registered address, mailing address)
// company overall details (table view and review grants)
import React, { Component } from "react";
import { connect } from "react-redux";
import CompanyBasicDetails from "./CompanyBasicDetails";
import CompanyOverallDetails from "./CompanyOverallDetails";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";

class CompanyDetailsPage extends Component {
	handleToAdditionalPartnerDetailsPage() {
		this.props.onContinue();
	}

	isCompanyDetailsPassing(){
		this.props.onCheck()
	}

	checkCompanyBasicDetailValid(){
		const { companyBasicDetailsReducer, localAddressInputReducer } = this.props;
		const { mailingAddressCheckbox } = companyBasicDetailsReducer;
		const { mailingBlock, mailingLevel, mailingPostalCode, mailingStreet, mailingUnit, mailingLine1, mailingCountry, mailingCity, mailingLine2, mailingLine3, mailingLine4 } = localAddressInputReducer;
		let errorCount = 0;

		if(mailingAddressCheckbox.isToggled) {
			if(mailingCountry.value !== "SG"){
				if(mailingCity.value === "" || !mailingCity.isValid || mailingLine1.value === "" || !mailingLine1.isValid || !mailingLine2.isValid || !mailingLine3.isValid || !mailingLine4 ){
					errorCount++;
				}
			} else {
				if(mailingBlock.value === "" ||
					 mailingLevel.value === ""  ||
					 mailingPostalCode.value === "" ||
					 mailingStreet.value === ""  ||
					 mailingUnit.value === "" ||
					 !mailingBlock.isValid ||
					 !mailingLevel.isValid ||
					 !mailingPostalCode.isValid ||
					 !mailingStreet.isValid  ||
					 !mailingUnit.isValid) {
						 errorCount++
					 }
			}
		}
		return errorCount;
	}

	render() {
		const {commonReducer} = this.props;
		const labels = commonReducer.appData.companyDetails.labels
		return (
			<div className="uob-content" id="companyDetails-section">
				<CompanyBasicDetails {...this.props} />
				<CompanyOverallDetails {...this.props} />
				<div className="uob-input-separator" />
				<div className="uob-button-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToAdditionalPartnerDetailsPage()} />
				</div>
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToAdditionalPartnerDetailsPage()} />
				</div>

				{
					!this.props.onFixedButton() &&
					<div className="uob-input-separator fixedContinue fixedContinueGray">
						<PrimaryButton label={labels.continueButton} onClick={() => this.isCompanyDetailsPassing()}/>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, companyBasicDetailsReducer, localAddressInputReducer } = state;
	return { commonReducer, companyBasicDetailsReducer, localAddressInputReducer };
};

export default connect(mapStateToProps)(CompanyDetailsPage);
