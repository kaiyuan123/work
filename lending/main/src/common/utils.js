/* global GLOBAL_CONFIG */
const uploadUrl = `${GLOBAL_CONFIG.API_PATH}/user/application/{identifier}/customer/uploaddocument/{docType}`;
export const mapValueToLabel = (arrayOfItems, value) => {
  const item = arrayOfItems.find(x => x.value === value);
  return item ? item.description : value;
};

export const eliminateDuplicates = (arr) => {
  let i,
    len = arr.length,
    out = [],
    obj = {};

  for (i = 0; i < len; i++) {
    obj[arr[i]] = 0;
  }
  for (i in obj) {
    out.push(i);
  }
  return out;
}

export const imageExists = (url, callback) => {
  var img = new Image();
  img.onload = function () { callback(true); };
  img.onerror = function () { callback(false); };
  img.src = url;
};

export const mapValueToDescription = (obj) => {
  const objects = Object.keys(obj);
  const tmpArray = [];
  objects.map((key) => {
    tmpArray.push({
      value: key,
      description: obj[key]
    });
    return tmpArray;
  })
  return tmpArray
};

export const fileUploadHelper = (file, inputId, identifier, docType, { successDispatch, errorDispatch, progressDispatch }) => {
  const xhr = new window.XMLHttpRequest();
  const formData = new window.FormData();
  const url = uploadUrl.replace("{identifier}", identifier).replace("{docType}", docType);
  //const url = 'http://localhost:9090/Apply/api/fileUpload';
  if (inputId === "signature" || inputId === "drawSignature") {
    formData.append("file", file, "signature.png");
  } else {
    formData.append("file", file);
  }
  xhr.upload.addEventListener(
    "progress",
    e => {
      const percentage = e.loaded / e.total * 100;
      progressDispatch(Math.round(percentage));
    },
    false
  );
  xhr.onloadend = e =>
    e.target.status === 200 ? successDispatch(e.target.response) : errorDispatch(e.target.response);
  xhr.open("POST", `${url}`);
  xhr.send(formData);
};

export const bytesToSize = bytes => {
  var sizes = ["Bytes", "KB", "MB", "GB", "TB"];
  if (bytes === 0) {
    return "0 Byte";
  }
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);
  return Math.round(bytes / Math.pow(1024, i), 2) + " " + sizes[i];
};

export const groupByCode = (json, groupBy) => {
  return json.reduce((returnObj, obj) => {
    (returnObj[obj[groupBy]] = returnObj[obj[groupBy]] || []).push(obj);
    return returnObj;
  }, {});
};

export const capitalize = (str) => {
  return str.toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' ');
}

export const sendDataToSparkline = (dataElementJson, productCode = "", stepNo = "", isLanding = false, isFormStart = false, isInitiate = false, referenceNo = "", isSubmit = false) => {
  if (window._satellite) {
    if (!window.dataElement) {
      window.dataElement = {};
    }

    const dataElement = window.dataElement || {};
    dataElement.product_id = productCode;
    dataElement.product_name = dataElementJson[productCode].productName;
    dataElement.product_category = dataElementJson[productCode].productCatergory;
    dataElement.user_type = dataElementJson.userType;
    if (stepNo !== 1 && stepNo !== "" && (isInitiate || isSubmit)){
      dataElement.submission_id = referenceNo;
    }

    if (isLanding) {
      dataElement.event_name = dataElementJson.eventNameMyinfoStart;
    } else if (isFormStart) {
      dataElement.event_name = dataElementJson.eventNameFormStart;
    } else if (isSubmit) {
      dataElement.event_name = dataElementJson.eventNameFormSubmit;
      setTimeout(function(){ window._satellite.track("form_submit")}, 3000);
    } else {
      dataElement.event_name = dataElementJson.eventNameStep.replace("{step}", stepNo);
      window._satellite.track("form_complete_step");
    }
  }
};
