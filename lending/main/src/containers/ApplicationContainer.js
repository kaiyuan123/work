import React, { Component } from "react";
import { connect } from "react-redux";
import WrapContainer from "./WrapContainer";
import LoadingPage from "./../pages/LoadingPage";
import ThankYouPage from "./../pages/ThankYouPage";
import ContactDetailsPage from "./../pages/ContactDetailsPage";
import PersonalIncomeDetailsPage from "./../pages/PersonalIncomeDetailsPage";
import CompanyDetailsPage from "./../pages/CompanyDetailsPage";
import AdditionalPartnersDetailsPage from "./../pages/AdditionalPartnersDetailsPage";
import ConfirmDetailsPage from "./../pages/ConfirmDetailsPage";
import { scrollToSection, scrollBackToSection, setRetrievingMyInfo, handleErrorMessage, scrollToElement, setDuplicateTabId, toggleNav } from "./../actions/commonAction";
import { retrieveMyInfoDetails, submitInitiateApplication, submitPartialApplication, submitApplication, setErrorWithValue } from "./../actions/applicationAction";
import { handleAdditionalErrorMessage, setAdditionalPartnersInputError, handleAdditionalErrorMessageMobile } from './../actions/additionalPartnersDetailsAction'
import { eliminateDuplicates } from "../common/utils";
import { LocalEnvironment } from "./../api/httpApi"
import moment from 'moment';
import { sendDataToSparkline } from "./../common/utils";
import localStore from './../common/localStore';
import SessionExpirePopup from "./../pages/SessionExpirePopup";
import queryString from 'query-string';


class ApplicationContainer extends Component {
  // componentWillMount() {
  //   const { dispatch } = this.props;
  //   let pathname = window.location.pathname;
  //   const rootPath = pathname === '/corporate/loans/application' ? root_path_commerical : root_path_business
  //   this.props.history.push(LocalEnvironment ? "/application" : `${rootPath}/application`);
  //   dispatch(setRetrievingMyInfo(true));
  //   dispatch(retrieveMyInfoDetails());
  // }

  componentWillMount() {
    const { dispatch, applicationReducer, commonReducer } = this.props;
    const parameter = applicationReducer.startingParameter;
    const params = queryString.parse(parameter);
    // const duplicateTabs = btoa(`${moment()}&${Math.random() + 100}`);
    // localStore.setStore('duplicateTabs',duplicateTabs);
    if (commonReducer.appData) {
      let pathname = localStore.getStore("productCode").toLowerCase();
      const isCommerical = pathname.includes("cmb", 0);
      const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
      const businessPathUrl = commonReducer.appData.pathname_uri_business;
      const root_path = isCommerical ? corporatePathUrl : businessPathUrl;
      const parameterLength = (parseInt(commonReducer.appData.maxLengthParams, 10) - 1);
      let requiredParameter= [];
      commonReducer.appData.dataElementObj.requiredParameter.map((i) => {
          const conCatString = params[i] ? `${i}=${params[i]}` : '';
          if(conCatString !== '') {
            requiredParameter.push(conCatString);
          }
          return requiredParameter;
      });
      const parameterNeed = `?${requiredParameter.join('&').substring(0,parameterLength)}`;
      this.props.history.push(LocalEnvironment ? `/application${parameterNeed}` : `${root_path}application${parameterNeed}`);
      dispatch(setRetrievingMyInfo(true));
      dispatch(retrieveMyInfoDetails(() => this.redirectToErrorPage(), isCommerical));
      if (localStore.getStore('duplicateTabs') !== "" && localStore.getStore('duplicateTabs') !== undefined && localStore.getStore('duplicateTabs') !== null) {
        const duplicateTabs = btoa(`${moment()}&${pathname}&${Math.random() + 100}`);
        localStore.setStore('duplicateTabs',duplicateTabs);
      } else {
        const duplicateTabs = btoa(`${moment()}&${pathname}&${Math.random() + 100}`);
        localStore.setStore('duplicateTabs',duplicateTabs);
        dispatch(setDuplicateTabId(duplicateTabs));
      }
    } else {
      this.props.history.push({
        pathname: "apply",
        search: parameter
      })
    }
  }
  checkisValidFields(fields) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (!fields[i].isValid) {
        if (window.navigator.vendor) {
          const fieldError = fields.find(x => x.isValid === false);
          dispatch(scrollToElement(fieldError.name))
        }
        errorCount++;
      }
    }
    if (errorCount > 0) {
      return false;
    }
    return true;
  }

  checkEmptyFields(fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (fields[i].value === "") {
        dispatch(action(fields[i].name, errorMsg));
        if (window.navigator.vendor) {
          const fieldError = fields.find(x => x.value === '');
          // console.log(fieldError.name);
          dispatch(scrollToElement(fieldError.name))
          errorCount++;
        }
      }
    }
    if (errorCount > 0) {
      return false;
    }
    return true;
  }

  checkAdditionalEmptyFields(key, fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (fields[i].value === "") {
        dispatch(action(key, fields[i].name, errorMsg));
        errorCount++;
      }
    }
    if (errorCount > 0) {
      return false;
    }
    return true;
  }

  checkAdditionalNotValidFields(key, fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (!fields[i].isValid) {
        dispatch(action(key, fields[i].name, errorMsg));
        errorCount++;
      }
    }
    if (errorCount > 0) {
      return false;
    }
    return true;
  }

  isContactDetailsPassChecking() {
    const { contactDetailsReducer, commonReducer } = this.props;
    const { emailAddress, isCommerical, requestLoan, annualTurnover } = contactDetailsReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    if (!this.checkEmptyFields([emailAddress], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (!this.checkisValidFields([emailAddress], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (isCommerical) {
      if (!this.checkEmptyFields([requestLoan, annualTurnover], requiredMsg, handleErrorMessage)) {
        errorCount++
      }

      if (!this.checkisValidFields([requestLoan, annualTurnover], requiredMsg, handleErrorMessage)) {
        errorCount++
      }
    }

    if (errorCount > 0) {
      return false;
    }

    return true;
  }

  isPersonalDetailsPassChecking() {
    const { commonReducer, personalIncomeDetailsReducer } = this.props;
    const { maritalStatus } = personalIncomeDetailsReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;

    if (!this.checkEmptyFields([maritalStatus], requiredMsg, handleErrorMessage)) {
      errorCount++;
    }

    if (!this.checkisValidFields([maritalStatus], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (errorCount > 0) {
      return false
    }
    return true;
  }

  isCompanyDetailsPassing() {
    const { companyBasicDetailsReducer, localAddressInputReducer, commonReducer } = this.props;
    const { mailingAddressCheckbox } = companyBasicDetailsReducer;
    const { mailingBlock, mailingLevel, mailingPostalCode, mailingStreet, mailingUnit, mailingLine1, mailingCity, mailingCountry, mailingLine2, mailingLine3, mailingLine4 } = localAddressInputReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;

    if (mailingAddressCheckbox.isToggled) {
      if (mailingCountry.value !== "SG") {
        if (!this.checkEmptyFields([mailingCity, mailingLine1], requiredMsg, handleErrorMessage)) {
          errorCount++
        }

        if (!this.checkisValidFields([mailingCity, mailingLine1, mailingLine2, mailingLine3, mailingLine4], requiredMsg, handleErrorMessage)) {
          errorCount++
        }
      } else {
        if (!this.checkEmptyFields([mailingPostalCode, mailingBlock, mailingLevel, mailingStreet, mailingUnit], requiredMsg, handleErrorMessage)) {
          errorCount++
        }

        if (!this.checkisValidFields([mailingPostalCode, mailingBlock, mailingLevel, mailingStreet, mailingUnit], requiredMsg, handleErrorMessage)) {
          errorCount++;
        }
      }
    }
    if (errorCount > 0) {
      return false
    }
    return true;
  }

  isAdditionalPartnerCheckingPass() {
    const { dispatch, additionalPartnersDetailsReducer, commonReducer } = this.props;
    const { hidePartnerField, partnersID } = additionalPartnersDetailsReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    let invalidMsg = commonReducer.appData.errorMsgs.invalidPhone;
    let duplicateNricMsg = commonReducer.appData.errorMsgs.duplicateNricMsg;
    if (!hidePartnerField && partnersID.length > 0) {
      let duplicateKeys = [];
      for (let j = 0; j < partnersID.length; j++) {
        for (let k = j + 1; k < partnersID.length; k++) {
          const key = partnersID[k];
          const compareKey = partnersID[j];
          if (k !== j && additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.value === additionalPartnersDetailsReducer[`partners${compareKey}`].partnersNRIC.value) {
            duplicateKeys.push(partnersID[k]);
            duplicateKeys.push(partnersID[j]);
          }
        }
      }
      duplicateKeys = eliminateDuplicates(duplicateKeys);
      if (duplicateKeys.length > 0) {
        for (let i = 0; i < duplicateKeys.length; i++) {
          const key = duplicateKeys[i];
          dispatch(setAdditionalPartnersInputError(key, 'partnersNRIC', duplicateNricMsg));
          errorCount++;
        }
      }
      additionalPartnersDetailsReducer.partnersID.map((key) => {
        const partnersEmail = additionalPartnersDetailsReducer[`partners${key}`].partnersEmail;
        const partnersNRIC = additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC;
        const partnersName = additionalPartnersDetailsReducer[`partners${key}`].partnersName;
        const partnersMobileNo = additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo;
        if (partnersMobileNo.value === '+65') {
          dispatch(handleAdditionalErrorMessageMobile(key, "partnersMobileNo", invalidMsg, partnersMobileNo.value))
        }
        if (!this.checkAdditionalEmptyFields(key, [partnersEmail, partnersNRIC, partnersName, partnersMobileNo], requiredMsg, handleAdditionalErrorMessage)) {
          errorCount++;
        }

        if (!this.checkAdditionalNotValidFields(key, [partnersEmail, partnersNRIC, partnersName, partnersMobileNo], requiredMsg, handleAdditionalErrorMessage)) {
          errorCount++;
        }
        return errorCount;
      })
    }

    if (errorCount > 0) {
      return false
    }

    return true
  }

  handleToPersonalIncomeDetails() {
    const { dispatch } = this.props;
    if (!this.isContactDetailsPassChecking()) {
      return;
    }
    // const dataObj = this.getDataForSubmission();
    // dispatch(submitPartialApplication(dataObj));
    this.handleTracking("contactDetails");
    dispatch(scrollToSection("personalIncomeDetails"));
  }

  handleToCompanyDetails() {
    const { dispatch } = this.props;
    // if(!this.isAllChecking()) return;
    this.handleTracking("personalDetails");
    const dataObj = this.getDataForSubmission();
    dispatch(submitPartialApplication(dataObj));
    dispatch(scrollToSection("companyDetails"));
  }

  handleToAdditionalPartnerDetailsPage() {
    const { dispatch } = this.props;
    // if(!this.isAllChecking()) return;
    this.handleTracking("companyDetails");
    const dataObj = this.getDataForSubmission();
    dispatch(submitPartialApplication(dataObj));
    dispatch(scrollToSection("additionalPartnersDetails"));
  }

  handleToReviewPage() {
    const { dispatch } = this.props;
    // if(!this.isAllChecking()) return;
    this.handleTracking("additionalApplicants");
    const dataObj = this.getDataForSubmission();
    dispatch(submitPartialApplication(dataObj));
    dispatch(scrollToSection("confirmDetailsPage"));
  }

  handleInitiateApplication() {
    const { dispatch } = this.props;
    const dataObj = this.getDataForInitiation();
    dispatch(submitInitiateApplication(dataObj, () => this.handleToPersonalIncomeDetails()));
  }

  getDataForInitiation() {
    const {
      contactDetailsReducer,
      personalIncomeDetailsReducer
    } = this.props;

    const {
      emailAddress,
      isCommerical,
      requestLoan,
      annualTurnover,
    } = contactDetailsReducer;

    const {
      legalId
    } = personalIncomeDetailsReducer;

    const dataObjBasicDetails = {
      person: [{
        basicInfo: {
          legalId: legalId.value,
          emailAddress: emailAddress.value
        },
      }],
      entity: {
        requestLoan: isCommerical ? requestLoan.value : null,
        annualTurnover: isCommerical ? annualTurnover.value : null
      }
    }

    return dataObjBasicDetails;
  }

  handleGreyButton() {
    const { commonReducer, contactDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer, additionalPartnersDetailsReducer, confirmTandCReducer, personalIncomeDetailsReducer } = this.props;
    const {
      emailAddress,
      annualTurnover,
      requestLoan,
      isCommerical
    } = contactDetailsReducer;

    const { maritalStatus } = personalIncomeDetailsReducer;

    const { mailingAddressCheckbox } = companyBasicDetailsReducer;
    const { mailingBlock, mailingLevel, mailingPostalCode, mailingStreet, mailingUnit, mailingLine1, mailingCountry, mailingCity, mailingLine2, mailingLine3, mailingLine4 } = localAddressInputReducer;

    const { hidePartnerField, partnersID } = additionalPartnersDetailsReducer;

    const currentSection = commonReducer.currentSection;
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "additionalPartnersDetails", "confirmDetailsPage"];
    const indexOfSteps = steps.indexOf(currentSection);

    const { acknowledgementCheckbox } = confirmTandCReducer;

    let errorCount = 0;

    if (indexOfSteps >= 0) {
      const contactFieldValid = emailAddress.isValid && emailAddress.value !== '';
      const loanValid = annualTurnover.isValid && annualTurnover.value !== '' && requestLoan.isValid && requestLoan.value !== '';
      const fullValidation = isCommerical ? contactFieldValid && loanValid : contactFieldValid;
      if (!fullValidation) {
        errorCount++
      }
    }

    if (indexOfSteps >= 1) {

      if (maritalStatus.value === "" || !maritalStatus.isValid) {
        errorCount++;
      }
    }

    if (indexOfSteps >= 2) {
      if (mailingAddressCheckbox.isToggled) {
        if (mailingCountry.value !== "SG") {
          if (mailingCity.value === "" || !mailingCity.isValid || mailingLine1.value === "" || !mailingLine1.isValid || !mailingLine2.isValid || !mailingLine3.isValid || !mailingLine4.isValid) {
            errorCount++;
          }
        } else {
          if (mailingBlock.value === "" ||
            mailingLevel.value === "" ||
            mailingPostalCode.value === "" ||
            mailingStreet.value === "" ||
            mailingUnit.value === "" ||
            !mailingBlock.isValid ||
            !mailingLevel.isValid ||
            !mailingPostalCode.isValid ||
            !mailingStreet.isValid ||
            !mailingUnit.isValid) {
            errorCount++
          }
        }
      }
    }

    if (indexOfSteps >= 3) {
      if (!hidePartnerField && partnersID.length > 0) {
        for (let j = 0; j < partnersID.length; j++) {
          for (let k = j + 1; k < partnersID.length; k++) {
            const key = partnersID[k];
            const compareKey = partnersID[j];
            if (k !== j && additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.value === additionalPartnersDetailsReducer[`partners${compareKey}`].partnersNRIC.value) {
              errorCount++;
            }
          }
        }
        additionalPartnersDetailsReducer.partnersID.map((key) => {
          const partnersEmail = additionalPartnersDetailsReducer[`partners${key}`].partnersEmail;
          const partnersNRIC = additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC;
          const partnersName = additionalPartnersDetailsReducer[`partners${key}`].partnersName;
          const partnersMobileNo = additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo;

          const isAdditionalPartnerValid = partnersEmail.value !== "" && partnersEmail.isValid && partnersNRIC.value !== "" && partnersNRIC.isValid && partnersName.value !== "" && partnersName.isValid && partnersMobileNo.value !== "" && partnersMobileNo.value !== "+65"  && partnersMobileNo.isValid;
          if (!isAdditionalPartnerValid) {
            errorCount++
          }
          return errorCount;
        })
      }
    }

    if (indexOfSteps >= 4) {
      if (!acknowledgementCheckbox.isToggled) {
        errorCount++;
      }
    }

    if (errorCount > 0) {
      return false
    }
    return true
  }

  getDataForSubmission() {
    const { applicationReducer, personalIncomeDetailsReducer, additionalPartnersDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer } = this.props;
    const { myInfoData, initiateMyinfoData } = applicationReducer;
    // const { emailAddress } = contactDetailsReducer;
    const { maritalStatus } = personalIncomeDetailsReducer;
    const { partnersID } = additionalPartnersDetailsReducer;
    const { mailingAddressCheckbox } = companyBasicDetailsReducer;
    const { mailingPostalCode, mailingBlock, mailingStreet, mailingBuilding, mailingLevel, mailingUnit, mailingCountry, mailingCity, mailingLine1, mailingLine2, mailingLine3, mailingLine4 } = localAddressInputReducer;

    let personObj = [];
    // const havePartner = shareholders[0].hasOwnProperty('PARTNER');
    if (partnersID.length > 0) {
      partnersID.map((key) => {
        const isPartnerEmpty = additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.value === "" && additionalPartnersDetailsReducer[`partners${key}`].partnersName.value === "" && additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.value === "" && (additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.value === "" || additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.value === "+65" ) ? true : false;

        const partnersEmail = additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.value !== "" ? additionalPartnersDetailsReducer[`partners${key}`].partnersEmail.value : null;
        const partnersNRIC = additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.value !== "" ? additionalPartnersDetailsReducer[`partners${key}`].partnersNRIC.value : null;
        const partnersName = additionalPartnersDetailsReducer[`partners${key}`].partnersName.value !== "" ? additionalPartnersDetailsReducer[`partners${key}`].partnersName.value : null;
        const partnersMobileNo = additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.value !== "" ? `${additionalPartnersDetailsReducer[`partners${key}`].partnersMobileNo.value}` : null;

        if (!isPartnerEmpty) {
          personObj.push({
            legalId: partnersNRIC,
            name: partnersName,
            emailAddress: partnersEmail,
            mobileNumber: partnersMobileNo && partnersMobileNo !== '+65' ? `${partnersMobileNo.replace(/ +/g, "")}` : null
          })
        }
        return personObj;
      });
    }

    const mailingAddressObj = {
      type: 'M',
      unitNo: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingUnit.value : '',
      street: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingStreet.value : '',
      block: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingBlock.value : '',
      postalCode: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingPostalCode.value : '',
      floor: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingLevel.value : '',
      building: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingBuilding.value : '',
      country: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingCountry.value : 'SG',
      city: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingCity.value : '',
      line1: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingLine1.value : '',
      line2: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingLine2.value : '',
      line3: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingLine3.value : '',
      line4: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingLine4.value : '',
    };

    const addressObjs = [myInfoData.entity.addresses[0]];
    if (mailingAddressCheckbox.isToggled) {
      addressObjs.push(mailingAddressObj)
    }

    const addressesObj = {
      addresses: addressObjs
    }

    // const isSgOrPr = residentialStatus.value === 'C' || residentialStatus.value === 'P';
    const dataObj = {
      additionalAppointment: personObj,
      entity: addressesObj,
      person: [
        {
          basicInfo: {
            passportNumber: initiateMyinfoData.person[0].basicInfo.passportNumber ? initiateMyinfoData.person[0].basicInfo.passportNumber : null,
            passportExpiryDate: initiateMyinfoData.person[0].basicInfo.passportExpiryDate ? initiateMyinfoData.person[0].basicInfo.passportExpiryDate : null,
          },
          personalInfo: {
            maritalStatus: maritalStatus.value
          }
        }
      ]
    }
    return dataObj;
  }


  isAllChecking() {
    const { dispatch, confirmTandCReducer, commonReducer } = this.props;
    const { acknowledgementCheckbox } = confirmTandCReducer;
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "additionalPartnersDetails", "confirmDetailsPage"];
    const indexOfSteps = steps.indexOf(commonReducer.currentSection);

    if (indexOfSteps >= 0) {
      if (indexOfSteps > 0 && !this.isContactDetailsPassChecking()) {
        dispatch(scrollBackToSection('contactDetails'));
      }
      if (indexOfSteps === 0) {
        this.isContactDetailsPassChecking()
      }
    }

    if (indexOfSteps >= 1) {
      if (indexOfSteps > 1 && !this.isPersonalDetailsPassChecking()) {
        dispatch(scrollBackToSection('personalIncomeDetails'))
      }
      if (indexOfSteps === 1) {
        this.isPersonalDetailsPassChecking()
      }
    }

    if (indexOfSteps >= 2) {
      if (indexOfSteps > 2 && !this.isCompanyDetailsPassing()) {
        dispatch(scrollBackToSection('companyDetails'))
      }
      if (indexOfSteps === 2) {
        this.isCompanyDetailsPassing()
      }
    }

    if (indexOfSteps >= 3) {
      if (indexOfSteps > 3 && !this.isAdditionalPartnerCheckingPass()) {
        dispatch(scrollBackToSection('additionalPartnersDetails'))
      }
      if (indexOfSteps === 3) {
        this.isAdditionalPartnerCheckingPass()
      }
    }

    if (indexOfSteps >= 4 && !acknowledgementCheckbox.isToggled) {
      const checkboxError = commonReducer.appData.confirmDetails.confirmTandC.errorMsgCheckbox
      dispatch(handleErrorMessage("acknowledgementCheckbox", checkboxError))
      dispatch(scrollToElement("confirmTandC"));
    }
  }

  handleToUOBSite() {
    window.location.href = "https://www.uobgroup.com";
  }

  handleSubmitApplication() {
    const { dispatch } = this.props;
    this.handleTracking("review");
    const dataObj = this.getDataForSubmission();
    dispatch(toggleNav(true));
    dispatch(submitApplication(dataObj, () => this.redirectToErrorPage()));
  }

  redirectToErrorPage() {
    this.props.history.push({
      pathname: "error"
    })
  }

  handleTracking(step) {
    const {
      commonReducer,
      applicationReducer
    } = this.props;
    const { isInitial, initiateMyinfoData, productCode } = applicationReducer
    const dataElement = commonReducer.appData.dataElementObj;
    const id = initiateMyinfoData && initiateMyinfoData.referenceNumber && isInitial ? initiateMyinfoData.referenceNumber : "";
    const code = productCode ? productCode.toLowerCase() : "";
    let stepNo = 0;
    switch (step) {
      case 'contactDetails':
        stepNo = 1;
        break;
      case 'personalDetails':
        stepNo = 2;
        break;
      case 'companyDetails':
        stepNo = 3;
        break;
      case 'additionalApplicants':
        stepNo = 4;
        break;
      case 'review':
        stepNo = 5;
        break;
      default:
        stepNo = 1;
    }
    sendDataToSparkline(dataElement, code, stepNo, false, false, isInitial, id);
  }

  componentDidUpdate(prevState) {
    const { commonReducer, applicationReducer } = this.props;
    if (prevState.commonReducer.appData !== commonReducer.appData) {
      const dataElement = commonReducer.appData.dataElementObj
      const productCode = applicationReducer.productCode ? applicationReducer.productCode.toLowerCase() : "";
      sendDataToSparkline(dataElement, productCode, "", false, true);
    }
  }

  handleToErrorLocal(e) {
    const { commonReducer, dispatch , applicationReducer} = this.props;
    const duplicateTabId = commonReducer.duplicateTab;
    const localStoreId = localStore.getStore("duplicateTabs");
    const productCode = localStore.getStore("productCode");
    let count = 0;
    const supportOS = ["Chrome", "Firefox"];
    supportOS.map((i) => {
      const str = window.navigator.userAgent.toString();
      if (str.indexOf(i) >= 0){
        count ++;
      }
      return count
    });
    if (count > 0) {
      if( duplicateTabId !== localStoreId && applicationReducer.productCode === productCode ) {
        dispatch(setErrorWithValue("DuplicatedTabMsg"));
        this.props.history.push({
          pathname: `error`,
        })
      }
    }
  }

	componentDidMount() {
    window.addEventListener('storage', (e) => this.handleToErrorLocal(e));
  }

  componentWillUnmount() {
    window.removeEventListener('storage', (e) => this.handleToErrorLocal(e));
  }

  render() {
    const { commonReducer, applicationReducer } = this.props;
    const { dataLoaded, sessionExpirePopup } = applicationReducer;
    const currentSection = commonReducer.currentSection;
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "additionalPartnersDetails", "confirmDetailsPage"];
    const indexOfSteps = steps.indexOf(currentSection);

    return (
      <WrapContainer {...this.props}>
        {!dataLoaded && commonReducer.isRetrieving && <LoadingPage />}
        {dataLoaded && (
          (currentSection === "thankyou" ? <ThankYouPage /> :
            <div>
              {sessionExpirePopup && <SessionExpirePopup {...this.props} onContinue={() => this.handleToUOBSite()} />}
              <ContactDetailsPage {...this.props} onContinue={() => this.handleInitiateApplication()} onCheck={() => this.isAllChecking()} onFixedButton={() => this.handleGreyButton()} />
              {indexOfSteps >= 1 && <PersonalIncomeDetailsPage {...this.props} onCheck={() => this.isAllChecking()} onContinue={() => this.handleToCompanyDetails()} onFixedButton={() => this.handleGreyButton()} />}
              {indexOfSteps >= 2 && <CompanyDetailsPage {...this.props} onContinue={() => this.handleToAdditionalPartnerDetailsPage()} onCheck={() => this.isAllChecking()} onFixedButton={() => this.handleGreyButton()} />}
              {indexOfSteps >= 3 && <AdditionalPartnersDetailsPage {...this.props} onContinue={() => this.handleToReviewPage()} onCheck={() => this.isAllChecking()} onFixedButton={() => this.handleGreyButton()} />}
              {indexOfSteps >= 4 && <ConfirmDetailsPage {...this.props} onContinue={() => this.handleSubmitApplication()} onCheck={() => this.isAllChecking()} onFixedButton={() => this.handleGreyButton()} />}
            </div>
          )
        )}
      </WrapContainer>
    );
  }
}

const mapStateToProps = state => {
  const { commonReducer, applicationReducer, contactDetailsReducer, personalIncomeDetailsReducer, additionalPartnersDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer, confirmTandCReducer } = state;
  return { commonReducer, applicationReducer, contactDetailsReducer, personalIncomeDetailsReducer, additionalPartnersDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer, confirmTandCReducer };
};

export default connect(mapStateToProps)(ApplicationContainer);
