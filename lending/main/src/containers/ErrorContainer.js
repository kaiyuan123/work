import React, { Component } from 'react';
import { connect } from 'react-redux';
import WrapContainer from './WrapContainer';
import ErrorMessagePage from './../pages/ErrorMessagePage';
// import { sendDataToSparkline } from './../common/utils';

class ErrorContainer extends Component {

  render() {
    const { applicationReducer } = this.props;
    const { errorCode } = applicationReducer;
    return (
      <WrapContainer {...this.props}>
        <ErrorMessagePage {...this.props} errorCode={errorCode}/>
      </WrapContainer>
    );
  }
}

const mapStateToProps = (state) => {
  const { applicationReducer, commonReducer } = state;
  return { applicationReducer, commonReducer };
}

export default connect(mapStateToProps)(ErrorContainer);
