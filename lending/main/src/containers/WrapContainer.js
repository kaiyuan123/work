import React, { Component } from "react";
import { connect } from "react-redux";
import closeIcon from "./../assets/images/cross-white.svg";
import menuHamburger from "./../assets/images/menu-hamburger.svg";
import { getInitialData, setErrorMessage } from "./../actions/commonAction";
import GenericErrorMessage from "./../components/GenericErrorMessage/GenericErrorMessage";
import Loader from "./../components/Loader/Loader";
import NavItem from "./../components/NavItem/NavItem";
import { toggleNav } from "./../actions/commonAction";
// import Beforeunload from "react-beforeunload";
import localStore from './../common/localStore';

import "./../assets/css/bootstrap.css";
import "./../assets/css/uob-form-loan.css";
import "./../assets/css/eBiz-style.css";

class WrapContainer extends Component {
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(getInitialData());
  }

  handleOnClearMessage() {
    const { dispatch } = this.props;
    dispatch(setErrorMessage(""));
  }

  handleClearStore() {
    localStore.clear()
  }

  closeNav() {
    const { commonReducer, dispatch } = this.props;
    if (commonReducer.toggleNav) {
      dispatch(toggleNav(true));
    } 
  }

  toggleNav(showNav) {
		const { dispatch } = this.props;
    dispatch(toggleNav(showNav));
	}

  render() {
    const { commonReducer, applicationReducer } = this.props;
    let imgUrl = "./images/logos/uob-eBiz-Logo-blue.svg";
    let imgUrlWhite = "./images/logos/uob-eBiz-Logo.svg";
    let rightByYouLogo = "./images/logos/right-by-you-logo.png"
    let pathname = window.location.pathname;
    const currentPath = pathname.substr(pathname.lastIndexOf('/') + 1);
    const currentSection = commonReducer.currentSection;
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "additionalPartnersDetails", "confirmDetailsPage"];
    const indexOfSteps = steps.indexOf(currentSection);
    const fullWhitePage = commonReducer.isRetrieving || currentSection === "thankyou" || currentPath === "error"
    const isRetrieving = fullWhitePage ? "isRetrieving" : "";
    const productCode = applicationReducer.myInfoData !== null && applicationReducer.productCode ? applicationReducer.productCode.toUpperCase() : "";
    const productTitle = commonReducer.appData !== null && commonReducer.appData.productTitle[productCode];
    const navbarLabel = commonReducer.appData !== null && commonReducer.appData.navBar;
    const invalidCode = commonReducer.appData !== null && commonReducer.appData.validProductCode.indexOf(productCode.toLowerCase()) >= 0;
    const productTitleError = commonReducer.appData !== null && commonReducer.appData.productTitleThankYou[productCode];
    const isfullwhite = fullWhitePage ? "whitePage-header" : "";
    const navBarHide = fullWhitePage ? "whitepage-navBar": "";
    const toggleNavCSS = commonReducer.toggleNav ? "showNav": "hideNav";
    const progressPercentage = ((indexOfSteps + 1) / steps.length) * 100;
    return (
      <div className={`uob-form-loan ${isfullwhite}`}>
        {commonReducer.isLoading && <Loader />}
        <div>
          {fullWhitePage &&
            <div className={`apply-page-header-container ${isfullwhite}`}>
              <div className="apply-page-header-title">
                <img className="img-size" alt="logo" src={imgUrl} />
                {invalidCode && < div className="title-vertical-line" />}
                {invalidCode && <div className="title-text"> {productTitleError} </div>}
              </div>
              <div className="right-by-you-container">
                <img alt="right-by-you" src={rightByYouLogo} />
              </div>
            </div>
          }
          {commonReducer.appData &&
            <nav
            className={`navbar navbar-expand-lg navbar-dark bg-primary-left fixed-top ${isRetrieving} ${toggleNavCSS}`}
              id="sideNav"
            >
              <div className="mobileView">
								<span className="close-icon bookAppointmentCloseMark sideBarCloseImage" onClick={() => this.toggleNav(true)}>
											<img className="close-icon" src={closeIcon} alt="Close-Icon" width={20} height={20} />
								</span>
							</div>
              <div className="leftTopLogo">
                <img src={imgUrlWhite} alt="Logo" className="uob-logo" width="160" />
              </div>
              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav">
                  <NavItem
                    defaultStep={true}
                    navTitle={navbarLabel.myInfoBusiness}
                    defaultLink={true}
                    stepFinished={true}
                  />
                  <NavItem
                    stepFinished={indexOfSteps > 0}
                    currentActiveStep={indexOfSteps === 0}
                    linkSelected={
                      (indexOfSteps === 0 && this.props.location.hash === "") ||
                      (indexOfSteps >= 0 &&
                        this.props.location.hash === "#contactDetails-section")
                    }
                    linkIdName={"#contactDetails-section"}
                    navTitle={navbarLabel.contactDetails}
                  />
                  <NavItem
                    stepFinished={indexOfSteps > 1}
                    currentActiveStep={indexOfSteps === 1}
                    linkSelected={
                      (indexOfSteps === 1 && this.props.location.hash === "") ||
                      (indexOfSteps >= 1 &&
                        this.props.location.hash ===
                        "#personalIncomeDetails-section")
                    }
                    linkIdName={"#personalIncomeDetails-section"}
                    navTitle={navbarLabel.personalIncomeDetails}
                  />
                  <NavItem
                    stepFinished={indexOfSteps > 2}
                    currentActiveStep={indexOfSteps === 2}
                    linkSelected={
                      (indexOfSteps === 2 && this.props.location.hash === "") ||
                      (indexOfSteps >= 2 &&
                        this.props.location.hash === "#companyDetails-section")
                    }
                    linkIdName={"#companyDetails-section"}
                    navTitle={navbarLabel.companyDetails}
                  />
                  <NavItem
                    stepFinished={indexOfSteps > 3}
                    currentActiveStep={indexOfSteps === 3}
                    linkSelected={
                      (indexOfSteps === 3 && this.props.location.hash === "") ||
                      (indexOfSteps >= 3 &&
                        this.props.location.hash ===
                        "#additionalPartnersDetails-section")
                    }
                    linkIdName={"#additionalPartnersDetails-section"}
                    navTitle={navbarLabel.partnersDetails}
                  />
                  <NavItem
                    stepFinished={indexOfSteps > 4}
                    currentActiveStep={indexOfSteps === 4}
                    linkSelected={
                      (indexOfSteps === 4 && this.props.location.hash === "") ||
                      (indexOfSteps >= 4 &&
                        this.props.location.hash ===
                        "#confirmDetailsPage-section")
                    }
                    linkIdName={"#confirmDetailsPage-section"}
                    navTitle={navbarLabel.review}
                  />
                </ul>
              </div>
            </nav>
          }

          {commonReducer.appData !== null && applicationReducer.myInfoData !== null &&
            <div className={`container-fluid p-0 content_1 productTitle-header ${navBarHide} ${toggleNavCSS}`}>
							<div className={`mobile-flex`}>
                <button className={`menuButton ${toggleNavCSS}`} type="button" onClick={() => this.toggleNav(commonReducer.toggleNav)}>
                  <img src={menuHamburger} alt="Menu-Icon" width={25} height={30} />
                </button>
                <div className={`displayHeader ${toggleNavCSS}`}>
                  <img className="mobile-header-logo" alt="logo" src={imgUrl} />
                  <div className="mobile-header-vertical-line"/>
                  <div className="width-200">
                    <span className="font-12">{productTitle.split(" | ")[0]}</span>
                    <br/>
                    <span className="font-14">{productTitle.split(" | ")[1]}</span>
                  </div>
                </div>
                <section
                  className="resume-section d-column"
                  id="home"
                >
                  <div className="my-auto">
                    <h1 className="mt-0 title1">
                      <span className="title2">
                        {productTitle}
                      </span>
                    </h1>
                  </div>
                </section>
                {!fullWhitePage && <img className="rightByYouLogo" src={rightByYouLogo} alt="Right By You" />}
              </div>
              <div className="progressTopMain">
                <div className="mobileProgressTop" style={{width:progressPercentage+'%'}}/>
              </div>
            </div>
          }

        </div>
        <div className={`uob-form-loan-container ${toggleNavCSS}`}>
          <div className="uob-body" onClick={() => this.closeNav()}>
            {commonReducer.appData !== null && this.props.children}
          </div>
        </div>
        {commonReducer.messageContent !== "" && (
          <GenericErrorMessage
            {...this.props}
            interval={60}
            messageContent={commonReducer.messageContent}
            onClearMessage={this.handleOnClearMessage.bind(this)}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { commonReducer } = state;
  return { commonReducer };
};

export default connect(mapStateToProps)(WrapContainer);
