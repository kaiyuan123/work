import React, { Component } from "react";
import { connect } from "react-redux";
import WrapContainer from "./WrapContainer";
import ContactDetailsPage from "./../pages/ContactDetailsPage";
import PersonalIncomeDetailsPage from "./../pages/PersonalIncomeDetailsPage";
import CompanyDetailsPage from "./../pages/CompanyDetailsPage";
import AdditionalPartnersDetails from "./../pages/AdditionalPartnersDetails";
import ConfirmDetailsPage from "./../pages/ConfirmDetailsPage";

class LandingContainer extends Component {
	render() {
		return (
			<WrapContainer {...this.props}>
				<ContactDetailsPage {...this.props} />
				<PersonalIncomeDetailsPage {...this.props} />
				<CompanyDetailsPage {...this.props} />
				<AdditionalPartnersDetails {...this.props} />
				<ConfirmDetailsPage {...this.props} />
			</WrapContainer>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer } = state;
	return { commonReducer };
};

export default connect(mapStateToProps)(LandingContainer);
