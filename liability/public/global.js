// eslint-disable-next-line no-unused-vars
const GLOBAL_CONFIG = {
	API_PATH: "/bb/api/v1",
	COMMON_API_PATH: "/api/sg/v1",
	CALLBACK_API_PATH: "/myinfobiz/entity-person",
	MYINFO_URL: "/dummysingpass.html?redirect_uri=http://pibtsg95:6507/camelcar/callback&client_id=stg_uob_one_casacc&purpose=opening an account online with UOB&attributes=aliasname,assessableincome,birthcountry,dob,edulevel,email,employment,hanyupinyinaliasname,hanyupinyinname,hdbtype,housingtype,marriedname,marital,mobileno,name,nationality,regadd,sex&state=",
	formConfig: "./liability.json",
	availableLocationsConfig: "./availableLocations.json",
	myinfoDataJson: "./myinfoData.json",
	initiateMyinfoDataJson: "./initiateJsonFile.json",
	timeout: "45000",
	Local_Environment: true,
	Force_URL_redirect: false,
	Force_Google_Tag: false,
	root_path: "/businessbanking/loans/",
	uobSite: "https://www.uobgroup.com",
	apiKeyProd: "AIzaSyDuBueHyg0D3QlqkxRV7D3wpfIO-PMkCMA",
	apiKeyDevp: "AIzaSyDuBueHyg0D3QlqkxRV7D3wpfIO-PMkCMA",
	clientProd: "gme-uobsingapore&channel=bb-deposits",
	clientDevp: "gme-uobsingapore&channel=bb-deposits",
	inDevelopment: true 
};
