import * as types from "./actionTypes";

export const handleAskQuestionPopup = (popuptoggle, questionnaires) => {
    let noOfQuestions=0;

    questionnaires.map((question, i) => {
        return question.subType.length > 0 ? 
            question.subType.map((quesSub, j) => {
                return noOfQuestions++;
            })
        : noOfQuestions++;
    });
    
    return {
        type: types.ASK_POPUP_SHOW,
        popuptoggle: popuptoggle,
        noOfQuestions: noOfQuestions,
        questionnaires: questionnaires
    }
}

export const handleHideAskQuestionPopup = (popuptoggle) => {
    return (dispatch, getState) => {
        let questionnaires = getState().askQuestionsReducer.questionnaires;
        questionnaires[0].map((question, i) => {
            return question.subType.length > 0 ? 
                question.subType.map((quesSub, j) => {
                    return [quesSub.value = "", quesSub.isValid = true];
                })
            : [question.value = "",  question.isValid = true];
        });
        
        dispatch({
            type: types.ASK_POPUP_HIDE_ON_CLOSE_BUTTON,
            popuptoggle: popuptoggle,
            noOfQuestions: 0,
            noOfButtonChecked:0,
            questionnaires: questionnaires
        })

    }
}

export const handlePopupHideOnOkButton = (popuptoggle) => {
    return {
        type: types.ASK_POPUP_HIDE_ON_OK_BUTTON,
        popuptoggle: popuptoggle,

    }
}

export const handleIsChecked = (values, lengthofButtonChecked, index, subindex) => {
    return (dispatch, getState) => {
      let questionnaires = getState().askQuestionsReducer.questionnaires;

        if(questionnaires[0][index].subType.length > 0){
            if(questionnaires[0][index].subType[subindex].value === "") {
                lengthofButtonChecked++;
            }
            questionnaires[0][index].subType[subindex].value = values;
            questionnaires[0][index].subType[subindex].isValid = true;

        }else{
            if(questionnaires[0][index].value === "") {
                lengthofButtonChecked++;
            }
            questionnaires[0][index].value = values;
            questionnaires[0][index].isValid = true;
        }
      dispatch({
        type: types.ASK_POPUP_QUESTION_VALUES,
          questionnaires,
            noOfButtonChecked: lengthofButtonChecked
      })

    };
}

// handle set error msg
export const handleErrorMessage = (field, index, subindex, errorMsg) => {
  return (dispatch, getState) => {
    let questionnaires = getState().askQuestionsReducer.questionnaires;
    if(field.id.indexOf('_') === 1 ){
        questionnaires[0][index].subType[subindex].isValid = false;
    }else{
      questionnaires[0][index].isValid = false;
    }

    dispatch({
      type: types.SET_ERROR_MESSAGE_QUESTIONS,
      questionnaires,
      errorMsg
    })
  }
}
