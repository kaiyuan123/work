import * as types from "./actionTypes";

export const setDropdownFocusStatus = (isFocus, field) => {
	return (dispatch) => {
		dispatch({
			type: types.BOOK_APPOINTMENT_DROPDOWN_FOCUS,
			isFocus,
			field
		});
	};
};

export const selectDropdownItem = (value, description, field,filterItem) => {
	return (dispatch) => {
		dispatch({
			type: types.BOOK_APPOINTMENT_DROPDOWN_ITEM_SELECT,
			value,
			field,
			description,
			filterItem
		})
	};
};

export const setDateTimeValue = (value) => {
	return (dispatch) => {
		dispatch({
			type: types.SET_DATE_TIME_VALUE,
			value
		})
	};
};

export const changeSearchInputValue = (searchValue, field) => {
	return (dispatch) => {
		dispatch({
			type: types.BOOK_APPOINTMENT_DROPDOWN_SEARCH_INPUT_CHANGE,
			searchValue,
			field
		});
	};
};

export const rightLocationClick = (searchValue,filterItem) => {
	return (dispatch) => {
		dispatch({
			type: types.BOOK_APPOINTMENT_RIGHT_LOCATION_CLICK,
			searchValue,
			filterItem
		});
	};
};

export const bookAppointmentTogglePopUp = (status) => {
	return (dispatch) => {
		dispatch({
			type: types.BOOK_APPOINTMENT_TOGGLE_POP_UP,
			status
		});
	};
};

export const bookAppointmentSelectNavigation = (field) => {
	return (dispatch) => {
		if(field === "onClickBranch") {
			dispatch({
				type: types.BOOK_APPOINTMENT_SELECT_NAVIGATION,
				onClickBranch: true,
				onClickDate: false,
				itemActive: '0'
			})
		}

		if(field === "onClickDate") {
			dispatch({
				type: types.BOOK_APPOINTMENT_SELECT_NAVIGATION,
				onClickBranch: false,
				onClickDate: true,
				itemActive: '1'
			})
		}
	}
}
