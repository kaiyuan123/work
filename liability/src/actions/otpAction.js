import * as types from './actionTypes';
import { Application } from './../api/httpApi';
import { setLoadingStatus, setErrorMessage } from './commonAction';

export const setCountDownStatus = (status) => {
  return (dispatch) => {
    dispatch(setErrorMessage(""));
    dispatch({
      type: types.SET_COUNT_DOWN_STATUS,
      status
    });
  };
}

export const sendOtp = () => {
  return (dispatch, getState) => {
    const globalErrors = getState().commonReducer.appData.globalErrors;
    dispatch(isSendingOTP(true));
    dispatch(setErrorMessage(""));
    Application.sendOtp().then((res) => {
      dispatch(setOtpPrefix(res.data.prefix));
      dispatch(isSendingOTP(false));
    }).catch(e => {
      dispatch(setErrorMessage(globalErrors.apiException));
      dispatch(isSendingOTP(false));
    });
  }
}

export const verifyOtp = (value, applicationID, redirectOnSuccess) => {
  return (dispatch, getState) => {
    const globalErrors = getState().commonReducer.appData.globalErrors;
    dispatch(setLoadingStatus(true));
    Application.verifyOtp(value).then((res) => {
      dispatch(setLoadingStatus(false));
      Application.getApplication(applicationID).then((res) => {
        dispatch({
          type: types.STORE_MY_INFO_REPONSES,
          response: res.data
        })
      dispatch(setRetrieveApplicationMode(true));
      redirectOnSuccess && redirectOnSuccess();
      }).catch(e => {
          dispatch(setLoadingStatus(false));
          dispatch(setErrorMessage(globalErrors.apiException));
      });        
    }).catch(e => {
      if (e.response && e.response.status === 400) {
        dispatch(setOtpErrorMessage(globalErrors.invalidOTP));
      } else if(e.response && e.response.status === 500){
        dispatch(setOtpErrorMessage(globalErrors.invalidOTP));
      }else if(e.response && e.response.status === 404){
        dispatch(setOtpErrorMessage(globalErrors.invalidOTP));
      }else {
        dispatch(setErrorMessage(globalErrors.apiException));
      }
      dispatch(resetOtpToEmpty());
      dispatch(setLoadingStatus(false));
    });
  }
}

export const setRetrieveApplicationMode = (isRetrieveApplication) => {
  return {
    type: types.IS_RETRIEVE_APPLICATION_MODE,
    isRetrieveApplication
  }
}

export const setOtpPrefix = (prefix) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_OTP_PREFIX,
      prefix
    });
  }
}

export const setOtpTriedCount = (otpTriedCount) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_OTP_TRIED_COUNT,
      otpTriedCount
    });
  }
}

export const handleOtpChange = (code) => {
  return (dispatch) => {
    dispatch({
      type: types.OTP_INPUT_CHANGE,
      code
    });
  }
}

const isSendingOTP = (status) => {
  return {
    type: types.IS_SENDING_OTP,
    status
  };
}

export const setOtpErrorMessage = (errorMsg) => {
  return {
    type: types.SET_OTP_ERROR_MESSAGE,
    errorMsg
  }
}

const resetOtpToEmpty = () => {
  return {
    type: types.RESET_OTP_CODE_TO_EMPTY
  }
}
