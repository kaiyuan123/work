import * as types from "./actionTypes";

export const clearSignature = () => {
  return (dispatch) => {
    dispatch({
      type: types.CLEAR_SIGNATURE
    })
  }
}

export const saveSignature = (trimmedDataURL) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_SIGNATURE_DATA,
      trimmedDataURL
    })
  }
}
