import * as types from './actionTypes';

export const showSaveAndExitPopup = (showPopup) => {
	return {
		type: types.SHOW_SAVE_AND_EXIT_POPUP,
		showPopup
	}
}

