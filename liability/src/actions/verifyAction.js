import * as types from "./actionTypes";
// import localStore from './../common/localStore';
import { setErrorMessage, setProcessingStatus, setCurrentSection, scrollToSection } from './commonAction';
import { setErrorWithValue, setSessionExpirePopup, setInitateMyInfoResponse, isWhichTypeSTPorNSTP, isKnockOutScenario, setAccountHolderType, setWhichKnockOutScenarioFalse } from './applicationAction';
import { setRetrieveCompanyDetailsData } from "./companyBasicDetailsAction";
import { Application } from "./../api/httpApi";
import { setRetrieveContactDetailsData } from "./contactDetailsAction";
import { setOperatingMandateData } from "./operatingMandateAction";
import { setAccountSetupData } from "./accountSetupAction";
import { setShareHoldersData } from "./shareHoldersAction";
import { setasrApprovedPersonData } from "./asrDetailsAction";
import { capitalize } from './../common/utils';
import { setFilterNames, setRetrieveTaxDeclarations } from './taxSelfDeclarationsAction';
import { handleAskQuestionPopup } from './askQuestionsAction';

export const handleTextInputChange = (data, field) => {
  return (dispatch) => {
    dispatch({
      type: types.VERIFY_UEN_HANDLE_TEXT_INPUT_CHANGE,
      value: data.value,
      field,
      isValid: data.isValid,
      errorMsg: data.errorMsg,
      isInitial: data.isInitial
    });
  };
};

// set error message
export const setUENTextInputRequiredError = (field, errorMsg) => {
  return (dispatch) => {
    dispatch(setErrorMessage(""));
    dispatch({
      type: types.UEN_SET_TEXT_INPUT_REQUIRED_ERROR,
      errorMsg,
      field
    });
  };
}

export const stepToShowOTP = (isOTPPageShow) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_OTP_PAGE,
      isOTPPageShow
    });
  };
}

export const getVerificationCode = (code, redirectOnSuccess, redirectToErrorPage) => {
  return (dispatch, getState) => {
    dispatch(setProcessingStatus(true));
    Application.getVerificationCode(code).then(response => {
      dispatch(setVerifyLoanData(response.data));
      dispatch(setProcessingStatus(false));
      redirectOnSuccess && redirectOnSuccess();
    }).catch(e => {
      dispatch(setProcessingStatus(false));
      if (e.response && e.response.data.errorCode === 'AppSubmitted') {
        dispatch(setErrorWithValue("AppSubmitted"));
        redirectToErrorPage && redirectToErrorPage();
      } else if (e.response && e.response.data.errorCode === 'AppNotFound') {
        dispatch(setErrorWithValue("AppNotFound"));
        redirectToErrorPage && redirectToErrorPage();
      } else if (e.response && e.response === 403) {
        dispatch(setProcessingStatus(false));
        dispatch(setSessionExpirePopup(true));
      } else {
        dispatch(setErrorWithValue("serviceDown"));
        redirectToErrorPage && redirectToErrorPage();
      }
    });
  }
}

export const setVerifyLoanData = (companyDetailsDataObj) => {
  return {
    type: types.SET_VERIFY_APPLICATION_DATA,
    status: companyDetailsDataObj.status,
    applicationId: companyDetailsDataObj.applicationId,
    refNo: companyDetailsDataObj.referenceNumber,
    productCode: companyDetailsDataObj.productCode,
    expiryDate: companyDetailsDataObj.expiryDate,
    createdDate: companyDetailsDataObj.createdDate
  }
}

export const setSaveAndRetrieveData = (response) => {
  return (dispatch, getState) => {

    const companyDetails = response.entity;
    let screenIndex = response.type ? response.type.substring(response.type.lastIndexOf('|') + 1) : '0';
    let isKnockOut = response.type.substring(0, response.type.indexOf('|')) === "STP" ? false : true;
    const operatingMandate = response.operatingMandate;
    const mainApplicant = response.person[0];
    let accountHolderType;

    dispatch(setInitateMyInfoResponse(response));
    dispatch(setRetrieveContactDetailsData(companyDetails));
    dispatch(setRetrieveCompanyDetailsData(companyDetails));
    dispatch(setAccountSetupData(response.accountSetup, response.entity));
    dispatch(isWhichTypeSTPorNSTP(response.type));
    dispatch(isKnockOutScenario(isKnockOut));

    if (operatingMandate) {
        accountHolderType = operatingMandate.approvedSignatories.filter(function (user) {
            return user.legalId === mainApplicant.basicInfo.legalId
        });
        dispatch(setAccountHolderType(accountHolderType));
    }

    if (operatingMandate) {
        dispatch(setOperatingMandateData(operatingMandate, mainApplicant));
    }

    if (response.kyc && response.kyc !== [] && response.kyc.length > 0) {
        dispatch(setShareHoldersData(response.kyc));
    }

    if (response.asr) {
        dispatch(setasrApprovedPersonData(response.asr));
    }

    if (response.taxDeclarations) {
        let items = [];
        const shareholders = response && response.entity && response.entity.shareholders ? response.entity.shareholders : [];
        if (shareholders !== []) {
            shareholders.map((item) => {
                if (item.personReference !== null) {
                    items.push({
                        name: item.personReference.personName ? item.personReference.personName === null ? '' : capitalize(item.personReference.personName) : '',
                        legalId: item.personReference.idno ? item.personReference.idno === null ? '' : item.personReference.idno : '',
                        mobileNumber: '+65',
                        emailAddress: '',
                        nationality: item.personReference.nationality ? item.personReference.nationality === null ? '' : item.personReference.nationality : ''
                    })
                }
                return items;
            })
        }
        const appointments = response && response.entity && response.entity.appointments ? response.entity.appointments : [];
        if (appointments !== []) {
            appointments.map((item) => {
                if (item.personReference !== null) {
                    items.push({
                        name: item.personReference.personName ? item.personReference.personName === null ? '' : capitalize(item.personReference.personName) : '',
                        legalId: item.personReference.idno ? item.personReference.idno === null ? '' : item.personReference.idno : '',
                        mobileNumber: '+65',
                        emailAddress: '',
                        nationality: item.personReference.nationality ? item.personReference.nationality === null ? '' : item.personReference.nationality : ''
                    })
                }
                return items;
            })
        }

        dispatch(setFilterNames(items));
        dispatch(setRetrieveTaxDeclarations(response.taxDeclarations));
    }

    switch (screenIndex) {
      case "1":
          dispatch(setCurrentSection('companyDetails'));
          dispatch(scrollToSection("companyDetails"));
          break;

      case "2":
          if (response.questions) {
              dispatch(setCurrentSection('accountSetup'));
              dispatch(scrollToSection("accountSetup"));
          } else {
              dispatch(setCurrentSection('companyDetails'));
              dispatch(scrollToSection("companyDetails"));
              const questionObj = getState().commonReducer.appData.askQuestion.questions;
              dispatch(handleAskQuestionPopup(true, questionObj));
          }
          break;

      case "3":
          if (!isKnockOut) {
              dispatch(setCurrentSection('operatingMandate'));
              dispatch(scrollToSection("operatingMandate"));
          } else {
              dispatch(setCurrentSection('confirmDetailsPage'));
              dispatch(scrollToSection("confirmDetailsPage"));
          }
          break;

      case "4":
          if (operatingMandate.signatorySelected === '3') {
              dispatch(isKnockOutScenario(true));
              dispatch(setWhichKnockOutScenarioFalse("operatingMandateComplex"));
              dispatch(setCurrentSection('confirmDetailsPage'));
              dispatch(scrollToSection("confirmDetailsPage"));
          } else {
              if (response.kyc && response.kyc !== [] && response.kyc.length > 0) {
                  dispatch(setCurrentSection('shareHolders'));
                  dispatch(scrollToSection("shareHolders"));
              } else {
                  dispatch(setCurrentSection('taxSelfDeclarations'));
                  dispatch(scrollToSection("taxSelfDeclarations"));
              }
          }
          break;

      case "5":
          dispatch(setCurrentSection('taxSelfDeclarations'));
          dispatch(scrollToSection("taxSelfDeclarations"));
          break;

      case "6":
      case "7":
      case "8":
          if (response.entity.basicProfile.entityType === 'LC') {
              dispatch(setCurrentSection('uploadDocument'));
              dispatch(scrollToSection("uploadDocument"));
          } else {
              if (response.entity.basicProfile.businessConstitution !== 'S') {
                  dispatch(setCurrentSection('asrDetails'));
                  dispatch(scrollToSection("asrDetails"));
              }
          }
          break;

      default:
          dispatch(setCurrentSection('personalIncomeDetails'));
          dispatch(scrollToSection("personalIncomeDetails"));
          break;
    }
  }
}
