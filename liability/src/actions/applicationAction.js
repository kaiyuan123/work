import * as types from "./actionTypes";
import { Application, LocalEnvironment, Config, uobSite } from "./../api/httpApi";
// import { getValueByCode } from "../common/utils";
import { setMyInfoFlowStatus, setProcessingStatus, setErrorMessage, setRetrievingMyInfo, setApplicationReferenceNo, setCurrentSection, setLoadingStatus, setLoggedInCompanyInfo, setInlineErrorMessage, scrollToSection, setExpiryDate, setApplicationStatus } from "./commonAction";
import { setMyInfoContactDetailsData, setRetrieveContactDetailsData } from "./contactDetailsAction";
import { setMyInfoPersonalIncomeDetailsData, setIncomeDetailData } from "./personalIncomeDetailsAction";
import { setResidentialAddress, setCompanyAddressData, setCompanyUnformattedAddress } from "./localAddressInputAction";
import { setCompanyBasicData, setCompanyAddressType, setRetrieveCompanyDetailsData } from "./companyBasicDetailsAction";
import { setCompanyPreviousNamesData, setCompanyPreviousUENData, setCompanyCapitalsData, setCompanyAppointmentsData, setCompanyShareholdersData, setCompanyFinancialsData, setCompanyGrantsData } from "./companyOverallDetailsAction";
import { handlePopupHideOnOkButton, handleAskQuestionPopup } from './askQuestionsAction';
import { setOperatingMandateData } from "./operatingMandateAction";
import { setasrApprovedPersonData } from "./asrDetailsAction";
import { setShareHoldersData } from "./shareHoldersAction";
import { setAccountSetupData } from "./accountSetupAction";
import { setRetrieveTaxDeclarations } from "./taxSelfDeclarationsAction";
import { setUENTextInputRequiredError, stepToShowOTP, setSaveAndRetrieveData } from "./verifyAction"
import { setOtpPrefix } from "./otpAction"
// import { validateNRIC } from "./../common/validations";
import { capitalize } from './../common/utils';
// import localStore from "./../common/localStore";
import { setFilterNames } from './taxSelfDeclarationsAction';
import { showErrorMessage,saveSignatureData, setSignatureUploadStatus, setPopupStatus, setSignatureUploadIsValidStatus, setUploadSignatureSaveImage, setDrawSignatureSaveImage } from './signatureAction';
import queryString from 'query-string';

//Retrieve MY Info
export const retrieveMyInfoDetails = (redirectToErrorPage) => {
    return (dispatch, getState) => {
        const response = getState().retrievingReducer.myInfoData;
        // console.log("MyInfo", response);
        dispatch(setRetrievingMyInfo(true));
        if (response !== null && response.entity && response.entity !== null && response.person && response.person !== null) {
            Promise.resolve().then(() => {
                const companyDetails = response.entity;
                const residentialAddress = response.person[0].addresses[0];
                dispatch(setMyInfoContactDetailsData(response.person, companyDetails));
                dispatch(setMyInfoPersonalIncomeDetailsData(response.person, residentialAddress));
                dispatch(setIncomeDetailData(response.person));
                dispatch(setResidentialAddress(residentialAddress))
                dispatch(setCompanyBasicData(response.entity.basicProfile));
                dispatch(setCompanyPreviousNamesData(response.entity.previousNames));
                dispatch(setCompanyPreviousUENData(response.entity.previousUens));
                dispatch(setCompanyCapitalsData(response.entity.capitals));
                dispatch(setCompanyAppointmentsData(response.entity.appointments));
                dispatch(setCompanyShareholdersData(response.entity.shareholders));
                dispatch(setCompanyFinancialsData(response.entity.financials));
                dispatch(setCompanyGrantsData(response.entity.grants));
                dispatch(setCompanyAddressType(response.entity.addresses[0].standard));
                if (response.entity.addresses[0].standard === "N" || response.entity.addresses[0].standard === "L") {
                    dispatch(setCompanyUnformattedAddress(response.entity.addresses[0]));
                } else {
                    dispatch(setCompanyAddressData(response.entity.addresses[0]));
                }

                if (response.entity.basicProfile.ownership !== "1") {
                    dispatch(setWhichKnockOutScenarioFalse("ownership"));
                    dispatch(isKnockOutScenario(true));
                }

                const isRetrieveApplication = getState().applicationReducer.isRetrieveApplication;
                if (isRetrieveApplication) {
                    dispatch(setSaveAndRetrieveData(response));
                }
            }).then(() => {
                dispatch(setRetrievingMyInfo(false));
                dispatch(setApplicationDataLoaded());
                dispatch(setMyInfoFlowStatus(true));
            });
        } else {
            Promise.resolve().then(() => {
                dispatch(setErrorWithValue("NOREF"));
            }).then(() => {
                redirectToErrorPage && redirectToErrorPage();
            });
        }
    };
};

export const setApplicationDataLoaded = () => {
    return {
        type: types.SET_APPLICATION_DATA_LOADED
    };
};

export const setAvailableLocationData = (applicationID) => {
    return (dispatch) => {
        Application.getAvailableLocationData(applicationID).then(response => {
            Promise.resolve().then(() => {
                dispatch({
                    type: types.SET_AVAILABLE_LOCATION_DATA,
                    response: response.data
                })
            });
        }).catch(e => {
            dispatch(setErrorMessage('Available Location Data Error'));
        });
    }
};

//Initiate Application
export const submitInitiateApplication = (dataObj, handleToPersonalIncomeDetails, redirectToErrorPage) => {
    return (dispatch, getState) => {
        const errorMsg = getState().commonReducer.appData.globalErrors.apiException;
        const parameter = getState().applicationReducer.startingParameter;
        const params = queryString.parse(parameter);
        // const jsonData = getState().commonReducer.appData;
        // get data from state
        const requiredParameterArray = getState().commonReducer.appData.dataElementObj.requiredParameter;
        const parameterLength = (parseInt(getState().commonReducer.appData.maxLengthParams, 10) - 1);
        let requiredParameter= [];
        requiredParameterArray.map((i) => {
            const conCatString = params[i] ? `${i}=${params[i]}` : '';
            if(conCatString !== '') {
              requiredParameter.push(conCatString);
            }
            return requiredParameter;
        });
        const parameterNeed = `?${requiredParameter.join('&').substring(0, parameterLength)}`;

        dispatch(setProcessingStatus(true));
        if (LocalEnvironment) {
            Config.getInitiateMyinfoData().then(response => {
                Promise.resolve()
                    .then(() => {
                        const data = response;
                        const operatingMandate = data.operatingMandate;
                        const mainApplicant = data.person[0];
                        let accountHolderType;
                        if (operatingMandate) {
                            accountHolderType = operatingMandate.approvedSignatories.filter(function (user) {
                                return user.legalId === mainApplicant.basicInfo.legalId
                            });
                            dispatch(setAccountHolderType(accountHolderType));
                        }
                        dispatch(setInitateMyInfoResponse(data));
                        if (operatingMandate) {
                            dispatch(setOperatingMandateData(operatingMandate, mainApplicant));
                        }
                        dispatch(setAccountSetupData(data.accountSetup, data.entity));
                        if (data.kyc && data.kyc !== [] && data.kyc.length > 0) {
                            dispatch(setShareHoldersData(data.kyc));
                        }
                        if (data.asr) {
                            dispatch(setasrApprovedPersonData(data.asr));
                        }
                        const isKnockOut = data.type === "STP" ? false : true;
                        dispatch(isWhichTypeSTPorNSTP(data.type));
                        dispatch(isKnockOutScenario(isKnockOut));

                        let items = [];
                        const shareholders = data && data.entity && data.entity.shareholders ? data.entity.shareholders : [];
                        if (shareholders !== []) {
                            shareholders.map((item) => {
                                if (item.personReference !== null) {
                                    items.push({
                                        name: item.personReference.personName ? item.personReference.personName === null ? '' : capitalize(item.personReference.personName) : '',
                                        legalId: item.personReference.idno ? item.personReference.idno === null ? '' : item.personReference.idno : '',
                                        mobileNumber: '+65',
                                        emailAddress: '',
                                        nationality: item.personReference.nationality ? item.personReference.nationality === null ? '' : item.personReference.nationality : ''
                                    })
                                }
                                return items;
                            })
                        }
                        const appointments = data && data.entity && data.entity.appointments ? data.entity.appointments : [];
                        if (appointments !== []) {
                            appointments.map((item) => {
                                if (item.personReference !== null) {
                                    items.push({
                                        name: item.personReference.personName ? item.personReference.personName === null ? '' : capitalize(item.personReference.personName) : '',
                                        legalId: item.personReference.idno ? item.personReference.idno === null ? '' : item.personReference.idno : '',
                                        mobileNumber: '+65',
                                        emailAddress: '',
                                        nationality: item.personReference.nationality ? item.personReference.nationality === null ? '' : item.personReference.nationality : ''
                                    })
                                }
                                return items;
                            })
                        }

                        dispatch(setFilterNames(items));

                    }).then(() => {
                        // localStore.clear();
                        dispatch(setProcessingStatus(false));
                        handleToPersonalIncomeDetails && handleToPersonalIncomeDetails();
                    })
            }).catch(e => {
                dispatch(setProcessingStatus(false))
                dispatch(setErrorMessage(errorMsg));
            })
        } else {
            Application.submitInitiateApplication(dataObj, parameterNeed).then(response => {
                const data = response.data;
                Promise.resolve()
                    .then(() => {
                        const operatingMandate = data.operatingMandate;
                        const mainApplicant = data.person[0];
                        let accountHolderType;
                        if (operatingMandate) {
                            accountHolderType = operatingMandate.approvedSignatories.filter(function (user) {
                                return user.legalId === mainApplicant.basicInfo.legalId
                            });
                            dispatch(setAccountHolderType(accountHolderType));
                        }
                        dispatch(setInitateMyInfoResponse(data));
                        if (operatingMandate) {
                            dispatch(setOperatingMandateData(operatingMandate, mainApplicant));
                        }
                        dispatch(setAccountSetupData(data.accountSetup, data.entity));

                        if (data.kyc && data.kyc !== [] && data.kyc.length > 0) {
                            dispatch(setShareHoldersData(data.kyc));
                        }
                        if (data.asr) {
                            dispatch(setasrApprovedPersonData(data.asr));
                        }

                        const isKnockOut = data.type === "STP" ? false : true;
                        dispatch(isWhichTypeSTPorNSTP(data.type));
                        dispatch(isKnockOutScenario(isKnockOut));

                        let items = [];
                        const shareholders = data && data.entity && data.entity.shareholders ? data.entity.shareholders : [];
                        if (shareholders !== []) {
                            shareholders.map((item) => {
                                if (item.personReference !== null) {
                                    items.push({
                                        name: item.personReference.personName ? item.personReference.personName === null ? '' : capitalize(item.personReference.personName) : '',
                                        legalId: item.personReference.idno ? item.personReference.idno === null ? '' : item.personReference.idno : '',
                                        mobileNumber: '+65',
                                        emailAddress: '',
                                        nationality: item.personReference.nationality ? item.personReference.nationality === null ? '' : item.personReference.nationality : ''
                                    })
                                }
                                return items;
                            })
                        }
                        const appointments = data && data.entity && data.entity.appointments ? data.entity.appointments : [];
                        if (appointments !== []) {
                            appointments.map((item) => {
                                if (item.personReference !== null) {
                                    items.push({
                                        name: item.personReference.personName ? item.personReference.personName === null ? '' : capitalize(item.personReference.personName) : '',
                                        legalId: item.personReference.idno ? item.personReference.idno === null ? '' : item.personReference.idno : '',
                                        mobileNumber: '+65',
                                        emailAddress: '',
                                        nationality: item.personReference.nationality ? item.personReference.nationality === null ? '' : item.personReference.nationality : ''
                                    })
                                }
                                return items;
                            })
                        }

                        dispatch(setFilterNames(items));
                        dispatch(setExpiryDate(data.expiry));
                    }).then(() => {
                        dispatch(setProcessingStatus(false));
                        handleToPersonalIncomeDetails && handleToPersonalIncomeDetails();
                    })
            }).catch(e => {
                if (e.response && e.response.status === 403) {
                    dispatch(setProcessingStatus(false));
                    //dispatch(setErrorWithValue("ConcurrentLogin"));
                    //redirectToErrorPage && redirectToErrorPage();
                    dispatch(setSessionExpirePopup(true));
                } else {
                    dispatch(setProcessingStatus(false))
                    dispatch(setErrorMessage(errorMsg));
                }
            })
        }

    };
};

//Save Application
export const submitPartialApplication = (dataObj, applicationID, redirectToErrorPage) => {
    return (dispatch) => {
        Application.submitPartialApplication(dataObj, applicationID).catch(e => {
            if (e.response && e.response.status === 403) {
                //dispatch(setErrorWithValue("ConcurrentLogin"));
                //redirectToErrorPage && redirectToErrorPage();
                dispatch(setSessionExpirePopup(true));
            }
        });
    }
}

//Save Upload document
export const saveUploadDocument = (dataObj, applicationID, img, position) => {
	return (dispatch, getState) => {
		const errorMsg = getState().commonReducer.appData.globalErrors.apiException;
    const currentSection = getState().commonReducer.currentSection;
    const desktop = getState().commonReducer.isMobile;
    const image = getState().signatureReducer.image;
    const isSign = getState().signatureReducer.isSign;
		dispatch(setErrorMessage(""));
		dispatch(setProcessingStatus(true))
		Application.saveUploadDocument(dataObj, applicationID).then(response => {
			Promise.resolve()
				.then(() => {
					dispatch(setProcessingStatus(false));
					dispatch(setErrorMessage(""));
					(!desktop || !isSign) && dispatch(saveSignatureData(img));
          currentSection === "confirmDetailsPage" && dispatch(setPopupStatus(false));
          currentSection === "confirmDetailsPage" && dispatch(setSignatureUploadStatus(true));
          currentSection === "confirmDetailsPage" && dispatch(setSignatureUploadIsValidStatus(true));
          currentSection === "confirmDetailsPage" && !isSign && dispatch(setUploadSignatureSaveImage(image, position));
          currentSection === "confirmDetailsPage" && isSign && dispatch(setDrawSignatureSaveImage(img));
				})
		}).catch(e => {
			dispatch(setProcessingStatus(false));
			dispatch(setErrorMessage(errorMsg));
			// dispatch(showErrorMessage(errorMsg));
		});
	};
};

//Book An Appointment
export const saveBookAppointment = (dataObj, applicationID) => {
    return dispatch => {
        Application.saveBookAppointment(dataObj, applicationID);
    };
};

//Questions Submission
export const askQuestionsSubmission = (dataObj, applicationID, ) => {
    if (LocalEnvironment) {
        return dispatch => {
            Application.askQuestionsSubmission(dataObj, applicationID);
            dispatch(handlePopupHideOnOkButton(false));
            dispatch(setProcessingStatus(false));
            dispatch(scrollToSection("accountSetup"));
        };
    } else {
        return (dispatch, getState) => {
            const errorMsg = getState().commonReducer.appData.globalErrors.apiException;
            Application.askQuestionsSubmission(dataObj, applicationID).then(response => {
                Promise.resolve()
                    .then(() => {
                        dispatch(handlePopupHideOnOkButton(false));
                        dispatch(setProcessingStatus(false));
                        dispatch(scrollToSection("accountSetup"));
                    })
            }).catch(e => {
                dispatch(setProcessingStatus(false));
                dispatch(setErrorMessage(errorMsg));
            });

        };
    }

};

export const submitSaveAndExitApplication = (dataObj, applicationID) => {
    return (dispatch, getState) => {
        const errorMsg = getState().commonReducer.appData.globalErrors.apiException;
        Application.submitPartialApplication(dataObj, applicationID).then(response => {
            Promise.resolve()
                .then(() => {
                    dispatch(setProcessingStatus(false));
                    window.location.href = uobSite;
                })
        }).catch(e => {
            dispatch(setProcessingStatus(false));
            dispatch(setErrorMessage(errorMsg));
        });
    };
};

export const retrieveUENSubmission = (redirectToErrorPage, dataObj) => {
    return (dispatch, getState) => {
        dispatch(setProcessingStatus(true));
        const globalErrors = getState().commonReducer.appData.globalErrors;

        dispatch(setInlineErrorMessage(""));
        dispatch(setErrorMessage(""));
        Application.retrieveUENSubmission(dataObj).then(response => {
            dispatch(setLoggedInCompanyInfo(dataObj));
            Application.sendOtp().then((res) => {
                const data = res.data;
                dispatch(setOtpPrefix(data.prefix));
                dispatch(stepToShowOTP(true));
                dispatch(setProcessingStatus(false));
            }).catch(e => {
                dispatch(setProcessingStatus(false));
                dispatch(setErrorMessage(globalErrors.apiException));
            });
        }).catch(e => {
            if (e.response && (e.response.status === 401)) {
                dispatch(setInlineErrorMessage(globalErrors.unauthorized));
                dispatch(setUENTextInputRequiredError("uen", ""));
            } else if (e.response && e.response.status === 302) {
                dispatch(setErrorMessage(globalErrors.concurrentLogin));
                window.open(e.response.location, '_parent');
            } else if (e.response && e.response.data.errorCode === 'AppSubmitted') {
                dispatch(setInlineErrorMessage(globalErrors.appSubmitted));
            } else if (e.response && e.response.data.errorCode === 'AppNotFound') {
                dispatch(setInlineErrorMessage(globalErrors.unauthorized));
            } else if (e.response && e.response.data.errorCode === 'BranchAppoinmentTaken') {
                dispatch(setInlineErrorMessage(globalErrors.appSubmitted));
            } else if (e.response && e.response === 403) {
                dispatch(setProcessingStatus(false));
                dispatch(setSessionExpirePopup(true));
            } else {
                dispatch(setErrorWithValue("serviceDown"));
                redirectToErrorPage && redirectToErrorPage();
            }
            dispatch(setProcessingStatus(false));
        });

    };
}

export const setSessionExpirePopup = (showPopup) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_SESSIONEXPIRE_POPUP,
            showPopup
        })
    }
}

export const setMyInfoResponse = (response) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_MYINFO_RESPONSES,
            response
        })
    }
}

export const setInitateMyInfoResponse = (response) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_INITIATE_MYINFO_RESPONSES,
            response
        })
    }
}

export const setAccountHolderType = (accountHolderType) => {
    let isKeyManApply = accountHolderType.length > 0;
    return (dispatch) => {
        dispatch({
            type: types.SET_ACCOUNT_HOLDER_TYPE,
            isKeyManApply
        })
    }
}

export const submitApplication = (dataObj, applicationId, redirectToErrorPage) => {
    return (dispatch) => {
        dispatch(setLoadingStatus(true));
        /*const referenceNo = getState().applicationReducer.initiateMyinfoData.referenceNumber;
        dispatch(setApplicationReferenceNo(referenceNo));
        dispatch(setCurrentSection('thankyou'));*/
        dispatch(setDataObj(dataObj));
        Application.submitApplication(dataObj, applicationId).then(response => {
            const referenceNo = response.data.referenceNumber;
            const status = response.data.status;
            dispatch(setApplicationReferenceNo(referenceNo));
            dispatch(setApplicationStatus(status));
            dispatch(setLoadingStatus(false));
            if (referenceNo !== null && referenceNo !== undefined && referenceNo !== '') {
                dispatch(setCurrentSection('thankyou'));
            } else {
                dispatch(setErrorWithValue("NOREF"));
                redirectToErrorPage && redirectToErrorPage();
            }
        }).catch(e => {
          if (e.response && e.response.data.errorCode && e.response.data.errorCode !== '') {
            dispatch(setErrorWithValue(e.response.errorCode));
            redirectToErrorPage && redirectToErrorPage();
          } else if ( e.response && e.response.status ) {
            dispatch(setErrorWithValue(`${e.response.status}Error`));
            redirectToErrorPage && redirectToErrorPage();
          } else {
            dispatch(setErrorWithValue("serviceDown"));
            redirectToErrorPage && redirectToErrorPage();
          }
          dispatch(setLoadingStatus(false));
        })
    }
}

export const setErrorWithValue = (errorCode) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ERROR_MESSAGE_WITH_ERRORCODE,
            errorCode
        })
    }
}

export const setErrorWithDescription = (description) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ERROR_MESSAGE_WITH_DESCRIPTION,
            description
        })
    }
}

export const storeParameter = (parameter, productCode) => {
    return (dispatch) => {
        dispatch({
            type: types.STORE_PARAMETER,
            parameter,
            productCode
        })
    }
}

export const isWhichTypeSTPorNSTP = (status) => {
    return (dispatch) => {
        dispatch({
            type: types.WHICH_TYPE,
            status
        })
    }
}

export const isKnockOutScenario = (isKnockOut) => {
    return (dispatch) => {
        dispatch({
            type: types.IS_KNOCKOUT_SCENARIO,
            isKnockOut
        })
    }
}

export const setWhichKnockOutScenarioFalse = (field) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_WHICH_KNOCKOUT_SCENARIO_FAIL,
            field
        })
    }
}


export const setDataObj = (dataObj) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_DATA_OBJ,
            dataObj
        })
    }
}
