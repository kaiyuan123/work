import * as types from './actionTypes';
import { Address } from './../api/httpApi';
import { setErrorMessage } from './commonAction';
import { capitalize } from "./../common/utils";

export const handleAddressInputChange = (data, field) => {
  return (dispatch, getState) => {
    dispatch({
      type: types.LOCAL_ADRESS_INPUT_CHANGE,
      value: data.value,
      field,
      isValid: data.isValid,
      errorMsg: data.errorMsg,
      isInitial: data.isInitial
    });
  };
}

export const autoGetLocalAddress = (value, addressType, field) => {
  return (dispatch, getState) => {
    const postalCode = getState().localAddressInputReducer[field];
    //const isRetrieveApplication = getState().applicationReducer.isRetrieveApplication;
    // if(isRetrieveApplication){
    //   postalCode.count = 0;
    // }
    if (postalCode.isValid && postalCode.count < 5) {
      //auto get and fill block, street and house no
      dispatch(isGettingLocalAddress(true, addressType));
      Address.getAddressByCode(value).then((res) => {
        if (res.status === 200) {
          const address = res.data;
          dispatch({
            type: types.LOCAL_ADDRESS_INPUT_AUTO_FILL_IN,
            addressType,
            street: address.streetAddress ? capitalize(address.streetAddress) : "",
            block: address.blockNumber,
            building: address.buildingName ? capitalize(address.buildingName) : "",
            addressFormat: address.addressFormat,
            count: postalCode.count + 1
          });
        } else {
          const incrementCount = postalCode.count + 1;
          dispatch(autoFillInFailed(addressType, incrementCount));
        }
      }).catch(e => {
        if (e.response && e.response.status === 400) {
          const incrementCount = postalCode.count + 1;
          dispatch(autoFillInFailed(addressType, incrementCount));
        } else {
          dispatch(setErrorMessage('Address search service is currently not available. Please fill in manually.'));
          dispatch(addressServiceDown(addressType));
        }
      });
    } else if (postalCode.isValid && postalCode.count === 5) {
      dispatch(resetAutoFillInStatus(addressType));
    }
  }
}

const autoFillInFailed = (addressType, count) => {
  return {
    type: types.LOCAL_ADDRESS_INPUT_AUTO_FILL_IN_FAIL,
    addressType,
    count
  };
}

const resetAutoFillInStatus = (addressType) => {
  return {
    type: types.LOCAL_ADDRESS_INPUT_RESET_AUTO_FILL_IN_STATUS,
    addressType
  }
}

const addressServiceDown = (addressType) => {
  return {
    type: types.LOCAL_ADDRESS_INPUT_SERVICE_DOWN,
    addressType
  }
}

const isGettingLocalAddress = (isLoading, addressType) => {
  return {
    type: types.IS_GETTING_LOCAL_ADDRESS,
    isLoading,
    addressType
  };
}

export const setLocalAddressTextInputRequiredError = (field, errorMsg) => {
  return (dispatch) => {
    dispatch({
      type: types.LOCAL_ADDRESS_SET_TEXT_INPUT_REQUIRED_ERROR,
      errorMsg,
      field
    });
  };
}

export const setResidentialAddress = (obj) => {
  return {
    type: types.SET_MYINFO_RESIDENTIAL_ADDRESS,
    residentialStreet: obj.street ? obj.street : '-',
    residentialBlock: obj.block ? obj.block : '-',
    residentialPostalCode: obj.postalCode ? obj.postalCode : '-',
    residentialLevel: obj.floor ? obj.floor : '-',
    residentialUnit: obj.unitNo ? obj.unitNo : '-',
    residentialBuilding: obj.building ? obj.building : '',
    residentialCountry: obj.country ? obj.country : '-'
  }
}

//Set to not required
export const setToNotRequired = (field) => {
  return (dispatch) => {
    dispatch({
      type: types.LOCAL_ADDRESS_SET_NOT_REQUIRED,
      field
    })
  };
}

export const setCompanyAddressData = obj => {
  return {
    type: types.SET_MYINFO_COMPANY_ADDRESS_DATA,
    companyPostalCode: obj && obj["postalCode"] ? obj["postalCode"] : "-",
    companyBlock: obj && obj["block"] ? obj["block"] : "-",
    companyStreet: obj && obj["street"] ? obj["street"] : "-",
    companyBuilding: obj && obj["building"] ? obj["building"] : "",
    companyLevel: obj && obj["floor"] ? obj["floor"] : "-",
    companyUnit: obj && obj["unitNo"] ? obj["unitNo"] : "-",
    companyCountry: obj && obj["country"] ? obj["country"] : "-",
    companyLine1: obj && obj["line1"] ? obj["line1"] : "-",
    companyLine2: obj && obj["line2"] ? obj["line2"] : "-",
    companyStandard: obj && obj["standard"] ? obj["standard"] : "",
  }
}

//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
  return (dispatch) => {
    dispatch({
      type: types.LOCAL_ADDRESS_DROPDOWN_FOCUS,
      isFocus,
      field
    });
  };
};

//choose item from dropdown
export const selectDropdownItem = (value, description, field) => {
  let standard = "N";
  if (value === "SG") {
    standard = "D";
  }
  return (dispatch) => {
    dispatch({
      type: types.LOCAL_ADDRESS_DROPDOWN_ITEM_SELECT,
      value,
      field,
      description,
      standard
    })
  };
};

export const changeSearchInputValue = (searchValue, field) => {
  return (dispatch) => {
    dispatch({
      type: types.LOCAL_ADDRESS_DROPDOWN_SEARCH_INPUT_CHANGE,
      searchValue,
      field
    });
  };
};

export const setCompanyUnformattedAddress = (obj) => {
    return {
        type: types.MYINFO_SET_COMPANY_UNFORMATTED_ADDRESS_DATA,
        companyCountry: obj && obj["country"] ? obj["country"] : "-",
        companyLine1: obj && obj["line1"] ? obj["line1"] : "-",
        companyLine2: obj && obj["line2"] ? obj["line2"] : "-",
        companyStandard: obj && obj["standard"] ? obj["standard"] : "",
    }
}
