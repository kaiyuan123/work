import * as types from "./actionTypes";

export const setCompanyBasicData = (obj) => {
    return {
        type: types.MYINFO_SET_COMPANY_BASIC_DETAIL_DATA,
        companyRegistrationNumber: obj && obj["uen"] ? obj["uen"] : "",
        registeredCompanyName: obj && obj.entityName ? obj.entityName : "",
        typeOfCompany: obj && obj["entityType"] ? obj["entityType"] : "",
        businessConstitutionType: obj && obj["businessConstitution"] ? obj["businessConstitution"] : "",
        companyStatus: obj && obj["entityStatus"] ? obj["entityStatus"] : "",
        countryOfIncorporation: obj && obj["countryOfIncorporation"] ? obj["countryOfIncorporation"] : "",
        ownership: obj && obj["ownership"] ? obj["ownership"] : "",
        registrationDate: obj && obj["registrationDate"] ? obj["registrationDate"] : "",
        companyExpiryDate: obj && obj["businessExpiryDate"] ? obj["businessExpiryDate"] : "",
        primaryActivityCode:
            obj && obj["primaryActivityCode"]
                ? obj["primaryActivityCode"]
                : "",
        primaryActivityDesc:
            obj && obj["primaryActivityDesc"]
                ? obj["primaryActivityDesc"]
                : "",
        secondaryActivityCode: obj && obj["secondaryActivityCode"] ? obj["secondaryActivityCode"] : "",
        secondaryActivityDesc: obj && obj["secondaryActivityDesc"] ? obj["secondaryActivityDesc"] : "",

    };
};

// export const setCompanyUnformattedAddress = (obj) => {
//     return {
//         type: types.MYINFO_SET_COMPANY_UNFORMATTED_ADDRESS_DATA,
//         unformattedCompanyCountry: obj && obj["country"] ? obj["country"] : "",
//         companyAddressLine1: obj && obj["line1"] ? obj["line1"] : "",
//         companyAddressLine2: obj && obj["line2"] ? obj["line2"] : ""
//     }
// }

export const setCompanyAddressType = (standard) => {
    return {
        type: types.MYINFO_SET_COMPANY_ADDRESS_TYPE,
        standard: standard
    }
}

export const handleIsChecked = (field, isToggled) => {
    return {
        type: types.LENDING_IS_CHECKED,
        field,
        isToggled: !isToggled
    }
}

export const handleIsCheckedClientele = (field, isToggled, text, isValid) => {
    return (dispatch, getState) => {
        const primaryClientele = getState().companyBasicDetailsReducer.primaryClientele;
        if (field === "individualClients" || field === "corporateClients") {
            if (!isToggled) {
                primaryClientele.push(text);
                dispatch(checkPrimaryClienteleIsValid(true))
            } else {
                primaryClientele.splice(primaryClientele.indexOf(text), 1);
                if (primaryClientele.length < 1) {
                    dispatch(checkPrimaryClienteleIsValid(false))
                }
                else {
                    dispatch(checkPrimaryClienteleIsValid(true))
                }
            }
        }

        dispatch({
            type: types.PRIMARY_CLIENTELE_HANDLE_TOGGLE,
            field,
            isToggled: !isToggled,
            isValid: !isValid,
            primaryClientele
        })
    }

};

export const checkPrimaryClienteleIsValid = (status) => {
    return (dispatch) => {
        dispatch({
            type: types.PRIMARY_CLIENTELE_IS_VALID,
            status
        });
    };
};

//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
    return (dispatch) => {
        dispatch({
            type: types.MAILINGADDRESS_DROPDOWN_FOCUS,
            isFocus,
            field
        });
    };
}

//choose item from dropdown
export const selectDropdownItem = (value, field, description) => {
    if (field === "mailingCountry") {
        const element = document.getElementById("editableDropdown-flagImg");
        element.style.display = "block";
    }

    return (dispatch) => {
        dispatch({
            type: types.MAILINGADDRESS_DROPDOWN_ITEM_SELECT,
            value,
            field,
            description
        })
    };
}

export const handleTextInputChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.COMPANYBASIC_HANDLE_TEXT_INPUT_CHANGE,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
};

export const changeSearchInputValue = (searchValue, field) => {
    return (dispatch) => {
        dispatch({
            type: types.COMPANYBASICDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE,
            searchValue,
            field
        });
    };
}

export const selectRadioItem = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.COMPANY_SELECT_RADIO_BUTTON,
            value: data.value,
            field
        })
    }
}

export const setRetrieveCompanyDetailsData = (companyDetailsDataObj) => {
    return (dispatch) => {
        const localAddress = companyDetailsDataObj.addresses[1] ? companyDetailsDataObj.addresses[1]:'';
        const primaryClienteleBase = companyDetailsDataObj.primaryClienteleBase ? companyDetailsDataObj.primaryClienteleBase : '';
        let individualToggle, corporateToggle;
        if(localAddress){
            dispatch(setRetrieveMailingAddress(localAddress));
        }
        if (primaryClienteleBase) {
            primaryClienteleBase.map((base, i) => {
                if (base === 'Individual') {
                    individualToggle = true;
                } else if (base === 'Corporate') {
                    corporateToggle = true;
                } 
                return true;
            });
        }
        dispatch({
            type: types.SET_RETRIEVE_COMPANY_DETAILS,
            annualTurnover: companyDetailsDataObj && companyDetailsDataObj.annualTurnover ? companyDetailsDataObj.annualTurnover : '',
            primaryClienteleBase: primaryClienteleBase ? primaryClienteleBase : [],
            individualClients: individualToggle ? individualToggle : false,
            corporateClients: corporateToggle ? corporateToggle : false,
            mailingAddressCheckbox: companyDetailsDataObj.addresses[1] ? true : false,
        });

    }
}

export const setRetrieveMailingAddress = (localAddress) => {
    let autoFilled;
    if(localAddress.postalCode === ''){
        autoFilled = false;
    }else{
       autoFilled = true;
    }
    return {
        type:types.SET_RETRIEVE_LOCAL_ADDRESS,
        mailingUnit: localAddress.unitNo ? localAddress.unitNo : '',
        mailingStreet: localAddress.street ? localAddress.street : '',
        mailingBlock: localAddress.block ? localAddress.block : '',
        mailingPostalCode: localAddress.postalCode ? localAddress.postalCode : '',
        count: 0,
        autoFilled: autoFilled,
        mailingLevel: localAddress.floor ? localAddress.floor : '',
        mailingBuilding: localAddress.building ? localAddress.building : '',
        mailingCountry: localAddress.country ? localAddress.country : '',
        mailingCity: localAddress.city ? localAddress.city : '',
        mailingLine1: localAddress.line1 ? localAddress.line1 : '',
        mailingLine2: localAddress.line2 ? localAddress.line2 : '',
        mailingLine3: localAddress.line3 ? localAddress.line3 : '',
        mailingLine4: localAddress.line4 ? localAddress.line4 : '',
        addressType: localAddress.type ? localAddress.type : ''
    }
}
