import * as types from "./actionTypes";
import { capitalize } from "../common/utils";
import { setTextInputHasEditedWithKey, setCurrentSection } from "./commonAction";
import { isKnockOutScenario, setWhichKnockOutScenarioFalse } from "./applicationAction";

export const setOperatingMandateData = (operatingMandateObj, mainApplicantObj) => {
	return (dispatch) => {
		dispatch(populateSignatoryList(operatingMandateObj.approvedSignatories[0], 0));
		if (operatingMandateObj.approvedSignatories.length > 1) {
			for (let i = 1; i < operatingMandateObj.approvedSignatories.length; i++) {
				dispatch(populateAdditionalSignatoryList(operatingMandateObj.approvedSignatories[i], i));
				dispatch(incrementSignatoryList(i));
			}
		}
		dispatch({
			type: types.APPROVED_SIGNATORY_LIST,
			signatories: operatingMandateObj.approvedSignatories,
			signatorySelected: operatingMandateObj.signatorySelected ? operatingMandateObj.signatorySelected : '',
			mainApplicantLegalId: mainApplicantObj.basicInfo.legalId,
		})
	}
}

export const populateSignatoryList = (obj, key) => {
	return (dispatch, getState) => {
		let setupBIBPlusUser = getState().accountSetupReducer.setupBIBPlus.isToggled;
		let isRetrieveApplication = getState().applicationReducer.isRetrieveApplication;
		const name = capitalize(obj.name);
		const nric = obj.legalId;
		const email = obj.emailAddress ? obj.emailAddress : "";
		const phone = obj.mobileNumber ? obj.mobileNumber : "";
		const userId = obj.userId ? obj.userId : "";
		const bibPlusUserInd = obj.bibPlusUserInd ? obj.bibPlusUserInd : '';
		let bibUserToggle = "";
		if (bibPlusUserInd) {
			bibUserToggle = bibPlusUserInd === 'Y' ? true : false;
		}
		//isRetrieveApplication = true;
		if (userId === "") {
			if (setupBIBPlusUser) {
				if (isRetrieveApplication) {
					// bibUserToggle = bibUserToggle;
				} else {
					bibUserToggle = true
				}
			} else {
				if (isRetrieveApplication) {
					// bibUserToggle = bibUserToggle;
				} else {
					bibUserToggle = false
				}

			}
		} else {
			bibUserToggle = true
		}
		dispatch({
			type: types.POPULATE_SIGNATORY_LIST,
			key: `signatories${key}`,
			signatoriesName: name,
			signatoriesNRIC: nric,
			signatoriesEmail: email,
			signatoriesMobileNo: phone !== "" ? phone : "+65",
			signatoriesBibUserId: userId,
			signatoriesToggleBIBUser: bibUserToggle,
			phoneMyinfo: phone !== "" ? true : false,
			emailMyinfo: email !== "" ? true : false,
			bibUserIdMyinfo: userId !== "" ? true : false
		})
	}
}

export const incrementSignatoryList = (key) => {
	return (dispatch, getState) => {
		let signatoriesID = getState().operatingMandateReducer.signatoriesID;
		signatoriesID.push(key);
		dispatch({
			type: types.INCREMENT_SIGNATORYLIST_COUNT,
			signatoriesID
		});
	};
}

//Populate other Signatory List items
export const populateAdditionalSignatoryList = (obj, key) => {
	return (dispatch, getState) => {
		let setupBIBPlusUser = getState().accountSetupReducer.setupBIBPlus.isToggled;
		let isRetrieveApplication = getState().applicationReducer.isRetrieveApplication;
		const name = capitalize(obj.name);
		const nric = obj.legalId;
		const email = obj.emailAddress ? obj.emailAddress : "";
		const phone = obj.mobileNumber ? obj.mobileNumber : "";
		const userId = obj.userId ? obj.userId : "";
		let bibUserToggle = "";
		const bibPlusUserInd = obj.bibPlusUserInd ? obj.bibPlusUserInd : '';
		if (bibPlusUserInd) {
			bibUserToggle = bibPlusUserInd === 'Y' ? true : false;
		}
		//isRetrieveApplication = true;
		if (userId === "") {
			if (setupBIBPlusUser) {
				if (isRetrieveApplication) {
					// bibUserToggle = bibUserToggle;
				} else {
					bibUserToggle = true
				}
			} else {
				if (isRetrieveApplication) {
					// bibUserToggle = bibUserToggle;
				} else {
					bibUserToggle = false
				}

			}
		} else {
			bibUserToggle = true
		}

		dispatch({
			type: types.POPULATE_ADDITIONAL_SIGNATORY_LIST,
			signatories: `signatories${key}`,
			signatoriesName: name,
			signatoriesNRIC: nric,
			signatoriesEmail: email,
			signatoriesMobileNo: phone !== "" ? phone : "+65",
			signatoriesBibUserId: userId,
			signatoriesToggleBIBUser: bibUserToggle,
			phoneMyinfo: phone !== "" ? true : false,
			emailMyinfo: email !== "" ? true : false,
			bibUserIdMyinfo: userId !== "" ? true : false
		})
	}
}


export const handleIsSignatoryChecked = (data, field) => {
	return (dispatch, getState) => {
		const currentSection = getState().commonReducer.currentSection;
		const normalSectionArr = getState().operatingMandateReducer.normalSectionArr;
		const complexSectionArr = getState().operatingMandateReducer.complexSectionArr;
		const setReviewSection = getState().operatingMandateReducer.setReviewSection;

		if (currentSection !== 'confirmDetailsPage' || !setReviewSection) {
			normalSectionArr.push(currentSection);
		}
		if (data.value === '3') {
			dispatch(isKnockOutScenario(true));
			dispatch(setWhichKnockOutScenarioFalse("operatingMandateComplex"));
			if (complexSectionArr.length > 0) {
				dispatch(setCurrentSection('confirmDetailsPage'));
			} else {
				dispatch(setCurrentSection('operatingMandate'));
			}

		} else {
			if (normalSectionArr.length > 1) {
				dispatch(setCurrentSection(normalSectionArr[normalSectionArr.length - 1]));
			} else {
				dispatch(setCurrentSection('operatingMandate'));
			}
			dispatch(isKnockOutScenario(false));
		}

		dispatch({
			type: types.SIGNATORY_CHECKED_VALUE,
			value: data.value,
			field
		})
	}
}

export const setReviewSection = (key) => {
	return {
		type: types.SET_REVIEW_SECTION,
		setReviewSection: key
	}
}

export const handleTextInputChange = (data, key, field) => {
	return (dispatch) => {
		dispatch({
			type: types.OPERATING_MANDATE_NAME_HANDLE_TEXT_INPUT_CHANGE,
			key,
			value: data.value,
			field,
			isValid: data.isValid,
			errorMsg: data.errorMsg,
			isInitial: data.isInitial
		});

		dispatch(setTextInputHasEditedWithKey(key, field))
	};
};

export const updateBibUserToggle = (key, isToggled) => {
	const bibUserToggle = isToggled;
	return {
		type: types.UPDATE_BIB_USER_TOGGLE,
		field: `signatories${key}`,
		isToggled: bibUserToggle,
		isValid: true
	}
}

export const handleBibPlusUserToggle = (field, isToggled) => {
	return {
		type: types.BIBPLUSEUSER_HANDLE_TOGGLE,
		field,
		isToggled: isToggled
	}
}

export const handleIsChecked = (key, field, isToggled, isValid) => {
	return {
		type: types.BIBUSER_HANDLE_TOGGLE,
		key,
		field,
		isToggled: !isToggled,
		isValid: !isValid
	}
};

// handle set error msg
export const handleSignatoriesAdditionalErrorMessage = (key, field, errorMsg, value) => {
	return (dispatch) => {
		dispatch({
			type: types.SET_SIGNATORIES_ADDITIONAL_ERROR_MESSAGE_INPUT,
			field,
			key: `signatories${key}`,
			errorMsg,
			value
		})
	}
}

export const setSignatoriesPartnersInputError = (key, field, errorMsg) => {
	return {
		type: types.SET_SIGNATORIES_PARTNERS_INPUT_ERROR,
		key,
		field,
		errorMsg
	}
}
