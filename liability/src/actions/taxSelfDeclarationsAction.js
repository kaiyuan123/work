import * as types from "./actionTypes";
import { scrollToElement, setErrorMessage } from "./../actions/commonAction";
import { Address, Application } from './../api/httpApi';

export const handleIsChecked = (field, isToggled) => {
    return {
        type: types.CRS_IS_CHECKED,
        field,
        isToggled: !isToggled
    }
}

// handle set error msg
export const handleAdditionalErrorMessage = (key, field, errorMsg) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT,
            field,
            key: `tinNoOthers${key}`,
            errorMsg
        })
    }
}

export const handleAdditionalErrorMessageCP = (key, field, errorMsg) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT_CP,
            field,
            key: `controllingPerson${key}`,
            errorMsg
        })
    }
}


export const handleAdditionalErrorMessageCPT = (cpKey, key, field, errorMsg) => {
    return (dispatch) => {
        dispatch({
            type: types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT_CPT,
            field,
            cpKey: `controllingPerson${cpKey}`,
            key: `tinNoOthers${key}`,
            errorMsg
        })
    }
}

export const uncheckRest = () => {
    return {
        type: types.CRS_UNCHECK_REST
    }
}

export const uncheckNone = () => {
    return {
        type: types.CRS_UNCHECK_NONE
    }
}

export const handleButtonChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_BUTTON_CHANGE,
            value: data,
            field
        });
    };
};


export const handleButtonChangeOthers = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_BUTTON_CHANGE_OTHERS,
            value: data,
            key,
            field
        });
    };
};

export const handleTextInputChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_CHANGE,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
};


export const handleTextInputChangeOthers = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_CHANGE_OTHERS,
            key,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
}

//handle dropdown focus
export const setDropdownFocusStatusChange = (isFocus, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_FOCUS_CHANGE,
            isFocus,
            key,
            field
        });
    };
};


//choose item from dropdown
export const selectDropdownItemChange = (value, description, key, field) => {
    return (dispatch, getState) => {
        if (field === "othersCountry") {
            const othersCountry = getState().taxSelfDeclarationsReducer[key] ? getState().taxSelfDeclarationsReducer[key].othersCountry.value : '';
            if (othersCountry !== "") {
                const element = document.getElementById("editableDropdown-flagImg");
                element.style.display = "block";
            }
        }

        if (field === "cpCoB") {
            const cpCoB = getState().taxSelfDeclarationsReducer[key] ? getState().taxSelfDeclarationsReducer[key].cpCoB.value : '';
            if (cpCoB !== "") {
                const element = document.getElementById("editableDropdown-flagImg");
                element.style.display = "block";
            }
        }

        dispatch({
            type: types.CRS_DROPDOWN_ITEM_SELECT_CHANGE,
            value,
            key,
            field,
            description
        })
    };
};

export const changeSearchInputValueOther = (searchValue, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_SEARCH_INPUT_CHANGE,
            key,
            searchValue,
            field
        });
    };
};


//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_FOCUS,
            isFocus,
            field
        });
    };
};

export const selectStatus = (value, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_SELECT_STATUS,
            value: value,
            field
        });
    };
}

//choose item from dropdown
export const selectDropdownItem = (value, description, field) => {
    return (dispatch, getState) => {
        const countryNone = getState().taxSelfDeclarationsReducer ? getState().taxSelfDeclarationsReducer.countryNone.value : '';
        if (field === "countryNone" && countryNone !== "") {
            const element = document.getElementById("editableDropdown-flagImg");
            element.style.display = "block";
        }

        dispatch({
            type: types.CRS_DROPDOWN_ITEM_SELECT,
            value,
            field,
            description
        })
    };
};

export const changeSearchInputValue = (searchValue, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_SEARCH_INPUT,
            searchValue,
            field
        });
    };
};

export const selectRadioItem = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_SELECT_RADIO_BUTTON,
            value: data.value,
            key,
            field
        })
    }
}

export const selectRadioItemNone = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_SELECT_RADIO_BUTTON_NONE,
            value: data.value,
            field
        })
    }
}

//increment count
export const incrementCountry = (key) => {
    return (dispatch, getState) => {
        let tinNoOthersId = getState().taxSelfDeclarationsReducer.tinNoOthersId;
        tinNoOthersId.push(key + 1);
        dispatch({
            type: types.CRS_INCREMENT_COUNTRY_COUNT,
            tinNoOthersId
        });
    };
}

export const addNewCountry = (key) => {
    return {
        type: types.CRS_ADD_COUNTRY,
        tinNoOthers: `tinNoOthers${key + 1}`

    }
}

//Remove partner
export const removeCountry = (key) => {
    return {
        type: types.CRS_REMOVE_COUNTRY,
        tinNoOthers: `tinNoOthers${key}`
    }
}

//decrease count
export const decreaseCountry = (key) => {
    return (dispatch, getState) => {
        let tinNoOthersId = getState().taxSelfDeclarationsReducer.tinNoOthersId;
        let index = tinNoOthersId.indexOf(key);

        dispatch({
            type: types.CRS_DECREASE_COUNTRY,
            tinNoOthersId: [...tinNoOthersId.slice(0, index), ...tinNoOthersId.slice(index + 1)]
        });
    };
}

export const setTinNoOthersInputError = (key, field, errorMsg) => {
    return (dispatch) => {
        dispatch(scrollToElement(field + key));
        dispatch({
            type: types.SET_CRS_OTHERS_INPUT_ERROR,
            key: `tinNoOthers${key}`,
            field,
            errorMsg
        })
    }
}

export const setCPInputError = (key, field, errorMsg) => {
    return (dispatch) => {
        dispatch(scrollToElement(field + key));
        dispatch({
            type: types.SET_CRS_CP_INPUT_ERROR,
            key: `controllingPerson${key}`,
            field,
            errorMsg
        })
    }
}


export const setTinNoOthersInputErrorCP = (cpKey, key, field, errorMsg) => {
    return (dispatch) => {
        dispatch(scrollToElement(field + cpKey + key));
        dispatch({
            type: types.SET_CRS_OTHERS_INPUT_ERROR_CP,
            cpKey: `controllingPerson${cpKey}`,
            key: `tinNoOthers${key}`,
            field,
            errorMsg
        })
    }
}
//increment count
export const incrementCP = (key) => {
    return (dispatch, getState) => {
        let controllingPersonId = getState().taxSelfDeclarationsReducer.controllingPersonId;
        controllingPersonId.push(key + 1);
        dispatch({
            type: types.CRS_INCREMENT_CP_COUNT,
            controllingPersonId
        });
    };
}

export const addNewCP = (key) => {
    return {
        type: types.CRS_ADD_CP,
        controllingPerson: `controllingPerson${key + 1}`

    }
}

//Remove partner
export const removeCP = (key) => {
    return {
        type: types.CRS_REMOVE_CP,
        controllingPerson: `controllingPerson${key}`
    }
}

//decrease count
export const decreaseCP = (key) => {
    return (dispatch, getState) => {
        let controllingPersonId = getState().taxSelfDeclarationsReducer.controllingPersonId;
        let index = controllingPersonId.indexOf(key);

        dispatch({
            type: types.CRS_DECREASE_CP,
            controllingPersonId: [...controllingPersonId.slice(0, index), ...controllingPersonId.slice(index + 1)]
        });
    };
}

export const checkTaxResidencyIsValid = (status) => {
    return (dispatch) => {
        dispatch({
            type: types.CHECK_TAX_RESIDENCY_IS_VALID,
            status
        });
    };
};

export const checkTaxResidencyIsValidCP = (key, status) => {
    return (dispatch) => {
        dispatch({
            type: types.CHECK_TAX_RESIDENCY_IS_VALID_CP,
            key,
            status
        });
    };
};

export const autoGetLocalAddress = (value, key) => {
    return (dispatch, getState) => {
        const postalCode = getState().taxSelfDeclarationsReducer[key].residentialPostalCode;
        // const isRetrieveApplication = getState().applicationReducer.isRetrieveApplication;
        // if (isRetrieveApplication) {
        //     postalCode.count = 0;
        // }
        if (postalCode.isValid && postalCode.count < 5) {
            //auto get and fill block, street and house no
            dispatch(isGettingLocalAddress(true, key));
            Address.getAddressByCode(value).then((res) => {
                if (res.status === 200) {
                    const address = res.data;
                    dispatch({
                        type: types.LOCAL_ADDRESS_INPUT_AUTO_FILL_IN_CP,
                        key,
                        street: address.streetAddress,
                        block: address.blockNumber,
                        building: address.buildingName,
                        addressFormat: address.addressFormat,
                        count: postalCode.count + 1
                    });
                } else {
                    const incrementCount = postalCode.count + 1;
                    dispatch(autoFillInFailed(key, incrementCount));
                }
            }).catch(e => {
                if (e.response && e.response.status === 400) {
                    const incrementCount = postalCode.count + 1;
                    dispatch(autoFillInFailed(key, incrementCount));
                } else {
                    dispatch(setErrorMessage('Address search service is currently not available. Please fill in manually.'));
                    dispatch(addressServiceDown(key));
                }
            });
        } else if (postalCode.isValid && postalCode.count === 5) {
            dispatch(resetAutoFillInStatus(key));
        }
    }
}

const isGettingLocalAddress = (isLoading, key) => {
    return {
        type: types.IS_GETTING_LOCAL_ADDRESS_CP,
        isLoading,
        key
    };
}

const autoFillInFailed = (key, count) => {
    return {
        type: types.LOCAL_ADDRESS_INPUT_AUTO_FILL_IN_FAIL_CP,
        key,
        count
    };
}

const addressServiceDown = (key) => {
    return {
        type: types.LOCAL_ADDRESS_INPUT_SERVICE_DOWN_CP,
        key
    }
}

const resetAutoFillInStatus = (key) => {
    return {
        type: types.LOCAL_ADDRESS_INPUT_RESET_AUTO_FILL_IN_STATUS_CP,
        key
    }
}

export const handleIsCheckedCP = (key, field, isToggled) => {
    return {
        type: types.CRS_IS_CHECKED_CP,
        key,
        field,
        isToggled: !isToggled
    }
}

export const handleButtonChangeCP = (data, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_BUTTON_CHANGE_CP,
            value: data,
            key,
            field
        });
    };
};

export const handleButtonChangeOthersCP = (data, cpKey, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_BUTTON_CHANGE_OTHERS_CP,
            value: data,
            cpKey,
            key,
            field
        });
    };
};

//choose item from dropdown
export const selectDropdownItemChangeCP = (value, description, cpKey, key, field) => {
    return (dispatch, getState) => {
        if (field.includes("othersCountry")) {
            const othersCountry = getState().taxSelfDeclarationsReducer[cpKey][key] ? getState().taxSelfDeclarationsReducer[cpKey][key].othersCountry.value : '';
            if (othersCountry !== "") {
                const element = document.getElementById("editableDropdown-flagImg");
                element.style.display = "block";
            }
        }

        dispatch({
            type: types.CRS_DROPDOWN_ITEM_SELECT_CHANGE_CP,
            value,
            cpKey,
            key,
            field,
            description
        })
    };
};

export const changeSearchInputValueOtherCP = (searchValue, cpKey, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_SEARCH_INPUT_CHANGE_CP,
            cpKey,
            key,
            searchValue,
            field
        });
    };
};

//handle dropdown focus
export const setDropdownFocusStatusChangeCP = (isFocus, cpKey, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_DROPDOWN_FOCUS_CHANGE_CP,
            isFocus,
            cpKey,
            key,
            field
        });
    };
};

export const handleTextInputChangeOthersCP = (data, cpKey, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_CHANGE_OTHERS_CP,
            cpKey,
            key,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
}

export const selectRadioItemCP = (data, cpKey, key, field) => {
    return (dispatch) => {
        dispatch({
            type: types.CRS_SELECT_RADIO_BUTTON_CP,
            value: data.value,
            cpKey,
            key,
            field
        })
    }
}

//increment count
export const incrementCountryCP = (cpKey, key) => {
    return (dispatch, getState) => {
        let tinNoOthersId = getState().taxSelfDeclarationsReducer[cpKey][`tinNoOthersId`];
        tinNoOthersId.push(key + 1);
        dispatch({
            type: types.CRS_INCREMENT_COUNTRY_COUNT_CP,
            cpKey,
            tinNoOthersId
        });
    };
}

export const addNewCountryCP = (cpKey, key) => {
    return {
        type: types.CRS_ADD_COUNTRY_CP,
        cpKey,
        tinNoOthers: `tinNoOthers${key + 1}`

    }
}

//Remove country CP
export const removeCountryCP = (cpKey, key) => {
    return {
        type: types.CRS_REMOVE_COUNTRY_CP,
        cpKey,
        tinNoOthers: `tinNoOthers${key}`
    }
}

//decrease count
export const decreaseCountryCP = (cpKey, key) => {
    return (dispatch, getState) => {
        let tinNoOthersId = getState().taxSelfDeclarationsReducer[cpKey][`tinNoOthersId`];
        let index = tinNoOthersId.indexOf(key);

        dispatch({
            type: types.CRS_DECREASE_COUNTRY_CP,
            cpKey,
            tinNoOthersId: [...tinNoOthersId.slice(0, index), ...tinNoOthersId.slice(index + 1)]
        });
    };
}

export const clearErrorMsgCountry = () => {
    return (dispatch, getState) => {
        const tinNoOthersId = getState().taxSelfDeclarationsReducer.tinNoOthersId;
        const errorMsg = getState().commonReducer.appData.errorMsgs.duplicateMsg;
        for (let i = 0; i < tinNoOthersId.length; i++) {
            const key = tinNoOthersId[i];
            const othersCountryErrorMsg = getState().taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.errorMsg;
            if (othersCountryErrorMsg === errorMsg) {
                dispatch({
                    type: types.CLEAR_ERROR_MSG_COUNTRY,
                    key: `tinNoOthers${key}`
                });
            }
        }
    }
}


export const clearErrorMsgCountryCP = (cpKey) => {
    return (dispatch, getState) => {
        const tinNoOthersId = getState().taxSelfDeclarationsReducer[cpKey][`tinNoOthersId`];
        const errorMsg = getState().commonReducer.appData.errorMsgs.duplicateMsg;
        for (let i = 0; i < tinNoOthersId.length; i++) {
            const key = tinNoOthersId[i];
            const othersCountryErrorMsg = getState().taxSelfDeclarationsReducer[cpKey][`tinNoOthers${key}`].othersCountry.errorMsg;
            if (othersCountryErrorMsg === errorMsg) {
                dispatch({
                    type: types.CLEAR_ERROR_MSG_COUNTRY_CP,
                    cpKey,
                    key: `tinNoOthers${key}`
                });
            }
        }
    }
}
export const clearErrorMsgCPId = () => {
    return (dispatch, getState) => {
        const controllingPersonId = getState().taxSelfDeclarationsReducer.controllingPersonId;
        const errorMsg = getState().commonReducer.appData.errorMsgs.duplicateMsg;
        for (let i = 0; i < controllingPersonId.length; i++) {
            const key = controllingPersonId[i];
            const cpIdNoErrorMsg = getState().taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.errorMsg;
            if (cpIdNoErrorMsg === errorMsg) {
                dispatch({
                    type: types.CLEAR_ERROR_MSG_CP_ID,
                    key: `controllingPerson${key}`
                });
            }
        }
    }
}

export const handleSearchInputChange = (key, data) => {
    return (dispatch, getState) => {
        const items = getState().taxSelfDeclarationsReducer.items;
        const itemsFiltered = items.filter(item => item.name.toLowerCase().includes(data.value.toLowerCase()));
        dispatch({
            type: types.CP_SEARCH_INPUT_CHANGING,
            key,
            value: data.value,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial,
            itemsFiltered
        });
    }
}

export const handleOnSelectSearchItem = (key, item) => {
    return (dispatch) => {
        dispatch({
            type: types.CP_SEARCH_RESULT_SELECT,
            key,
            item
        });
    }
}

export const handleSearchInputFocus = (key, status) => {
    return (dispatch) => {
        dispatch({
            type: types.CP_SEARCH_INPUT_FOCUS,
            key,
            status
        });
    }
}

export const setFilterNames = (items) => {
    return {
        type: types.SET_CP_FILTER,
        items
    }
}

export const setRetrieveTaxDeclarations = (obj) => {
    return (dispatch, getState) => {
        //const crsObj = taxDeclarations.crs ? taxDeclarations.crs : '';
        const controllingPersonObj = obj.controllingPerson ? obj.controllingPerson : '';
        const crs = obj.crs !== [] && obj.crs !== null ? obj.crs : "";
        let checkboxSG = false;
        let checkboxUS = false;
        let checkboxOthers = false;
        let checkboxNone = false;
        let tinNoSG = "";
        let tinNoUS = "";
        let isUSResident = "";
        let countryNone = "";
        let haveTINNoNone = "";
        let tinNoNone = "";
        let reasonsNone = "";
        let othersNone = "";
        const tinNoOthers = [];
        crs.map((obj) => {
            if (obj.country === "SG" && obj.taxResidencyInd === "Y") {
                checkboxSG = true;
                tinNoSG = obj.tin && obj.tin !== null ? obj.tin : "";
            }
            else if (obj.country === "US" && obj.taxResidencyInd === "Y") {
                checkboxUS = true;
                tinNoUS = obj.tin && obj.tin !== null ? obj.tin : "";
                isUSResident = obj.usPersonInd;
            }
            else if (obj.taxResidencyInd !== "" && obj.taxResidencyInd !== null && obj.taxResidencyInd === "N") {
                checkboxNone = true;
                countryNone = obj.country;
                tinNoNone = obj.tin !== null && obj.tin !== "" ? obj.tin : "";
                reasonsNone = obj.reasonCode !== null && obj.reasonCode !== "" ? obj.reasonCode : "";

                if (tinNoNone !== "") {
                    haveTINNoNone = "Y";
                }

                if (reasonsNone !== "") {
                    haveTINNoNone = "N";
                    if (reasonsNone === "3") {
                        othersNone = obj.reasonDetail !== null && obj.reasonDetail !== "" ? obj.reasonDetail : "";
                    }
                }
            }
            else {
                checkboxOthers = true;
                tinNoOthers.push(obj);
            }
            return {
                checkboxSG, checkboxUS, checkboxOthers, tinNoSG, tinNoUS, isUSResident, tinNoOthers, checkboxNone, countryNone
            };
        })

        if (controllingPersonObj.length > 1) {
            const controllingPersonId = getState().taxSelfDeclarationsReducer.controllingPersonId;
            const maxKey = Math.max.apply(null, controllingPersonId);
            dispatch(incrementCP(maxKey));
            dispatch(addNewCP(maxKey));
        }

        if (controllingPersonObj.length) {
            controllingPersonObj.map((controllingPerson, i) => {
                dispatch(setRetrieveControllingPerson(controllingPersonObj[i], i));
                if (controllingPersonObj[i].crs.length) {
                    let otherCountry = 0;
                    controllingPersonObj[i].crs.map((crsObj, j) => {
                        if (crsObj.country !== 'SG' && crsObj.country !== 'US') {
                            otherCountry++;
                        }

                        return dispatch(setRetrieveControllingPersonCRS(crsObj, i, otherCountry));
                    })
                }
                return true;
            });
        }
        dispatch({
            type: types.SET_RETRIEVE_TAX_DECLARATION,
            fatcaCRSStatus: obj.fatcaStatus !== "" && obj.fatcaStatus !== null ? obj.fatcaStatus : "",
            checkboxSG: checkboxSG,
            checkboxUS: checkboxUS,
            checkboxOthers: checkboxOthers,
            checkboxNone: checkboxNone,
            tinNoSG: tinNoSG,
            tinNoUS: tinNoUS,
            isUSResident: isUSResident,
            countryNone: countryNone,
            haveTINNoNone: haveTINNoNone,
            tinNoNone: tinNoNone,
            reasonsNone: reasonsNone,
            othersNone: othersNone

        })
        if (tinNoOthers.length > 0) {
            dispatch(populateTinNoOthers(tinNoOthers));

        }
    }
}

export const populateTinNoOthers = (array) => {
    return (dispatch) => {
        dispatch(populateFirstTin(array[0], 0));
        if (array.length > 1) {
            for (let i = 1; i < array.length; i++) {
                if (array[i]) {
                    dispatch(incrementCountry(i))
                    dispatch(populateAdditionalTinNoOthers(array[i], i));
                }
            }
        }
    }
}

export const populateFirstTin = (obj, key) => {
    return (dispatch) => {
        let haveTINNo = "";
        const othersTINNo = obj.tin !== null && obj.tin !== "" ? obj.tin : "";
        const reasons = obj.reasonCode !== null && obj.reasonCode !== "" ? obj.reasonCode : "";
        const othersOthers = obj.reasonDetail !== null && obj.reasonDetail !== "" ? obj.reasonDetail : "";

        if (othersTINNo !== "") {
            haveTINNo = "Y";
        }

        if (reasons !== "") {
            haveTINNo = "N";
        }

        dispatch({
            type: types.RETRIEVE_POPULATE_FIRST_TIN,
            key: `tinNoOthers${key}`,
            othersCountry: obj.country !== null && obj.country !== "" ? obj.country : "",
            haveTINNo: haveTINNo,
            othersTINNo: othersTINNo,
            reasons: reasons,
            othersOthers: othersOthers
        })
    }
}

export const populateAdditionalTinNoOthers = (obj, key) => {
    let haveTINNo = "";
    const othersTINNo = obj.tin !== null && obj.tin !== "" ? obj.tin : "";
    const reasons = obj.reasonCode !== null && obj.reasonCode !== "" ? obj.reasonCode : "";
    const othersOthers = obj.reasonDetail !== null && obj.reasonDetail !== "" ? obj.reasonDetail : "";

    if (othersTINNo !== "") {
        haveTINNo = "Y";
    }

    if (reasons !== "") {
        haveTINNo = "N";
    }

    return {
        type: types.RETRIEVE_POPULATE_ADDITIONAL_TIN,
        key: `tinNoOthers${key}`,
        othersCountry: obj.country !== null && obj.country !== "" ? obj.country : "",
        haveTINNo: haveTINNo,
        othersTINNo: othersTINNo,
        reasons: reasons,
        othersOthers: othersOthers

    }
}

export const setRetrieveControllingPerson = (controllingPersonObj, key) => {
    return {
        type: types.SET_RETRIEVE_CONTROLLING_PERSON,
        cpName: controllingPersonObj.name,
        cpIdNo: controllingPersonObj.legalId,
        cpMobileNo: controllingPersonObj.mobileNumber,
        cpEmail: controllingPersonObj.emailAddress,
        cpDoB: controllingPersonObj.dateOfBirth,
        cpCoB: controllingPersonObj.countryOfBirth,
        cpNationality: controllingPersonObj.nationality,
        cpIdIssuingCountry: controllingPersonObj.legalIdIssueCountry,
        cpPercentageOwnership: controllingPersonObj.percentageOfOwnership,
        taxResidencyIsValid: true,
        controllingPerson: `controllingPerson${key}`
    }
}

export const setRetrieveControllingPersonCRS = (controllingPersonCRSObj, key, otherCountry) => {

    return (dispatch, getState) => {
        let data = {};
        switch (controllingPersonCRSObj.country) {
            case 'SG':
                dispatch(handleIsCheckedCP(`controllingPerson${key}`, 'checkboxSG', false));
                data = {
                    value: controllingPersonCRSObj.tin ? controllingPersonCRSObj.tin : '',
                    isValid: true
                }
                dispatch(handleTextInputChangeOthers(data, `controllingPerson${key}`, 'tinNoSG'));

                break;

            case 'US':
                dispatch(handleIsCheckedCP(`controllingPerson${key}`, 'checkboxUS', false));
                data = {
                    value: controllingPersonCRSObj.tin ? controllingPersonCRSObj.tin : '',
                    isValid: true
                }
                dispatch(handleTextInputChangeOthers(data, `controllingPerson${key}`, 'tinNoUS'));
                dispatch(handleButtonChangeCP(controllingPersonCRSObj.usPersonInd, `controllingPerson${key}`, 'isUSResident'))
                break;

            default:
                if (otherCountry > 1) {

                    const tinNoOthersId = getState().taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthersId`];
                    const maxKey = Math.max.apply(null, tinNoOthersId);
                    dispatch(incrementCountryCP(`controllingPerson${key}`, maxKey));
                    dispatch(addNewCountryCP(`controllingPerson${key}`, maxKey));
                }
                dispatch(handleIsCheckedCP(`controllingPerson${key}`, 'checkboxOthers', false));
                let othersTin = controllingPersonCRSObj.tin ? "Y" : "N";
                dispatch(handleButtonChangeOthersCP(othersTin, `controllingPerson${key}`, `tinNoOthers${otherCountry - 1}`, 'haveTINNo'));

                dispatch(selectDropdownItemChangeCP(controllingPersonCRSObj.country, 'othersCountry', `controllingPerson${key}`, `tinNoOthers${otherCountry - 1}`, 'othersCountry'));
                data = {
                    value: controllingPersonCRSObj.reasonCode
                }
                dispatch(selectRadioItemCP(data, `controllingPerson${key}`, `tinNoOthers${otherCountry - 1}`, 'reasons'));
                let dataOthers = {
                    value: controllingPersonCRSObj.tin ? controllingPersonCRSObj.tin : '',
                    isValid: true
                }
                dispatch(handleTextInputChangeOthersCP(dataOthers, `controllingPerson${key}`, `tinNoOthers${otherCountry - 1}`, 'othersTINNo'));
                let othersData = {
                    value: controllingPersonCRSObj.reasonDetail ? controllingPersonCRSObj.reasonDetail : '',
                    isValid: true
                }
                dispatch(handleTextInputChangeOthersCP(othersData, `controllingPerson${key}`, `tinNoOthers${otherCountry - 1}`, 'othersOthers'));
                key++;
                break;
        }

        dispatch({
            type: types.SET_RETRIEVE_CP_CRS
        });
    }
}

export const validateTIN = (country, tinNo) => {
    let validTin = true;
    Application.validateTIN(country, tinNo).then((res) => {
        if (res.status === 200) {
            //set true
            validTin = true;
        }
    }).catch(e => {
        if (e.response && e.response.errorCode === "invalid.tin") {
            //set false
            validTin = false;
        } else {
            //set true
            validTin = true;
        }
    });

    return validTin;
}

export const handleTextInputBlur = (data, country, field) => {
    return (dispatch, getState) => {
        let isValid = getState().taxSelfDeclarationsReducer[field].isValid;
        let errorMsg = getState().taxSelfDeclarationsReducer[field].errorMsg;

        if (isValid !== false) {
            isValid = validateTIN(country, data.value);
            errorMsg = getState().commonReducer.appData.errorMsgs.invalidMsg;
        }
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_BLUR,
            value: data.value,
            field,
            isValid: isValid,
            errorMsg: !isValid ? errorMsg : "",
            isInitial: data.isInitial
        });
    };
};

export const handleTextInputBlurOthers = (data, key, country, field) => {
    return (dispatch, getState) => {
        let isValid = getState().taxSelfDeclarationsReducer[key][field].isValid;
        let errorMsg = getState().taxSelfDeclarationsReducer[key][field].errorMsg;

        if (isValid !== false) {
            isValid = validateTIN(country, data.value);
            errorMsg = getState().commonReducer.appData.errorMsgs.invalidMsg;
        }
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_BLUR_OTHERS,
            key,
            value: data.value,
            field,
            isValid: isValid,
            errorMsg: !isValid ? errorMsg : "",
            isInitial: data.isInitial
        });
    };
}

export const handleTextInputBlurOthersCP = (data, cpKey, key, country, field) => {
    return (dispatch, getState) => {
        let isValid = getState().taxSelfDeclarationsReducer[cpKey][key][field].isValid;
        let errorMsg = getState().taxSelfDeclarationsReducer[cpKey][key][field].errorMsg;

        if (isValid !== false) {
            isValid = validateTIN(country, data.value);
            errorMsg = getState().commonReducer.appData.errorMsgs.invalidMsg;
        }
        dispatch({
            type: types.CRS_HANDLE_TEXT_INPUT_BLUR_OTHERS_CP,
            cpKey,
            key,
            value: data.value,
            field,
            isValid: isValid,
            errorMsg: !isValid ? errorMsg : "",
            isInitial: data.isInitial
        });
    };
}
