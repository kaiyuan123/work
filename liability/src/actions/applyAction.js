import localStore from './../common/localStore';
import queryString from 'query-string';

export const setParamterToLocalStorage = (parameter) => {
    return (dispatch, getState) => {
      const productId = getState().commonReducer.appData.productCode;
      const params = queryString.parse(parameter);
      const code = params[productId] ? params[productId] : '';
      localStore.setStore('params',parameter);
      localStore.setStore('productCode', code);
    }
}
