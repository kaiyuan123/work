import * as types from "./actionTypes";
import { setTextInputHasEdited } from "./commonAction";

export const setAccountSetupData = (acountSetupObj, companyDetailsObj) => {
    return (dispatch, getState) => {
        const entityNameEmpty = acountSetupObj.name ? acountSetupObj.name : "";
        const contactEmpty = acountSetupObj.additionalServices.bibPlus.authorizedPerson.name ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.name : "";
        const phoneEmpty = acountSetupObj.additionalServices.bibPlus.authorizedPerson.mobileNumber ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.mobileNumber : "";
        const emailEmpty = acountSetupObj.additionalServices.bibPlus.authorizedPerson.emailAddress ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.emailAddress : "";
        const purposeOfAccount = acountSetupObj.purpose ? acountSetupObj.purpose : '';
        let transactionalToggle, loanRepaymentToggle, investmentTogle, othersToggle, otherPurpose;
        const BIBBulkService = acountSetupObj.additionalServices.bibPlus.additionalServices.bulkServicesInd;
        const BIBRemittanceMessage = acountSetupObj.additionalServices.bibPlus.additionalServices.remittanceServiceInd;
        const isRetrieveApplication = getState().applicationReducer.isRetrieveApplication;
        //let isRegisterOfPayNow = acountSetupObj.additionalServices.corporatePayNow.registerOfPayNow ? acountSetupObj.additionalServices.corporatePayNow.registerOfPayNow : '';

        if (purposeOfAccount) {
            purposeOfAccount.map((purpose, i) => {
                if (purpose === 'Transactional') {
                    transactionalToggle = true;
                } else if (purpose === 'Loan Repayment') {
                    loanRepaymentToggle = true;
                } else if (purpose === 'Investment') {
                    investmentTogle = true;
                }
                else {
                    othersToggle = true;
                    otherPurpose = purpose.substring(purpose.lastIndexOf('-') + 2);
                }
                return true;
            });
        }

        let isRegisterOfPayNow;

        if (acountSetupObj.additionalServices.corporatePayNow.suffix) {
            isRegisterOfPayNow = true;
        } else {
            if (!isRetrieveApplication) {
                isRegisterOfPayNow = true;
            } else {
                if (acountSetupObj.additionalServices.corporatePayNow.suffix === "") {
                    isRegisterOfPayNow = true;
                } else {
                    isRegisterOfPayNow = false;
                }
            }
        }

        dispatch({
            type: types.MYINFO_SET_ACCOUNT_SETUP_DATA,
            entityName: acountSetupObj.name ? acountSetupObj.name : "",
            uen: companyDetailsObj.basicProfile.uen,
            principalName: acountSetupObj.additionalServices.bibPlus.authorizedPerson.name ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.name : "",
            mobileNumber: acountSetupObj.additionalServices.bibPlus.authorizedPerson.mobileNumber ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.mobileNumber : "",
            emailAddress: acountSetupObj.additionalServices.bibPlus.authorizedPerson.emailAddress ? acountSetupObj.additionalServices.bibPlus.authorizedPerson.emailAddress : "",
            entityNameMyinfo: entityNameEmpty !== "" ? true : false,
            contactMyinfo: contactEmpty !== "" ? true : false,
            phoneMyinfo: phoneEmpty !== "" ? true : false,
            emailMyinfo: emailEmpty !== "" ? true : false,
            chequebooks: acountSetupObj.numberOfChequebooks ? acountSetupObj.numberOfChequebooks : '',
            purposeOfAccount: purposeOfAccount ? purposeOfAccount : [],
            transactional: transactionalToggle ? transactionalToggle : false,
            loanRepayment: loanRepaymentToggle ? loanRepaymentToggle : false,
            investment: investmentTogle ? investmentTogle : false,
            others: othersToggle ? othersToggle : false,
            otherPurpose: othersToggle ? otherPurpose : '',
            entityNoSuffix: acountSetupObj.additionalServices.corporatePayNow.suffix ? acountSetupObj.additionalServices.corporatePayNow.suffix : '',
            registerOfPayNow: isRegisterOfPayNow,
            isRetrieveApplication: isRetrieveApplication,
            BIBBulkService: BIBBulkService === 'Y' ? true : false,
            BIBRemittanceMessage: BIBRemittanceMessage === 'Y' ? true : false,
            BIBGroupId: acountSetupObj.additionalServices.bibPlus.groupId ? acountSetupObj.additionalServices.bibPlus.groupId : '',
            setupBIBPlus: acountSetupObj.additionalServices.bibPlus.userRoleType === null ? false : true,
        });
    };
}

export const handleTextInputChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.ACCOUNT_NAME_HANDLE_TEXT_INPUT_CHANGE,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });

        dispatch(setTextInputHasEdited(field));
    };
};

export const handleIsChecked = (field, isToggled, text, isValid) => {
    return (dispatch, getState) => {
        const purposeOfAccount = getState().accountSetupReducer.purposeOfAccount;
        if (field === "transactional" || field === "loanRepayment" || field === "investment" || field === "others") {
            if (!isToggled) {
                purposeOfAccount.push(text);
                dispatch(checkPurposeOfAccountIsValid(true))
            } else {
                purposeOfAccount.splice(purposeOfAccount.indexOf(text), 1);
                if (purposeOfAccount.length < 1) {
                    dispatch(checkPurposeOfAccountIsValid(false))
                }
                else {
                    dispatch(checkPurposeOfAccountIsValid(true))
                }
            }
        }

        dispatch({
            type: types.ACCOUNT_SETUP_HANDLE_TOGGLE,
            field,
            isToggled: !isToggled,
            isValid: !isValid,
            purposeOfAccount
        })
    }

};

export const setDropdownFocusStatus = (isFocus, field) => {
    return (dispatch) => {
        dispatch({
            type: types.ACCOUNT_SETUP_DROPDOWN_FOCUS,
            isFocus,
            field
        });
    };
};

export const selectDropdownItem = (value, description, field) => {
    return (dispatch) => {
        dispatch({
            type: types.ACCOUNT_SETUP_DROPDOWN_ITEM_SELECT,
            value,
            field,
            description
        })
    };
};

export const changeSearchInputValue = (searchValue, field) => {
    return (dispatch) => {
        dispatch({
            type: types.ACCOUNT_SETUP_DROPDOWN_SEARCH_INPUT_CHANGE,
            searchValue,
            field
        });
    };
};

export const checkPurposeOfAccountIsValid = (status) => {
    return (dispatch) => {
        dispatch({
            type: types.ACCOUNT_SETUP_PURPOSE_OF_ACCOUNT_IS_VALID,
            status
        });
    };
};
