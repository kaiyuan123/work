import * as types from "./actionTypes";
// import { groupByCode } from "../common/utils";
import moment from 'moment';
import { setTextInputHasEdited } from "./commonAction";
export const handleTextInputChange = (data, field) => {
    return (dispatch, getState) => {
        dispatch({
            type: types.PERSONALDETAILS_HANDLE_TEXT_INPUT_CHANGE,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
        dispatch(setTextInputHasEdited(field));
    };
};

//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
    return (dispatch) => {
        dispatch({
            type: types.PERSONALDETAILS_DROPDOWN_FOCUS,
            isFocus,
            field
        });
    };
};

//choose item from dropdown
export const selectDropdownItem = (value, description, field) => {
    return (dispatch) => {
        dispatch({
            type: types.PERSONALDETAILS_DROPDOWN_ITEM_SELECT,
            value,
            field,
            description
        })
    };
};

export const changeSearchInputValue = (searchValue, field) => {
    return (dispatch) => {
        dispatch({
            type: types.PERSONALDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE,
            searchValue,
            field
        });
    };
};

export const setMyInfoPersonalIncomeDetailsData = (shareholdersDataObj, residentialAddressObj) => {
    const contactDetailsDataObj = shareholdersDataObj && shareholdersDataObj[0].basicInfo;
    const personalInfoDataObj = shareholdersDataObj && shareholdersDataObj[0].personalInfo;
    const passportNumberEmpty = contactDetailsDataObj && contactDetailsDataObj.passportNumber ? contactDetailsDataObj.passportNumber : '';
    const passportExpiryDateEmpty = contactDetailsDataObj && contactDetailsDataObj.passportExpiryDate ? moment(contactDetailsDataObj.passportExpiryDate, "YYYY-MM-DD").format("DD/MM/YYYY") : '';
    return {
        type: types.SET_MYINFO_PERSONAL_INCOME_DETAILS,
        legalId: contactDetailsDataObj && contactDetailsDataObj.legalId ? contactDetailsDataObj.legalId : '',
        dateOfBirth: personalInfoDataObj && personalInfoDataObj.dateOfBirth ? personalInfoDataObj.dateOfBirth : '',
        gender: personalInfoDataObj && personalInfoDataObj.gender ? personalInfoDataObj.gender : '',
        maritalStatus: personalInfoDataObj && personalInfoDataObj.maritalStatus ? personalInfoDataObj.maritalStatus : '',
        nationality: personalInfoDataObj && personalInfoDataObj.nationality ? personalInfoDataObj.nationality : '',
        residentialStatus: contactDetailsDataObj && contactDetailsDataObj.residentialStatus ? contactDetailsDataObj.residentialStatus : '',
        residentialCountry: residentialAddressObj && residentialAddressObj.country ? residentialAddressObj.country : '',
        propertyType: residentialAddressObj && residentialAddressObj.propertyType ? residentialAddressObj.propertyType : '-',
        passportNumber: contactDetailsDataObj && contactDetailsDataObj.passportNumber ? contactDetailsDataObj.passportNumber : '',
        passportExpiryDate: contactDetailsDataObj && contactDetailsDataObj.passportExpiryDate ? moment(contactDetailsDataObj.passportExpiryDate, "YYYY-MM-DD").format("DD/MM/YYYY") : '',
        passportNumberMyInfo: passportNumberEmpty !== "" ? true : false,
        passportExpiryDateMyinfo: passportExpiryDateEmpty !== "" ? true : false,
        workPassStatus: contactDetailsDataObj && contactDetailsDataObj.workPassStatus ? contactDetailsDataObj.workPassStatus : '',
        workPassExpiryDate: contactDetailsDataObj && contactDetailsDataObj.workPassExpiryDate ? contactDetailsDataObj.workPassExpiryDate : '',
        countryOfBirth: personalInfoDataObj && personalInfoDataObj.countryOfBirth ? personalInfoDataObj.countryOfBirth : ''
    };
};

export const setIncomeDetailData = (shareholdersDataObj) => {
    const yearlyIncome = shareholdersDataObj[0].noaHistory ? shareholdersDataObj[0].noaHistory : {};
    return (dispatch) => {
        dispatch({
            type: types.SET_INCOME_DETAIL_DATA,
            yearlyIncome,
            incomeObj: yearlyIncome
        })
    }
}
