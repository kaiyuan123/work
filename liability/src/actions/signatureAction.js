import * as types from "./actionTypes";


export const showErrorMessage = (errorMsg) => {
	return (dispatch) => {
		dispatch({
			type: types.SIGNATURE_ERROR_SHOW,
			errorMsg
		});
	};
};

export const changeProgressValue = (newValue) => {
	return (dispatch) => {
		dispatch({
			type: types.CHANGE_PROGRESS_VALUE,
			newValue
		});
	};
};
export const removeErrorMessage = () => {
	return (dispatch) => {
		dispatch({
			type: types.SIGNATURE_ERROR_REMOVE
		});
	};
};
export const saveSignatureData = (trimmedDataURL) => {
	return (dispatch) => {
		dispatch({
			type: types.SIGNATURE_SAVE_DATA,
			trimmedDataURL
		});
	};
};

export const setSignatureUploadStatus = (status) => {
	return (dispatch) => {
		dispatch({
			type: types.UPLOAD_SIGNATURE_SUCCESSFUL,
			status
		})
	}
}

export const setPopupStatus = (status) => {
	return (dispatch) => {
		dispatch({
			type: types.SET_POP_UP_STATUS,
			status
		})
	}
}

export const setSignatureUploadIsValidStatus = (status) => {
	return (dispatch) => {
		dispatch({
			type: types.SET_SIGNATURE_UPLOAD_ISVALID,
			status
		})
	}
}

export const setSignatureTabStatus = (status) => {
	return (dispatch) => {
		dispatch({
			type: types.SET_SIGNATURE_TAB_STATUS,
			status
		})
	}
}

export const setUploadSignatureImage = (image) => {
	return (dispatch) => {
		dispatch ({
			type: types.SET_UPLOAD_SIGNATURE_IMAGE,
			image
		})
	}
}

export const setUploadSignatureSaveImage = (image) => {
	return (dispatch) => {
		dispatch ({
			type: types.SET_UPLOAD_SIGNATURE_SAVE_IMAGE,
			image
		})
	}
}

export const setDrawSignatureImage = (image) => {
	return (dispatch) => {
		dispatch ({
			type: types.SET_DRAW_SIGNATURE_IMAGE,
			image
		})
	}
}

export const setDrawSignatureSaveImage = (image) => {
	console.log(image);
	return (dispatch) => {
		dispatch ({
			type: types.SET_DRAW_SIGNATURE_SAVE_IMAGE,
			image
		})
	}
}
