import * as types from "./actionTypes";
import { capitalize } from "../common/utils";
import { setTextInputHasEditedWithKeyShareholder } from "./commonAction"

export const setShareHoldersData = (shareHoldersObj) => {
	const shareHoldersData = shareHoldersObj ? shareHoldersObj:"";
 	return (dispatch) => {

    	dispatch(populateShareHoldersList(shareHoldersData[0], 0));
	    if (shareHoldersData.length > 1) {
	      for (let i = 1; i < shareHoldersData.length; i++) {
	      	dispatch(populateAdditionalShareHoldersList(shareHoldersData[i], i));
	        dispatch(incrementShareHoldersList(i));
	      }
	    }
	    dispatch({
	      type: types.APPROVED_SHAREHOLDERS_LIST,
	      shareHoldersList: shareHoldersData
	    })
	  }
}

export const populateShareHoldersList = (obj, key) => {
  return (dispatch) => {
    const name = capitalize(obj.name);
    const nric = obj.legalId;
    const email = obj.emailAddress ? obj.emailAddress : "";
    const phone = obj.mobileNumber ? obj.mobileNumber : "";
    dispatch({
      type: types.POPULATE_SHAREHOLDERS_LIST,
      key: `shareholder${key}`,
      shareholderName: name,
      shareholderNRIC: nric,
      shareholderEmail: email,
      shareholderMobileNo: phone !== "" ? phone : "+65",
      phoneMyinfo: phone !== "" ? true : false,
	  emailMyinfo: email !== "" ? true : false
    })
  }
}

//Populate other Share Holders items
export const populateAdditionalShareHoldersList = (obj, key) => {
	return (dispatch) => {
	   const name = capitalize(obj.name);
	   const nric = obj.legalId;
	   const email = obj.emailAddress ? obj.emailAddress : "";
	   const phone = obj.mobileNumber ? obj.mobileNumber : "";
	  dispatch({
	    type: types.POPULATE_ADDITIONAL_SHAREHOLDERS_LIST,
	    shareholder: `shareholder${key}`,
	    shareholderName: name,
	    shareholderNRIC: nric,
      	shareholderEmail: email,
      	shareholderMobileNo: phone !== "" ? phone : "+65",
	    phoneMyinfo: phone !== "" ? true : false,
		emailMyinfo: email !== "" ? true : false
	  })
	}
}

export const incrementShareHoldersList = (key) => {
  return (dispatch, getState) => {
    let shareholderID = getState().shareHoldersReducer.shareholderID;
    shareholderID.push(key);
    dispatch({
      type: types.INCREMENT_SHAREHOLDERS_COUNT,
      shareholderID
    });
  };
}

export const handleTextInputChange = (data, key, field) => {
	return (dispatch) => {
		dispatch({
			type: types.SHAREHOLDERS_HANDLE_TEXT_INPUT_CHANGE,
			key,
			value: data.value,
			field,
			isValid: data.isValid,
			errorMsg: data.errorMsg,
			isInitial: data.isInitial
		});

		dispatch(setTextInputHasEditedWithKeyShareholder(key,field))
	};
};

export const handleShareHolderErrorMessage = (key, field, errorMsg, value) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_SHAREHOLDER_ERROR_MESSAGE_INPUT,
      field,
      key: `shareholder${key}`,
      errorMsg,
      value
    })
  }
}

export const setShareHoldersInputError = (key, field, errorMsg) => {
  return {
    type: types.SET_SHAREHOLDER_INPUT_ERROR,
    key,
    field,
    errorMsg
  }
}
