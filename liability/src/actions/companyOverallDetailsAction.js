import * as types from "./actionTypes";

export const setCompanyPreviousNamesData = (obj) => {
    return {
        type: types.SET_MYINFO_COMPANY_PREVIOUSNAMES_DATA,
        previousNames: obj
    }
}

export const setCompanyPreviousUENData = (obj) => {
    return {
        type: types.SET_MYINFO_COMPANY_PREVIOUSUEN_DATA,
        previousUens: obj
    }
}

export const setCompanyCapitalsData = (obj) => {
    return {
        type: types.SET_MYINFO_COMPANY_CAPITALS_DATA,
        capitals: obj
    }
}

export const setCompanyAppointmentsData = (obj) => {
    return {
        type: types.SET_MYINFO_COMPANY_APPOINTMENT_DATA,
        appointments: obj
    }
}

export const setCompanyShareholdersData = (obj) => {
    return {
        type: types.SET_MYINFO_COMPANY_SHAREHOLDERS_DATA,
        shareholders: obj
    }
}

export const setCompanyFinancialsData = (obj) => {
    return {
        type: types.SET_MYINFO_COMPANY_FINANCIALS_DATA,
        financials: obj
    }
}

export const setCompanyGrantsData = (obj) => {
    return {
        type: types.SET_MYINFO_COMPANY_GRANTS_DATA,
        grants: obj
    }
}