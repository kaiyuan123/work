import React, { Component } from "react";
import { connect } from "react-redux";
// import Dropdown from "./../components/Dropdown/Dropdown";
import TextInput from "./../components/TextInput/TextInput";
import { capitalize } from "./../common/utils";
import moment from 'moment';
import ToggleInputWithoutTitle from "./../components/ToggleInput/ToggleInputWithoutTitle/ToggleInputWithoutTitle";
import { handleIsChecked, setDropdownFocusStatus, selectDropdownItem, handleTextInputChange, changeSearchInputValue, selectRadioItem, handleIsCheckedClientele } from "./../actions/companyBasicDetailsAction";
import LocalAddressInput from "./../components/Uob/LocalAddressInput/LocalAddressInput";
import SaveAndExit from "./SaveAndExit";
import Checkbox from "./../components/Checkbox/Checkbox";

class CompanyBasicDetails extends Component {

    handleSaveAndExit() {
        this.props.onSaveAndExit();
    }

    handleOnChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChange(data, field));
    }

    handleOnCheckbox(field, isToggled) {
        const { dispatch } = this.props;
        dispatch(handleIsChecked(field, isToggled));
    }

    handleOnCheckboxClientele(field, isToggled, text, isValid) {
        const { dispatch } = this.props;
        dispatch(handleIsCheckedClientele(field, isToggled, text, isValid));
    }

    handleDropdownBlur(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(false, field));
    }

    handleDropdownFocus(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(true, field));
    }

    handleDropdownClick(data, field) {
        const { dispatch, companyBasicDetailsReducer } = this.props;

        if (companyBasicDetailsReducer[field].value === data.value) {
            return;
        }

        dispatch(selectDropdownItem(data.value, field, data.description));
    }

    handleOnSearchChange(e, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValue(value, field));
    }

    handleRadioSelection(data, field) {
        const { dispatch } = this.props;
        dispatch(selectRadioItem(data, field));
    }

    render() {
        const { commonReducer, companyBasicDetailsReducer } = this.props;
        const { companyRegistrationNumber, registeredCompanyName, companyStatus, ownership, registrationDate, companyExpiryDate, mailingAddressCheckbox, annualTurnover } = companyBasicDetailsReducer;
        // const { isRetrieveApplication } = applicationReducer;
        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";

        const companyDetailsValues = commonReducer.appData.companyDetails;
        const companyBasicDetailsValues = companyDetailsValues.companyBasicDetails;
        const labels = companyBasicDetailsValues.labels;

        const errorMsgList = commonReducer.appData.errorMsgs;
        // const countriesNamesMap = inputValues.countriesNamesMap;
        const ownershipList = inputValues.ownership;

        const ownershipValue = ownership.value !== "" ? ownership.value : "NA";
        // const mailingChecked = mailingAddressCheckbox.isToggled ? "" : "darkGrey";
        const registrationDateFormat = registrationDate.value !== "" && moment(registrationDate.value, "YYYY-MM-DD").isValid() ? moment(registrationDate.value, "YYYY-MM-DD").format("DD/MM/YYYY") : "-";
        const companyExpiryDateFormat = companyExpiryDate.value !== "" && moment(companyExpiryDate.value, "YYYY-MM-DD").isValid() ? moment(companyExpiryDate.value, "YYYY-MM-DD").format("DD/MM/YYYY") : "-";

        return (
            <div className="uob-content">
                <div className="save-button-container">
                    <h1>{companyDetailsValues.title}</h1>
                    <SaveAndExit {...this.props} onClick={() => this.handleSaveAndExit()} />
                </div>
                <div className="uob-form-separator" />
                <p className="uob-headline">{companyDetailsValues.headline}</p>
                <h1 className="sub-title">{companyBasicDetailsValues.subtitle}</h1>


                <div className="top-border">
                    <div className="fullTable confirmDeta ils-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyRegistrationNumber}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {companyRegistrationNumber.value}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.registeredCompanyName}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {registeredCompanyName.value}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.ownership}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {ownershipList[ownershipValue]}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyStatus}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {capitalize(companyStatus.value)}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-border">
                    <div className="fullTable confirmDetails-flexContainer">
                        <div className="right-border halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.registrationDate}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {registrationDateFormat}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="halfRow">
                            <div className="confirmDetails-label-container ">
                                <div className="confirmDetails-labelValue">
                                    <div className="confirmDetails-LabelCSS">
                                        {labels.companyExpiryDate}
                                    </div>
                                    <div className="confirmDetails-ValueCSS">
                                        {companyExpiryDateFormat}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <div className="sub-div-common bottom-border">
                    <div className="p-l-10 positionRelative">
                        <TextInput
                            inputID="annualTurnover"
                            label={labels.annualTurnoverDesc}
                            value={annualTurnover.value}
                            errorMsg={annualTurnover.errorMsg}
                            onChange={data => this.handleOnChange(data, "annualTurnover")}
                            isValid={annualTurnover.isValid}
                            validator={["required", "isFloatNumber", "validateMinimumOne", "maxSize|10"]}
                            isMinOne={true}
                            errorMsgList={errorMsgList}
                            hasIcon={false}
                        />
                    </div>
                </div>
                <div className="multicheckbox-container" id="primaryClientele">
                    <span className="checkBoxBottomSub checkbox-headlabel">{labels.primaryClientele}</span>                    
                    <Checkbox
                        description={labels.corporateClients}
                        isChecked={companyBasicDetailsReducer.corporateClients.isToggled}
                        onClick={() => this.handleOnCheckboxClientele('corporateClients', companyBasicDetailsReducer.corporateClients.isToggled, 'Corporate Clients')}
                    />
                    <Checkbox
                        description={labels.individualClients}
                        isChecked={companyBasicDetailsReducer.individualClients.isToggled}
                        onClick={() => this.handleOnCheckboxClientele('individualClients', companyBasicDetailsReducer.individualClients.isToggled, 'Individual Clients')}
                    />                   
                    <br />
                    {!companyBasicDetailsReducer.primaryClienteleIsValid && <div style={{
                        color: 'red', fontSize: '14px',
                        marginLeft: '20px'
                    }}>This field is required</div>}
                </div>
                <h1 className="sub-title">{companyBasicDetailsValues.subtitle2}</h1>
                <LocalAddressInput
                    labels={labels}
                    errorMsgList={errorMsgList}
                    addressType="company"
                    inputValues={inputValues}
                />
                <div className="flexDisplay" id="mailingAddressCheckbox">
                    <h1 className={`sub-title`}>{companyBasicDetailsValues.mailingSubtitle}</h1>
                    <ToggleInputWithoutTitle
                        inputID="mailingAddressCheckbox"
                        isToggled={mailingAddressCheckbox.isToggled}
                        onClick={() => this.handleOnCheckbox('mailingAddressCheckbox', mailingAddressCheckbox.isToggled)}
                    />
                </div>
                {mailingAddressCheckbox.isToggled &&
                    <div className="mailingDetails-section">
                        <LocalAddressInput
                            labels={labels}
                            errorMsgList={errorMsgList}
                            addressType="mailing"
                            inputValues={inputValues}
                        />
                    </div>
                }
            </div >
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, companyBasicDetailsReducer, localAddressInputReducer } = state;
    return { commonReducer, companyBasicDetailsReducer, localAddressInputReducer };
};

export default connect(mapStateToProps)(CompanyBasicDetails);
