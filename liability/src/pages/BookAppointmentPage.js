import React, { Component } from "react";
import { connect } from "react-redux";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import BookAppointmentSearch from "./../components/BookAppointmentSearch/BookAppointmentSearch";
import BookAppointmentSearchNoMap from "./../components/BookAppointmentSearch/BookAppointmentSearchNoMap";
import NavigationBar from "./../components/NavigationBar/NavigationBar";
import Script from 'react-load-script'
import {
	setAvailableLocationData
} from "./../actions/applicationAction";
import { scrollToSection } from "./../actions/commonAction";
import { bookAppointmentTogglePopUp } from "./../actions/bookAppointmentAction";
import closeIcon from "./../assets/images/cross-grey.svg";
let versionUpdate = (new Date()).getTime();

class BookAppointmentPage extends Component {

	state = {
		scriptLoaded: false,
		scriptError: false,
	};

	componentDidMount() {
		const { dispatch, applicationReducer } = this.props;
		const { initiateMyinfoData } = applicationReducer;
		const applicationID = initiateMyinfoData !== null ? initiateMyinfoData.id : null;
		dispatch(setAvailableLocationData(applicationID));
	}

	handleToSubmitApplication() {
		this.props.onContinue();
	}

	handleScriptCreate() {
		this.setState({ scriptLoaded: false })
	}

	handleScriptError() {
		this.setState({ scriptLoaded: true, scriptError: true })
	}

	handleScriptLoad() {
		this.setState({ scriptLoaded: true, scriptError: false });
	}

	isAllChecking() {
		this.props.onCheck()
	}

	checkAllValidation() {
		const { bookAppointmentReducer } = this.props;
		// const { bookingBranch, bookingTimeSlots, dateTimeValue } = bookAppointmentReducer;
		const { bookingBranch, dateTimeValue } = bookAppointmentReducer;

		if (bookingBranch.value === "" || !bookingBranch.isValid ||
			dateTimeValue.value === "" || !dateTimeValue.isValid) {
			return false;
		}
		return true;
	}

	handleBookAppointmentTogglePopUp(status) {
		const { commonReducer, dispatch } = this.props;
		const currentSection = commonReducer.currentSection;
		dispatch(bookAppointmentTogglePopUp(status));
		if (!status) {
			dispatch(scrollToSection(currentSection));
		}
	}

	render() {
		const { commonReducer, bookAppointmentReducer } = this.props;
		const { availableLocationDataLoaded, onClickDate } = bookAppointmentReducer;
		const bookAppointmentValue = commonReducer.appData.bookAppointment;
		const isFromConfirmDetails = this.props.isFromConfirmDetails() ? "" : "uob-content";
		const height = window.innerHeight;
		if (!this.state.scriptLoaded) {
			return (
				<div>
					<Script
						url={`https://www.uobgroup.com/uob-branches-and-atms/assets/js/data_sg.js?v=${versionUpdate}`}
						onCreate={this.handleScriptCreate.bind(this)}
						onError={this.handleScriptError.bind(this)}
						onLoad={this.handleScriptLoad.bind(this)}
					/>
					<div className={`${isFromConfirmDetails} bookAppointment-popup`} id="bookAppointment-section">
						<div className="clickOutsideBox" onClick={() => this.handleBookAppointmentTogglePopUp(false)} />
						<div className={`bookAppointment-inner`} style={{height: `${height}px`}}>
							<div className={`bookAppointment-content`}>
								<table style={{ width: "100%" }}>
									<tbody>
										<tr>
											<td>
												<h1 className="title-padding">{bookAppointmentValue.title}</h1>
											</td>
											<td>
												<span className="close-icon bookAppointmentCloseMark" style={{ top: '20px', right: '20px' }} onClick={() => this.handleBookAppointmentTogglePopUp(false)}>
													<img className="close-icon" src={closeIcon} alt="Close-Icon" />
												</span>
											</td>
										</tr>
									</tbody>
								</table>
								<div className="uob-form-separator" />
								<div className="blobs">
									<div className="blob-center" />
									<div className="blob" />
									<div className="blob" />
									<div className="blob" />
									<div className="blob" />
									<div className="blob" />
									<div className="blob" />
									<div className="loadingWording">Loading Google Map…</div>
								</div>
							</div>
						</div>

						<div className="clickOutsideBox" onClick={() => this.handleBookAppointmentTogglePopUp(false)} />
					</div>
				</div>
			)
		} else if (!availableLocationDataLoaded) {
			return (
				<div className={`${isFromConfirmDetails} bookAppointment-popup`} id="bookAppointment-section">
					<div className="clickOutsideBox" onClick={() => this.handleBookAppointmentTogglePopUp(false)} />
					<div className={`bookAppointment-inner`} style={{height: `${height}px`}}>
						<div className={`bookAppointment-content`}>
							<table style={{ width: "100%" }}>
								<tbody>
									<tr>
										<td>
											<h1 className="title-padding">{bookAppointmentValue.title}</h1>
										</td>
										<td>
											<span className="close-icon bookAppointmentCloseMark" style={{ top: '20px', right: '20px' }} onClick={() => this.handleBookAppointmentTogglePopUp(false)}>
												<img className="close-icon" src={closeIcon} alt="Close-Icon" />
											</span>
										</td>
									</tr>
								</tbody>
							</table>
							<div className="blobs">
								<div className="blob-center" />
								<div className="blob" />
								<div className="blob" />
								<div className="blob" />
								<div className="blob" />
								<div className="blob" />
								<div className="blob" />
								<div className="loadingWording">Loading Google Map…</div>
							</div>
						</div>
						<div className="clickOutsideBox" onClick={() => this.handleBookAppointmentTogglePopUp(false)} />
					</div>
				</div>
			)
		}
		return (
			<div className={`${isFromConfirmDetails} bookAppointment-popup`} id="bookAppointment-section">
				<style dangerouslySetInnerHTML={{ __html: `.uob-form-loan.popup-on {overflow: initial}` }} />
				<div className="clickOutsideBox" onClick={() => this.handleBookAppointmentTogglePopUp(false)} />
				<div className={`bookAppointment-inner`} style={{height: `${height}px`}}>
					<div className={`bookAppointment-content`}>
						<table style={{ width: "100%" }}>
							<tbody>
								<tr>
									<td>
										<h1 className="title-padding">{bookAppointmentValue.title}</h1>
									</td>
									<td>
										<span className="close-icon bookAppointmentCloseMark" style={{ top: '20px', right: '20px' }} onClick={() => this.handleBookAppointmentTogglePopUp(false)}>
											<img className="close-icon" src={closeIcon} alt="Close-Icon" />
										</span>
									</td>
								</tr>
							</tbody>
						</table>
						{!this.state.scriptError ? <BookAppointmentSearch {...this.props} /> :
							<BookAppointmentSearchNoMap {...this.props} />}
					</div>
					{onClickDate &&
						<div>
							<div className="uob-input-separator bookAppointmentFixed">
								<PrimaryButton label={bookAppointmentValue.submit} onClick={() => this.handleToSubmitApplication()} />
							</div>
							{!this.checkAllValidation() &&
								<div className="uob-input-separator bookAppointmentFixed bookAppointmentFixedGray">
									<PrimaryButton label={bookAppointmentValue.submit} onClick={() => this.isAllChecking()} />
								</div>
							}
						</div>
					}
				</div>
				<div className="clickOutsideBox" onClick={() => this.handleBookAppointmentTogglePopUp(false)} />
			</div>
		)
	}
}

const mapStateToProps = state => {
	const { commonReducer, retrievingReducer, applicationReducer, bookAppointmentReducer } = state;
	return { commonReducer, retrievingReducer, applicationReducer, bookAppointmentReducer };
};

export default connect(mapStateToProps)(BookAppointmentPage);
