import React, { Component } from 'react';
import TextInput from "./../components/TextInput/TextInput";
import { connect } from 'react-redux';
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
// import { getURLParameter } from "../common/utils";
import { handleTextInputChange, getVerificationCode } from "./../actions/verifyAction";
import { setParamterToLocalStorage } from "./../actions/applyAction";
import { retrieveUENSubmission, storeParameter, setErrorWithValue } from "./../actions/applicationAction";
import localStore from './../common/localStore';
import OtpPage from './OtpPage';
import { handleErrorMessage } from "./../actions/commonAction";
import queryString from 'query-string';
import moment from 'moment';

export class VerifyPage extends Component {

	componentWillMount() {
		const { dispatch,verifyReducer } = this.props;
		const parameter = queryString.parse(this.props.location.search);
		const code = parameter.code ? parameter.code : "";
		const mode = parameter.mode ? parameter.mode : "";
		if (code !== "") {			
			dispatch(getVerificationCode(code+'&mode='+mode, () => this.redirectOnSuccess(), () => this.redirectToErrorPage()));
		} else {
			this.props.history.push({
				pathname: "error"
			})
		}
	}

	redirectOnSuccess() {
		const { dispatch } = this.props;
		const { verifyReducer } = this.props;

		const expiryDate = new Date(verifyReducer.expiryDate.substring(0, verifyReducer.expiryDate.indexOf(' ')));
		const dateNow = new Date(moment().format("YYYY-MM-DD"));


		if(verifyReducer.status !== 'INITIATE'){
			dispatch(setErrorWithValue('AppSubmitted'));
        	this.props.history.push({
				pathname: "error"
			})
		} else if(expiryDate > dateNow) {
			const url = "?productCode=" + verifyReducer.productCode;
			dispatch(setParamterToLocalStorage(url));
			const params = localStore.getStore('params');
			dispatch(storeParameter(params, verifyReducer.productCode));
		}else {
			dispatch(setErrorWithValue("linkExpired"))
			this.props.history.push({
				pathname: "error"
			})
		}

	}

	handleOnChange(data, field) {
		const { dispatch } = this.props;
		dispatch(handleTextInputChange(data, field));
	}

	handleToVerifyUEN() {
		const { dispatch, verifyReducer } = this.props;
		const { uen, applicationId } = verifyReducer;
		const dataObj = {
			id: applicationId,
			uen: uen.value
		}

		dispatch(retrieveUENSubmission(() => this.redirectToErrorPage(), dataObj));
	}

	redirectToErrorPage() {
		const { applicationReducer } = this.props;
		this.props.history.push({
			pathname: "error"
		})
	}

	checkEmptyFields(fields, errorMsg, action) {
		const { dispatch } = this.props;
		let errorCount = 0;
		for (let i = 0; i < fields.length; i++) {
			if (fields[i].value === "") {
				dispatch(action(fields[i].name, errorMsg));
				errorCount++;
			}
		}
		if (errorCount > 0) {
			return false;
		}
		return true;
	}

	checkisValidFields(fields) {
		let errorCount = 0;
		for (let i = 0; i < fields.length; i++) {
			if (!fields[i].isValid) {
				errorCount++;
			}
		}
		if (errorCount > 0) {
			return false;
		}
		return true;
	}

	isUENValuePassing() {
		const { commonReducer, verifyReducer } = this.props;
		const { uen } = verifyReducer;
		let errorCount = 0;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
		let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;


		if (!this.checkEmptyFields([uen], requiredMsg, handleErrorMessage)) {
			errorCount++;
		}

		if (!this.checkisValidFields([uen], invalidMsg, handleErrorMessage)) {
			errorCount++;
		}

		if (errorCount > 0) {
			return false
		} else {
			this.handleToVerifyUEN();
		}
		return true;
	}

	render() {
		const { commonReducer, verifyReducer } = this.props;
		const retrieveValue = commonReducer.appData.retrievePage;
		const { inlineMessage } = commonReducer
		const errorMsgList = commonReducer.appData.errorMsgs;
		const { uen, isOTPPageShow, productCode, refNo } = verifyReducer;
		const productTitle = commonReducer.appData !== null && commonReducer.appData.productTitle[productCode];
		let imgUrl = "./images/logos/uob-eBiz-Logo-blue.svg";
		return (
			<div>
				<style dangerouslySetInnerHTML={{
					__html: `
					body { padding-left: 0;
					height: 100vh; width: 100%;}
					.uob-form-loan-container { background: transparent;}
					.uob-content { padding: 0; }
					#sideNav { display: none }
					.logoRight { display: block!important;  }
					#home .title2 { display: none }
					section.resume-section { padding: 32px 35px 0!important;}
					.blue-uob-logo {padding: 26px 40px}
					`
				}} />
				<div className='uob-logo-applyPage'>
					<img className="logo" alt="logo" src='./images/logos/uobLogo.svg' />
					<span className="text-with-logo">| {productTitle}</span>
					<img className="rightByYouLogo" src="./images/logos/right-by-you-logo.jpg" alt="Right By You" />
				</div>
				<div className="displayVerify">					
	                <img className="img-size" alt="logo" src={imgUrl} />
	                <div className="title-vertical-line"/>
	                <div className="mob-prodtitle">
	                  
	                  <span className="font-16">{productTitle}</span>
	                </div>
	             </div>
				<div className="tiles">
					<div className="tiles__item_new">
						<div className="tiles__container">
							<div className="messageBox-title">
								{retrieveValue.retrieve}
							</div>
							<p>{retrieveValue.reference} {refNo}</p>
							{!isOTPPageShow ? <div>
								<div className="center-content">
									<div>
										<div className="sub-head">{retrieveValue.businessUEN}</div>
											{inlineMessage && <div className="inline-error-message">
											{inlineMessage}
										</div>}
										<div className="input-container">
											<TextInput
												inputID={'uenId'}
												value={uen.value}
												label={retrieveValue.labelUEN}
												errorMsg={uen.errorMsg}
												isValid={uen.isValid}
												onChange={(data) => this.handleOnChange(data, 'uen')}
												validator={["maxSize|10"]}
												errorMsgList={errorMsgList}
												hasIcon={false}
											/>
										</div>
									</div>
									<div className="button-container">
										<PrimaryButton label={retrieveValue.continue} onClick={() => this.isUENValuePassing()} isLoading={commonReducer.isProcessing} />
									</div>

								</div>
							</div> : <div>
									<div className="otp-content">{retrieveValue.keyInText}</div>
									<div className="center-content">
										<OtpPage {...this.props} />
									</div>
								</div>}

						</div>

					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	const { commonReducer, verifyReducer } = state;
	return { commonReducer, verifyReducer };
}

export default connect(mapStateToProps)(VerifyPage);
