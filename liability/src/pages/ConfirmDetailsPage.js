//  Confirm Review details (display all the details)
//  Confirm T and C (agreement)
import React, { Component } from "react";
import { connect } from "react-redux";
import ConfirmReviewDetails from "./ConfirmReviewDetails";
import ConfirmTandC from "./ConfirmTandC";
import Signature from "./Signature";
//import UploadDocument from "./UploadDocument";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";

class ConfirmDetailsPage extends Component {

	handleToThankYouPage() {
		this.props.onContinue();
	}

	handleToBookAppointment() {
		this.props.openBookAppointment();
	}

	isAllChecking() {
		this.props.onCheck()
	}

	// isFromConfirmDetails() {
	// 	return true;
	// }

	render() {
		const { commonReducer, applicationReducer } = this.props;
		const labels = commonReducer.appData.confirmDetails.confirmTandC.labels;

		return (
			<div className="uob-content" id="confirmDetailsPage-section">

				<ConfirmReviewDetails {...this.props} />
				<br />
				{/*{applicationReducer.isKnockOut &&
				<BookAppointmentPage {...this.props} isFromConfirmDetails={() => this.isFromConfirmDetails()}/>
				}*/}
				<ConfirmTandC {...this.props} />

				{applicationReducer.isKeyManApply && !applicationReducer.isKnockOut &&
					<Signature  {...this.props} />
				}

				{!this.props.onFixedButton() &&
					<div className="uob-input-separator fixedContinue fixedContinueGray">
						<PrimaryButton label={applicationReducer.isKnockOut ? labels.nextButton : labels.submitButton}
							onClick={() => this.isAllChecking()} />
					</div>
				}

				{this.props.onFixedButton() && applicationReducer.isKnockOut &&
					<div className="uob-input-separator fixedContinue">
						<PrimaryButton label={labels.bookAppointment} onClick={() => this.handleToBookAppointment()} />
					</div>
				}

				{this.props.onFixedButton() && !applicationReducer.isKnockOut &&
					<div className="uob-input-separator fixedContinue">
						<PrimaryButton label={labels.submitButton} onClick={() => this.handleToThankYouPage()} />
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, applicationReducer, retrievingReducer, additionalPartnersDetailsReducer, localAddressInputReducer, companyBasicDetailsReducer, confirmTandCReducer, contactDetailsReducer, signatureReducer, drawSignatureReducer, bookAppointmentReducer } = state;
	return {
		commonReducer,
		applicationReducer,
		retrievingReducer,
		additionalPartnersDetailsReducer,
		localAddressInputReducer,
		companyBasicDetailsReducer,
		confirmTandCReducer,
		contactDetailsReducer,
		signatureReducer,
		drawSignatureReducer,
		bookAppointmentReducer
	};
};

export default connect(mapStateToProps)(ConfirmDetailsPage);
