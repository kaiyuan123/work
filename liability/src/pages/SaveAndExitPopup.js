import React, { Component } from "react";
import { connect } from "react-redux";
import { showSaveAndExitPopup } from "./../actions/saveAndExitPopupAction"
import closeIcon from "./../assets/images/cross-grey.svg";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";

class SaveAndExitPopup extends Component {	

	handlePopup(popuptoggle) {
		const { dispatch } = this.props;
		dispatch(showSaveAndExitPopup(popuptoggle));
	}

	handleToParentSite() {
		this.props.onContinue();
	}

	render() {
		const { commonReducer, saveAndExitPopupReducer } = this.props;
		const saveAndExitValue = commonReducer.appData.SaveAndExitPopup;
		const applicationExpiryDate = commonReducer.applicationExpiryDate;
		const email = saveAndExitValue.email.replace("{applicationExpiryDate}", applicationExpiryDate);
		
		return (
			<div className='save-popup-container'>
				<div className='save-popup-inner' >
					<span className="close-icon">
						<img src={closeIcon} alt="Close-Icon" onClick={() => this.handlePopup(false)} />
					</span>
					<div className="save-popup-content">
						<span>{saveAndExitValue.title}</span>
						<p dangerouslySetInnerHTML={{ __html: email }}></p>
					</div>
					<PrimaryButton label={saveAndExitValue.leave} onClick={() => this.handleToParentSite()} isLoading={commonReducer.isProcessing} />
				</div>
			</div>
		)
	}

}

const mapStateToProps = state => {
    const { commonReducer } = state;
    return { commonReducer };
};

export default connect(mapStateToProps)(SaveAndExitPopup);
