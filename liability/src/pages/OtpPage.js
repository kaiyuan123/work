import React, { Component } from 'react';
import { connect } from 'react-redux';
import SmsToken from './../components/SmsToken/SmsToken';
import { setCountDownStatus, sendOtp, verifyOtp, setOtpTriedCount, handleOtpChange, setOtpErrorMessage } from './../actions/otpAction';
import { setErrorMessage } from './../actions/commonAction';
// import queryString from 'query-string';
import localStore from './../common/localStore';

class OtpPage extends Component {  

  handleOnChangeCode(value) {
    const { dispatch, verifyReducer } = this.props;
    const { applicationId  } = verifyReducer;
    dispatch(handleOtpChange(value));
    if (value.length === 6) {
      dispatch(setErrorMessage(''));
      dispatch(verifyOtp(value, applicationId, () => this.redirectOnSuccess()));
    }
  }


  redirectOnSuccess() {
    const { commonReducer, applicationReducer } = this.props;
    let pathname = localStore.getStore("productCode").toLowerCase();
    const isCommerical = pathname.includes("cmb", 0);
    const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
    const businessPathUrl = commonReducer.appData.pathname_uri_business;
    const root_path = isCommerical ? corporatePathUrl : businessPathUrl;
    this.props.history.push({
        pathname : `${root_path}application`,
        search : applicationReducer.startingParameter
    })
  }

  handleSendToken() {
    const { dispatch, otpReducer } = this.props;
    dispatch(setOtpErrorMessage(''));
    dispatch(handleOtpChange(''));
    dispatch(setOtpTriedCount(otpReducer.otpTriedCount + 1));
    dispatch(setCountDownStatus(true));
    dispatch(sendOtp());
  }

  handleOnEndCountDown() {
    const { dispatch } = this.props;
    dispatch(setCountDownStatus(false));
  }

  render() {
    const { commonReducer, otpReducer } = this.props;
    const otp = commonReducer.appData.retrievePage;
    const count = otpReducer.otpTriedCount;
    const interval = count > 3 ? 120 : (count === 3 ? 60 : count * 15);
    return (
      <div>

        <SmsToken
            interval={interval}
            isMobileHide={true}
            value={otpReducer.code}
            prefix={otpReducer.prefix}
            isCountingDown={otpReducer.isCountingDown}
            onGetToken={() => this.handleSendToken()}
            onEndCountDown={() => this.handleOnEndCountDown()}
            onChangeCode={(value) => this.handleOnChangeCode(value)}
            resendLabel={otp.resendOtp}
            isLoading={otpReducer.isLoading}
            inlineErrorMsg={otpReducer.inlineErrorMsg}
          />
      </div>

    );
  }
}

const mapStateToProps = (state) => {
  const { otpReducer, commonReducer, basicDetailsReducer } = state;
  return { otpReducer, commonReducer, basicDetailsReducer };
}

export default connect(mapStateToProps)(OtpPage);
