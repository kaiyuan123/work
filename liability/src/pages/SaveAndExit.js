import React, { Component } from "react";
import { connect } from "react-redux";
import saveExit from './../assets/images/saveexit.svg';

class SaveAndExit extends Component {

	render() {
		const { commonReducer, onClick } = this.props;
		const currentSection = commonReducer.currentSection;
		let hideSaveExit;
		if(currentSection === 'confirmDetailsPage'){
			hideSaveExit = 'hideSaveExit';
		}
		return (
			<div className={`save-exit-container ${hideSaveExit}`} onClick={onClick}>
				<img src={saveExit} alt='tick' />
			</div>
		)
	}

}

const mapStateToProps = state => {
    const { commonReducer } = state;
    return { commonReducer };
};

export default connect(mapStateToProps)(SaveAndExit);
