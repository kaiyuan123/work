import React, { Component } from 'react';
import { connect } from 'react-redux';
// import errorImg from './../assets/images/errorImg.svg';
import MessageBox from './../components/MessageBox/MessageBox';
// import {detectPathName} from './../common/utils';
import moment from "moment";
import localStore from './../common/localStore';
import { LocalEnvironment } from "./../api/httpApi"
import { deleteApplication } from '../actions/retrievingAction';


class ErrorMessagePage extends Component {
  componentWillMount() {
    // const { applicationReducer, commonReducer } = this.props;
    // const code = applicationReducer.errorCode;
    // const errorValue = commonReducer.appData.errorMessageBox;
    // const paramString = this.props.location.search;
    // const paraArray = paramString.split('&');
    // const parameter = paraArray.splice(1).join('&');
    // if(code === 'MYRJTD'){
    //   const url = detectPathName() + errorValue["MYRJTD"].url + "?" + parameter;
    //   window.open(url, '_parent');
    // }
  }

  handleToDelete() {
    const { dispatch, errorCode } = this.props;
    const applicationId = errorCode.description.applicationId;
    dispatch(deleteApplication(applicationId, () => this.redirectToErrorPage(), () => this.redirectToApplication()));
  }

  redirectToErrorPage() {
    const { commonReducer } = this.props;
    let pathname = localStore.getStore("productCode").toLowerCase();
    const isCommerical = pathname.includes("cmb", 0);
    const corporateUrl = commonReducer.appData.pathname_uri_commercial;
    const businessUrl = commonReducer.appData.pathname_uri_business;
    const applyPath = !LocalEnvironment ? isCommerical ? corporateUrl : businessUrl : "/";
    this.props.history.push({
      pathname: `${applyPath}error`
      // search: "code=" + applicationReducer.errorCode
    })
  }

  redirectToApplication() {
    const { commonReducer, applicationReducer } = this.props;
    // let pathname = window.location.pathname;
    let pathname = localStore.getStore("productCode").toLowerCase();
    const isCommerical = pathname.includes("cmb", 0);
    const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
    const businessPathUrl = commonReducer.appData.pathname_uri_business;
    const root_path = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : "/";
    this.props.history.push({
      pathname: `${root_path}application`,
      search: applicationReducer.startingParameter
    })
  }

  render() {
    const { commonReducer, errorCode } = this.props;
    const productTitle = commonReducer.appData.productTitleThankYou;
    const errorValue = commonReducer.appData.errorMessageBox;
    const code = errorValue[errorCode.value] ? errorCode.value : 'serviceDown';

    //Subtitle
    let subtitleDisplay = errorValue[code].subtitle;
    const description = errorCode.description && errorCode.description !== "" ? errorCode.description : "";

    const referenceNo = description !== "" && description.referenceNumber !== "" && description.referenceNumber !== null ? description.referenceNumber : "";
    const date = description !== "" && description.createdDate !== "" && description.createdDate !== null ? moment(description.createdDate).format("DD/MM/YYYY") : "";
    const companyName = description !== "" && description.entityName !== "" && description.entityName !== null ? description.entityName : "";
    const codeDesc = description !== "" && description.productCode !== "" && description.productCode !== null ? productTitle[description.productCode] : "";
    const expiredDate = description !== "" && description.expiryDate !== "" && description.expiryDate !== null ? moment(description.expiryDate).format("DD/MM/YYYY") : "";

    if (code === "AppExistSameApplicant") {
      subtitleDisplay = errorValue[code].subtitle.replace("{referenceNo}", referenceNo).replace("{date}", date).replace("{companyName}", companyName).replace("{productTitle}", codeDesc).replace("{expiredDate}", expiredDate);
    }

    if (code === "AppExist" || code === "AppExistDifferentApplicant") {
      subtitleDisplay = errorValue[code].subtitle.replace("{date}", date).replace("{companyName}", companyName).replace("{productTitle}", codeDesc);
    }

    return (
      <div id="loadingPage" style={{ top: "60px" }} className='uob-content'>
        <div className="error-pg-container">
          <MessageBox
            title={errorValue[code].title}
            subtitle={subtitleDisplay}
          />

          {code === "AppExistSameApplicant" &&
            <div className='error-button-container'>
              <div className="error-button" onClick={() => this.handleToDelete()}>{errorValue.continueButton}</div>
            </div>}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { commonReducer } = state;
  return { commonReducer };
}

export default connect(mapStateToProps)(ErrorMessagePage);
