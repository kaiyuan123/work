import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setErrorWithValue } from "./../actions/applicationAction";
import { setParamterToLocalStorage } from "./../actions/applyAction";
import { LocalEnvironment, Force_Google_Tag } from "./../api/httpApi"
import { setErrorMessage, setCompatibilityErrorMessage } from "./../actions/commonAction";
import Loader from "./../components/Loader/Loader";
import queryString from 'query-string';
import GenericErrorMessage from "./../components/GenericErrorMessage/GenericErrorMessage";
import { sendDataToSparkline } from "./../common/utils";

export class BusinessApplyPage extends Component {
  componentWillMount() {
    const { dispatch, commonReducer } = this.props;
    const parameter = window.location.search;
    let pathname = window.location.pathname;
    dispatch(setParamterToLocalStorage(parameter));
    const dataElement = commonReducer.appData.dataElementObj;
    const productId = commonReducer.appData.productCode;
    const params = queryString.parse(parameter);
    const code = params[productId] ? params[productId].toLowerCase() : '';
    const isCommerical = code.includes("cmb", 0);
		const isBusinessPathname = pathname.includes("business");
		const isCommercialPathname = pathname.includes("corporate");

    let validProductCode = false;
    if ( commonReducer.appData.validProductCode.indexOf(code) >= 0 ) {
      if ( !LocalEnvironment && isBusinessPathname ) {
        validProductCode = code.includes("bb", 0);
      } else if ( !LocalEnvironment && isCommercialPathname) {
        validProductCode = code.includes("cmb", 0);
      } else if (LocalEnvironment) {
        validProductCode = true;
      }
    }

    if (code !== "" && validProductCode) {
      if (!Force_Google_Tag) {
        sendDataToSparkline(dataElement, code, "", true)
      }
      

      // let requiredParameter= [];
      // commonReducer.appData.dataElementObj.requiredParameter.map((i) => {
      //     const conCatString = params[i] ? `${i}=${params[i]}` : '';
      //     if(conCatString !== '') {
      //       requiredParameter.push(conCatString);
      //     }
      //     return requiredParameter;
      // });
      // const parameterNeed = requiredParameter.join('&');
      //
      // if ( parameterNeed.length > parameterLength) {
      //   const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
      //   const businessPathUrl = commonReducer.appData.pathname_uri_business;
      //   const applyPath = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : "/";
      //   dispatch(setErrorWithValue("InvalidParameterLength"));
      //   this.props.history.push({
      //     pathname: `${applyPath}error`,
      //   })
      // }
    } else {
      const corporatePathUrl = commonReducer.appData.pathname_uri_commercial;
      const businessPathUrl = commonReducer.appData.pathname_uri_business;
      const applyPath = !LocalEnvironment ? isCommerical ? corporatePathUrl : businessPathUrl : "/";
      dispatch(setErrorWithValue("InvalidProductCode"))
      this.props.history.push({
        pathname: `${applyPath}error`,
      })
    }

  }

  handleToMyInfo() {
    const { commonReducer } = this.props;
    const jsonUrl = commonReducer.appData.applyPage.mockServiceURL;
    const realUrl = commonReducer.appData.applyPage.url;
    let pathname = window.location.pathname;
    const splitPathName = pathname.split("/");
    const isCommercial = splitPathName.indexOf("corporate") === 1;
    const redirectURL = isCommercial ? encodeURIComponent(commonReducer.appData.applyPage.redirect_uri_commercial) : encodeURIComponent(commonReducer.appData.applyPage.redirect_uri_business)
    const pathnameURL = isCommercial ? commonReducer.appData.pathname_uri_commercial : commonReducer.appData.pathname_uri_business;
    const redirect_uri = LocalEnvironment ? encodeURIComponent(commonReducer.appData.applyPage.redirect_uri) : redirectURL;
    const pathname_uri = LocalEnvironment ? commonReducer.appData.applyPage.redirect_uri : pathnameURL

    const url = commonReducer.appData.isMockService ? jsonUrl.replace("{redirect_uri}", redirect_uri).replace("{pathname}", pathname_uri) : realUrl;
    window.open(url, '_parent');
  }

  handleToWithoutMyinfo() {
    const { commonReducer } = this.props;
    const productId = commonReducer.appData.productCode;
    const params = queryString.parse(this.props.location.search);
    const productCode = params[productId] ? params[productId] : '';
    const url = commonReducer.appData.applyPage.withoutMyInfoURL[productCode.toUpperCase()];
    window.open(url, '_parent');
  }

  handleOnClearMessage() {
    const { dispatch } = this.props;
    dispatch(setErrorMessage(""));
  }

  
  render() {
    const { commonReducer } = this.props;
    const parameter = window.location.search;
    const params = queryString.parse(parameter);
    const productId = commonReducer.appData.productCode;
    const code = params[productId] ? params[productId].toUpperCase() : 'BB-BPL';
    const productTitle = commonReducer.appData.productTitle[code];

    const withMyinfoLabels = commonReducer.appData.applyPage.withMyinfo;
    // const withoutMyInfoLabels = commonReducer.appData.applyPage.withoutMyInfo;

    // const withoutDesc = withoutMyInfoLabels.description === "" ? "body-right-button-withoutDesc" : "";
    const withoutErrorMsg = commonReducer.compatibilityMessage === "" ? "apply-page-body-container-padding" : "";
    return (
      <div>
        {commonReducer.isLoading && <Loader />}
        <style dangerouslySetInnerHTML={{
          __html: `
        body { padding: 0;  background: url(./images/applyPageImages/background.png); background-size: cover;
        background-repeat: no-repeat;
        background-position: 100%;
        height: 100%; width: 100%;}
        .uob-form-loan-container { background: transparent;}
        .uob-content { padding: 0; }
        #sideNav { display: none }
        .logoRight { display: block!important;  }
        #home .title2 { display: none }
        section.resume-section { padding: 32px 35px 0!important;}
        .blue-uob-logo {padding: 26px 40px}
        .apply-page-tick {
          list-style-image: url('./images/applyPageImages/tick_list.svg');
          margin: 0px
        }
        `
        }} />
        {commonReducer.appData !== null &&
          <div>
            <div className='apply-page-header-container'>
              <div className="apply-page-header-title apply">
                <img className="img-size" alt="logo" src='./images/logos/uob-eBiz-Logo-blue.svg' />
                <div className="title-text"> {productTitle} </div>
              </div>
              <div className="displayHeader">
                <img className="img-size" alt="logo" src='./images/logos/uob-eBiz-Logo-blue.svg' />
                <div className="title-vertical-line"/>
                <div className="mob-prodtitle">
                  {productTitle.split(" | ")[0]}
                  <br/>
                  <span className="font-16">{productTitle.split(" | ")[1]}</span>
                </div>
              </div>
              <div className="right-by-you-container">
                <img alt="right-by-you" src='./images/logos/right-by-you-logo.png' />
              </div>
            </div>
            
            <div className={`corporate-apply-page-body-container ${withoutErrorMsg}`}>
              <div className="corporate-apply-page-body-left">
                <div className="corporate-body-left-content">
                  <div className="corporate-body-left-title" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.title }} />
                  <div className="corporate-body-left-subtite">
                    <div className="corporate-body-left-subtite-img">
                      <img alt="logo" src='./images/applyPageImages/clock.svg' />
                    </div>
                    <div className="corporate-body-left-subtite-text" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.subtite }} />
                  </div>
                  <div className="corporate-body-left-description" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.description }} />
                  <div className='corporate-body-left-button-container'>
                    <div className="corporate-body-left-button" onClick={() => this.handleToMyInfo()}>{withMyinfoLabels.buttonText}</div>
                  </div>
                </div>
                <div className="corporate-body-left-footer">
                  <div className="corporate-body-left-footer-image" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.footerImage }} />
                  <div className="corporate-body-left-footer-text" dangerouslySetInnerHTML={{ __html: withMyinfoLabels.footerDesc }}>
                  </div>
                </div>
              </div>
            </div>
          </div>
        }

        {commonReducer.messageContent !== "" && (
          <GenericErrorMessage
            {...this.props}
            interval={120}
            messageContent={commonReducer.messageContent}
            onClearMessage={this.handleOnClearMessage.bind(this)}
          />
        )}
      </div>
    );

  }

}

const mapStateToProps = (state) => {
  const { applyReducer, commonReducer, verifyReducer } = state;
  return { applyReducer, commonReducer, verifyReducer };
}

export default connect(mapStateToProps)(BusinessApplyPage);
