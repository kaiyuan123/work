import React, { Component } from "react";
import { connect } from "react-redux";
import { callNumberPopup } from "./../actions/commonAction";

class CallNumberPopup extends Component {		

	showCallNumberPopup(status) {
		const { dispatch } = this.props;
		dispatch(callNumberPopup(status));
	}

	render() {
		const { commonReducer } = this.props;
		const CallNumberPopup = commonReducer.appData.CallNumberPopup;
		
		return (
			<div className='save-popup-container call-popup'>
				<div className='save-popup-inner' >					
					<div className="save-popup-content">
						<p>{CallNumberPopup.number}</p>
						<div className="number-btn">
							<span onClick={() => this.showCallNumberPopup(false)}>Cancel</span>
							<span><a href="tel:+65 6259 8188">Call</a></span>
						</div>
					</div>
				</div>
			</div>
		)
	}

}

const mapStateToProps = state => {
    const { commonReducer } = state;
    return { commonReducer };
};

export default connect(mapStateToProps)(CallNumberPopup);
