import React, { Component } from 'react';
import { connect } from 'react-redux';
import greenTick from './../assets/images/green-tick-successful.svg';
import MessageBox from './../components/MessageBox/MessageBox';
import { sendDataToSparkline } from "./../common/utils";
import { Force_Google_Tag } from "./../api/httpApi";
import moment from "moment";

class ThankYouPage extends Component {
  componentWillMount() {
    const { commonReducer, applicationReducer } = this.props;
    const { isInitial } = applicationReducer;
    let pathname = window.location.pathname;
    const dataElement = commonReducer.appData.dataElementObj
    const productCode = applicationReducer.productCode ? applicationReducer.productCode.toLowerCase() : "";
    const referenceNo = commonReducer.referenceNo;
    const isThereBookAppointment = applicationReducer.dataObj && applicationReducer.dataObj.branchAppointment;
    if (!Force_Google_Tag) {
      if (isThereBookAppointment) {
        sendDataToSparkline(dataElement, productCode, "", false, false, isInitial, referenceNo, true, false, pathname, true);
      } else {
        sendDataToSparkline(dataElement, productCode, "", false, false, isInitial, referenceNo, true);
      }
    }
  }

  render() {
    const { commonReducer, applicationReducer } = this.props;
    const { dataObj, productCode } = applicationReducer;
    const applicationStatus = commonReducer.applicationStatus;

    const isThereBookAppointment = dataObj && dataObj.branchAppointment;
    let thankyouValues = commonReducer.appData.thankYouPage;

    if (isThereBookAppointment) {
      thankyouValues = commonReducer.appData.thankYouPageBookingAppointment;
    } else if (applicationStatus === 'PENDING') {
      thankyouValues = commonReducer.appData.thankYouPagePartner;
    }

    const code = productCode.toUpperCase();
    const productTitle = commonReducer.appData.productTitleThankYou;
    const codeDesc = productTitle[code]

    const referenceNo = commonReducer.referenceNo;
    const applicationExpiryDate = commonReducer.applicationExpiryDate;
    const date = moment().format("DD/MM/YYYY");
    const subTitle = thankyouValues.subtitle.replace("{referenceNo}", referenceNo).replace("{date}", date).replace("{productTitle}", codeDesc).replace("{applicationExpiryDate}", applicationExpiryDate);
    
    return (
      <div id="loadingPage" className='uob-content'>
        <div className="error-pg-container">
          <MessageBox
            isThankyou
            img={greenTick}
            title={thankyouValues.title}
            subtitle={subTitle}
            description={thankyouValues.description}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { commonReducer, applicationReducer, taxSelfDeclarationsReducer, applyReduer } = state;
  return { commonReducer, applicationReducer, taxSelfDeclarationsReducer, applyReduer };
}

export default connect(mapStateToProps)(ThankYouPage);
