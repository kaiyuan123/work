import React, { Component } from "react";
import { connect } from "react-redux";
import Review from "./../components/Review/Review";

export class ConfirmReviewDetails extends Component {
    render() {
        const { commonReducer, applicationReducer, operatingMandateReducer } = this.props;
        const { isKnockOut } = applicationReducer;
        const confirmDetailsValue = commonReducer.appData.confirmDetails.ConfirmReviewDetails;
        const pageToReview = operatingMandateReducer.signatorySelected.value === "" ? confirmDetailsValue.pageToReview.slice(0,5) :
        (isKnockOut ? confirmDetailsValue.pageToReview.slice(0,6) : confirmDetailsValue.pageToReview);
        return (
            <div className="reviewContent">
                <h1>{confirmDetailsValue.title}</h1>
                { pageToReview.map((reducerName, index) => <Review takeFromReducer={reducerName} {...this.props} key={index}/>) }
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, applicationReducer } = state;
    return { commonReducer, applicationReducer };
};

export default connect(mapStateToProps)(ConfirmReviewDetails);
