import React, { Component } from "react";
import { connect } from "react-redux";
import TextInput from "./../components/TextInput/TextInput";
import Checkbox from "./../components/Checkbox/Checkbox";
import Dropdown from "./../components/Dropdown/Dropdown";
import { mapValueToDescription } from "./../common/utils";
import { handleIsChecked } from "./../actions/accountSetupAction";
import SaveAndExit from "./SaveAndExit";

import {
	handleTextInputChange,
	setDropdownFocusStatus,
	selectDropdownItem,
	changeSearchInputValue
} from "./../actions/accountSetupAction";

class AccountDetails extends Component {

	handleSaveAndExit() {
		this.props.onSaveAndExit();
	}

	handleOnChange(data, field) {
		const { dispatch } = this.props;
		dispatch(handleTextInputChange(data, field));
	}

	handleDropdownBlur(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(false, field));
	}

	handleDropdownFocus(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(true, field));
	}

	handleDropdownClick(data, field) {
		const { dispatch, accountSetupReducer } = this.props;
		if (accountSetupReducer[field].value === data.value) {
			return;
		}
		dispatch(selectDropdownItem(data.value, data.description, field));
	}

	handleOnCheckbox(field, isToggled, text, isValid) {
		const { dispatch } = this.props;
		dispatch(handleIsChecked(field, isToggled, text, isValid));
	}

	handleOnSearchChange(e, field) {
		const { dispatch } = this.props;
		const value = e.target.value;
		dispatch(changeSearchInputValue(value, field));
	}

	render() {
		const { commonReducer, accountSetupReducer } = this.props;
		const { accountName, currencyOfAccount, chequebooks } = accountSetupReducer;
		const accountSetupValue = commonReducer.appData.accountSetup;
		const labels = accountSetupValue.labels;
		const errorMsgList = commonReducer.appData.errorMsgs;
		const errorMsgListOthers = commonReducer.appData.errorMsgsOthers;
		const inputValues = commonReducer.appData.inputValues
			? commonReducer.appData.inputValues
			: "";
		const countriesNamesMap = inputValues.countriesNamesMap;
		// const purposeOfAccountList = inputValues.purposeOfAccountList;
		const chequeBooksList = inputValues.chequebooks;
		const hideColon = accountSetupReducer.others.isToggled ? ":" : "";
		return (
			<div className="uob-content">
				<div className="save-button-container">
					<h1>{accountSetupValue.title}</h1>
					<SaveAndExit {...this.props} onClick={() => this.handleSaveAndExit()} />
				</div>
				<div className="uob-form-separator" />
				<p className="uob-headline">{accountSetupValue.headline}</p>
				<h1 className="sub-title">{accountSetupValue.subtitle}</h1>

				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow right-border positionRelative">
							<TextInput
								inputID={'accountName'}
								label={labels.accountName}
								value={accountName.value}
								errorMsg={accountName.errorMsg}
								onChange={data => this.handleOnChange(data, "accountName")}
								isValid={accountName.isValid}
								errorMsgList={errorMsgList}
								validator={["required", "maxSize|70"]}
								hasIcon={false}
								// isMyInfo={accountName.isMyInfo}
								isReadOnly={true}
								isDisabled={true}
								hasEdit={accountName.hasEdit}
							/>
						</div>
						<div className="halfRow positionRelative dropdown-nopadding">
							<Dropdown
								inputID="chequebooks"
								label={labels.chequebooks}
								dropdownItems={mapValueToDescription(
									chequeBooksList
								)}
								isFocus={chequebooks.isFocus}
								value={chequebooks.value}
								isValid={chequebooks.isValid}
								errorMsg={chequebooks.errorMsg}
								errorMsgList={errorMsgList}
								focusOutItem={true}
								validator={["required"]}
								searchValue={chequebooks.searchValue}
								onBlur={this.handleDropdownBlur.bind(this, "chequebooks")}
								onFocus={this.handleDropdownFocus.bind(this, "chequebooks")}
								onClick={data => this.handleDropdownClick(data, "chequebooks")}
								onSearchChange={event =>
									this.handleOnSearchChange(event, "chequebooks")
								}
							/>
						</div>
					</div>
				</div>
				<div className="top-border bottom-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow dropdown-nopadding">
							<Dropdown
								inputID="currencyOfAccount"
								label={labels.currencyOfAccount}
								focusOutItem={true}
								value={currencyOfAccount.value}
								dropdownItems={mapValueToDescription(
									countriesNamesMap
								)}
								currency={true}
								flagImgName={labels.currencyFlagCountry}
								currencyDescription={labels.currencyDescription}
								isReadOnly
								flagImg
							/>
						</div>

					</div>
				</div>
				<div className="multicheckbox-container" id="purposeOfAccountMain">
					<span className="checkBoxBottomSub checkbox-headlabel">{labels.purposeOfAccount}</span>
					<Checkbox
						description={accountSetupValue.purposeOfAccountList.labels.transactional}
						isChecked={accountSetupReducer.transactional.isToggled}
						onClick={() => this.handleOnCheckbox('transactional', accountSetupReducer.transactional.isToggled, 'Transactional')}
					/>
					<Checkbox
						description={accountSetupValue.purposeOfAccountList.labels.loanRepayment}
						isChecked={accountSetupReducer.loanRepayment.isToggled}
						onClick={() => this.handleOnCheckbox('loanRepayment', accountSetupReducer.loanRepayment.isToggled, 'Loan Repayment')}
					/>
					<Checkbox
						description={accountSetupValue.purposeOfAccountList.labels.investment}
						isChecked={accountSetupReducer.investment.isToggled}
						onClick={() => this.handleOnCheckbox('investment', accountSetupReducer.investment.isToggled, 'Investment')}
					/>
					<div className="flex-alignCenter">
						<Checkbox
							isChecked={accountSetupReducer.others.isToggled}
							onClick={() => this.handleOnCheckbox('others', accountSetupReducer.others.isToggled, 'Others')}
						/>
						<div className="others-container">
							<div className="others-label font-14 p-r-20 p-t-20">{`${accountSetupValue.purposeOfAccountList.labels.others}${hideColon}`}</div>
							{accountSetupReducer.others.isToggled &&
								<div className="bottom-border positionRelative p-t-20 width-others">
									<TextInput
										inputID={'otherPurpose'}
										label={accountSetupValue.purposeOfAccountList.labels.otherPurpose}
										isReadOnly={false}
										value={accountSetupReducer.otherPurpose.value}
										errorMsg={accountSetupReducer.otherPurpose.errorMsg}
										onChange={data => this.handleOnChange(data, "otherPurpose")}
										isValid={accountSetupReducer.otherPurpose.isValid}
										validator={["required", "maxSize|35"]}
										errorMsgList={errorMsgListOthers}
										hasIcon={false}
										type="Others"
									/>
								</div>
							}
							<br />
						</div>
					</div>
					<br />
					{!accountSetupReducer.purposeOfAccountIsValid && <div style={{
						color: 'red', fontSize: '14px',
						marginLeft: '20px'
					}}>This field is required</div>}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, accountSetupReducer } = state;
	return { commonReducer, accountSetupReducer };
};

export default connect(mapStateToProps)(AccountDetails);
