import React, { Component } from "react";
import { connect } from "react-redux";
import FileUpload from "./../components/FileUpload/FileUpload";
import {
	dispatchUpdateUploadProgress,
	dispatchUploadFile,
	dispatchErrorMsg,
	dispatchResetUploadStatus,
	dispatchDeleteImage
} from './../actions/uploadDocumentsAction';
import { setErrorMessage } from './../actions/commonAction';
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import SaveAndExit from "./SaveAndExit";

class UploadDocument extends Component {
	handleSaveAndExit() {
		this.props.onSaveAndExit();
	}

	updateUploadProgress(id, progress) {
		const { dispatch } = this.props;
		dispatch(dispatchUpdateUploadProgress(id, progress));
	}

	handleUploadFile(id, name, size) {
		const { dispatch } = this.props;
		dispatch(dispatchUploadFile(id, name, size));
	}

	handleErrorMsg(id, error) {
		const { dispatch } = this.props;
		dispatch(setErrorMessage(error));
		dispatch(dispatchErrorMsg(id, error));
	}

	handleUploadErrorMsg(id, error) {
		const { dispatch } = this.props;
		dispatch(setErrorMessage(error));
		dispatch(dispatchResetUploadStatus(id));
	}

	handleDeleteImage(id) {
		const { dispatch } = this.props;
		dispatch(dispatchDeleteImage(id));
	}

	handleToDocumentUploadPage() {
		this.props.onContinue();
	}

	isDocumentUploadPass() {
		const { uploadDocumentsReducer } = this.props;
		if (uploadDocumentsReducer.MADocument.inputValue === "") {
			this.handleUploadErrorMsg('MADocument', 'Upload Document is required.');
		}
		this.props.onCheck()
	}

	render() {
		const { commonReducer, uploadDocumentsReducer, applicationReducer } = this.props;
		const { MADocument } = uploadDocumentsReducer;
		const { initiateMyinfoData } = applicationReducer;
		const applicationID = initiateMyinfoData !== null ? initiateMyinfoData.id : null;
		// const globalErrors = commonReducer.appData.globalErrors;
		// const errorMsgs = commonReducer.appData.errorMsgs;

		const uploadDocuments = commonReducer.appData.uploadDocuments;
		const labels = uploadDocuments.labels;

		return (
			<div id="uploadDocument-section"
					 style={{ minHeight: '500px', height: (parseInt(window.innerHeight, 10) - 20) + 'px' }}>
				<div className="uob-content p-b-0">
					<div className="uob-input-separator">
						<div className="save-button-container">
							<h1>{uploadDocuments.title}</h1>
							<SaveAndExit {...this.props} onClick={() => this.handleSaveAndExit()}/>
						</div>
					</div>
					<div className="font-16 fileUploadSubTitle">
						{uploadDocuments.uploadDoc}
					</div>
					<div className="font-12" style={{ marginBottom: '20px', marginTop: '10px' }}>
						(Supported files: PDF, JPG, PNG with max file size of 3MB)
					</div>
					<div>
						<div className="mainUploadSection">
							<FileUpload
								inputID={'MADocument'}
								documentTitleName={'M&A Document'}
								docType={'ma'}
								identifier={applicationID}

								isValid={MADocument.isValid}
								fileSize={MADocument.fileSize}
								inputValue={MADocument.inputValue}
								progressValue={MADocument.progress}
								sampleGuide={uploadDocuments.guide}

								updateUploadProgress={(id, progress) => this.updateUploadProgress(id, progress)}
								handleUploadFile={(id, name, size) => this.handleUploadFile(id, name, size)}
								handleShowErrorMsg={(id, error) => this.handleErrorMsg(id, error)}
								handleShowUploadErrorMsg={(id, error) => this.handleUploadErrorMsg(id, error)}
								handleDeleteImage={(id) => this.handleDeleteImage(id)}
							/>
						</div>
					</div>
					<br/>
					<br/>
				</div>
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToDocumentUploadPage()}/>
				</div>

				{
					!this.props.onFixedButton() &&
					<div>
						<div className="uob-input-separator fixedContinue fixedContinueGray">
							<PrimaryButton label={labels.continueButton} onClick={() => this.isDocumentUploadPass()}/>
						</div>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, uploadDocumentsReducer } = state;
	return { commonReducer, uploadDocumentsReducer };
};

export default connect(mapStateToProps)(UploadDocument);
