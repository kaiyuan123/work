import React, { Component } from "react";
import { connect } from "react-redux";
import TextInput from "./../components/TextInput/TextInput";
import Dropdown from "./../components/Dropdown/Dropdown";
import Checkbox from "./../components/Checkbox/Checkbox";
import RadioButton from "./../components/RadioButton/RadioButton";
import { handleIsChecked, handleTextInputChange, handleButtonChange, setDropdownFocusStatus, setDropdownFocusStatusChange, selectDropdownItemChange, selectDropdownItem, handleTextInputChangeOthers, changeSearchInputValueOther, changeSearchInputValue, handleButtonChangeOthers, selectRadioItem, incrementCountry, addNewCountry, uncheckRest, uncheckNone, decreaseCountry, removeCountry, clearErrorMsgCountry, selectRadioItemNone, handleTextInputBlur, handleTextInputBlurOthers } from "./../actions/taxSelfDeclarationsAction";

import SecondaryButton from "./../components/SecondaryButton/SecondaryButton";
import deleteIcon from "./../assets/images/delete_icon.svg"
import { mapValueToDescription } from "../common/utils";

class TaxSelfDeclarations extends Component {

    handleOnBlurOthers(data, key, country, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputBlurOthers(data, key, country, field));

    }

    handleOnBlur(data, country, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputBlur(data, country, field));

    }

    handleRadioSelection(data, key, field) {
        const { dispatch } = this.props;
        dispatch(selectRadioItem(data, key, field));
    }

    handleRadioSelectionNone(data, field) {
        const { dispatch } = this.props;
        dispatch(selectRadioItemNone(data, field));
    }

    //Delete
    deleteCountry(key) {
        const { dispatch } = this.props;
        dispatch(decreaseCountry(key));
        dispatch(removeCountry(key));

    }

    handleOnCheckbox(field, isToggled) {
        const { dispatch } = this.props;
        if (field === "checkboxNone") {
            dispatch(uncheckRest());
        } else {
            dispatch(uncheckNone());
        }
        dispatch(handleIsChecked(field, isToggled));
    }

    handleOnSearchChange(e, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValue(value, field));
    }

    handleOnSearchChangeOthers(e, key, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValueOther(value, key, field));
    }

    handleOnChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChange(data, field));
    }

    handleOnTextChangeOthers(data, key, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChangeOthers(data, key, field));
    }

    handleOnButtonChange(data, field) {
        const { dispatch } = this.props;
        dispatch(handleButtonChange(data, field));
    }

    handleOnButtonChangeOthers(data, key, field) {
        const { dispatch } = this.props;
        dispatch(handleButtonChangeOthers(data, key, field));
    }

    handleDropdownFocus(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(true, field));
    }

    handleDropdownFocusChange(key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChange(true, key, field));
    }


    handleDropdownBlur(field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatus(false, field));
    }

    handleDropdownBlurChange(key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChange(false, key, field));
    }
    handleDropdownClick(data, field) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        if (taxSelfDeclarationsReducer[field].value === data.value) {
            return;
        }
        dispatch(selectDropdownItem(data.value, data.description, field));
    }

    handleDropdownClickChange(data, key, field) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        if (taxSelfDeclarationsReducer[key][field].value === data.value) {
            return;
        }

        if (field.includes("othersCountry")) {
            dispatch(clearErrorMsgCountry());
        }

        dispatch(selectDropdownItemChange(data.value, data.description, key, field));
    }

    addhaveTINNo(key) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        const tinNoOthersId = taxSelfDeclarationsReducer.tinNoOthersId;
        const maxKey = Math.max.apply(null, tinNoOthersId);
        dispatch(incrementCountry(maxKey));
        dispatch(addNewCountry(maxKey));

    }


    renderCountries() {
        const { taxSelfDeclarationsReducer, commonReducer } = this.props;
        const { tinNoOthersId } = taxSelfDeclarationsReducer;
        const excludes = ["SG", "US"];
        //haveTINNo mapping
        const maxKey = Math.max.apply(null, tinNoOthersId);
        const length = tinNoOthersId.length;
        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        const countriesNamesMap = inputValues.countriesNamesMap;
        const reasonsList = inputValues.reasons;

        const tmpArray = [];
        const errorMsgList = commonReducer.appData.errorMsgs;
        const errorMsgListOthers = commonReducer.appData.errorMsgsOthers;
        const maxCountries = parseInt(taxSelfDeclarationsValue.maxCountries, 10);

        tinNoOthersId.map((key, i) => {
            const bottomBorder = taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "Y" ? "" : "bottom-border";
            tmpArray.push(
                <div key={key} >
                    {length > 1 &&
                        <div className="crs-others-container">
                            <h1 className="sub-title partner-subtitle">{i + 1}</h1>
                            <img src={deleteIcon} className="partner-delete-icon" alt="delete" onClick={() => this.deleteCountry(key)} />
                        </div>
                    }
                    <div className="crs-container">
                        <div className={`top-border ${bottomBorder}`}>
                            <div className="fullTable confirmDetails-flexContainer">
                                <div className="halfRow right-border positionRelative dropdown-nopadding">
                                    <Dropdown
                                        flagImg
                                        inputID={`othersCountry${key}`}
                                        label={labels.othersCountry}
                                        value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value}
                                        errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.errorMsg}
                                        isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.isValid}
                                        isFocus={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.isFocus}
                                        dropdownItems={mapValueToDescription(
                                            countriesNamesMap
                                        )}
                                        searchValue={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.searchValue}
                                        onBlur={this.handleDropdownBlurChange.bind(this, `tinNoOthers${key}`, 'othersCountry')}
                                        onFocus={this.handleDropdownFocusChange.bind(this, `tinNoOthers${key}`, 'othersCountry')}
                                        onClick={data => this.handleDropdownClickChange(data, `tinNoOthers${key}`, 'othersCountry')}
                                        onSearchChange={event =>
                                            this.handleOnSearchChangeOthers(event, `tinNoOthers${key}`, 'othersCountry')
                                        }
                                        validator={["required"]}
                                        excludes={excludes}
                                    />
                                </div>
                                <div className="halfRow positionRelative">
                                    <div className="crs-buttons p-t-5">
                                        <div className="txt-container">
                                            <span className="crs-txt">{labels.haveTINNo}</span>{!taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.isValid &&
                                                <span className="errorMsg">{labels.errorMsgRequired}</span>}
                                        </div>
                                        <div className="flex-1">
                                            <input type="radio" id={`othersYes${key}`} checked={taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "Y" ? true : false} name={`radioButtonOthers${key}`} value="Y" onChange={(e) => this.handleOnButtonChangeOthers(e.target.value, `tinNoOthers${key}`, "haveTINNo")} />
                                            <label className="lbl-first" htmlFor={`othersYes${key}`}>{labels.yesButton}</label>

                                            <input type="radio" id={`othersNo${key}`} name={`radioButtonOthers${key}`} value="N" checked={taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "N" ? true : false} onChange={(e) => this.handleOnButtonChangeOthers(e.target.value, `tinNoOthers${key}`, "haveTINNo")} />
                                            <label className="lbl-second" htmlFor={`othersNo${key}`}>{labels.noButton}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "Y" &&
                            <div className="sub-div-common bottom-border">
                                <div className="p-l-10 positionRelative">
                                    <TextInput
                                        inputID={`othersTINNo${key}`}
                                        label={labels.othersTINNo}
                                        value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.value}
                                        errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.errorMsg}
                                        isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.isValid}
                                        onChange={(data) => this.handleOnTextChangeOthers(data, `tinNoOthers${key}`, 'othersTINNo')}
                                        onBlur={(data) => this.handleOnBlurOthers(data, `tinNoOthers${key}`, taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value, 'othersTINNo')}
                                        errorMsgList={errorMsgList}
                                        validator={["required", "isAlphanumeric", "maxSize|19"]}
                                        hasIcon={false}
                                    />
                                </div>
                            </div>
                        }

                        {taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value === "N" &&
                            <div>
                                <div className="crs-txt-container">
                                    <span className="crs-txt crs-questions">{labels.reason}</span>
                                </div>
                                <RadioButton
                                    inputID={`reasons${key}`}
                                    value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.value}
                                    radioObj={mapValueToDescription(
                                        reasonsList
                                    )}
                                    isDisabled={false}
                                    errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.errorMsg}
                                    isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.isValid}
                                    onClick={(data) => this.handleRadioSelection(data, `tinNoOthers${key}`, "reasons")}
                                />
                                {taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.value === "3" &&
                                    <div className="p-l-65">
                                        <div className="others-container">
                                            <div className="others-label p-r-0">{labels.othersNone}</div>
                                            <div className="bottom-border positionRelative width-others">
                                                <TextInput
                                                    inputID={`othersOthers${key}`}
                                                    label={labels.othersNone}
                                                    value={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers.value}
                                                    errorMsg={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers.errorMsg}
                                                    isValid={taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers.isValid}
                                                    onChange={(data) => this.handleOnTextChangeOthers(data, `tinNoOthers${key}`, 'othersOthers')}
                                                    onBlur={(data) => this.handleOnBlurOthers(data, `tinNoOthers${key}`, taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value, 'othersOthers')}
                                                    errorMsgList={errorMsgListOthers}
                                                    validator={["required", "maxSize|70"]}
                                                    type="Others"
                                                    maxCharacters={70}
                                                    hasIcon={false}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        }

                        {maxKey === key && tinNoOthersId.length < maxCountries &&
                            <SecondaryButton
                                label={labels.addCountry}
                                onClick={() => this.addhaveTINNo(key)}
                            />
                        }
                    </div>
                </div>
            );
            return tmpArray;
        })
        return tmpArray;
    }

    render() {
        const { commonReducer, taxSelfDeclarationsReducer } = this.props;
        const { checkboxSG, checkboxUS, checkboxOthers, checkboxNone, tinNoSG, tinNoUS, countryNone, isUSResident, taxResidencyIsValid, haveTINNoNone, tinNoNone, reasonsNone, othersNone } = taxSelfDeclarationsReducer;
        //Lists
        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";
        const errorMsgList = commonReducer.appData.errorMsgs;
        const errorMsgListOthers = commonReducer.appData.errorMsgsOthers;
        const countriesNamesMap = inputValues.countriesNamesMap;
        const reasonsList = inputValues.reasons;

        //Values
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        const bottomBorder = haveTINNoNone.value === "Y" ? "" : "bottom-border";
        return (
            <div id="taxResidency">
                <h1 className="sub-title">{taxSelfDeclarationsValue.subtitle}</h1>
                <p className="sub-headline">{taxSelfDeclarationsValue.subHeadline}</p>
                {!taxResidencyIsValid
                    &&
                    <div className="errorMsg">{labels.errorMsgRequiredOne}</div>
                }
                <Checkbox
                    inputID="checkboxSG"
                    description={labels.checkboxSG}
                    isChecked={checkboxSG.isToggled}
                    onClick={() => this.handleOnCheckbox('checkboxSG', checkboxSG.isToggled)}
                />
                {
                    checkboxSG.isToggled &&
                    <div className="crs-container">
                        <div className="sub-div-common bottom-border">
                            <div className="p-l-10 positionRelative">
                                <TextInput
                                    inputID="tinNoSG"
                                    label={labels.tinNoSG}
                                    value={tinNoSG.value}
                                    errorMsg={tinNoSG.errorMsg}
                                    onChange={data => this.handleOnChange(data, "tinNoSG")}
                                    onBlur={data => this.handleOnBlur(data, "SG", "tinNoSG")}
                                    isValid={tinNoSG.isValid}
                                    validator={["required", "isAlphanumeric", "maxSize|10"]}
                                    errorMsgList={errorMsgList}
                                    maxCharacters={10}
                                    hasIcon={false}
                                />
                            </div>
                        </div>
                    </div>
                }
                <Checkbox
                    inputID="checkboxUS"
                    description={labels.checkboxUS}
                    isChecked={checkboxUS.isToggled}
                    onClick={() => this.handleOnCheckbox('checkboxUS', checkboxUS.isToggled)}
                />
                {
                    checkboxUS.isToggled &&
                    <div className="crs-container">
                        <div className="top-border bottom-border">
                            <div className="fullTable confirmDetails-flexContainer">
                                <div className="halfRow right-border positionRelative">
                                    <TextInput
                                        inputID="tinNoUS"
                                        label={labels.tinNoUS}
                                        value={tinNoUS.value}
                                        errorMsg={tinNoUS.errorMsg}
                                        onChange={data => this.handleOnChange(data, "tinNoUS")}
                                        isValid={tinNoUS.isValid}
                                        validator={["required", "isNumber", "exactSize|9"]}
                                        errorMsgList={errorMsgList}
                                        hasIcon={false}
                                        onBlur={data => this.handleOnBlur(data, "US", "tinNoUS")}
                                        maxCharacters={9}
                                    />
                                </div>
                                <div className="halfRow positionRelative">
                                    <div className="crs-buttons">
                                        <div className="txt-container">
                                            <span className="crs-txt">{labels.isUSResident}</span>
                                            {!isUSResident.isValid && <span className="errorMsg">{labels.errorMsgRequired}</span>}
                                        </div>
                                        <div className="flex-1">
                                            <input type="radio" id="usYes" name="radioButtonUS" value="Y" checked={isUSResident.value === "Y" ? true : false} onChange={(e) => this.handleOnButtonChange(e.target.value, "isUSResident")} />
                                            <label className="lbl-first" htmlFor="usYes">{labels.yesButton}</label>

                                            <input type="radio" id="usNo" name="radioButtonUS" value="N" checked={isUSResident.value === "N" ? true : false} onChange={(e) => this.handleOnButtonChange(e.target.value, "isUSResident")} />
                                            <label className="lbl-second" htmlFor="usNo">{labels.noButton}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                <Checkbox
                    inputID="checkboxOthers"
                    description={labels.checkboxOthers}
                    isChecked={checkboxOthers.isToggled}
                    onClick={() => this.handleOnCheckbox('checkboxOthers', checkboxOthers.isToggled)}
                />
                {
                    checkboxOthers.isToggled &&
                    <div>
                        {this.renderCountries()}
                    </div>


                }
                <Checkbox
                    inputID="checkboxNone"
                    description={labels.checkboxNone}
                    isChecked={checkboxNone.isToggled}
                    onClick={() => this.handleOnCheckbox('checkboxNone', checkboxNone.isToggled)}
                />

                {
                    checkboxNone.isToggled &&
                    <div className="crs-container">
                        <span className="crs-txt float-none">{labels.none}</span>
                        <div className="p-t-20">
                            <div className={`top-border ${bottomBorder}`}>
                                <div className="fullTable confirmDetails-flexContainer">
                                    <div className="halfRow right-border positionRelative dropdown-nopadding">
                                        <Dropdown
                                            inputID="countryNone"
                                            label={labels.countryNone}
                                            dropdownItems={mapValueToDescription(
                                                countriesNamesMap
                                            )}
                                            isFocus={countryNone.isFocus}
                                            value={countryNone.value}
                                            isValid={countryNone.isValid}
                                            errorMsg={countryNone.errorMsg}
                                            focusOutItem={true}
                                            searchValue={countryNone.searchValue}
                                            onBlur={this.handleDropdownBlur.bind(this, "countryNone")}
                                            onFocus={this.handleDropdownFocus.bind(this, "countryNone")}
                                            onClick={data => this.handleDropdownClick(data, "countryNone")}
                                            onSearchChange={event =>
                                                this.handleOnSearchChange(event, "countryNone")
                                            }
                                            flagImg
                                            validator={["required"]}
                                        />
                                    </div>
                                    <div className="halfRow positionRelative">
                                        <div className="crs-buttons">
                                            <div className="txt-container">
                                                <span className="crs-txt">{labels.haveTINNo}</span>
                                                {!haveTINNoNone.isValid && <span className="errorMsg">{labels.errorMsgRequired}</span>}
                                            </div>
                                            <div className="flex-1">
                                                <input type="radio" id="noneYes" name="radioButtonNone" value="Y" checked={haveTINNoNone.value === "Y" ? true : false} onChange={(e) => this.handleOnButtonChange(e.target.value, "haveTINNoNone")} />
                                                <label className="lbl-first" htmlFor="noneYes">{labels.yesButton}</label>

                                                <input type="radio" id="noneNo" name="radioButtonNone" value="N" checked={haveTINNoNone.value === "N" ? true : false} onChange={(e) => this.handleOnButtonChange(e.target.value, "haveTINNoNone")} />
                                                <label className="lbl-second" htmlFor="noneNo">{labels.noButton}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {haveTINNoNone.value === "Y" &&
                                <div className="sub-div-common bottom-border">
                                    <div className="p-l-10 positionRelative">
                                        <TextInput
                                            inputID="tinNoNone"
                                            label={labels.othersTINNo}
                                            value={tinNoNone.value}
                                            errorMsg={tinNoNone.errorMsg}
                                            isValid={tinNoNone.isValid}
                                            onChange={data => this.handleOnChange(data, "tinNoNone")}
                                            errorMsgList={errorMsgList}
                                            validator={["required", "isAlphanumeric", "maxSize|19"]}
                                            onBlur={data => this.handleOnBlur(data, countryNone.value, "tinNoNone")}
                                            hasIcon={false}
                                        />
                                    </div>
                                </div>
                            }

                            {haveTINNoNone.value === "N" &&
                                <div>
                                    <div className="crs-txt-container">
                                        <span className="crs-txt crs-questions">{labels.reason}</span>
                                    </div>
                                    <RadioButton
                                        inputID="reasonsNone"
                                        value={reasonsNone.value}
                                        radioObj={mapValueToDescription(
                                            reasonsList
                                        )}
                                        isDisabled={false}
                                        errorMsg={reasonsNone.errorMsg}
                                        isValid={reasonsNone.isValid}
                                        onClick={(data) => this.handleRadioSelectionNone(data, "reasonsNone")}
                                    />
                                    {reasonsNone.value === "3" &&
                                        <div className="p-l-65">
                                            <div className="others-container">
                                                <div className="others-label p-r-0">{labels.othersNone}</div>
                                                <div className="bottom-border positionRelative width-others">
                                                    <TextInput
                                                        inputID="othersNone"
                                                        maxCharacters={70}
                                                        label={labels.othersNone}
                                                        value={othersNone.value}
                                                        errorMsg={othersNone.errorMsg}
                                                        onChange={data => this.handleOnChange(data, "othersNone")}
                                                        // onBlur={data => this.handleOnBlur(data, "othersNone")}
                                                        isValid={othersNone.isValid}
                                                        validator={["required", "maxSize|70"]}
                                                        errorMsgList={errorMsgListOthers}
                                                        hasIcon={false}
                                                        type="Others"
                                                    />
                                                </div>
                                            </div>
                                        </div>}
                                </div>
                            }
                        </div>
                    </div>
                }
            </div>
        );
    }

}
const mapStateToProps = state => {
    const { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer } = state;
    return { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer };
};

export default connect(mapStateToProps)(TaxSelfDeclarations);
