// company basic details (basic details, registered address, mailing address)
// company overall details (table view and review grants)
import React, { Component } from "react";
import { connect } from "react-redux";
import CompanyBasicDetails from "./CompanyBasicDetails";
import CompanyOverallDetails from "./CompanyOverallDetails";
import AskQuestionPopup from "./AskQuestionPopup";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import { handleAskQuestionPopup } from "./../actions/askQuestionsAction";

class CompanyDetailsPage extends Component {

	handlePopup(popuptoggle, questionObj) {
		this.props.onContinue();
		const { dispatch } = this.props;
		dispatch(handleAskQuestionPopup(popuptoggle, questionObj));
	}

	isCompanyDetailsPassing() {
		this.props.onCheck()
	}

	render() {
		const { commonReducer, askQuestionsReducer } = this.props;
		const { popuptoggle } = askQuestionsReducer;
		const labels = commonReducer.appData.companyDetails.labels;
		const askQuestionsValue = commonReducer.appData.askQuestion;
		const questionnaires = askQuestionsValue.questions;
		return (
			<div id="companyDetails-section">
				<CompanyBasicDetails {...this.props} />
				<CompanyOverallDetails {...this.props} />
				<div className="uob-input-separator" />
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => { this.handlePopup(true, questionnaires) }} />
				</div>

				{popuptoggle &&
					<AskQuestionPopup {...this.props} />
				}

				{
					!this.props.onFixedButton() &&
					<div>
					{
						// <SaveAndExit {...this.props} onClick={() => this.isCompanyDetailsPassing()} />
					}
					<div className="uob-input-separator fixedContinue fixedContinueGray">
						<PrimaryButton label={labels.continueButton} onClick={() => this.isCompanyDetailsPassing()} />
					</div>
					</div>
				}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, companyBasicDetailsReducer, localAddressInputReducer, askQuestionsReducer } = state;
	return { commonReducer, companyBasicDetailsReducer, localAddressInputReducer, askQuestionsReducer };
};

export default connect(mapStateToProps)(CompanyDetailsPage);
