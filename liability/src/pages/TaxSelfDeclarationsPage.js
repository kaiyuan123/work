import React, { Component } from "react";
import { connect } from "react-redux";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import TaxSelfDeclarations from "./TaxSelfDeclarations";
import FATCA from './FATCA';
import SaveAndExit from "./SaveAndExit";

class TaxSelfDeclarationsPage extends Component {

    handleSaveAndExit() {
        this.props.onSaveAndExit();
    }

    handleToASRDetailsPage() {
        this.props.onContinue();
    }

    isCRSCheckingPass() {
        this.props.onCheck()
    }

    render() {
        const { commonReducer, contactDetailsReducer } = this.props;
        // const { checkboxSG, checkboxUS, checkboxOthers, checkboxNone, tinNoSG, tinNoUS, countryNone, haveTINNoNone, tinNoNone, reasonsNone, isUSResident, fatcaCRSStatus } = taxSelfDeclarationsReducer;
        const { businessConstitution } = contactDetailsReducer;

        //Values
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;

        //Validation

        const isNotSoleProp = businessConstitution.value !== "S";

        return (
            <div className="uob-content" id="taxSelfDeclarations-section">
                <div className="save-button-container">
                    <h1>{taxSelfDeclarationsValue.title}</h1>
                    <SaveAndExit {...this.props} onClick={() => this.handleSaveAndExit()} />
                </div>
                <p className="uob-headline" dangerouslySetInnerHTML={{ __html: taxSelfDeclarationsValue.headline }} />
                <TaxSelfDeclarations {...this.props} />
                {isNotSoleProp &&
                    <div id="FATCA-section">
                        <FATCA {...this.props} />
                    </div>
                }
                <div className="uob-input-separator fixedContinue">
                    <PrimaryButton label={labels.continueButton} onClick={() => this.handleToASRDetailsPage()} isLoading={commonReducer.isProcessing} />
                </div>

                {
                    !this.props.onFixedButton() &&
                    <div>
                        <div className="uob-input-separator fixedContinue fixedContinueGray">
                            <PrimaryButton label={labels.continueButton} onClick={() => this.isCRSCheckingPass()} isLoading={commonReducer.isProcessing} />
                        </div>
                    </div>
                }
                <div className="tax-glossary" dangerouslySetInnerHTML={{ __html: taxSelfDeclarationsValue.glossary }} />
            </div >
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer, contactDetailsReducer } = state;
    return { commonReducer, companyBasicDetailsReducer, taxSelfDeclarationsReducer, contactDetailsReducer };
};

export default connect(mapStateToProps)(TaxSelfDeclarationsPage);
