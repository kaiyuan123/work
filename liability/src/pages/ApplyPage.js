import React, { Component } from 'react';
import { connect } from 'react-redux';
import queryString from 'query-string';
import BusinessApplyPage from './BusinessApplyPage';
import CorporateApplyPage from './CorporateApplyPage';
import { LocalEnvironment } from "./../api/httpApi"


export class ApplyPage extends Component {

	render() {
		const { commonReducer } = this.props;
		const parameter = window.location.search;
		const params = queryString.parse(parameter);
		const productId = commonReducer.appData.productCode;
		let pathname = window.location.pathname;
		const isCorporate = LocalEnvironment ? true : pathname.includes("corporate");
		const isCommerical = params[productId] !== "" && params[productId] ? (params[productId].toLowerCase()).includes("cmb", 0) && isCorporate : false;
		return (
			<div>
				{isCommerical && <CorporateApplyPage {...this.props} />}
				{!isCommerical && <BusinessApplyPage {...this.props} />}
			</div>
		);

	}

}

const mapStateToProps = (state) => {
	const { applyReducer, commonReducer, verifyReducer } = state;
	return { applyReducer, commonReducer, verifyReducer };
}

export default connect(mapStateToProps)(ApplyPage);
