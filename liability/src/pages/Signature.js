import React, { Component } from "react";
import { connect } from "react-redux";
import SignatureUpload from "./../components/SignatureUpload/SignatureUpload";
import MobileSignature from "./../components/MobileSignature/MobileSignature";
import { clearSignature, saveSignature } from "./../actions/drawSignatureAction";
import { isMobileDevice } from "./../actions/commonAction";
import {
	saveUploadDocument
} from "./../actions/applicationAction";

export class Signature extends Component {
	// componentDidMount() {
	// 	const { dispatch } = this.props;
	//
	// 	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
	// 			window.navigator.userAgent
	// 		)) {
	// 		return dispatch(isMobileDevice(true));
	// 	}
	//
	// 	return dispatch(isMobileDevice(false));
	// }

	clearSignature() {
		const { dispatch } = this.props;
		dispatch(clearSignature());
	}

	getImageData(trimData) {
		const { dispatch, applicationReducer } = this.props;
		const { initiateMyinfoData } = applicationReducer;
		const applicationID = initiateMyinfoData !== null ? initiateMyinfoData.id : null;
		const trimmedDataURL = trimData.getTrimmedCanvas().toDataURL('image/png');

		const avatarImage = trimData.getTrimmedCanvas();
		const MAX_QUALITY_IMAGE = 0.9;
		const TYPE_IMAGE = "image/jpeg";

		dispatch(saveSignature(trimmedDataURL));

		if (!HTMLCanvasElement.prototype.toBlob) {
			Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
				value: function (callback, type, quality) {
					let canvas = this;
					setTimeout(function() {
						let binStr = atob( canvas.toDataURL(type, quality).split(',')[1] ),
							len = binStr.length,
							arr = new Uint8Array(len);

						for (let i = 0; i < len; i++ ) {
							arr[i] = binStr.charCodeAt(i);
						}
						callback( new Blob( [arr], {type: type || 'image/png'} ) );
					});
				}
			});
		}

		avatarImage.toBlob(function (blob) {
				const formData = new window.FormData();
				formData.append("file", blob, "signature.png");
				formData.append("documentType","signature");
				dispatch(saveUploadDocument(formData, applicationID));
			},
			TYPE_IMAGE,
			MAX_QUALITY_IMAGE
		);
	}

	render() {
		const { commonReducer } = this.props;
		const signatureUploadValue = commonReducer.appData.signatureUpload;
		if (commonReducer.isMobile) {
			return (
				<div id="signatureUpload-section" style={{paddingTop: '20px'}}>
					<div>
						<h1 className="sectionTitle">{signatureUploadValue.mobileTitle}</h1>
					</div>
					<p className="uob-headline" >
						{signatureUploadValue.mobileHeadline}
					</p>
					<MobileSignature {...this.props} />
				</div>
			)
		}
		return (
			<div id="signatureUpload-section">
				<SignatureUpload {...this.props} />
			</div>
		)
	}
}

const mapStateToProps = state => {
	const { commonReducer,applicationReducer, signatureReducer, drawSignatureReducer } = state;
	return { commonReducer,applicationReducer, signatureReducer, drawSignatureReducer };
};

export default connect(mapStateToProps)(Signature);
