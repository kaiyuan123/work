import React, { Component } from "react";
import { connect } from "react-redux";
import closeIcon from "./../assets/images/cross-grey.svg";
import imgUrl from "./../assets/images/info.svg";
import { handleHideAskQuestionPopup, handleIsChecked } from "./../actions/askQuestionsAction";
import { askQuestionsSubmission } from "./../actions/applicationAction";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import { scrollToPopupErrorElement, scrollToElement, scrollToSection } from "./../actions/commonAction";
import { handleErrorMessage } from "./../actions/askQuestionsAction";
import { findTrueInList } from "./../common/utils";
import { setWhichKnockOutScenarioFalse, isKnockOutScenario } from "./../actions/applicationAction";
import { sendDataToSparkline } from "./../common/utils";
import { Force_Google_Tag } from "./../api/httpApi"

class AskQuestionPopup extends Component {
	// constructor() {
	//     super();
	//     debugger;
	//     this.state = { 
	//       height: window.outerHeight, 
	//       width: window.outerWidth < 768 ? 100+'%' : window.outerWidth-document.querySelector('#sideNav').clientWidth-10
	//     };
	//     this.updateDimensions = this.updateDimensions.bind(this);
	//  }

	// componentDidMount() {
	//     window.addEventListener("resize", this.updateDimensions);
	// }

	// updateDimensions() {
	//     this.setState({
	//       height: window.outerHeight, 
	//       width: window.outerWidth
	//     });
	// }

	componentWillMount() {
		const { dispatch } = this.props;
		dispatch(scrollToElement("popupDetails"));
	}

	checkEmptyFields(fields, errorMsg, action) {
	    const { dispatch } = this.props;
	    let errorCount = 0;
	    fields[0].map((question, i) => {
            return question.subType.length > 0 ?
                question.subType.map((quesSub, j) => {
                    return  quesSub.value === "" && dispatch(action(quesSub, i, j, errorMsg));
                })
            : question.value === "" ? dispatch(action(question, i, errorMsg)) : '';
        });

	    dispatch(scrollToPopupErrorElement());
	    if (errorCount > 0) {
	      return false;
	    }
	    return true;
	  }

	isAskQuestionPopupPassing() {
		const {commonReducer, askQuestionsReducer } = this.props;
		const { questionnaires } = askQuestionsReducer;
		let errorCount = 0;
    	let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;

		if (!this.checkEmptyFields([questionnaires[0]], requiredMsg, handleErrorMessage)) {
	      errorCount++
	    }

	    if (errorCount > 0) {
	      return false
	    }
	    return true;
	}

	handlePopup(popuptoggle) {
		const { dispatch } = this.props;
		dispatch(handleHideAskQuestionPopup(popuptoggle));
		dispatch(scrollToSection("companyDetails"));
	}

	handleToAccountSetupPage(popuptoggle) {
		const { dispatch, askQuestionsReducer, applicationReducer, commonReducer} = this.props;
    	const { isInitial, initiateMyinfoData, productCode } = applicationReducer
		const dataElement = commonReducer.appData.dataElementObj;
		const id = initiateMyinfoData && initiateMyinfoData.referenceNumber && isInitial ? initiateMyinfoData.referenceNumber : "";
		const code = productCode ? productCode.toLowerCase(): "";
		if (!Force_Google_Tag) {
			sendDataToSparkline(dataElement, code, 4, false, false, isInitial, id);
		}
		const {questionnaires} = askQuestionsReducer;
		let tmpArray = [];

		questionnaires[0].map((question, i) => {
			return question.subType.length > 0 ?
				[ tmpArray.push({'id':question.id,'value':question.value}),
				    question.subType.map((quesSub, j) => {
				    	return tmpArray.push({'id':quesSub.id,'value':quesSub.value});
				    })
				] :
				tmpArray.push({'id':question.id,'value':question.value})
		})


		const applicationID = initiateMyinfoData !== null ? initiateMyinfoData.id : null;
		const dataObj = {
			questions:  tmpArray
		}
		if (findTrueInList("Y", dataObj.questions)) {
			dispatch(isKnockOutScenario(true));
			dispatch(setWhichKnockOutScenarioFalse("askQuestion"))
		}
		dispatch(askQuestionsSubmission(dataObj, applicationID));
		
	}

	handleOnRadioButton(value, lengthofButtonChecked,index,subindex) {
		const { dispatch } = this.props;
		dispatch(handleIsChecked(value, lengthofButtonChecked, index, subindex));
	}


	render() {
		const { commonReducer, askQuestionsReducer } = this.props;
		const askQuestionsValue = commonReducer.appData.askQuestion;
		const questionArr = askQuestionsValue.questions;
		const lengthofButtonChecked = askQuestionsReducer.noOfButtonChecked;
		let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
		const leftNavWidth = document.querySelector('#sideNav').clientWidth;
		const generateQuestions = (question, key, subkey) => {
			return (
					<div className={key % 2 === 0 ? 'question-container odd-class':'question-container even-class'}>
						<div className="qnt-txt custom-tooltip" dangerouslySetInnerHTML={{ __html: question.desc }}></div>
						<div className="buttons-container" id={`question${question.id}`}>
							{
								!question.isValid && <span className="popup-errorMsg">{requiredMsg}</span>
							}
							<div>
								<input
									type="radio"
									id={`yes${question.id}`}
									name={`question${question.id}`}
									value={'Y'}
									onChange={(e) => this.handleOnRadioButton(e.target.value, lengthofButtonChecked, key, subkey)}
								/>
								<label className="lbl-first" htmlFor={`yes${question.id}`}>{askQuestionsValue.yes}</label>
							</div>
							<div>
								<input
									type="radio"
									id={`no${question.id}`}
									name={`question${question.id}`}
									value={'N'}
									onChange={(e) => this.handleOnRadioButton(e.target.value, lengthofButtonChecked,key, subkey)}
								/>
								<label className="lbl-second" htmlFor={`no${question.id}`}>{askQuestionsValue.no}</label>
							</div>
						</div>
					</div>
			)
		}

		return (
			<div>
				<div className='popup' id='popupDetails'>
					<div className='popup_inner'>
						<div className="inner-content">
							<span>
								<img className="close-icon" src={closeIcon} alt="Close-Icon" onClick={() => this.handlePopup(false)} />
							</span>
							<h1>{askQuestionsValue.title}</h1>
							<div className="prefillSubtitle-container">
								<div className="prefillSubtitle-text">
									<img className="prefillSubtitle-icon" src={imgUrl} />
									{askQuestionsValue.prefillSubtitle}
								</div>
							</div>
							<div className="scrollable">
									{questionArr.map((question, i) => (
										<div key={i}>
											{question.title !== '' && <div className='question-header'>{question.title}</div>}
											{	question.subType.length === 0 ? generateQuestions(question,i) :
												<div>
												    <div className="sub-question-header" dangerouslySetInnerHTML={{ __html: question.desc }} />
													<div className={i % 2 === 0 ? 'odd-class child':'even-class'}>
														<span className="list-number" id="question3">{i+1+'.'}</span>
														<ol className="alpha sub-list">
															{question.subType.map((quest, j) => (
																<li key={j}>
																{generateQuestions(quest, i, j)}
																</li>
															))}
														</ol>
													</div>
												</div>
											}

										</div>
									))}


							</div>
						</div>
						<div className="uob-input-separator fixedContinue">
							<PrimaryButton label={askQuestionsValue.confirmButton} onClick={() => this.handleToAccountSetupPage(false)} isLoading={commonReducer.isProcessing} />
						</div>


						{
							lengthofButtonChecked !== askQuestionsReducer.noOfQuestions &&
							<div className="uob-input-separator fixedContinue fixedContinueGray">
								<PrimaryButton label={askQuestionsValue.nextButton} onClick={() => this.isAskQuestionPopupPassing()} isLoading={commonReducer.isProcessing} />
							</div>
						}

					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer } = state;
	return { commonReducer };
};


export default connect(mapStateToProps)(AskQuestionPopup);
