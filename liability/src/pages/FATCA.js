import React, { Component } from "react";
import { connect } from "react-redux";
import TableViewFATCA from "./../components/TableView/TableViewFATCA/TableViewFATCA";
import Dropdown from "./../components/Dropdown/Dropdown";
import SecondaryButton from "./../components/SecondaryButton/SecondaryButton";
import deleteIcon from "./../assets/images/delete_icon.svg";
import TextInput from "./../components/TextInput/TextInput";
import SearchBar from './../components/SearchBar/SearchBar';
import { handleTextInputChangeOthers, setDropdownFocusStatusChange, changeSearchInputValueOther, selectDropdownItemChange, incrementCP, addNewCP, decreaseCP, removeCP, autoGetLocalAddress, handleIsCheckedCP, handleButtonChangeCP, handleButtonChangeOthersCP, selectDropdownItemChangeCP, changeSearchInputValueOtherCP, setDropdownFocusStatusChangeCP, handleTextInputChangeOthersCP, selectRadioItemCP, incrementCountryCP, addNewCountryCP, decreaseCountryCP, removeCountryCP, clearErrorMsgCountryCP, handleSearchInputChange, handleOnSelectSearchItem, handleSearchInputFocus, handleTextInputBlurOthers, handleTextInputBlurOthersCP, clearErrorMsgCPId } from "./../actions/taxSelfDeclarationsAction";
import { handleTextInputWithKeyFocus } from "./../actions/commonAction";
import { mapValueToDescription } from "../common/utils";
import Checkbox from "./../components/Checkbox/Checkbox";
import RadioButton from "./../components/RadioButton/RadioButton";

class FATCA extends Component {

    handleOnBlurOthers(data, cpKey, key, country, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputBlurOthersCP(data, cpKey, key, country, field));
    }

    handleSearchOnFocus(key, status) {
        const { dispatch } = this.props;
        dispatch(handleSearchInputFocus(key, status));
    }

    handleChooseSearchItem(key, item) {
        const { dispatch } = this.props;
        dispatch(handleOnSelectSearchItem(key, item));
    }

    handleOnChangeSearchValue(data, key) {
        const { dispatch } = this.props;
        dispatch(handleSearchInputChange(key, data));
    }

    //Delete
    deleteCountry(cpKey, key) {
        const { dispatch } = this.props;
        dispatch(decreaseCountryCP(cpKey, key));
        dispatch(removeCountryCP(cpKey, key));

    }

    addhaveTINNo(cpKey) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        const tinNoOthersId = taxSelfDeclarationsReducer[cpKey][`tinNoOthersId`];
        const maxKey = Math.max.apply(null, tinNoOthersId);
        dispatch(incrementCountryCP(cpKey, maxKey));
        dispatch(addNewCountryCP(cpKey, maxKey));

    }

    handleRadioSelection(data, cpKey, key, field) {
        const { dispatch } = this.props;
        dispatch(selectRadioItemCP(data, cpKey, key, field));
    }

    handleOnTextChangeOthers(data, cpKey, key, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChangeOthersCP(data, cpKey, key, field));
    }

    handleDropdownFocusChangeCP(cpkey, key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChangeCP(true, cpkey, key, field));
    }

    handleDropdownBlurChangeCP(cpKey, key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChangeCP(false, cpKey, key, field));
    }

    handleOnSearchChangeOthersCP(e, cpKey, key, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValueOtherCP(value, cpKey, key, field));
    }

    handleDropdownClickChangeCP(data, cpKey, key, field) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        if (taxSelfDeclarationsReducer[cpKey][key][field].value === data.value) {
            return;
        }

        if (field.includes("othersCountry")) {
            dispatch(clearErrorMsgCountryCP(cpKey));
        }

        dispatch(selectDropdownItemChangeCP(data.value, data.description, cpKey, key, field));
    }

    handleOnButtonChangeOthers(data, cpKey, key, field) {
        const { dispatch } = this.props;
        dispatch(handleButtonChangeOthersCP(data, cpKey, key, field));
    }

    handleOnButtonChange(data, key, field) {
        const { dispatch } = this.props;
        dispatch(handleButtonChangeCP(data, key, field));
    }

    handleOnCheckbox(key, field, isToggled) {
        const { dispatch } = this.props;
        dispatch(handleIsCheckedCP(key, field, isToggled));
    }

    //Delete
    deleteCP(key) {
        const { dispatch } = this.props;
        dispatch(decreaseCP(key));
        dispatch(removeCP(key));

    }

    addCP() {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        const controllingPersonId = taxSelfDeclarationsReducer.controllingPersonId;
        const maxKey = Math.max.apply(null, controllingPersonId);
        dispatch(incrementCP(maxKey));
        dispatch(addNewCP(maxKey));

    }

    handleOnSearchChangeOthers(e, key, field) {
        const { dispatch } = this.props;
        const value = e.target.value;
        dispatch(changeSearchInputValueOther(value, key, field));
    }

    handleDropdownClickChange(data, key, field) {
        const { dispatch, taxSelfDeclarationsReducer } = this.props;
        if (taxSelfDeclarationsReducer[key][field].value === data.value) {
            return;
        }

        dispatch(selectDropdownItemChange(data.value, data.description, key, field));
    }

    handleDropdownFocusChange(key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChange(true, key, field));
    }

    handleDropdownBlurChange(key, field) {
        const { dispatch } = this.props;
        dispatch(setDropdownFocusStatusChange(false, key, field));
    }

    handleOnChange(data, key, field) {
        const { dispatch } = this.props;
        dispatch(handleTextInputChangeOthers(data, key, field));
        if (field === 'residentialPostalCode') {
            dispatch(autoGetLocalAddress(data.value, key));
        }
        if (field.includes("cpIdNo")) {
            dispatch(clearErrorMsgCPId());
        }
    }

    handleTextInputOnFocus(key, data, country, field, status, isTIN = false) {
        const { dispatch } = this.props;
        dispatch(handleTextInputWithKeyFocus(key, field, status));
        if (isTIN) {
            dispatch(handleTextInputBlurOthers(data, key, country, field));
        }
    }

    renderOthers(cpKey) {
        const { taxSelfDeclarationsReducer, commonReducer } = this.props;

        const tinNoOthersId = taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthersId`];

        const excludes = ["SG", "US"];
        //haveTINNo mapping
        const maxKey = Math.max.apply(null, tinNoOthersId);
        const length = tinNoOthersId.length;
        const inputValues = commonReducer.appData.inputValues
            ? commonReducer.appData.inputValues
            : "";
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        const countriesNamesMap = inputValues.countriesNamesMap;
        const reasonsList = inputValues.reasons;

        const tmpArray = [];
        const errorMsgList = commonReducer.appData.errorMsgs;
        const errorMsgListOthers = commonReducer.appData.errorMsgsOthers;
        const maxCountries = parseInt(taxSelfDeclarationsValue.maxCountries, 10);

        tinNoOthersId.map((key, i) => {
            const bottomBorder = taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "Y" ? "" : "bottom-border";
            tmpArray.push(
                <div key={key} >
                    {length > 1 &&
                        <div className="crs-others-container">
                            <h1 className="sub-title partner-subtitle">{i + 1}</h1>                                                       <img src={deleteIcon} className="partner-delete-icon" alt="delete" onClick={() => this.deleteCountry(`controllingPerson${cpKey}`, key)} />

                        </div>
                    }
                    <div className="crs-container">
                        <div className={`top-border ${bottomBorder}`}>
                            <div className="fullTable confirmDetails-flexContainer">
                                <div className="halfRow right-border positionRelative dropdown-nopadding">
                                    <Dropdown
                                        flagImg
                                        inputID={`othersCountry${cpKey}${key}`}
                                        label={labels.othersCountry}
                                        value={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.value}
                                        errorMsg={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.errorMsg}
                                        isValid={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.isValid}
                                        isFocus={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.isFocus}
                                        dropdownItems={mapValueToDescription(
                                            countriesNamesMap
                                        )}
                                        searchValue={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.searchValue}
                                        onBlur={this.handleDropdownBlurChangeCP.bind(this, `controllingPerson${cpKey}`, `tinNoOthers${key}`, 'othersCountry')}
                                        onFocus={this.handleDropdownFocusChangeCP.bind(this, `controllingPerson${cpKey}`, `tinNoOthers${key}`, 'othersCountry')}
                                        onClick={data => this.handleDropdownClickChangeCP(data, `controllingPerson${cpKey}`, `tinNoOthers${key}`, 'othersCountry')}
                                        onSearchChange={event =>
                                            this.handleOnSearchChangeOthersCP(event, `controllingPerson${cpKey}`, `tinNoOthers${key}`, 'othersCountry')
                                        }
                                        validator={["required"]}
                                        excludes={excludes}
                                    />
                                </div>
                                <div className="halfRow positionRelative">
                                    <div className="crs-buttons p-t-5">
                                        <div className="txt-container">
                                            <span className="crs-txt">{labels.haveTINNo}</span>
                                            {!taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.isValid &&
                                                <span className="errorMsg">{labels.errorMsgRequired}</span>}
                                        </div>
                                        <div className="flex-1">
                                            <input type="radio" id={`othersYes${cpKey}${key}`} checked={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "Y" ? true : false} name={`radioButtonOthers${cpKey}${key}`} value="Y" onChange={(e) => this.handleOnButtonChangeOthers(e.target.value, `controllingPerson${cpKey}`, `tinNoOthers${key}`, "haveTINNo")} />
                                            <label className="lbl-first" htmlFor={`othersYes${cpKey}${key}`}>{labels.yesButton}</label>

                                            <input type="radio" id={`othersNo${cpKey}${key}`} name={`radioButtonOthers${cpKey}${key}`} value="N" checked={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "N" ? true : false} onChange={(e) => this.handleOnButtonChangeOthers(e.target.value, `controllingPerson${cpKey}`, `tinNoOthers${key}`, "haveTINNo")} />
                                            <label className="lbl-second" htmlFor={`othersNo${cpKey}${key}`}>{labels.noButton}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "Y" &&

                            <div className="sub-div-common bottom-border">
                                <div className="p-l-10 positionRelative">
                                    <TextInput
                                        inputID={`othersTINNo${cpKey}${key}`}
                                        label={labels.othersTINNo}
                                        value={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersTINNo.value}
                                        errorMsg={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersTINNo.errorMsg}
                                        isValid={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersTINNo.isValid}
                                        onChange={(data) => this.handleOnTextChangeOthers(data, `controllingPerson${cpKey}`, `tinNoOthers${key}`, 'othersTINNo')}
                                        onBlur={(data) => this.handleOnBlurOthers(data, `controllingPerson${cpKey}`, `tinNoOthers${key}`, taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.value, 'othersTINNo')}
                                        errorMsgList={errorMsgList}
                                        validator={["required", "isAlphanumeric", "maxSize|19"]}
                                        hasIcon={false}
                                    />
                                </div>
                            </div>
                        }

                        {taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].haveTINNo.value === "N" &&
                            <div>
                                <div className="crs-txt-container">
                                    <span className="crs-txt crs-questions">{labels.reason}</span>
                                </div>
                                <RadioButton
                                    inputID={`reasons${cpKey}${key}`}
                                    value={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].reasons.value}
                                    radioObj={mapValueToDescription(
                                        reasonsList
                                    )}
                                    isDisabled={false}
                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].reasons.errorMsg}
                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].reasons.isValid}
                                    onClick={(data) => this.handleRadioSelection(data, `controllingPerson${cpKey}`, `tinNoOthers${key}`, "reasons")}
                                />
                                {taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].reasons.value === "3" &&
                                    <div className="p-l-65">
                                        <div className="others-container">
                                            <div className="others-label p-r-0">{labels.othersNone}</div>
                                            <div className="bottom-border positionRelative width-others">
                                                <TextInput
                                                    inputID={`othersOthers${cpKey}${key}`}
                                                    maxCharacters={70}
                                                    label={labels.othersNone}
                                                    value={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersOthers.value}
                                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersOthers.errorMsg}
                                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersOthers.isValid}
                                                    onChange={(data) => this.handleOnTextChangeOthers(data, `controllingPerson${cpKey}`, `tinNoOthers${key}`, 'othersOthers')}
                                                    // onBlur={(data) => this.handleOnBlurOthers(data, `controllingPerson${cpKey}`, `tinNoOthers${key}`, taxSelfDeclarationsReducer[`controllingPerson${cpKey}`][`tinNoOthers${key}`].othersCountry.value, 'othersOthers')}
                                                    errorMsgList={errorMsgListOthers}
                                                    validator={["required", "maxSize|70"]}
                                                    hasIcon={false}
                                                    type="Others"

                                                />
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        }
                        {maxKey === key && tinNoOthersId.length < maxCountries &&
                            <SecondaryButton
                                label={labels.addCountry}
                                onClick={() => this.addhaveTINNo(`controllingPerson${cpKey}`)}
                            />
                        }
                    </div>
                </div>
            );
            return tmpArray;
        })
        return tmpArray;

    }


    renderControllingPerson() {
        const { taxSelfDeclarationsReducer, commonReducer } = this.props;
        const { controllingPersonId, itemsFiltered } = taxSelfDeclarationsReducer;

        //controllingPerson mapping
        const maxKey = Math.max.apply(null, controllingPersonId);
        const length = controllingPersonId.length;
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        // const inputValues = commonReducer.appData.inputValues
        //     ? commonReducer.appData.inputValues
        //     : "";
        // const countriesNamesMap = inputValues.countriesNamesMap;
        const tmpArray = [];
        const errorMsgList = commonReducer.appData.errorMsgs;
        const maxControllingPerson = parseInt(taxSelfDeclarationsValue.maxControllingPerson, 10);

        controllingPersonId.map((key, i) => {
            const applicantNumber = i + 1;
            tmpArray.push(
                <div key={key} style={{ paddingTop: "30px" }}>
                    <div className="partner-subtitle-container" id={`cp${key}`}>
                        <div>
                            <h1 className="sub-title partner-subtitle">{applicantNumber}</h1>
                            <h1 className="sub-title cp-title">{labels.cpBasicDetails}</h1>
                        </div>
                        {length > 1 &&
                            <div className="uob-secondary-button-container">
                                <img src={deleteIcon} className="partner-delete-icon" alt="delete" onClick={() => this.deleteCP(key)} />
                            </div>
                        }
                    </div>

                    <div className="top-border">
                        <div className="fullTable confirmDetails-flexContainer">
                            <div className="halfRow right-border positionRelative">
                                <SearchBar
                                    {...this.props}
                                    items={itemsFiltered}
                                    label={labels.cpName}
                                    value={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.value}
                                    isFocus={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.isFocus}
                                    isLoading={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.isLoading}
                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.isValid}
                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.errorMsg}
                                    errorMsgList={errorMsgList}
                                    onChange={(data) => { this.handleOnChangeSearchValue(data, `controllingPerson${key}`) }}
                                    onClick={(item) => { this.handleChooseSearchItem(`controllingPerson${key}`, item) }}
                                    onFocus={() => { this.handleSearchOnFocus(`controllingPerson${key}`, true) }}
                                    onBlur={() => { this.handleSearchOnFocus(`controllingPerson${key}`, false) }}
                                    validator={['required', 'isFullName', 'maxSize|70']}
                                />

                            </div>
                            <div className="halfRow positionRelative">
                                <TextInput
                                    inputID={`cpIdNo${key}`}
                                    label={labels.cpIdNo}
                                    isUpperCase
                                    value={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.value}
                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.errorMsg}
                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.isValid}
                                    onChange={(data) => this.handleOnChange(data, `controllingPerson${key}`, 'cpIdNo')}
                                    errorMsgList={errorMsgList}
                                    validator={["required", "isNRIC"]}
                                    hasIcon={false}
                                    isFocus={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.isFocus}
                                    onFocus={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "cpIdNo", true)}
                                    onBlur={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "cpIdNo", false)}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="top-border">
                        <div className="fullTable confirmDetails-flexContainer">
                            <div className="halfRow right-border positionRelative">
                                <TextInput
                                    type="Phone"
                                    inputID={`cpMobileNo${key}`}
                                    label={labels.cpMobileNo}
                                    value={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo.value}
                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo.errorMsg}
                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo.isValid}
                                    onChange={(data) => this.handleOnChange(data, `controllingPerson${key}`, 'cpMobileNo')}
                                    validator={["required", "isPhoneNumber"]}
                                    errorMsgList={errorMsgList}
                                    hasIcon={false}
                                    isReadOnly={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo.isReadOnly}
                                    isFocus={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo.isFocus}
                                    onlySg={false}
                                    onFocus={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "cpMobileNo", true)}
                                    onBlur={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "cpMobileNo", false)}
                                />
                            </div>
                            <div className="halfRow positionRelative">
                                <TextInput
                                    inputID={`cpEmail${key}`}
                                    label={labels.cpEmail}
                                    value={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpEmail.value}
                                    errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpEmail.errorMsg}
                                    isValid={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpEmail.isValid}
                                    onChange={(data) => this.handleOnChange(data, `controllingPerson${key}`, 'cpEmail')}
                                    errorMsgList={errorMsgList}
                                    validator={["required", "isEmail", "maxSize|30"]}
                                    hasIcon={false}
                                    isFocus={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpEmail.isFocus}
                                    onFocus={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "cpEmail", true)}
                                    onBlur={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "cpEmail", false)}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="sub-div-common bottom-border positionRelative">
                        <div className="p-l-10">
                            <TextInput
                                isNumber
                                inputID={`cpPercentageOwnership${key}`}
                                label={labels.cpPercentageOwnership}
                                value={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership.value}
                                errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership.errorMsg}
                                isValid={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership.isValid}
                                onChange={(data) => this.handleOnChange(data, `controllingPerson${key}`, 'cpPercentageOwnership')}
                                errorMsgList={errorMsgList}
                                maxCharacters={3}
                                validator={["required", "range|0|100"]}
                                hasIcon={false}
                                isFocus={taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership.isFocus}
                                onFocus={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "cpPercentageOwnership", true)}
                                onBlur={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "cpPercentageOwnership", false)}
                            />
                        </div>
                    </div>
                    <div className="partner-subtitle-container">
                        <div>
                            <h1 className="sub-title">{labels.cpTaxResidency}</h1>
                            <h1 className="sub-headline">{labels.cpTaxResidencyDesc}</h1>
                        </div>
                    </div>
                    <div id={`taxResidency${key}`}>
                        {!taxSelfDeclarationsReducer[`controllingPerson${key}`].taxResidencyIsValid
                            &&
                            <div className="errorMsg">{labels.errorMsgRequiredOne}</div>
                        }
                        <Checkbox
                            inputID={`checkboxSG${key}`}
                            description={labels.checkboxSG}
                            isChecked={taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxSG.isToggled}
                            onClick={() => this.handleOnCheckbox(`controllingPerson${key}`, 'checkboxSG', taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxSG.isToggled)}
                        />
                        {taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxSG.isToggled &&
                            <div className="crs-container">
                                <div className="sub-div-common bottom-border">
                                    <div className="p-l-10 positionRelative">
                                        <TextInput
                                            inputID={`tinNoSG${key}`}
                                            label={labels.tinNoSG}
                                            value={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoSG.value}
                                            errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoSG.errorMsg}
                                            onChange={data => this.handleOnChange(data, `controllingPerson${key}`, "tinNoSG")}
                                            isValid={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoSG.isValid}
                                            validator={["required", "isAlphanumeric", "maxSize|10"]}
                                            isFocus={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoSG.isFocus}
                                            onFocus={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "tinNoSG", true)}
                                            onBlur={(data) => this.handleTextInputOnFocus(`controllingPerson${key}`, data, "SG", "tinNoSG", false, true)}
                                            errorMsgList={errorMsgList}
                                            hasIcon={false}
                                            maxCharacters={10}
                                        />
                                    </div>
                                </div>
                            </div>
                        }
                        <Checkbox
                            inputID={`checkboxUS${key}`}
                            description={labels.checkboxUS}
                            isChecked={taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxUS.isToggled}
                            onClick={() => this.handleOnCheckbox(`controllingPerson${key}`, 'checkboxUS', taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxUS.isToggled)}
                        />
                        {taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxUS.isToggled &&
                            <div className="crs-container">
                                <div className="sub-div-common bottom-border">
                                    <div className="p-l-10 positionRelative">
                                        <TextInput
                                            inputID="tinNoUS"
                                            label={labels.tinNoUS}
                                            value={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoUS.value}
                                            errorMsg={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoUS.errorMsg}
                                            onChange={data => this.handleOnChange(data, `controllingPerson${key}`, "tinNoUS")}
                                            isValid={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoUS.isValid}
                                            isFocus={taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoUS.isFocus}
                                            onFocus={() => this.handleTextInputOnFocus(`controllingPerson${key}`, "tinNoUS", true)}
                                            onBlur={(data) => this.handleTextInputOnFocus(`controllingPerson${key}`, data, "US", "tinNoUS", false, true)}
                                            validator={["required", "isNumber", "exactSize|9"]}
                                            errorMsgList={errorMsgList}
                                            hasIcon={false}
                                            maxCharacters={9}
                                        />
                                    </div>
                                </div>
                            </div>
                        }
                        <Checkbox
                            inputID={`checkboxOthers${key}`}
                            description={labels.checkboxOthers}
                            isChecked={taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxOthers.isToggled}
                            onClick={() => this.handleOnCheckbox(`controllingPerson${key}`, 'checkboxOthers', taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxOthers.isToggled)}
                        />
                        {taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxOthers.isToggled &&
                            <div>
                                {this.renderOthers(key)}
                            </div>


                        }
                    </div>
                    {maxKey === key && controllingPersonId.length < maxControllingPerson &&
                        <div style={{ paddingTop: "20px" }} >
                            <SecondaryButton
                                label={labels.addCP}
                                onClick={() => this.addCP(key)}
                            />
                        </div>
                    }
                </div>
            );
            return tmpArray;
        })
        return tmpArray;
    }

    render() {
        const { commonReducer, taxSelfDeclarationsReducer } = this.props;
        const { fatcaCRSStatus } = taxSelfDeclarationsReducer;

        //Lists
        const taxSelfDeclarationsValue = commonReducer.appData.taxSelfDeclarations;
        const labels = taxSelfDeclarationsValue.labels;
        const paddingTop = fatcaCRSStatus.isValid ? "60px" : "0px";
        return (
            <div style={{ paddingTop: paddingTop }} id="fatca-section">
                {!fatcaCRSStatus.isValid &&
                    <div style={{ paddingTop: "60px" }}
                        className="radio-errorMsg">
                        {labels.errorMsgFatca}
                    </div>
                }
                <h1 className="desc" style={{ marginTop: "0px" }}>{taxSelfDeclarationsValue.fatcaTitle}</h1>
                <p className="sub-headline">
                    {taxSelfDeclarationsValue.fatcaHeadline}
                </p>
                <TableViewFATCA
                    colTitles={labels.fatcaColTitle}
                    labels={labels}
                    inputID="fatcaCRSStatus"
                />
                {fatcaCRSStatus.value === "PB" &&
                    <div id="controllingPerson-section">
                        <h1 className="desc">
                            {taxSelfDeclarationsValue.cpTitle}
                        </h1>
                        <p className="sub-headline" dangerouslySetInnerHTML={{ __html: taxSelfDeclarationsValue.cpHeadline }} />
                        <p className="sub-headline-bold">{taxSelfDeclarationsValue.cpSubHeadline}</p>
                        {this.renderControllingPerson()}
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { commonReducer, taxSelfDeclarationsReducer } = state;
    return { commonReducer, taxSelfDeclarationsReducer };
};

export default connect(mapStateToProps)(FATCA);
