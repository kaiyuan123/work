import React, { Component } from "react";
import TableViewCompany from "../components/TableView/TableViewCompany/TableViewCompany";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import RadioButton from "./../components/RadioButton/RadioButton";
import { mapValueToDescription } from "./../common/utils";
import { selectRadioItem, prefilledapplicantResolution, prefilledSignatorySelected } from "./../actions/asrDetailsAction";
import { connect } from "react-redux";
import SaveAndExit from "./SaveAndExit";

class ASRDetailsPage extends Component {

	componentWillMount() {
		const { dispatch, contactDetailsReducer, operatingMandateReducer } = this.props;
		const { entityType, businessConstitution } = contactDetailsReducer;
		const { signatorySelected } = operatingMandateReducer;
		dispatch(prefilledapplicantResolution(entityType, businessConstitution));
		dispatch(prefilledSignatorySelected(signatorySelected));
	}

	isASRDetailsPassing() {
		this.props.onCheck();
	}

	handleSaveAndExit() {
		this.props.onSaveAndExit();
	}

	handleToReviewPage() {
		this.props.onContinue();
	}

	handleOnRadioButton(data, field) {
		const { dispatch } = this.props;
		dispatch(selectRadioItem(data, field));
	}

	selectRadioItem(data, field) {
		const { dispatch } = this.props;
		dispatch(selectRadioItem(data, field));
	}

	render() {
		const { commonReducer, asrDetailsReducer, operatingMandateReducer } = this.props;
		const { applicantResolution,
			particularsOfApprovedPersons,
			applicantName
		} = asrDetailsReducer;
		const { signatorySelected } = operatingMandateReducer;
		let { signingApprovedPerson } = asrDetailsReducer;
		signingApprovedPerson = signatorySelected;
		const asrDetailsValue = commonReducer.appData.asrDetails;
		const labels = asrDetailsValue.labels;
		const inputValues = commonReducer.appData.inputValues ? commonReducer.appData.inputValues : "";
		const applicantResolutionList = inputValues.applicantResolutionList;
		const signingApprovedPersonList = inputValues.signingApprovedPersonList;

		return (
			<div className="uob-content asr-details" id="asrDetails-section">
				<div className="save-button-container">
					<h1>{asrDetailsValue.title}</h1>
					<SaveAndExit {...this.props} onClick={() => this.handleSaveAndExit()} />
				</div>
				<p className="uob-headline">
					{asrDetailsValue.headline}
				</p>
				<div className="medium-txt">
					{asrDetailsValue.certifyP1}

				</div>
				<div className="radiobutton-container radio-type2">
					<RadioButton
						inputID="applicantResolution"
						value={applicantResolution.value}
						radioObj={mapValueToDescription(
							applicantResolutionList
						)}
						isDisabled={true}
						errorMsg={applicantResolution.errorMsg}
						isValid={applicantResolution.isValid}
						onClick={(data) => this.handleOnRadioButton(data, "applicantResolution")}
					/>
				</div>
				<h1 className="asr-heading">{asrDetailsValue.resolved}</h1>

				<div className="agreementContent">
					<u>{asrDetailsValue.appointment}</u>
					<ol>
						<li>
							{asrDetailsValue.bank}
							&nbsp;<span className="dash">{applicantName.value}</span>&nbsp;
							{asrDetailsValue.applicant}
						</li>
					</ol>
					<div dangerouslySetInnerHTML={{ __html: asrDetailsValue.acknowledgementText }} />
				</div>
				<div className="legend-box">
					<u>{labels.legend}</u><br />
					<div>
						<div>
							<span className="legent-char">{labels.hash}</span><span className="legent-txt">{labels.hastTxt}</span>
						</div>
						<div>
							<span className="legent-char">{labels.plus}</span><span className="legent-txt">{labels.plusTxt}</span>
						</div>
						<div>
							<span className="legent-char">{labels.star}</span><span className="legent-txt">{labels.starTxt}</span>
						</div>
					</div>
				</div>
				<h1 className="sub-title m-t-0 asr-heading">{labels.schedule}</h1>
				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow positionRelative mob-border">
							<div className="read-only-text-input--container">
								<div>
									<label className="read-only-text-input--label--focused">{labels.appName}</label>
									<div className="read-only-text-input--input--focused">{applicantName.value}</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer radio-head">
						<div className="halfRow positionRelative mob-border">
							<div className="read-only-text-input--container">
								<div>
									<label className="read-only-text-input--label--focused">{labels.signCond}</label>
									<div className="radiobutton-container" id="signingApprovedPerson">
										<RadioButton
											inputID="signingApprovedPersonList"
											value={signingApprovedPerson.value}
											radioObj={mapValueToDescription(
												signingApprovedPersonList
											)}
											isDisabled={true}
											errorMsg={signingApprovedPerson.errorMsg}
											isValid={signingApprovedPerson.isValid}
											onClick={(data) => this.handleOnRadioButton(data, "signingApprovedPerson")}
										/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<h1 className="sub-title">{asrDetailsValue.particularsApp}</h1>

				<div className="uob-input-separator enableOverflowX">
					<TableViewCompany
						tableType={'particularsOfPersons'}
						colTitles={labels.particularsOfPersonsColTitle}
						tableContent={particularsOfApprovedPersons.value}
						mapItems={inputValues}
					/>
				</div>
				<h1 className="sub-title ">{asrDetailsValue.particularsCert}</h1>
				<div className="uob-input-separator enableOverflowX">
					<TableViewCompany
						tableType={'particularsOfPersons'}
						colTitles={labels.particularsOfPersonsColTitle}
						tableContent={particularsOfApprovedPersons.value}
						mapItems={inputValues}
					/>
				</div>
				<div className="legend-box m-bot0">
					{asrDetailsValue.particularsSub}
				</div>

				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToReviewPage()} isLoading={commonReducer.isProcessing} />
				</div>
				
				{
					!this.props.onFixedButton() &&
					<div>
						<div className="uob-input-separator fixedContinue fixedContinueGray">
							<PrimaryButton label={labels.continueButton} onClick={() => this.isASRDetailsPassing()} isLoading={commonReducer.isProcessing} />
						</div>
					</div>
				}

			</div>
		)
	}
}

const mapStateToProps = state => {
	const { commonReducer, asrDetailsReducer, operatingMandateReducer } = state;
	return { commonReducer, asrDetailsReducer, operatingMandateReducer };
};

export default connect(mapStateToProps)(ASRDetailsPage);
