import React, { Component } from "react";
import TextInput from "./../components/TextInput/TextInput";
import RadioButton from "./../components/RadioButton/RadioButton";
import { mapValueToDescription } from "./../common/utils";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import Checkbox from "./../components/Checkbox/Checkbox";
import { connect } from "react-redux";
import { handleIsChecked, handleIsSignatoryChecked, handleTextInputChange } from "./../actions/operatingMandateAction";
import SaveAndExit from "./SaveAndExit";

class OperatingMandatePage extends Component {
	handleSaveAndExit() {
		this.props.onSaveAndExit();
	}

	handleToReviewPage() {
		this.props.onHandleToReview();
	}

	handleToTaxSelfDeclarationsPage() {
		this.props.onContinue();
	}

	isOperatingMandatePassing() {
		this.props.onCheck()
	}

	handleOnChange(data, key, field) {
		const { dispatch } = this.props;
		dispatch(handleTextInputChange(data, key, field));
	}

	handleOnRadioButton(data, field) {
		const { dispatch } = this.props;
		dispatch(handleIsSignatoryChecked(data, field));
	}

	handleOnCheckbox(key, field, isToggled, isValid) {
		const { dispatch } = this.props;
		dispatch(handleIsChecked(key, field, isToggled, isValid));
	}

	renderApprovedSignatoryList() {
		const { operatingMandateReducer, commonReducer, accountSetupReducer } = this.props;
		const isBibPlusUserToggled = accountSetupReducer.setupBIBPlus.isToggled;
		const { signatoriesID, mainApplicantLegalId } = operatingMandateReducer;
		// const { isRetrieveApplication } = applicationReducer;
		// const length = signatoriesID.length;
		//let isRetrieveApplication = true;
		const operatingMandateValue = commonReducer.appData.operatingMandate;
		const subHeading = operatingMandateValue.subHeading;
		const sublabels = subHeading.labels;
		const tmpArray = [];
		const errorMsgList = commonReducer.appData.errorMsgs;
		const borderClass = !isBibPlusUserToggled ? 'bottom-border' : '';
		signatoriesID.map((key, i) => {

			let checkifMainApplicant = mainApplicantLegalId === operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value;
			const className = !checkifMainApplicant ? 'positionRelative' : '';
			const applicantNumber = i + 1;

			tmpArray.push(
				<div key={key} className={`user${key}`}>
					<div className="sub-title-confirm" id={`${key}`}>{sublabels.user.replace("{id}", applicantNumber)}</div>
					<div className="top-border">
						<div className="fullTable confirmDetails-flexContainer">
							<div className="halfRow right-border positionRelative">
								<TextInput
									inputID={`signatoriesName${key}`}
									label={sublabels.name}
									value={operatingMandateReducer[`signatories${key}`].signatoriesName.value}
									errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesName.errorMsg}
									isValid={operatingMandateReducer[`signatories${key}`].signatoriesName.isValid}
									onChange={(data) => this.handleOnChange(data, `signatories${key}`, 'signatoriesName')}
									validator={["isFullName", "maxSize|70"]}
									errorMsgList={errorMsgList}
									isMyInfo={operatingMandateReducer[`signatories${key}`].signatoriesName.isMyInfo}
									hasIcon={false}
									isReadOnly={operatingMandateReducer[`signatories${key}`].signatoriesName.isReadOnly}
								/>
							</div>
							<div className="halfRow positionRelative">
								<TextInput
									isUpperCase
									inputID={`signatoriesNRIC${key}`}
									label={sublabels.nric}
									value={operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value}
									errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesNRIC.errorMsg}
									isValid={operatingMandateReducer[`signatories${key}`].signatoriesNRIC.isValid}
									onChange={(data) => this.handleOnChange(data, `signatories${key}`, 'signatoriesNRIC')}
									errorMsgList={errorMsgList}
									validator={["isNRIC"]}
									isMyInfo={operatingMandateReducer[`signatories${key}`].signatoriesNRIC.isMyInfo}
									hasIcon={false}
									isReadOnly={operatingMandateReducer[`signatories${key}`].signatoriesNRIC.isReadOnly}
								/>
							</div>
						</div>
					</div>
					<div className="top-border">
						<div className="fullTable confirmDetails-flexContainer">
							<div className={`halfRow right-border ${className} ${borderClass}`}>
								<TextInput
									type="Phone"
									inputID={`signatoriesMobileNo${key}`}
									label={sublabels.mobile}
									value={operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.value}
									errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.errorMsg}
									isValid={operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.isValid}
									onChange={(data) => this.handleOnChange(data, `signatories${key}`, 'signatoriesMobileNo')}
									errorMsgList={errorMsgList}
									validator={["isPhoneNumber"]}
									isMyInfo={operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.isMyInfo}
									hasIcon={!checkifMainApplicant ? true : false}
									isReadOnlyMobile={checkifMainApplicant ? true : operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.isReadOnly}
									onlySg={false}
									hasEdit={operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.hasEdit}
								/>
							</div>
							<div className={`halfRow positionRelative ${borderClass}`}>
								<TextInput
									inputID={`signatoriesEmail${key}`}
									label={sublabels.email}
									value={operatingMandateReducer[`signatories${key}`].signatoriesEmail.value}
									errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesEmail.errorMsg}
									isValid={operatingMandateReducer[`signatories${key}`].signatoriesEmail.isValid}
									onChange={(data) => this.handleOnChange(data, `signatories${key}`, 'signatoriesEmail')}
									errorMsgList={errorMsgList}
									validator={["isEmail", "maxSize|30"]}
									isMyInfo={operatingMandateReducer[`signatories${key}`].signatoriesEmail.isMyInfo}
									hasIcon={!checkifMainApplicant ? true : false}
									isReadOnly={checkifMainApplicant ? true : operatingMandateReducer[`signatories${key}`].signatoriesEmail.isReadOnly}
									hasEdit={operatingMandateReducer[`signatories${key}`].signatoriesEmail.hasEdit}
								/>
							</div>
						</div>
					</div>
					{checkifMainApplicant && isBibPlusUserToggled ? <div className="top-border bibuser">
						<Checkbox
							description={sublabels.setupBIBUser}
							isChecked={operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled}
							onClick={() => this.handleOnCheckbox(`signatories${key}`, 'signatoriesToggleBIBUser', operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled, `signatoriesToggleBIBUser.isValid`, key)}
						/>

						{operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled && <div className="bibuser-container">
							<div className="sub-div-common flexCenterFullWidth positionRelative bottom-border">
								<div className="p-l-10 fullWidth">
									<TextInput
										inputID={'bibUserId'}
										label={sublabels.bibUserId}
										isReadOnly={false}
										value={operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.value}
										errorMsg={operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.errorMsg}
										onChange={data => this.handleOnChange(data, `signatories${key}`, "signatoriesBibUserId")}
										isValid={operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.isValid}
										validator={["isBIBUserId", "maxSize|20"]}
										errorMsgList={errorMsgList}
										isMyInfo={operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.isMyInfo}
										hasIcon={true}
									/>
								</div>
							</div>
							<div className="companyGroupIDInfo">{sublabels.helpText}</div>
						</div>}
					</div> : <div>
							{isBibPlusUserToggled ? <div className="top-border bottom-border bibuser">
								<Checkbox
									description={sublabels.setupBIBUser}
									isChecked={operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled}
									onClick={() => this.handleOnCheckbox(`signatories${key}`, 'signatoriesToggleBIBUser', operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled, `signatoriesToggleBIBUser.isValid`)}
								/></div> : ''}
						</div>}
				</div>
			);
			return tmpArray;
		})
		return tmpArray;
	}

	render() {
		const { commonReducer, operatingMandateReducer, companyBasicDetailsReducer } = this.props;
		const { approvedSignatories, signatorySelected } = operatingMandateReducer;
		const lengthOfsignatories = approvedSignatories.value.length;
		const isNotSoleProp = !(companyBasicDetailsReducer.businessConstitutionType.value === "S" && companyBasicDetailsReducer.typeOfCompany.value === "BN");
		const inputValues = commonReducer.appData.inputValues
			? commonReducer.appData.inputValues
			: "";
		const signatoryList = lengthOfsignatories > 1  && isNotSoleProp ? inputValues.signatoryTypeList1 : inputValues.signatoryTypeList2;

		// const signatoriesArr = approvedSignatories.value;
		const operatingMandateValue = commonReducer.appData.operatingMandate;
		const subHeading = operatingMandateValue.subHeading;
		// const sublabels = subHeading.labels;
		// const errorMsgList = commonReducer.appData.errorMsgs;
		const labels = operatingMandateValue.labels;
		return (
			<div className="uob-content" id="operatingMandate-section">
				<div className="save-button-container">
					<h1>{operatingMandateValue.title}</h1>
					<SaveAndExit {...this.props} onClick={() => this.handleSaveAndExit()} />
				</div>
				<p className="uob-headline">
					{operatingMandateValue.headline}
				</p>
				<div className="sub-title" id="signatorySelected">{subHeading.title1}</div>
				<div className="medium-txt">{subHeading.subtitle1}</div>
				<div className="radiobutton-container">
					<RadioButton
						inputID="authorisationLimit"
						value={signatorySelected.value}
						radioObj={mapValueToDescription(
							signatoryList
						)}
						isDisabled={false}
						errorMsg={signatorySelected.errorMsg}
						isValid={signatorySelected.isValid}
						onClick={(data) => this.handleOnRadioButton(data, "signatorySelected")}
					/>
				</div>

				<div className="signatory-info">
					<div className="big-text">{subHeading.title2}</div>
					<div className="medium-txt">{subHeading.subtitle2}</div>
					<div>{this.renderApprovedSignatoryList()}</div>
				</div>


				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => { signatorySelected.value === "3" ? this.handleToReviewPage() : this.handleToTaxSelfDeclarationsPage() }} isLoading={commonReducer.isProcessing} />
				</div>

				{
					!this.props.onFixedButton() &&
					<div>
						<div className="uob-input-separator fixedContinue fixedContinueGray">
							<PrimaryButton label={labels.continueButton} onClick={() => this.isOperatingMandatePassing()} />
						</div>
					</div>
				}
			</div>

		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, operatingMandateReducer } = state;
	return { commonReducer, operatingMandateReducer };
};

export default connect(mapStateToProps)(OperatingMandatePage);
