/* global GLOBAL_CONFIG */
const uploadUrl = `${GLOBAL_CONFIG.API_PATH}/applications/{identifier}/upload`;
export const mapValueToLabel = (arrayOfItems, value) => {
	const item = arrayOfItems.find(x => x.value === value);
	return item ? item.description : value;
};

export const eliminateDuplicates = (arr) => {
	let i,
		len = arr.length,
		out = [],
		obj = {};

	for (i = 0; i < len; i++) {
		obj[arr[i]] = 0;
	}
	for (i in obj) {
		out.push(i);
	}
	return out;
}

export const imageExists = (url, callback) => {
	var img = new Image();
	img.onload = function () {
		callback(true);
	};
	img.onerror = function () {
		callback(false);
	};
	img.src = url;
};

export const mapValueToDescription = (obj) => {
	const objects = Object.keys(obj);
	const tmpArray = [];
	objects.map((key) => {
		tmpArray.push({
			value: key,
			description: obj[key]
		});
		return tmpArray;
	})
	return tmpArray
};

export const mapValueToDescriptionArrayObj = (obj) => {
	const objects = Object.keys(obj);
	const tmpArray = [];
	objects.map((key) => {
		tmpArray.push({
			key: obj[key].value,
			value: obj[key].value,
			description: obj[key].description
		});
		return tmpArray;
	})
	return tmpArray
};

export const mapValueToDescriptionRiskArrayObj = (obj) => {
	const objects = Object.keys(obj);
	const tmpArray = [];
	objects.map((key) => {
		tmpArray.push({
			key: obj[key].value,
			value: obj[key].value,
			description: obj[key].description,
			risk: obj[key].risk
		});
		return tmpArray;
	})
	return tmpArray
};

export const mapValueToDescriptionRisk = (obj) => {
	const objects = Object.keys(obj);
	const tmpArray = [];
	objects.map((key) => {
		tmpArray.push({
			key: obj[key].description,
			value: obj[key].description,
			description: obj[key].description,
			risk: obj[key].risk
		});
		return tmpArray;
	})
	return tmpArray
};

export const fileUploadHelper = (file, inputId, identifier, docType, { successDispatch, errorDispatch, progressDispatch }) => {
	const xhr = new window.XMLHttpRequest();
	const formData = new window.FormData();
	/*const url = uploadUrl.replace("{identifier}", identifier).replace("{docType}", docType);*/
	const url = uploadUrl.replace("{identifier}", identifier);
	if (inputId === "signature" || inputId === "drawSignature") {
		formData.append("file", file, "signature.png");
	} else {
		formData.append("file", file);
		formData.append("documentType", docType);
	}

	/*----- code for local testing progress bar start ------*/

	/*let i = 0;
	let timer;
	function increment() {
		if (i <= 100) {
			progressDispatch(Math.round(i));
		} else {
			clearInterval(timer);
		}
		i++;
	}
	timer = setInterval(function () {
		increment()
	}, 10);*/

	/*----- code for local testing progress bar end ------*/

	xhr.upload.addEventListener(
		"progress",
		e => {
			const percentage = e.loaded / e.total * 100;
			progressDispatch(Math.round(percentage));
		},
		false
	);
	xhr.onloadend = e =>
		e.target.status === 200 ? successDispatch(e.target.response) : errorDispatch(e.target.response);
	xhr.open("POST", `${url}`);
	xhr.send(formData);
};

export const bytesToSize = bytes => {
	var sizes = ["Bytes", "KB", "MB", "GB", "TB"];
	if (bytes === 0) {
		return "0 Byte";
	}
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);
	return Math.round(bytes / Math.pow(1024, i), 2) + " " + sizes[i];
};

export const groupByCode = (json, groupBy) => {
	return json.reduce((returnObj, obj) => {
		(returnObj[obj[groupBy]] = returnObj[obj[groupBy]] || []).push(obj);
		return returnObj;
	}, {});
};

export const getValueByCode = (code, list) => {
	const item = list.find(x => x.value === code);
	return item ? item.risk : null;
}

export const findTrueInList = (code, list) => {
	const item = list.find(x => x.value === code);
	return item ? true : false;
}

export const capitalize = (str) => {
	return str.toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' ');
}

export const getURLParameter = (searchString, name) => {
	const params = searchString.substring(0, 1) === "?" ? searchString : "?" + searchString;
	return decodeURIComponent((new RegExp('[?|&]' + name + '=([^&;]+?)(&|#|;|$)').exec(params) || [null, ''])[1].replace(/\+/g, '%20')) || '';
}

export const sendDataToSparkline = (dataElementJson, productCode = "", stepNo = "", isLanding = false, isFormStart = false, isInitiate = false, referenceNo = "", isSubmit = false, isBookAppointmentStart = false, pathname, isBookAppointmentSubmit = false ) => {
	if (window._satellite) {
		if (!window.dataElement) {
			window.dataElement = {};
		}

		const dataElement = window.dataElement || {};
		dataElement.product_id = productCode;
		dataElement.product_name = dataElementJson[productCode].productName;
		dataElement.product_category = dataElementJson[productCode].productCatergory;
		dataElement.user_type = dataElementJson.userType;
		if (stepNo !== 1 && stepNo !== "" && (isInitiate || isSubmit)) {
			dataElement.submission_id = referenceNo;
		}

		if (isLanding) {
			dataElement.event_name = dataElementJson.eventNameMyinfoStart;
		} else if (isFormStart) {
			dataElement.event_name = dataElementJson.eventNameFormStart;
		} else if (isSubmit) {
			if( !isBookAppointmentSubmit ) {
				dataElement.event_name = dataElementJson.eventNameFormSubmit;
				setTimeout(function () {
					window._satellite.track("form_submit")
				}, 3000);
			} else {
				dataElement.virtual_pagename = `${pathname}/appointment_submit`;
				window._satellite.track('virtual_page_view');
			}
		} else if (isBookAppointmentStart) {
			dataElement.virtual_pagename = `${pathname}/appointment_start`;
			window._satellite.track('virtual_page_view');
		} else {
			dataElement.event_name = dataElementJson.eventNameStep.replace("{step}", stepNo);
			if (stepNo === 9) {
				window.dtmCustomEventName = dataElement.event_name.replace(/[0-9]+$/, '');
				window._satellite.track(window.dtmCustomEventName);
			} else {
				window._satellite.track("form_complete_step");
			}
		}
	}
};
