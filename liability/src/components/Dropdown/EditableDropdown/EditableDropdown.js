import React from 'react';
import PropTypes from 'prop-types';
import ScrollLock from "react-scroll-lock-component";
import Moment from 'react-moment';
import 'babel-polyfill';
import './EditableDropdown.css';

import arrowDownImg from './../../../assets/images/icon-arrow-down.svg';
import arrowLightDownImg from './../../../assets/images/icon-arrow-down-light.svg';
import arrowDownErrorImg from './../../../assets/images/icon-arrow-down-error.svg';
// import crossImg from './../../../assets/images/cross-grey.svg';
// import searchImg from './../../../assets/images/search_icon.svg';

const calendarStrings = {
	sameDay: '[Today] DD/MM/YYYY [-] LT',
	nextDay: '[Tmr] DD/MM/YYYY [-] LT',
	lastWeek: 'ddd DD/MM/YYYY [-] LT',
	nextWeek: 'ddd DD/MM/YYYY [-] LT',
	sameElse: 'ddd DD/MM/YYYY [-] LT'
};
const uniqueId = () => {
	return 'id-' + Math.random().toString(36).substr(2, 16);
};

const EditableDropdown = props => {
	const {
		inputID,
		isFocus,
		isValid,
		isDisabled,
		errorMsg,
		label,
		value,
		searchValue,
		dropdownItems,
		onBlur,
		onFocus,
		onClick,
		onSearchChange,
		flagImg,
		excludes,
		errorClick,
		notAbleToSelect,
		focusOutItem,
		hideSearch
	} = props;

	const renderDropdownItems = myFunc => (item, i) => {
		
		let countryFlagPath = flagImg ? `./images/countriesFlags/${item.value.toLowerCase()}.svg` : null;
		return (
			<div
				key={i}
				onClick={
					myFunc
						? () => {
							myFunc(item);
							onBlur();
						}
						: () => {
							onBlur();
						}
				}
				className='dropdown-selection-item'
			>
				{flagImg ?
					<img className="dropdown-flagImg" onError={(e) => e.target.style.display = 'none'} src={countryFlagPath}
						alt='icon' /> : null}
				{inputID === "bookingTimeSlots" ? <Moment calendar={calendarStrings}>
					{item.description}
				</Moment> : item.description
				}

			</div>
		);
	};
	const renderItems = renderDropdownItems(onClick);
	const mapValueToLabel = (arrayOfItems, value) => {
		const item = arrayOfItems.find(x => x.value === value);
		return item ? item.description : value;
	};
	const focus = isFocus || value || !isValid ? "--focused" : "";
	const isError = isValid ? "" : "--error"
	const showErrorLabel = isValid ? "" : "dropdownLabel--error";
	const containerErrorStyle = isValid ? "" : "dropdown--content--error";
	const searchFocus = isFocus ? "--focused" : "";
	let bottomClass = '';
	
	if(searchFocus === '--focused'){
		document.querySelector('#mailingCountry img') ? document.querySelector('#mailingCountry img').style="" : '';
		//350: It is the height of the dropdown list which opens in one shot including the padding and all plus extra space as we have fixed element at the bottom.
		if(window.innerHeight - document.querySelector('#'+inputID).getBoundingClientRect().top < 350){
			bottomClass = 'drop-up';
		}else{
			bottomClass = 'drop-down';
		}
	}

	let newDropdownItems = dropdownItems;
	if (excludes && excludes.length > 0) {
		newDropdownItems = dropdownItems.filter(item => !excludes.includes(item.value));
	}
	let countryFlagPath = flagImg ? `./images/countriesFlags/${value.toLowerCase()}.svg` : null;
	let searchRef;

	const onDropListBlur = (e) => {
		focusOutItem && !focusInCurrentTarget(e) && onBlur();
	}

	const focusInCurrentTarget = ({ relatedTarget, currentTarget }) => {
		let target = relatedTarget;
		if (target === null) {
			target = document.activeElement;
		}
		let node = target.parentNode || target.parent;
		while (node !== null) {
			if (node === currentTarget) return true;
			node = node.parentNode;
		}
		return false;
	}

	//Reason for change if you seek explanation: I do not mean to change the codes regarding the filter, is because it is also affecting other dropdowns. For example, you can search Primary Industry Classification by it's industry code (user does not know this for it is for myInfo), you can try this by reverting to the previous version and typing out "20". (Should not be happening). Another *minor* issue is, for country wise, our mapping does not follow internationally, we follow MyInfo strictly. Sometmies MyInfo change accordingly in order to prevent duplicates. We cannot be be sure. Would have remained if not for the code.

	return (
		<div className='dropdown' tabIndex='1' id={inputID ? inputID : uniqueId()} onBlur={(e) => onDropListBlur(e)}>
			<div className={`dropdown--vertical-container${isError}`}>
				<div
					className={"dropdown-container" + focus}
					onClick={() => {
						if (isDisabled) {
							if (notAbleToSelect) {
								errorClick && errorClick()
							} else {
								return null;
							}
						}
						const searchInput = searchRef;
						if (!hideSearch && newDropdownItems.length > 5) {
							setTimeout(() => searchInput.focus(), 0);
						}
						return isFocus ? onBlur() : onFocus();
					}}
				>
					<div className={`dropdown--content ${containerErrorStyle}`}>

						<div className='dropdown--content-left'>
							<div className={"dropdown--inputLabel" + focus + " " + showErrorLabel}>
								{`${label}${isValid ? "" : ` ${errorMsg}`}`}
							</div>
							<div className={`dropdown-input-field`}>
								{!hideSearch && newDropdownItems.length > 5 &&
									<input
										onChange={onSearchChange ? onSearchChange : null}
										onFocus={onFocus ? onFocus : null}
										className={"dropdown--searchValue" + searchFocus}
										value={searchValue}
										placeholder='Search'
										ref={search => (searchRef = search)}
									/>
								}
								<div className={`hideValue${searchFocus}`}>
									{flagImg && value !== "" ?
										<img className={"dropdown-flagImg editableDropdown-flagImg"} id='editableDropdown-flagImg'
											onError={(e) => e.target.style.display = 'none' } src={countryFlagPath} alt='icon' /> : null
									}
									<div className={`dropdown--value-text${isError}`}>
										{inputID === "bookingTimeSlots" && mapValueToLabel(dropdownItems, value) !== "" ? <Moment calendar={calendarStrings}>
											{mapValueToLabel(dropdownItems, value)}
										</Moment> : mapValueToLabel(dropdownItems, value)
										}
									</div>
								</div>
							</div>
						</div>

						{notAbleToSelect ? <img
							className='dropdown--inputArrow'
							src={arrowLightDownImg}
							alt='dropdown-arrow'
						/> : (isValid ?
							<img
								className='dropdown--inputArrow'
								src={arrowDownImg}
								alt='dropdown-arrow'
							/> :
							<img
								className='dropdown--inputArrow'
								src={arrowDownErrorImg}
								alt='dropdown-arrow'
							/>)
						}
					</div>
				</div>

				<div className={`dropdown--search-and-selection-flex-container${searchFocus} ${bottomClass}`}>
					<ScrollLock>
						<div className={"dropdown-selection" + searchFocus}>
							{newDropdownItems
								.filter(item =>
									item.description.toLowerCase().startsWith(searchValue.toLowerCase()) ||
									item.description.toLowerCase().includes(" " + searchValue.toLowerCase())
								)
								.map(renderItems)}
							<div className='iphone-fix' />
						</div>
					</ScrollLock>
				</div>
			</div>
			<div className="bottomLine" />
			<div className="fullErrorContent" />
		</div>
	);
};

EditableDropdown.propTypes = {
	label: PropTypes.string.isRequired,
	dropdownItems: PropTypes.array.isRequired,
	isFocus: PropTypes.bool.isRequired,
	isValid: PropTypes.bool.isRequired,
	isDisabled: PropTypes.bool,
	onFocus: PropTypes.func.isRequired,
	onBlur: PropTypes.func.isRequired,
	value: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired,
	onSearchChange: PropTypes.func
};

EditableDropdown.defaultProps = {
	isFocus: false,
	isValid: true,
	dropdownItems: [],
	errorMsg: '',
	label: 'Label',
	value: '',
	searchValue: '',
	isDisabled: false,
	focusOutItem: true,
	notAbleToSelect: false
};

export default EditableDropdown;
