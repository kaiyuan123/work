import React from 'react';
import { AsYouType } from 'libphonenumber-js';
import { mapValueToLabel } from "../../common/utils";
// import edit_blue_icon from './../../assets/images/edit_blue_icon.svg';
import pencilIcon from "./../../assets/images/pencil-icon.svg";
import { scrollBackToSection } from "./../../actions/commonAction";

const Review = props => {
  const { takeFromReducer, commonReducer, dispatch } = props;
  const handleScrollBackTosection = (section) => {
    dispatch(scrollBackToSection(section));
  }
  const country = number => {
      const formatter = new AsYouType();
      formatter.input(number);
      return formatter.country;
  };
  const formatNumber = number => {
      const formatter = new AsYouType();
      return formatter.input(number);
  };
  const page = takeFromReducer.page;
  let reducer = props[`${page}Reducer`];
  let isNotSoleProp = !(props.companyBasicDetailsReducer.businessConstitutionType.value === "S" && props.companyBasicDetailsReducer.typeOfCompany.value === "BN");
  let labels = commonReducer.appData[page] ? commonReducer.appData[page].labels : "";
  if (page === "companyDetails" || page === "localAddressInput") {
    labels = commonReducer.appData.companyDetails.companyBasicDetails.labels;
    if (page === "companyDetails") {
      reducer = props[`companyBasicDetailsReducer`];
    }
  }
  if (page === "operatingMandate") {
    labels = commonReducer.appData[page].subHeading.labels;
  }
  const requiredFields = commonReducer.appData.confirmDetails.ConfirmReviewDetails[page];
  const inputValues = commonReducer.appData.inputValues
      ? commonReducer.appData.inputValues
      : "";
  let sectionIndex = 0;
  // let mid = false;
  let mailingChecked = !props.companyBasicDetailsReducer.mailingAddressCheckbox.isToggled && page === "localAddressInput" ? false : true;

  const listSection = (idForListLength, fieldArray, reducer, fieldTitle, type, halfColumn, hasControllingPersonTaxOther=false) => idForListLength.map((key) => {
    const objFromReducer = reducer[`${type}${key}`];
    if (!hasControllingPersonTaxOther && type === "controllingPerson" &&
    objFromReducer.hasOwnProperty('tinNoOthersId') && objFromReducer.tinNoOthersId.length > 0) {
      let hasControllingPersonTaxOther = true;
      return (
        <div key={key}>
          <h1 className="sub-title-confirm"> {key + 1} </h1>
          { listField(fieldArray, objFromReducer, fieldTitle, halfColumn) }
          { listSection(objFromReducer.tinNoOthersId, fieldArray.slice(7,), objFromReducer, fieldTitle.slice(7,), "tinNoOthers", halfColumn.slice(7,), hasControllingPersonTaxOther) }
        </div>
      )
    }
    return (
      <div key={key}>
        { !hasControllingPersonTaxOther ? <h1 className="sub-title-confirm"> {key + 1} </h1> : <div className="p-t-20"> </div>}
        { listField(fieldArray, objFromReducer, fieldTitle, halfColumn) }
      </div>
    )}
  );

  const listField = (fieldArray, reducer, fieldTitle, halfColumn, fieldsInSection=0, mid=false) => fieldArray.map((field, j) => {
    if (reducer[field] === undefined) {
      return null;
    }
    let requiredLabels = reducer[field].name ? reducer[field].name : field;
    let showField = (requiredLabels === "signatoriesBibUserId" || reducer[field].value || reducer[field].hasOwnProperty('isToggled') || reducer[field] instanceof Array === true) &&
    (labels[requiredLabels] !== undefined || fieldTitle !== "");
    if ((requiredLabels === "tinNoNone" && reducer.haveTINNoNone && reducer.haveTINNoNone.value === "N") ||
    (requiredLabels === "othersTINNo" && reducer.haveTINNo && reducer.haveTINNo.value === "N") ||
    (requiredLabels === "reasonsNone" && reducer.haveTINNoNone && reducer.haveTINNoNone.value === "Y") ||
    (requiredLabels === "reasons" && reducer.haveTINNo && reducer.haveTINNo.value === "Y") ||
    (requiredLabels === "tinNoSG" && reducer.checkboxSG && reducer.checkboxSG.isToggled === false) ||
    ((requiredLabels === "tinNoUS" || requiredLabels === "isUSResident") && reducer.checkboxUS && reducer.checkboxUS.isToggled === false)) {
      return null; // hide field
    }
    if (!showField) {
      return null;
    } else {
      fieldsInSection = fieldsInSection + 1;
    }
    let displayValue = "";
    let actualValue = "";
    let showCountryImg = false;
    let midBorder = "";
    let halfWidth = "";
    let addTopRightBorder = false;
    const hideTopBorder = fieldsInSection !== 1 ? "--hide" : "";
    if ((reducer[field].hasOwnProperty('halfColumn') && reducer[field].halfColumn === true) || halfColumn[j]) {
      halfWidth = "half-width";
      if (mid === false) {
        mid = true;
      } else {
        midBorder = "-left-border";
        mid = false;
      }
    } else if (mid === true) {
      mid = false;
      addTopRightBorder = true;
    }
    if (requiredLabels === "primaryNatureOfBusiness" && inputValues[requiredLabels]) {
      displayValue = mapValueToLabel(inputValues[requiredLabels], reducer[field].value);
    } else if (requiredLabels === "secondaryNatureOfBusiness" && inputValues[reducer.primaryNatureOfBusiness.value]) {
      displayValue = mapValueToLabel(inputValues[reducer.primaryNatureOfBusiness.value], reducer[field].value);
    } else if (requiredLabels === "signatoriesBibUserId" && !reducer[field].value) {
      displayValue = reducer.signatoriesToggleBIBUser.isToggled === true ? "Yes" : "No";
    } else if (requiredLabels === "maritalStatus" && inputValues[requiredLabels]) {
      displayValue = inputValues[requiredLabels][reducer[field].value];
    } else if (requiredLabels === "signatorySelected" && inputValues[`signatoryTypeList1`]) {
        displayValue = inputValues[`signatoryTypeList1`][reducer[field].value];
    } else if (requiredLabels.toLowerCase().includes('reasons') && inputValues[`reasonsShortened`]) {
      displayValue = inputValues[`reasonsShortened`][reducer[field].value] + (reducer[field].value === "3" ? " - " + (field === "reasonsNone" ? reducer.othersNone.value : reducer.othersOthers.value) : "");
    } else if (requiredLabels.toLowerCase().includes('country')) {
      displayValue = reducer[field].description == undefined ? 'Singapore' : reducer[field].description ;
      actualValue = reducer[field].value.toLowerCase();
      showCountryImg = true;
    } else if (requiredLabels.toLowerCase().includes('mobile') && country(reducer[field].value) !== undefined) {
      displayValue = formatNumber(reducer[field].value);
      actualValue = country(reducer[field].value).toLowerCase();
      showCountryImg = true;
    } else if (reducer[field] instanceof Array === true) {
      displayValue = reducer[field].join(", ");
    } else if (reducer[field].isToggled === true || reducer[field].value === "Y") {
      displayValue = "Yes";
    } else if (reducer[field].isToggled === false || reducer[field].value === "N") {
      displayValue = "No";
    } else {
      displayValue = reducer[field].value;
    }
    if (field === "purposeOfAccount") {
      let array = [];
      if (reducer.transactional.isToggled) {
        array.push("Transactional");
      }
      if (reducer.loanRepayment.isToggled) {
        array.push("Loan Repayment");
      }
      if (reducer.investment.isToggled) {
        array.push("Investment");
      }
      if (reducer.others.isToggled) {
        array.push(`Others - ${reducer.otherPurpose.value}`);
      }
      displayValue = array.join(", ");
    }
    if (field === "payNowID") {
      displayValue = reducer.payNowUEN.value + reducer[field].value;
    }
    return(
      <div key={j}>
        { addTopRightBorder && <div className={`review-field-row`}></div> }
        <div className={`top-border${hideTopBorder}`}>
          <div className={`review-field-row ${halfWidth}${midBorder}`}>
            { fieldTitle !== "" ? <div className="review-field-label">{fieldTitle[j]}</div>
            : <div className="review-field-label">{labels[requiredLabels]} </div> }
            <div className="review-field-value">
              { showCountryImg === true && <img className="dropdown-flagImg" onError={(e) => e.target.style.display = 'none'} src={`./images/countriesFlags/${actualValue}.svg`} alt='icon' /> }
              { displayValue }
            </div>
          </div>
        </div>
      </div>
    )
  })

  return(
    <div>
      {requiredFields.map((section, i) => {
        sectionIndex = sectionIndex + 1;
        let passiveBusiness = true;
        let hasShareholders = true;
        let additionalServiceChecked = true;
        let showSection = true;
        let showFatca = true;
        let hidePageTitle = (sectionIndex !== 1 || page === "localAddressInput") ? "hide-page-title" : "";
        let fieldArray = section.fields;
        if (page === "localAddressInput" && reducer.mailingCountry.value !== "SG") {
          fieldArray = section.foreignFields;
        }
        if ((section.sectionTitle === "Additional Services - PayNow Corporate Services" && reducer.payNowID.value === "") ||
        (section.sectionTitle === "Additional Services - Business Internet Banking (BIBPlus)" && reducer.BIBRemittanceMessage.isDisabled === true)) {
          additionalServiceChecked = false;
        }
        if ((section.pageTitle === "Approved Signatories/Business Internet Banking Users" && props.accountSetupReducer.BIBRemittanceMessage.isDisabled === true)) {
          fieldArray = fieldArray.slice(0,4);
        }
        if (((section.sectionTitle === "Tax Residency" || section.fields[0] === "othersCountry") && reducer.checkboxNone.isToggled === true) ||
        (section.fields[0] === "countryNone" && reducer.checkboxNone.isToggled === false) ||
        (section.fields[0] === "othersCountry" && reducer.checkboxOthers.isToggled === false)) {
          showSection = false;
        }
        if (section.pageTitle === "Shareholder Details" && reducer.shareHoldersList.value.length === 0) {
          hasShareholders = false;
        }
        if (section.sectionTitle === "FATCA and CRS Status" && reducer.fatcaCRSStatus.value === "PB") {
          fieldArray = section.pbFields;
        }
        if (section.sectionTitle === "Controlling Person" && reducer.fatcaCRSStatus.value !== "PB") {
          passiveBusiness = false;
        }
        if ((!isNotSoleProp) && section.sectionTitle === "FATCA and CRS Status") {
          showFatca = false;
        }
        // for listing sections
        let idForListLength = 0;
        let type = "";
        if (reducer.hasOwnProperty('signatoriesID')) {
          idForListLength = reducer.signatoriesID;
          type = "signatories";
        }
        if (section.pageTitle === "Shareholder Details" && reducer.hasOwnProperty('shareholderID')) {
          idForListLength = reducer.shareholderID;
          type = "shareholder";
        }
        if (section.pageTitle === "" && reducer.hasOwnProperty('tinNoOthersId')) {
          idForListLength = reducer.tinNoOthersId;
          type = "tinNoOthers";
        }
        if (section.sectionTitle === "Controlling Person" && reducer.hasOwnProperty('controllingPersonId')) {
          idForListLength = reducer.controllingPersonId;
          type = "controllingPerson";
        }
        let fieldTitle = "";
        if (section.hasOwnProperty('fieldTitle')) {
          fieldTitle = section.fieldTitle;
        }
        let halfColumn = "";
        let mid = false;
        if (section.hasOwnProperty('halfColumn')) {
          halfColumn = section.halfColumn;
        }
        return(
          <div key={i}>
            <div className="review-header-container">
              <div>
                <h1 className={`desc ${hidePageTitle}`}>{takeFromReducer.title}</h1>
                { (section.pageTitle && section.pageTitle !== "" && hasShareholders && idForListLength.length > 0) ? <h1 className="desc desc-confirm">{section.pageTitle}</h1> : null }
                { section.sectionTitle && mailingChecked && additionalServiceChecked && passiveBusiness && showFatca && <h1 className="sub-title-confirm">{section.sectionTitle}</h1> }
              </div>
              { hasShareholders && page!=="contactDetails" && <img className={`review-page-pencil ${hidePageTitle}`} src={pencilIcon} alt="pencil" onClick={() => handleScrollBackTosection(takeFromReducer.page)} /> }
            </div>
            { passiveBusiness && hasShareholders && showSection && additionalServiceChecked &&
            ((section.pageTitle !== undefined && idForListLength.length > 0) ?
            listSection(idForListLength, fieldArray, reducer, fieldTitle, type, halfColumn)
            :  mailingChecked && listField(fieldArray, reducer, fieldTitle, halfColumn)) }

            { isNotSoleProp && section.sectionTitle === "FATCA and CRS Status" &&
            <div className="top-border">
              { fieldArray.map((field, h) => {
                let midBorder = "";
                let addTopRightBorder = false;
                if (halfColumn[h]) {
                  if (mid === false) {
                    mid = true;
                  } else {
                    midBorder = "-left-border";
                    mid = false;
                  }
                } else if (mid === true) {
                  mid = false;
                  addTopRightBorder = true;
                }
                let halfWidth = halfColumn[h] ? "half-width" : "";
                return(
                  <div key={h}>
                    { addTopRightBorder && <div className={`review-field-row`}></div> }
                    <div className={`review-field-row ${halfWidth}${midBorder}`} key={h}>
                      <div className="review-field-label">{fieldTitle[h]}</div>
                      <div className="review-field-value">{field}</div>
                    </div>
                  </div>
                )
              }) }
            </div>
          }
          </div>
        )
      })}
    </div>
  )
}

export default Review;
