import React, { Component } from "react";
import finishedImg from "./../../assets/images/navbar-finished.svg";
import currentImg from "./../../assets/images/navbar-current.svg";
import notFinishedImg from "./../../assets/images/navbar-not-finished.svg";
import initImg from "./../../assets/images/init.svg";
import dottedLineImg from "./../../assets/images/navbar-continue-line.svg";

class NavItem extends Component {
  render() {
    const {
      currentActiveStep,
      defaultStep,
      linkIdName,
      linkSelected,
      navTitle,
      stepFinished,
      dottedLine,
      onClick
    } = this.props;
    const showWhiteBoldLink = linkSelected ? "show-white-bold-link" : "";
    const showWhiteLink = stepFinished || currentActiveStep ? "show-white-link" : "";
    const linkClickEnable = linkSelected || currentActiveStep || stepFinished;
    const showFinishedImg = (!defaultStep && stepFinished) ? "show-finished-img": "finished-img";
    const showCurrentImg = currentActiveStep ? "show-current-img" : "current-img";
    const showNotFinishedImg = !currentActiveStep && !stepFinished ? "show-not-finished-img" : "not-finished-img";
    const showInitImg = defaultStep ? "show-init-img" : "not-finished-img";
    const hideVerticalLine = defaultStep;
    const whiteLine = (stepFinished || currentActiveStep) ? "white" : "";
    const haveDottedLine = dottedLine ? "dottedLinePadding" : "";
    return (
      <li className={`nav-item p-relative ${haveDottedLine}`}>
        {dottedLine && <span className="dotted-line"><img src={dottedLineImg} alt="dottedLine" /> </span>}
        {!hideVerticalLine && !dottedLine && <div className={`vertical-line ${whiteLine}`}/>}
        <span className={`${showFinishedImg}`}> <img src={finishedImg} alt="finished"/> </span>
        <span className={`${showCurrentImg}`}> <img src={currentImg} alt="current" /> </span>
        <span className={`${showNotFinishedImg}`}> <img src={notFinishedImg} alt="not finished"/> </span>
        <span className={`${showInitImg}`}> <img src={initImg} alt="default"/> </span>
        {linkClickEnable && (
          <a
            className={`nav-link js-scroll-trigger ${showWhiteLink} ${showWhiteBoldLink}`}
            href={linkIdName}
            onClick={onClick}
          >
            {navTitle}
          </a>
        )}
        {!linkClickEnable && (
          <span className={`nav-link js-scroll-trigger}`}>{navTitle}</span>
        )}
      </li>
    );
  }
}

export default NavItem;
