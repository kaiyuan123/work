import React from 'react';
import PropTypes from 'prop-types';
import './Checkbox.css';


const Checkbox = props => {
	const {
		description, onClick, isChecked,
		errorMsg, isValid, isDisabled, isRadio, inputID
	} = props;
	
	const checkAndDisabled = isDisabled && !isChecked ? "checkDisabled" : ""
	const makeFontBlue = inputID === "mailingAddressCheckbox" ? "mailingFontColor" : "";
	const isCheckBoxCRS = inputID && (inputID === "checkboxSG" || inputID === "checkboxUS" || inputID === "checkboxOthers" || inputID === "checkboxNone" || inputID.includes("checkboxSG") || inputID.includes("checkboxUS") || inputID.includes("checkboxOthers") || inputID.includes("checkboxNone"));
	const paddingCRS = isCheckBoxCRS ? "check-top-20" : "";
	const labelBold = isCheckBoxCRS && isChecked ? "labelFontWeight" : "";
	return (
		<div className={`toggle-input--container ${paddingCRS}`}>
			{!isValid &&
				<div className="errorMsg">
					{errorMsg}
				</div>}
			{!isRadio && <label className={`checkbox-container ${makeFontBlue} ${labelBold}`}>
				{description}
				<input type="checkbox"
					onChange={onClick ? onClick : null}
					checked={isChecked}
					disabled={isDisabled}
				/>
				<span className={`checkmark ${checkAndDisabled}`} />
			</label>}
			{isRadio && <label className="radio-container">
				<input type="radio" className="radioButton"
					onClick={onClick ? onClick : null}
					checked={isChecked}
					disabled={isDisabled}
				/>
				{description}
				<span className={`radio-checkmark`}></span>
			</label>}
		</div>
	);
}

Checkbox.propTypes = {
	onClick: PropTypes.func
};

Checkbox.defaultProps = {
	isValid: true,
	errorMsg: '',
};

export default Checkbox;
