import React from "react";
import PropTypes from "prop-types";

import "./ReadOnlyTextInputLine.css";
import nonEditableImg from "./../../../assets/images/non-editable-icon4.svg";

const ReadOnlyTextInputLine = props => {
  const { value, hasIcon } = props;
  return (
    <div className="read-only-text-input-others--container">
      <div className={`read-only-text-input-others--input--focused`}>{value}</div>
      {hasIcon && (
        <img
          className={"read-only-text-input-others--tick--visible"}
          src={nonEditableImg}
          alt="tick"
        />
      )}
    </div>
  );
};

ReadOnlyTextInputLine.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  hasIcon: PropTypes.bool
};

ReadOnlyTextInputLine.defaultProps = {
  label: "Label",
  value: "",
  hasIcon: true
};

export default ReadOnlyTextInputLine;
