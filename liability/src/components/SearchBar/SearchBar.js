import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { validators } from './../../common/validations';
import ScrollLock from "react-scroll-lock-component";
import ReactList from 'react-list';

import './SearchBar.css';

class SearchBar extends Component {
  onDropListBlur = (e) => {
    const { onBlur } = this.props;
    !this.focusInCurrentTarget(e) && onBlur();
  }

  focusInCurrentTarget = ({ relatedTarget, currentTarget }) => {
    let target = relatedTarget;
    if (target === null) { target = document.activeElement; }
    let node = target.parentNode || target.parent;
    while (node !== null) {
      if (node === currentTarget) return true;
      node = node.parentNode;
    }
    return false;
  }


  handleOnChange(e) {
    const { validator, errorMsgList } = this.props;
    const inputValue = e.target.value;
    let isValid = true;
    let errorMsg = "";
    if (validator && validator.length > 0) {
      let result = validators(validator, inputValue);
      isValid = result.status;
      errorMsg = result.errorMsg !== '' ? errorMsgList[result.errorMsg].replace("{number}", result.number).replace("{secondnumber}", result.secondnumber) : '';
    }

    const data = {
      value: inputValue,
      isValid,
      errorMsg,
      isInitial: false
    };
    this.props.onChange(data);
  };

  renderItem = (index, key) => {
    const { onClick, onBlur, items } = this.props;
    return (
      <div
        key={key}
        className="search--dropdown-selection-item"
        onClick={() => {
          onClick && onClick(items[index]);
          onBlur && onBlur();
        }}
      >
        {items[index].name}
      </div>
    );
  }

  render() {
    const { label, onFocus, isFocus, value, minSearchLength, items, isValid, errorMsg } = this.props;
    const searchFocus = isFocus || value.length > 0 || !isValid ? "--focused" : "";
    const searchLength = minSearchLength ? minSearchLength : 3;
    const isShow = isFocus && (value.length >= searchLength) ? "--show" : "";
    const showErrorLabel = isValid ? "" : "searchInputLabel--error";
    const errorContainerStyle = isValid ? "" : "searchInputLabel-container--error";

    return (

      <div tabIndex='1' className={"search--flex-container" + searchFocus} onBlur={(e) => this.onDropListBlur(e)}>
        <div className={`search--input-container ${errorContainerStyle}`}>
          <div className='search--input-box'>
            <label className={`search--textInputLabel${searchFocus} ${showErrorLabel}`}>
              {`${label}${isValid ? "" : ` ${errorMsg}`}`}
            </label>
            <input
              onChange={this.handleOnChange.bind(this)}
              onFocus={onFocus ? onFocus : null}
              className={"search--searchValue" + searchFocus}
              value={value}
            />

          </div>
        </div>

        <ScrollLock>
          <div className={"search--dropdown-selection" + isShow}>
            <ReactList
              itemRenderer={this.renderItem}
              length={items && items.length}
              type='uniform'
            />
            <div className='iphone-fix' />
          </div>
        </ScrollLock>
        <div className="bottomLine" />
        <div className="fullErrorContent" />
      </div>
    );
  }
}

SearchBar.propTypes = {
  isFocus: PropTypes.bool,
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func,
  minSearchLength: PropTypes.number,
  items: PropTypes.array,
  isValid: PropTypes.bool,
  errorMsg: PropTypes.string
};

export default SearchBar;
