import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { connect } from "react-redux";
import moment from 'moment';
import Swipe from 'react-easy-swipe';
/*import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';*/
import NavigationBar from "./../NavigationBar/NavigationBar";
import { Map, InfoWindow, Marker, GoogleApiWrapper, Circle } from './google-maps-react';
import "./BookAppointmentSearch.css";
import bookingLocation from "./../../assets/images/googleMap/bookingLocation.svg";
import branchIcon from "./../../assets/images/googleMap/marker-image.svg";
import branchIconSelected from "./../../assets/images/googleMap/marker-image-selected.svg";
import Dropdown from "./../../components/Dropdown/Dropdown";
import { mapValueToDescription } from "./../../common/utils";
import closeIcon from "./../../assets/images/cross-grey.svg";
import companyPointer from "./../../assets/images/googleMap/companyPointer.svg";
import bookingCalenderIcon from "./../../assets/images/googleMap/bookingCalenderIcon.svg";

import openLocationList from "./../../assets/images/googleMap/openLocationList.svg";
import closeLocationList from "./../../assets/images/googleMap/closeLocationList.svg";
import checkMark from "./../../assets/images/googleMap/checkMark.svg";
import searchIcon from "./../../assets/images/googleMap/search.svg";

import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import arrowLightDownImg from './../../assets/images/icon-arrow-down-light.svg';
import arrowDownImg from './../../assets/images/icon-arrow-down.svg';

import {
	client,
	apiKey
} from "./../../api/httpApi";

import {
	setDropdownFocusStatus,
	selectDropdownItem,
	changeSearchInputValue,
	rightLocationClick,
	setDateTimeValue,
	bookAppointmentSelectNavigation
} from "./../../actions/bookAppointmentAction";


let CONTACTS = [];
let branchesAvailable = false;
let availableTimesForDate = [];
let timeUnderAvailableTimes = [];
let currentCompanyZipCode = null

class BookAppointmentPage extends Component {

	state = {
		popupOpen: false,
		currentAddress: false,
		currentAddressGeoCodes: null,
		currentCircleGeoCodes: null,
		currentSearchValue: '',
		selectedObj: null,
		displayedContacts: null,
		showingInfoWindow: false,
		activeMarker: {},
		selectedPlace: {},
		showRightContent: true,
		startDate: null,
		availableTimes: [],
		zoomIndex: 13
	};


	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		/*this.handleSelect = this.handleSelecdf;;4ct.bind(this);*/
	}

	/*handleSelect(data){
		this.setState({
			startDate: ''
		});
	}*/

	handleChange(date) {
		const { dispatch, bookAppointmentReducer } = this.props;
		const { availableDateArray } = bookAppointmentReducer;

		availableTimesForDate = [];
		timeUnderAvailableTimes = [];

		availableTimesForDate = availableDateArray.filter((el) => {
			return el.split('T')[0] === moment(date).format('YYYY-MM-DD');
		});

		timeUnderAvailableTimes = availableTimesForDate.filter((el) => {
			return el.split('T')[1] === moment(date).format('HH:mm');
		});

		if (timeUnderAvailableTimes.length > 0) {
			this.setState({
				startDate: new Date(date),
				availableTimes: availableTimesForDate
			});
		}
		else {
			this.setState({
				startDate: new Date(availableTimesForDate[0]),
				availableTimes: availableTimesForDate
			});
		}

		let dateTimeFormat = moment(date).format('YYYY-MM-DD') + 'T' + moment(date).format('HH:mm');
		dispatch(setDateTimeValue(dateTimeFormat));
	}

	componentDidMount() {
		const { bookAppointmentReducer, retrievingReducer } = this.props;
		const { availableLocationsBranchIDS } = bookAppointmentReducer;

		/*CONTACTS = window.branchList;*/
		CONTACTS = window.branchList.filter((el) => {
			return availableLocationsBranchIDS.indexOf(el.branchId.toLowerCase()) !== -1;
		});

		if (retrievingReducer.myInfoData !== null) {
			currentCompanyZipCode = retrievingReducer.myInfoData.entity.addresses.filter((el) => {
				return el.type === "R";
			});

			if (currentCompanyZipCode.length > 0) {
				this.getLatLngByZipcode(currentCompanyZipCode[0]['postalCode']);
			}
		}

		this.setState({
			displayedContacts: CONTACTS
		})
	}

	closeShowingInfoWindow() {
		this.setState({
			showingInfoWindow: false,
			activeMarker: null
		})
	}

	closePopupWindow() {
		if (this.state.showingInfoWindow) {
			this.setState({
				popupOpen: false,
				showingInfoWindow: false,
				activeMarker: null
			})
		}
	}

	toggleRightContent() {
		this.setState({
			showRightContent: !this.state.showRightContent
		});
		if (this.state.selectedObj !== null && !this.state.showRightContent) {
			let searchValue = this.state.selectedObj.branchId.toLowerCase();
			let targetBranch = document.getElementById(`branchSelect_${searchValue}`);
			setTimeout(function () {
				targetBranch.scrollIntoView()
			}, 500);
		}
	}

	anotherFunc(e) {
		// console.log(e)
	}
	onSwipeDown(event) {
		// console.log("down", event);
		let result = event;
		return result;
	}

	onSwipeUp(event) {
		// console.log("up", event);
	}
	onMarkerClick = (props, marker, e, item) => {
		/*this.setState({
			selectedObj: item,
			selectedPlace: props,
			activeMarker: marker,
			showingInfoWindow: true
		});
		this.bottomLinkClicked();*/
		this.rightLocationClickHandler(item, true);
	};

	bottomLinkClicked = () => {
		const { dispatch, bookAppointmentReducer } = this.props;
		let availableBranchObj = bookAppointmentReducer.availableLocations.content;

		let searchValue = this.state.selectedObj.branchId.toLowerCase();
		let filterItem = availableBranchObj.filter((itemData) => {
			return searchValue === itemData.branchId
		});
		dispatch(rightLocationClick(searchValue, filterItem));

		/*let searchList = this.state.selectedObj.name.toLowerCase();
		let displayedContacts = CONTACTS.filter((el) => {
			return el.name.toLowerCase().indexOf(searchList) !== -1;
		});*/

		this.setState({
			displayedContacts: CONTACTS/*displayedContacts*/,
			currentSearchValue: this.state.selectedObj.name,
			showRightContent: true
		});

		availableTimesForDate = filterItem[0]['availableTimeSlots'].filter((el) => {
			return el.split('T')[0] === moment(new Date(filterItem[0]['availableTimeSlots'][0])).format('YYYY-MM-DD');
		});

		this.setState({
			startDate: new Date(filterItem[0]['availableTimeSlots'][0]),
			availableTimes: availableTimesForDate
		});
		dispatch(setDateTimeValue(filterItem[0]['availableTimeSlots'][0]));

		let targetBranch = document.getElementById(`branchSelect_${searchValue}`);
		targetBranch.scrollIntoView();

		this.closeShowingInfoWindow();
	};


	onMapClicked = () => {
		this.closePopupWindow();
	};


	handleToSubmitApplication() {
		this.props.onContinue();
	}

	handlePopupEvent = () => {
		document.getElementById('geocodeInput').value = '';

		let _this = this;
		this.setState({
			zoomIndex: 12
		});

		setTimeout(function () {
			_this.setState({
				zoomIndex: 13
			});
		}, 1);

		this.setState({ popupOpen: !this.state.popupOpen, showRightContent: true, displayedContacts: CONTACTS });

		if (this.state.selectedObj !== null) {
			let searchValue = this.state.selectedObj.branchId.toLowerCase();
			let targetBranch = document.getElementById(`branchSelect_${searchValue}`);
			setTimeout(function () {
				targetBranch.scrollIntoView()
			}, 100);
		}

	};

	searchHandler = (event) => {
		let searchList = event.target.value.toLowerCase();
		let displayedContacts = CONTACTS.filter((el) => {
			let searchValue = el.name.toLowerCase();
			let searchLoaction = el.address.toLowerCase();
			return searchValue.startsWith(searchList.toLowerCase()) ||
				searchValue.toLowerCase().includes(" " + searchList.toLowerCase()) ||
				searchLoaction.startsWith(searchList.toLowerCase()) ||
				searchLoaction.toLowerCase().includes(" " + searchList.toLowerCase());
		});
		/*this.setState({ displayedContacts: displayedContacts, currentSearchValue: event.target.value });*/
		this.setState({ displayedContacts: displayedContacts });

		this.closePopupWindow();
	}

	rightLocationClickHandler = (item, isMarker = false) => {
		const { dispatch, bookAppointmentReducer } = this.props;
		let availableBranchObj = bookAppointmentReducer.availableLocations.content;

		let searchValue = item.branchId.toLowerCase();
		let filterItem = availableBranchObj.filter((itemData) => {
			return searchValue === itemData.branchId
		});

		availableTimesForDate = filterItem[0]['availableTimeSlots'].filter((el) => {
			return el.split('T')[0] === moment(new Date(filterItem[0]['availableTimeSlots'][0])).format('YYYY-MM-DD');
		});

		let _this = this;

		this.setState({
			zoomIndex: 13
		});

		_this.setState({
			currentCircleGeoCodes: [item.lat, item.lng],
		});

		setTimeout(function () {
			_this.setState({
				startDate: new Date(filterItem[0]['availableTimeSlots'][0]),
				selectedObj: item,
				availableTimes: availableTimesForDate,
				showRightContent: true,
				zoomIndex: 18
			});
		}, 1);


		dispatch(setDateTimeValue(filterItem[0]['availableTimeSlots'][0]));

		dispatch(rightLocationClick(searchValue, filterItem));

		let targetBranch = document.getElementById(`branchSelect_${searchValue}`);
		setTimeout(function () {
			targetBranch.scrollIntoView()
		}, 100);

		if (!isMarker) {
			dispatch(bookAppointmentSelectNavigation("onClickDate"))
		}
	}

	onInfoWindowOpen() {
		const button = (
			<div>
				<div>{this.state.selectedPlace.name}</div>
				<button className="bookAppointmentButton" onClick={() => this.bottomLinkClicked()}>
					Book an Appointment
				</button>
			</div>
		);

		ReactDOM.render(
			React.Children.only(button),
			document.getElementById("popupContent")
		);
	}

	handleDropdownBlur(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(false, field));
		this.closePopupWindow();
	}

	handleDropdownFocus(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(true, field));
		this.closePopupWindow();
	}

	handleDropdownClick(data, field) {
		const { dispatch, bookAppointmentReducer } = this.props;
		let availableBranchObj = bookAppointmentReducer.availableLocations.content;

		if (bookAppointmentReducer[field].value === data.value) {
			return;
		}

		if (field === "bookingBranch") {
			let filterItem = availableBranchObj.filter((item) => {
				return data.value === item.branchId
			});

			dispatch(selectDropdownItem(data.value, data.description, field, filterItem));

			availableTimesForDate = filterItem[0]['availableTimeSlots'].filter((el) => {
				return el.split('T')[0] === moment(new Date(filterItem[0]['availableTimeSlots'][0])).format('YYYY-MM-DD');
			});

			let currentSelectedItem = this.state.displayedContacts.filter((item) => {
				return data.value.toLowerCase() === item.branchId.toLowerCase()
			});

			this.setState({
				startDate: new Date(filterItem[0]['availableTimeSlots'][0]),
				availableTimes: availableTimesForDate,
				selectedObj: currentSelectedItem[0],
			});
			dispatch(setDateTimeValue(filterItem[0]['availableTimeSlots'][0]));
		}
		else {
			dispatch(selectDropdownItem(data.value, data.description, field));
		}

		this.closePopupWindow();
	}


	handleOnSearchChange(e, field) {
		const { dispatch } = this.props;
		const value = e.target.value;
		dispatch(changeSearchInputValue(value, field));
	}

	handleOnClickNav(item) {
		const { dispatch, bookAppointmentReducer } = this.props;
		const { bookingBranch } = bookAppointmentReducer;
		if (item === '0') {
			dispatch(bookAppointmentSelectNavigation("onClickBranch"))
		}

		if (item === '1' && bookingBranch.value !== "") {
			dispatch(bookAppointmentSelectNavigation("onClickDate"))
		}
	}

	getLatLngByZipcode(zipcode) {
		let _this = this;
		let google = this.props.google;
		let geocoder = new google.maps.Geocoder();
		let address = zipcode;
		let latitude = '';
		let longitude = '';
		geocoder.geocode({ 'address': address }, function (results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				latitude = results[0].geometry.location.lat();
				longitude = results[0].geometry.location.lng();
				_this.setState({
					currentAddress: true,
					currentAddressGeoCodes: [latitude, longitude],
					currentCircleGeoCodes: [latitude, longitude],
				});
			} else {
				_this.setState({
					currentAddress: true,
					currentAddressGeoCodes: null,
					currentCircleGeoCodes: null
				});
			}
		});
	}

	renderSearchContent() {
		const { commonReducer, bookAppointmentReducer } = this.props;
		const bookAppointmentValue = commonReducer.appData.bookAppointment;
		const labels = bookAppointmentValue.labels;

		branchesAvailable = false;
		let isMobileDevice = false;

		if (this.state.displayedContacts === null) {
			return null
		}

		if ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			window.navigator.userAgent
		))) {
			isMobileDevice = true;
		}
		const height = window.innerHeight - 105;
		const listHeight = window.innerHeight - 163;
		return (
			<div>
				<div className="remodal-overlay remodal-is-opened"
					style={{ display: this.state.popupOpen ? "block" : "none" }} />
				<div className="remodal-is-opened" style={{ display: "flex" }}>
					<div className="appointmentMainPopup">
						<div>
							<div className="google-map-container" style={{ minHeight: `${height}px` }}>
								<Map google={this.props.google}
									containerStyle={{
										width: '100%',
										height: '100%',
										position: 'absolute',
										top: 0,
										border: '1px solid rgba(204, 204, 204, 0.59)'
									}}
									initialCenter={{
										lat: this.state.selectedObj !== null ? this.state.selectedObj.lat : this.state.currentAddressGeoCodes !== null ? this.state.currentAddressGeoCodes[0] : 1.355255,
										lng: this.state.selectedObj !== null ? this.state.selectedObj.lng : this.state.currentAddressGeoCodes !== null ? this.state.currentAddressGeoCodes[1] : 103.82313899999997
									}}
									center={{
										lat: this.state.selectedObj !== null ? this.state.selectedObj.lat : this.state.currentAddressGeoCodes !== null ? this.state.currentAddressGeoCodes[0] : 1.355255,
										lng: this.state.selectedObj !== null ? this.state.selectedObj.lng : this.state.currentAddressGeoCodes !== null ? this.state.currentAddressGeoCodes[1] : 103.82313899999997
									}}
									zoom={this.state.zoomIndex}
									mapTypeControl={false}
									clickableIcons={false}
									fullscreenControl={false}
									streetViewControl={false}
									onClick={this.onMapClicked}
									zoomControlOptions={{ position: this.props.google.maps.ControlPosition.LEFT_BOTTOM }}

									styles={[{
										featureType: "administrative",
										elementType: "labels.text.fill",
										stylers: [{ color: "#444444" }]
									}, {
										featureType: "landscape",
										elementType: "all",
										stylers: [{ color: "#f2f2f2" }]
									}, {
										featureType: "water",
										elementType: "all",
										stylers: [{ color: "#91bde7" }, { visibility: "on" }]
									}, {
										featureType: "road.arterial",
										elementType: "labels.icon",
										stylers: [{ visibility: "off" }]
									}, {
										featureType: "administrative.neighborhood",
										elementType: "labels.icon",
										stylers: [{ visibility: "off" }]
									}, {
										featureType: "administrative.neighborhood",
										elementType: "labels.text",
										stylers: [{ visibility: "off" }]
									}, {
										featureType: "poi",
										elementType: "all",
										stylers: [{ visibility: "off" }]
									}, {
										featureType: "poi.government",
										elementType: "labels",
										stylers: [{ visibility: "on" }]
									}, {
										featureType: "poi.park",
										elementType: "geometry",
										stylers: [{ visibility: "on" }]
									}, {
										featureType: "poi.park",
										elementType: "labels",
										stylers: [{ visibility: "on" }]
									}, {
										featureType: "poi.attraction",
										elementType: "labels",
										stylers: [{ visibility: "on" }]
									}]}

								/*styles={[{
									featureType: "administrative",
									elementType: "labels.text.fill",
									stylers: [{ color: "#444444" }]
								}, {
									featureType: "administrative.neighborhood",
									elementType: "labels.text",
									stylers: [{ visibility: "off" }]
								}, {
									featureType: "administrative.neighborhood",
									elementType: "labels.icon",
									stylers: [{ visibility: "on" }]
								}, {
									featureType: "landscape",
									elementType: "all",
									stylers: [{ color: "#f2f2f2" }]
								}, {
									featureType: "poi",
									elementType: "all",
									stylers: [{ visibility: "off" }]
								}, {
									featureType: "poi.attraction",
									elementType: "labels",
									stylers: [{ visibility: "on" }]
								}, {
									featureType: "poi.attraction",
									elementType: "labels.text",
									stylers: [{ visibility: "on" }]
								}, {
									featureType: "poi.attraction",
									elementType: "labels.icon",
									stylers: [{ visibility: "on" }]
								}, {
									featureType: "poi.business",
									elementType: "all",
									stylers: [{ visibility: "simplified" }]
								}, {
									featureType: "poi.business",
									elementType: "labels.text",
									stylers: [{ visibility: "on" }]
								}, {
									featureType: "poi.business",
									elementType: "labels.icon",
									stylers: [{ visibility: "on" }, { weight: "1.10" }]
								}, {
									featureType: "poi.government",
									elementType: "labels",
									stylers: [{ visibility: "on" }]
								}, {
									featureType: "poi.park",
									elementType: "geometry",
									stylers: [{ visibility: "on" }]
								}, {
									featureType: "poi.park",
									elementType: "labels.text",
									stylers: [{ visibility: "simplified" }]
								}, {
									featureType: "road",
									elementType: "all",
									stylers: [{ saturation: -100 }, { lightness: 45 }]
								}, {
									featureType: "road.highway",
									elementType: "all",
									stylers: [{ visibility: "simplified" }]
								}, {
									featureType: "road.arterial",
									elementType: "labels.text",
									stylers: [{ visibility: "on" }]
								}, {
									featureType: "road.arterial",
									elementType: "labels.icon",
									stylers: [{ visibility: "off" }]
								}, {
									featureType: "road.local",
									elementType: "labels.text",
									stylers: [{ visibility: "off" }]
								}, {
									featureType: "transit",
									elementType: "all",
									stylers: [{ visibility: "off" }]
								}, {
									featureType: "water",
									elementType: "all",
									stylers: [{ color: "#91bde7" }, { visibility: "on" }]
								}]}*/
								>
									{
										this.state.currentAddressGeoCodes !== null &&
										<Marker position={{
											lat: this.state.currentAddressGeoCodes[0],
											lng: this.state.currentAddressGeoCodes[1]
										}}
											title={'Company Location'}
											name={'Company Location'}
											icon={{
												url: companyPointer,
												anchor: new this.props.google.maps.Point(15, 18.75),
												scaledSize: new this.props.google.maps.Size(30, 37.5)
											}}
										/>
									}

									{
										this.state.currentAddressGeoCodes !== null &&
										<Circle
											center={{
												lat: this.state.currentCircleGeoCodes[0],
												lng: this.state.currentCircleGeoCodes[1]
											}}
											radius={5000}
											strokeColor="#0000FF"
											strokeOpacity={0.2}
											strokeWeight={1}
											fillColor="#0000FF"
											fillOpacity={0.1}
										/>
									}

									{

										(this.state.displayedContacts).map((item, index) => {
											let isSelected = (item.branchId).toLowerCase() === bookAppointmentReducer.bookingBranch.value;
											if (item.type === "Branch") {
												return (
													<Marker key={index}
														title={item.name}
														name={item.name}
														position={{ lat: item.lat, lng: item.lng }}
														onClick={(props, marker, e) => this.onMarkerClick(props, marker, e, item)}
														bounce={isSelected}
														icon={{
															url: isSelected ? branchIconSelected : branchIcon,
															anchor: isSelected ? new this.props.google.maps.Point(15, 18.75) : new this.props.google.maps.Point(15, 18.75),
															scaledSize: isSelected ? new this.props.google.maps.Size(30, 37.5) : new this.props.google.maps.Size(30, 37.5)
														}}
													/>

												)
											}
										})
									}
									{/*<InfoWindow
										marker={this.state.activeMarker}
										visible={this.state.showingInfoWindow}
										onOpen={() => {
											this.onInfoWindowOpen();
										}}
									>
										<div id="popupContent"/>
									</InfoWindow>*/}
								</Map>
								<Swipe onSwipeDown={this.onSwipeDown} onSwipeUp={this.onSwipeUp}>
									<div className={`holder list-panel popupRightSearchMain ${isMobileDevice && this.state.showRightContent ? "" : "close"}`}
										style={{ width: this.state.showRightContent ? '330px' : 0 }}>
										<div className="geocodesearch-block" style={{ display: this.state.showRightContent ? 'block' : 'none' }}>
											<input id="geocodeInput" type="text" onChange={this.searchHandler} placeholder={labels.googleInput}
												autoComplete="off" />
											<img src={searchIcon} width={50} className="icon-search" />
										</div>

										<ul className={`storelist ${isMobileDevice && this.state.showRightContent ? "" : "remove-border"}`} style={{ overflowY: this.state.showRightContent ? 'auto' : 'hidden'  , height: `${listHeight}px`}}>
											{
												(this.state.displayedContacts).map((item, index) => {
													if (item.type === "Branch") {
														branchesAvailable = true;
														return (
															<li id={`branchSelect_${(item.branchId).toLowerCase()}`}
																className={`store ${(item.branchId).toLowerCase() === bookAppointmentReducer.bookingBranch.value ? 'selected' : ''}`}
																key={index} onClick={() => this.rightLocationClickHandler(item)} >
																<div>
																	<div className="title">{item.name}</div>
																	<address>{item.address},<br />{item.postalcode}</address>
																	<ul className="buttons">
																		<li>
																			<span className="branchTel">Tel: {item.tel}</span>
																		</li>
																	</ul>
																	<div className="openhour">
																		{item.OpHrMonFri !== "" && <div className="time">Mon-Fri: {item.OpHrMonFri}</div>}
																		{item.OpHrSat !== "" && <div className="time">Sat: {item.OpHrSat}</div>}
																		{item.OpHrSun !== "" && <div className="time">Sun: {item.OpHrSun}</div>}
																	</div>
																	{item.DescSum !== "" && <div className="serviceMessage">
																		{item.DescSum !== "" && <div><span className="descSumContent"
																			dangerouslySetInnerHTML={{ __html: item.DescSum }} />
																		</div>}
																	</div>}
																	<div className="bottomSpace" />
																</div>
																{/* <button type="button" className="branchSelectButton" onClick={() => this.handlePopupEvent()}>select</button> */}
																{/*{(item.branchId).toLowerCase() === bookAppointmentReducer.bookingBranch.value &&
															<img src={checkMark} width={35} height={27}
																	 style={{
																		 position: 'absolute',
																		 bottom: '20px',
																		 right: '20px'
																	 }}
															/>
															}*/}
															</li>
														)
													}
												})
											}
											{!branchesAvailable && <span style={{
												padding: '10px 20px',
												display: 'block',
												fontSize: '12px',
												color: 'red'
											}}>{labels.errorNoResults}</span>}
										</ul>

										{isMobileDevice ?
											<div>
												<div className="popupRightToggleSwitch" onClick={() => this.toggleRightContent()} >
													<div className={`semi-circle ${this.state.showRightContent ? "remove" : ""}`} />

												</div>
											</div> :
											<div className="popupRightToggleSwitch" onClick={() => this.toggleRightContent()}>
												<img src={this.state.showRightContent ? closeLocationList : openLocationList} width={23}
													height={23}
													style={{
														transform: this.state.showRightContent ? 'rotate(0deg)' : 'rotate(180deg)'
													}}
												/>
											</div>
										}
									</div>
								</Swipe>
							</div>
						</div>

					</div>
				</div>
			</div>

		)
	}

	render() {
		const { commonReducer, bookAppointmentReducer } = this.props;
		const { bookingBranch, bookingTimeSlots, availableDateArray, onClickDate, onClickBranch, itemActive } = bookAppointmentReducer;
		const errorMsgList = commonReducer.appData.errorMsgs;

		const bookAppointmentValue = commonReducer.appData.bookAppointment;
		const labels = bookAppointmentValue.labels;

		let bookingBranchList = {};
		let bookingTimeSlotsList = {};

		if (CONTACTS !== null) {
			(CONTACTS).map((item, index) => {
				if (item.type === "Branch") {
					bookingBranchList[(item.branchId).toLowerCase()] = item.name.split("-")[1];
				}
			});
		}

		if (bookAppointmentReducer.selectedObject !== null && bookAppointmentReducer.selectedObject !== undefined && bookAppointmentReducer.selectedObject[0] !== undefined) {
			(bookAppointmentReducer.selectedObject[0]['availableTimeSlots']).map((item, index) => {
				bookingTimeSlotsList[index] = item;
			});
		}

		// console.log(this.onSwipeDown())
		return (
			<div>
				<NavigationBar
					items={bookAppointmentValue.navigationBarItmes}
					onClick={(item) => this.handleOnClickNav(item)}
					isActive={itemActive}
				/>
				{onClickDate &&
					<div className="bookAppointment-m">
						<p className="uob-headline-m">{bookAppointmentValue.subtitle1}</p>
						<div className="bookAppointment-table">
							<div className="positionRelative bookAppointment-table-left">
								<Dropdown
									inputID="bookingBranch"
									label={labels.bookingBranch}
									dropdownItems={mapValueToDescription(
										bookingBranchList
									)}
									isFocus={bookingBranch.isFocus}
									value={bookingBranch.value}
									isValid={bookingBranch.isValid}
									errorMsg={bookingBranch.errorMsg}
									errorMsgList={errorMsgList}
									focusOutItem={true}
									validator={["required"]}
									searchValue={bookingBranch.searchValue}
									onBlur={this.handleDropdownBlur.bind(this, "bookingBranch")}
									onFocus={this.handleDropdownFocus.bind(this, "bookingBranch")}
									onClick={data => this.handleDropdownClick(data, "bookingBranch")}
									onSearchChange={event =>
										this.handleOnSearchChange(event, "bookingBranch")
									}
								/>
							</div>
							<div className="bookAppointment-table-right positionRelative">
								<div className="positionRelative">
									<div>
										<div className="textInputContainer">
											<div className="textInputContent">
												<label className="textInputLabel--focused">Select Time</label>
												<input type="text" style={{ color: this.state.startDate === null ? '#b1b1b1' : '' }}
													value={this.state.startDate !== null ? moment(this.state.startDate).format('DD/MM/YYYY - hh:mm A') : 'Select UOB Branch first'}
													className="textInput--focused" readOnly={true} />
												<DatePicker
													selected={this.state.startDate}
													onChange={this.handleChange}
													/*onSelect={this.handleSelect}*/
													showTimeSelect
													disabled={bookAppointmentReducer.bookingBranch.value === ""}
													includeDates={availableDateArray}
													includeTimes={this.state.availableTimes}
													dateFormat="d/MM/yyyy - h:mm aa"
													className="dateTimeInputBox"
												/>
											</div>
										</div>
										{this.state.startDate !== null && <div className="bottomLine" />}
										<div className="fullErrorContent" />
									</div>
								</div>
								<div style={{ position: 'absolute', right: '0', top: '0' }}>
									{this.state.startDate !== "" && this.state.startDate !== null ?
										<img
											className='dropdown--inputArrow'
											src={arrowDownImg}
											alt='dropdown-arrow'
										/>
										: <img
											className='dropdown--inputArrow'
											src={arrowLightDownImg}
											alt='dropdown-arrow'
										/>
									}

								</div>
							</div>
						</div>
					</div>
				}

				{onClickBranch &&
					<div>
						{this.renderSearchContent()}
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = state => {
	const { commonReducer, retrievingReducer, applicationReducer, bookAppointmentReducer } = state;
	return { commonReducer, retrievingReducer, applicationReducer, bookAppointmentReducer };
};

export default connect(mapStateToProps)(GoogleApiWrapper({
	apiKey: apiKey,
	client: client
})(BookAppointmentPage));
