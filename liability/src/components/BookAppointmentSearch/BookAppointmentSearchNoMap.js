import React, { Component } from "react";
import { connect } from "react-redux";
import moment from 'moment';
import "./BookAppointmentSearch.css";
import bankBranchList from './utils/bankBranchList'
import Dropdown from "./../../components/Dropdown/Dropdown";
import NavigationBar from "./../NavigationBar/NavigationBar";
import { mapValueToDescription } from "./../../common/utils";
import branchIcon from "./../../assets/images/googleMap/branchIcon.png";
import closeIcon from "./../../assets/images/cross-grey.svg";
import bookingLocation from "./../../assets/images/googleMap/bookingLocation.svg";
import bookingCalenderIcon from "./../../assets/images/googleMap/bookingCalenderIcon.svg";

import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import arrowLightDownImg from './../../assets/images/icon-arrow-down-light.svg';
import arrowDownImg from './../../assets/images/icon-arrow-down.svg';

import {
	setDropdownFocusStatus,
	selectDropdownItem,
	changeSearchInputValue,
	rightLocationClick,
	setDateTimeValue,
	bookAppointmentSelectNavigation
} from "./../../actions/bookAppointmentAction";


let CONTACTS = [];
let branchesAvailable = false;
let availableTimesForDate = [];
let timeUnderAvailableTimes = [];

class BookAppointmentSearchNoMap extends Component {

	state = {
		popupOpen: false,
		currentSearchValue: '',
		selectedObj: null,
		displayedContacts: null,
		showingInfoWindow: false,
		activeMarker: {},
		selectedPlace: {},
		showRightContent: false,
		startDate: null,
		availableTimes: [],
		onClickBranch: true,
		onClickDate: false,
		onClickDateDisabled: true,
		itemActive: '0'
	};


	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		/*this.handleSelect = this.handleSelect.bind(this);*/
	}

	/*handleSelect(data){
		this.setState({
			startDate: ''
		});
	}*/

	handleChange(date) {
		const { dispatch, bookAppointmentReducer } = this.props;
		const { availableDateArray } = bookAppointmentReducer;

		availableTimesForDate = [];
		timeUnderAvailableTimes = [];

		availableTimesForDate = availableDateArray.filter((el) => {
			return el.split('T')[0] === moment(date).format('YYYY-MM-DD');
		});

		timeUnderAvailableTimes = availableTimesForDate.filter((el) => {
			return el.split('T')[1] === moment(date).format('HH:mm');
		});

		if (timeUnderAvailableTimes.length > 0) {
			this.setState({
				startDate: new Date(date),
				availableTimes: availableTimesForDate
			});
		}
		else {
			this.setState({
				startDate: new Date(availableTimesForDate[0]),
				availableTimes: availableTimesForDate
			});
		}

		let dateTimeFormat = moment(date).format('YYYY-MM-DD') + 'T' + moment(date).format('HH:mm');
		dispatch(setDateTimeValue(dateTimeFormat));
	}

	componentDidMount() {
		const { bookAppointmentReducer } = this.props;
		const { availableLocationsBranchIDS } = bookAppointmentReducer;

		/*CONTACTS = bankBranchList;*/
		CONTACTS = bankBranchList.filter((el) => {
			return availableLocationsBranchIDS.indexOf(el.branchId.toLowerCase()) !== -1;
		});

		this.setState({
			displayedContacts: CONTACTS
		})
	}

	closeShowingInfoWindow() {
		this.setState({
			showingInfoWindow: false,
			activeMarker: null
		})
	}

	closePopupWindow() {
		if (this.state.showingInfoWindow) {
			this.setState({
				popupOpen: false,
				showingInfoWindow: false,
				activeMarker: null
			})
		}
	}

	toggleRightContent() {
		this.setState({
			showRightContent: !this.state.showRightContent
		})
	}

	onMarkerClick = (props, marker, e, item) => {
		this.setState({
			selectedObj: item,
			selectedPlace: props,
			activeMarker: marker,
			showingInfoWindow: true
		});
		this.bottomLinkClicked();
	};

	bottomLinkClicked = () => {
		const { dispatch, bookAppointmentReducer } = this.props;
		let availableBranchObj = bookAppointmentReducer.availableLocations.content;

		let searchValue = this.state.selectedObj.branchId.toLowerCase();
		let filterItem = availableBranchObj.filter((itemData) => {
			return searchValue === itemData.branchId
		});
		dispatch(rightLocationClick(searchValue, filterItem));

		/*let searchList = this.state.selectedObj.name.toLowerCase();
		let displayedContacts = CONTACTS.filter((el) => {
			return el.name.toLowerCase().indexOf(searchList) !== -1;
		});*/

		this.setState({
			displayedContacts: CONTACTS/*displayedContacts*/,
			currentSearchValue: this.state.selectedObj.name,
			showRightContent: true
		});

		availableTimesForDate = filterItem[0]['availableTimeSlots'].filter((el) => {
			return el.split('T')[0] === moment(new Date(filterItem[0]['availableTimeSlots'][0])).format('YYYY-MM-DD');
		});

		this.setState({
			startDate: new Date(filterItem[0]['availableTimeSlots'][0]),
			availableTimes: availableTimesForDate
		});
		dispatch(setDateTimeValue(filterItem[0]['availableTimeSlots'][0]));

		let targetBranch = document.getElementById(`branchSelect_${searchValue}`);
		targetBranch.scrollIntoView();

		this.closeShowingInfoWindow();
	};


	onMapClicked = () => {
		this.closePopupWindow();
	};


	handleToSubmitApplication() {
		this.props.onContinue();
	}

	handlePopupEvent = () => {
		this.setState({ popupOpen: !this.state.popupOpen, showRightContent: false });
	};

	searchHandler = (event) => {
		let searchList = event.target.value.toLowerCase();
		let displayedContacts = CONTACTS.filter((el) => {
			let searchValue = el.name.toLowerCase();
			let searchLoaction = el.address.toLowerCase();
			return searchValue.startsWith(searchList.toLowerCase()) ||
				searchValue.toLowerCase().includes(" " + searchList.toLowerCase()) ||
				searchLoaction.startsWith(searchList.toLowerCase()) ||
				searchLoaction.toLowerCase().includes(" " + searchList.toLowerCase());
		});
		/*this.setState({ displayedContacts: displayedContacts, currentSearchValue: event.target.value });*/
		this.setState({ displayedContacts: displayedContacts });

		this.closePopupWindow();
	}

	rightLocationClickHandler = (item) => {
		const { dispatch, bookAppointmentReducer } = this.props;
		let availableBranchObj = bookAppointmentReducer.availableLocations.content;

		let searchValue = item.branchId.toLowerCase();
		let filterItem = availableBranchObj.filter((itemData) => {
			return searchValue === itemData.branchId
		});
		dispatch(rightLocationClick(searchValue, filterItem));
		this.closePopupWindow();
	}


	handleDropdownBlur(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(false, field));
		this.closePopupWindow();
	}

	handleDropdownFocus(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(true, field));
		this.closePopupWindow();
	}

	handleDropdownClick(data, field) {
		const { dispatch, bookAppointmentReducer } = this.props;
		let availableBranchObj = bookAppointmentReducer.availableLocations.content;

		if (bookAppointmentReducer[field].value === data.value) {
			return;
		}

		if (field === "bookingBranch") {
			let filterItem = availableBranchObj.filter((item) => {
				return data.value === item.branchId
			});

			dispatch(selectDropdownItem(data.value, data.description, field, filterItem));

			availableTimesForDate = filterItem[0]['availableTimeSlots'].filter((el) => {
				return el.split('T')[0] === moment(new Date(filterItem[0]['availableTimeSlots'][0])).format('YYYY-MM-DD');
			});

			this.setState({
				startDate: new Date(filterItem[0]['availableTimeSlots'][0]),
				availableTimes: availableTimesForDate
			});
			dispatch(setDateTimeValue(filterItem[0]['availableTimeSlots'][0]));
		}
		else {
			dispatch(selectDropdownItem(data.value, data.description, field));
		}

		this.closePopupWindow();
	}


	handleOnSearchChange(e, field) {
		const { dispatch } = this.props;
		const value = e.target.value;
		dispatch(changeSearchInputValue(value, field));
	}

	handleOnClickNav(item) {
		const { dispatch, bookAppointmentReducer } = this.props;
		const { bookingBranch } = bookAppointmentReducer;
		if (item === '0') {
			dispatch(bookAppointmentSelectNavigation("onClickBranch"))
		}

		if (item === '1') {
			dispatch(bookAppointmentSelectNavigation("onClickDate"))
		}
	}


	renderSearchContent() {
		const { commonReducer, bookAppointmentReducer } = this.props;
		const bookAppointmentValue = commonReducer.appData.bookAppointment;
		const labels = bookAppointmentValue.labels;

		branchesAvailable = false;

		if (this.state.displayedContacts === null) {
			return null
		}

		return (
			<div>
				<div className="remodal-overlay remodal-is-opened"
					style={{ display: this.state.popupOpen ? "block" : "none" }} />
				<div className=" remodal-is-opened" style={{ display: "flex" }}>
					<div className="appointmentMainPopup">
						<div>
							<div style={{
								width: '100%',
								position: 'relative',
								display: 'flex',
								alignItems: 'center',
								minHeight: '200px',
								textAlign: 'center',
								border: '1px solid #d0d3d6'
							}}>
								<div style={{ margin: '0 auto' }}>This page didn’t load google maps correctly</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		)
	}

	render() {
		const { commonReducer, bookAppointmentReducer } = this.props;
		const { bookingBranch, bookingTimeSlots, availableDateArray, onClickDate, onClickBranch, itemActive } = bookAppointmentReducer;
		const errorMsgList = commonReducer.appData.errorMsgs;

		const bookAppointmentValue = commonReducer.appData.bookAppointment;
		const labels = bookAppointmentValue.labels;

		let bookingBranchList = {};
		let bookingTimeSlotsList = {};

		if (CONTACTS !== null) {
			(CONTACTS).map((item, index) => {
				if (item.type === "Branch") {
					bookingBranchList[(item.branchId).toLowerCase()] = item.name.split("-")[1];
				}
			});
		}

		if (bookAppointmentReducer.selectedObject !== null && bookAppointmentReducer.selectedObject !== undefined && bookAppointmentReducer.selectedObject[0] !== undefined) {
			(bookAppointmentReducer.selectedObject[0]['availableTimeSlots']).map((item, index) => {
				bookingTimeSlotsList[index] = item;
			});
		}

		return (
			<div>
				<NavigationBar
					items={bookAppointmentValue.navigationBarItmes}
					onClick={(item) => this.handleOnClickNav(item)}
					isActive={itemActive}
				/>
				{onClickDate &&
					<div className="bookAppointment-m">
						<p className="uob-headline-m">{bookAppointmentValue.subtitle1}</p>
						<div className="bookAppointment-table">
							<div className="positionRelative bookAppointment-table-left">
								<Dropdown
									inputID="bookingBranch"
									label={labels.bookingBranch}
									dropdownItems={mapValueToDescription(
										bookingBranchList
									)}
									isFocus={bookingBranch.isFocus}
									value={bookingBranch.value}
									isValid={bookingBranch.isValid}
									errorMsg={bookingBranch.errorMsg}
									errorMsgList={errorMsgList}
									focusOutItem={true}
									validator={["required"]}
									searchValue={bookingBranch.searchValue}
									onBlur={this.handleDropdownBlur.bind(this, "bookingBranch")}
									onFocus={this.handleDropdownFocus.bind(this, "bookingBranch")}
									onClick={data => this.handleDropdownClick(data, "bookingBranch")}
									onSearchChange={event =>
										this.handleOnSearchChange(event, "bookingBranch")
									}
								/>
							</div>
							<div className="bookAppointment-table-right positionRelative">
								<div className="positionRelative">
									<div>
										<div className="textInputContainer">
											<div className="textInputContent">
												<label className="textInputLabel--focused">Select Time</label>
												<input type="text" style={{ color: this.state.startDate === null ? '#b1b1b1' : '' }}
													value={this.state.startDate !== null ? moment(this.state.startDate).format('DD/MM/YYYY - hh:mm A') : 'Select UOB Branch first'}
													className="textInput--focused" readOnly={true} />
												<DatePicker
													selected={this.state.startDate}
													onChange={this.handleChange}
													/*onSelect={this.handleSelect}*/
													showTimeSelect
													disabled={bookAppointmentReducer.bookingBranch.value === ""}
													includeDates={availableDateArray}
													includeTimes={this.state.availableTimes}
													dateFormat="d/MM/yyyy - h:mm aa"
													className="dateTimeInputBox"
												/>
											</div>
										</div>
										{this.state.startDate !== null && <div className="bottomLine" />}
										<div className="fullErrorContent" />
									</div>
								</div>
								<div style={{ position: 'absolute', right: '0', top: '0' }}>
									{this.state.startDate !== "" && this.state.startDate !== null ?
										<img
											className='dropdown--inputArrow'
											src={arrowDownImg}
											alt='dropdown-arrow'
										/>
										: <img
											className='dropdown--inputArrow'
											src={arrowLightDownImg}
											alt='dropdown-arrow'
										/>
									}

								</div>
							</div>
						</div>
					</div>
				}

				{onClickBranch &&
					<div>
						{this.renderSearchContent()}
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = state => {
	const { commonReducer, applicationReducer, bookAppointmentReducer } = state;
	return { commonReducer, applicationReducer, bookAppointmentReducer };
};

export default connect(mapStateToProps)(BookAppointmentSearchNoMap);
