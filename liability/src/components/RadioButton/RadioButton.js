import React from 'react';
import PropTypes from 'prop-types';
import './RadioButton.css';

const renderRadioInput = (radioObj, onClick, isDisabled, value, inputID) => {

  return radioObj.map((item, i) => {
    const isChecked = value === item.value;
    const checkAndDisabled = isChecked && isDisabled ? "disabledCheckRadio" : '';
    const notCheckedDisabled = !isChecked && isDisabled ? "disabledText" : '';
    const cssTax = inputID.includes("reasons") ? "adjust-top" : "";
    return (
      <label key={i} className="radio-container" >
        <input type="radio" name={`${inputID}-radio`} className="radioButton"
          onClick={() => onClick(item)}
          checked={isChecked}
          disabled={isDisabled}
        />
        <span className={`radio-description ${notCheckedDisabled}`} dangerouslySetInnerHTML={{ __html: item.description }}></span>
        <span className={`${cssTax} radio-checkmark ${checkAndDisabled}`}></span>
      </label>
    )
  })
}
const RadioButton = props => {
  const { onClick, errorMsg, isValid, isDisabled, inputID, radioObj, value } = props;
  return (
    <div className={`radio-toggle-input--container `}>
      {renderRadioInput(radioObj, (item) => onClick && onClick(item), isDisabled, value, inputID)}
      {!isValid &&
        <div className="radio-errorMsg">
          {errorMsg}
        </div>
      }
    </div>
  );
}

RadioButton.propTypes = {
  onClick: PropTypes.func
};

RadioButton.defaultProps = {
  isValid: true,
  errorMsg: '',
};

export default RadioButton;
