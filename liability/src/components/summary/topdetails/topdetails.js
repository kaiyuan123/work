import React from 'react';
import PropTypes from 'prop-types';

import './topdetails.css';
import carImg from './../../assets/images/loading_icon.svg';
const Topdetails = props => {
  const { brandModel, brandMake, monthlyPayment, loanAmount} = props;
  return (
    <div className = 'Top_Details'>
      <div className = 'Top_Details_left'>
        <img src=""/>
        <p> {brandMake} {brandModel} </p>
      </div>
      <div className = 'Top_Details_right'>
        <h3> Monthly Payment </h3>
        <p>{monthlyPayment}</p>
        <h3> Loan Amount </h3>
        <p>{loanAmount}</p>
      </div>
    </div>
  );
};


export default Topdetails;
