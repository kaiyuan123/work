import React from 'react';

import './tableDetails.css';


const TableDetails = props => {
  const { monthlyPaymentlabels, monthlyPaymentvalue , loanAmountlabels, loanAmountvalue, purchasePricelabels, purchasePricevalue, downpaymentlabels, downpaymentvalue, loanTenurelabels, loanTenurevalue, interestRatelabels, interestRatevalue, effectiveinterestRatelabels,effectiveinterestRatevalue } = props;
  return (
      <div className="OverviewtableDetails">
          <div className="overview_Seperator">
              <div className="overviewLabels"> {monthlyPaymentlabels} </div>
              <div className="overviewValue"> {monthlyPaymentvalue} </div>
          </div>
          <div className="overview_Seperator">
              <div className="overviewLabels"> {loanAmountlabels} </div>
              <div className="overviewValue"> {loanAmountvalue} </div>
          </div>
          <div className="tableDetails_Seperator">
              <div className="tableDetailsLabels"> {purchasePricelabels} </div>
              <div className="tableDetailsValue"> {purchasePricevalue} </div>
          </div>
          <div className="tableDetails_Seperator">
              <div className="tableDetailsLabels"> {downpaymentlabels}</div>
              <div className="tableDetailsValue"> {downpaymentvalue}</div>
          </div>
          <div className="tableDetails_Seperator">
              <div className="tableDetailsLabels"> {loanTenurelabels}</div>
              <div className="tableDetailsValue"> {loanTenurevalue}</div>
          </div>
          <div className="tableDetails_Seperator">
              <div className="tableDetailsLabels"> {interestRatelabels} </div>
              <div className="tableDetailsValue"> {interestRatevalue} </div>
          </div>
          <div className="tableDetails_Seperator">
              <div className="tableDetailsLabels"> {effectiveinterestRatelabels} </div>
              <div className="tableDetailsValue"> {effectiveinterestRatevalue} </div>
          </div>
    </div>
  );
};


export default TableDetails;
