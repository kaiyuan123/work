import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import deleteIcon from "./../../assets/images/delete_icon.svg";

import './FileUpload.css';

import { bytesToSize, fileUploadHelper } from './../../common/utils';

const progressBarMaxWidth = 517;

const FileUpload = props => {
	const {
		inputID,
		isValid,
		fileSize,
		inputValue,
		progressValue,
		identifier,
		docType,
		documentTitleName,
		updateUploadProgress,
		handleUploadFile,
		handleShowErrorMsg,
		handleShowUploadErrorMsg,
		handleDeleteImage,
		sampleGuide
	} = props;
	const showErrorLabel = isValid ? "" : "file-upload--error-drag";
	const fileSizeString = fileSize ? fileSize : "";
	const descriptionContent = isValid && inputValue ? inputValue.replace(/^.*[\\]/, "") : "";
	const inputProgressValue = progressValue ? progressValue : 0;
	let fileInputRefDrag = null;

	const checkFileExtension = (file, allowedExtensions) => allowedExtensions.includes(file.split(".").pop());

	const handleDropFileOnChange = () => {
		const file = fileInputRefDrag.files[0];
		if (file.name !== null && file.name !== undefined) {
			const extensionIsAllowed = checkFileExtension(file.name, ["jpg", "jpeg", "png", "pdf", "JPG", "JPEG", "PNG", "PDF"]);
			const actions = {
				successDispatch: () => ({}),
				errorDispatch: () => {
					handleShowUploadErrorMsg(inputID, 'File upload fail. Please try again later.');
				},
				progressDispatch: progress => updateUploadProgress(inputID, progress)
			};
			if (extensionIsAllowed) {
				if (file.size <= 4194304) {
					fileUploadHelper(file, inputID, identifier, docType, actions);
					handleUploadFile(inputID, file.name, bytesToSize(file.size), []);
				} else {
					handleShowErrorMsg(inputID, "File size cannot exceed 3MB");
				}
			} else {
				handleShowErrorMsg(inputID, "Can only be either jpg, png or pdf");
			}
		}

	};

	const handleDrop = acceptedFiles => {
		const file = acceptedFiles[0];
		const extensionIsAllowed = checkFileExtension(file.name, ["jpg", "jpeg", "png", "pdf", "JPG", "JPEG", "PNG", "PDF"]);
		const actions = {
			successDispatch: () => ({}),
			errorDispatch: () => {
				handleShowUploadErrorMsg(inputID, 'File upload fail. Please try again later.');
			},
			progressDispatch: progress => updateUploadProgress(inputID, progress)
		};
		if (extensionIsAllowed) {
			if (file.size <= 4194304) {
				fileUploadHelper(file, inputID, identifier, docType, actions);
				handleUploadFile(inputID, file.name, bytesToSize(file.size), []);
			} else {
				handleShowErrorMsg(inputID, "File size cannot exceed 3MB");
			}
		} else {
			handleShowErrorMsg(inputID, "Can only be either jpg, png or pdf");
		}
	};

	const dropUploadContainer = () => {
		return (
			<Dropzone
				onDrop={handleDrop}
				disableClick
				multiple={false}
				className="file-upload-container-drag"
			>
				<div className="file-upload-wrapper"
					/*data-text={'Drag file here or Browse'}*/>
					<div className="uploadInputSection">
						<div>+</div>
						<div>{isValid ? 'Browse or drop your file here' : 'Please upload document'}</div>
					</div>
					<input
						accept='.jpg,.JPG,.jpeg,.JPEG,.png,.pdf,.PDF,.PNG'
						ref={i => {
							fileInputRefDrag = i;
						}}
						type='file'
						className='file-upload-input-drag'
						onClick={() => {
							fileInputRefDrag.value = null;
							updateUploadProgress(inputID, 0);
						}}
						onChange={handleDropFileOnChange}
					/>
				</div>
			</Dropzone>
		);
	};

	return (
		<div>
			<div id={inputID} className={`file-upload--container ${showErrorLabel}`}>
				<table className="fileUploadTable">
					<tbody>
					<tr>
						<td style={{ width: '35%' }}>
							<div className="font-16 uploadSectionTitle">{documentTitleName}</div>
							<div className="upload-guide" dangerouslySetInnerHTML={{ __html: sampleGuide }}/>
						</td>
						<td style={{ width: '40%' }}>
							<div className="fileUploadSection">
								{parseInt(inputProgressValue, 10) === 0 && <div className="fileUploadDropperMain">
									{dropUploadContainer()}
								</div>}
								{parseInt(inputProgressValue, 10) !== 0 && <div className="fileUploadProgressMain">
									<div className="fileNameDescription">{descriptionContent}</div>
									{parseInt(inputProgressValue, 10) !== 100 && <div className='file-upload-progress-drag'
																																		style={{ width: ((progressBarMaxWidth * inputProgressValue) / 100) + 'px' }}/>}
								</div>}
							</div>

						</td>
						{parseInt(inputProgressValue, 10) === 100 &&
						<td style={{ width: '25%' }}>
							<div className="font-16 removeUploadedFile" onClick={() => handleDeleteImage(inputID)}>
								<img src={deleteIcon} className="partner-delete-icon" alt="delete"/>
							</div>
							<div className="font-14 file-size">{fileSizeString}</div>
						</td>
						}
						{
							// parseInt(inputProgressValue, 10) === 100 &&
							// <td style={{ width: '50px' }}>
							// 	<div className="font-14">{fileSizeString}</div>
							// </td>
						}
					</tr>
					</tbody>
				</table>
			</div>
		</div>

	);
};

FileUpload.propTypes = {
	inputID: PropTypes.string.isRequired,
	identifier: PropTypes.string.isRequired,
	docType: PropTypes.string.isRequired,
	documentTitleName: PropTypes.string.isRequired,
	updateUploadProgress: PropTypes.func,
	handleUploadFile: PropTypes.func,
	handleShowErrorMsg: PropTypes.func,
	handleShowUploadErrorMsg: PropTypes.func,
	handleDeleteImage: PropTypes.func
};


export default FileUpload;
