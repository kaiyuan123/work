import React from "react";
// import moment from "moment";
import NumberFormat from 'react-number-format';
// import deleteIcon from "./../../../assets/images/delete_icon.svg";
// import "./TableDesktop.css";
// const getValueByCode = (code, list) => {
//   const item = list.find(x => x.incomeType === code);
//   return item ? item.value : null;
// }

const generateTableRows = (tableContent, type, mapItems) => {
  return tableContent.map((row, i) => {
      const YearlyAssessableIncome = row.amount ? row.amount: '';
      const YearlyEmploymentIncome = row.employment ? row.employment  : '';
      const YearlyTradeIncome = row.trade ? row.trade : '';
      const YearlyRentalIncome = row.rent ? row.rent : '';
      const year = row.yearOfAssessment === null || row.yearOfAssessment === '' || row.yearOfAssessment === 'NA' || row.yearOfAssessment === 'null' ? '' : row.yearOfAssessment;
        return (
          <div key={i} className="income-breakdown-table--row-flex">
            <div className="income-breakdown-table--row-item">{year}</div>
            <div className="income-breakdown-table--row-item">
              <NumberFormat
                value={YearlyAssessableIncome}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'$'}
              />
            </div>
            <div className="income-breakdown-table--row-item">
              <NumberFormat value={YearlyTradeIncome}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'$'}
              />
            </div>
            <div className="income-breakdown-table--row-item">
              <NumberFormat value={YearlyEmploymentIncome}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'$'}
              />
            </div>
            <div className="income-breakdown-table--row-item">
              <NumberFormat value={YearlyRentalIncome}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'$'}
              />
            </div>
          </div>
        )
  });
}

const TableView = props => {
  return (
    <div className="income-breakdown-table--main-container-flex new-table">
      <div className="income-breakdown-table--titles-container-flex">
        {props.colTitles.map((title, i) => (
          <div key={i} className="income-breakdown-table--title-flex">
            {title}
          </div>
        ))}
      </div>
      <div className="income-breakdown-table--rows-container-flex">
        {generateTableRows(props.tableContent, props.tableType, props.mapItems)}
      </div>
    </div>
  );
};

export default TableView;
