import React, { Component } from "react";
import { connect } from "react-redux";
import Dropzone from 'react-dropzone'
import ReactAvatarEditor from './ReactAvatarEditor'
import SignaturePad from 'react-signature-canvas';
import "./MobileSignature.css";
import { changeProgressValue, removeErrorMessage, showErrorMessage, setPopupStatus, setSignatureTabStatus, setUploadSignatureImage } from "./../../actions/signatureAction";
import { clearSignature, saveSignature } from "./../../actions/drawSignatureAction";
import { saveUploadDocument } from "./../../actions/applicationAction";

import closeIcon from "./../../assets/images/cross-grey.svg";
// import zoomInBlue from "./../../assets/images/signatureIcons/normalImage/zoomIn.svg";
// import zoomOutBlue from "./../../assets/images/signatureIcons/normalImage/zoomOut.svg";
// import rotateLeftBlue from "./../../assets/images/signatureIcons/normalImage/rotateLeft.svg";
// import rotateRightBlue from "./../../assets/images/signatureIcons/normalImage/rotateRight.svg";
import checkMark from "./../../assets/images/checkMark.svg";

// import zoomInRed from "./../../assets/images/signatureIcons/normalImage/zoomInError.svg";
// import zoomOutRed from "./../../assets/images/signatureIcons/normalImage/zoomOutError.svg";
// import rotateLeftRed from "./../../assets/images/signatureIcons/normalImage/rotateLeftError.svg";
// import rotateRightRed from "./../../assets/images/signatureIcons/normalImage/rotateRightError.svg";

let contentWidth = window.innerWidth - 112;
let contentHeight = 360;
const positionDataX = 0.5;
const positionDataY = 0.5;
const positionMove = 0.02;
const positionMargin = 0.01;


export class MobileSignature extends Component {
	state = {
		image: '',
		saveImage: '',
		allowZoomOut: false,
		position: { x: positionDataX, y: positionDataY },
		scale: 1,
		rotate: 0,
		borderRadius: 0,
		preview: null,
		imageWidth: 0,
		imageHeight: 0,
		originalWidth: 0,
		originalHeight: 0,
		width: contentWidth,
		height: contentHeight,
		changeDimensions: false,
		inputProgressValue: 0,
		imageLoadToCanvas:false,
		browseText: this.props.commonReducer.appData.signatureUpload.labels.button1,
		popupOpen: false,
		showReviewMobile: false,
		isSign: true,
		drawSignatureEnded: false,
		pressSave: false,
		drawSignature: null,
		saveDraw:null
	};

	constructor(props) {
		super(props);
		this.sigPad = null;
	}

	clear(onClear){
		this.sigPad.clear()
		onClear && onClear();
	}

	handlePopupEvent() {
		const {dispatch, signatureReducer} = this.props;
		if ((signatureReducer.saveImage !== "" && signatureReducer.saveImage !== null && signatureReducer.saveImage !== undefined) && !signatureReducer.saveThruIsSign) {
			dispatch(setUploadSignatureImage(signatureReducer.saveImage));
			dispatch(setSignatureTabStatus(false));
			this.setState({
				position: signatureReducer.position,
			})
		} else if ((signatureReducer.drawSignature !== "" && signatureReducer.drawSignature !== null && signatureReducer.drawSignature !== undefined) && signatureReducer.saveThruIsSign) {
			dispatch(setSignatureTabStatus(true));
			this.setState({
				drawSignature: signatureReducer.drawSignature,
				saveDraw: signatureReducer.drawSignature,
				drawSignatureEnded: true
			})
		}

		if (signatureReducer.isSign && (signatureReducer.drawSignature === "" || signatureReducer.drawSignature === null || signatureReducer.drawSignature === undefined)) {
			this.clear(this.clearSignature())
		} else if (!signatureReducer.isSign && (signatureReducer.saveImage === "" || signatureReducer.saveImage === null || signatureReducer.saveImage === undefined)) {
			dispatch(setUploadSignatureImage(''));
		}
		dispatch(setPopupStatus(!signatureReducer.popupOpen));
	}

	handleResize = () => {
		// contentWidth = (window.innerWidth < 992) ? (window.innerWidth < 768) ? window.innerWidth - 152 : window.innerWidth - 192 : (window.innerWidth - 360 < 920) ? window.innerWidth - 440 : 920;
		contentWidth = window.innerWidth;
		contentHeight = window.innerWidth;
		this.setState({
			scale: 1,
			rotate: 0,
			width: contentWidth,
			height: contentHeight
		});
	}

	componentDidMount() {
		this.handleResize();
		window.addEventListener('resize', this.handleResize)
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.handleResize)
	}

	checkFileExtension = (file, allowedExtensions) => allowedExtensions.includes(file.split(".").pop());

	handleNewImage = e => {
		const { dispatch, commonReducer } = this.props;
		const signatureUpload = commonReducer.appData.signatureUpload;
		const labels = signatureUpload.labels;

		dispatch(changeProgressValue(0));
		this.setState({
			imageLoadToCanvas: false
		});

		if (e.target.files.length > 0) {
			this.setState({
				position: { x: positionDataX, y: positionDataY },
				scale: 1,
				rotate: 0,
				width: contentWidth,
				height: contentHeight,
				changeDimensions: false
			});
			let _this = this;
			if (e.target.files && e.target.files[0]) {
				let reader = new FileReader();
				reader.onload = function (e) {
					let img = new Image();
					img.onload = function () {
						_this.setState({
							imageWidth: contentWidth,
							imageHeight: (img.height / img.width) * contentWidth,
							originalWidth: img.width,
							originalHeight: img.height,
						});
					};
					img.src = reader.result;
				};
				reader.readAsDataURL(e.target.files[0]);
			}


			let file = e.target.files[0];
			const extensionIsAllowed = this.checkFileExtension(file.name, ["jpg", "png", "jpeg", "JPG", "PNG", "JPEG"]);
			if (!extensionIsAllowed) {
				dispatch(showErrorMessage(labels.error1));
				dispatch(setUploadSignatureImage(''));
				return this.setState({ image: '', preview: null })
			}
			else if (file.size > 1048576) {
				dispatch(showErrorMessage(labels.error2));
				dispatch(setUploadSignatureImage(''));
				return this.setState({ image: '', preview: null })
			}
			else {
				dispatch(showErrorMessage(""));
				dispatch(setUploadSignatureImage(e.target.files[0]));
				return this.setState({ image: e.target.files[0], browseText: labels.button2, preview: null,imageLoadToCanvas: true })
			}
		}
		else {
			// dispatch(showErrorMessage(labels.error4));
			// return this.setState({ image: '', preview: null })
		}

		this.setState({
			pressSave: false
		})
	}

	handleDrop = acceptedFiles => {

		const { dispatch, commonReducer } = this.props;
		const signatureUpload = commonReducer.appData.signatureUpload;
		const labels = signatureUpload.labels;

		dispatch(changeProgressValue(0));
		this.setState({
			imageLoadToCanvas: false
		});

		if (acceptedFiles.length > 0) {
			this.setState({
				position: { x: positionDataX, y: positionDataY },
				scale: 1,
				rotate: 0,
				width: contentWidth,
				height: contentHeight,
				changeDimensions: false
			});

			let _this = this;
			if (acceptedFiles[0]) {
				let reader = new FileReader();
				reader.onload = function (e) {
					let img = new Image();
					img.onload = function () {
						_this.setState({
							imageWidth: contentWidth,
							imageHeight: (img.height / img.width) * contentWidth,
							originalWidth: img.width,
							originalHeight: img.height,
						});
					};
					img.src = reader.result;
				};
				reader.readAsDataURL(acceptedFiles[0]);
			}

			let file = acceptedFiles[0];
			const extensionIsAllowed = this.checkFileExtension(file.name, ["jpg", "png", "jpeg", "JPG", "PNG", "JPEG"]);
			if (!extensionIsAllowed) {
				dispatch(showErrorMessage(labels.error1));
				return this.setState({ image: '', preview: null })
			}
			else if (file.size > 1000000) {
				dispatch(showErrorMessage(labels.error2));
				return this.setState({ image: '', preview: null })
			}
			else {
				// dispatch(showErrorMessage(labels.error3));
				return this.setState({ image: acceptedFiles[0], browseText: labels.button2, preview: null,imageLoadToCanvas: true })
			}
		}
		else {
			// dispatch(showErrorMessage(labels.error4));
			// return this.setState({ image: '', preview: null })
		}
	}

	getXScale() {
		const canvasAspect = this.state.width / this.state.height
		const imageAspect = this.state.imageWidth / this.state.imageHeight

		return Math.min(1, canvasAspect / imageAspect)
	}

	getYScale() {
		const canvasAspect = this.state.height / this.state.width;
		const imageAspect = this.state.imageHeight / this.state.imageWidth

		return Math.min(1, canvasAspect / imageAspect)
	}

	handleSave = data => {
		const { dispatch, applicationReducer } = this.props;
		const { initiateMyinfoData } = applicationReducer;
		const applicationID = initiateMyinfoData !== null ? initiateMyinfoData.id : null;
		const img = this.editor.getImageScaledToCanvas().toDataURL();
		const rect = this.editor.getCroppingRect();
		const avatarImage = this.editor.getImage();
		const MAX_QUALITY_IMAGE = 0.9;
		const TYPE_IMAGE = "image/jpeg";

		// const saveImg = this.editor.getImage();
		// saveImg.toBlob(function (blob) {
		// });
		dispatch(removeErrorMessage());

		this.setState({
			preview: {
				img,
				rect,
				scale: this.state.scale,
				width: this.state.width,
				height: this.state.height,
				borderRadius: this.state.borderRadius,
			},
		});

		if (!HTMLCanvasElement.prototype.toBlob) {
			Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
				value: function (callback, type, quality) {
					let canvas = this;
					setTimeout(function () {
						let binStr = atob(canvas.toDataURL(type, quality).split(',')[1]),
							len = binStr.length,
							arr = new Uint8Array(len);

						for (let i = 0; i < len; i++) {
							arr[i] = binStr.charCodeAt(i);
						}
						callback(new Blob([arr], { type: type || 'image/png' }));
					});
				}
			});
		}
		const position = this.state.position;
		avatarImage.toBlob(function (blob) {
				const formData = new window.FormData();
				formData.append("file", blob, "signature.png");
				formData.append("documentType", "signature");
				dispatch(saveUploadDocument(formData, applicationID, img, position));
			},
			TYPE_IMAGE,
			MAX_QUALITY_IMAGE
		);
	}

	handleScale = (name, value) => {
		let newValue;
		let scale;
		if (name === "zoomIn") {
			newValue = parseFloat(value) + 0.1
		}
		if (name === "zoomOut") {
			newValue = parseFloat(value) - 0.1
		}
		if (newValue < 1) {
			scale = parseFloat(1)
		}
		else if (newValue <= 2) {
			scale = newValue < 1 ? parseFloat(newValue) : parseFloat(newValue)
		}
		else {
			scale = parseFloat(2)
		}
		this.setState({ scale })
	}

	rotateLeft = e => {
		e.preventDefault();
		let newRotation = parseInt(this.state.rotate, 10) === 360 || parseInt(this.state.rotate, 10) === -360 ? -90 : parseInt(this.state.rotate, 10) - 90;

		this.setState({
			rotate: newRotation,
		});
		if (newRotation === -270 || newRotation === -90 ||
			newRotation === 270 || newRotation === 90) {
			this.setState({
				changeDimensions: true,
				imageWidth: contentWidth,
				imageHeight: (this.state.originalWidth / this.state.originalHeight) * contentWidth
			})
		}
		else {
			this.setState({
				changeDimensions: false,
				imageWidth: contentWidth,
				imageHeight: (this.state.originalHeight / this.state.originalWidth) * contentWidth
			})
		}
	}

	rotateRight = e => {
		e.preventDefault();
		let newRotation = parseInt(this.state.rotate, 10) === 360 || parseInt(this.state.rotate, 10) === -360 ? 90 : parseInt(this.state.rotate, 10) + 90;
		this.setState({
			rotate: newRotation,
		});
		if (newRotation === -270 || newRotation === -90 ||
			newRotation === 270 || newRotation === 90) {
			this.setState({
				changeDimensions: true,
				imageWidth: contentWidth,
				imageHeight: (this.state.originalWidth / this.state.originalHeight) * contentWidth
			})
		}
		else {
			this.setState({
				changeDimensions: false,
				imageWidth: contentWidth,
				imageHeight: (this.state.originalHeight / this.state.originalWidth) * contentWidth
			})
		}

	};

	logCallback(e) {
		// eslint-disable-next-line
	}

	setEditorRef = editor => {
		if (editor) this.editor = editor
	}

	handlePositionChange = position => {
		this.setState({ position })
	}

	handleMoveUpDown = (name, value) => {
		let yMin = ((positionDataY * this.getYScale()) / this.state.scale) - positionMargin;
		let yMax = (1 + positionMargin) - yMin;

		let newRotation = this.state.rotate;

		if (value.y >= yMin && value.y <= yMax) {
			if(name === "moveUp"){
				if (newRotation === -90 || newRotation === 270) {
					this.setState({ position: { y: value.y, x: (value.x - positionMove) < yMin ? yMin : value.x - positionMove } })
				}
				else if (newRotation === -180 || newRotation === 180) {
					this.setState({ position: { x: value.x, y: (value.y - positionMove) < yMin ? yMin : value.y - positionMove } })
				}
				else if (newRotation === -270 || newRotation === 90) {
					this.setState({ position: { y: value.y, x: (value.x + positionMove) > yMax ? yMax : value.x + positionMove } })
				}
				else {
					this.setState({ position: { x: value.x, y: (value.y + positionMove) > yMax ? yMax : value.y + positionMove } })
				}
			}
			if(name === "moveDown"){
				if (newRotation === -90 || newRotation === 270) {
					this.setState({ position: { y: value.y, x: (value.x + positionMove) > yMax ? yMax : value.x + positionMove } })
				}
				else if (newRotation === -180 || newRotation === 180) {
					this.setState({ position: { x: value.x, y: (value.y + positionMove) > yMax ? yMax : value.y + positionMove } })
				}
				else if (newRotation === -270 || newRotation === 90) {
					this.setState({ position: { y: value.y, x: (value.x - positionMove) < yMin ? yMin : value.x - positionMove } })				}
				else {
					this.setState({ position: { x: value.x, y: (value.y - positionMove) < yMin ? yMin : value.y - positionMove } })
				}
			}
		}
	}

	handleMoveLeftRight = (name, value) => {
		let xMin = ((positionDataX * this.getXScale()) / this.state.scale) - positionMargin;
		let xMax = (1 + positionMargin) - xMin;

		let newRotation = this.state.rotate;

		if (value.x >= xMin && value.x <= xMax) {
			if(name === "moveLeft"){
				if (newRotation === -90 || newRotation === 270) {
					this.setState({ position: { x: value.x, y: (value.y + positionMove) > xMax ? xMax : value.y + positionMove } })
				}
				else if (newRotation === -180 || newRotation === 180) {
					this.setState({ position: { y: value.y, x: (value.x - positionMove) < xMin ? xMin : value.x - positionMove } })
				}
				else if (newRotation === -270 || newRotation === 90) {
					this.setState({ position: { x: value.x, y: (value.y - positionMove) < xMin ? xMin : value.y - positionMove } })
				}
				else {
					this.setState({ position: { y: value.y, x: (value.x + positionMove) > xMax ? xMax : value.x + positionMove } })
				}
			}
			if(name === "moveRight"){
				if (newRotation === -90 || newRotation === 270) {
					this.setState({ position: { x: value.x, y: (value.y - positionMove) < xMin ? xMin : value.y - positionMove } })
				}
				else if (newRotation === -180 || newRotation === 180) {
					this.setState({ position: { y: value.y, x: (value.x + positionMove) > xMax ? xMax : value.x + positionMove } })
				}
				else if (newRotation === -270 || newRotation === 90) {
					this.setState({ position: { x: value.x, y: (value.y + positionMove) > xMax ? xMax : value.y + positionMove } })
				}
				else {
					this.setState({ position: { y: value.y, x: (value.x - positionMove) < xMin ? xMin : value.x - positionMove } })
				}
			}
		}
	}

	getImageData(trimData) {
		const { dispatch, applicationReducer } = this.props;
		const { initiateMyinfoData } = applicationReducer;
		const applicationID = initiateMyinfoData !== null ? initiateMyinfoData.id : null;
		const trimmedDataURL = trimData.getTrimmedCanvas().toDataURL('image/png');

		const avatarImage = trimData.getTrimmedCanvas();
		const MAX_QUALITY_IMAGE = 0.9;
		const TYPE_IMAGE = "image/jpeg";

		dispatch(saveSignature(trimmedDataURL));

		if (!HTMLCanvasElement.prototype.toBlob) {
			Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
				value: function (callback, type, quality) {
					let canvas = this;
					setTimeout(function() {
						let binStr = atob( canvas.toDataURL(type, quality).split(',')[1] ),
							len = binStr.length,
							arr = new Uint8Array(len);

						for (let i = 0; i < len; i++ ) {
							arr[i] = binStr.charCodeAt(i);
						}
						callback( new Blob( [arr], {type: type || 'image/png'} ) );
					});
				}
			});
		}

		avatarImage.toBlob(function (blob) {
				const formData = new window.FormData();
				formData.append("file", blob, "signature.png");
				formData.append("documentType","signature");
				dispatch(saveUploadDocument(formData, applicationID, trimmedDataURL));
			},
			TYPE_IMAGE,
			MAX_QUALITY_IMAGE
		);

		this.setState({
			pressSave: true,
			drawSignature: trimmedDataURL
		})
	}


	clearSignature() {
		const { dispatch } = this.props;
		this.setState({
			drawSignatureEnded: false,
			drawSignature: null
		})
		dispatch(clearSignature());
	}

	onEnd() {
		this.setState({
			drawSignatureEnded: true
		})

		this.setState({
			pressSave: false
		})
	}

	onBegin() {
		this.setState({
			drawSignatureEnded: false
		})
	}

	renderDrawSignature() {
		const { commonReducer } = this.props;
		const label = commonReducer.appData.signaturePad;
		return (
			<div className="draw-signature-main-container" style={{ position: 'relative' }}>
				{commonReducer.isProcessing && <div className="fakeUploadLoader">
					<div className="blobs">
						<div className="blob-center"/>
						<div className="blob"/>
						<div className="blob"/>
						<div className="blob"/>
						<div className="blob"/>
						<div className="blob"/>
						<div className="blob"/>
					</div>
				</div>}

				<div className={`draw-signature-pad-container`} style={{width:`${window.innerWidth}px`, height: `${window.innerWidth}px`}}>
					<SignaturePad canvasProps={{height: window.innerWidth, width: window.innerWidth}} backgroundColor={'rgba(235,235,235,0.5)'}
												ref={(ref) => { this.sigPad = ref }} onEnd={() => this.onEnd()} onBegin={() => this.onBegin()}/>
				</div>

				{ this.state.drawSignature !== null &&
					<img className="draw-signature-image" src={this.state.drawSignature}/>
				}

				<div className="draw-signature-button-container">
					<button className="draw-signaturebutton" onClick={() => this.clear(this.clearSignature())}>
						{label.SignatureBrowserAgain}
					</button>
				</div>
			</div>

		)
	}

	renderSignatureUpload() {
		const { commonReducer, signatureReducer } = this.props;
		const signature = signatureReducer.signature;
		// const signatureInputProgressValue = signatureReducer.inputProgressValue;
		// const signatureUpload = commonReducer.appData.signatureUpload;
		// const labels = signatureUpload.labels;

		const isErrorHide = ((this.state.image !== null && this.state.image !== undefined && this.state.image !== "") || signature.errorMsg === "");

		const mainContainerClass = isErrorHide ? 'signature--avatar-editor-frame-container' : 'signature--avatar-editor-frame-container-red';
		// const saveButtonClass = this.state.image === null || this.state.image === undefined || this.state.image === "" ? signature.errorMsg !== "" ? 'signature--right-side-button-red' : 'signature--right-side-button-disabled' : signature.errorMsg !== "" ? 'signature--right-side-button button--red' : 'signature--right-side-button';
		// const signatureUploadMiddleTextClass = isErrorHide ? 'signatureUploadMiddleText' : 'signatureUploadMiddleText MiddleTextRed';
		return (
			<div>
				<div style={{ position: 'relative' }}>
					{commonReducer.isProcessing && <div className="fakeUploadLoader">
						<div className="blobs">
							<div className="blob-center"/>
							<div className="blob"/>
							<div className="blob"/>
							<div className="blob"/>
							<div className="blob"/>
							<div className="blob"/>
							<div className="blob"/>
						</div>
					</div>}
					<div className={"signature--avatar-editor-main-container"}>
						<div id='avatar-editor-frame-div' className={mainContainerClass}>
							<Dropzone
								onDrop={this.handleDrop}
								disableClick
								multiple={false}
								style={{
									width: '100%',
									marginBottom: '0px',
									position: 'relative'
								}}
							>
								{!commonReducer.isProcessing && signatureReducer.image === "" &&
								<div>
									<div style={{ position: 'absolute', zIndex: 5, width: '100%', cursor: 'pointer' }}>
										<input style={{ width: '100%', height: this.state.height }}
													 name="newImage"
													 type='file'
													 className={"signature-input"}
													 onChange={this.handleNewImage}
										/>
									</div>
								</div>
								}
								{
									signatureReducer.signature.errorMsg !== "" &&
									<div className="signature-error-container">
										<div className="signature-error-label" style={{width:`${window.innerWidth}px`, height: `${window.innerWidth}px`}}>
											{signatureReducer.signature.errorMsg}
										</div>
									</div>
								}
								<ReactAvatarEditor
									ref={this.setEditorRef}
									scale={parseFloat(this.state.scale)}
									width={!this.state.changeDimensions ? this.state.width : this.state.height}
									height={!this.state.changeDimensions ? this.state.height : this.state.width}
									position={this.state.position}
									onPositionChange={this.handlePositionChange}
									rotate={parseFloat(this.state.rotate)}
									borderRadius={0}
									/*onLoadFailure={this.logCallback.bind(this, 'onLoadFailed')}
									onLoadSuccess={this.logCallback.bind(this, 'onLoadSuccess')}
									onImageReady={this.logCallback.bind(this, 'onImageReady')}*/
									image={signatureReducer.image}
								/>
							</Dropzone>
							<div>
							</div>
						</div>
					</div>
				</div>

			</div>
		)
	}

	handleBottomNav(status) {
		const { dispatch, signatureReducer } = this.props;
		if ( status !== signatureReducer.isSign) {
			dispatch(setSignatureTabStatus(status))
		}
	}

	render() {
		const { commonReducer, signatureReducer } = this.props;
		const labels = commonReducer.appData.signatureUpload;
		const width = window.innerWidth - 20;
		const showRedBorder = !signatureReducer.signatureIsValid;
    const agreementButtonClass = signatureReducer.saveSignatureSuccessful ? 'signatureButton signatureButton--selected':showRedBorder?'signatureButton signatureButton--error':'signatureButton';

		return (
			<div>
			<table width={'100%'}>
				<tbody>
				<tr>
					<td style={{ width: '60px', textAlign: 'center', cursor: 'pointer' }}
							onClick={() => this.handlePopupEvent()}>
						<div className={agreementButtonClass}>
							{signatureReducer.saveSignatureSuccessful && <img style={{marginLeft: '20px'}} src={checkMark} alt="check-Icon" width={43} height={35}/>}
							<div style={{flex: '1'}}> {labels.mobileTitle} </div>
						</div>
					</td>
				</tr>
				</tbody>
			</table>
			{ !signatureReducer.signatureIsValid  && <div className="errorMsg" style={{ textAlign: 'left',marginTop: '5px', paddingLeft: '0'}}>{labels.labels.error4}</div>}
			<div>
				<div className="remodal-overlay remodal-is-opened" style={{ display: signatureReducer.popupOpen ? "block" : "none" }}/>
					<div className="remodal-wrapper remodal-is-opened" style={{ display: signatureReducer.popupOpen  ? "flex" : "none" }}>
					{signatureReducer.popupOpen && <style dangerouslySetInnerHTML={{ __html: `html, body {overflow: hidden !important}` }} />}
						<div className="signatureMainPopup">
							<div className="signatureCloseMark" onClick={() => this.handlePopupEvent()}>
								<img className="close-icon" src={closeIcon} alt="Close-Icon"/>
							</div>
							<h1 style={{ padding: '10px', width: '80%' }} className="sectionTitle">{labels.mobileTitle}</h1>
							<p className="uob-headline">
								{labels.mobileSubtitle}
							</p>
							<div style={{width:`${window.innerWidth}px`, height: `${window.innerWidth}px`}}>
								{
									signatureReducer.isSign
										? this.renderDrawSignature()
										: this.renderSignatureUpload()
								}
							</div>
							<div className="bottom-navigation-container">
								<div className="bottom-navigation-label-container" onClick={() => this.handleBottomNav(true)}>
									<div className={`bottom-navigation-label${signatureReducer.isSign ? '--selected' : ""}`}>
										{labels.mobileNavSign}
									</div>
							  </div>
								<div className="bottom-navigation-label-container" onClick={() => this.handleBottomNav(false)}>
									<div className={`bottom-navigation-label${!signatureReducer.isSign ? '--selected' : ""}`}>
										{labels.mobileNavLibrary}
									</div>
									<input
										name="newImage"
										type='file'
										className={"mobile-signature-input"}
										onChange={this.handleNewImage}
									/>
								</div>
							</div>
							<div className="signatureButtonContainer" style={{width : `${width}px`}}>
							{signatureReducer.isSign ?
								<input type="button"
									className="signatureUploadButton"
									value={labels.labels.save}
									onClick={() => this.getImageData(this.sigPad)}
									disabled={!this.state.drawSignatureEnded}
								/>
							:
							<input type="button"
								className="signatureUploadButton"
								value={labels.labels.save}
								onClick={this.handleSave}
								disabled={this.state.image === null || this.state.image === undefined || this.state.image === ""}
							/>
							}
							</div>
						</div>
					</div>
				</div>
			</div>
		)

	}
}

const mapStateToProps = state => {
	const { commonReducer, signatureReducer, applicationReducer, drawSignatureReducer } = state;
	return { commonReducer, signatureReducer, applicationReducer, drawSignatureReducer };
};

export default connect(mapStateToProps)(MobileSignature);
