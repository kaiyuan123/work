import React, { Component } from "react";
// import PropTypes from 'prop-types';
import SignaturePad from 'react-signature-canvas';

import './DrawSignature.css'
class DrawSignature extends Component {
  constructor(props) {
    super(props);
    this.sigPad = {};
  }

  clear(onClear){
    this.sigPad.clear()
    onClear && onClear();
  }

  render () {
     const {onClear, onTrim, label } = this.props;
     // const hideSignaturePad = trimmedDataURL && isDisabled ? "displayNone" : "";

     return (
     <div className="signature-main-container">
      {/*{ trimmedDataURL && isDisabled ?
        <div className="signature-pad-img">
          <img src={trimmedDataURL}/>
        </div>
        : null
      }*/}

			 {/*<div className={`${hideSignaturePad} signature-pad-container`}>
        <SignaturePad canvasProps={{height: 174}}
          ref={(ref) => { this.sigPad = ref }} />
      </div>*/}

			 <div className={`signature-pad-container`}>
				 <SignaturePad canvasProps={{height: 174}} backgroundColor={'rgba(255,255,255,1)'}
											 ref={(ref) => { this.sigPad = ref }} />
			 </div>

       <div className="signature-button-container">
         <button className="signaturebutton" onClick={() => this.clear(onClear)}>
           {label.SignatureBrowserAgain}
         </button>
         <button className="signaturebutton" onClick={() => onTrim && onTrim(this.sigPad)}>
           {label.SignatureSave}
         </button>
       </div>
     </div>
    )
   }
}

export default DrawSignature;
