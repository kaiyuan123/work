import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { handleAddressInputChange, autoGetLocalAddress, setDropdownFocusStatus, selectDropdownItem, changeSearchInputValue } from './../../../actions/localAddressInputAction';
import { mapValueToDescription } from "./../../../common/utils";
import TextInput from './../../TextInput/TextInput';
import Dropdown from "./../../Dropdown/Dropdown";
import './LocalAddressInput.css';

class LocalAddressInput extends Component {

  handleOnChange(data, field) {
    const { dispatch, addressType } = this.props;
    dispatch(handleAddressInputChange(data, field));
    if (field === 'mailingPostalCode') {
      dispatch(autoGetLocalAddress(data.value, addressType, field));
    }
  }

  handleDropdownBlur(field) {
    const { dispatch } = this.props;
    dispatch(setDropdownFocusStatus(false, field));
  }

  handleDropdownFocus(field) {
    const { dispatch } = this.props;
    dispatch(setDropdownFocusStatus(true, field));
  }

  handleDropdownClick(data, field) {
    const { dispatch, localAddressInputReducer } = this.props;
    if (localAddressInputReducer[field].value === data.value) {
      return;
    }
    dispatch(selectDropdownItem(data.value, data.description, field));
  }

  handleOnSearchChange(e, field) {
    const { dispatch } = this.props;
    const value = e.target.value;
    dispatch(changeSearchInputValue(value, field));
  }

  render() {

    const { localAddressInputReducer, propertyType, addressType, labels, errorMsgList, inputValues } = this.props;

    const standard = localAddressInputReducer[`${addressType}Standard`];

    const street = localAddressInputReducer[`${addressType}Street`];
    const block = localAddressInputReducer[`${addressType}Block`];
    const postalCode = localAddressInputReducer[`${addressType}PostalCode`];
    const building = localAddressInputReducer[`${addressType}Building`];
    const level = localAddressInputReducer[`${addressType}Level`];
    const unit = localAddressInputReducer[`${addressType}Unit`];
    const country = localAddressInputReducer[`${addressType}Country`];

    const city = localAddressInputReducer[`${addressType}City`];
    const line1 = localAddressInputReducer[`${addressType}Line1`];
    const line2 = localAddressInputReducer[`${addressType}Line2`];
    const line3 = localAddressInputReducer[`${addressType}Line3`];
    const line4 = localAddressInputReducer[`${addressType}Line4`];

    
    const retrieveAddressType = localAddressInputReducer.retrieveAddressType;
    let retrieveType = false;
    if(retrieveAddressType.value === 'M' && country.value !== 'SG'){
      retrieveType = true;
    }else{
      retrieveType = false;
    }
    const isFormatted = standard.value === "D" || standard.value === "F";
    const withBuildingNameR = addressType === "residential" ? building && building.value !== '' ? "bottom-border" : "" : "";
    const withBuildingName = addressType === "company" || addressType === "mailing" ? building && building.value !== '' ? "" : "bottom-border" : "";
    const positionRelative = postalCode.isReadOnly ? "" : "positionRelative";
    const propertyTypeList = inputValues ? inputValues.propertyType : "";
    const countriesNamesMap = inputValues ? inputValues.countriesNamesMap : "";
    let bottomBorder = ""
    if (isFormatted && !retrieveType) {
      if (!(postalCode && (postalCode.autoFilled || postalCode.count >= 5))) {
        bottomBorder = "bottom-border";
      }
    };
    const positionRelativeOverseas = addressType !== "company" && addressType !== "residential" ? "positionRelative" : "";
    const bottomBorderOverseas = addressType === "company" ? "bottom-border" : "";

    return (
      <div>
        <div className="top-border">
          <div className={`fullTable confirmDetails-flexContainer ${bottomBorder}`}>
            <div className={`halfRow right-border ${positionRelative} dropdown-nopadding mob-border`}>
              <Dropdown
                inputID={`${addressType}Country`}
                isReadOnly={addressType !== "mailing" ? true : false}
                label={labels[`${addressType}Country`]}
                dropdownItems={mapValueToDescription(
                  countriesNamesMap
                )}
                isFocus={country.isFocus}
                value={country.value}
                isValid={country.isValid}
                errorMsg={country.errorMsg}
                focusOutItem={true}
                searchValue={country.searchValue}
                onBlur={this.handleDropdownBlur.bind(this, `${addressType}Country`)}
                onFocus={this.handleDropdownFocus.bind(this, `${addressType}Country`)}
                onClick={data => this.handleDropdownClick(data, `${addressType}Country`)}
                onSearchChange={event =>
                  this.handleOnSearchChange(event, `${addressType}Country`)
                }
                flagImg
              />
            </div>
            <div className={`halfRow ${positionRelative}`}>
              { !retrieveType && isFormatted &&
                <TextInput
                  type='Postal'
                  inputID={`${addressType}PostalCode`}
                  isValid={postalCode ? postalCode.isValid : true}
                  errorMsg={postalCode ? postalCode.errorMsg : ''}
                  label={labels[`${addressType}PostalCode`]}
                  value={postalCode ? postalCode.value : ''}
                  isLoading={postalCode ? postalCode.isLoading : false}
                  onChange={(data) => this.handleOnChange(data, `${addressType}PostalCode`)}
                  validator={["required", "isNumber", "exactSize|6"]}
                  isReadOnly={postalCode.isReadOnly}
                  hasIcon={false}
                  errorMsgList={errorMsgList}
                  isNumber
                />
              }

              {(!isFormatted || retrieveType) && addressType !== "company" && addressType !== "residential" &&
                <TextInput
                  inputID={`${addressType}City`}
                  label={labels[`${addressType}City`]}
                  value={city.value}
                  errorMsg={city.errorMsg}
                  onChange={data => this.handleOnChange(data, `${addressType}City`)}
                  isValid={city.isValid}
                  validator={["required", "maxSize|30"]}
                  errorMsgList={errorMsgList}
                  hasIcon={false}
                />
              }
            </div>
          </div>
        </div>
        {isFormatted && !retrieveType ?
          <div>
            {
              ((postalCode && (postalCode.autoFilled || postalCode.count >= 5))) &&
              <div>
                <div className="top-border">
                  <div className="fullTable confirmDetails-flexContainer">
                    <div className={`halfRow right-border ${positionRelative}`}>
                      <TextInput
                        inputID={`${addressType}Block`}
                        isValid={block ? block.isValid : true}
                        errorMsg={block ? block.errorMsg : ''}
                        label={propertyType && propertyType === 'LANDED' ? labels[`${addressType}HouseNo`] : labels[`${addressType}Block`]}
                        value={block ? block.value : ''}
                        isReadOnly={block ? block.isReadOnly : false}
                        onChange={(data) => this.handleOnChange(data, `${addressType}Block`)}
                        validator={["required", "isAlphanumeric", "maxSize|7"]}
                        hasIcon={false}
                        errorMsgList={errorMsgList}
                      />
                    </div>
                    <div className={`halfRow ${positionRelative}`}>
                      <TextInput
                        inputID={`${addressType}Street`}
                        isValid={street ? street.isValid : true}
                        errorMsg={street ? street.errorMsg : ''}
                        label={labels[`${addressType}Street`]}
                        value={street ? street.value : ''}
                        isReadOnly={street ? street.isReadOnly : false}
                        onChange={(data) => this.handleOnChange(data, `${addressType}Street`)}
                        validator={["required", "maxSize|30"]}
                        hasIcon={false}
                        errorMsgList={errorMsgList}
                      />
                    </div>
                  </div>
                </div>
              </div>
            }
            {((postalCode && (postalCode.autoFilled || postalCode.count >= 5))) &&
              <div>
                <div className={`top-border ${withBuildingName} ${withBuildingNameR}`}>
                  <div className="fullTable confirmDetails-flexContainer">
                    <div className={`halfRow ${positionRelative} right-border`}>
                      <TextInput
                        inputID={`${addressType}Level`}
                        isReadOnly={level.isReadOnly}
                        label={labels[`${addressType}Level`]}
                        value={level.value}
                        errorMsg={level.errorMsg}
                        onChange={(data) => this.handleOnChange(data, `${addressType}Level`)}
                        isValid={level.isValid}
                        validator={["required", "isAlphanumeric", "maxSize|4"]}
                        hasIcon={false}
                        errorMsgList={errorMsgList}
                      />
                    </div>
                    <div className={`halfRow ${positionRelative}`}>
                      <TextInput
                        inputID={`${addressType}Unit`}
                        isReadOnly={unit.isReadOnly}
                        label={labels[`${addressType}Unit`]}
                        value={unit.value}
                        errorMsg={unit.errorMsg}
                        onChange={(data) => this.handleOnChange(data, `${addressType}Unit`)}
                        isValid={unit.isValid}
                        validator={["required", "isAlphanumeric", "maxSize|7"]}
                        hasIcon={false}
                        errorMsgList={errorMsgList}
                      />
                    </div>
                  </div>
                </div>

                {(addressType !== "residential" && building && building.value !== '') &&
                  <div className="sub-div-common bottom-border">
                    <div className="p-l-10">
                      <TextInput
                        inputID={`${addressType}Building`}
                        label={labels[`${addressType}Building`]}
                        value={building.isReadOnly ? building.value : ''}
                        isReadOnly
                        hasIcon={false}
                        errorMsgList={errorMsgList}
                      />
                    </div>
                  </div>
                }

                {addressType === "residential" ?
                  <div>
                    {(building && building.value !== '') ?
                      <div className="bottom-border">
                        <div className="fullTable confirmDetails-flexContainer">
                          <div className="halfRow positionRelative right-border">
                            <TextInput
                              inputID={`${addressType}Building`}
                              label={labels[`${addressType}Building`]}
                              value={building.isReadOnly ? building.value : ''}
                              isReadOnly
                              hasIcon={false}
                              errorMsgList={errorMsgList}
                            />
                          </div>
                          <div className="halfRow positionRelative dropdown-nopadding">
                            <Dropdown
                              inputID="propertyType"
                              isReadOnly={true}
                              label={labels.propertyType}
                              dropdownItems={mapValueToDescription(
                                propertyTypeList
                              )}
                              isFocus={propertyType.isFocus}
                              value={propertyType}
                              isValid={propertyType.isValid}
                              errorMsg={propertyType.errorMsg}
                              focusOutItem={true}
                              isDisabled={true}
                            />
                          </div>
                        </div>
                      </div> :
                      <div className="sub-div-common bottom-border flex-alignCenter">
                        <div className="p-l-10 dropdown-nopadding">
                          <Dropdown
                            inputID="propertyType"
                            isReadOnly={true}
                            label={labels.propertyType}
                            dropdownItems={mapValueToDescription(
                              propertyTypeList
                            )}
                            isFocus={propertyType.isFocus}
                            value={propertyType}
                            isValid={propertyType.isValid}
                            errorMsg={propertyType.errorMsg}
                            focusOutItem={true}
                            isDisabled={true}
                          />
                        </div>
                      </div>
                    }
                  </div>
                  : null}
              </div>
            }
          </div>
          : (
            <div>
              <div className={`sub-div-common mob-border ${positionRelativeOverseas}`}>
                <div className="p-l-10 fullWidth">
                  <TextInput
                    inputID={`${addressType}Line1`}
                    label={labels[`${addressType}Line1`]}
                    value={line1.value}
                    errorMsg={line1.errorMsg}
                    onChange={data => this.handleOnChange(data, `${addressType}Line1`)}
                    isValid={line1.isValid}
                    validator={["required", "maxSize|30"]}
                    errorMsgList={errorMsgList}
                    hasIcon={false}
                    isReadOnly={addressType !== "company" && addressType !== "residential" ? false : true}
                  />
                </div>
              </div>
              <div className={`sub-div-common ${positionRelativeOverseas}`}>
                <div className={`p-l-10 fullWidth ${bottomBorderOverseas}`}>
                  <TextInput
                    inputID={`${addressType}Line2`}
                    label={labels[`${addressType}Line2`]}
                    value={line2.value}
                    errorMsg={line2.errorMsg}
                    onChange={data => this.handleOnChange(data, `${addressType}Line2`)}
                    isValid={line2.isValid}
                    validator={["maxSize|30"]}
                    errorMsgList={errorMsgList}
                    hasIcon={false}
                    isReadOnly={addressType !== "company" && addressType !== "residential" ? false : true}
                  />
                </div>
              </div>
              {addressType !== "company" &&
                <div>
                  <div className="sub-div-common positionRelative">
                    <div className="p-l-10 fullWidth">
                      <TextInput
                        inputID={`${addressType}Line3`}
                        label={labels[`${addressType}Line3`]}
                        value={line3.value}
                        errorMsg={line3.errorMsg}
                        onChange={data => this.handleOnChange(data, `${addressType}Line3`)}
                        isValid={line3.isValid}
                        validator={["maxSize|30"]}
                        errorMsgList={errorMsgList}
                        hasIcon={false}
                      />
                    </div>
                  </div>
                  <div className="sub-div-common positionRelative">
                    <div className="p-l-10 fullWidth bottom-border">
                      <TextInput
                        inputID={`${addressType}Line4`}
                        label={labels[`${addressType}Line4`]}
                        value={line4.value}
                        errorMsg={line4.errorMsg}
                        onChange={data => this.handleOnChange(data, `${addressType}Line4`)}
                        isValid={line4.isValid}
                        validator={["maxSize|30"]}
                        errorMsgList={errorMsgList}
                        hasIcon={false}
                      />
                    </div>
                  </div>
                </div>
              }

            </div>
          )
        }
      </div>
    );
  }
}

LocalAddressInput.propTypes = {
  labels: PropTypes.object.isRequired,
  propertyType: PropTypes.string.isRequired,
  addressType: PropTypes.string.isRequired
}

LocalAddressInput.defaultProps = {
  propertyType: 'HDB'
};

const mapStateToProps = (state) => {
  const { localAddressInputReducer } = state;
  return { localAddressInputReducer };
}

export default connect(mapStateToProps)(LocalAddressInput);
