import React from 'react';
import PropTypes from 'prop-types';

import './NavigationBar.css';

// import upArrow from './../../assets/images/sortImg.svg';

// const Icon = props => {
//   const isVisible = props.isActive ? '--visible' : '';
//   return <img className={`upDownArrow${isVisible}`} src={upArrow} alt='tick' />;
// };

const NavigationBar = props => {
  const { onClick, items, isActive } = props;
  return (
    <div className='sortingbar-wrap'>
      <div className='sortingbar-container'>
        <div className='sortingbar'>
          <ul className={`sortingbar-list`}>
            {
              items.map((item) => {
                return (
                  <li key={item.key} onClick={() => onClick(item.key)} className={isActive === item.key ? `sortingbar-list-item--active` : `sortingbar-list-item`}>
                    {item.value}
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
    </div>
  );
}

NavigationBar.propTypes = {
  onClick: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired
};

export default NavigationBar;
