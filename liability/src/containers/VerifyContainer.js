import React, { Component } from 'react';
import { connect } from 'react-redux';
import VerifyPage from './../pages/VerifyPage';
import { getInitialData } from "./../actions/commonAction";

import "./../assets/css/landing-style.css";

class VerifyContainer extends Component { 
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(getInitialData());
  }

  render() {
    const { commonReducer } = this.props;
    return (
      <div className="verifyPage-container">
        {commonReducer.appData !== null &&  <VerifyPage {...this.props}/> }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { applicationReducer, commonReducer } = state;
  return { applicationReducer, commonReducer };
}

export default connect(mapStateToProps)(VerifyContainer);
