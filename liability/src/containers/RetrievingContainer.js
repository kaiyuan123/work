import React, { Component } from 'react';
import { connect } from 'react-redux';
import RetrievingPage from './../pages/RetrievingPage';
import { getInitialData } from "./../actions/commonAction";

import "./../assets/css/landing-style.css";

class ApplyContainer extends Component {
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(getInitialData());
  }

  render() {
    const { commonReducer } = this.props;
    return (
      <div className="loading-container">
        {commonReducer.appData !== null && <RetrievingPage {...this.props}/>}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { applicationReducer, commonReducer } = state;
  return { applicationReducer, commonReducer };
}

export default connect(mapStateToProps)(ApplyContainer);
