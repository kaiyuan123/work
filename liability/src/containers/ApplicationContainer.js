import React, { Component } from "react";
import { connect } from "react-redux";
import WrapContainer from "./WrapContainer";
import ThankYouPage from "./../pages/ThankYouPage";
import ContactDetailsPage from "./../pages/ContactDetailsPage";
import PersonalIncomeDetailsPage from "./../pages/PersonalIncomeDetailsPage";
import CompanyDetailsPage from "./../pages/CompanyDetailsPage";
import AccountSetupPage from "./../pages/AccountSetupPage";
import OperatingMandatePage from "./../pages/OperatingMandatePage";
import ConfirmDetailsPage from "./../pages/ConfirmDetailsPage";
import ASRDetailsPage from "./../pages/ASRDetailsPage";
import ShareHoldersPage from "./../pages/ShareHoldersPage";
import TaxSelfDeclarationsPage from "./../pages/TaxSelfDeclarationsPage";
import BookAppointmentPage from "./../pages/BookAppointmentPage";
import { handleSignatoriesAdditionalErrorMessage, setSignatoriesPartnersInputError, setReviewSection } from "./../actions/operatingMandateAction";
import SaveAndExitPopup from "./../pages/SaveAndExitPopup";
import CallNumberPopup from "./../pages/CallNumberPopup";
import SessionExpirePopup from "./../pages/SessionExpirePopup";
import { handleShareHolderErrorMessage, setShareHoldersInputError } from "./../actions/shareHoldersAction";
import { eliminateDuplicates } from "../common/utils";
import UploadDocument from "./../pages/UploadDocument";
import { scrollToSection, setRetrievingMyInfo, handleErrorMessage, scrollToElement, scrollBackToSection, setErrorMessage, setErrorCounter, setDuplicateTabId, isMobileDevice } from "./../actions/commonAction";
import {
  retrieveMyInfoDetails,
  submitInitiateApplication,
  submitPartialApplication,
  submitSaveAndExitApplication,
  submitApplication,
  setErrorWithValue
} from "./../actions/applicationAction";
import {
  checkPurposeOfAccountIsValid
} from "./../actions/accountSetupAction";
import { handleAdditionalErrorMessage, setTinNoOthersInputError, handleAdditionalErrorMessageCP, checkTaxResidencyIsValid, checkTaxResidencyIsValidCP, handleAdditionalErrorMessageCPT, setTinNoOthersInputErrorCP, setCPInputError } from './../actions/taxSelfDeclarationsAction';
import { setWhichKnockOutScenarioFalse, isKnockOutScenario } from "./../actions/applicationAction";
import { bookAppointmentTogglePopUp } from "./../actions/bookAppointmentAction";
import { LocalEnvironment, Force_URL_redirect, Force_Google_Tag, uobSite } from "./../api/httpApi"
import { showErrorMessage, setSignatureUploadIsValidStatus } from "./../actions/signatureAction";
import { showSaveAndExitPopup } from "./../actions/saveAndExitPopupAction";
import { sendDataToSparkline } from "./../common/utils";
import localStore from './../common/localStore';
import moment from 'moment';
import queryString from 'query-string';
import {  checkPrimaryClienteleIsValid } from "./../actions/companyBasicDetailsAction";

class ApplicationContainer extends Component {
  componentWillMount() {
    const { dispatch, applicationReducer, commonReducer } = this.props;
    const parameter = applicationReducer.startingParameter;
    const params = queryString.parse(parameter);
    if (LocalEnvironment && Force_URL_redirect) {
      return true;
    }
    else {
      if (commonReducer.appData) {
        let pathname = window.location.pathname;
        const isCommerical = pathname.includes("corporate", 0);
        const corporateUrl = commonReducer.appData.pathname_uri_commercial;
        const businessUrl = commonReducer.appData.pathname_uri_business;
        const root_path = isCommerical ? corporateUrl : businessUrl;
        const parameterLength = (parseInt(commonReducer.appData.maxLengthParams, 10) - 1);
        let requiredParameter= [];
        commonReducer.appData.dataElementObj.requiredParameter.map((i) => {
            const conCatString = params[i] ? `${i}=${params[i]}` : '';
            if(conCatString !== '') {
              requiredParameter.push(conCatString);
            }
            return requiredParameter;
        });
        const parameterNeed = `?${requiredParameter.join('&').substring(0,parameterLength)}`;
        this.props.history.push(LocalEnvironment ? `/application${parameterNeed}` : `${root_path}application${parameterNeed}`);
        if (localStore.getStore('duplicateTabs') !== "" && localStore.getStore('duplicateTabs') !== undefined && localStore.getStore('duplicateTabs') !== null) {
          const duplicateTabs = btoa(`${moment()}&${pathname}&${Math.random() + 100}`);
          localStore.setStore('duplicateTabs',duplicateTabs);
        } else {
          const duplicateTabs = btoa(`${moment()}&${pathname}&${Math.random() + 100}`);
          localStore.setStore('duplicateTabs',duplicateTabs);
          dispatch(setDuplicateTabId(duplicateTabs));
        }

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
            window.navigator.userAgent
          )) {
          dispatch(isMobileDevice(true));
        }
      }
      dispatch(setRetrievingMyInfo(true));
      dispatch(retrieveMyInfoDetails(() => this.redirectToErrorPage()));
    }
  }

  checkisValidFields(fields) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (!fields[i].isValid) {
        const fieldError = fields.find(x => x.isValid === false);
        dispatch(scrollToElement(fieldError.name))
        errorCount++;
      }
    }
    if (errorCount > 0) {
      //dispatch(setErrorCounter(errorCount));
      return false;
    }
    return true;
  }

  checkEmptyFieldsStatic(fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (fields[i].value === "") {
        dispatch(action(fields[i].name, errorMsg));
        errorCount++;
      }
    }
    if (errorCount > 0) {
      //dispatch(setErrorCounter(errorCount));
      return false;
    }
    return true;
  }

  checkisValidFieldsStatic(fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (!fields[i].isValid) {
        dispatch(action(fields[i].name, errorMsg));
        errorCount++;
      }
    }
    if (errorCount > 0) {
      //dispatch(setErrorCounter(errorCount));
      return false;
    }
    return true;
  }

  checkEmptyFields(fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (fields[i].value === "") {
        dispatch(action(fields[i].name, errorMsg));
        const fieldError = fields.find(x => x.value === '');
        dispatch(scrollToElement(fieldError.name))
        errorCount++;
      }
    }
    if (errorCount > 0) {
      //dispatch(setErrorCounter(errorCount));
      return false;
    }
    return true;
  }

  checkAdditionalValidFields(key, fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (!fields[i].isValid) {
        dispatch(action(key, fields[i].name, errorMsg));
        const fieldError = fields.find(x => x.isValid === false);
        dispatch(scrollToElement(fieldError.name + key));
        errorCount++;
      }
    }
    if (errorCount > 0) {
      //dispatch(setErrorCounter(errorCount));
      return false;
    }
    return true;
  }

  checkAdditionalEmptyFields(key, fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (fields[i].value === "") {
        dispatch(action(key, fields[i].name, errorMsg));
        const fieldError = fields.find(x => x.value === '');
        dispatch(scrollToElement(fieldError.name + key));
        errorCount++;
      }
    }
    if (errorCount > 0) {
      //dispatch(setErrorCounter(errorCount));
      return false;
    }
    return true;
  }

  checkAdditionalValidFieldsCP(cpKey, key, fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (!fields[i].isValid) {
        dispatch(action(cpKey, key, fields[i].name, errorMsg));
        const fieldError = fields.find(x => x.isValid === false);
        dispatch(scrollToElement(fieldError.name + cpKey + key));
        errorCount++;
      }
    }
    if (errorCount > 0) {
      //dispatch(setErrorCounter(errorCount));
      return false;
    }
    return true;
  }

  checkAdditionalEmptyFieldsCP(cpKey, key, fields, errorMsg, action) {
    const { dispatch } = this.props;
    let errorCount = 0;
    for (let i = 0; i < fields.length; i++) {
      if (fields[i].value === "") {
        dispatch(action(cpKey, key, fields[i].name, errorMsg));
        const fieldError = fields.find(x => x.value === '');
        dispatch(scrollToElement(fieldError.name + cpKey + key));
        errorCount++;
      }
    }
    if (errorCount > 0) {
      //dispatch(setErrorCounter(errorCount));
      return false;
    }
    return true;
  }

  isCRSCheckingPass() {
    const { taxSelfDeclarationsReducer, commonReducer, dispatch, contactDetailsReducer } = this.props;
    const { tinNoOthersId, checkboxOthers, checkboxUS, checkboxSG, checkboxNone, countryNone, tinNoSG, tinNoUS, isUSResident, fatcaCRSStatus, controllingPersonId, haveTINNoNone, tinNoNone, reasonsNone, othersNone } = taxSelfDeclarationsReducer;
    const { businessConstitution } = contactDetailsReducer;

    const isNotSoleProp = businessConstitution.value !== "S";
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;
    let duplicateMsg = commonReducer.appData.errorMsgs.duplicateMsg;
    let moreThan100PercentMsg = commonReducer.appData.errorMsgs.moreThan100PercentMsg;
    const if1Checked = checkboxNone.isToggled || checkboxOthers.isToggled || checkboxSG.isToggled || checkboxUS.isToggled;

    if (!if1Checked) {
      dispatch(checkTaxResidencyIsValid(false));
      dispatch(scrollToElement('taxResidency'));
      errorCount++;
    }

    if (if1Checked) {
      dispatch(checkTaxResidencyIsValid(true));
    }

    let duplicateKeys = [];
    for (let j = 0; j < tinNoOthersId.length; j++) {
      for (let k = j + 1; k < tinNoOthersId.length; k++) {
        const key = tinNoOthersId[k];
        const compareKey = tinNoOthersId[j];
        if (k !== j && taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value === taxSelfDeclarationsReducer[`tinNoOthers${compareKey}`].othersCountry.value) {
          duplicateKeys.push(tinNoOthersId[k]);
          duplicateKeys.push(tinNoOthersId[j]);
        }
      }
    }
    duplicateKeys = eliminateDuplicates(duplicateKeys);
    if (duplicateKeys.length > 0) {
      for (let i = 0; i < duplicateKeys.length; i++) {
        const key = duplicateKeys[i];
        dispatch(setTinNoOthersInputError(key, 'othersCountry', duplicateMsg));
        errorCount++;
      }
    }

    if (checkboxUS.isToggled) {
      if (!this.checkEmptyFields([tinNoUS, isUSResident], requiredMsg, handleErrorMessage)) {
        errorCount++;
      }

      if (!this.checkisValidFields([tinNoUS, isUSResident], invalidMsg, handleErrorMessage)) {
        errorCount++;
      }
    }

    if (checkboxSG.isToggled) {
      if (!this.checkEmptyFields([tinNoSG], requiredMsg, handleErrorMessage)) {
        errorCount++;
      }

      if (!this.checkisValidFields([tinNoSG], invalidMsg, handleErrorMessage)) {
        errorCount++
      }
    }

    if (tinNoOthersId && tinNoOthersId.length > 0) {
      taxSelfDeclarationsReducer.tinNoOthersId.map((key) => {
        const haveTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo;
        const othersCountry = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry;
        const othersTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo;
        const reasons = taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons;
        const othersOthers = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers;

        if (checkboxOthers.isToggled) {
          if (haveTINNo.value !== "") {
            if (haveTINNo.value === "Y") {
              if (!this.checkAdditionalEmptyFields(key, [othersCountry, othersTINNo], requiredMsg, handleAdditionalErrorMessage)) {
                errorCount++;
              }
              if (!this.checkAdditionalValidFields(key, [othersCountry, othersTINNo], invalidMsg, handleAdditionalErrorMessage)) {
                errorCount++;
              }
            } else {
              if (reasons.value === "3") {
                if (!this.checkAdditionalEmptyFields(key, [othersCountry, reasons, othersOthers], requiredMsg, handleAdditionalErrorMessage)) {
                  errorCount++;
                }
                if (!this.checkAdditionalValidFields(key, [othersCountry, reasons, othersOthers], invalidMsg, handleAdditionalErrorMessage)) {
                  errorCount++;
                }
              } else {
                if (!this.checkAdditionalEmptyFields(key, [othersCountry, reasons], requiredMsg, handleAdditionalErrorMessage)) {
                  errorCount++;
                }
                if (!this.checkAdditionalValidFields(key, [othersCountry, reasons], invalidMsg, handleAdditionalErrorMessage)) {
                  errorCount++;
                }
              }
            }
          } else {
            if (!this.checkAdditionalEmptyFields(key, [othersCountry, haveTINNo], requiredMsg, handleAdditionalErrorMessage)) {
              errorCount++;
            }
            if (!this.checkAdditionalValidFields(key, [othersCountry, haveTINNo], invalidMsg, handleAdditionalErrorMessage)) {
              errorCount++;
            }
          }
        }

        return errorCount;
      })
    }

    if (checkboxNone.isToggled) {
      if (haveTINNoNone.value !== "") {
        if (haveTINNoNone.value === "Y") {
          if (!this.checkEmptyFields([countryNone, tinNoNone], requiredMsg, handleErrorMessage)) {
            errorCount++;
          }

          if (!this.checkisValidFields([countryNone, tinNoNone], invalidMsg, handleErrorMessage)) {
            errorCount++;
          }
        } else {
          if (reasonsNone.value === "3") {
            if (!this.checkEmptyFields([countryNone, reasonsNone, othersNone], requiredMsg, handleErrorMessage)) {
              errorCount++;
            }

            if (!this.checkisValidFields([countryNone, reasonsNone, othersNone], invalidMsg, handleErrorMessage)) {
              errorCount++;
            }
          } else {
            if (!this.checkEmptyFields([countryNone, reasonsNone], requiredMsg, handleErrorMessage)) {
              errorCount++;
            }

            if (!this.checkisValidFields([countryNone, reasonsNone], invalidMsg, handleErrorMessage)) {
              errorCount++;
            }
          }
        }
      } else {
        if (!this.checkEmptyFields([countryNone, haveTINNoNone], requiredMsg, handleErrorMessage)) {
          errorCount++;
        }

        if (!this.checkisValidFields([countryNone, haveTINNoNone], invalidMsg, handleErrorMessage)) {
          errorCount++;
        }
      }
    }
    if (isNotSoleProp) {
      if (!this.checkEmptyFields([fatcaCRSStatus], requiredMsg, handleErrorMessage)) {
        errorCount++;
      }

      if (!this.checkisValidFields([fatcaCRSStatus], invalidMsg, handleErrorMessage)) {
        errorCount++;
      }
      if (fatcaCRSStatus.value === "PB" && controllingPersonId && controllingPersonId.length > 0) {
        let duplicateKeysCP = [];
        for (let j = 0; j < controllingPersonId.length; j++) {
          for (let k = j + 1; k < controllingPersonId.length; k++) {
            const key = controllingPersonId[k];
            const compareKey = controllingPersonId[j];
            if (k !== j && taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.value === taxSelfDeclarationsReducer[`controllingPerson${compareKey}`].cpIdNo.value) {
              duplicateKeysCP.push(controllingPersonId[k]);
              duplicateKeysCP.push(controllingPersonId[j]);
            }
          }
        }
        duplicateKeysCP = eliminateDuplicates(duplicateKeysCP);
        if (duplicateKeysCP.length > 0) {
          for (let i = 0; i < duplicateKeysCP.length; i++) {
            const key = duplicateKeysCP[i];
            dispatch(setCPInputError(key, 'cpIdNo', duplicateMsg));
            errorCount++;
          }
        }
        taxSelfDeclarationsReducer.controllingPersonId.map((key) => {
          const cpName = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName;
          const cpIdNo = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo;
          const cpMobileNo = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo;
          const cpEmail = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpEmail;
          const cpPercentageOwnership = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership;

          const checkboxOthers = taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxOthers;
          const checkboxSG = taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxSG;
          const checkboxUS = taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxUS;
          const tinNoSG = taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoSG;
          const tinNoUS = taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoUS;
          const tinNoOthersId = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthersId`];

          const if1Checked = checkboxOthers.isToggled || checkboxSG.isToggled || checkboxUS.isToggled;

          if (!if1Checked) {
            dispatch(checkTaxResidencyIsValidCP(`controllingPerson${key}`, false));
            dispatch(scrollToElement(`taxResidency${key}`));
            errorCount++;
          }

          if (if1Checked) {
            dispatch(checkTaxResidencyIsValidCP(`controllingPerson${key}`, true));
          }
          if (cpMobileNo.value === '+65') {
            dispatch(handleAdditionalErrorMessageCP(key, "cpMobileNo", invalidMsg, cpMobileNo.value))
          }

          let duplicateKeys = [];
          for (let j = 0; j < tinNoOthersId.length; j++) {
            for (let k = j + 1; k < tinNoOthersId.length; k++) {
              const tKey = tinNoOthersId[k];
              const compareKey = tinNoOthersId[j];
              if (k !== j && taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].othersCountry.value === taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${compareKey}`].othersCountry.value) {
                duplicateKeys.push(tinNoOthersId[k]);
                duplicateKeys.push(tinNoOthersId[j]);
              }
            }
          }
          duplicateKeys = eliminateDuplicates(duplicateKeys);
          if (duplicateKeys.length > 0) {
            for (let i = 0; i < duplicateKeys.length; i++) {
              const tKey = duplicateKeys[i];
              dispatch(setTinNoOthersInputErrorCP(key, tKey, 'othersCountry', duplicateMsg));
              errorCount++;
            }
          }

          if (!this.checkAdditionalEmptyFields(key, [cpName, cpIdNo, cpMobileNo, cpEmail, cpPercentageOwnership], requiredMsg, handleAdditionalErrorMessageCP)) {
            errorCount++;
          }

          if (!this.checkAdditionalValidFields(key, [cpName, cpIdNo, cpMobileNo, cpEmail, cpPercentageOwnership], invalidMsg, handleAdditionalErrorMessageCP)) {
            errorCount++;
          }


          if (checkboxSG.isToggled) {
            if (!this.checkAdditionalEmptyFields(key, [tinNoSG], requiredMsg, handleAdditionalErrorMessageCP)) {
              errorCount++;
            }
            if (!this.checkAdditionalValidFields(key, [tinNoSG], invalidMsg, handleAdditionalErrorMessageCP)) {
              errorCount++;
            }
          }

          if (checkboxUS.isToggled) {
            if (!this.checkAdditionalEmptyFields(key, [tinNoUS], requiredMsg, handleAdditionalErrorMessageCP)) {
              errorCount++;
            }
            if (!this.checkAdditionalValidFields(key, [tinNoUS], invalidMsg, handleAdditionalErrorMessageCP)) {
              errorCount++;
            }

          }
          if (tinNoOthersId && tinNoOthersId.length > 0) {
            taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthersId`].map((tKey) => {
              const haveTINNo = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].haveTINNo;
              const othersCountry = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].othersCountry;
              const othersTINNo = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].othersTINNo;
              const reasons = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].reasons;
              const othersOthers = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].othersOthers;


              if (checkboxOthers.isToggled) {
                if (haveTINNo.value !== "") {
                  if (haveTINNo.value === "Y") {
                    if (!this.checkAdditionalEmptyFieldsCP(key, tKey, [othersCountry, othersTINNo], requiredMsg, handleAdditionalErrorMessageCPT)) {
                      errorCount++;
                    }

                    if (!this.checkAdditionalValidFieldsCP(key, tKey, [othersCountry, othersTINNo], invalidMsg, handleAdditionalErrorMessageCPT)) {
                      errorCount++;
                    }
                  } else {
                    if (reasons.value === "3") {
                      if (!this.checkAdditionalEmptyFieldsCP(key, tKey, [othersCountry, reasons, othersOthers], requiredMsg, handleAdditionalErrorMessageCPT)) {
                        errorCount++;
                      }

                      if (!this.checkAdditionalValidFieldsCP(key, tKey, [othersCountry, reasons, othersOthers], invalidMsg, handleAdditionalErrorMessageCPT)) {
                        errorCount++;
                      }
                    } else {
                      if (!this.checkAdditionalEmptyFieldsCP(key, tKey, [othersCountry, reasons], requiredMsg, handleAdditionalErrorMessageCPT)) {
                        errorCount++;
                      }

                      if (!this.checkAdditionalValidFieldsCP(key, tKey, [othersCountry, reasons], invalidMsg, handleAdditionalErrorMessageCPT)) {
                        errorCount++;
                      }
                    }
                  }
                } else {
                  if (!this.checkAdditionalEmptyFieldsCP(key, tKey, [othersCountry, haveTINNo], requiredMsg, handleAdditionalErrorMessageCPT)) {
                    errorCount++;
                  }
                  if (!this.checkAdditionalValidFieldsCP(key, tKey, [othersCountry, haveTINNo], invalidMsg, handleAdditionalErrorMessageCPT)) {
                    errorCount++;
                  }
                  errorCount++
                }
              }

              return errorCount;
            })
          }

          return errorCount;
        })
        let percentageTotal = 0;
        taxSelfDeclarationsReducer.controllingPersonId.map((key) => {
          const cpPercentageOwnership = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership;
          percentageTotal = percentageTotal + parseInt(cpPercentageOwnership.value, 10);
          return percentageTotal;
        })
        if (percentageTotal > 100) {
          dispatch(setErrorMessage(moreThan100PercentMsg));
          errorCount++;
        }
      }
    }

    if (errorCount > 0) {
      return false
    }

    return true
  }

  isContactDetailsPassChecking() {
    const { contactDetailsReducer, commonReducer } = this.props;
    const { emailAddress, primaryNatureOfBusiness, secondaryNatureOfBusiness, primaryCountryOfOperation } = contactDetailsReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;

    if (!this.checkEmptyFields([primaryCountryOfOperation, primaryNatureOfBusiness, secondaryNatureOfBusiness, emailAddress], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (!this.checkisValidFields([primaryCountryOfOperation, primaryNatureOfBusiness, secondaryNatureOfBusiness, emailAddress], invalidMsg, handleErrorMessage)) {
      errorCount++
    }

    // if (businessConstitution.value !== "S") {
    //   if (!this.checkEmptyFields([entitySubType], requiredMsg, handleErrorMessage)) {
    //     errorCount++
    //   }
    //
    //   if (!this.checkisValidFields([entitySubType], invalidMsg, handleErrorMessage)) {
    //     errorCount++
    //   }
    // }

    if (errorCount > 0) {
      return false;
    }

    return true;
  }

  isPersonalIncomeDetailsPassChecking() {
    const { personalIncomeDetailsReducer, commonReducer } = this.props;
    const { maritalStatus } = personalIncomeDetailsReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;

    if (!this.checkEmptyFields([maritalStatus], requiredMsg, handleErrorMessage)) {
      errorCount++;
    }

    if (!this.checkisValidFields([maritalStatus], invalidMsg, handleErrorMessage)) {
      errorCount++;
    }

    if (errorCount > 0) {
      return false;
    }

    return true;
  }

  isAccountSetupPassChecking() {
    const { accountSetupReducer, commonReducer, dispatch } = this.props;
    const {
      accountName, purposeOfAccount, registerOfPayNow,
      setupBIBPlus, BIBGroupId, BIBMobileNumber, BIBContactPerson, BIBEmailAddress, payNowID, others, otherPurpose
    } = accountSetupReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;

    if (purposeOfAccount.length < 1) {
      dispatch(checkPurposeOfAccountIsValid(false));
      dispatch(scrollToElement('purposeOfAccountMain'));
      errorCount++
    }

    if (purposeOfAccount.length > 0) {
      dispatch(checkPurposeOfAccountIsValid(true));
    }

    if (!this.checkEmptyFields([accountName, purposeOfAccount], requiredMsg, handleErrorMessage)) {
      errorCount++
    }


    if (registerOfPayNow.isToggled) {
      if (!this.checkisValidFields([payNowID], invalidMsg, handleErrorMessage)) {
        errorCount++
      }
    }

    if (others.isToggled) {
      if (!this.checkEmptyFields([otherPurpose], requiredMsg, handleErrorMessage)) {
        errorCount++
      }

      if (!this.checkisValidFields([otherPurpose], invalidMsg, handleErrorMessage)) {
        errorCount++
      }
    }

    if (!this.checkEmptyFields([BIBMobileNumber, BIBContactPerson, BIBEmailAddress], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (!this.checkisValidFields([BIBContactPerson, BIBMobileNumber, BIBEmailAddress], invalidMsg, handleErrorMessage)) {
      errorCount++
    }

    if (setupBIBPlus.isToggled) {
      if (!this.checkisValidFields([BIBGroupId], invalidMsg, handleErrorMessage)) {
        errorCount++
      }
    }

    if (errorCount > 0) {
      return false
    }
    return true;
  }

  isDocumentUploadPass() {
    const { dispatch, uploadDocumentsReducer } = this.props;
    let errorCount = 0;
    if (!uploadDocumentsReducer.MADocument.isValid || uploadDocumentsReducer.MADocument.inputValue === "") {
      dispatch(scrollToElement('uploadDocument'));
      errorCount++
    }

    if (errorCount > 0) {
      return false
    }
    return true;
  }

  isAppointmentBookingPass() {
    const { bookAppointmentReducer, commonReducer } = this.props;
    const { bookingBranch } = bookAppointmentReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;

    if (!this.checkEmptyFields([bookingBranch], requiredMsg, handleErrorMessage)) {
      errorCount++
    }

    if (!this.checkisValidFields([bookingBranch], invalidMsg, handleErrorMessage)) {
      errorCount++
    }

    if (errorCount > 0) {
      return false;
    }

    return true;
  }

  handleToPersonalIncomeDetails() {
    const { dispatch } = this.props;
    if (!this.isContactDetailsPassChecking()) {
      return;
    }
    this.handleTracking("contactDetails");
    dispatch(scrollToSection("personalIncomeDetails"));
  }

  handleToCompanyDetails(steps) {
    const { dispatch } = this.props;
    this.handleTracking(steps);
    this.handleSubmitPartialApplication();
    dispatch(scrollToSection("companyDetails"));
  }

  handlePopup(steps) {
    // const { dispatch } = this.props;
    this.handleSubmitPartialApplication();
    this.handleTracking(steps);
  }

  handleToBookAppointment() {
    const { dispatch } = this.props;
    /*if (!this.isAllChecking()) {
      return;
    }
    this.handleSubmitPartialApplication();*/
    dispatch(scrollToSection("bookAppointment"));
  }

  handleBookAppointmentTogglePopUp(status) {
    const { commonReducer, dispatch } = this.props;
    const currentSection = commonReducer.currentSection;
    dispatch(bookAppointmentTogglePopUp(status));
    if (!status) {
      dispatch(scrollToSection(currentSection));
    }
  }

  handleToOperatingMandatePage(steps) {
    const { dispatch } = this.props;
    this.handleSubmitPartialApplication();
    this.handleTracking(steps);
    dispatch(scrollToSection("operatingMandate"));
  }

  handleToShareHoldersPage(steps) {
    const { dispatch } = this.props;
    this.handleSubmitPartialApplication();
    this.handleTracking(steps);
    dispatch(scrollToSection("shareHolders"));
  }

  handleToASRDetailsPage(steps) {
    const { dispatch } = this.props;
    this.handleSubmitPartialApplication();
    this.handleTracking(steps);
    dispatch(scrollToSection("asrDetails"));
  }

  handleToReviewPage(steps) {
    const { dispatch, operatingMandateReducer } = this.props;
    const { signatorySelected, complexSectionArr } = operatingMandateReducer;
    if (signatorySelected.value === '3') {
      dispatch(setReviewSection(true));
      complexSectionArr.push('confirmDetailsPage');
      dispatch(isKnockOutScenario(true));
      dispatch(setWhichKnockOutScenarioFalse("operatingMandateComplex"))
    } else {
      dispatch(setReviewSection(false));
    }
    this.handleSubmitPartialApplication();
    this.handleTracking(steps);
    dispatch(scrollToSection("confirmDetailsPage"));
  }

  handleToDocumentUploadPage(steps) {
    const { dispatch } = this.props;
    this.handleSubmitPartialApplication();
    this.handleTracking(steps);
    dispatch(scrollToSection("uploadDocument"));
  }

  handleToTaxSelfDeclarationsPage(steps) {
    //const sectionArr = operatingMandateReducer.sectionArr;
    //sectionArr.push(steps);

    const { dispatch } = this.props;
    this.handleSubmitPartialApplication();
    this.handleTracking(steps)
    dispatch(scrollToSection("taxSelfDeclarations"));
  }

  handleSaveAndExit(stateObj) {
    const { dispatch } = this.props;
    let fieldName;
    if (document.querySelectorAll('.textInputLabel-container--error').length > 0) {
      fieldName = document.getElementsByClassName('textInputLabel-container--error')[0].id;
      dispatch(scrollToElement(fieldName));
    } else if (document.querySelectorAll('.PhoneInputContainer--error').length > 0) {
      fieldName = document.getElementsByClassName('PhoneInputContainer--error')[0].id;
      dispatch(scrollToElement(fieldName));
    } else if (document.querySelectorAll('.textInputLabel--error').length > 0) {
      fieldName = document.getElementsByClassName('textInputLabel--error')[0].id;
      dispatch(scrollToElement(fieldName));
    } else {
      dispatch(showSaveAndExitPopup(true));
    }

  }

  handleToParentSite() {
    this.handleSubmitPartialApplication('saveAndExitButton');
  }

  handleToUOBSite() {
    window.location.href = uobSite;
  }

  handleInitiateApplication() {
    const { dispatch } = this.props;
    const dataObj = this.getDataForInitiation();
    dispatch(submitInitiateApplication(dataObj, () => this.handleToPersonalIncomeDetails(), () => this.redirectToErrorPage()));
  }

  getDataForInitiation() {
    const {
      contactDetailsReducer
      // personalIncomeDetailsReducer
    } = this.props;

    const {
      emailAddress,
      entitySubType,
      secondaryNatureOfBusiness,
      primaryCountryOfOperation,
      primaryNatureOfBusiness
    } = contactDetailsReducer;

    // const {
    //   legalId
    // } = personalIncomeDetailsReducer;

    const dataObjBasicDetails = {
      person: [{
        basicInfo: {
          emailAddress: emailAddress.value
        },
      }],
      entity: {
        basicProfile: {
          natureOfBusiness: `${primaryNatureOfBusiness.value}|${secondaryNatureOfBusiness.value}`,
          primaryCountryOfOperation: primaryCountryOfOperation.value
        }
      }
    }

    return dataObjBasicDetails;
  }


  isAllChecking() {
    const { dispatch, commonReducer, confirmTandCReducer, contactDetailsReducer, applicationReducer, drawSignatureReducer, signatureReducer, shareHoldersReducer } = this.props;
    const { acknowledgementCheckbox } = confirmTandCReducer;
    const { entityType } = contactDetailsReducer;
    const { shareHoldersList } = shareHoldersReducer;
    const { isKnockOut, isKnockOutField, isKeyManApply, isRetrieveApplication } = applicationReducer;
    const beforeASAPFalse = isKnockOutField.entitySubType && isKnockOutField.countryOfIncorporation && isKnockOutField.natureOfBusiness && isKnockOutField.ownership && isKnockOutField.askQuestion;
    const showOperatingMandate = !isKnockOut || (isKnockOut && !beforeASAPFalse && isKnockOutField.operatingMandateComplex);
    let isShareHoldersList = "";
    if (shareHoldersList.value.length > 0) {
      isShareHoldersList = true;
    } else {
      isShareHoldersList = false;
    }
    if (!this.isContactDetailsPassChecking()) {
      dispatch(scrollBackToSection('contactDetails'));
      return false;
    }

    if (!this.isCompanyDetailsPassing()) {
      dispatch(scrollBackToSection('companyDetails'));
      return false;
    }

    if (!this.isAccountSetupPassChecking()) {
      dispatch(scrollBackToSection('accountSetup'));
      return false;
    }
    if (showOperatingMandate && !this.isOperatingMandatePassing()) {
      dispatch(scrollBackToSection('operatingMandate'));
      return false;
    }

    if (!isKnockOut && entityType.value === 'LC' && isShareHoldersList && !this.isShareHoldersListPassing()) {
      dispatch(scrollBackToSection('shareHolders'));
      return false;
    }

    if (!isKnockOut && !this.isCRSCheckingPass()) {
      dispatch(scrollBackToSection('taxSelfDeclarations'));
      return false;
    }

    if (!isKnockOut && entityType.value === 'LC' && (applicationReducer.isWhichType === "STP" || isRetrieveApplication) && !this.isDocumentUploadPass()) {
      dispatch(scrollBackToSection('uploadDocument'));
      return false;
    }

    if (isKnockOut && !this.isAppointmentBookingPass()) {
      dispatch(scrollBackToSection('bookAppointment'));
      return false;
    }

    if (!acknowledgementCheckbox.isToggled) {
      const checkboxError = commonReducer.appData.confirmDetails.confirmTandC.errorMsgCheckbox
      dispatch(handleErrorMessage("acknowledgementCheckbox", checkboxError))
      dispatch(scrollToElement("confirmTandC"));
      return false;
    }

    // if (!isKnockOut && isKeyManApply && ((commonReducer.isMobile && drawSignatureReducer.trimmedDataURL === null) || (!commonReducer.isMobile && signatureReducer.signaturePad.trimmedDataURL === null))) {
    if (!isKnockOut && isKeyManApply) {
      if(commonReducer.isMobile && (signatureReducer.signaturePad.trimmedDataURL === null || drawSignatureReducer.signaturePad.trimmedDataURL === null )) {
        dispatch(setSignatureUploadIsValidStatus(false))
      } else if (!commonReducer.isMobile && signatureReducer.signaturePad.trimmedDataURL === null) {
        const signatureUploadError = commonReducer.appData.signatureUpload.labels.error4
        dispatch(showErrorMessage(signatureUploadError));
      }
      dispatch(scrollBackToSection('signatureUpload-section'));
      return false;
    }
    return true;
  }

  isAllBookingChecking() {
    const { dispatch } = this.props;

    if (!this.isAppointmentBookingPass()) {
      dispatch(scrollBackToSection('bookAppointment'));
      return false;
    }

    return true;
  }

  getDataForSubmission(saveAndExitButton) {
    const { commonReducer, contactDetailsReducer, applicationReducer, companyBasicDetailsReducer, localAddressInputReducer, accountSetupReducer, operatingMandateReducer, shareHoldersReducer, asrDetailsReducer, taxSelfDeclarationsReducer, personalIncomeDetailsReducer, bookAppointmentReducer } = this.props;
    const { mailingAddressCheckbox, annualTurnover, primaryClientele } = companyBasicDetailsReducer;
    let primaryClienteleBaseArr = [];
    if (primaryClientele) {
      primaryClientele.map((base, i) => {
        if (base === 'Individual Clients') {
            primaryClienteleBaseArr.push('Individual');
        } else if (base === 'Corporate Clients') {
            primaryClienteleBaseArr.push('Corporate');
        } 
        return true;
      });
    }
    
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "uploadDocument", "asrDetails", "confirmDetailsPage", "bookAppointment"];
    let indexOfSteps;
    if (saveAndExitButton) {
      indexOfSteps = steps.indexOf(commonReducer.currentSection);
      indexOfSteps = indexOfSteps - 1;
    } else {
      indexOfSteps = steps.indexOf(commonReducer.currentSection);
    }

    const { initiateMyinfoData, isKnockOut, isKnockOutField } = applicationReducer;
    const { maritalStatus } = personalIncomeDetailsReducer;
    const {
      entityType,
      businessConstitution
    } = contactDetailsReducer;
    const {
      mailingUnit,
      mailingStreet,
      mailingBlock,
      mailingPostalCode,
      mailingLevel,
      mailingBuilding,
      mailingCity,
      mailingCountry,
      mailingLine1,
      mailingLine2,
      mailingLine3,
      mailingLine4
    } = localAddressInputReducer;
    const companyAddress = initiateMyinfoData.entity.addresses[0];

    const {
      accountName,
      currencyOfAccount,
      purposeOfAccount,
      payNowID,
      BIBGroupId,
      // BIBRoleType,
      BIBBulkService,
      BIBRemittanceMessage,
      BIBContactPerson,
      // BIBEmailAddress,
      BIBMobileNumber,
      registerOfPayNow,
      setupBIBPlus,
      chequebooks,
      BIBEmailAddress,
      otherPurpose
    } = accountSetupReducer;

    const {
      signatoriesID,
      // mainApplicantLegalId,
      signatorySelected
    } = operatingMandateReducer;

    const {
      signingApprovedPerson
    } = asrDetailsReducer;

    const {
      shareholderID
    } = shareHoldersReducer;

    const {
      checkboxSG,
      checkboxUS,
      tinNoSG,
      isUSResident,
      tinNoOthersId,
      tinNoUS,
      checkboxOthers,
      controllingPersonId,
      fatcaCRSStatus,
      checkboxNone,
      countryNone,
      haveTINNoNone,
      tinNoNone,
      reasonsNone,
      othersNone
    } = taxSelfDeclarationsReducer;

    const mailingAddressObj = {
      type: 'M',
      unitNo: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingUnit.value : '',
      street: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingStreet.value : '',
      block: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingBlock.value : '',
      postalCode: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingPostalCode.value : '',
      floor: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingLevel.value : '',
      building: mailingAddressCheckbox.isToggled && mailingCountry.value === 'SG' ? mailingBuilding.value : '',
      country: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingCountry.value : 'SG',
      city: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingCity.value : '',
      line1: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingLine1.value : '',
      line2: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingLine2.value : '',
      line3: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingLine3.value : '',
      line4: mailingAddressCheckbox.isToggled && mailingCountry.value !== 'SG' ? mailingLine4.value : '',
    };

    const addressObjs = [companyAddress];
    if (mailingAddressCheckbox.isToggled) {
      addressObjs.push(mailingAddressObj)
    }

    const accountSetupValue = commonReducer.appData.accountSetup;
    let checkboxPOA = purposeOfAccount;
    if (purposeOfAccount.indexOf(accountSetupValue.purposeOfAccountList.labels.others) >= 0) {
      checkboxPOA.splice(purposeOfAccount.indexOf(accountSetupValue.purposeOfAccountList.labels.others), 1);
      checkboxPOA.push(`Others - ${otherPurpose.value}`)
    }

    const accountSetupObj = {
      name: accountName.value,
      currency: currencyOfAccount.value,
      purpose: checkboxPOA,
      numberOfChequebooks: chequebooks.value,
      additionalServices: {
        corporatePayNow: {
          suffix: registerOfPayNow.isToggled ? payNowID.value : null
        },
        bibPlus: {
          groupId: setupBIBPlus.isToggled ? BIBGroupId.value : null,
          userRoleType: setupBIBPlus.isToggled ? "S" : null,
          additionalServices: {
            bulkServicesInd: setupBIBPlus.isToggled && BIBBulkService.isToggled ? "Y" : "N",
            remittanceServiceInd: setupBIBPlus.isToggled && BIBRemittanceMessage.isToggled ? "Y" : "N",
          },
          authorizedPerson: {
            name: BIBContactPerson.value,
            mobileNumber: BIBMobileNumber.value.replace(/ +/g, ""),
            emailAddress: BIBEmailAddress.value
          }
        }
      }
    }
    let approvedSignatoriesObj = [];

    if (signatoriesID.length > 0) {
      signatoriesID.map((key) => {
        const isPartnerEmpty = operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.value === "" && operatingMandateReducer[`signatories${key}`].signatoriesEmail.value === "" && operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value === "" && operatingMandateReducer[`signatories${key}`].signatoriesName.value === "" ? true : false;
        const signatoriesMobileNo = operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.value !== "" ? operatingMandateReducer[`signatories${key}`].signatoriesMobileNo.value : null;
        const signatoriesEmail = operatingMandateReducer[`signatories${key}`].signatoriesEmail.value !== "" ? operatingMandateReducer[`signatories${key}`].signatoriesEmail.value : null;
        const signatoriesNRIC = operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value !== "" ? operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value : null;
        const signatoriesName = operatingMandateReducer[`signatories${key}`].signatoriesName.value !== "" ? `${operatingMandateReducer[`signatories${key}`].signatoriesName.value}` : null;
        const bibUserId = operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.value !== "" ? operatingMandateReducer[`signatories${key}`].signatoriesBibUserId.value : null;
        const signatoriesToggleBIBUser = operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser.isToggled;
        if (!isPartnerEmpty) {
          approvedSignatoriesObj.push({
            name: signatoriesName,
            legalId: signatoriesNRIC,
            mobileNumber: signatoriesMobileNo.replace(/ +/g, ""),
            emailAddress: signatoriesEmail,
            bibPlusUserInd: signatoriesToggleBIBUser ? 'Y' : 'N',
            userId: signatoriesToggleBIBUser ? bibUserId : null
          })
        }
        return approvedSignatoriesObj;
      });
    }

    let shareHoldersObj = [];

    if (shareholderID.length > 0) {
      shareholderID.map((key) => {
        const isShareHolderEmpty = shareHoldersReducer[`shareholder${key}`].shareholderMobileNo.value === "" && shareHoldersReducer[`shareholder${key}`].shareholderEmail.value === "" && shareHoldersReducer[`shareholder${key}`].shareholderNRIC.value === "" && shareHoldersReducer[`shareholder${key}`].shareholderName.value === "" ? true : false;
        const shareholderMobileNo = shareHoldersReducer[`shareholder${key}`].shareholderMobileNo.value !== "" ? shareHoldersReducer[`shareholder${key}`].shareholderMobileNo.value : null;
        const shareholderEmail = shareHoldersReducer[`shareholder${key}`].shareholderEmail.value !== "" ? shareHoldersReducer[`shareholder${key}`].shareholderEmail.value : null;
        const shareholderNRIC = shareHoldersReducer[`shareholder${key}`].shareholderNRIC.value !== "" ? shareHoldersReducer[`shareholder${key}`].shareholderNRIC.value : null;
        const shareholderName = shareHoldersReducer[`shareholder${key}`].shareholderName.value !== "" ? `${shareHoldersReducer[`shareholder${key}`].shareholderName.value}` : null;

        if (!isShareHolderEmpty) {
          shareHoldersObj.push({
            name: shareholderName,
            legalId: shareholderNRIC,
            mobileNumber: shareholderMobileNo.replace(/ +/g, ""),
            emailAddress: shareholderEmail,
          })
        }
        return shareHoldersObj;
      });
    }

    const asrObj = {
      signingApprovedPerson: signingApprovedPerson.value
    };

    let crsObj = [];
    if (checkboxSG.isToggled) {
      crsObj.push({
        country: "SG",
        tin: tinNoSG.value,
        reasonCode: null,
        reasonDetail: null,
        usPersonInd: "N",
        taxResidencyInd: "Y"
      })
    }

    if (checkboxUS.isToggled) {
      crsObj.push({
        country: "US",
        tin: tinNoUS.value,
        reasonDetail: null,
        reasonCode: null,
        usPersonInd: isUSResident.value,
        taxResidencyInd: "Y"
      })
    }

    if (checkboxNone.isToggled) {
      const tin = haveTINNoNone.value === "Y" ? tinNoNone.value : null;
      const reason = haveTINNoNone.value === "N" ? reasonsNone.value : null;
      // const checked = checkboxNone.isToggled ? "Y" : "N";
      const detail = haveTINNoNone.value === "N" ? reasonsNone.value === "3" ? othersNone.value : null : null;
      crsObj.push({
        country: countryNone.value,
        tin: tin,
        reasonCode: reason,
        reasonDetail: detail,
        usPersonInd: "N",
        taxResidencyInd: "N",

      })
    }
    if (checkboxOthers.isToggled) {
      if (tinNoOthersId.length > 0) {
        tinNoOthersId.map((key) => {
          const haveTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo.value;
          const country = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value !== "" ? taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value : null;
          const othersTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.value !== "" ? taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo.value : null;
          const reasons = taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.value !== "" ? taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons.value : null;
          const othersOthers = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers.value !== "" ? taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers.value : null;

          crsObj.push({
            country: country,
            tin: haveTINNo === "Y" ? othersTINNo : null,
            reasonCode: haveTINNo === "N" ? reasons : null,
            reasonDetail: haveTINNo === "N" ? reasons === "3" ? othersOthers : null : null,
            usPersonInd: "N",
            taxResidencyInd: "Y"
          });
          return crsObj;
        });

      }
    }

    let controllingPersonArray = [];
    if (businessConstitution.value !== "S" && fatcaCRSStatus.value === "PB") {
      if (controllingPersonId.length > 0) {
        controllingPersonId.map((key) => {
          const name = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.value !== "" ? taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName.value : null;
          const legalId = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.value !== "" ? taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.value : null;
          const percentageOfOwnership = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership.value !== "" ? taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership.value : null;
          const mobileNumber = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo.value !== "" ? taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo.value.replace(/ +/g, "") : null;
          const emailAddress = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpEmail.value !== "" ? taxSelfDeclarationsReducer[`controllingPerson${key}`].cpEmail.value : null;

          let cpCrsObj = [];
          if (taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxSG.isToggled) {
            cpCrsObj.push({
              country: "SG",
              tin: taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoSG.value,
              reasonCode: null,
              reasonDetail: null,
              usPersonInd: "N"
            })
          }
          if (taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxUS.isToggled) {
            cpCrsObj.push({
              country: "US",
              tin: taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoUS.value,
              reasonCode: null,
              reasonDetail: null,
              usPersonInd: null
            })
          }
          if (taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxOthers.isToggled) {
            if (taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoOthersId.length > 0) {
              taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoOthersId.map((cpKey) => {
                const haveTINNo = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${cpKey}`].haveTINNo.value;
                const country = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${cpKey}`].othersCountry.value !== "" ? taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${cpKey}`].othersCountry.value : null;
                const othersTINNo = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${cpKey}`].othersTINNo.value !== "" ? taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${cpKey}`].othersTINNo.value : null;
                const reasons = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${cpKey}`].reasons.value !== "" ? taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${cpKey}`].reasons.value : null;
                const othersOthers = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${cpKey}`].othersOthers.value !== "" ? taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${cpKey}`].othersOthers.value : null;
                cpCrsObj.push({
                  country: country,
                  tin: haveTINNo === "Y" ? othersTINNo : null,
                  reasonCode: haveTINNo === "N" ? reasons : null,
                  reasonDetail: haveTINNo === "N" ? reasons === "3" ? othersOthers : null : null,
                  usPersonInd: "N"
                });
                return cpCrsObj;
              });
            }
          }

          controllingPersonArray.push({
            name,
            legalId,
            percentageOfOwnership,
            mobileNumber,
            emailAddress,
            crs: cpCrsObj
          });
          return controllingPersonArray;
        })
      }
    }

    const beforeASAPFalse = isKnockOutField.entitySubType && isKnockOutField.countryOfIncorporation && isKnockOutField.natureOfBusiness && isKnockOutField.ownership && isKnockOutField.askQuestion;
    const showOperatingMandate = !isKnockOut || (isKnockOut && !beforeASAPFalse && isKnockOutField.operatingMandateComplex);
    const dataObj = {
      type: `${initiateMyinfoData.type}|${indexOfSteps}`,
      person: [
        {
          basicInfo: {
            passportNumber: initiateMyinfoData.person[0].basicInfo.passportNumber ? initiateMyinfoData.person[0].basicInfo.passportNumber : null,
            passportExpiryDate: initiateMyinfoData.person[0].basicInfo.passportExpiryDate ? initiateMyinfoData.person[0].basicInfo.passportExpiryDate : null,
          },
          personalInfo: {
            maritalStatus: maritalStatus.value
          }
        }
      ],
      entity: {
        annualTurnover: annualTurnover.value,
        primaryClienteleBase: primaryClienteleBaseArr,
        addresses: addressObjs
      },
      accountSetup: accountSetupObj,
      operatingMandate: showOperatingMandate ? {
        approvedSignatories: approvedSignatoriesObj,
        signatorySelected: signatorySelected.value
      } : null,
      taxDeclarations: !isKnockOut ? {
        crs: crsObj,
        controllingPerson: businessConstitution.value !== "S" && fatcaCRSStatus.value === "PB" ? controllingPersonArray : null,
        fatcaStatus: fatcaCRSStatus.value
      } : null
    }
    if (entityType.value === 'LC' && !isKnockOut) {
      dataObj.kyc = shareHoldersObj;
    }

    if (businessConstitution.value !== 'S' && !isKnockOut) {
      dataObj.asr = asrObj;
    }

    if (isKnockOut) {
      dataObj.branchAppointment = {
        branchId: bookAppointmentReducer.bookingBranch.value,
        bookingDateTime: bookAppointmentReducer.dateTimeValue.value
      };
    }
    return dataObj;
  }

  handleSubmitPartialApplication(saveAndExitButton) {
    const { dispatch, applicationReducer } = this.props;
    const { initiateMyinfoData } = applicationReducer;
    const applicationID = initiateMyinfoData.id;
    const dataObj = this.getDataForSubmission(saveAndExitButton);
    if (saveAndExitButton) {
      dispatch(submitSaveAndExitApplication(dataObj, applicationID))
    } else {
      dispatch(submitPartialApplication(dataObj, applicationID));
    }

  }

  handleSubmitApplication() {
    const { dispatch, applicationReducer } = this.props;
    const { initiateMyinfoData } = applicationReducer;
    // if (!this.isAllChecking()) {
    //   return;
    // }
    const applicationID = initiateMyinfoData.id;
    const dataObj = this.getDataForSubmission();
    dispatch(submitApplication(dataObj, applicationID));
  }

  handleBookAppointmentSubmitApplication() {
    const { dispatch, applicationReducer, bookAppointmentReducer } = this.props;
    const { initiateMyinfoData } = applicationReducer;
    if (!this.isAllBookingChecking()) {
      return;
    }
    const applicationID = initiateMyinfoData.id;
    const dataObj = this.getDataForSubmission();
    dataObj.branchAppointment = {
      branchId: bookAppointmentReducer.bookingBranch.value,
      bookingDateTime: bookAppointmentReducer.dateTimeValue.value
    };
    dispatch(submitApplication(dataObj, applicationID));
    dispatch(bookAppointmentTogglePopUp(false));
  }

  redirectToErrorPage() {
    // const { applicationReducer } = this.props;
    // localStore.clear();
    this.props.history.push({
      pathname: "error"
    })
  }

  isCompanyDetailsPassing() {
    const { dispatch, companyBasicDetailsReducer, localAddressInputReducer, commonReducer } = this.props;
    const { mailingAddressCheckbox, annualTurnover, primaryClientele } = companyBasicDetailsReducer;
    const {
      mailingUnit,
      mailingStreet,
      mailingBlock,
      mailingPostalCode,
      mailingLevel,
      mailingCity,
      mailingLine1,
      mailingCountry
    } = localAddressInputReducer;

    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;

    if (primaryClientele.length < 1) {
      dispatch(checkPrimaryClienteleIsValid(false));
      dispatch(scrollToElement('primaryClientele'));
      errorCount++
    }

    if (primaryClientele.length > 0) {
      dispatch(checkPrimaryClienteleIsValid(true));
    }
    if (!this.checkEmptyFields([annualTurnover, primaryClientele], requiredMsg, handleErrorMessage)) {
      errorCount++;
    }
    if (!this.checkisValidFields([annualTurnover], invalidMsg, handleErrorMessage)) {
      errorCount++;
    }
    if (mailingAddressCheckbox.isToggled) {
      if (mailingCountry.value !== "SG") {
        if (!this.checkEmptyFields([mailingCity, mailingLine1,], requiredMsg, handleErrorMessage)) {
          errorCount++
        }
        if (!this.checkisValidFields([mailingCity, mailingLine1,], invalidMsg, handleErrorMessage)) {
          errorCount++
        }
      } else {
        if (!this.checkEmptyFields([mailingPostalCode, mailingBlock, mailingLevel, mailingStreet, mailingUnit], requiredMsg, handleErrorMessage)) {
          errorCount++
        }
        if (!this.checkisValidFields([mailingPostalCode, mailingBlock, mailingLevel, mailingStreet, mailingUnit], invalidMsg, handleErrorMessage)) {
          errorCount++
        }
      }
    }
    if (errorCount > 0) {
      dispatch(setErrorCounter(errorCount));
      return false
    }
    return true;
  }

  isOperatingMandatePassing() {
    const { dispatch, operatingMandateReducer, commonReducer, accountSetupReducer } = this.props;
    const { signatorySelected, signatoriesID, mainApplicantLegalId } = operatingMandateReducer;
    const isBibPlusUserToggled = accountSetupReducer.setupBIBPlus.isToggled;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    let duplicateMsg = commonReducer.appData.errorMsgs.duplicateMsg;
    let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;

    if (signatoriesID.length > 0) {
      let duplicateKeys = [];
      for (let j = 0; j < signatoriesID.length; j++) {
        for (let k = j + 1; k < signatoriesID.length; k++) {
          const key = signatoriesID[k];
          const compareKey = signatoriesID[j];
          if (k !== j && operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value === operatingMandateReducer[`signatories${compareKey}`].signatoriesNRIC.value) {
            duplicateKeys.push(signatoriesID[k]);
            duplicateKeys.push(signatoriesID[j]);
          }
        }
      }
      duplicateKeys = eliminateDuplicates(duplicateKeys);
      if (duplicateKeys.length > 0) {
        for (let i = 0; i < duplicateKeys.length; i++) {
          const key = duplicateKeys[i];
          dispatch(setSignatoriesPartnersInputError(`signatories${key}`, 'signatoriesNRIC', duplicateMsg));
          errorCount++;
        }
      }
      operatingMandateReducer.signatoriesID.map((key) => {
        const signatoriesEmail = operatingMandateReducer[`signatories${key}`].signatoriesEmail;
        const signatoriesNRIC = operatingMandateReducer[`signatories${key}`].signatoriesNRIC;
        const signatoriesName = operatingMandateReducer[`signatories${key}`].signatoriesName;
        const signatoriesMobileNo = operatingMandateReducer[`signatories${key}`].signatoriesMobileNo;
        const signatoriesBibUserId = operatingMandateReducer[`signatories${key}`].signatoriesBibUserId;
        const signatoriesToggleBIBUser = operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser;
        if (signatoriesMobileNo.value === '+65') {
          dispatch(handleSignatoriesAdditionalErrorMessage(key, "signatoriesMobileNo", invalidMsg, signatoriesMobileNo.value))
        }
        if (mainApplicantLegalId === operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value && isBibPlusUserToggled && signatoriesToggleBIBUser.isToggled) {
          if (!this.checkAdditionalEmptyFields(key, [signatoriesEmail, signatoriesNRIC, signatoriesName, signatoriesMobileNo, signatoriesBibUserId], requiredMsg, handleSignatoriesAdditionalErrorMessage)) {
            errorCount++;
          }
          if (!this.checkAdditionalValidFields(key, [signatoriesEmail, signatoriesNRIC, signatoriesName, signatoriesMobileNo, signatoriesBibUserId], invalidMsg, handleSignatoriesAdditionalErrorMessage)) {
            errorCount++;
          }
        } else {
          if (!this.checkAdditionalEmptyFields(key, [signatoriesEmail, signatoriesNRIC, signatoriesName, signatoriesMobileNo], requiredMsg, handleSignatoriesAdditionalErrorMessage)) {
            errorCount++;
          }
          if (!this.checkAdditionalValidFields(key, [signatoriesEmail, signatoriesNRIC, signatoriesName, signatoriesMobileNo], invalidMsg, handleSignatoriesAdditionalErrorMessage)) {
            errorCount++;
          }
        }
        return errorCount;
      })
    }
    if (!this.checkEmptyFields([signatorySelected], requiredMsg, handleErrorMessage)) {
      errorCount++
    }
    if (!this.checkisValidFields([signatorySelected], invalidMsg, handleErrorMessage)) {
      errorCount++
    }
    if (errorCount > 0) {
      return false
    }
    return true;

  }

  isShareHoldersListPassing() {
    const { dispatch, shareHoldersReducer, commonReducer } = this.props;
    const { shareholderID } = shareHoldersReducer;
    let errorCount = 0;
    let requiredMsg = commonReducer.appData.errorMsgs.requiredMsg;
    let duplicateMsg = commonReducer.appData.errorMsgs.duplicateMsg;
    let invalidMsg = commonReducer.appData.errorMsgs.invalidMsg;

    if (shareholderID.length > 0) {
      let duplicateKeys = [];
      for (let j = 0; j < shareholderID.length; j++) {
        for (let k = j + 1; k < shareholderID.length; k++) {
          const key = shareholderID[k];
          const compareKey = shareholderID[j];
          if (k !== j && shareHoldersReducer[`shareholder${key}`].shareholderNRIC.value === shareHoldersReducer[`shareholder${compareKey}`].shareholderNRIC.value) {
            duplicateKeys.push(shareholderID[k]);
            duplicateKeys.push(shareholderID[j]);
          }
        }
      }
      duplicateKeys = eliminateDuplicates(duplicateKeys);
      if (duplicateKeys.length > 0) {
        for (let i = 0; i < duplicateKeys.length; i++) {
          const key = duplicateKeys[i];
          dispatch(setShareHoldersInputError(`shareholder${key}`, 'shareholderNRIC', duplicateMsg));
          errorCount++;
        }
      }
      shareHoldersReducer.shareholderID.map((key) => {
        const shareholderEmail = shareHoldersReducer[`shareholder${key}`].shareholderEmail;
        const shareholderNRIC = shareHoldersReducer[`shareholder${key}`].shareholderNRIC;
        const shareholderName = shareHoldersReducer[`shareholder${key}`].shareholderName;
        const shareholderMobileNo = shareHoldersReducer[`shareholder${key}`].shareholderMobileNo;
        if (shareholderMobileNo.value === '+65') {
          dispatch(handleShareHolderErrorMessage(key, "shareholderMobileNo", invalidMsg, shareholderMobileNo.value))
        }
        if (!this.checkAdditionalEmptyFields(key, [shareholderEmail, shareholderNRIC, shareholderName, shareholderMobileNo], requiredMsg, handleShareHolderErrorMessage)) {
          errorCount++;
        }

        if (!this.checkAdditionalValidFields(key, [shareholderEmail, shareholderNRIC, shareholderName, shareholderMobileNo], invalidMsg, handleShareHolderErrorMessage)) {
          errorCount++;
        }
        return errorCount;
      })
    }
    if (errorCount > 0) {
      return false
    }
    return true;
  }

  handleTracking(step) {
    const {
      commonReducer,
      applicationReducer
    } = this.props;
    const { isInitial, initiateMyinfoData, productCode } = applicationReducer
    const dataElement = commonReducer.appData.dataElementObj;
    const id = initiateMyinfoData && initiateMyinfoData.referenceNumber && isInitial ? initiateMyinfoData.referenceNumber : "";
    const code = productCode ? productCode.toLowerCase() : "";
    let stepNo = 0;
    switch (step) {
      case 'contactDetails':
        stepNo = 1;
        break;
      case 'personalDetails':
        stepNo = 2;
        break;
      case 'companyDetails':
        stepNo = 3;
        break;
      case 'sanctionQns':
        stepNo = 4;
        break;
      case 'accountSetup':
        stepNo = 5;
        break;
      case 'operatingMandate':
        stepNo = 6;
        break;
      case 'shareholderList':
        stepNo = 7;
        break;
      case 'taxSelfDeclarationDetails':
        stepNo = 8;
        break;
      case 'uploadDocument':
        stepNo = 9;
        break;
      case 'asr':
        stepNo = 10;
        break;
      case 'review':
        stepNo = 11;
        break;
      default:
        stepNo = 1;
    }

    if (!Force_Google_Tag) {
      sendDataToSparkline(dataElement, code, stepNo, false, false, isInitial, id);
    }
  }

  componentDidUpdate(prevState) {
    const { commonReducer, applicationReducer } = this.props;
    if (prevState.commonReducer.appData !== commonReducer.appData) {
      const dataElement = commonReducer.appData.dataElementObj;
      const productCode = applicationReducer.productCode ? applicationReducer.productCode.toLowerCase() : "";
      if (!Force_Google_Tag) {
        sendDataToSparkline(dataElement, productCode, "", false, true);
      }
    }
  }

  isFromConfirmDetails() {
    return false;
  }

  // New isAllChecking
  isAllCheckingNew() {
    const { dispatch, commonReducer, applicationReducer, contactDetailsReducer, shareHoldersReducer, confirmTandCReducer, drawSignatureReducer, signatureReducer } = this.props;
    const { isKnockOut, isKnockOutField, isRetrieveApplication, isKeyManApply } = applicationReducer;
    const { entityType } = contactDetailsReducer;
    const { shareHoldersList } = shareHoldersReducer;
    const { acknowledgementCheckbox } = confirmTandCReducer;
    // check steps
    const currentSection = commonReducer.currentSection;
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "uploadDocument", "asrDetails", "confirmDetailsPage", "bookAppointment"];
    const indexOfSteps = steps.indexOf(currentSection);
    // check operating mandate
    const beforeASAPFalse = isKnockOutField.entitySubType && isKnockOutField.countryOfIncorporation && isKnockOutField.natureOfBusiness && isKnockOutField.ownership && isKnockOutField.askQuestion;
    const showOperatingMandate = !isKnockOut || (isKnockOut && !beforeASAPFalse && isKnockOutField.operatingMandateComplex);
    // check shareholders list
    let isShareHoldersList = "";
    if (shareHoldersList.value.length > 0) {
      isShareHoldersList = true;
    } else {
      isShareHoldersList = false;
    }
    if (indexOfSteps >= 0) {
      if (indexOfSteps > 0 && !this.isContactDetailsPassChecking()) {
        dispatch(scrollBackToSection('contactDetails'));
      }
      if (indexOfSteps === 0) {
        this.isContactDetailsPassChecking()
      }
    }

    if (indexOfSteps >= 1) {
      if (indexOfSteps > 1 && !this.isPersonalIncomeDetailsPassChecking()) {
        dispatch(scrollBackToSection('personalIncomeDetails'))
      }
      if (indexOfSteps === 1) {
        this.isPersonalIncomeDetailsPassChecking()
      }
    }

    if (indexOfSteps >= 2) {
      if (indexOfSteps > 2 && !this.isCompanyDetailsPassing()) {
        dispatch(scrollBackToSection('companyDetails'))
      }
      if (indexOfSteps === 2) {
        this.isCompanyDetailsPassing()
      }
    }

    if (indexOfSteps >= 3) {
      if (indexOfSteps > 3 && !this.isAccountSetupPassChecking()) {
        dispatch(scrollBackToSection('accountSetup'))
      }
      if (indexOfSteps === 3) {
        this.isAccountSetupPassChecking()
      }
    }

    if (showOperatingMandate && indexOfSteps >= 4) {
      if (indexOfSteps > 4 && !this.isOperatingMandatePassing()) {
        dispatch(scrollBackToSection('operatingMandate'))
      }
      if (indexOfSteps === 4) {
        this.isOperatingMandatePassing()
      }
    }

    if (!isKnockOut && entityType.value === 'LC' && isShareHoldersList && indexOfSteps >= 5) {
      if (indexOfSteps > 5 && !this.isShareHoldersListPassing()) {
        dispatch(scrollBackToSection('shareHolders'))
      }
      if (indexOfSteps === 5) {
        this.isShareHoldersListPassing()
      }
    }

    if (!isKnockOut && indexOfSteps >= 6) {
      if (indexOfSteps > 5 && !this.isCRSCheckingPass()) {
        dispatch(scrollBackToSection('taxSelfDeclarations'))
      }
      if (indexOfSteps === 6) {
        this.isCRSCheckingPass()
      }
    }

    if (!isKnockOut && indexOfSteps >= 7 && entityType.value === 'LC' && (applicationReducer.isWhichType === "STP" || isRetrieveApplication)) {
      if (indexOfSteps > 7 && !this.isDocumentUploadPass()) {
        dispatch(scrollBackToSection('uploadDocument'))
      }
      if (indexOfSteps === 7) {
        this.isDocumentUploadPass()
      }
    }

    if (indexOfSteps >= 9 && !acknowledgementCheckbox.isToggled) {
      const checkboxError = commonReducer.appData.confirmDetails.confirmTandC.errorMsgCheckbox
      dispatch(handleErrorMessage("acknowledgementCheckbox", checkboxError))
      dispatch(scrollToElement("confirmTandC"));
    }
    //   if (indexOfSteps >= 9 && !isKnockOut && isKeyManApply && ((commonReducer.isMobile && drawSignatureReducer.trimmedDataURL === null) || (!commonReducer.isMobile && signatureReducer.signaturePad.trimmedDataURL === null))) {
    if ( indexOfSteps >= 9 && !isKnockOut && isKeyManApply) {
      if(commonReducer.isMobile && (signatureReducer.signaturePad.trimmedDataURL === null || drawSignatureReducer.signaturePad.trimmedDataURL === null )) {
        dispatch(setSignatureUploadIsValidStatus(false))
      } else if (!commonReducer.isMobile && signatureReducer.signaturePad.trimmedDataURL === null) {
        const signatureUploadError = commonReducer.appData.signatureUpload.labels.error4
        dispatch(showErrorMessage(signatureUploadError));
      }
      dispatch(scrollToElement('signatureUpload-section'));
    }
  }

  handleGreyButton() {
    const { commonReducer, applicationReducer, contactDetailsReducer, shareHoldersReducer, confirmTandCReducer, drawSignatureReducer, signatureReducer, localAddressInputReducer, companyBasicDetailsReducer, accountSetupReducer, operatingMandateReducer, uploadDocumentsReducer, taxSelfDeclarationsReducer, personalIncomeDetailsReducer } = this.props;
    const { isKnockOut, isKnockOutField, isRetrieveApplication, isKeyManApply } = applicationReducer;
    const { entityType, emailAddress, primaryCountryOfOperation, primaryNatureOfBusiness, secondaryNatureOfBusiness, businessConstitution } = contactDetailsReducer;
    const { maritalStatus } = personalIncomeDetailsReducer;
    const { shareHoldersList } = shareHoldersReducer;
    const { acknowledgementCheckbox } = confirmTandCReducer;
    const { mailingAddressCheckbox, annualTurnover, primaryClientele } = companyBasicDetailsReducer;
    const {
      mailingUnit,
      mailingStreet,
      mailingBlock,
      mailingPostalCode,
      mailingLevel,
      mailingCity,
      mailingCountry,
      mailingLine1
    } = localAddressInputReducer;
    const {
      accountName, currencyOfAccount, purposeOfAccount, otherPurpose, others, registerOfPayNow,
      setupBIBPlus, BIBGroupId, BIBMobileNumber, BIBContactPerson, BIBEmailAddress, payNowID
    } = accountSetupReducer;
    const { signatoriesID, mainApplicantLegalId, signatorySelected } = operatingMandateReducer;
    const { shareholderID } = shareHoldersReducer;

    const { tinNoOthersId, checkboxOthers, checkboxUS, checkboxSG, checkboxNone, countryNone, tinNoSG, tinNoUS, isUSResident, fatcaCRSStatus, controllingPersonId, haveTINNoNone, tinNoNone, reasonsNone, othersNone } = taxSelfDeclarationsReducer;
    // check steps
    const currentSection = commonReducer.currentSection;
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "uploadDocument", "asrDetails", "confirmDetailsPage", "bookAppointment"];
    const indexOfSteps = steps.indexOf(currentSection);
    // check operating mandate
    const beforeASAPFalse = isKnockOutField.entitySubType && isKnockOutField.countryOfIncorporation && isKnockOutField.natureOfBusiness && isKnockOutField.ownership && isKnockOutField.askQuestion;
    const showOperatingMandate = !isKnockOut || (isKnockOut && !beforeASAPFalse && isKnockOutField.operatingMandateComplex);
    // check shareholders list
    let isShareHoldersList = "";
    if (shareHoldersList.value.length > 0) {
      isShareHoldersList = true;
    } else {
      isShareHoldersList = false;
    }

    let errorCount = 0;

    if (indexOfSteps >= 0) {
      const contactFieldValid = emailAddress.isValid && emailAddress.value !== '';
      const primaryCountryOfOperationValid = primaryCountryOfOperation.isValid && primaryCountryOfOperation.value !== '';
      const primaryNatureOfBusinessValid = primaryNatureOfBusiness.isValid && primaryNatureOfBusiness.value !== '';
      // const secondaryNatureOfBusinessCSS = primaryNatureOfBusinessValid ? "" : "lightGrey";
      const secondaryNatureOfBusinessValid = secondaryNatureOfBusiness.isValid && secondaryNatureOfBusiness.value !== '';
      const fullValidation = secondaryNatureOfBusinessValid && contactFieldValid && primaryCountryOfOperationValid && primaryNatureOfBusinessValid;
      if (!fullValidation) {
        errorCount++
      }
    }

    if (indexOfSteps >= 1) {
      const maritalStatusValid = maritalStatus.value !== "" && maritalStatus.isValid;
      if (!maritalStatusValid) {
        errorCount++;
      }
    }

    if (indexOfSteps >= 2) {
      if (annualTurnover.value === "" || !annualTurnover.isValid) {
        errorCount++;
      }

      if (primaryClientele.length === 0) {
        errorCount++;
      }

      if (mailingAddressCheckbox.isToggled) {
        if (mailingCountry.value !== "SG") {
          if (mailingCity.value === "" || !mailingCity.isValid || mailingLine1.value === "" || !mailingLine1.isValid) {
            errorCount++;
          }
        } else {
          if (mailingBlock.value === "" ||
            mailingLevel.value === "" ||
            mailingPostalCode.value === "" ||
            mailingStreet.value === "" ||
            mailingUnit.value === "" ||
            !mailingBlock.isValid ||
            !mailingLevel.isValid ||
            !mailingPostalCode.isValid ||
            !mailingStreet.isValid ||
            !mailingUnit.isValid) {
            errorCount++
          }
        }
      }
    }

    if (indexOfSteps >= 3) {
      if (purposeOfAccount.length === 0) {
        errorCount++;
      }

      if (others.isToggled) {
        if (otherPurpose.value === "" || !otherPurpose.isValid)
          errorCount++;
      }

      if (
        accountName.value === "" || !accountName.isValid ||
        currencyOfAccount.value.toLowerCase() !== "sgd" || !currencyOfAccount.isValid
      ) {
        errorCount++;
      }

      if (registerOfPayNow.isToggled && !payNowID.isValid) {
        errorCount++;
      }

      if (BIBMobileNumber.value === "" || !BIBMobileNumber.isValid ||
        BIBContactPerson.value === "" || !BIBContactPerson.isValid ||
        BIBEmailAddress.value === "" || !BIBEmailAddress.isValid
      ) {
        errorCount++;
      }

      if (setupBIBPlus.isToggled && !BIBGroupId.isValid) {
        errorCount++;
      }
    }

    if (showOperatingMandate && indexOfSteps >= 4) {
      const isBibPlusUserToggled = accountSetupReducer.setupBIBPlus.isToggled;
      if (signatoriesID.length > 0) {
        operatingMandateReducer.signatoriesID.map((key) => {
          const signatoriesEmail = operatingMandateReducer[`signatories${key}`].signatoriesEmail;
          const signatoriesNRIC = operatingMandateReducer[`signatories${key}`].signatoriesNRIC;
          const signatoriesName = operatingMandateReducer[`signatories${key}`].signatoriesName;
          const signatoriesMobileNo = operatingMandateReducer[`signatories${key}`].signatoriesMobileNo;
          const signatoriesBibUserId = operatingMandateReducer[`signatories${key}`].signatoriesBibUserId;
          const signatoriesToggleBIBUser = operatingMandateReducer[`signatories${key}`].signatoriesToggleBIBUser;

          const isApprovedSignatoryValid = signatoriesEmail.value !== "" && signatoriesEmail.isValid && signatoriesNRIC.value !== "" && signatoriesNRIC.isValid && signatoriesName.value !== "" && signatoriesName.isValid && signatoriesMobileNo.value !== "" && signatoriesMobileNo.value !== '+65' && signatoriesMobileNo.isValid;
          let isBibUserIdValid = true;

          if (mainApplicantLegalId === operatingMandateReducer[`signatories${key}`].signatoriesNRIC.value && isBibPlusUserToggled && signatoriesToggleBIBUser.isToggled) {
            isBibUserIdValid = signatoriesBibUserId.value !== "" && signatoriesBibUserId.isValid;
          }


          if (!isApprovedSignatoryValid || !isBibUserIdValid) {
            errorCount++
          }
          return errorCount;
        })
      }

      if (signatorySelected.value === "" || !signatorySelected.isValid) {
        errorCount++;
      }
    }

    if (!isKnockOut && entityType.value === 'LC' && isShareHoldersList && indexOfSteps >= 5) {
      if (shareholderID.length > 0) {
        shareHoldersReducer.shareholderID.map((key) => {
          const shareholderEmail = shareHoldersReducer[`shareholder${key}`].shareholderEmail;
          const shareholderNRIC = shareHoldersReducer[`shareholder${key}`].shareholderNRIC;
          const shareholderName = shareHoldersReducer[`shareholder${key}`].shareholderName;
          const shareholderMobileNo = shareHoldersReducer[`shareholder${key}`].shareholderMobileNo;

          const isApprovedshareHoldersValid = shareholderEmail.value !== "" && shareholderEmail.isValid && shareholderNRIC.value !== "" && shareholderNRIC.isValid && shareholderName.value !== "" && shareholderName.isValid && shareholderMobileNo.value !== "" && shareholderMobileNo.value !== '+65' && shareholderMobileNo.isValid;

          if (!isApprovedshareHoldersValid) {
            errorCount++
          }
          return errorCount;
        })
      }
    }

    if (!isKnockOut && indexOfSteps >= 6) {
      const if1Checked = checkboxNone.isToggled || checkboxOthers.isToggled || checkboxSG.isToggled || checkboxUS.isToggled;
      const isNotSoleProp = businessConstitution.value !== "S";
      const checkTINNOYes = countryNone.value !== "" && countryNone.isValid && tinNoNone.value !== "" && tinNoNone.isValid ? true : false;
      const checkReason = reasonsNone.value === "3" ? othersNone.isValid && othersNone.value !== "" : true;
      const checkTINNONo = countryNone.value !== "" && countryNone.isValid && reasonsNone.value !== "" && reasonsNone.isValid ? checkReason : false;
      if (if1Checked) {
        if (checkboxSG.isToggled) {
          if (!(tinNoSG.value !== "" && tinNoSG.isValid)) {
            errorCount++;
          }
        }

        if (checkboxUS.isToggled) {
          if (!(tinNoUS.value !== "" && tinNoUS.isValid && isUSResident.value !== "" && isUSResident.isValid)) {
            errorCount++;
          }
        }

        if (checkboxOthers.isToggled) {
          if (tinNoOthersId.length > 0) {
            let duplicateKeys = [];
            for (let j = 0; j < tinNoOthersId.length; j++) {
              for (let k = j + 1; k < tinNoOthersId.length; k++) {
                const key = tinNoOthersId[k];
                const compareKey = tinNoOthersId[j];
                if (k !== j && taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value === taxSelfDeclarationsReducer[`tinNoOthers${compareKey}`].othersCountry.value) {
                  duplicateKeys.push(tinNoOthersId[k]);
                  duplicateKeys.push(tinNoOthersId[j]);
                }
              }
            }
            duplicateKeys = eliminateDuplicates(duplicateKeys);
            if (duplicateKeys.length > 0) {
              for (let i = 0; i < duplicateKeys.length; i++) {
                errorCount++;
              }
            }

            for (let j = 0; j < tinNoOthersId.length; j++) {
              for (let k = j + 1; k < tinNoOthersId.length; k++) {
                const key = tinNoOthersId[k];
                const compareKey = tinNoOthersId[j];
                if (k !== j && taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry.value === taxSelfDeclarationsReducer[`tinNoOthers${compareKey}`].othersCountry.value) {
                  errorCount++;
                }
              }
            }

            taxSelfDeclarationsReducer.tinNoOthersId.map((key) => {
              const haveTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].haveTINNo;
              const othersCountry = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersCountry;
              const othersTINNo = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersTINNo;
              const reasons = taxSelfDeclarationsReducer[`tinNoOthers${key}`].reasons;
              const othersOthers = taxSelfDeclarationsReducer[`tinNoOthers${key}`].othersOthers;

              const checkTINNOYes = othersCountry.value !== "" && othersCountry.isValid && othersTINNo.value !== "" && othersTINNo.isValid ? true : false;
              const checkReason = reasons.value === "3" ? othersOthers.isValid && othersOthers.value !== "" : true;
              const checkTINNONo = othersCountry.value !== "" && othersCountry.isValid && reasons.value !== "" && reasons.isValid ? checkReason : false;

              if (haveTINNo.value !== "") {
                if (haveTINNo.value === "Y") {
                  if (!checkTINNOYes) {
                    errorCount++;
                  }
                } else {
                  if (!checkTINNONo) {
                    errorCount++;
                  }
                }
              } else {
                errorCount++;
              }
              return errorCount;
            })
          }
        }

        if (checkboxNone.isToggled) {
          if (haveTINNoNone.value !== "") {
            if (haveTINNoNone.value === "Y") {
              if (!checkTINNOYes) {
                errorCount++;
              }
            } else {
              if (!checkTINNONo) {
                errorCount++;
              }
            }
          } else {
            errorCount++;
          }
        }
      } else {
        errorCount++;
      }

      if (isNotSoleProp && fatcaCRSStatus.value === "") {
        errorCount++;
      }

      if (isNotSoleProp && fatcaCRSStatus.value === "PB" && controllingPersonId && controllingPersonId.length > 0) {
        let duplicateKeysCP = [];
        for (let j = 0; j < controllingPersonId.length; j++) {
          for (let k = j + 1; k < controllingPersonId.length; k++) {
            const key = controllingPersonId[k];
            const compareKey = controllingPersonId[j];
            if (k !== j && taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo.value === taxSelfDeclarationsReducer[`controllingPerson${compareKey}`].cpIdNo.value) {
              duplicateKeysCP.push(controllingPersonId[k]);
              duplicateKeysCP.push(controllingPersonId[j]);
            }
          }
        }
        duplicateKeysCP = eliminateDuplicates(duplicateKeysCP);
        if (duplicateKeysCP.length > 0) {
          for (let i = 0; i < duplicateKeysCP.length; i++) {
            errorCount++;
          }
        }
        taxSelfDeclarationsReducer.controllingPersonId.map((key) => {
          const cpName = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpName;
          const cpIdNo = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpIdNo;
          const cpMobileNo = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpMobileNo;
          const cpEmail = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpEmail;
          const cpPercentageOwnership = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership;

          const checkboxOthers = taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxOthers;
          const checkboxSG = taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxSG;
          const checkboxUS = taxSelfDeclarationsReducer[`controllingPerson${key}`].checkboxUS;
          const tinNoSG = taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoSG;
          const tinNoUS = taxSelfDeclarationsReducer[`controllingPerson${key}`].tinNoUS;
          const tinNoOthersId = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthersId`];

          const if1Checked = checkboxOthers.isToggled || checkboxSG.isToggled || checkboxUS.isToggled;

          if (!if1Checked) {
            errorCount++;
          }

          if (cpMobileNo.value === '+65') {
            errorCount++;
          }

          let duplicateKeys = [];
          for (let j = 0; j < tinNoOthersId.length; j++) {
            for (let k = j + 1; k < tinNoOthersId.length; k++) {
              const tKey = tinNoOthersId[k];
              const compareKey = tinNoOthersId[j];
              if (k !== j && taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].othersCountry.value === taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${compareKey}`].othersCountry.value) {
                duplicateKeys.push(tinNoOthersId[k]);
                duplicateKeys.push(tinNoOthersId[j]);
              }
            }
          }
          duplicateKeys = eliminateDuplicates(duplicateKeys);
          if (duplicateKeys.length > 0) {
            for (let i = 0; i < duplicateKeys.length; i++) {
              errorCount++;
            }
          }

          if (cpName.value === "" || !cpName.isValid || cpIdNo.value === "" || !cpIdNo.isValid || cpMobileNo.value === "" || !cpMobileNo.isValid || cpEmail.value === "" || !cpEmail.isValid || cpPercentageOwnership.value === "" || !cpPercentageOwnership.isValid) {
            errorCount++;
          }

          if (checkboxSG.isToggled) {
            if (tinNoSG.value === "" || !tinNoSG.isValid) {
              errorCount++;
            }
          }

          if (checkboxUS.isToggled) {
            if (tinNoUS.value === "" || !tinNoUS.isValid) {
              errorCount++;
            }
          }

          if (tinNoOthersId && tinNoOthersId.length > 0) {
            taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthersId`].map((tKey) => {
              const haveTINNo = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].haveTINNo;
              const othersCountry = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].othersCountry;
              const othersTINNo = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].othersTINNo;
              const reasons = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].reasons;
              const othersOthers = taxSelfDeclarationsReducer[`controllingPerson${key}`][`tinNoOthers${tKey}`].othersOthers;


              if (checkboxOthers.isToggled) {
                if (haveTINNo.value !== "") {
                  if (haveTINNo.value === "Y") {
                    if (othersCountry.value === "" || !othersCountry.isValid || othersTINNo.value === "" || !othersTINNo.isValid) {
                      errorCount++;
                    }
                  } else {
                    if (reasons.value === "3") {
                      if (othersCountry.value === "" || !othersCountry.isValid || reasons.value === "" || !reasons.isValid || othersOthers.value === "" || !othersOthers.isValid) {
                        errorCount++;
                      }
                    } else {
                      if (othersCountry.value === "" || !othersCountry.isValid || reasons.value === "" || !reasons.isValid) {
                        errorCount++;
                      }
                    }
                  }
                } else {
                  if (othersCountry.value === "" || !othersCountry.isValid || haveTINNo.value === "" || !haveTINNo.isValid) {
                    errorCount++;
                  }
                }
              }
              return errorCount;
            })
          }
          return errorCount;
        })

        let percentageTotal = 0;
        taxSelfDeclarationsReducer.controllingPersonId.map((key) => {
          const cpPercentageOwnership = taxSelfDeclarationsReducer[`controllingPerson${key}`].cpPercentageOwnership;
          percentageTotal = percentageTotal + parseInt(cpPercentageOwnership.value, 10);
          return percentageTotal;
        })
        if (percentageTotal > 100) {
          errorCount++;
        }
      }
    }

    if (!isKnockOut && indexOfSteps >= 7 && entityType.value === 'LC' && (applicationReducer.isWhichType === "STP" || isRetrieveApplication)) {
      if (
        !uploadDocumentsReducer.MADocument.isValid || uploadDocumentsReducer.MADocument.inputValue === "") {
        errorCount++;
      }
    }


    if (indexOfSteps >= 9) {
      if (!acknowledgementCheckbox.isToggled) {
        errorCount++;
      }

      if (!isKnockOut && isKeyManApply) {
        if(commonReducer.isMobile) {
          if(signatureReducer.isSign && drawSignatureReducer.signaturePad.trimmedDataURL === null) {
             errorCount++;
          } else if (!signatureReducer.isSign && signatureReducer.signaturePad.trimmedDataURL === null) {
            errorCount++;
          }
        } else if (!commonReducer.isMobile && signatureReducer.signaturePad.trimmedDataURL === null) {
          errorCount++;
        }
      }

      /*if (isKnockOut && (bookingBranch.value === "" || !bookingBranch.isValid)) {
        errorCount++
      }*/
    }

    if (errorCount > 0) {
      return false
    }

    return true
  }

  handleToErrorLocal(e) {
    const { commonReducer, dispatch , applicationReducer} = this.props;
    const duplicateTabId = commonReducer.duplicateTab;
    const localStoreId = localStore.getStore("duplicateTabs");
    const productCode = localStore.getStore("productCode");

    let count = 0;
    const supportOS = ["Chrome", "Firefox"];
    supportOS.map((i) => {
      const str = window.navigator.userAgent.toString();
      if (str.indexOf(i) >= 0){
        count ++;
      }
      return count
    });

    if (count > 0) {
      if( duplicateTabId !== localStoreId && applicationReducer.productCode === productCode ) {
        dispatch(setErrorWithValue("DuplicatedTabMsg"));
        this.props.history.push({
          pathname: `error`,
        })
      }
    }
  }

  componentDidMount() {
    window.addEventListener('storage', (e) => this.handleToErrorLocal(e));
  }

  componentWillUnmount() {
    window.removeEventListener('storage', (e) => this.handleToErrorLocal(e));
  }

  render() {
    const { commonReducer, applicationReducer, contactDetailsReducer, shareHoldersReducer, bookAppointmentReducer, saveAndExitPopupReducer } = this.props;
    const { entityType, businessConstitution } = contactDetailsReducer;
    const { isKnockOut, isKnockOutField, isRetrieveApplication, sessionExpirePopup } = applicationReducer;
    // const { asrData } = asrDetailsReducer;
    const { shareHoldersList } = shareHoldersReducer;
    const { showSaveAndExitPopup } = saveAndExitPopupReducer;
    // const { approvedSignatories, signatorySelected } = operatingMandateReducer;
    const currentSection = commonReducer.currentSection;
    const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "uploadDocument", "asrDetails", "confirmDetailsPage", "bookAppointment"];
    const indexOfSteps = steps.indexOf(currentSection);
    let isShareHoldersList = "";
    if (shareHoldersList.value.length > 0) {
      isShareHoldersList = true;
    } else {
      isShareHoldersList = false;
    }
    const isCallNumberPopup = commonReducer.callNumberPopup;

    const beforeASAPFalse = isKnockOutField.entitySubType && isKnockOutField.countryOfIncorporation && isKnockOutField.natureOfBusiness && isKnockOutField.ownership && isKnockOutField.askQuestion;
    const showOperatingMandate = !isKnockOut || (isKnockOut && !beforeASAPFalse && isKnockOutField.operatingMandateComplex);
    return (

      <WrapContainer {...this.props} clickBookAppointment={(status) => this.handleBookAppointmentTogglePopUp(status)}>
        {(currentSection !== "thankyou" && bookAppointmentReducer.isBookingPopupShow && indexOfSteps >= 1) ?
          <BookAppointmentPage {...this.props} onCheck={() => this.isAllBookingChecking()}
            onContinue={() => this.handleBookAppointmentSubmitApplication()}
            isFromConfirmDetails={() => this.isFromConfirmDetails()}
          /> :
          (currentSection === "thankyou" ? <ThankYouPage /> :
            <div>
              {sessionExpirePopup && <SessionExpirePopup {...this.props} onContinue={() => this.handleToUOBSite()} />}
              {showSaveAndExitPopup && <SaveAndExitPopup {...this.props} onContinue={() => this.handleToParentSite()} />}

              <ContactDetailsPage {...this.props} onContinue={() => this.handleInitiateApplication()}
                onCheck={() => this.isAllCheckingNew()} onFixedButton={() => this.handleGreyButton()} />
              {indexOfSteps >= 1 &&
                <PersonalIncomeDetailsPage {...this.props} onContinue={() => this.handleToCompanyDetails("personalDetails")} onCheck={() => this.isAllCheckingNew()} onSaveAndExit={() => this.handleSaveAndExit('personalIncomeDetailsReducer')} onFixedButton={() => this.handleGreyButton()} />}
              {indexOfSteps >= 2 &&
                <CompanyDetailsPage {...this.props} onContinue={() => this.handlePopup("companyDetails")} onCheck={() => this.isAllCheckingNew()} onSaveAndExit={() => this.handleSaveAndExit('companyBasicDetailsReducer')} onFixedButton={() => this.handleGreyButton()} />}
              {indexOfSteps >= 3 && <AccountSetupPage {...this.props} onContinue={() => { isKnockOut ? this.handleToReviewPage("accountSetup") : this.handleToOperatingMandatePage("accountSetup") }} onCheck={() => this.isAllCheckingNew()} onSaveAndExit={() => this.handleSaveAndExit('accountSetupReducer')} onFixedButton={() => this.handleGreyButton()} />}
              {indexOfSteps >= 4 && showOperatingMandate &&
                <OperatingMandatePage {...this.props}
                  onContinue={() => {
                    isShareHoldersList ? this.handleToShareHoldersPage("operatingMandate") : this.handleToTaxSelfDeclarationsPage("operatingMandate")
                  }
                  } onCheck={() => this.isAllCheckingNew()} onHandleToReview={() => this.handleToReviewPage("operatingMandate")} onSaveAndExit={() => this.handleSaveAndExit('operatingMandateReducer')} onFixedButton={() => this.handleGreyButton()} />}

              {
                !isKnockOut && isShareHoldersList && indexOfSteps >= 5 &&
                <ShareHoldersPage {...this.props} onContinue={() => { this.handleToTaxSelfDeclarationsPage("shareholderList") }} onCheck={() => this.isAllCheckingNew()} onSaveAndExit={() => this.handleSaveAndExit('shareHoldersReducer')} onFixedButton={() => this.handleGreyButton()} />
              }
              {indexOfSteps >= 6 && !isKnockOut && <TaxSelfDeclarationsPage {...this.props} onContinue={() => { businessConstitution.value !== 'S' ? entityType.value === "LC" ? this.handleToDocumentUploadPage("taxSelfDeclarationDetails") : this.handleToASRDetailsPage("taxSelfDeclarationDetails") : this.handleToReviewPage("taxSelfDeclarationDetails") }} onCheck={() => this.isAllCheckingNew()} onSaveAndExit={() => this.handleSaveAndExit('taxSelfDeclarationsReducer')} onFixedButton={() => this.handleGreyButton()} />}

              {
                indexOfSteps >= 7 && entityType.value === 'LC' && (applicationReducer.isWhichType === "STP" || isRetrieveApplication) && !isKnockOut &&
                <UploadDocument {...this.props} onContinue={() => this.handleToASRDetailsPage("uploadDocument")}
                  onCheck={() => this.isAllCheckingNew()} onSaveAndExit={() => this.handleSaveAndExit('uploadDocumentsReducer')} onFixedButton={() => this.handleGreyButton()} />
              }

              {
                businessConstitution.value !== 'S' && !isKnockOut && indexOfSteps >= 8 && <ASRDetailsPage {...this.props} onContinue={() => this.handleToReviewPage("asr")} onSaveAndExit={() => this.handleSaveAndExit()} onFixedButton={() => this.handleGreyButton()} onCheck={() => this.isAllCheckingNew()} />
              }
              {indexOfSteps >= 9 && <ConfirmDetailsPage {...this.props}
                onContinue={() => this.handleSubmitApplication("review")}
                onCheck={() => this.isAllCheckingNew()}
                onFixedButton={() => this.handleGreyButton()}
                openBookAppointment={() => this.handleBookAppointmentTogglePopUp(true)}
              />}
            </div>
          )
        }
      </WrapContainer>
    );
  }
}

const mapStateToProps = state => {

  const { commonReducer, applicationReducer, retrievingReducer, contactDetailsReducer, personalIncomeDetailsReducer, companyBasicDetailsReducer, localAddressInputReducer, accountSetupReducer, confirmTandCReducer, drawSignatureReducer, signatureReducer, operatingMandateReducer, asrDetailsReducer, shareHoldersReducer, taxSelfDeclarationsReducer, uploadDocumentsReducer, bookAppointmentReducer, saveAndExitPopupReducer } = state;
  return {
    commonReducer,
    applicationReducer,
    retrievingReducer,
    contactDetailsReducer,
    personalIncomeDetailsReducer,
    companyBasicDetailsReducer,
    localAddressInputReducer,
    accountSetupReducer,
    confirmTandCReducer,
    signatureReducer,
    drawSignatureReducer,
    operatingMandateReducer,
    asrDetailsReducer,
    shareHoldersReducer,
    taxSelfDeclarationsReducer,
    uploadDocumentsReducer,
    bookAppointmentReducer,
    saveAndExitPopupReducer
  };
};

export default connect(mapStateToProps)(ApplicationContainer);
