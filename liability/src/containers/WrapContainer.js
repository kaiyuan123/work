import React, { Component } from "react";
import { connect } from "react-redux";
import { getInitialData, setErrorMessage, scrollToElement, callNumberPopup } from "./../actions/commonAction";
import GenericErrorMessage from "./../components/GenericErrorMessage/GenericErrorMessage";
import Loader from "./../components/Loader/Loader";
import NavItem from "./../components/NavItem/NavItem";
import { handleHideAskQuestionPopup } from "./../actions/askQuestionsAction";
import { toggleNav } from "./../actions/commonAction";
import "./../assets/css/uob-form-loan.css";
import "./../assets/css/eBiz-style.css";
import closeIcon from "./../assets/images/cross-grey.svg";
import closeIconWhite from "./../assets/images/x-white.svg";
import bookingIcon from "./../assets/images/googleMap/bookingIcon.svg";
import phoneIcon from "./../assets/images/phone.svg";
import { sendDataToSparkline } from "./../common/utils";
import { Force_Google_Tag } from "./../api/httpApi";
import { showSaveAndExitPopup } from "./../actions/saveAndExitPopupAction";
import CallNumberPopup from "./../pages/CallNumberPopup";
// import Beforeunload from "react-beforeunload";
import localStore from './../common/localStore';

class WrapContainer extends Component {
	componentWillMount() {
		const { dispatch } = this.props;
		dispatch(getInitialData());
	}

	closeNav() {
		const { commonReducer, saveAndExitPopupReducer, askQuestionsReducer, dispatch } = this.props;
		const { popuptoggle } = askQuestionsReducer;
		const { showSaveAndExitPopup } = saveAndExitPopupReducer;
		if (!showSaveAndExitPopup && commonReducer.toggleNav) {
			dispatch(toggleNav(true));
		}
	}

	handleOnClearMessage() {
		const { dispatch } = this.props;
		dispatch(setErrorMessage(""));
	}

	handlePopup(popuptoggle) {
		const { dispatch } = this.props;
		dispatch(handleHideAskQuestionPopup(popuptoggle));
	}

	showBookAppointment(status) {
		const { commonReducer, applicationReducer } = this.props;
		const { isInitial, initiateMyinfoData, productCode } = applicationReducer;
		if (status === true) {
			if (!Force_Google_Tag) {
				const dataElement = commonReducer.appData.dataElementObj;
				const id = initiateMyinfoData && initiateMyinfoData.referenceNumber && isInitial ? initiateMyinfoData.referenceNumber : "";
				const code = productCode ? productCode.toLowerCase() : "";
				let pathname = window.location.pathname;
				sendDataToSparkline(dataElement, code, "", false, false, isInitial, id, false, true, pathname);
			}
		}
		this.props.clickBookAppointment(status);
	}

	handleClearStore() {
		localStore.clear()
	}

	toggleNav(showNav) {
		const { dispatch } = this.props;
		dispatch(toggleNav(showNav));
	}

	handleSaveAndExit() {
		const { dispatch } = this.props;
		let fieldName;
		if (document.querySelectorAll('.textInputLabel-container--error').length > 0) {
			fieldName = document.getElementsByClassName('textInputLabel-container--error')[0].id;
			dispatch(scrollToElement(fieldName));
		} else if (document.querySelectorAll('.PhoneInputContainer--error').length > 0) {
			fieldName = document.getElementsByClassName('PhoneInputContainer--error')[0].id;
			dispatch(scrollToElement(fieldName));
		} else if (document.querySelectorAll('.textInputLabel--error').length > 0) {
			fieldName = document.getElementsByClassName('textInputLabel--error')[0].id;
			dispatch(scrollToElement(fieldName));
		} else {
			dispatch(showSaveAndExitPopup(true));
		}
	}

	showCallNumberPopup(status) {
		const { dispatch } = this.props;
		dispatch(callNumberPopup(status));
	}

	render() {
		const { commonReducer, retrievingReducer, contactDetailsReducer, askQuestionsReducer, applicationReducer, shareHoldersReducer, bookAppointmentReducer, saveAndExitPopupReducer } = this.props;
		const { isKnockOut, hideNavBar, isKnockOutField, isRetrieveApplication } = applicationReducer;
		const { entityType, businessConstitution } = contactDetailsReducer;
		const { popuptoggle } = askQuestionsReducer;
		const { shareHoldersList } = shareHoldersReducer;

		let imgUrlWhite = "./images/logos/uob-eBiz-Logo.svg"; //white UOB text
		let imgUrl = "./images/logos/uob-eBiz-Logo-blue.svg"; //blue UOB text
		let rightByYouLogo = "./images/logos/right-by-you-logo.jpg"; //rightByYouLogo
		let pathname = window.location.pathname;
		const currentPath = pathname.substr(pathname.lastIndexOf('/') + 1);
		const currentSection = commonReducer.currentSection;
		const steps = ["contactDetails", "personalIncomeDetails", "companyDetails", "accountSetup", "operatingMandate", "shareHolders", "taxSelfDeclarations", "uploadDocument", "asrDetails", "confirmDetailsPage", "bookAppointment"];
		const indexOfSteps = steps.indexOf(currentSection);
		const whiteScreenLogo = currentPath === "error" || currentSection === "thankyou";
		const productCode = applicationReducer.productCode !== "" && applicationReducer.productCode ? (applicationReducer.productCode).toUpperCase() : "";
		const productTitle = commonReducer.appData !== null && commonReducer.appData.productTitle[productCode];
		const invalidCode = commonReducer.appData !== null && commonReducer.appData.validProductCode.indexOf(productCode.toLowerCase()) >= 0;
		const productTitleError = commonReducer.appData !== null && commonReducer.appData.productTitle[productCode];
		const beforeASAPFalse = isKnockOutField.entitySubType && isKnockOutField.countryOfIncorporation && isKnockOutField.natureOfBusiness && isKnockOutField.ownership && isKnockOutField.askQuestion;
		const showOperatingMandate = !isKnockOut || (isKnockOut && !beforeASAPFalse && isKnockOutField.operatingMandateComplex);
		let isShareHoldersList = "";
		if (shareHoldersList.value.length > 0) {
			isShareHoldersList = true;
		} else {
			isShareHoldersList = false;
		}

		const navbarLabel = commonReducer.appData !== null && commonReducer.appData.navBar;
		const { showSaveAndExitPopup } = saveAndExitPopupReducer;
		let isPopup = popuptoggle ? 'popup-on' : 'popup-off';
		let isdottedLine = false;

		if (indexOfSteps === 0) {
			isdottedLine = true
		} else if (applicationReducer.isWhichType === "STP" && indexOfSteps > 0 && indexOfSteps < 5 && !isKnockOut) {
			isdottedLine = true
		}

		const isfullwhite = whiteScreenLogo ? "whitePage-header" : "";
		const navBarHide = whiteScreenLogo ? "whitepage-navBar" : "";
		const toggleNavCSS = commonReducer.toggleNav ? "showNav" : "hideNav";
		const stepsLength = steps.length - 1;
		const progressPercentage = ((indexOfSteps + 1) / stepsLength) * 100;
		const isCallNumberPopup = commonReducer.callNumberPopup;
		const isAndroid = navigator.userAgent.toLowerCase().indexOf("android") > -1;
		return (
			<div>
				{isCallNumberPopup && <CallNumberPopup {...this.props} />}

				<div className={`uob-form-loan ${isPopup} ${isfullwhite}`}>
					{commonReducer.isLoading && <Loader />}
					{whiteScreenLogo &&
						<div className={`apply-page-header-container ${isfullwhite}`}>
							<div className="apply-page-header-title apply">
								<img className="img-size" alt="logo" src={imgUrl} />								
								{invalidCode && <div className="title-text"> {productTitleError} </div>}
							</div>
							<div className="displayHeader">
				                <img className="img-size" alt="logo" src='./images/logos/uob-eBiz-Logo-blue.svg' />
				                {
				                	productTitle ? <div className="cont-wrap">
					                	 <div className="title-vertical-line"/>
					                	 <div className="mob-prodtitle">
						                  {productTitle.split(" | ")[0]}
						                  <br/>
						                  <span className="font-16">{productTitle.split(" | ")[1]}</span>
						                </div> 
						            </div> : ''
				                }
				               
				                
				              </div>
							<div className="right-by-you-container">
								<img alt="right-by-you" src={rightByYouLogo} />
							</div>
						</div>
					}
					<div>
						{commonReducer.appData &&
							<nav
								className={`navbar navbar-expand-lg navbar-dark bg-primary-left fixed-top ${toggleNavCSS}`}
								id="sideNav"
							>
								<span className="sideBarCloseImage" onClick={() => this.toggleNav(true)}>
									<img className="close-icon" src={closeIconWhite} alt="Close-Icon" width={20} height={20} />
								</span>
								<div className="leftTopLogo">
									<img src={imgUrlWhite} alt="Logo" className="uob-logo" width="160" />
								</div>
								<div
									className="collapse navbar-collapse"
									id="navbarSupportedContent"
								>
									<ul className="navbar-nav">
										<NavItem
											defaultStep={true}
											navTitle={navbarLabel.myInfo}
											defaultLink={true}
											stepFinished={true}
										/>
										<NavItem
											stepFinished={indexOfSteps > 0}
											currentActiveStep={indexOfSteps === 0}
											linkSelected={
												(indexOfSteps === 0 && this.props.location.hash === "") ||
												(indexOfSteps >= 0 &&
													this.props.location.hash === "#contactDetails-section")
											}
											linkIdName={"#contactDetails-section"}
											navTitle={navbarLabel.contactDetails}
											onClick={() => {
												popuptoggle && this.handlePopup(false)
											}}

										/>
										<NavItem
											stepFinished={indexOfSteps > 1}
											currentActiveStep={indexOfSteps === 1}
											linkSelected={
												(indexOfSteps === 1 && this.props.location.hash === "") ||
												(indexOfSteps >= 1 &&
													this.props.location.hash ===
													"#personalIncomeDetails-section")
											}
											linkIdName={"#personalIncomeDetails-section"}
											navTitle={navbarLabel.personalIncomeDetails}
											onClick={() => {
												popuptoggle && this.handlePopup(false)
											}}
										/>
										<NavItem
											stepFinished={indexOfSteps > 2}
											currentActiveStep={indexOfSteps === 2}
											linkSelected={
												(indexOfSteps === 2 && this.props.location.hash === "") ||
												(indexOfSteps >= 2 &&
													this.props.location.hash === "#companyDetails-section")
											}
											linkIdName={"#companyDetails-section"}
											navTitle={navbarLabel.businessDetails}
											onClick={() => {
												popuptoggle && this.handlePopup(false)
											}}
										/>
										<NavItem
											stepFinished={indexOfSteps > 3}
											currentActiveStep={indexOfSteps === 3}
											linkSelected={
												(indexOfSteps === 3 && this.props.location.hash === "") ||
												(indexOfSteps >= 3 && this.props.location.hash === "#accountSetup-section")
											}
											linkIdName={"#accountSetup-section"}
											navTitle={navbarLabel.accountSetup}
										/>
										{!hideNavBar && showOperatingMandate && indexOfSteps > 3 &&
											<NavItem
												stepFinished={indexOfSteps > 4}
												currentActiveStep={indexOfSteps === 4}
												linkSelected={
													(indexOfSteps === 4 && this.props.location.hash === "") ||
													(indexOfSteps >= 4 && this.props.location.hash === "#operatingMandate-section")
												}
												linkIdName={"#operatingMandate-section"}
												navTitle={navbarLabel.operatingMandate}
											/>
										}

										{!hideNavBar && !isKnockOut && indexOfSteps > 4 &&
											<div>
												{
													entityType.value === 'LC' && isShareHoldersList && <NavItem
														stepFinished={indexOfSteps > 5}
														currentActiveStep={indexOfSteps === 5}
														linkSelected={
															(indexOfSteps === 5 && this.props.location.hash === "") ||
															(indexOfSteps >= 5 && this.props.location.hash === "#shareHolders-section")
														}
														linkIdName={"#shareHolders-section"}
														navTitle={navbarLabel.shareholderDetails}
													/>
												}

												<NavItem
													stepFinished={indexOfSteps > 6}
													currentActiveStep={indexOfSteps === 6}
													linkSelected={
														(indexOfSteps === 6 && this.props.location.hash === "") ||
														(indexOfSteps >= 6 && this.props.location.hash === "#taxSelfDeclarations-section")
													}
													linkIdName={"#taxSelfDeclarations-section"}
													navTitle={navbarLabel.taxSelfDeclaration}
												/>
												{entityType.value === 'LC' && (applicationReducer.isWhichType === "STP" || isRetrieveApplication) &&
													<NavItem
														stepFinished={indexOfSteps > 7}
														currentActiveStep={indexOfSteps === 7}
														linkSelected={
															(indexOfSteps === 7 && this.props.location.hash === "") ||
															(indexOfSteps >= 7 && this.props.location.hash === "#uploadDocument-section")
														}
														linkIdName={"#uploadDocument-section"}
														navTitle={navbarLabel.documentUpload}
													/>
												}

												{
													businessConstitution.value !== 'S' && <NavItem
														stepFinished={indexOfSteps > 8}
														currentActiveStep={indexOfSteps === 8}
														linkSelected={
															(indexOfSteps === 8 && this.props.location.hash === "") ||
															(indexOfSteps >= 8 && this.props.location.hash === "#asrDetails-section")
														}
														linkIdName={"#asrDetails-section"}
														navTitle={navbarLabel.asr}
													/>
												}

											</div>
										}

										<NavItem
											stepFinished={indexOfSteps > 9}
											currentActiveStep={indexOfSteps === 9}
											linkSelected={
												(indexOfSteps === 9 && this.props.location.hash === "") ||
												(indexOfSteps >= 9 &&
													this.props.location.hash ===
													"#confirmDetailsPage-section")
											}
											linkIdName={"#confirmDetailsPage-section"}
											navTitle={navbarLabel.review}
											dottedLine={isdottedLine}
										/>
										{indexOfSteps > 0 && <li className="bookAppointmentNavMain p-relative">
											<div>
												<table className="bookAppointmentTable">
													<tbody>
														<tr>
															<td colSpan={2}>
																<div className="bookAppointmentNavTitle">{navbarLabel.needHelp}</div>
															</td>
														</tr>
														<tr>
															<td style={{ width: '28px' }}>
																<img src={phoneIcon} alt="phoneIcon" width={13} height={17} />
															</td>
															<td>
																<span>{navbarLabel.number}</span>
															</td>
														</tr>
														<tr onClick={() => this.showBookAppointment(true)} style={{ cursor: 'pointer' }}>
															<td style={{ paddingTop: '5px' }}>
																<img src={bookingIcon} alt="bookingIcon" width={17} height={16} />
															</td>
															<td>
																<button type="button" className="bookAppointmentNavButton">{navbarLabel.appointment}</button>
															</td>
														</tr>
													</tbody>
												</table>

												<table className="linksTableNavSide">
													<tbody>
														<tr>
															{
																isAndroid ? <td onClick={() => this.showCallNumberPopup(true)}>
																	<span>{navbarLabel.call}</span>
																</td> : <td><span><a href="tel:+65 6259 8188">{navbarLabel.call}</a></span></td>
															}
														</tr>

														<tr onClick={() => this.showBookAppointment(true)} style={{ cursor: 'pointer' }}>
															<td>
																<span>{navbarLabel.appointment}</span>
															</td>
														</tr>
														<tr>
															<td onClick={() => this.handleSaveAndExit()}>
																<span>{navbarLabel.save}</span>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</li>}
									</ul>
								</div>
							</nav>
						}

						{commonReducer.appData !== null && retrievingReducer.myInfoData !== null &&
							<div className={`container-fluid p-0 content_1 productTitle-header ${navBarHide} ${toggleNavCSS}`}>

								<div className="displayHeader">
									<button className={`menuButton  ${toggleNavCSS}`} type="button" onClick={() => this.toggleNav(commonReducer.toggleNav)}>☰</button>
									<img className="img-size" alt="logo" src={imgUrl} />
									<div className="title-vertical-line" />
									<div className="mob-prodtitle">
										{productTitle.split(" | ")[0]}
										<br />
										<span className="font-16">{productTitle.split(" | ")[1]}</span>
									</div>
								</div>
								<section
									className="resume-section d-flex d-column"
									id="home"
								>
									<div className="my-auto">
										<h1 className="mt-0 title1">
											<span className="title2">
												{productTitle}
											</span>
										</h1>
									</div>
								</section>
								{
									<div className="progressTopMain">
										<div className="mobileProgressTop" style={{ width: progressPercentage + '%' }} />
									</div>
								}
								{!bookAppointmentReducer.isBookingPopupShow && !whiteScreenLogo &&
									<img className="rightByYouLogo" src={rightByYouLogo} alt="Right By You" />
								}
							</div>
						}
					</div>
					<div className={`uob-form-loan-container ${toggleNavCSS}`}>
						<div className="uob-body" onClick={() => this.closeNav()}>
							{commonReducer.appData !== null && this.props.children}
						</div>
					</div>
					{commonReducer.messageContent !== "" && (
						<GenericErrorMessage
							{...this.props}
							interval={60}
							messageContent={commonReducer.messageContent}
							onClearMessage={this.handleOnClearMessage.bind(this)}
						/>
					)}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, retrievingReducer, contactDetailsReducer, askQuestionsReducer, shareHoldersReducer, bookAppointmentReducer, saveAndExitPopupReducer } = state;
	return { commonReducer, retrievingReducer, contactDetailsReducer, askQuestionsReducer, shareHoldersReducer, bookAppointmentReducer, saveAndExitPopupReducer };
};

export default connect(mapStateToProps)(WrapContainer);
