import * as types from './../actions/actionTypes';

const initialState = {
	signature: {
		image:'',
		isValid: true,
		errorMsg: ''
	},
	signaturePad: {
		trimmedDataURL: null
	},
	saveSignatureSuccessful: false,
	popupOpen: false,
	signatureIsValid: true,
	isSign: true,
	saveImage: '',
	image: '',
	position: null,
	drawSignature: null,
	saveThruIsSign: true
};

const signatureReducer = (state = initialState, action) => {
	switch (action.type) {
		case types.SIGNATURE_ERROR_SHOW:
			return {
				...state,
				inputProgressValue: 0,
				signature: {
					...state['signature'],
					isValid: false,
					errorMsg: action.errorMsg
				},
				signaturePad: {
					trimmedDataURL: null
				}
			};
		case types.SIGNATURE_ERROR_REMOVE:
			return {
				...state,
				inputProgressValue: 0,
				signature: {
					...state['signature'],
					isValid: true,
					errorMsg: ''
				},
				signaturePad: {
					trimmedDataURL: null
				}
			};
		case types.SIGNATURE_SAVE_DATA:
			return {
				...state,
				inputProgressValue: 100,
				signature: {
					...state['signature'],
					isValid: true,
					errorMsg: ''
				},
				signaturePad: {
					trimmedDataURL: action.trimmedDataURL
				}
			};
		case types.CHANGE_PROGRESS_VALUE:
			return {
				...state,
				inputProgressValue: 0,
				signaturePad: {
					trimmedDataURL: null
				}
			};

		case types.UPLOAD_SIGNATURE_SUCCESSFUL:
			return {
				...state,
				saveSignatureSuccessful: action.status
			}

		case types.SET_POP_UP_STATUS:
			return {
				...state,
				popupOpen: action.status
			}

		case types.SET_SIGNATURE_UPLOAD_ISVALID:
			return {
				...state,
				signatureIsValid: action.status
			}

		case types.SET_SIGNATURE_TAB_STATUS:
			return {
				...state,
				isSign: action.status,
				signatureIsValid: true
			}

		case types.SET_UPLOAD_SIGNATURE_IMAGE:
			return {
				...state,
				image: action.image,
				signatureIsValid: true
			}

		case types.SET_UPLOAD_SIGNATURE_SAVE_IMAGE:
			return {
				...state,
				saveImage: action.image,
				position: action.position,
				saveThruIsSign: false,
				signatureIsValid: true
			}

		case types.SET_DRAW_SIGNATURE_SAVE_IMAGE:
			return {
				...state,
				drawSignature: action.image,
				saveThruIsSign: true,
				signatureIsValid: true
			}
		default:
			return state;
	}
};

export default signatureReducer;
