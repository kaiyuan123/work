import * as types from './../actions/actionTypes';

const initialState = {
  myInfoParameter: "",
  myInfoData : null
};

const RetrievingReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.STORE_MY_INFO_PARAMETER:
      return {
        ...state,
        myInfoParameter: action.parameter
      }

    case types.STORE_MY_INFO_REPONSES:
      return {
        ...state,
        myInfoData: action.response
      }
      
    default:
      return state;
  }
}

export default RetrievingReducer;
