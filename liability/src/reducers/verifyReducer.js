import * as types from './../actions/actionTypes';

const initialState = {
	uen: {
  	name: "uen",
  	isValid: true,
  	value: "",
  	errorMsg: "",
  	isInitial: true
  },
  isOTPPageShow: false,
  isLinkActive: false,
  retrieveError: false,
	serviceDown: false,
	dataLoaded: true,
	productCode: '',
	isCommerical: false,
	requestLoan: '',
	annualTurnover: '',
	entityName: '',
	refNo: '',
	applicationId: '',
	status:'',
	expiryDate:'',
	createdDate:''
}

const verifyReducer = (state = initialState, action) => {
	switch (action.type) {

		case types.VERIFY_UEN_HANDLE_TEXT_INPUT_CHANGE:
	      	return {
		        ...state,
		        [action.field]: {
					...state[action.field],
					value: action.value,
			        isValid: action.isValid,
			        errorMsg: action.errorMsg,
			        isInitial: false
				}
		    };

		case types.UEN_SET_TEXT_INPUT_REQUIRED_ERROR:
	      	return {
	        	...state,
	        	[action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
	      	};

		case types.SET_OTP_PAGE:
      		return { ...state, isOTPPageShow: action.isOTPPageShow };

      	case types.SET_ERROR_MESSAGE_INPUT:
	      	return {
	        	...state,
	        	[action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
	      	};

	    case types.IS_RETRIEVAL_LINK_ACTIVE:
	      return {...state,  isLinkActive: action.isLinkActive };

	    case types.SET_VERIFY_APPLICATION_DATA:
	      return {
	        ...state,
	        retrieveError: false,
	        serviceDown: false,
	        dataLoaded: true,
	        applicationId: action.applicationId,
	        refNo: action.refNo,
	        expiryDate: action.expiryDate,
	        productCode: action.productCode,
	        createdDate: action.createdDate,
	        status: action.status
	      };

		default:
	      return state;
	}
}

export default verifyReducer;
