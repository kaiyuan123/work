import * as types from './../actions/actionTypes';

const initialState = {
  isLoading: true,
  isProcessing: false,
  currentStep: 'signIn',
  currentSection: 'contactDetails',
  uen:'',
  appData: null,
  messageContent: '',
  compatibilityMessage: '',
  inlineMessage: '',
  accessToken: '',
  referenceNo: '',
  identifier: '',
  isMyInfoFlow: false,
  isExpanded: false,
  isRetrieving: true,
	isMobile:false,
  errorCount: 0,
  applicationExpiryDate: '',
  applicationStatus: '',
  duplicateTab: '',
  toggleNav: false,
  myInfoParams: '',
  callNumberPopup: false
};

const commonReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.INITIAL_APP_DATA:
      return { ...state, appData: action.appData };

    case types.SET_APP_LOADING_STATUS:
      return { ...state, isLoading: action.isLoading };

    case types.SET_APP_PROCESSING_STATUS:
      return { ...state, isProcessing: action.isProcessing };

    case types.SET_ERROR_CONTENT_MESSAGE:
      return { ...state, messageContent: action.messageContent };

    case types.SET_INLINE_ERROR_MESSAGE:
      return { ...state, inlineMessage: action.inlineMessage };

    case types.CHANGE_CURRENT_STEP:
      return { ...state, currentStep: action.step };

    case types.SET_JWT_TOKEN:
      return { ...state, accessToken: action.accessToken };

    case types.SET_CURRENT_SECTION:
      return { ...state, currentSection: action.currentSection };

    case types.SET_IDENTIFIER:
      return { ...state, identifier: action.identifier };

    case types.SET_SHOW_DOCUMENT_STATUS:
      return { ...state, showUploadDocuments: action.status };

    case types.SET_MYINFO_FLOW_STATUS:
      return { ...state, isMyInfoFlow: action.status };

    case types.SET_HELP_MESSAGE_EXPANDED:
      return { ...state, isExpanded: action.isExpanded };

    case types.SET_APPLICATION_REFERENCE_NO:
      return { ...state, referenceNo: action.referenceNo };

    case types.SET_APPLICATION_STATUS:
      return { ...state, applicationStatus: action.status };

    case types.SET_APPLICATION_EXPIRY_DATE:
      return { ...state, applicationExpiryDate: action.expiryDate };

    case types.SET_RETRIEVING_MYINFO:
      return {
        ...state,
        isRetrieving: action.status
      }

    case types.IS_MOBILE_DEVICE:
      return {
        ...state,
        isMobile: action.status
      }
    case types.SET_LOGGEDIN_COMPANY_INFO:
      return { ...state,  uen: action.uen};

    case types.SET_GLOBAL_ERROR_COUNTER:
      return { ...state,  errorCount: action.errorCounter};

    case types.SET_ERROR_COMPATIBILITY_MESSAGE:
      return {
        ...state,
        compatibilityMessage: action.messageContent
      }

    case types.SET_DUPLICATE_TAB_ID:
      return {
        ...state,
        duplicateTab: action.id
      }

    case types.TOGGLE_NAV:
      return {
        ...state,
        toggleNav: action.status
      }

    case types.SET_MYINFO_PARAMS:
      return {
        ...state,
        myInfoParams: action.myInfoParams
      }

    case types.SET_CALL_NUMBER_POPUP:
      return {
        ...state,
        callNumberPopup: action.status
      }

    default:
      return state;
  }
}

export default commonReducer;
