import * as types from './../actions/actionTypes';

const initialState = {
    items: [],
    crs: [],
    itemsFiltered: [],
    taxResidencyIsValid: true,
    checkboxSG: {
        isToggled: false,
        isValid: true,
    },
    checkboxUS: {
        isToggled: false,
        isValid: true,
    },
    checkboxOthers: {
        isToggled: false,
        isValid: true,
    },
    checkboxNone: {
        isToggled: false,
        isValid: true,
    },
    tinNoSG: {
        name: 'tinNoSG',
        value: '',
        isValid: true,
        errorMsg: '',
        isInitial: true
    },
    tinNoUS: {
        name: 'tinNoUS',
        value: '',
        isValid: true,
        errorMsg: '',
        isInitial: true
    },
    isUSResident: {
        name: 'isUSResident',
        value: '',
        isValid: true,
        errorMsg: '',
        isInitial: true
    },
    tinNoOthersId: [0],
    tinNoOthers0: {
        haveTINNo: {
            name: 'haveTINNo',
            value: '',
            isValid: true,
            errorMsg: '',
            isInitial: true
        },
        othersCountry: {
            name: "othersCountry",
            searchValue: "",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
        },
        othersTINNo: {
            name: "othersTINNo",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
        },
        reasons: {
            name: "reasons",
            isValid: true,
            value: "",
            errorMsg: "*is required",
            isInitial: true
        },
        othersOthers: {
            name: "othersOthers",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
        }
    },
    countryNone: {
        name: 'countryNone',
        isValid: true,
        isFocus: false,
        value: '',
        errorMsg: '',
        isInitial: true,
        isMyInfo: false,
        description: ''
    },
    haveTINNoNone: {
        name: 'haveTINNoNone',
        value: '',
        isValid: true,
        errorMsg: '',
        isInitial: true
    },
    tinNoNone: {
        name: "tinNoNone",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true,
    },
    reasonsNone: {
        name: "reasonsNone",
        isValid: true,
        value: "",
        errorMsg: "*is required",
        isInitial: true
    },
    othersNone: {
        name: "othersNone",
        isValid: true,
        value: "",
        errorMsg: "",
        isInitial: true,
    },
    fatcaCRSStatus: {
        name: 'fatcaCRSStatus',
        isValid: true,
        isFocus: false,
        value: '',
        errorMsg: '',
        isInitial: true,
        isMyInfo: false,
        description: ''

    },
    controllingPersonId: [0],
    controllingPerson0: {
        cpName: {
            name: 'cpName',
            value: '',
            isValid: true,
            errorMsg: '',
            isInitial: true,
            isFocus: false
        },
        cpIdNo: {
            name: "cpIdNo",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
        },
        cpMobileNo: {
            name: "cpMobileNo",
            isValid: true,
            value: "+65",
            errorMsg: "",
            isInitial: true,
        },
        cpEmail: {
            name: "cpEmail",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true,
        },
        cpPercentageOwnership: {
            name: "cpPercentageOwnership",
            isValid: true,
            value: "",
            errorMsg: "",
            isInitial: true

        },
        taxResidencyIsValid: true,
        checkboxSG: {
            isToggled: false,
            isValid: true,
        },
        checkboxUS: {
            isToggled: false,
            isValid: true,
        },
        checkboxOthers: {
            isToggled: false,
            isValid: true,
        },
        tinNoSG: {
            name: 'tinNoSG',
            value: '',
            isValid: true,
            errorMsg: '',
            isInitial: true
        },
        tinNoUS: {
            name: 'tinNoUS',
            value: '',
            isValid: true,
            errorMsg: '',
            isInitial: true
        },
        tinNoOthersId: [0],
        tinNoOthers0: {
            haveTINNo: {
                name: 'haveTINNo',
                value: '',
                isValid: true,
                errorMsg: '',
                isInitial: true
            },
            othersCountry: {
                name: "othersCountry",
                searchValue: "",
                isValid: true,
                value: "",
                errorMsg: "",
                isInitial: true,
                isFocus: false,
            },
            othersTINNo: {
                name: "othersTINNo",
                isValid: true,
                value: "",
                errorMsg: "",
                isInitial: true,
            },
            reasons: {
                name: "reasons",
                isValid: true,
                value: "",
                errorMsg: "*is required",
                isInitial: true
            },
            othersOthers: {
                name: "othersOthers",
                isValid: true,
                value: "",
                errorMsg: "",
                isInitial: true,
            }
        }
    }
};

const taxSelfDeclarationsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CRS_DROPDOWN_ITEM_SELECT:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    value: action.value,
                    description: action.description,
                    isValid: true, errorMsg: '', isInitial: false
                }
            };


        case types.CRS_DROPDOWN_ITEM_SELECT_CHANGE:
            return {
                ...state, [action.key]:
                {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value,
                        description: action.description,
                        isValid: true,
                        errorMsg: '',
                        isInitial: false
                    }
                }
            };

        case types.CRS_DROPDOWN_FOCUS_CHANGE:
            return {
                ...state, [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        isFocus: action.isFocus,
                        isValid: true
                    }
                }
            };

        case types.CRS_DROPDOWN_SEARCH_INPUT_CHANGE:
            return {
                ...state, [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        searchValue: action.searchValue,
                        isValid: true
                    }
                }
            };


        case types.CRS_DROPDOWN_SEARCH_INPUT:
            return {
                ...state, [action.field]:
                {
                    ...state[action.field],
                    searchValue: action.searchValue,
                    isValid: true
                }
            };


        case types.CRS_HANDLE_TEXT_INPUT_CHANGE_OTHERS:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value,
                        isValid: action.isValid,
                        errorMsg: action.errorMsg,
                        isInitial: false
                    }
                }
            };
        case types.CRS_HANDLE_TEXT_INPUT_BLUR:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    value: action.value,
                    isValid: action.isValid,
                    errorMsg: action.errorMsg,
                    isInitial: false
                }
            };

        case types.CRS_HANDLE_TEXT_INPUT_BLUR_OTHERS:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value,
                        isValid: action.isValid,
                        errorMsg: action.errorMsg,
                        isInitial: false
                    }
                }
            };

        case types.CRS_IS_CHECKED:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    isToggled: action.isToggled,
                    isValid: true
                },
                taxResidencyIsValid: true
            };

        case types.CRS_HANDLE_TEXT_INPUT_CHANGE:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    value: action.value,
                    isValid: action.isValid,
                    errorMsg: action.errorMsg,
                    isInitial: false
                }
            };

        case types.CRS_HANDLE_BUTTON_CHANGE:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    value: action.value,
                    isValid: true
                }
            };

        case types.CRS_HANDLE_BUTTON_CHANGE_OTHERS:
            return {
                ...state, [action.key]:
                {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value,
                        isValid: true
                    }
                }
            };

        case types.CRS_SELECT_RADIO_BUTTON:
            return {
                ...state, [action.key]:
                {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value,
                        errorMsg: "",
                        isValid: true
                    }
                }
            };
        case types.CRS_SELECT_RADIO_BUTTON_NONE:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    value: action.value,
                    errorMsg: "",
                    isValid: true
                }
            };

        case types.MYINFO_SET_COMPANY_UEN_DATA:
            return {
                ...state,
                tinNoSG: {
                    ...state.tinNoSG,
                    value: action.tinNoSG
                }
            };

        case types.CRS_ADD_COUNTRY:
            return {
                ...state,
                [action.tinNoOthers]: {
                    haveTINNo: {
                        name: 'haveTINNo',
                        value: '',
                        isValid: true,
                        errorMsg: '',
                        isInitial: true
                    },
                    othersCountry: {
                        name: "othersCountry",
                        searchValue: "",
                        isValid: true,
                        value: "",
                        errorMsg: "",
                        isInitial: true,
                    },
                    othersTINNo: {
                        name: "othersTINNo",
                        isValid: true,
                        value: "",
                        errorMsg: "",
                        isInitial: true,
                    },
                    reasons: {
                        name: "reasons",
                        isValid: true,
                        value: "",
                        errorMsg: "*is required",
                        isInitial: true
                    },
                    othersOthers: {
                        name: "othersOthers",
                        isValid: true,
                        value: "",
                        errorMsg: "",
                        isInitial: true,
                    }
                }
            };

        case types.CRS_INCREMENT_COUNTRY_COUNT:
            return {
                ...state,
                tinNoOthersId: action.tinNoOthersId
            };

        case types.CRS_DROPDOWN_FOCUS:
            return {
                ...state, [action.field]: {
                    ...state[action.field],
                    isFocus: action.isFocus,
                    isValid: true
                }
            };

        case types.CRS_UNCHECK_REST:
            return {
                ...state,
                checkboxSG: {
                    ...state,
                    isToggled: false,
                },
                checkboxUS: {
                    ...state,
                    isToggled: false
                },
                checkboxOthers: {
                    ...state,
                    isToggled: false
                }
            };

        case types.CRS_UNCHECK_NONE:
            return {
                ...state,
                checkboxNone: {
                    ...state,
                    isToggled: false
                }
            };

        case types.CRS_REMOVE_COUNTRY:
            let newState = state;
            delete newState[action.tinNoOthers];

            return {
                ...newState
            };

        case types.CRS_DECREASE_COUNTRY:
            return {
                ...state,
                tinNoOthersId: action.tinNoOthersId
            };

        case types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    [action.field]: { ...state[action.key][action.field], errorMsg: action.errorMsg, isValid: false }
                }
            };
        case types.SET_ERROR_MESSAGE_INPUT:
            return {
                ...state,
                [action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
            };

        case types.SET_CRS_OTHERS_INPUT_ERROR:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        isValid: false,
                        errorMsg: action.errorMsg,
                        isInitial: false
                    }
                }
            };

        case types.SET_CRS_CP_INPUT_ERROR:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        isValid: false,
                        errorMsg: action.errorMsg,
                        isInitial: false
                    }
                }
            };

        case types.CRS_SELECT_STATUS:
            return {
                ...state,
                [action.field]: {
                    ...state[action.field],
                    value: action.value,
                    isValid: true
                }
            };

        case types.CRS_ADD_CP:
            return {
                ...state,
                [action.controllingPerson]: {
                    cpName: {
                        name: 'cpName',
                        value: '',
                        isValid: true,
                        errorMsg: '',
                        isInitial: true,
                        isFocus: false
                    },
                    cpIdNo: {
                        name: "cpIdNo",
                        isValid: true,
                        value: "",
                        errorMsg: "",
                        isInitial: true,
                    },
                    cpMobileNo: {
                        name: "cpMobileNo",
                        isValid: true,
                        value: "+65",
                        errorMsg: "",
                        isInitial: true,
                    },
                    cpEmail: {
                        name: "cpEmail",
                        isValid: true,
                        value: "",
                        errorMsg: "",
                    },
                    cpPercentageOwnership: {
                        name: "cpPercentageOwnership",
                        isValid: true,
                        value: "",
                        errorMsg: "",
                        isInitial: true
                    },
                    taxResidencyIsValid: true,
                    checkboxSG: {
                        isToggled: false,
                        isValid: true,
                    },
                    checkboxUS: {
                        isToggled: false,
                        isValid: true,
                    },
                    checkboxOthers: {
                        isToggled: false,
                        isValid: true,
                    },
                    tinNoSG: {
                        name: 'tinNoSG',
                        value: '',
                        isValid: true,
                        errorMsg: '',
                        isInitial: true
                    },
                    tinNoUS: {
                        name: 'tinNoUS',
                        value: '',
                        isValid: true,
                        errorMsg: '',
                        isInitial: true
                    },
                    tinNoOthersId: [0],
                    tinNoOthers0: {
                        haveTINNo: {
                            name: 'haveTINNo',
                            value: '',
                            isValid: true,
                            errorMsg: '',
                            isInitial: true
                        },
                        othersCountry: {
                            name: "othersCountry",
                            searchValue: "",
                            isValid: true,
                            value: "",
                            errorMsg: "",
                            isFocus: false,
                            isInitial: true,
                        },
                        othersTINNo: {
                            name: "othersTINNo",
                            isValid: true,
                            value: "",
                            errorMsg: "",
                            isInitial: true,
                        },
                        reasons: {
                            name: "reasons",
                            isValid: true,
                            value: "",
                            errorMsg: "*is required",
                            isInitial: true
                        },
                        othersOthers: {
                            name: "othersOthers",
                            isValid: true,
                            value: "",
                            errorMsg: "",
                            isInitial: true,
                        }
                    }
                }
            };

        case types.CRS_INCREMENT_CP_COUNT:
            return {
                ...state,
                controllingPersonId: action.controllingPersonId
            };

        case types.CRS_REMOVE_CP:
            let newStateCP = state;
            delete newStateCP[action.controllingPerson];

            return {
                ...newStateCP
            };

        case types.CRS_DECREASE_CP:
            return {
                ...state,
                controllingPersonId: action.controllingPersonId
            };

        case types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT_CP:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    [action.field]: { ...state[action.key][action.field], errorMsg: action.errorMsg, isValid: false }
                }
            };

        case types.CHECK_TAX_RESIDENCY_IS_VALID:
            return {
                ...state,
                taxResidencyIsValid: action.status,
            };

        case types.LOCAL_ADDRESS_INPUT_AUTO_FILL_IN_CP:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    residentialPostalCode: {
                        ...state[action.key].residentialPostalCode,
                        isLoading: false,
                        isReadOnly: false,
                        autoFilled: true,
                        count: action.count,
                    },
                    residentialStreet: {
                        ...state[action.key].residentialStreet,
                        value: action.street,
                        isValid: true,
                        isInitial: false,
                        isReadOnly: true
                    },
                    residentialBlock: {
                        ...state[action.key].residentialBlock,
                        value: action.block,
                        isValid: true,
                        isInitial: false,
                        isReadOnly: true
                    },
                    residentialBuilding: {
                        ...state[action.key].residentialBuilding,
                        value: action.building,
                        isValid: true,
                        isInitial: false,
                        isReadOnly: true
                    }
                }
            };

        case types.IS_GETTING_LOCAL_ADDRESS_CP:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    residentialPostalCode: {
                        ...state[action.key].residentialPostalCode,
                        isLoading: action.isLoading
                    }
                }
            };

        case types.LOCAL_ADDRESS_INPUT_AUTO_FILL_IN_FAIL_CP:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    residentialPostalCode: {
                        ...state[action.key].residentialPostalCode,
                        isValid: false,
                        errorMsg: 'is Invalid',
                        isReadOnly: false,
                        isLoading: false,
                        autoFilled: false,
                        count: action.count,
                    },
                    residentialStreet: {
                        ...state[action.key].residentialStreet,
                        isReadOnly: false,
                        isInitial: true,
                        value: '',
                    }
                },
                residentialBlock: {
                    ...state[action.key].residentialBlock,
                    isReadOnly: false,
                    isInitial: true,
                    value: ''
                },
                residentialBuilding: {
                    ...state[action.key].residentialBuilding,
                    isReadOnly: false,
                    isInitial: true,
                    value: ''
                }
            };

        case types.LOCAL_ADDRESS_INPUT_SERVICE_DOWN_CP:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    residentialPostalCode: {
                        ...state[action.key].residentialPostalCode,
                        isReadOnly: false,
                        isLoading: false,
                        autoFilled: false,
                        count: 10
                    }
                }
            };
        case types.LOCAL_ADDRESS_INPUT_RESET_AUTO_FILL_IN_STATUS_CP:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    residentialPostalCode: {
                        ...state[action.key].residentialPostalCode,
                        isReadOnly: false,
                        isLoading: false,
                        autoFilled: false,
                        count: 10
                    },
                    residentialStreet: {
                        ...state[action.key].residentialStreet,
                        isReadOnly: false,
                        isInitial: true,
                        value: ''
                    },
                    residentialBlock: {
                        ...state[action.key].residentialBlock,
                        isReadOnly: false,
                        isInitial: true,
                        value: ''
                    },
                    residentialBuilding: {
                        ...state[action.key].residentialBuilding,
                        isReadOnly: false,
                        isInitial: true,
                        value: ''
                    }
                }
            };
        case types.CRS_IS_CHECKED_CP:
            return {
                ...state, [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        isToggled: action.isToggled,
                        isValid: true
                    },
                    taxResidencyIsValid: true
                }
            };

        case types.CRS_HANDLE_BUTTON_CHANGE_CP:
            return {
                ...state, [action.key]: {
                    ...state[action.key],
                    [action.field]: {
                        ...state[action.key][action.field],
                        value: action.value,
                        isValid: true
                    }
                }
            };

        case types.CRS_HANDLE_BUTTON_CHANGE_OTHERS_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        [action.field]: {
                            ...state[action.cpKey][action.key][action.field],
                            value: action.value,
                            isValid: true
                        }
                    }
                }
            };

        case types.CRS_DROPDOWN_SEARCH_INPUT_CHANGE_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        [action.field]: {
                            ...state[action.cpKey][action.key][action.field],
                            searchValue: action.searchValue,
                            isValid: true
                        }
                    }
                }
            };

        case types.CRS_DROPDOWN_ITEM_SELECT_CHANGE_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        [action.field]: {
                            ...state[action.cpKey][action.key][action.field],
                            value: action.value,
                            description: action.description,
                            isValid: true,
                            errorMsg: '',
                            isInitial: false
                        }
                    }
                }
            };

        case types.CRS_DROPDOWN_FOCUS_CHANGE_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        [action.field]: {
                            ...state[action.cpKey][action.key][action.field],
                            isFocus: action.isFocus,
                            isValid: true
                        }
                    }
                }
            };

        case types.CRS_HANDLE_TEXT_INPUT_CHANGE_OTHERS_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        [action.field]: {
                            ...state[action.cpKey][action.key][action.field],
                            value: action.value,
                            isValid: action.isValid,
                            errorMsg: action.errorMsg,
                            isInitial: false
                        }
                    }
                }
            };

        case types.CRS_HANDLE_TEXT_INPUT_BLUR_OTHERS_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        [action.field]: {
                            ...state[action.cpKey][action.key][action.field],
                            value: action.value,
                            isValid: action.isValid,
                            errorMsg: action.errorMsg,
                            isInitial: false
                        }
                    }
                }
            };

        case types.CRS_SELECT_RADIO_BUTTON_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        [action.field]: {
                            ...state[action.cpKey][action.key][action.field],
                            value: action.value,
                            errorMsg: "",
                            isValid: true
                        }
                    }
                }
            };

        case types.CRS_INCREMENT_COUNTRY_COUNT_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    tinNoOthersId: action.tinNoOthersId
                }
            };

        case types.CRS_ADD_COUNTRY_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.tinNoOthers]: {
                        haveTINNo: {
                            name: 'haveTINNo',
                            value: '',
                            isValid: true,
                            errorMsg: '',
                            isInitial: true
                        },
                        othersCountry: {
                            name: "othersCountry",
                            searchValue: "",
                            isValid: true,
                            value: "",
                            errorMsg: "",
                            isFocus: false,
                            isInitial: true,
                        },
                        othersTINNo: {
                            name: "othersTINNo",
                            isValid: true,
                            value: "",
                            errorMsg: "",
                            isInitial: true,
                        },
                        reasons: {
                            name: "reasons",
                            isValid: true,
                            value: "",
                            errorMsg: "*is required",
                            isInitial: true
                        },
                        othersOthers: {
                            name: "othersOthers",
                            isValid: true,
                            value: "",
                            errorMsg: "",
                            isInitial: true,
                        }
                    }
                }
            };

        case types.CRS_REMOVE_COUNTRY_CP:
            let newStateOthers = state;
            delete newStateOthers[action.cpKey][action.tinNoOthers];

            return {
                ...newStateOthers
            };

        case types.CRS_DECREASE_COUNTRY_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    tinNoOthersId: action.tinNoOthersId
                }
            };


        case types.CHECK_TAX_RESIDENCY_IS_VALID_CP:
            return {
                ...state, [action.key]:
                {
                    ...state[action.key],
                    taxResidencyIsValid: action.status,
                }
            };

        case types.SET_ADDITIONAL_ERROR_MESSAGE_INPUT_CPT:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        [action.field]: {
                            ...state[action.cpKey][action.key][action.field],
                            errorMsg: action.errorMsg,
                            isValid: false
                        }
                    }
                }
            };

        case types.CLEAR_ERROR_MSG_COUNTRY:
            return {
                ...state, [action.key]: {
                    ...state[action.key],
                    othersCountry: {
                        ...state[action.key].othersCountry,
                        isValid: true,
                        errorMsg: ""
                    }
                }
            }

        case types.CLEAR_ERROR_MSG_CP_ID:
            return {
                ...state, [action.key]: {
                    ...state[action.key],
                    cpIdNo: {
                        ...state[action.key].cpIdNo,
                        isValid: true,
                        errorMsg: ""
                    }
                }
            }

        case types.CLEAR_ERROR_MSG_COUNTRY_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        othersCountry: {
                            ...state[action.cpKey][action.key].othersCountry,
                            isValid: true,
                            errorMsg: ""
                        }
                    }
                }
            }

        case types.SET_CRS_OTHERS_INPUT_ERROR_CP:
            return {
                ...state, [action.cpKey]:
                {
                    ...state[action.cpKey],
                    [action.key]: {
                        ...state[action.cpKey][action.key],
                        [action.field]: {
                            ...state[action.cpKey][action.key][action.field],
                            isValid: false,
                            errorMsg: action.errorMsg,
                            isInitial: false
                        }
                    }
                }
            };

        case types.CP_SEARCH_INPUT_CHANGING:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    cpName: {
                        ...state[action.key].cpName,
                        value: action.value,
                        isValid: action.isValid,
                        errorMsg: action.errorMsg
                    }
                },
                itemsFiltered: action.itemsFiltered

            };

        case types.CP_SEARCH_RESULT_SELECT:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    cpName: {
                        ...state[action.key].cpName,
                        value: action.item.name
                    },
                    cpIdNo: {
                        ...state[action.key].cpIdNo,
                        value: action.item.legalId
                    },
                    cpMobileNo: {
                        ...state[action.key].cpMobileNo,
                        value: action.item.mobileNumber
                    },
                    cpEmail: {
                        ...state[action.key].cpEmail,
                        value: action.item.emailAddress
                    },
                    cpNationality: {
                        ...state[action.key].cpNationality,
                        value: action.item.nationality
                    }
                }
            };

        case types.CP_SEARCH_INPUT_FOCUS:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    cpName: {
                        ...state[action.key].cpName,
                        isFocus: action.status
                    }
                }
            };
        case types.SET_CP_FILTER:
            return {
                ...state,
                items: action.items
            }

        case types.SET_RETRIEVE_TAX_DECLARATION:
            return {
                ...state,
                fatcaCRSStatus: { ...state.fatcaCRSStatus, value: action.fatcaCRSStatus },
                checkboxSG: { ...state.checkboxSG, isToggled: action.checkboxSG },
                checkboxUS: { ...state.checkboxUS, isToggled: action.checkboxUS },
                checkboxOthers: { ...state.checkboxOthers, isToggled: action.checkboxOthers },
                checkboxNone: { ...state.checkboxNone, isToggled: action.checkboxNone },
                tinNoSG: { ...state.tinNoSG, value: action.tinNoSG },
                tinNoUS: { ...state.tinNoUS, value: action.tinNoUS },
                isUSResident: { ...state.isUSResident, value: action.isUSResident },
                countryNone: { ...state.countryNone, value: action.countryNone },
                haveTINNoNone: { ...state.haveTINNoNone, value: action.haveTINNoNone },
                tinNoNone: { ...state.tinNoNone, value: action.tinNoNone },
                reasonsNone: { ...state.reasonsNone, value: action.reasonsNone },
                othersNone: { ...state.othersNone, value: action.othersNone }
            };

        case types.RETRIEVE_POPULATE_FIRST_TIN:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    othersCountry: {
                        ...state[action.key]["othersCountry"],
                        value: action.othersCountry,
                        isInitial: true,
                        isValid: true,
                        isReadOnly: true,
                        isFocus: false
                    },
                    haveTINNo: {
                        ...state[action.key]["haveTINNo"],
                        value: action.haveTINNo,
                        isInitial: true,
                        isValid: true,
                        isReadOnly: true,
                        isFocus: false
                    },
                    othersTINNo: {
                        ...state[action.key]["othersTINNo"],
                        value: action.othersTINNo,
                        isInitial: true,
                        isValid: true,
                        hasEdit: false
                    },
                    reasons: {
                        ...state[action.key]["reasons"],
                        value: action.reasons,
                        isInitial: true,
                        isValid: true,
                        isFocus: false,
                        hasEdit: false
                    },
                    othersOthers: {
                        ...state[action.key]["othersOthers"],
                        value: action.othersOthers,
                        isInitial: true,
                        isValid: true,
                        isFocus: false,
                        hasEdit: false
                    }
                }
            };

        case types.RETRIEVE_POPULATE_ADDITIONAL_TIN:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    haveTINNo: {
                        name: 'haveTINNo',
                        value: action.haveTINNo,
                        isValid: true,
                        errorMsg: '',
                        isInitial: true
                    },
                    othersCountry: {
                        name: "othersCountry",
                        isValid: true,
                        value: action.othersCountry,
                        errorMsg: "",
                        isFocus: false,
                        isInitial: true,
                    },
                    othersTINNo: {
                        name: "othersTINNo",
                        isValid: true,
                        value: action.othersTINNo,
                        errorMsg: "",
                        isInitial: true,
                    },
                    reasons: {
                        name: "reasons",
                        isValid: true,
                        value: action.reasons,
                        errorMsg: "*is required",
                        isInitial: true
                    },
                    othersOthers: {
                        name: "othersOthers",
                        isValid: true,
                        value: action.othersOthers,
                        errorMsg: "",
                        isInitial: true,
                    }
                }
            };

        case types.SET_RETRIEVE_CONTROLLING_PERSON:
            return {
                ...state,
                [action.controllingPerson]: {
                    cpName: {
                        name: 'cpName',
                        value: action.cpName,
                        isValid: true,
                        errorMsg: '',
                        isInitial: true,
                        isFocus: false
                    },
                    cpIdNo: {
                        name: "cpIdNo",
                        isValid: true,
                        value: action.cpIdNo,
                        errorMsg: "",
                        isInitial: true,
                    },
                    cpMobileNo: {
                        name: "cpMobileNo",
                        isValid: true,
                        value: action.cpMobileNo,
                        errorMsg: "",
                        isInitial: true,
                    },
                    cpEmail: {
                        name: "cpEmail",
                        isValid: true,
                        value: action.cpEmail,
                        errorMsg: "",
                    },
                    cpPercentageOwnership: {
                        name: "cpPercentageOwnership",
                        isValid: true,
                        value: action.cpPercentageOwnership,
                        errorMsg: "",
                        isInitial: true
                    },
                    taxResidencyIsValid: true,
                    checkboxSG: {
                        isToggled: false,
                        isValid: true,
                    },
                    checkboxUS: {
                        isToggled: false,
                        isValid: true,
                    },
                    checkboxOthers: {
                        isToggled: false,
                        isValid: true,
                    },
                    tinNoSG: {
                        name: 'tinNoSG',
                        value: '',
                        isValid: true,
                        errorMsg: '',
                        isInitial: true
                    },
                    tinNoUS: {
                        name: 'tinNoUS',
                        value: '',
                        isValid: true,
                        errorMsg: '',
                        isInitial: true
                    },
                    isUSResident: {
                        name: 'isUSResident',
                        value: '',
                        isValid: true,
                        errorMsg: '',
                        isInitial: true
                    },
                    tinNoOthersId: [0],
                    tinNoOthers0: {
                        haveTINNo: {
                            name: 'haveTINNo',
                            value: '',
                            isValid: true,
                            errorMsg: '',
                            isInitial: true
                        },
                        othersCountry: {
                            name: "othersCountry",
                            searchValue: "",
                            isValid: true,
                            value: "",
                            errorMsg: "",
                            isFocus: false,
                            isInitial: true,
                        },
                        othersTINNo: {
                            name: "othersTINNo",
                            isValid: true,
                            value: "",
                            errorMsg: "",
                            isInitial: true,
                        },
                        reasons: {
                            name: "reasons",
                            isValid: true,
                            value: "",
                            errorMsg: "*is required",
                            isInitial: true
                        },
                        othersOthers: {
                            name: "othersOthers",
                            isValid: true,
                            value: "",
                            errorMsg: "",
                            isInitial: true,
                        }
                    }
                }
            };
        default:
            return state;
    }
}

export default taxSelfDeclarationsReducer;
