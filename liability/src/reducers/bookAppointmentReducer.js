import * as types from './../actions/actionTypes';

const initialState = {
	availableLocations: null,
	availableLocationsBranchIDS: null,
	selectedObject: null,
	availableLocationDataLoaded: false,
	isBookingPopupShow: false,
	availableDateArray: null,
	bookingBranch: {
		name: 'bookingBranch',
		isValid: true,
		isFocus: false,
		value: '',
		errorMsg: '',
		isInitial: true,
		isMyInfo: false,
		description: ''
	},
	bookingTimeSlots: {
		name: 'bookingTimeSlots',
		isValid: true,
		isFocus: false,
		value: '',
		errorMsg: '',
		isInitial: true,
		isMyInfo: false,
		description: ''
	},
	dateTimeValue: {
		isValid: true,
		value: '',
		errorMsg: '',
		description: ''
	},
	onClickBranch: true,
	onClickDate: false,
	onClickDateDisabled: true,
	itemActive: '0'
};

const bookAppointmentReducer = (state = initialState, action) => {
	switch (action.type) {
		case types.SET_AVAILABLE_LOCATION_DATA:
			let availableBranchID = [];
			(action.response['content']).map((item) => {
				availableBranchID.push(item.branchId)
				return availableBranchID;
			});
			return {
				...state,
				availableLocations: action.response,
				availableLocationsBranchIDS: availableBranchID,
				availableLocationDataLoaded: true
			};

		case types.BOOK_APPOINTMENT_RIGHT_LOCATION_CLICK:
			return {
				...state,
				bookingBranch: {
					...state['bookingBranch'],
					value: action.searchValue,
					isValid: true,
					isFocus: false,
					errorMsg: '',
				},
				selectedObject: action.filterItem,
				availableDateArray: action.filterItem[0]['availableTimeSlots'],
				bookingTimeSlots: {
					...state['bookingTimeSlots'],
					value: '',
					isValid: true,
					isFocus: false,
					errorMsg: '',
					description: ''
				}
			};

		case types.SET_ERROR_MESSAGE_INPUT:
			return {
				...state,
				[action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
			};

		case types.SET_DATE_TIME_VALUE:
			return {
				...state,
				dateTimeValue: {
					...state['dateTimeValue'],
					value: action.value,
					isValid: true
				}
			};

		case types.BOOK_APPOINTMENT_DROPDOWN_FOCUS:
			return { ...state, [action.field]: { ...state[action.field], isFocus: action.isFocus } };

		case types.BOOK_APPOINTMENT_DROPDOWN_SEARCH_INPUT_CHANGE:
			return { ...state, [action.field]: { ...state[action.field], searchValue: action.searchValue } };

		case types.BOOK_APPOINTMENT_TOGGLE_POP_UP:
			if (action.status) {
				return {
					...state,
					isBookingPopupShow: action.status
				};
			}
			return initialState;

		case types.BOOK_APPOINTMENT_DROPDOWN_ITEM_SELECT:
			if (action.field === "bookingBranch") {
				return {
					...state,
					[action.field]: {
						...state[action.field],
						value: action.value,
						description: action.description,
						isValid: true,
						errorMsg: '',
						isInitial: false
					},
					selectedObject: action.filterItem,
					availableDateArray: action.filterItem[0]['availableTimeSlots'],
					bookingTimeSlots: {
						...state['bookingTimeSlots'],
						value: '',
						description: ''
					}
				};
			}
			return {
				...state,
				[action.field]: {
					...state[action.field],
					value: action.value,
					description: action.description,
					isValid: true,
					errorMsg: '',
					isInitial: false
				}
			};
		case types.BOOK_APPOINTMENT_SELECT_NAVIGATION:
			return {
				...state,
				onClickBranch: action.onClickBranch,
				onClickDate: action.onClickDate,
				itemActive: action.itemActive,
			}
		default:
			return state;
	}
};

export default bookAppointmentReducer;
