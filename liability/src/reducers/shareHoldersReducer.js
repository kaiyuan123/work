import * as types from './../actions/actionTypes';

const initialState = {
	shareHoldersList: {
        name: 'shareHoldersList',
        openRows: [],
        value: []
    },
	shareholderID: [0],
	shareholder0: {
	    shareholderName: {
	      name: "shareholderName",
	      isValid: true,
	      value: "",
	      errorMsg: "",
	      isInitial: true,
	      isReadOnly: false,
	      isMyInfo: false,
				hasEdit: false
	    },
	    shareholderNRIC: {
	      name: "shareholderNRIC",
	      isValid: true,
	      value: "",
	      errorMsg: "",
	      isInitial: true,
	      isReadOnly: false,
	      isMyInfo: false,
				hasEdit: false
	    },
	    shareholderEmail: {
	      name: "shareholderEmail",
	      isValid: true,
	      value: "",
	      errorMsg: "",
	      isInitial: true,
	      isReadOnly: false,
	      isMyInfo: false,
				hasEdit: false
	    },
	    shareholderMobileNo: {
	      name: "shareholderMobileNo",
	      isValid: true,
	      value: "",
	      errorMsg: "",
	      isInitial: true,
	      isReadOnly: false,
	      isMyInfo: false,
				hasEdit: false
	    }
	},
}

const shareHoldersReducer = (state = initialState, action) => {
	switch (action.type) {

		case types.APPROVED_SHAREHOLDERS_LIST:
	      return {
	        ...state,
	        shareHoldersList: {...state.shareHoldersList, value: action.shareHoldersList}
	    }

	    case types.POPULATE_SHAREHOLDERS_LIST:
	    	
	      return {
	        ...state,
	        [action.key]: {
	          ...state[action.key],
	          shareholderName: {
	            ...state[action.key]["shareholderName"],
	            value: action.shareholderName,
	            isInitial: true,
	            isValid: true,
	            isReadOnly: true,
							hasEdit: false
	          },
	          shareholderNRIC: {
	            ...state[action.key]["shareholderNRIC"],
	            value: action.shareholderNRIC,
	            isInitial: true,
	            isValid: true,
	            isReadOnly: true,
							hasEdit: false
	          },
	          shareholderEmail: {
	            ...state[action.key]["shareholderEmail"],
	            value: action.shareholderEmail,
	            isInitial: true,
	            isValid: true,
	            isReadOnly: false,
	            isMyInfo: action.emailMyinfo,
							hasEdit: false
	          },
	          shareholderMobileNo: {
	            ...state[action.key]["shareholderMobileNo"],
	            value: action.shareholderMobileNo,
	            isInitial: true,
	            isValid: true,
	            isReadOnly: false,
	            isMyInfo: action.phoneMyinfo,
							hasEdit: false
	          }

	        }
	    };

	    case types.POPULATE_ADDITIONAL_SHAREHOLDERS_LIST:
	    	
	      return {
	        ...state,
	        [action.shareholder]: {
	          shareholderName: {
	            name: "shareholderName",
	            isValid: true,
	            value: action.shareholderName,
	            errorMsg: "",
	            isInitial: true,
	            isReadOnly: true,
							hasEdit: false
	          },

	          shareholderNRIC: {
	            name: "shareholderNRIC",
	            isValid: true,
	            value: action.shareholderNRIC,
	            errorMsg: "",
	            isInitial: true,
	            isReadOnly: true,
							hasEdit: false
	          },

	          shareholderEmail: {
	            name: "shareholderEmail",
	            isValid: true,
	            value: action.shareholderEmail,
	            errorMsg: "",
	            isInitial: true,
	            isReadOnly: false,
	            isMyInfo: action.emailMyinfo,
							hasEdit: false
	          },

	          shareholderMobileNo: {
	            name: "shareholderMobileNo",
	            isValid: true,
	            value: action.shareholderMobileNo,
	            errorMsg: "",
	            isInitial: true,
	            isReadOnly: false,
	            isMyInfo: action.phoneMyinfo,
							hasEdit: false
	          }
	        }
	    };

	    case types.INCREMENT_SHAREHOLDERS_COUNT:
	      return {
	        ...state,
	        shareholderID: action.shareholderID
	    };

	    case types.SHAREHOLDERS_HANDLE_TEXT_INPUT_CHANGE:
	      return {
		        ...state,
		        [action.key]: {
		          ...state[action.key],
		          [action.field]: {
		            ...state[action.key][action.field],
		            value: action.value,
		            isValid: action.isValid,
		            errorMsg: action.errorMsg,
		            isInitial: false
		          }
		        }
		    };

		case types.SET_SHAREHOLDER_ERROR_MESSAGE_INPUT:
	      return {
	        ...state,
	        [action.key]: {
	          ...state[action.key],
	          [action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false, value: action.value }
	        }
	    };

	   case types.SET_SHAREHOLDER_INPUT_ERROR:
			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: {
						...state[action.key][action.field],
						isValid: false,
						errorMsg: action.errorMsg,
						isInitial: false
					}
				}
			};

		case types.SET_TEXT_INPUT_HAS_EDITED_WITH_KEY_SHAREHOLDER:
			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: {
						...state[action.key][action.field],
						hasEdit: true
					}
				}
			};

		default:
	      return state;
	}
}

export default shareHoldersReducer;
