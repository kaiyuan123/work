import * as types from './../actions/actionTypes';

const initialState = {
	showSaveAndExitPopup: false,
	screenToSave:''
}

const saveAndExitPopupReducer = (state = initialState, action) => {
	switch (action.type) {

		case types.SHOW_SAVE_AND_EXIT_POPUP:
	    return {
	        ...state,
	        showSaveAndExitPopup: action.showPopup,
	        screenToSave: action.stateObj
	    }

		default:
	      return state;
	}

}

export default saveAndExitPopupReducer;