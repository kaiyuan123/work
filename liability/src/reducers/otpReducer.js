import * as types from './../actions/actionTypes';

const initialState = {
  isCountingDown: false,
  prefix: '',
  code: '',
  otpTriedCount: 1,
  isLoading: false,
  isVerified: false,  
  inlineErrorMsg: ''
};

const otpReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_COUNT_DOWN_STATUS:
      return { ...state, isCountingDown: action.status };

    case types.SET_OTP_PREFIX:
      return { ...state, prefix: action.prefix };

    case types.SET_OTP_TRIED_COUNT:
      return { ...state, otpTriedCount: action.otpTriedCount };

    case types.OTP_INPUT_CHANGE:
      return { ...state, code: action.code };

    case types.IS_SENDING_OTP:
      return { ...state, isLoading: action.status };

    case types.SET_OTP_ERROR_MESSAGE:
      return { ...state, inlineErrorMsg: action.errorMsg };

    case types.RESET_OTP_CODE_TO_EMPTY:
      return { ...state, code: '' };    

    case types.RESET_TO_INITIAL:
      return initialState;

    default:
      return state;
  }
}

export default otpReducer;
