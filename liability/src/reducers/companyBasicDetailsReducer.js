import * as types from './../actions/actionTypes';

const initialState = {

  companyRegistrationNumber: {
    name: 'companyRegistrationNumber',
    value: ''
  },

  registeredCompanyName: {
    name: 'registeredCompanyName',
    value: '',

  },

  typeOfCompany: {
    name: 'typeOfCompany',
    value: ''
  },

  businessConstitutionType: {
    name: 'businessConstitutionType',
    value: ''
  },
  
  companyStatus: {
    name: 'companyStatus',
    value: ''
  },

  countryOfIncorporation: {
    name: 'countryOfIncorporation',
    value: ''
  },

  ownership: {
    name: 'ownership',
    value: ''
  },

  registrationDate: {
    name: 'registrationDate',
    value: ''
  },

  companyExpiryDate: {
    name: 'companyExpiryDate',
    value: ''
  },

  primaryActivityCode: {
    name: 'primaryActivityCode',
    value: ''
  },

  primaryActivityDesc: {
    name: 'primaryActivityDesc',
    value: ''
  },

  secondaryActivityCode: {
    name: 'secondaryActivityCode',
    value: ''
  },

  secondaryActivityDesc: {
    name: 'secondaryActivityDesc',
    value: ''
  },

  mailingCountry: {
    name: 'mailingCountry',
    isValid: true,
    value: 'SG',
    searchValue: '',
    errorMsg: '',
    isInitial: true
  },

  mailingAddressCheckbox: {
    isToggled: false,
    isValid: true,
  },
  overseasAddress1: {
    name: 'overseasAddress1',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true
  },
  overseasAddress2: {
    name: 'overseasAddress2',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true
  },
  overseasAddress3: {
    name: 'overseasAddress3',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true
  },
  overseasAddress4: {
    name: 'overseasAddress4',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true
  },
  mailingCity: {
    name: 'mailingCity',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true
  },
  unformattedCompanyCountry: {
    name: 'unformattedCompanyCountry',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true
  },
  companyAddressLine1: {
    name: 'companyAddressLine1',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true
  },
  companyAddressLine2: {
    name: 'companyAddressLine2',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true
  },
  standard: {
    name: 'standard',
    value: '',
    isValid: true,
    errorMsg: '',
    isInitial: true
  },
  annualTurnover: {
    name: "annualTurnover",
    isValid: true,
    isFocus: false,
    value: '',
    errorMsg: '',
    isInitial: true,
    description: ''
  },
  primaryClientele: [],
  primaryClienteleIsValid: true,
  individualClients: {
    isToggled: false,
    isValid: false
  },
  corporateClients: {
    isToggled: false,
    isValid: false
  }
};

const companyBasicDetailsReducer = (state = initialState, action) => {
  switch (action.type) {

    case types.COMPANY_SELECT_RADIO_BUTTON:
      return {
        ...state,
        [action.field]: { ...state[action.field], value: action.value, errorMsg: "", isValid: true }
      }

    case types.MYINFO_SET_COMPANY_ADDRESS_TYPE:
      return {
        ...state,
        standard: {
          ...state.standard,
          value: action.standard
        }
      }

    case types.MYINFO_SET_COMPANY_UNFORMATTED_ADDRESS_DATA:
      return {
        ...state,
        unformattedCompanyCountry: {
          ...state.unformattedCompanyCountry,
          value: action.unformattedCompanyCountry
        },
        companyAddressLine1: {
          ...state.companyAddressLine1,
          value: action.companyAddressLine1
        },
        companyAddressLine2: {
          ...state.companyAddressLine2,
          value: action.companyAddressLine2
        }
      }

    case types.MYINFO_SET_COMPANY_BASIC_DETAIL_DATA:
      return {
        ...state,
        companyRegistrationNumber: {
          ...state.companyRegistrationNumber,
          value: action.companyRegistrationNumber
        },
        registeredCompanyName: {
          ...state.registeredCompanyName,
          value: action.registeredCompanyName,
        },
        typeOfCompany: {
          ...state.typeOfCompany,
          value: action.typeOfCompany,
        },
        businessConstitutionType: {
          ...state.businessConstitutionType,
          value: action.businessConstitutionType,
        },
        companyStatus: {
          ...state.companyStatus,
          value: action.companyStatus,
        },
        countryOfIncorporation: {
          ...state.countryOfIncorporation,
          value: action.countryOfIncorporation,
        },
        ownership: {
          ...state.ownership,
          value: action.ownership,
        },
        registrationDate: {
          ...state.registrationDate,
          value: action.registrationDate,
        },
        companyExpiryDate: {
          ...state.companyExpiryDate,
          value: action.companyExpiryDate,
        },
        primaryActivityCode: {
          ...state.primaryActivityCode,
          value: action.primaryActivityCode,
        },
        primaryActivityDesc: {
          ...state.primaryActivityDesc,
          value: action.primaryActivityDesc,
        },
        secondaryActivityCode: {
          ...state.secondaryActivityCode,
          value: action.secondaryActivityCode
        },
        secondaryActivityDesc: {
          ...state.secondaryActivityDesc,
          value: action.secondaryActivityDesc,
        }
      };
    case types.LENDING_IS_CHECKED:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          isToggled: action.isToggled,
          isValid: true
        }
      };

    case types.MAILINGADDRESS_DROPDOWN_FOCUS:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          isFocus: action.isFocus
        }
      };

    case types.MAILINGADDRESS_DROPDOWN_ITEM_SELECT:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          value: action.value,
          description: action.description,
          isValid: true,
          errorMsg: '',
          isInitial: false
        }
      };

    case types.COMPANYBASIC_HANDLE_TEXT_INPUT_CHANGE:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          value: action.value,
          isValid: action.isValid,
          errorMsg: action.errorMsg,
          isInitial: false
        }
      };

    case types.COMPANYBASICDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          searchValue: action.searchValue
        }
      };

    case types.SET_ERROR_MESSAGE_INPUT:
      return {
        ...state,
        [action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
      };

    case types.SET_RETRIEVE_COMPANY_DETAILS:          
      return {
          ...state,
          annualTurnover: { ...state.annualTurnover, value: action.annualTurnover, isInitial: false },
          primaryClientele: action.primaryClienteleBase,
          individualClients:  {...state.individualClients, isToggled: action.individualClients, isValid: action.individualClients ? true:false},
          corporateClients:  {...state.corporateClients, isToggled: action.corporateClients, isValid: action.corporateClients ? true:false},
          mailingAddressCheckbox: {...state.mailingAddressCheckbox, isToggled: action.mailingAddressCheckbox }
      }

    case types.PRIMARY_CLIENTELE_HANDLE_TOGGLE:
    return {
        ...state,
        [action.field]: {
          ...state[action.field],
          isToggled: action.isToggled,
          isValid: action.isValid
        },
        primaryClientele: action.primaryClientele
      };

    case types.PRIMARY_CLIENTELE_IS_VALID:
      return {
        ...state,
        primaryClienteleIsValid: action.status,
      };

    default:
      return state;
  }
}

export default companyBasicDetailsReducer;
