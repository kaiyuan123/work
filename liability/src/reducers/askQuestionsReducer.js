import * as types from './../actions/actionTypes';

const initialState = { 
  popuptoggle: false,
  noOfQuestions: 0,
  noOfButtonChecked: 0,
  questionnaires: []
};

const askQuestionsReducer = (state = initialState, action) => { 
  switch (action.type) { 

    case types.ASK_POPUP_SHOW:
      return {
          ...state,
          popuptoggle: action.popuptoggle,
          noOfQuestions: action.noOfQuestions,
          questionnaires: [action.questionnaires]
        };

      case types.ASK_POPUP_QUESTION_VALUES:
      return {
          ...state, 
          questionnaires: { ...state.questionnaires },
          noOfButtonChecked: action.noOfButtonChecked
        };
        
      case types.ASK_POPUP_HIDE_ON_CLOSE_BUTTON:
      return {
          ...state, 
          popuptoggle: action.popuptoggle,
          noOfQuestions: action.noOfQuestions,
          questionnaires: [],
          noOfButtonChecked: action.noOfButtonChecked
        };

      case types.ASK_POPUP_HIDE_ON_OK_BUTTON:
      return {
        ...state,
        popuptoggle: action.popuptoggle,
      }

      case types.SET_ERROR_MESSAGE_QUESTIONS:
      return {
        ...state,
        questionnaires: { ...state.questionnaires }
      }

        
      default:
        return state;
  }
}

export default askQuestionsReducer;