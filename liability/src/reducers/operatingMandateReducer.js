import * as types from './../actions/actionTypes';

const initialState = {
	typeOfCompany: "",

	signatorySelected: {
		name: "signatorySelected",
		isValid: true,
		isFocus: false,
		value: '',
		errorMsg: '',
		isInitial: true,
		description: ''
	},
	mainApplicantLegalId: "",
	approvedSignatories: {
		name: 'approvedSignatories',
		openRows: [],
		value: []
	},
	signatoriesID: [0],
	signatories0: {
		signatoriesName: {
			name: "signatoriesName",
			isValid: true,
			value: "",
			errorMsg: "",
			isInitial: true,
			isReadOnly: false,
			isMyInfo: false,
			hasEdit: false
		},
		signatoriesNRIC: {
			name: "signatoriesNRIC",
			isValid: true,
			value: "",
			errorMsg: "",
			isInitial: true,
			isReadOnly: false,
			isMyInfo: false,
			hasEdit: false
		},
		signatoriesEmail: {
			name: "signatoriesEmail",
			isValid: true,
			value: "",
			errorMsg: "",
			isInitial: true,
			isReadOnly: false,
			isMyInfo: false,
			hasEdit: false
		},
		signatoriesMobileNo: {
			name: "signatoriesMobileNo",
			isValid: true,
			value: "",
			errorMsg: "",
			isInitial: true,
			isReadOnly: false,
			isMyInfo: false,
			hasEdit: false
		},
		signatoriesBibUserId: {
			name: "signatoriesBibUserId",
			isValid: true,
			value: "",
			errorMsg: "",
			isInitial: true,
			isMyInfo: false,
			hasEdit: false
		},
		signatoriesToggleBIBUser: {
			isToggled: false,
			isValid: false
		}
	},
	setupBIBPlusUser: {
		isToggled: false,
		isValid: false,
		isDisabled: false
	},
	normalSectionArr: [],
	complexSectionArr: [],
	setReviewSection: false

};

const operatingMandateReducer = (state = initialState, action) => {
	switch (action.type) {

		case types.OPERATING_MANDATE_NAME_HANDLE_TEXT_INPUT_CHANGE:
			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: {
						...state[action.key][action.field],
						value: action.value,
						isValid: action.isValid,
						errorMsg: action.errorMsg,
						isInitial: false
					}
				}
			};

		case types.SIGNATORY_CHECKED_VALUE:
			return {
				...state,
				[action.field]: { ...state[action.field], value: action.value, errorMsg: "", isValid: true }
			}

		case types.SET_REVIEW_SECTION:
			return {
				...state,
				setReviewSection: action.setReviewSection
			}

		case types.APPROVED_SIGNATORY_LIST:
			return {
				...state,
				approvedSignatories: { ...state.approvedSignatories, value: action.signatories },
				signatorySelected: {...state.signatorySelected, value: action.signatorySelected },
				mainApplicantLegalId: action.mainApplicantLegalId
			}

		case types.SET_SIGNATORIES_ADDITIONAL_ERROR_MESSAGE_INPUT:
			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: { ...state[action.key][action.field], errorMsg: action.errorMsg, isValid: false,value: action.value }
				}
			};

		case types.INCREMENT_SIGNATORYLIST_COUNT:
			return {
				...state,
				signatoriesID: action.signatoriesID
			};

		case types.POPULATE_SIGNATORY_LIST:
			// if(action.signatoriesMobileNo === ""){
			// 	action.signatoriesMobileNo = "+65"
			// }
			return {
				...state,
				[action.key]: {
					...state[action.key],
					signatoriesName: {
						...state[action.key]["signatoriesName"],
						value: action.signatoriesName,
						isInitial: true,
						isValid: true,
						isReadOnly: true,
						hasEdit: false
					},
					signatoriesNRIC: {
						...state[action.key]["signatoriesNRIC"],
						value: action.signatoriesNRIC,
						isInitial: true,
						isValid: true,
						isReadOnly: true,
						hasEdit: false
					},
					signatoriesEmail: {
						...state[action.key]["signatoriesEmail"],
						value: action.signatoriesEmail,
						isInitial: true,
						isValid: true,
						isReadOnly: false,
						isMyInfo: action.emailMyinfo,
						hasEdit: false
					},
					signatoriesMobileNo: {
						...state[action.key]["signatoriesMobileNo"],
						value: action.signatoriesMobileNo,
						isInitial: true,
						isValid: true,
						isReadOnly: false,
						isMyInfo: action.phoneMyinfo,
						hasEdit: false
					},
					signatoriesBibUserId: {
						name: "signatoriesBibUserId",
						value: action.signatoriesBibUserId,
						errorMsg: "",
						isInitial: true,
						isReadOnly: false,
						isValid: true,
						isMyInfo: action.bibUserIdMyinfo,
						hasEdit: false
					},
					signatoriesToggleBIBUser: {
						...state[action.key],
						isToggled: action.signatoriesToggleBIBUser,
						isValid: true

					}
				}
			};

		case types.UPDATE_BIB_USER_TOGGLE:
			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: {
						...state[action.key][action.field],
						isToggled: action.isToggled,
						isValid: action.isValid
					}
				}

			}

		case types.POPULATE_ADDITIONAL_SIGNATORY_LIST:
			// if(action.signatoriesMobileNo === ""){
			// 	action.signatoriesMobileNo = "+65"
			// }
			return {
				...state,
				[action.signatories]: {
					signatoriesName: {
						name: "signatoriesName",
						isValid: true,
						value: action.signatoriesName,
						errorMsg: "",
						isInitial: true,
						isReadOnly: true,
						hasEdit: false
					},

					signatoriesNRIC: {
						name: "signatoriesNRIC",
						isValid: true,
						value: action.signatoriesNRIC,
						errorMsg: "",
						isInitial: true,
						isReadOnly: true,
						hasEdit: false
					},

					signatoriesEmail: {
						name: "signatoriesEmail",
						isValid: true,
						value: action.signatoriesEmail,
						errorMsg: "",
						isInitial: true,
						isReadOnly: false,
						isMyInfo: action.emailMyinfo,
						hasEdit: false
					},

					signatoriesMobileNo: {
						name: "signatoriesMobileNo",
						isValid: true,
						value: action.signatoriesMobileNo,
						errorMsg: "",
						isInitial: true,
						isReadOnly: false,
						isMyInfo: action.phoneMyinfo,
						hasEdit: false
					},
					signatoriesBibUserId: {
						name: "signatoriesBibUserId",
						value: action.signatoriesBibUserId,
						errorMsg: "",
						isInitial: true,
						isReadOnly: false,
						isValid: true,
						isMyInfo: action.bibUserIdMyinfo,
						hasEdit: false
					},
					signatoriesToggleBIBUser: {
						...state[action.key],
						isToggled: action.signatoriesToggleBIBUser,
						isValid: true

					}
				}
			};

		case types.BIBUSER_HANDLE_TOGGLE:
			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: {
						...state[action.key][action.field],
						isToggled: action.isToggled,
						isValid: action.isValid
					},
					signatoriesBibUserId: {
						...state[action.key]['signatoriesBibUserId'],
						name: "signatoriesBibUserId",
						value: '',
						isValid: action.isToggled ? true : false,
						errorMsg: '',
						isInitial: false
					}
				}

			}


		case types.BIBPLUSEUSER_HANDLE_TOGGLE:
			return {
				...state,
				[action.field]: {
					...state[action.field],
					isToggled: action.isToggled

				}
			}

		case types.SET_ERROR_MESSAGE_INPUT:
			return {
				...state,
				[action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
			};

		case types.SET_SIGNATORIES_PARTNERS_INPUT_ERROR:

			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: {
						...state[action.key][action.field],
						isValid: false,
						errorMsg: action.errorMsg,
						isInitial: false
					}
				}
			};

		case types.SET_TEXT_INPUT_HAS_EDITED_WITH_KEY:
			return {
				...state,
				[action.key]: {
					...state[action.key],
					[action.field]: {
						...state[action.key][action.field],
						hasEdit: true
					}
				}
			};
		default:
			return state;
	}

}

export default operatingMandateReducer;
