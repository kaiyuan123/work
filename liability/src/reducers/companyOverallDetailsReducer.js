import * as types from './../actions/actionTypes';

const initialState = {
    previousNames: {
        name: 'previousNames',
        openRows: [],
        value: []
    },
    previousUens: {
        name: 'previousUens',
        openRows: [],
        value: []
    },
    capitals: {
        name: 'capitals',
        openRows: [],
        value: []
    },
    appointments: {
        name: 'appointments',
        openRows: [],
        value: []
    },
    shareholders: {
        name: 'shareholders',
        openRows: [],
        value: []
    },
    financials: {
        name: 'financials',
        openRows: [],
        value: []
    },
    grants: {
        name: 'grants',
        openRows: [],
        value: []
    }
};

const companyOverallDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_MYINFO_COMPANY_PREVIOUSNAMES_DATA:
            return {
                ...state,
                previousNames: { ...state.previousNames, value: action.previousNames }
            };

        case types.SET_MYINFO_COMPANY_PREVIOUSUEN_DATA:
            return {
                ...state,
                previousUens: { ...state.previousUens, value: action.previousUens }
            };

        case types.SET_MYINFO_COMPANY_CAPITALS_DATA:
            return {
                ...state,
                capitals: { ...state.capitals, value: action.capitals }
            };

        case types.SET_MYINFO_COMPANY_APPOINTMENT_DATA:
            return {
                ...state,
                appointments: { ...state.appointments, value: action.appointments }
            };

        case types.SET_MYINFO_COMPANY_SHAREHOLDERS_DATA:
            return {
                ...state,
                shareholders: { ...state.shareholders, value: action.shareholders }
            };

        case types.SET_MYINFO_COMPANY_FINANCIALS_DATA:
            return {
                ...state,
                financials: { ...state.financials, value: action.financials }
            };

        case types.SET_MYINFO_COMPANY_GRANTS_DATA:
            return {
                ...state,
                grants: { ...state.grants, value: action.grants }
            }

        

        default:
            return state;
    }
}

export default companyOverallDetailsReducer;
