import * as types from "./../actions/actionTypes";

const initialState = {
  emailAddress: {
    name: "emailAddress",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true,
    isMyInfo: false,
    hasEdit: false
  },
  entityName: {
    name: "entityName",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true
  },
  uen: {
    name: "uen",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true
  },
  principalName: {
    name: "principalName",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true
  },
  alternateNames: {},
  mobileNumber: '',
  isCommerical: false,
  passportNumber: {
    name: "passportNumber",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true
  },
  passportExpiryDate: {
    name: "passportExpiryDate",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true
  },
  workPassStatus: {
    name: "workPassStatus",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true
  },
  workPassExpiryDate: {
    name: "workPassExpiryDate",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true
  },
  entityType: {
    name: "entityType",
    isValid: true,
    value: "",
    errorMsg: "",
    isInitial: true
  },
  entitySubType: {
    name: "entitySubType",
    isValid: true,
    value: "",
    searchValue: '',
    errorMsg: '',
    isInitial: true,
    isFocus: false
  },
  companyType: {
    name: "companyType",
    isValid: true,
    value: "",
    searchValue: '',
    errorMsg: '',
    isInitial: true,
    isFocus: false
  },
  businessConstitution: {
    name: "businessConstitution",
    isValid: true,
    value: "",
    searchValue: '',
    errorMsg: '',
    isInitial: true,
    isFocus: false
  },
  primaryCountryOfOperation: {
    name: "primaryCountryOfOperation",
    isValid: true,
    value: "",
    searchValue: '',
    errorMsg: '',
    isInitial: true,
    isFocus: false
  },
  primaryNatureOfBusiness: {
    name: "primaryNatureOfBusiness",
    isValid: true,
    value: "",
    searchValue: '',
    errorMsg: '',
    isInitial: true,
    isFocus: false

  },
  secondaryNatureOfBusiness: {
    name: "secondaryNatureOfBusiness",
    isValid: true,
    value: "",
    searchValue: '',
    errorMsg: '',
    isInitial: true,
    isFocus: false

  }
};

const contactDetailsReducer = (state = initialState, action) => {
  switch (action.type) {

    case types.CONTACTDETAILS_HANDLE_TEXT_INPUT_CHANGE:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          value: action.value,
          isValid: action.isValid,
          errorMsg: action.errorMsg,
          isInitial: false
        }
      };

    case types.SET_ERROR_MESSAGE_INPUT:
      return {
        ...state,
        [action.field]: { ...state[action.field], errorMsg: action.errorMsg, isValid: false }
      };
    case types.SET_CLEAR_SECONDARY:
      return {
        ...state,
        secondaryNatureOfBusiness: { ...state.secondaryNatureOfBusiness, value: "", isValid: true, errorMsg: "" }
      }
    case types.SET_MYINFO_CONTACT_DETAILS:
      return {
        ...state,
        emailAddress: { ...state.emailAddress, value: action.emailAddress, isInitial: true, isMyInfo: action.emailMyinfo },
        entityName: { ...state.entityName, value: action.entityName, isInitial: false },
        uen: { ...state.uen, value: action.uen, isInitial: false },
        entityType: { ...state.entityType, value: action.entityType, isInitial: false },
        companyType: { ...state.companyType, value: action.companyType, isInitial: false },
        businessConstitution: { ...state.businessConstitution, value: action.businessConstitution, isInitial: false },
        principalName: { ...state.principalName, value: action.principalName, isInitial: false },
        mobileNumber: { ...state.mobileNumber, value: action.mobileNumber, isInitial: false },
        alternateNames: action.alternateNames
      };

    case types.SET_RETRIEVE_CONTACT_DETAILS:
      return {
        ...state,
        primaryCountryOfOperation: { ...state.primaryCountryOfOperation, value: action.primaryCountryOfOperation, isInitial: false },
        primaryNatureOfBusiness: { ...state.primaryNatureOfBusiness, value: action.primaryNatureOfBusiness, isInitial: false },
        secondaryNatureOfBusiness: { ...state.secondaryNatureOfBusiness, value: action.secondaryNatureOfBusiness, isInitial: false }

      }

    case types.CONTACTDETAILS_DROPDOWN_FOCUS:
      return { ...state, [action.field]: { ...state[action.field], isFocus: action.isFocus } };

    case types.CONTACTDETAILS_DROPDOWN_ITEM_SELECT:
      return { ...state, [action.field]: { ...state[action.field], value: action.value, description: action.description, isValid: true, errorMsg: '', isInitial: false } };

    case types.CONTACTDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE:
      return { ...state, [action.field]: { ...state[action.field], searchValue: action.searchValue } };

    case types.SET_ERROR_FOR_SECONDARY:
      return {
        ...state,
        [action.field]: {
          ...state[action.field],
          isValid: false,
          errorMsg: action.errorMsg
        }
      }

    case types.SET_TEXT_INPUT_HAS_EDITED:
      return {
        ...state,
        [action.field]: { ...state[action.field], hasEdit: true }
      }
      
    case types.SET_TEXT_INPUT_FOCUS:
      return {
        ...state,
        [action.field]: {...state[action.field], isFocus: action.status}
      }

    default:
      return state;
  }
};

export default contactDetailsReducer;
