/* global GLOBAL_CONFIG */
import axios from 'axios';

const getFormConfigURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.formConfig : `${GLOBAL_CONFIG.API_PATH}/specifications`;
// const getFormConfigURL = GLOBAL_CONFIG.formConfig
const getInitiateMyinfoURL = GLOBAL_CONFIG.initiateMyinfoDataJson;
const getAvailableLocationURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.availableLocationsConfig : `${GLOBAL_CONFIG.API_PATH}/applications`;
// const retrieveLoanWithMyInfoURL = `${GLOBAL_CONFIG.API_PATH}/applications`;
//const retrieveLoanWithMyInfoURL = GLOBAL_CONFIG.myinfoDataJson; //hardcoded data
const retrieveLoanWithMyInfoURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.myinfoDataJson : `${GLOBAL_CONFIG.API_PATH}/applications/application`;
const getAddressByPostalCodeURL = `${GLOBAL_CONFIG.COMMON_API_PATH}/referencedata/address`;
const commonAPIURL = `${GLOBAL_CONFIG.API_PATH}/applications`;
const callbackURL = GLOBAL_CONFIG.Local_Environment ? GLOBAL_CONFIG.myinfoDataJson : `${GLOBAL_CONFIG.CALLBACK_API_PATH}`
const verifyURL = `${GLOBAL_CONFIG.API_PATH}/applications/verify`;
const sendTokenURL = `${GLOBAL_CONFIG.API_PATH}/access/sendotp`;
const verifyTokenURL = `${GLOBAL_CONFIG.API_PATH}/access/verifyotp`;

const validateTINURL = `${GLOBAL_CONFIG.COMMON_API_PATH}/validate/tin`;

const LocalEnvironment = GLOBAL_CONFIG.Local_Environment;
const Force_URL_redirect = GLOBAL_CONFIG.Force_URL_redirect;
const Force_Google_Tag = GLOBAL_CONFIG.Force_Google_Tag;
const root_path = GLOBAL_CONFIG.root_path;
const uobSite = GLOBAL_CONFIG.uobSite;
const apiKey = GLOBAL_CONFIG.inDevelopment ? GLOBAL_CONFIG.apiKeyDevp : GLOBAL_CONFIG.apiKeyProd;
const client = GLOBAL_CONFIG.inDevelopment ? GLOBAL_CONFIG.clientDevp : GLOBAL_CONFIG.clientProd;
const timeout = GLOBAL_CONFIG.timeout;
const httpapi = axios.create();
httpapi.defaults.timeout = timeout;

const Application = {
	submitInitiateApplication: (dataObj, parameter) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/application${parameter}`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	submitPartialApplication: (dataObj, applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	submitApplication: (dataObj, applicationID) => {
		if (dataObj.branchAppointment !== undefined && dataObj.branchAppointment !== null) {
			return new Promise((resolve, reject) => {
				httpapi
					.post(`${commonAPIURL}/${applicationID}/bookappointment`, dataObj)
					.then(response => {
						resolve(response);
					})
					.catch(e => reject(e));
			});
		}
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}/submit`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	saveUploadDocument: (dataObj, applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}/upload`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	saveBookAppointment: (dataObj, applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}/bookings`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},

	askQuestionsSubmission: (dataObj, applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}/question`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	getAvailableLocationData: (applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.get(GLOBAL_CONFIG.Local_Environment ? getAvailableLocationURL : `${GLOBAL_CONFIG.API_PATH}/appointments/slots`)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	myInfoCallback: (parameter) => {
		return new Promise((resolve, reject) => {
			httpapi
				.get(`${callbackURL}${parameter}`)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
	retrieveUENSubmission: (dataObj) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/enquiry`, dataObj)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},

	getVerificationCode: (code) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${verifyURL}?code=${code}`)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},

	deleteApplication: (applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/${applicationID}/delete`)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},

	checkDuplicate: () => {
		return new Promise((resolve, reject) => {
			httpapi
				.post(`${commonAPIURL}/checkduplicate`)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},

	sendOtp: () => {
		return new Promise((resolve, reject) => {
			httpapi.post(sendTokenURL, {}).then((response) => {
				resolve(response);
			}).catch((e) => reject(e));
		});
	},

	verifyOtp: (value) => {
		return new Promise((resolve, reject) => {
			httpapi.post(verifyTokenURL, { otp: value }).then((response) => {
				resolve(response);
			}).catch((e) => reject(e));
		});
	},

	getApplication: (applicationID) => {
		return new Promise((resolve, reject) => {
			httpapi
				.get(`${commonAPIURL}/${applicationID}`)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},

	validateTIN: (country, tinNo) => {
		return new Promise((resolve, reject) => {
			httpapi
				.get(`${validateTINURL}/${country}/${tinNo}`)
				.then(response => {
					resolve(response);
				})
				.catch(e => reject(e));
		});
	},
};

const Config = {
	getInitialData: () => {
		return new Promise((resolve) => {
			axios.get(getFormConfigURL).then((response) => {
				resolve(response.data);
			});
		});
	},
	getMyInfoData: () => {
		return new Promise((resolve, reject) => {
			axios.get(retrieveLoanWithMyInfoURL).then((response) => {
				resolve(response.data);
			}).catch((e) => reject(e));
		});
	},
	getInitiateMyinfoData: () => {
		return new Promise((resolve, reject) => {
			axios.get(getInitiateMyinfoURL).then((response) => {
				resolve(response.data);
			}).catch((e) => reject(e));
		});
	}
}

const Address = {
	getAddressByCode: (postCode) => {
		return new Promise((resolve, reject) => {
			axios.get(`${getAddressByPostalCodeURL}?postalcode=${postCode}`).then((response) => {
				resolve(response);
			}).catch((e) => reject(e));
		})
	}
};

export {
	Application,
	Config,
	Address,
	LocalEnvironment,
	Force_URL_redirect,
	Force_Google_Tag,
	root_path,
	uobSite,
	apiKey,
	client
}
