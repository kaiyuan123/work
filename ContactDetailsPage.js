import React, { Component } from "react";
import { connect } from "react-redux";
// import Dropdown from "./../components/Dropdown/Dropdown";
import RadioButton from "./../components/RadioButton/RadioButton";
import TextInput from "./../components/TextInput/TextInput";
import PrimaryButton from "./../components/PrimaryButton/PrimaryButton";
import { handleTextInputChange } from "./../actions/contactDetailsAction";
import { capitalize } from "../common/utils";
import { mapValueToDescription } from "./../common/utils";
import {
	setDropdownFocusStatus,
	selectDropdownItem,
	changeSearchInputValue,
	selectRadioItem
} from "./../actions/contactDetailsAction";

class ContactDetailsPage extends Component {
	handleToPersonalIncomeDetails() {
		this.props.onContinue();
	}

	handleOnChange(data, field) {
		const { dispatch } = this.props;
		dispatch(handleTextInputChange(data, field));
	}

	isContactDetailsPassChecking(){
		this.props.onCheck();
	}

	handleDropdownBlur(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(false, field));
	}

	handleDropdownFocus(field) {
		const { dispatch } = this.props;
		dispatch(setDropdownFocusStatus(true, field));
	}

	handleDropdownClick(data, field) {
		const { dispatch, contactDetailsReducer } = this.props;
		if (contactDetailsReducer[field].value === data.value) {
			return;
		}
		dispatch(selectDropdownItem(data.value, data.description, field));
	}

	handleOnSearchChange(e, field) {
		const { dispatch } = this.props;
		const value = e.target.value;
		dispatch(changeSearchInputValue(value, field));
	}

	handleRadioSelection(data, field) {
		const { dispatch } = this.props;
		dispatch(selectRadioItem(data, field));
	}

	render() {
		const { commonReducer, contactDetailsReducer } = this.props;
		const {
			emailAddress,
			entityName,
			uen,
			principalName,
			mobileNumber,
			alternateNames,
			annualTurnover,
			requestLoan,
			isCommerical
		} = contactDetailsReducer;
		const errorMsgList = commonReducer.appData.errorMsgs;
		const inputValues = commonReducer.appData.inputValues;
		const requestLoanList = inputValues.requestLoanType;
		const annualTurnoverList = inputValues.annualTurnoverType;
		const contactDetailsValue = commonReducer.appData.contactDetails;
		const labels = contactDetailsValue.labels;


		const contactFieldValid = emailAddress.isValid && emailAddress.value !== '';
		const loanValid = annualTurnover.isValid && annualTurnover.value !== '' && requestLoan.isValid && requestLoan.value !== '';
		const fullValidation = isCommerical ? contactFieldValid && loanValid : contactFieldValid;
		const hanYuPinYinName = alternateNames.hanYuPinYinName ? alternateNames.hanYuPinYinName : '';
		const hanYuPinYinAliasName = alternateNames.hanYuPinYinAliasName ? alternateNames.hanYuPinYinAliasName : '';
		const alias = alternateNames.alias ? alternateNames.alias : '';
		const marriedName = alternateNames.marriedName ? alternateNames.marriedName : '';
		const haveAlternateName = Object.keys(alternateNames).length > 0;
		const otherNames = () => {
			const tmpArray = [];
			if (hanYuPinYinName !== "")
				tmpArray.push(
					<div key="alias1">
						{capitalize(hanYuPinYinName)}
					</div>
				);
			if (hanYuPinYinAliasName !== "")
				tmpArray.push(
					<div key="alias2">
						{capitalize(hanYuPinYinAliasName)}
					</div>
				);
			if (alias !== "")
				tmpArray.push(
					<div key="alias3">
						{capitalize(alias)}
					</div>
				);
			if (marriedName !== "")
				tmpArray.push(
					<div key="alias4">
						{capitalize(marriedName)}
					</div>
				);
			return tmpArray;
		};
		const isDisabled = (commonReducer.currentSection !== 'contactDetails' && commonReducer.currentSection !== 'thankyou') ? true : false;
		const positionRelative = isDisabled ? "" : "positionRelative";

		return (
			<div className="uob-content" id="contactDetails-section">
				<div className="prefillSubtitle-container">
					<div className="prefillSubtitle-text">
						<span className="prefillSubtitle-icon">&#9432;</span>
						{contactDetailsValue.prefillSubtitle}
					</div>
				</div>
				<h1>{contactDetailsValue.title}</h1>
				<p className="uob-headline">
					{contactDetailsValue.headline}
				</p>

				<div className="top-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow right-border">
							<TextInput
								inputID="entityName"
								isReadOnly={true}
								label={labels.entityName}
								value={entityName.value}
								errorMsg={entityName.errorMsg}
								onChange={data => this.handleOnChange(data, "emailAddress")}
								isValid={entityName.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
							/>
						</div>
						<div className="halfRow positionRelative">
							<TextInput
								inputID="uen"
								isReadOnly={true}
								label={labels.uen}
								value={uen.value}
								errorMsg={uen.errorMsg}
								onChange={data => this.handleOnChange(data, "uen")}
								isValid={uen.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
							/>
						</div>
					</div>
				</div>

				<div className="sub-div-common">
					<div className="p-l-10">
						<TextInput
							inputID="principalName"
							label={labels.principalName}
							value={capitalize(principalName.value)}
							errorMsg={principalName.errorMsg}
							onChange={data => this.handleOnChange(data, "principalName")}
							isValid={principalName.isValid}
							validator={["required"]}
							errorMsgList={errorMsgList}
							isReadOnly={true}
							hasIcon={false}
						/>
					</div>
				</div>

				{haveAlternateName &&
					<div className="sub-div-common alignCenter" style={{ minHeight: '70px', height: 'initial' }}>
						<div className="fullTable confirmDetails-flexContainer">
							<div className="p-l-10" style={{ padding: '10px' }}>
								<div className="confirmDetails-label-container ">
									<div className="confirmDetails-labelValue">
										<div className="confirmDetails-LabelCSS">
											{labels.alias}
										</div>
										<div className="confirmDetails-ValueCSS">
											{otherNames().length > 0 && (
												<div>{otherNames()}</div>
											)}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				}

				<div className="top-border bottom-border">
					<div className="fullTable confirmDetails-flexContainer">
						<div className="halfRow right-border">
							<TextInput
								type="Phone"
								inputID="mobileNumber"
								isReadOnlyMobile={true}
								label={labels.mobileNumber}
								value={mobileNumber.value}
								errorMsg={mobileNumber.errorMsg}
								onChange={data => this.handleOnChange(data, "mobileNumber")}
								isValid={mobileNumber.isValid}
								validator={["required"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
								onlySg={false}
							/>
						</div>
						<div className={`halfRow ${positionRelative}`}>
							<TextInput
								inputID="email"
								isReadOnly={false}
								label={labels.emailAddress}
								value={emailAddress.value}
								errorMsg={emailAddress.errorMsg}
								onChange={data => this.handleOnChange(data, "emailAddress")}
								isValid={emailAddress.isValid}
								validator={["required", "isEmail", "maxSize|30"]}
								errorMsgList={errorMsgList}
								hasIcon={false}
								isDisabled={isDisabled}
							/>
						</div>
					</div>
				</div>

				{isCommerical &&
					<div>
						<div className="sub-title"> {labels.requestLoan} </div>
							<div className="sub-sub-title"> {labels.requestLoanSubtitle}</div>
									<RadioButton
										inputID="requestLoan"
										value = {requestLoan.value}
										radioObj = {mapValueToDescription(
											requestLoanList
										)}
										errorMsg={requestLoan.errorMsg}
										isValid={requestLoan.isValid}
										isDisabled={isDisabled}
										onClick={(data) => this.handleRadioSelection(data, "requestLoan")}
									/>

						<div className="sub-title"> {labels.annualTurnover} </div>
							<div className="sub-sub-title"> {labels.annualTurnoverSubtitle}</div>
									<RadioButton
										inputID="annualTurnover"
										value = {annualTurnover.value}
										radioObj = {mapValueToDescription(
											annualTurnoverList
										)}
										errorMsg={annualTurnover.errorMsg}
										isValid={annualTurnover.isValid}
										isDisabled={isDisabled}
										onClick={(data) => this.handleRadioSelection(data, "annualTurnover")}
									/>
					</div>
				}
				<div className="uob-terms" dangerouslySetInnerHTML={{ __html: contactDetailsValue.terms }} />
				<div className="uob-input-separator fixedContinue">
					<PrimaryButton label={labels.continueButton} onClick={() => this.handleToPersonalIncomeDetails()} isLoading={commonReducer.isProcessing} />
				</div>

				{
					!fullValidation &&
					<div>
						<div className="uob-input-separator fixedContinue fixedContinueGray">
							<PrimaryButton label={labels.continueButton} onClick={() => this.isContactDetailsPassChecking()} isLoading={commonReducer.isProcessing} />
						</div>
					</div>
				}

			</div>
		);
	}
}

const mapStateToProps = state => {
	const { commonReducer, contactDetailsReducer } = state;
	return { commonReducer, contactDetailsReducer };
};

export default connect(mapStateToProps)(ContactDetailsPage);
