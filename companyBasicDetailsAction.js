import * as types from "./actionTypes";

export const setCompanyBasicData = (obj) => {
    return {
        type: types.MYINFO_SET_COMPANY_BASIC_DETAIL_DATA,
        companyRegistrationNumber: obj && obj["uen"] ? obj["uen"] : "",
        registeredCompanyName: obj && obj.entityName ? obj.entityName : "",
        typeOfCompany: obj && obj["entityType"] ? obj["entityType"] : "",
        companyStatus: obj && obj["entityStatus"] ? obj["entityStatus"] : "",
        countryOfIncorporation: obj && obj["countryOfIncorporation"] ? obj["countryOfIncorporation"] : "",
        ownership: obj && obj["ownership"] ? obj["ownership"] : "",
        registrationDate: obj && obj["registrationDate"] ? obj["registrationDate"] : "",
        companyExpiryDate: obj && obj["businessExpiryDate"] ? obj["businessExpiryDate"] : "",
        primaryActivityCode:
            obj && obj["primaryActivityCode"]
                ? obj["primaryActivityCode"]
                : "",
        primaryActivityDesc:
            obj && obj["primaryActivityDesc"]
                ? obj["primaryActivityDesc"]
                : "",
        secondaryActivityCode: obj && obj["secondaryActivityCode"] ? obj["secondaryActivityCode"] : "",
        secondaryActivityDesc: obj && obj["secondaryActivityDesc"] ? obj["secondaryActivityDesc"] : "",

    };
};

export const setCompanyUnformattedAddress = (obj) => {
    return {
        type: types.MYINFO_SET_COMPANY_UNFORMATTED_ADDRESS_DATA,
        unformattedCompanyCountry: obj && obj["country"] ? obj["country"] : "",
        companyAddressLine1: obj && obj["line1"] ? obj["line1"] : "",
        companyAddressLine2: obj && obj["line2"] ? obj["line2"] : ""
    }
}

export const setCompanyAddressType = (standard) => {
    return {
        type: types.MYINFO_SET_COMPANY_ADDRESS_TYPE,
        standard: standard
    }
}

export const handleIsChecked = (field, isToggled) => {
    return {
        type: types.LENDING_IS_CHECKED,
        field,
        isToggled: !isToggled
    }
}

//handle dropdown focus
export const setDropdownFocusStatus = (isFocus, field) => {
    return (dispatch) => {
        dispatch({
            type: types.MAILINGADDRESS_DROPDOWN_FOCUS,
            isFocus,
            field
        });
    };
}

//choose item from dropdown
export const selectDropdownItem = (value, field, description) => {
    if (field === "mailingCountry") {
        const element = document.getElementById("editableDropdown-flagImg");
        element.style.display = "block";
    }

    return (dispatch) => {
        dispatch({
            type: types.MAILINGADDRESS_DROPDOWN_ITEM_SELECT,
            value,
            field,
            description
        })
    };
}

export const handleTextInputChange = (data, field) => {
    return (dispatch) => {
        dispatch({
            type: types.COMPANYBASIC_HANDLE_TEXT_INPUT_CHANGE,
            value: data.value,
            field,
            isValid: data.isValid,
            errorMsg: data.errorMsg,
            isInitial: data.isInitial
        });
    };
};

export const changeSearchInputValue = (searchValue, field) => {
    return (dispatch) => {
        dispatch({
            type: types.COMPANYBASICDETAILS_DROPDOWN_SEARCH_INPUT_CHANGE,
            searchValue,
            field
        });
    };
}

export const selectRadioItem = (data, field) => {
    return (dispatch) => {
      dispatch({
        type: types.CRS_SELECT_RADIO_BUTTON,
        value: data.value,
        field
      })
    }
}